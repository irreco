#ifdef WIN32
#include "stdafx.h"
#endif

#include <stdlib.h>

#include "IRTrans.h"


#ifdef WIN32

BOOL APIENTRY DllMain( HANDLE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
    switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
		case DLL_THREAD_ATTACH:
		case DLL_THREAD_DETACH:
		case DLL_PROCESS_DETACH:
			break;
    }
    return TRUE;
}

#endif


// ToDo:
// Byte Ordering (Big / Little Endian)
// irserver + LAN Firmware: CCF Umsetzung auf Long Codes (Calibration ?)



char WsockInit = 0;
NETWORKSTATUS	globalstat;
NETWORKRECV		globalrcv;
static			byte byteorder;


static void		SwapStatusBuffer (NETWORKSTATUS *stat);
static void		SwapCommandBuffer (NETWORKCOMMAND *com);
static void		swap_word (word *pnt);
static void		swap_long (int32_t *pnt);
static int		NetworkCommand (SOCKET serv, int netcommand,char remote[],char command[],word timeout,int adr,NETWORKSTATUS *stat);
static int		SendCommand (SOCKET serv, NETWORKCOMMAND *com,NETWORKSTATUS *stat);
static char		*trim (char st[],int i);
static int		FormatCCF (char ccfstr[],word data[],int len);

IRTRANSDLL_API int GetDeviceStatus (SOCKET serv, void *result)
{
	int res;

	NETWORKMODE status;
	NETWORKCOMMAND com;


	memset (&com,0,sizeof (com));
	com.netcommand = COMMAND_STATUS;

	memset (&status,0,sizeof (status));
	res = SendCommand (serv,&com,(NETWORKSTATUS *)&status);

	if (status.statustype == STATUS_MESSAGE) {
		memcpy (result,&status,sizeof (NETWORKSTATUS));
		return (1);
	}

	memcpy (result,&status,sizeof (NETWORKMODE));
	return (0);
}


IRTRANSDLL_API int GetDeviceStatusExN (SOCKET serv, int offset,void *result)
{
	int res;

	NETWORKMODEEXN status;
	NETWORKCOMMAND com;


	memset (&com,0,sizeof (com));
	com.adress = offset;
	com.netcommand = COMMAND_STATUSEXN;

	memset (&status,0,sizeof (status));
	res = SendCommand (serv,&com,(NETWORKSTATUS *)&status);

	if (status.statustype == STATUS_MESSAGE) {
		memcpy (result,&status,sizeof (NETWORKSTATUS));
		return (1);
	}

	memcpy (result,&status,sizeof (NETWORKMODEEXN));
	return (0);
}


IRTRANSDLL_API NETWORKSTATUS *ReloadIRDatabase (SOCKET serv)
{
	int res;
	res = NetworkCommand (serv, COMMAND_RELOAD,"","",0,0,&globalstat);

	if (res) return (&globalstat);
	else return (0);
}


IRTRANSDLL_API NETWORKSTATUS *SendCCFCommand (SOCKET serv, char ccfstr[],byte repeat,word mask,byte LEDSel,byte bus)
{
	int res;
	CCFCOMMAND ccf;
	unsigned int adr = 0;

	if (mask) adr = (mask & 0xffff) | 0x10000;

	if (bus == 255) adr |= 0x40000000;
	else adr |= (bus & (MAX_IR_DEVICES - 1)) << 19;

	if (LEDSel) {
		if (LEDSel == 'i' || LEDSel == 'I') adr |= 1 << 17;
		else if (LEDSel == 'e' || LEDSel == 'E') adr |= 2 << 17;
		else if (LEDSel == 'b' || LEDSel == 'B') adr |= 3 << 17;
		else if (LEDSel == 'a' || LEDSel == 'A') adr |= 3 << 17;
		else if (LEDSel == 'd' || LEDSel == 'D');
		else if (LEDSel >= 1 && LEDSel <= 8) adr |= 0x80000000 | ((LEDSel - 1) << 27);
	}

	memset (&ccf,0,sizeof (CCFCOMMAND));

	ccf.adress = adr;
	ccf.netcommand = COMMAND_SENDCCF;
	ccf.timeout = repeat;
	if (FormatCCF (ccfstr,ccf.ccf_data,256)) {
		memset (&globalstat,0,sizeof (NETWORKSTATUS));
		globalstat.netstatus = ERR_CCFLEN;
		globalstat.statuslevel = IR;
		globalstat.statustype = STATUS_MESSAGE;
		globalstat.statuslen = sizeof (NETWORKSTATUS);
		strcpy (globalstat.message,"CCF Error: CCF Code too long (max 256)\n");
		SwapStatusBuffer ((NETWORKSTATUS *)&globalstat);

		return (&globalstat);
	}

	res = SendCommand (serv,(NETWORKCOMMAND *)&ccf,&globalstat);
	if (res) return (&globalstat);
	else return (0);
}


IRTRANSDLL_API NETWORKSTATUS *SendCCFCommandLong (SOCKET serv, char ccfstr[],byte repeat,word mask,byte LEDSel,byte bus)
{
	int res;
	LONGCCFCOMMAND ccf;
	unsigned int adr = 0;

	if (mask) adr = (mask & 0xffff) | 0x10000;

	if (bus == 255) adr |= 0x40000000;
	else adr |= (bus & (MAX_IR_DEVICES - 1)) << 19;

	if (LEDSel) {
		if (LEDSel == 'i' || LEDSel == 'I') adr |= 1 << 17;
		else if (LEDSel == 'e' || LEDSel == 'E') adr |= 2 << 17;
		else if (LEDSel == 'b' || LEDSel == 'B') adr |= 3 << 17;
		else if (LEDSel == 'a' || LEDSel == 'A') adr |= 3 << 17;
		else if (LEDSel == 'd' || LEDSel == 'D');
		else if (LEDSel >= 1 && LEDSel <= 8) adr |= 0x80000000 | ((LEDSel - 1) << 27);
	}

	memset (&ccf,0,sizeof (LONGCCFCOMMAND));

	ccf.adress = adr;
	ccf.netcommand = COMMAND_SENDCCFLONG;
	ccf.timeout = repeat;

	if (FormatCCF (ccfstr,ccf.ccf_data,450)) {
		memset (&globalstat,0,sizeof (NETWORKSTATUS));
		globalstat.netstatus = ERR_CCFLEN;
		globalstat.statuslevel = IR;
		globalstat.statustype = STATUS_MESSAGE;
		globalstat.statuslen = sizeof (NETWORKSTATUS);
		strcpy (globalstat.message,"CCF Error: CCF Code too long (max 450)\n");
		SwapStatusBuffer ((NETWORKSTATUS *)&globalstat);

		return (&globalstat);
	}

	res = SendCommand (serv,(NETWORKCOMMAND *)&ccf,&globalstat);
	if (res) return (&globalstat);
	else return (0);
}



IRTRANSDLL_API NETWORKSTATUS *SendRemoteCommandEx (SOCKET serv, char rem[],char com[],word mask,byte LEDSel,byte bus)
{
	int res;
	unsigned int adr = 0;

	if (mask) adr = (mask & 0xffff) | 0x10000;

	if (bus == 255) adr |= 0x40000000;
	else adr |= (bus & (MAX_IR_DEVICES - 1)) << 19;

	if (LEDSel) {
		if (LEDSel == 'i' || LEDSel == 'I') adr |= 1 << 17;
		else if (LEDSel == 'e' || LEDSel == 'E') adr |= 2 << 17;
		else if (LEDSel == 'b' || LEDSel == 'B') adr |= 3 << 17;
		else if (LEDSel == 'a' || LEDSel == 'A') adr |= 3 << 17;
		else if (LEDSel == 'd' || LEDSel == 'D');
		else if (LEDSel >= 1 && LEDSel <= 8) adr |= 0x80000000 | ((LEDSel - 1) << 27);
	}

	res = NetworkCommand (serv, COMMAND_SEND,rem,com,0,adr,&globalstat);

	if (res) return (&globalstat);
	else return (0);
}



IRTRANSDLL_API NETWORKSTATUS *SendRemoteCommand (SOCKET serv, char rem[],char com[],int mask,int LEDSel,int bus)
{
	int res;
	int adr = 0;

	if (mask) adr = (mask & 0xffff) | 0x10000;

	if (bus == 255) adr |= 0x40000000;
	else adr |= (bus & (MAX_IR_DEVICES - 1)) << 19;

	if (LEDSel) adr |= (LEDSel & 3) << 17;

	res = NetworkCommand (serv, COMMAND_SEND,rem,com,0,adr,&globalstat);

	if (res) return (&globalstat);
	else return (0);
}




IRTRANSDLL_API NETWORKRECV *ReceiveIR (SOCKET serv)
{
	int res;
    STATUSBUFFER buf;

	memset (&buf,0,sizeof (STATUSBUFFER));

	while (1)
	{
		res = recv (serv,(char *)&buf,8,0);
		if (res != 8) return 0;

		if (buf.statuslen > 8)
		{
			res = recv (serv,(((char *)&buf) + 8),(buf.statuslen - 8) ,0);
			if (buf.statustype == STATUS_RECEIVE)
			{
				memcpy (&globalrcv,&buf,sizeof (NETWORKRECV));
				trim (globalrcv.remote,80);
				trim (globalrcv.command,20);
				trim (globalrcv.data,200);

				SwapStatusBuffer ((NETWORKSTATUS *)&globalrcv);

				return (&globalrcv);
			}
		}
	}
}




IRTRANSDLL_API NETWORKSTATUS *LearnIRCode (SOCKET serv, char rem[],char com[],unsigned short timeout)
{
	char st[5];

	NETWORKSTATUS stat;

	st[0] = 0;

	if (NetworkCommand (serv,COMMAND_LRNREM,rem,st,0,'L',&globalstat)) return (&globalstat);

	NetworkCommand (serv,COMMAND_LRNLONG,rem,com,timeout,'*',&globalstat);

	NetworkCommand (serv,COMMAND_CLOSE,rem,st,0,'L',&stat);

	return (&globalstat);
}

IRTRANSDLL_API int GetRemotes (SOCKET serv,int16_t offset,REMOTEBUFFER *rem)
{
	NetworkCommand (serv,COMMAND_GETREMOTES,"","",0,offset,(NETWORKSTATUS *)rem);

	if (rem->statustype != STATUS_REMOTELIST) return (1);

	return (0);
}

IRTRANSDLL_API int GetCommands (SOCKET serv,char rem[],int16_t offset,COMMANDBUFFER *com)
{
	NetworkCommand (serv,COMMAND_GETCOMMANDS,rem,"",0,offset,(NETWORKSTATUS *)com);

	if (com->statustype != STATUS_COMMANDLIST) return (1);

	return (0);
}


IRTRANSDLL_API NETWORKSTATUS *LearnRepeatIRCode (SOCKET serv, char rem[],char com[],unsigned short timeout)
{
	char st[5];

	NETWORKSTATUS stat;

	st[0] = 0;

	if (NetworkCommand (serv,COMMAND_LRNREM,rem,st,0,'L',&globalstat)) return (&globalstat);

	NetworkCommand (serv,COMMAND_LRNRPT,rem,com,timeout,'*',&globalstat);

	NetworkCommand (serv,COMMAND_CLOSE,rem,st,0,'L',&stat);

	return (&globalstat);
}


IRTRANSDLL_API void DisconnectIRTransServer (SOCKET serv)
{
	closesocket (serv);
}

IRTRANSDLL_API int ConnectIRTransServer (char host[],SOCKET *sock)
{
	char arr[2];
	short *pnt;
	int err;
	unsigned long id = 0;
	struct sockaddr_in serv_addr;
	unsigned long adr;
	struct hostent *he;
	struct in_addr addr;

#ifdef WIN32

	WORD	wVersionRequired;
    WSADATA	wsaData;

	if (!WsockInit) {
	    wVersionRequired = MAKEWORD(2,2);
		err = WSAStartup(wVersionRequired, &wsaData);
		if (err != 0) return (1);
		WsockInit = 1;
	}

#endif

	pnt = (short *)arr;
	*pnt = 1;
	byteorder = arr[1];

	adr = inet_addr (host);
	if (adr == INADDR_NONE) {
		he = (struct hostent *)gethostbyname (host);
		if (he == NULL) return (ERR_FINDHOST);
		memcpy(&addr, he->h_addr_list[0], sizeof(struct in_addr));
		adr = addr.s_addr;
	}

	*sock = socket (PF_INET,SOCK_STREAM,0);
	if (*sock < 0) return (ERR_OPENSOCKET);

	memset (&serv_addr,0,sizeof (serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = adr;
	serv_addr.sin_port = htons (TCP_PORT);

	if (connect (*sock,(struct sockaddr *)&serv_addr,sizeof (serv_addr)) < 0) return (ERR_CONNECT);

	send (*sock,(char *)&id,4,0);

	return (0);
}


static int NetworkCommand (SOCKET serv, int netcommand,char remote[],char command[],word timeout,int adr,NETWORKSTATUS *stat)
{
	LCDCOMMAND lcd;
	NETWORKCOMMAND *com;

	memset (&lcd,0,sizeof (lcd));

	com = (NETWORKCOMMAND *)&lcd;
	com->adress = adr;
	com->netcommand = netcommand;

	if (netcommand == COMMAND_LCD || netcommand == COMMAND_LCDINIT)
	{
		strcpy (lcd.framebuffer,remote);
		lcd.hgt = 4;
		lcd.wid = 40;
		lcd.lcdcommand = LCD_TEXT;
	}
	else
	{
		strcpy (com->remote,remote);
		if (command) strcpy (com->command,command);
		com->timeout = timeout;
	}

	return (SendCommand (serv,com,stat));
}


static int SendCommand (SOCKET serv, NETWORKCOMMAND *com,NETWORKSTATUS *stat)
{
	int res,size;
	STATUSBUFFER buf;

	com->protocol_version = 210;
	if (com->netcommand == COMMAND_LCD) size = sizeof (LCDCOMMAND);
	else if (com->netcommand == COMMAND_SENDCCF) size = sizeof (CCFCOMMAND);
	else if (com->netcommand == COMMAND_SENDCCFLONG) size = sizeof (LONGCCFCOMMAND);
	else size = sizeof (NETWORKCOMMAND);

	SwapCommandBuffer (com);

	memset (stat,0,sizeof (NETWORKSTATUS));

	res = send (serv,(char *)com,size,0);

	if (res != size)
	{
		closesocket (serv);

		stat->statustype = STATUS_MESSAGE;
		stat->statuslevel = FATAL;
		strcpy (stat->message,"Lost connection to IRTrans server");
		SwapStatusBuffer ((NETWORKSTATUS *)stat);

		return (1);
	}

	memset (&buf,0,sizeof (STATUSBUFFER));
	memset (stat,0,sizeof (NETWORKSTATUS));

	do {
		res = recv (serv,(char *)&buf,sizeof (STATUSBUFFER),0);
		if (res && buf.statustype == STATUS_MESSAGE) memcpy (stat,&buf,res);

		if (res && buf.statustype == STATUS_REMOTELIST) memcpy (stat,&buf,res);
		if (res && buf.statustype == STATUS_COMMANDLIST) memcpy (stat,&buf,res);
		if (res && buf.statustype == STATUS_DEVICEMODE) memcpy (stat,&buf,res);
		if (res && buf.statustype == STATUS_DEVICEMODEEXN) memcpy (stat,&buf,res);

		if (res == 8) return (0);

	} while (buf.statustype == STATUS_RECEIVE);

	SwapStatusBuffer ((NETWORKSTATUS *)stat);

	return (1);
}


static void SwapStatusBuffer (NETWORKSTATUS *stat)
{
	if (!byteorder) return;

	if (stat->statustype == STATUS_MESSAGE) swap_word (&stat->statuslevel);

	swap_long (&stat->clientid);
	swap_word (&stat->statuslen);
	swap_word (&stat->statustype);
	swap_word (&stat->adress);
	swap_word (&stat->netstatus);

	if (stat->statustype == STATUS_REMOTELIST || stat->statustype == STATUS_COMMANDLIST) {
		swap_word (&stat->statuslevel);
		swap_word ((word *)&stat->align);
	}

}

static void SwapCommandBuffer (NETWORKCOMMAND *com)
{
	if (!byteorder) return;

	swap_long (&com->adress);
	swap_long (&com->protocol_version);

	if (com->netcommand != COMMAND_LCD) swap_word (&com->timeout);
}


static void swap_word (word *pnt)
{
	byte *a,v;

	a = (byte *)pnt;
	v = a[0];
	a[0] = a[1];
	a[1] = v;
}


static void swap_long (int32_t *pnt)
{
	byte *a,v;

	a = (byte *)pnt;
	v = a[0];
	a[0] = a[3];
	a[3] = v;

	v = a[1];
	a[1] = a[2];
	a[2] = v;
}

static char *trim (char st[],int i)
{
	i--;
	while (i && st[i] == ' ') i--;
	st[i+1] = 0;
	return (st);
}

static int FormatCCF (char ccfstr[],word data[],int len)
{
	int i,j;

	i = j = 0;

	while (ccfstr[i]) {
		while (ccfstr[i] == ' ') i++;

		if (j == len && ccfstr[i]) return (1);
		data[j] = (word)strtol (ccfstr+i,NULL,16);

		if (byteorder) swap_word (data + j);

		j++;

		while (ccfstr[i] && ccfstr[i] != ' ') i++;
	}

	return (0);
}


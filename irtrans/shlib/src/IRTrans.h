#ifdef WIN32

#include <winsock2.h>
#include <windows.h>

#endif

#ifdef LINUX

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/un.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <errno.h>
#include <netdb.h>
#include <signal.h>
typedef int SOCKET;
typedef void* WSAEVENT;
#define closesocket close

#endif


#include <stdio.h>

#include "remote.h"
#include "network.h"
#include "errcode.h"


#ifdef WIN32

#ifdef IRTRANSDLL_EXPORTS
#define IRTRANSDLL_API __declspec(dllexport)
#else
#ifdef  __cplusplus
#define IRTRANSDLL_API extern "C" __declspec(dllimport)
#else
#define IRTRANSDLL_API __declspec(dllimport)
#endif
#endif

#else

#define IRTRANSDLL_API

#endif

IRTRANSDLL_API int ConnectIRTransServer (char host[],SOCKET *sock);
IRTRANSDLL_API void DisconnectIRTransServer (SOCKET serv);
IRTRANSDLL_API NETWORKSTATUS *LearnIRCode (SOCKET serv, char rem[],char com[],unsigned short timeout);
IRTRANSDLL_API NETWORKSTATUS *LearnRepeatIRCode (SOCKET serv, char rem[],char com[],unsigned short timeout);
IRTRANSDLL_API NETWORKSTATUS *SendRemoteCommand (SOCKET serv, char rem[],char com[],int mask,int LEDSel,int bus);
IRTRANSDLL_API NETWORKRECV *ReceiveIR (SOCKET serv);
IRTRANSDLL_API NETWORKSTATUS *ReloadIRDatabase (SOCKET serv);
IRTRANSDLL_API int GetDeviceStatus (SOCKET serv, void *result);
IRTRANSDLL_API int GetDeviceStatusExN (SOCKET serv, int offset,void *result);
IRTRANSDLL_API NETWORKSTATUS *SendRemoteCommandEx (SOCKET serv, char rem[],char com[],word mask,byte LEDSel,byte bus);
IRTRANSDLL_API NETWORKSTATUS *SendCCFCommand (SOCKET serv, char ccfstr[],byte repeat,word mask,byte LEDSel,byte bus);
IRTRANSDLL_API NETWORKSTATUS *SendCCFCommandLong (SOCKET serv, char ccfstr[],byte repeat,word mask,byte LEDSel,byte bus);
IRTRANSDLL_API int GetRemotes (SOCKET serv,int16_t offset,REMOTEBUFFER *rem);
IRTRANSDLL_API int GetCommands (SOCKET serv,char rem[],int16_t offset,COMMANDBUFFER *com);



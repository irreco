#include <stdio.h>

#include "IRTrans.h"

SOCKET irt_server;

main ()
{
  int res,i;
//  NETWORKSTATUS *stat;
//  REMOTEBUFFER remb;
//  COMMANDBUFFER comb;
  NETWORKMODEEXN status;


  res = ConnectIRTransServer ("localhost",&irt_server);

  if (res) {
    printf ("Connect Result: %d\n",res);
    return -1;
  }

  res = GetDeviceStatusExN (irt_server,0,&status);
  printf ("RES: %d\n",res);


  printf ("Count: %d\n",status.count);
  if (status.count > 8) status.count = 8;

  for (i=0;i < status.count;i++) {
	if (!memcmp (status.stat[i][0].remote2,"@@@~~~lan~~~@@@",15) ||
		!memcmp (status.stat[i][0].remote2,"@@@~~~LAN~~~@@@",15)) {

		status.stat[i][0].remote2[33] = 0;
		printf ("[%d]: Mac: %s   IP: %s\n",i,status.stat[i][0].remote2 + 16,status.stat[i][0].remote2 + 34);
	}
  }



/*

  res = GetRemotes (irt_server,0,&remb);
  printf ("RES: %d\n",res);
  printf ("Count: %d [%d]\n",remb.count_total,remb.count_buffer);
  for (res=0;res < remb.count_buffer;res++) printf ("%2d  %s\n",res,remb.remotes[res].name);

  res = GetCommands (irt_server,"irtrans",0,&comb);
  printf ("RES: %d\n",res);
  printf ("Count: %d [%d]\n",comb.count_total,comb.count_buffer);
  for (res=0;res < comb.count_buffer;res++) printf ("%2d  %s\n",res,comb.commands[res]);

 */


//  stat = SendCCFCommandLong (irt_server,"0000 007D 00A4 0000 00A7 0048 000C 003C 000C 0018 000C 0018 000C 0018 000C 003C 000C 0018 000C 0018 000C 0018 000C 0018 000C 003C 000C 0018 000C 003C 000C 003C 000C 0018 000C 003C 000C 003C 000C 003C 000C 003C 000C 003C 000C 0018 000C 0018 000C 003C 000C 0018 000C 0018 000C 0018 000C 0018 000C 0018 000C 0018 000C 003C 000C 003C 000C 003C 000C 003C 000C 003C 000C 0018 000C 003C 000C 003C 000C 0018 000C 0018 000C 0018 000C 0018 000C 0018 000C 0018 000C 0018 000C 0018 000C 0018 000C 0018 000C 0018 000C 0018 000C 003C 000C 003C 000C 003C 000C 003C 000C 0018 000C 0018 000C 0018 000C 0018 000D 03D4 00A7 0048 000C 003C 000C 0018 000D 0017 000C 0018 000C 003C 000C 0018 000C 0018 000C 0018 000C 0018 000C 003C 000C 0018 000C 003C 000C 003C 000C 0018 000C 003C 000C 003C 000C 003C 000C 003C 000C 003C 000D 0017 000C 0018 000C 003C 000C 0018 000C 0018 000C 0018 000C 0018 000C 0018 000C 0018 000C 0018 000C 0018 000C 0018 000C 0018 000C 003C 000C 003C 000C 0018 000C 0018 000C 003C 000C 0018 000C 003C 000D 003B 000C 003C 000C 0018 000C 0018 000C 0018 000C 0018 000C 003C 000C 003C 000C 0018 000D 0017 000C 0018 000C 0018 000C 0018 000C 0018 000C 0018 000C 0018 000C 0018 000C 0018 000C 0018 000C 0018 000C 0018 000C 0018 000C 0018 000C 0018 000C 0018 000C 0018 000D 0017 000C 0018 000C 0018 000C 0018 000C 0018 000C 0018 000C 0018 000C 0018 000C 0018 000C 0018 000C 0018 000C 0018 000C 003C 000D 0017 000C 0018 000C 003C 000C 003C 000C 0018 000C 0018 000C 0018 000C 0018 000C 0018 000C 0018 000C 0018 000C 0018 000D 0017 000C 003C 000C 0018 000C 0018 000C 0018 000C 0018 000C 003C 000C 0018 000C 0018 000C 0018 000C 003C 000C 003C 000C 003C 000C 0018 000C 0000",0,0,0,0);
  //stat = SendRemoteCommandEx (irt_server,"tcom","vol+",0,'i',0);
//  if (stat) {
//    printf ("Error: %s\n",stat->message);
//  }

  DisconnectIRTransServer (irt_server);

  return 0;
}

#!/bin/bash

create_irreco_database_main()
{
	COMMAND="$1"
	ARGS=("$@")
	case "$COMMAND" in
		--create-db)	create_irreco_database_create "${ARGS[@]:1}";;
		--set-perms)	create_irreco_database_set_perms "${ARGS[@]:1}";;
		--help|help|-h)	create_irreco_database_usage;;
		*)		echo "Error: Unknown command \"$COMMAND\"";
				create_irreco_database_usage;
				exit 1;;
	esac
}

create_irreco_database_usage()
{
	echo ""
	echo "Usage: $SCRIPT_NAME COMMAND"
	echo ""
	echo "Commands:"
	echo "    --create-db"
	echo "        Creates Irreco Database if it does not exists already."
	echo "    --set-perms"
	echo "        Create user which can to modify Irreco Database."
	echo ""
}

create_irreco_database_get_root()
{
	echo 
	echo "Give user which has permissions to create new databases. "
	echo "That is probably the MySQL root user."
	echo -n "Username (root): "
	read MYSQL_USERNAME
	
	if [[ "$MYSQL_USERNAME" == "" ]]; then
		MYSQL_USERNAME='root'
	fi
	
	echo -n "Password: "
	read -s MYSQL_PASSWORD
	echo 
	echo
}

create_irreco_database_create()
{
	create_irreco_database_get_root
	echo "Creating database ..."
	mysql --user="$MYSQL_USERNAME" --password="$MYSQL_PASSWORD" --batch \
		< irreco-database.sql
	if [[ "$?" == "0" ]]; then
		echo "... Done"
	fi
}

create_irreco_database_set_perms()
{
	create_irreco_database_get_root
	echo "Give password for irreco user."
	echo -n "Password: "
	read IRRECO_PASSWORD
	echo
	echo "Creating irreco user ..."
	create_irreco_database_set_perms_sql | \
	mysql --user="$MYSQL_USERNAME" --password="$MYSQL_PASSWORD" --batch	
}

create_irreco_database_set_perms_sql()
{
	IRRECO_PERMS='INSERT,DELETE,UPDATE,SELECT,LOCK TABLES'
	
	echo 'grant '"$IRRECO_PERMS"' on `irreco`.* to irreco@localhost;'
	echo 'grant '"$IRRECO_PERMS"' on `irreco`.* to irreco@127.0.0.1;'
	echo "set password for irreco@localhost = password('$IRRECO_PASSWORD');"
	echo "set password for irreco@127.0.0.1 = password('$IRRECO_PASSWORD');"
}

create_irreco_database_main "$@"















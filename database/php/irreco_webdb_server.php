<?php

/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007,2008  Arto Karppinen (arto.karppinen@iki.fi),
 * Joni Kokko (t5kojo01@students.oamk.fi),
 * Sami Mäki (kasmra@xob.kapsi.fi),
 * Harri Vattulainen (t5vaha01@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

require_once 'irreco_webdb_log.php';
require_once 'XML/RPC2/Server.php';

class IrrecoWebdbServer
{

	const ADD_USER_ERROR_MSG = "Adding user failed.";
	const UPLOAD_CONFIGURATION_ERROR_MSG = "Uploading configuration\nfailed.";
	const USERNAME_AND_PASSWORD_MISMATCH_MSG = "Username and password mismatch.";
	const FILE_ALREADY_EXIST_MSG = "Database already contains identical device.";
	const HASH_AND_DATA_MISMATCH_MSG = "File hash and file data don't match.";
	const FAULTCODE_ERROR_MSG = "\nInvalid string faultCode.";
        const LOGIN_ERROR_MSG = "Login failed.";
	const EMPTY_DB_MSG = "Database is empty.";
	const NO_THEMES_OF_REMOTE_MSG = "Can't find any theme of remote.";
	const NO_CONFIGURATIONS_OF_REMOTE_MSG = "Can't find any configuration of remote.";

	const ADD_USER_ERROR = 10001;
	const UPLOAD_CONFIGURATION_ERROR = 10002;
	const USERNAME_AND_PASSWORD_MISMATCH = 10003;
	const FILE_ALREADY_EXIST = 10004;
	const HASH_AND_DATA_MISMATCH = 10005;
	const FAULTCODE_ERROR = 10006;
        const LOGIN_ERROR = 10007;
	const EMPTY_DB_ERROR = 10008;
	const NO_THEMES_OF_REMOTE_ERROR = 10009;
	const NO_CONFIGURATIONS_OF_REMOTE_ERROR = 10010;

	private $database;

	public function __construct($database)
	{
		$this->database = $database;
	}

	/**
	* Sum two numbers.
	*
	* @param int num_a
	* @param int num_b
	* @return int Sum of integers
	*/
	public function sum($num_a, $num_b)
	{
		IrrecoLog::$server->log('XML-RPC sum');
		return $num_a + $num_b;
	}

	/**
	* Add user to database
	*
	* Returns TRUE if user is added successfully to database.
	*
	* @param string name Username
	* @param string email full email address
	* @param string passwd sha1-hash of password
	* @return boolean User added successfully
	*/
	function addUser($name, $email, $passwd)
	{
		IrrecoLog::$server->log('XML-RPC addUser');
		$table = 'user';

		/* Check for faultCode */
		if(strstr($name, "faultCode")) {
			throw new XML_RPC2_FaultException(self::FAULTCODE_ERROR_MSG,
							  self::FAULTCODE_ERROR);
		}
		if(strstr($email, "faultCode")) {
			throw new XML_RPC2_FaultException(self::FAULTCODE_ERROR_MSG,
							  self::FAULTCODE_ERROR);
		}

		/* Check parameters here */
		if(strlen($name) < 6) {
			throw new XML_RPC2_FaultException(self::ADD_USER_ERROR_MSG,
							  self::ADD_USER_ERROR);
		}

		if(strlen($email) < 6) {
			throw new XML_RPC2_FaultException(self::ADD_USER_ERROR_MSG,
							  self::ADD_USER_ERROR);
		}

		if(strlen($passwd) < 6) {
			throw new XML_RPC2_FaultException(self::ADD_USER_ERROR_MSG,
							  self::ADD_USER_ERROR);
		}

 		if(!strstr($email, "@")) {
			throw new XML_RPC2_FaultException(self::ADD_USER_ERROR_MSG,
							  self::ADD_USER_ERROR);
		}

		if(!strstr($email, ".")) {
			throw new XML_RPC2_FaultException(self::ADD_USER_ERROR_MSG,
							  self::ADD_USER_ERROR);
		}

		$data = $this->database->addUser($name, $email, $passwd, $table);

		if ($data == FALSE) {
			/* if failed */
			throw new XML_RPC2_FaultException(self::ADD_USER_ERROR_MSG,
							  self::ADD_USER_ERROR);
		}
		return $data;
	}

	/**
	* Add configuration to database
	*
	* Returns error message or 'configuration added successfully' as string
	*
	*
	* @param string backend Name for Backend. e.g. "IRTrans Transceiver"
	* @param string category Name for Category. e.g. "Tv"
	* @param string manufacturer Name for Manufacturer. e.g. "Sony"
	* @param string model Name for Model. e.g. "xyz"
	* @param string user Username
	* @param string password sha1-hash of password
	* @param string file_hash sha1-hash of file_data
	* @param string file_name Name for File
	* @param string file_data Content of file
	* @return string
	*/
	function uploadConfiguration($backend,
				     $category,
	 			     $manufacturer,
				     $model,
				     $user,
	 			     $password,
				     $file_hash,
				     $file_name,
				     $file_data)
	{
		IrrecoLog::$server->log('XML-RPC uploadConfiguration');

		/* Check for faultCode */
		if(strstr($backend, "faultCode")) {
			throw new XML_RPC2_FaultException(self::FAULTCODE_ERROR_MSG,
							  self::FAULTCODE_ERROR);
		}
		if(strstr($category, "faultCode")) {
			throw new XML_RPC2_FaultException(self::FAULTCODE_ERROR_MSG,
					self::FAULTCODE_ERROR);
		}
		if(strstr($manufacturer, "faultCode")) {
			throw new XML_RPC2_FaultException(self::FAULTCODE_ERROR_MSG,
					self::FAULTCODE_ERROR);
		}
		if(strstr($model, "faultCode")) {
			throw new XML_RPC2_FaultException(self::FAULTCODE_ERROR_MSG,
					self::FAULTCODE_ERROR);
		}
		if(strstr($user, "faultCode")) {
			throw new XML_RPC2_FaultException(self::FAULTCODE_ERROR_MSG,
					self::FAULTCODE_ERROR);
		}
		if(strstr($file_hash, "faultCode")) {
			throw new XML_RPC2_FaultException(self::FAULTCODE_ERROR_MSG,
					self::FAULTCODE_ERROR);
		}
		if(strstr($file_name, "faultCode")) {
			throw new XML_RPC2_FaultException(self::FAULTCODE_ERROR_MSG,
					self::FAULTCODE_ERROR);
		}
		if(strstr($file_data, "faultCode")) {
			throw new XML_RPC2_FaultException(self::FAULTCODE_ERROR_MSG,
					self::FAULTCODE_ERROR);
		}

		/* Check parameters here */
		if(strlen($backend) == 0) {
			throw new XML_RPC2_FaultException(
				self::UPLOAD_CONFIGURATION_ERROR_MSG,
				self::UPLOAD_CONFIGURATION_ERROR);
		}

		if(strlen($category) == 0) {
			throw new XML_RPC2_FaultException(
				self::UPLOAD_CONFIGURATION_ERROR_MSG,
				self::UPLOAD_CONFIGURATION_ERROR);
		}

		if(strlen($manufacturer) == 0) {
			throw new XML_RPC2_FaultException(
				self::UPLOAD_CONFIGURATION_ERROR_MSG,
				self::UPLOAD_CONFIGURATION_ERROR);
		}

		if(strlen($model) == 0) {
			throw new XML_RPC2_FaultException(
				self::UPLOAD_CONFIGURATION_ERROR_MSG,
				self::UPLOAD_CONFIGURATION_ERROR);
		}

		if(strlen($user) < 6) {
			throw new XML_RPC2_FaultException(
				self::UPLOAD_CONFIGURATION_ERROR_MSG,
				self::UPLOAD_CONFIGURATION_ERROR);
		}

		if(strlen($password) < 6) {
			throw new XML_RPC2_FaultException(
				self::UPLOAD_CONFIGURATION_ERROR_MSG,
				self::UPLOAD_CONFIGURATION_ERROR);
		}

		if(strlen($file_hash) != 40) {
			throw new XML_RPC2_FaultException(
				self::UPLOAD_CONFIGURATION_ERROR_MSG,
				self::UPLOAD_CONFIGURATION_ERROR);
		}

		/* try to add data to db */
		$table = 'configuration';
		$return = $this->database->uploadConfiguration($backend,
							       $category,
	 						       $manufacturer,
							       $model,
							       $user,
							       $password,
							       $file_hash,
							       $file_name,
							       $file_data);

		if ($return == 'username and password mismatch.') {
			throw new XML_RPC2_FaultException(
					self::USERNAME_AND_PASSWORD_MISMATCH_MSG,
					self::USERNAME_AND_PASSWORD_MISMATCH);
		}
		else if ($return == 'file already exist.') {
			throw new XML_RPC2_FaultException(
				self::FILE_ALREADY_EXIST_MSG,
				self::FILE_ALREADY_EXIST);
		}
		else if ($return == 'file hash and file data mismatch.') {
			throw new XML_RPC2_FaultException(
				self::HASH_AND_DATA_MISMATCH_MSG,
				self::HASH_AND_DATA_MISMATCH);
		}

		return $return;
	}

	/**
	 * Get list of categories that have configurations in them.
	 *
	 * @return array
	 */
	function getCategories()
	{
		IrrecoLog::$server->log('XML-RPC getCategories');
		$data = $this->database->getCategoryList();

		$array = array();
		foreach ($data as $item) {
			array_push($array, $item->name);
		}

		IrrecoLog::$database->log("Category list:\n" .
					  IrrecoLog::getVarDump($array));
		return $array;
	}
	/**
	 * Get list of all categories. Even those that don't have
	 * configurations in them.
	 *
	 * @return array
	 */
	function getAllCategories()
	{
		IrrecoLog::$server->log('XML-RPC getAllCategories');
		$data = $this->database->getWholeCategoryList();

		$array = array();
		foreach ($data as $item) {
			array_push($array, $item->name);
		}

		IrrecoLog::$database->log("Category list:\n" .
					  IrrecoLog::getVarDump($array));
		return $array;
	}

	/**
	 * Get list of manufacturers for category.
	 *
	 * @param string category Name for Category. e.g. "Tv"
	 * @return array
	 */
	function getManufacturers($category)
	{
		IrrecoLog::$server->log('XML-RPC getManufacturers');
		$data = $this->database->getManufacturerList($category);

		$array = array();
		foreach ($data as $item) {
			array_push($array, $item->name);
		}

		IrrecoLog::$database->log("Manufacturer list:\n" .
					  IrrecoLog::getVarDump($array));
		return $array;
	}

	/**
	 * Get list of all manufacturers. Even those that don't have
	 * configurations in them.
	 *
	 * @return array
	 */
	function getAllManufacturers()
	{
		IrrecoLog::$server->log('XML-RPC getAllManufacturers');
		$data = $this->database->getWholeManufacturerList();

		$array = array();
		foreach ($data as $item) {
			array_push($array, $item->name);
		}

		IrrecoLog::$database->log("Manufacturer list:\n" .
					  IrrecoLog::getVarDump($array));
		return $array;
	}

	/**
	 * Get list of models by manufacturer in selected category.
	 *
	 * @param string category Name for Category. e.g. "Tv"
	 * @param string manufacturer Name for Manufacturer. e.g. "Sony"
	 * @return array
	 */
	function getModels($category,$manufacturer)
	{
		IrrecoLog::$server->log('XML-RPC getModels');
		$data = $this->database->getModelList($category,$manufacturer);

		$array = array();
		foreach ($data as $item) {
			array_push($array, $item->model);
		}

		IrrecoLog::$database->log("Model list:\n" .
					  IrrecoLog::getVarDump($array));
		return $array;
	}


	/**
	 * Get id list for configurations by model.
	 *
	 * This function is deprecated. Use the getConfigurations function
	 * instead.
	 * @param string model Name for Model. e.g. "xyz"
	 * @return array
	 */
	function getConfigs($model)
	{
		IrrecoLog::$server->log('XML-RPC getConfigs');
		$data = $this->database->getConfigs($model);
		$array = array();
		foreach ($data as $item) {
			array_push($array, $item->id);
		}

		IrrecoLog::$database->log("Config id:\n" .
					  IrrecoLog::getVarDump($array));
		return $array;
	}

	/**
	 * Get id list for configurations by model and manufacturer
	 *
	 * @param string manufacturer Name for Manufacturer. e.g. "Sony"
	 * @param string model Name for Model. e.g. "xyz"
	 * @return array
	 */
	function getConfigurations($manufacturer, $model)
	{
		IrrecoLog::$server->log('XML-RPC getConfigurations');
		$data = $this->database->getConfigurations($manufacturer, $model);

		$array = array();
		foreach ($data as $item) {
			array_push($array, $item->id);
		}

		IrrecoLog::$database->log("Config id:\n" .
					  IrrecoLog::getVarDump($array));
		return $array;
	}

	/**
	 * Get all data from configuration
	 *
	 * Returns user, backend, category, manufacturer,
	 * model, file_hash, file_name, uploaded, download_count
	 *
	 * This function is deprecated. Use the getConfigurationById function
	 * instead.
	 * @param string id id-number for configuration.
	 * @return struct
	 */
	function getConfiguration($id)
	{
		IrrecoLog::$server->log('XML-RPC getConfiguration');
		$data = $this->database->getConfiguration($id);
		$array = array();

		$array['user'] = $data[0]->user;
		$array['backend'] = $data[0]->backend;
		$array['category'] = $data[0]->category;
		$array['manufacturer'] = $data[0]->manufacturer;
		$array['model'] = $data[0]->model;
		$array['file_hash'] = $data[0]->file_hash;
		$array['file_name'] = $data[0]->file_name;
		$array['uploaded'] = $data[0]->uploaded;
		$array['download_count'] = $data[0]->download_count;

		IrrecoLog::$database->log("Configuration:\n" .
					  IrrecoLog::getVarDump($array));
		return $array;
	}

	/**
	 * Get all data from configuration
	 *
	 * Returns user, backend, category, manufacturer,
	 * model, file_hash, file_name, uploaded, download_count
	 * @param int id id-number for configuration.
	 * @return struct
	 */
	function getConfigurationById($id)
	{
		IrrecoLog::$server->log('XML-RPC getConfigurationById');
		$data = $this->database->getConfiguration($id);
		$array = array();

		$array['user'] = $data[0]->user;
		$array['backend'] = $data[0]->backend;
		$array['category'] = $data[0]->category;
		$array['manufacturer'] = $data[0]->manufacturer;
		$array['model'] = $data[0]->model;
		$array['file_hash'] = $data[0]->file_hash;
		$array['file_name'] = $data[0]->file_name;
		$array['uploaded'] = $data[0]->uploaded;
		$array['download_count'] = $data[0]->download_count;

		IrrecoLog::$database->log("Configuration:\n" .
					  IrrecoLog::getVarDump($array));
		return $array;
	}

	/**
	 * Get configuration_id by name and date.
	 *
	 * Returns configuration_id or 0 if configuration does not exists.
	 * @param string file_hash sha1-hash of file_data
	 * @param string file_name Name for File
	 * @return int
	 */
	function getConfigIdByFilehashAndFilename($file_hash, $file_name)
	{
		IrrecoLog::$server->log(
				'XML-RPC getConfigIdByFilehashAndFilename');
		$id = $this->database->getConfigIdByFilehashAndFilename(
							$file_hash, $file_name);

		IrrecoLog::$server->log("Configuration_id: ". $id);
		return $id;
	}

	/**
	 * Get File
	 *
	 *
	 * Returns content of file
	 * @param string hash sha1-hash of file_data
	 * @param string name Name for File
	 * @return struct
	 */
	function getFile($hash,$name)
	{
		IrrecoLog::$server->log('XML-RPC getFile');
		$data = $this->database->getFileData($hash,$name);

		$array = array();

		$array['data']= $data[0]->data;

		IrrecoLog::$database->log("Data:\n" .
					  IrrecoLog::getVarDump($array));
		return $array;
	}


	/**
	* Get whether user already exists
	*
	* Returns TRUE if given name is already in database.
	* Used at irreco_webdb_register_dlg when adding new user.
	*
	* @param string name Username
	* @return boolean User exists
	*/
	function getUserExists($name)
	{
		IrrecoLog::$server->log('XML-RPC getUserExists');
		$table = 'user';
		$data = $this->database->getNameId($table, $name);

		if ($data == "") {
			return FALSE;
		} else {
			return TRUE;
		}
	}

	/**
	 * Create new Theme
	 *
	 * Returns theme_id
	 * @param string name Name of Theme
	 * @param string comment
	 * @param string preview_button name of preview button
	 * @param string folder name of themefolder
	 * @param string user Username
	 * @param string password sha1-hash of password
	 * @return int
	 */
	function createNewTheme($name, $comment, $preview_button,
				$folder, $user, $password)
	{
		IrrecoLog::$server->log('XML-RPC createNewTheme');
		$theme_id = $this->database->createNewTheme($name, $comment,
							    $preview_button,
							    $folder, $user,
							    $password);

		/* TODO when catching of exeptions is correctly handled */
		/* it might be better to throw exeption, until then.. */
		/*if ($theme_id == 'username and password mismatch.') {
			throw new XML_RPC2_FaultException(
				self::USERNAME_AND_PASSWORD_MISMATCH_MSG,
				self::USERNAME_AND_PASSWORD_MISMATCH);
		}*/

		if ($theme_id == -1) {
			IrrecoLog::$server->log('Username & password mismatch');
		} elseif ($theme_id == -2) {
			IrrecoLog::$server->log('No rights to themename');
		}

		IrrecoLog::$database->log("Theme_id: " .  $theme_id . "\n");
		return $theme_id;
	}

	/**
	 * Set Theme downloadable
	 *
	 * @param int id id-number for theme.
	 * @param bool downloadable
	 * @param string user Username
	 * @param string password sha1-hash of password
	 * @return bool
	 */
	function setThemeDownloadable($id, $downloadable, $user, $password)
	{
		IrrecoLog::$server->log('XML-RPC ThemeSetDownloadable');
		$success = $this->database->setThemeDownloadable($id,
								 $downloadable,
								 $user,
								 $password);

		if ($success == 'username and password mismatch.') {
			throw new XML_RPC2_FaultException(
				self::USERNAME_AND_PASSWORD_MISMATCH_MSG,
				self::USERNAME_AND_PASSWORD_MISMATCH);
		}
		return TRUE;
	}

	/**
	 * Get id list for themes
	 *
	 * @return array
	 */
	function getThemes()
	{
		IrrecoLog::$server->log('XML-RPC getThemes');
		$data = $this->database->getThemeList();

		if ($data == 'Database is empty.') {
			throw new XML_RPC2_FaultException(self::EMPTY_DB_MSG,
							  self::EMPTY_DB_ERROR);
		}

		$array = array();
		foreach ($data as $item) {
			array_push($array, $item->id);
		}

		IrrecoLog::$database->log("Theme id:\n" .
					  IrrecoLog::getVarDump($array));
		return $array;
	}

	/**
	 * Get all data from theme
	 *
	 * Returns name, user, comment, preview_button, folder,
	 * uploaded, modified, downloaded, download_count
	 * @param int id id-number for theme.
	 * @return struct
	 */

	function getThemeById($id)
	{
		IrrecoLog::$server->log('XML-RPC getThemeById');
		$data = $this->database->getTheme($id);
		$array = array();

		$array['name'] = $data[0]->name;
		$array['user'] = $data[0]->user;
		$array['comment'] = $data[0]->comment;
		$array['preview_button'] = $data[0]->preview_button;
		$array['folder'] = $data[0]->folder;
		$array['uploaded'] = $data[0]->uploaded;
		$array['modified'] = $data[0]->modified;
		$array['downloaded'] = $data[0]->downloaded;
		$array['download_count'] = intval($data[0]->download_count);

		IrrecoLog::$database->log("Theme:\n" .
					  IrrecoLog::getVarDump($array));
		return $array;
	}

	/**
	 * Get theme versions by name
	 *
	 * Returns id-list
	 * @param string name name for theme.
	 * @return array
	 */

	function getThemeVersionsByName($name)
	{
		IrrecoLog::$server->log('XML-RPC getThemeVersionsByName');
		$data = $this->database->getThemeVersionsByName($name);

		$array = array();
		foreach ($data as $item) {
			array_push($array, $item->id);
		}

		IrrecoLog::$database->log("Theme versions:\n".
					  IrrecoLog::getVarDump($array));
		return $array;
	}

	/**
	 * Get date for theme by id
	 *
	 * @param int id id-number for theme.
	 * @return string
	 */

	function getThemeDateById($id)
	{
		IrrecoLog::$server->log('XML-RPC getThemeById');
		$data = $this->database->getTheme($id);

		IrrecoLog::$database->log("Theme-date: ".$data[0]->uploaded);
		return $data[0]->uploaded;
	}

	/**
	 * Get theme_id by name and date.
	 *
	 * Returns theme_id or 0 if theme does not exists.
	 * @param string name Name for theme.
	 * @param string date theme creation date.
	 * @return int
	 */
	function getThemeIdByNameAndDate($name, $date)
	{
		IrrecoLog::$server->log('XML-RPC getThemeIdByNameAndDate');
		$id = $this->database->getThemeIdByNameAndDate($name, $date);

		IrrecoLog::$server->log("Theme_id: ". $id);
		return $id;
	}

	/**
	 * Add button to Theme
	 *
	 * Returns button_id
	 * @param string name Name of button
	 * @param bool allow_text
	 * @param string text_format_up
	 * @param string text_format_down
	 * @param int text_padding
	 * @param float text_h_align
	 * @param float text_v_align
	 * @param string image_up_hash
	 * @param string image_up_name
	 * @param string image_up
	 * @param string image_down_hash
	 * @param string image_down_name
	 * @param string image_down
	 * @param string folder
	 * @param int theme_id
	 * @param string user Username
	 * @param string password sha1-hash of password
	 * @return int
	 */
	function addButtonToTheme($name, $allow_text, $text_format_up,
				$text_format_down, $text_padding,
				$text_h_align, $text_v_align,
				$image_up_hash, $image_up_name, $image_up,
				$image_down_hash, $image_down_name, $image_down,
    				$folder, $theme_id, $user, $password)
	{

		IrrecoLog::$server->log('XML-RPC addButtonToTheme');

		$button_id = $this->database->addButtonToTheme(
				$name, $allow_text, $text_format_up,
				$text_format_down, $text_padding,
				$text_h_align, $text_v_align,
				$image_up_hash, $image_up_name,
				base64_decode($image_up), $image_down_hash,
				$image_down_name, base64_decode($image_down),
				$folder, $theme_id, $user, $password);

		if ($button_id == 'username and password mismatch.') {
			throw new XML_RPC2_FaultException(
				self::USERNAME_AND_PASSWORD_MISMATCH_MSG,
				self::USERNAME_AND_PASSWORD_MISMATCH);
		}
		else if ($button_id == 'file hash and file data mismatch.') {
			throw new XML_RPC2_FaultException(
				self::HASH_AND_DATA_MISMATCH_MSG,
				self::HASH_AND_DATA_MISMATCH);
		}

		IrrecoLog::$database->log("Button_id: " .  $button_id . "\n");
		return $button_id;
	}

	/**
	 * Get id list of buttons for theme
	 *
	 * @param int theme_id id of theme
	 * @return array
	 */
	function getButtons($theme_id)
	{
		IrrecoLog::$server->log('XML-RPC getButtons');
		$data = $this->database->getButtonList($theme_id);

		$array = array();
		foreach ($data as $item) {
			array_push($array, intval($item->id));
		}

		if (count($array) == 0) {
			array_push($array, 0);
		}

		IrrecoLog::$database->log("Button id:\n".
					  IrrecoLog::getVarDump($array));
		return $array;
	}

	/**
	 * Get all data from Button
	 *
	 * Returns name, allow_text, text_format_up, text_format_down,
	 * text_padding, image_up_hash, image_up_name, base64 encoded image_up,
	 * image_down_hash, image_down_name, base64 encoded image_down, folder
	 * @param int id id-number for button.
	 * @return struct
	 */
	function getButtonById($id)
	{
		IrrecoLog::$server->log('XML-RPC getButtonById');
		$array = $this->database->getButton($id);

		IrrecoLog::$database->log("Button:\n".
					$array['name'].", ".
					$array['allow_text'].", ".
					$array['text_format_up'].", ".
					$array['text_format_down'].", ".
					$array['text_padding'].", ".
					$array['text_h_align'].", ".
					$array['text_v_align'].", ".
					$array['image_up_hash'].", ".
					$array['image_up_name'].", ".
					"base64_image_data, ".
					$array['image_down_hash'].", ".
					$array['image_down_name'].", ".
					"base64_image_data, ".
					$array['folder']);

		return $array;
	}

	/**
	 * Get preview button
	 *
	 * Returns base64 encoded image-data string.
	 * @param int theme_id id for theme
	 * @return string
	 */

	function getPreviewButton($theme_id)
	{
		IrrecoLog::$server->log('XML-RPC getPreviewButton');
		$data = $this->database->getPreviewButton($theme_id);
		return base64_encode($data);
	}

	/**
	 * Add background to Theme
	 *
	 * Returns background_id
	 * @param string name Name of background
	 * @param string image_hash sha1-hash of image-data
	 * @param string image_name Name for image
	 * @param string image Content of image
	 * @param string folder name of button-folder
	 * @param int theme_id id of theme
	 * @param string user Username
	 * @param string password sha1-hash of password
	 * @return int
	 */
	function addBgToTheme($name, $image_hash, $image_name, $image,
			      $folder, $theme_id, $user, $password)
	{
		IrrecoLog::$server->log('XML-RPC addBgToTheme');
		$bg_id = $this->database->addBgToTheme($name, $image_hash,
						       $image_name,
						       base64_decode($image),
						       $folder, $theme_id,
						       $user, $password);

		if ($bg_id == 'username and password mismatch.') {
			throw new XML_RPC2_FaultException(
				self::USERNAME_AND_PASSWORD_MISMATCH_MSG,
				self::USERNAME_AND_PASSWORD_MISMATCH);
		}
		else if ($bg_id == 'file hash and file data mismatch.') {
			throw new XML_RPC2_FaultException(
				self::HASH_AND_DATA_MISMATCH_MSG,
				self::HASH_AND_DATA_MISMATCH);
		}

		IrrecoLog::$database->log("Bg_id: " .  $bg_id . "\n");
		return $bg_id;
	}

	/**
	 * Get id list of backgrounds for theme
	 *
	 * @param int theme_id id of theme
	 * @return array
	 */
	function getBackgrounds($theme_id)
	{
		IrrecoLog::$server->log('XML-RPC getBackgrounds');
		$data = $this->database->getBgList($theme_id);

		$array = array();
		foreach ($data as $item) {
			array_push($array, intval($item->id));
		}

		if (count($array) == 0) {
			array_push($array, 0);
		}

		IrrecoLog::$database->log("Bg id:\n" .
					  IrrecoLog::getVarDump($array));
		return $array;
	}

	/**
	 * Get all data from Background
	 *
	 * Returns name, image_hash, image_name,
	 * folder and base64 encoded image_data
	 * @param int id id-number for background.
	 * @return struct
	 */
	function getBgById($id)
	{
		IrrecoLog::$server->log('XML-RPC getBgById');
		$array = $this->database->getBg($id);

		IrrecoLog::$database->log("Bg: ".
					  $array['name'].", ".
					  $array['image_hash'].", ".
					  $array['image_name'].", ".
					  "base64_image_data, ".
					  $array['folder']);


		return $array;
	}

	/**
	 * Login to database
	 *
	 * Returns TRUE if login successful, FALSE otherwise.
	 *
	 * @param string user Username
	 * @param string password sha1-hash of password
	 * @return boolean
	 */
	function loginToDB($user, $password)
	{
		IrrecoLog::$server->log('XML-RPC loginToDB');

		/* Check for faultCode */
		if(strstr($user, "faultCode")) {
			throw new XML_RPC2_FaultException(self::FAULTCODE_ERROR_MSG,
					self::FAULTCODE_ERROR);
		}

		/* Check parameters here */
		if(strlen($user) < 6) {
			throw new XML_RPC2_FaultException(
				self::LOGIN_ERROR_MSG,
				self::LOGIN_ERROR);
		}

		if(strlen($password) < 6) {
			throw new XML_RPC2_FaultException(
				self::LOGIN_ERROR_MSG,
				self::LOGIN_ERROR);
		}

		$return = $this->database->loginToDB($user, $password);

		if ($return == FALSE) {
			throw new XML_RPC2_FaultException(
					self::USERNAME_AND_PASSWORD_MISMATCH_MSG,
					self::USERNAME_AND_PASSWORD_MISMATCH);
		}

		return $return;
	}

	/**
	 * Get max image-size in bytes
	 *
	 * Returns max image-size
	 * @return int
	 */
	function getMaxImageSize()
	{
		IrrecoLog::$server->log('XML-RPC getMaxImageSize');
		$val = trim(ini_get('post_max_size'));
		$last = strtolower($val[strlen($val)-1]);

		switch($last) {
		// The 'G' modifier is available since PHP 5.1.0
		case 'g':
			$val *= 1024;
		case 'm':
			$val *= 1024;
		case 'k':
			$val *= 1024;
		}

		$val = $val / 2 - (100 * 1024); //divided by 2 because of base64

		IrrecoLog::$server->log("Max image-size: " .  $val . "\n");
		return $val;
	}


	/**
	 * Create new Remote
	 *
	 * Returns remote_id
	 * @param string comment
	 * @param string category Name for Category. e.g. "Tv"
	 * @param string manufacturer Name for Manufacturer. e.g. "Sony"
	 * @param string model Name for Model. e.g. "xyz"
	 * @param string file_hash sha1-hash of file_data
	 * @param string file_name Name for File
	 * @param string file_data Content of file
	 * @param string user Username
	 * @param string password sha1-hash of password
	 * @return int
	 */
	function createNewRemote($comment, $category, $manufacturer,
				 $model, $file_hash, $file_name, $file_data,
				 $user, $password)
	{
		IrrecoLog::$server->log('XML-RPC createNewRemote');
		$remote_id = $this->database->createNewRemote($comment,
				$category, $manufacturer, $model, $file_hash,
				$file_name, $file_data, $user, $password);

		if ($remote_id == 'username and password mismatch.') {
			throw new XML_RPC2_FaultException(
				self::USERNAME_AND_PASSWORD_MISMATCH_MSG,
				self::USERNAME_AND_PASSWORD_MISMATCH);
		}
		else if ($remote_id == 'file hash and file data mismatch.') {
			throw new XML_RPC2_FaultException(
				self::HASH_AND_DATA_MISMATCH_MSG,
				self::HASH_AND_DATA_MISMATCH);
		}

		IrrecoLog::$database->log("Remote_id: " .  $remote_id . "\n");
		return $remote_id;
	}

	/**
	 * Set Remote downloadable
	 *
	 * @param int id id-number for remote.
	 * @param bool downloadable
	 * @param string user Username
	 * @param string password sha1-hash of password
	 * @return bool
	 */
	function setRemoteDownloadable($id, $downloadable, $user, $password)
	{
		IrrecoLog::$server->log('XML-RPC RemoteSetDownloadable');
		$success = $this->database->setRemoteDownloadable($id,
								  $downloadable,
								  $user,
								  $password);

		if ($success == 'username and password mismatch.') {
			throw new XML_RPC2_FaultException(
				self::USERNAME_AND_PASSWORD_MISMATCH_MSG,
				self::USERNAME_AND_PASSWORD_MISMATCH);
		}
		return TRUE;
	}


	/**
	 * Add Configuration to Remote
	 *
	 * @param int remote_id id-number for remote.
	 * @param int configuration_id id-number for configuration.
	 * @param string user Username
	 * @param string password sha1-hash of password
	 * @return bool
	 */
	function addConfigurationToRemote($remote_id, $configuration_id,
					  $user, $password)
	{
		IrrecoLog::$server->log('XML-RPC addConfigurationToRemote');
		$success = $this->database->addConfigurationToRemote($remote_id,
							$configuration_id,
							$user,
							$password);

		if ($success == 'username and password mismatch.') {
			throw new XML_RPC2_FaultException(
				self::USERNAME_AND_PASSWORD_MISMATCH_MSG,
				self::USERNAME_AND_PASSWORD_MISMATCH);
		}
		return TRUE;
	}

	/**
	 * Add Theme to Remote
	 *
	 * @param int remote_id id-number for remote.
	 * @param int theme_id id-number for theme.
	 * @param string user Username
	 * @param string password sha1-hash of password
	 * @return bool
	 */
	function addThemeToRemote($remote_id, $theme_id, $user, $password)
	{
		IrrecoLog::$server->log('XML-RPC addThemeToRemote');
		$success = $this->database->addThemeToRemote($remote_id,
							     $theme_id,
							     $user,
							     $password);

		if ($success == 'username and password mismatch.') {
			throw new XML_RPC2_FaultException(
				self::USERNAME_AND_PASSWORD_MISMATCH_MSG,
				self::USERNAME_AND_PASSWORD_MISMATCH);
		}
		return TRUE;
	}

	/**
	 * Get list of categories that have remotes in them.
	 *
	 * @return array
	 */
	function getRemoteCategories()
	{
		IrrecoLog::$server->log('XML-RPC getRemoteCategories');
		$data = $this->database->getRemoteCategories();

		$array = array();
		foreach ($data as $item) {
			array_push($array, $item->name);
		}

		IrrecoLog::$database->log("Category list:\n" .
					  IrrecoLog::getVarDump($array));
		return $array;
	}

	/**
	 * Get list of manufacturers for "remote category".
	 *
	 * @param string category Name for Category. e.g. "Tv"
	 * @return array
	 */
	function getRemoteManufacturers($category)
	{
		IrrecoLog::$server->log('XML-RPC getRemoteManufacturers');
		$data = $this->database->getRemoteManufacturers($category);

		$array = array();
		foreach ($data as $item) {
			array_push($array, $item->name);
		}

		IrrecoLog::$database->log("Manufacturer list:\n" .
					  IrrecoLog::getVarDump($array));
		return $array;
	}

	/**
	 * Get list of models by manufacturer in selected category.
	 *
	 * @param string category Name for Category. e.g. "Tv"
	 * @param string manufacturer Name for Manufacturer. e.g. "Sony"
	 * @return array
	 */
	function getRemoteModels($category,$manufacturer)
	{
		IrrecoLog::$server->log('XML-RPC getRemoteModels');
		$data = $this->database->getRemoteModels($category,
							 $manufacturer);

		$array = array();
		foreach ($data as $item) {
			array_push($array, $item->model);
		}

		IrrecoLog::$database->log("Model list:\n" .
					  IrrecoLog::getVarDump($array));
		return $array;
	}

	/**
	 * Get Creators for model.
	 *
	 * @param string category Name for Category. e.g. "Tv"
	 * @param string manufacturer Name for Manufacturer. e.g. "Sony"
	 * @param string model Name for Model. e.g. "xyz"
	 * @return array
	 */
	function getRemoteCreators($category, $manufacturer, $model)
	{
		IrrecoLog::$server->log('XML-RPC getRemoteCreators');
		$data = $this->database->getRemoteCreators($category,
							   $manufacturer,
							   $model);

		$array = array();
		foreach ($data as $item) {
			array_push($array, $item->user);
		}

		IrrecoLog::$database->log("Creator list:\n" .
					  IrrecoLog::getVarDump($array));
		return $array;
	}

	/**
	 * Get Remotes
	 *
	 * Returns list of remote_id
	 * @param string category Name for Category. e.g. "Tv"
	 * @param string manufacturer Name for Manufacturer. e.g. "Sony"
	 * @param string model Name for Model. e.g. "xyz"
	 * @param string user Username of Remote creator
	 * @return array
	 */
	function getRemotes($category, $manufacturer, $model, $user)
	{
		IrrecoLog::$server->log('XML-RPC getRemotes');
		$data = $this->database->getRemotes($category, $manufacturer,
						    $model, $user);

		$array = array();
		foreach ($data as $item) {
			array_push($array, intval($item->id));
		}

		IrrecoLog::$server->log("Remotes:\n" .
					IrrecoLog::getVarDump($array));
		return $array;
	}

	/**
	 * Get all data from remote
	 *
	 * Returns name, user, comment,  category, manufacturer,
	 * model, file_hash, file_name, uploaded, modified, downloaded,
	 * and download_count
	 * @param int id id-number for remote.
	 * @return struct
	 */
	function getRemoteById($id)
	{
		IrrecoLog::$server->log('XML-RPC getRemoteById');
		$array = $this->database->getRemoteByid($id);

		IrrecoLog::$server->log("Remote:\n" .
					 IrrecoLog::getVarDump($array));
		return $array;
	}

	/**
	 * Get themes of remote
	 *
	 * @param int remote_id id-number for remote.
	 * @return array
	 */
	function getThemesOfRemote($remote_id)
	{
		IrrecoLog::$server->log('XML-RPC getThemesOfRemote');
		$data = $this->database->getThemesOfRemote($remote_id);

		$array = array();

		if ($data == "Can't find any theme of remote.") {
			/*throw new XML_RPC2_FaultException(
					self::NO_THEMES_OF_REMOTE_MSG,
					self::NO_THEMES_OF_REMOTE_ERROR);*/
			array_push($array, 0);
		} else {
			foreach ($data as $item) {
				array_push($array, intval($item->id));
			}
		}

		IrrecoLog::$server->log("Theme id:\n" .
					IrrecoLog::getVarDump($array));
		return $array;
	}

	/**
	 * Get configurations of remote
	 *
	 * @param int remote_id id-number for remote.
	 * @return array
	 */
	function getConfigurationsOfRemote($remote_id)
	{
		IrrecoLog::$server->log('XML-RPC getConfigurationsOfRemote');
		$data = $this->database->getConfigurationsOfRemote($remote_id);

		$array = array();

		if ($data == "Can't find any configuration of remote.") {
			/*throw new XML_RPC2_FaultException(
				self::NO_CONFIGURATIONS_OF_REMOTE_MSG,
				self::NO_CONFIGURATIONS_OF_REMOTE_ERROR);*/
			array_push($array, 0);
		} else {
			foreach ($data as $item) {
				array_push($array, intval($item->id));
			}
		}

		IrrecoLog::$server->log("Configuration id:\n" .
					IrrecoLog::getVarDump($array));
		return $array;
	}

	/**
	 * Get remote data
	 *
	 * Returns content of file
	 * @param int remote_id id-number for remote.
	 * @return struct
	 */
	function getRemoteData($remote_id)
	{
		IrrecoLog::$server->log('XML-RPC getRemoteData');

		$remote = $this->database->getRemoteById($remote_id);

		$array = array();

		if ($remote != NULL) {
			$data = $this->database->getFileData(
				$remote['file_hash'], $remote['file_name']);
			$array['data'] = $data[0]->data;

			if ($data != NULL) {
				$this->database->increaseRemoteCounter(
								$remote_id);
			}
		}

		IrrecoLog::$database->log("Data:\n" .
					  IrrecoLog::getVarDump($array));
		return $array;
	}
	
	/**
	 * Get LIRC directories
	 *
	 * Returns array of directories
	 * @return array
	 */
	function getLircDirs()
	{
		IrrecoLog::$server->log('XML-RPC getLircDirs');
		$d = dir("../lircdb/lirc-devices/");
		IrrecoLog::$server->log("Handle: " . $d->handle . "\n");
		IrrecoLog::$server->log("Path: " . $d->path . "\n");

		$array = array();
		$i = 0;
		while (false !== ($entry = $d->read())) {
			if($entry != '.' && $entry != '..'){
				IrrecoLog::$server->log($entry."\n");
				$array[$i] = $entry;
				$i = $i + 1;
			}
		}
		$d->close();

		sort($array);
		return $array;
	}

	/**
	 * Get LIRC manufacturers
	 *
	 * Returns array of directories
	 * @param string directory
	 * @return array
	 */
 	function getLircManufacturers($range)
	{
		IrrecoLog::$server->log('XML-RPC getLircManufacturers');
		$d = dir("../lircdb/lirc-devices/" . $range);
		IrrecoLog::$server->log("Handle: " . $d->handle . "\n");
		IrrecoLog::$server->log("Path: " . $d->path . "\n");

		$array = array();
		$i = 0;
		while (false !== ($entry = $d->read())) {
			if($entry != '.' && $entry != '..'){
				IrrecoLog::$server->log($entry."\n");
				$array[$i] = $entry;
				$i = $i + 1;
			}
		}
		$d->close();

		sort($array);
		return $array;
	}
	/**
	 * Get LIRC models
	 *
	 * Returns array of models
	 * @param string manufacturer
	 * @return array
	 */
	function getLircModels($manufacturer)
	{
		IrrecoLog::$server->log('XML-RPC getLircModels');
		$d = dir("../lircdb/lirc-devices/" . $manufacturer);
		IrrecoLog::$server->log("Handle: " . $d->handle . "\n");
		IrrecoLog::$server->log("Path: " . $d->path . "\n");

		$array = array();
		$i = 0;
		while (false !== ($entry = $d->read())) {
			if($entry != '.' && $entry != '..'){
				IrrecoLog::$server->log($entry."\n");
				$array[$i] = $entry;
				$i = $i + 1;
			}
		}
		$d->close();

		sort($array);
		return $array;
	}
	/**
	 * Get LIRC device file contents
	 *
	 * Returns device file contents in an array
	 * @param string model
	 * @return array contents
	 */
	function getLircFile($model)
	{
		IrrecoLog::$server->log('XML-RPC getLircFile');
		$path = "../lircdb/lirc-devices/" . $model;

		/* $file_contents = file_get_contents($path); */

		$array = array();
		$array = file($path);

		/* return $file_contents; */
		return $array;
	}
}

?>

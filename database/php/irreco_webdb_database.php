<?php

/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007,2008  Arto Karppinen (arto.karppinen@iki.fi),
 * Joni Kokko (t5kojo01@students.oamk.fi),
 * Sami Mäki (kasmra@xob.kapsi.fi),
 * Harri Vattulainen (t5vaha01@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

require_once 'MDB2.php';
require_once 'irreco_webdb_log.php';

class IrrecoWebdbDatabase
{
	private $db;
	private $log;
	private $prepared_query;
	private $previous_result_set;

	function __construct($dsn)
	{
		$options = array('debug' => 2,
				 'result_buffering' => false);

		IrrecoLog::$database->log("Connecting to server...");
		$this->db = MDB2::factory($dsn, $options);

		$this->db->setFetchMode(MDB2_FETCHMODE_OBJECT);
		if (PEAR::isError($this->db)) {
			IrrecoLog::critical($this->db->getMessage());
		}
	}

	function __destruct() {
		if (is_object($this->db)) {
			$this->db->disconnect();
			$this->db = NULL;
		}
	}

	/**
	 * Run Query on database. Die on error.
	 */
	private function query($query)
	{
		/* Make sure that we dont collect multiple result
		   sets in memory. */
		if (is_object($this->previous_result_set)) {
			$this->previous_result_set->free();
			$this->previous_result_set = NULL;
		}

		IrrecoLog::$database->log('Query: "' . $query . '"');
		$result = $this->db->query($query);
		if (PEAR::isError($result)) {
			IrrecoLog::critical($result->getMessage());
		} else {
			$this->previous_result_set = $result;
			return $result;
		}
	}

	/**
	 * Prepare Query on database. Die on error.
	 */
	private function prepare($query, $types, $prepare)
	{
		/* Make sure that we dont prepare several queries. */
		if (is_object($this->prepared_query)) {
			$this->prepared_query->free();
			$this->prepared_query = NULL;
		}

		IrrecoLog::$database->log('Prepare: "' . $query . '"');
		$result = $this->db->prepare($query/*, $types, $prepare*/);
		if (PEAR::isError($result)) {
			IrrecoLog::critical($result->getMessage());
		} else {
			$this->prepared_query = $result;
			return $result;
		}
	}

	/**
	 * Execute Query on database. Die on error.
	 */
	private function execute($args)
	{
		/* Make sure that we dont collect multiple result
		   sets in memory. */
		if (is_object($this->previous_result_set)) {
			$this->previous_result_set->free();
			$this->previous_result_set = NULL;
		}

		IrrecoLog::$database->log('Execute: "' . serialize($args) . '"');
		$result = $this->prepared_query->execute($args);
		if (PEAR::isError($result)) {
			IrrecoLog::critical($result->getMessage());
		} else {
			$this->previous_result_set = $result;
			return $result;
		}
	}

	private function lockTable($table)
	{
		$this->query('LOCK TABLE ' .
			     $this->db->quoteIdentifier($table, 'text').
			     ' WRITE');
		/*
		$this->prepare('LOCK TABLE ? WRITE',
			       array('text'), MDB2_PREPARE_RESULT);
		$this->execute($table);
		*/
	}

	private function unlockTables()
	{
		$this->query('UNLOCK TABLES');
	}

	/**
	 * Add user to database
	 */
	function addUser($name, $email, $passwd, $table)
	{
		IrrecoLog::$database->log('Add user: "' . $name .
				'" to table "' . $table . '".');

		/* Lock table */
		$this->lockTable($table);

		/* Check for same name already at DB */
		$data = $this->getNameId($table, $name);
		if ($data != "") {
			IrrecoLog::$database->log('Name already at database.');
			return FALSE;
		}

		/* insert data */
		$this->query("INSERT INTO " .
				$this->db->quoteIdentifier($table, 'text').
				" VALUES (NULL, ".
				$this->db->quote($name, 'text'). ",
				". $this->db->quote($email, 'text'). ",
				". $this->db->quote($passwd, 'text').
				", NOW(), NOW() )" );

		/* Unlock table */
		$this->unlockTables();

		IrrecoLog::$database->log('Done.');

		return TRUE;
	}


	/**
	 * Add User agent data to database
	 */
	function addUserAgentData($user_agent)
	{
		IrrecoLog::$database->log("Client:" . $user_agent);
		$result = $this->query('SELECT user_agent AS '.
					'user_agent FROM user_agent WHERE date = CURDATE() '.
					'AND user_agent = '. $this->db->quote($user_agent, 'text'));
		$rows = $result->fetchAll();

		if($user_agent != $rows[0]->user_agent) {
			$this->query('INSERT INTO user_agent (user_agent, date, count) VALUES('.
					$this->db->quote($user_agent, 'text').', CURDATE(), 1)');
		}
		else{
			$this->query('UPDATE user_agent SET count = count+1 WHERE user_agent = '.
					$this->db->quote($user_agent, 'text').' AND date= CURDATE()');
		}

	}


	/**
	 * Add configuration to database
	 *
	 * @param string backend Name for Backend. e.g. "IRTrans Transceiver"
	 * @param string category Name for Category. e.g. "Tv"
	 * @param string manufacturer Name for Manufacturer. e.g. "Sony"
	 * @param string model Name for Model. e.g. "Sony"
	 * @param string user Username
	 * @param string password sha1-hash of password
	 * @param string file_hash sha1-hash of file_data
	 * @param string file_name Name for File
	 * @param string file_data Content of file
	 * @return string
	 */
	function uploadConfiguration($backend,
				     $category,
	 			     $manufacturer,
				     $model,
				     $user,
	 			     $password,
				     $file_hash,
				     $file_name,
				     $file_data)
	{
		/* Check if username matches up to password and get id */
		$user_id = $this->checkUsernameAndPassword($user, $password);
		if($user_id == NULL) {
			return('username and password mismatch.');
		}

		/* Check if file hash matches with sha1 sum of file data */
		if(sha1($file_data) != $file_hash) {
			return('file hash and file data mismatch.');
		}

		/* get id for backend, category and manufacturer */
		/* create new if needed */
		/* ?delete new id's if adding data fails? */
		$backend_id = $this->createNameId('backend', $backend);
		$category_id = $this->createNameId('category', $category);
		$manufacturer_id = $this->createNameId('manufacturer',
						      $manufacturer);

		/* insert data */
		$this->lockTable('file');

		if ($this->fileExists($file_hash, $file_name)) {
			$this->unlockTables();
			return('file already exist.');
		}

		$this->query('INSERT INTO file VALUES ( '.
			     $this->db->quote($file_hash, 'text'). ', '.
			     $this->db->quote($file_name, 'text'). ', '.
			     $this->db->quote($file_data, 'text'). ', '.
			     'NOW(), NOW(), 0)');

		/* Unlock table */
		$this->unlockTables();

		/* Create configuration */
		$this->lockTable('configuration');
		$this->query("INSERT INTO configuration VALUES (NULL, ".
			     $this->db->quote($user_id, 'text'). ", ".
			     $this->db->quote($backend_id, 'text'). ", ".
			     $this->db->quote($category_id, 'text'). ", ".
			     $this->db->quote($manufacturer_id, 'text'). ", ".
			     $this->db->quote($model, 'text'). ", ".
			     $this->db->quote($file_hash, 'text'). ", ".
			     $this->db->quote($file_name, 'text'). ")");

		/* Unlock table */
		$this->unlockTables();

		IrrecoLog::$database->log('configuration added successfully.');

		return('configuration added successfully');
	}

	function dumpResultToLog($result)
	{
		IrrecoLog::$database->log('Dumping result set.');

		$i = 1;
		while ($row = $result->fetchRow()) {
			IrrecoLog::$database->log(' Row ' . $i++ . ': ' .
						  serialize($row));
		}

		IrrecoLog::$database->log('Done.');
	}

	/**
	 * Check username and password.
	 */
	function checkUsernameAndPassword($name, $password)
	{
		$result = $this->query(
			'SELECT id AS id FROM user '.
			'WHERE name LIKE '. $this->db->quote($name, 'text').' '.
			'AND password = '. $this->db->quote($password, 'text'));
		$row = $result->fetchRow();

		if (is_object($row)) {
			$id = $row->id;
			IrrecoLog::$database->log(
					'username matches up to password');
		} else {
			$id = NULL;
			IrrecoLog::$database->log(
					'username and password mismatch');
		}
		return $id;
	}
	/**
	 * Check if file already exist
	 *
	 *
	 * @return TRUE if file exist
	 */
	function fileExists($file_hash, $file_name)
	{
		$result = $this->query(
		'SELECT hash AS hash, name AS name FROM file '.
		'WHERE hash = '. $this->db->quote($file_hash, 'text'). ' '.
		'AND name = '. $this->db->quote($file_name, 'text'));
		$row = $result->fetchRow();

		if (is_object($row)) {
			IrrecoLog::$database->log('file exist');
			return TRUE;
		}
		IrrecoLog::$database->log('file not exist');
		return FALSE;
	}

	/**
	 * Get ID of name.
	 */
	function getNameId($table, $name)
	{
		/* Attempt to fetch category. */
		$result = $this->query(
			'SELECT id AS id, name AS name FROM '.
			$this->db->quoteIdentifier($table, 'text') . ' ' .
			'WHERE name LIKE ' .
			$this->db->quote($name, 'text'));
		$row = $result->fetchRow();

		if (is_object($row)) {
			$id = $row->id;
			IrrecoLog::$database->log(
				'Id for name "' . $name .'" in table "' .
				$table . '" is "' . $id . '".', PEAR_LOG_DEBUG);
		} else {
			$id = NULL;
		}
		return $id;
	}

	/**
	 * Create ID for name if it does not exist, or get old ID of name.
	 */
	function createNameId($table, $name)
	{
		IrrecoLog::$database->log('Get id for name "' . $name .
					  '" in table "' . $table . '".');

		// Lock the table
		$this->lockTable($table);

		// Create category if it does not exists.
		$id = $this->getNameId($table, $name);
		if ($id != NULL) {
			$this->unlockTables();
			return $id;
		} else {
			$this->query('INSERT INTO ' .
				     $this->db->quoteIdentifier($table, 'text').
				     ' (name) VALUES(' .
				     $this->db->quote($name, 'text') . ')');
			$id = $this->db->lastInsertID();
		}

		$this->unlockTables();
		IrrecoLog::$database->log('Category ID: "' . $id . '".');
		return $id;
	}

	/**
	 * Get list of categories that have configurations in them.
	 *
	 * @return Two dimensional array with category rows.
	 */
	function getCategoryList()
	{
		$result = $this->query(
			'SELECT category.id AS id, '.
			'category.name AS name FROM category '.
			'RIGHT JOIN configuration '.
			'ON category.id = configuration.category '.
			'INNER JOIN file ON '.
			'configuration.file_hash = file.hash AND '.
			'configuration.file_name = file.name '.
			'GROUP BY category.name');
		$rows = $result->fetchAll();
		return $rows;
	}

	/**
	 * Get whole list of all categories. Even those that don't
	 * have configurations in them.
	 *
	 * @return Two dimensional array with all category rows.
	 */
	function getWholeCategoryList()
	{
		$result = $this->query(
			'SELECT category.id AS id, '.
			'category.name AS name FROM category '.
			'GROUP BY category.name');
		$rows = $result->fetchAll();
		return $rows;
	}

	/**
	 * Get list of manufacturers that have configurations in them.
	 *
	 * @return Two dimensional array with manufacturer rows for category.
	 */
	function getManufacturerList($category)
	{
		IrrecoLog::$database->log('Category name: "' . $category . '".');
		$result = $this->query(
			'SELECT manufacturer.id AS id, '.
			'manufacturer.name AS name FROM manufacturer '.
			'RIGHT JOIN configuration '.
			'ON manufacturer.id = configuration.manufacturer '.
			'WHERE manufacturer.id IN '.
			'(SELECT manufacturer FROM configuration '.
			'INNER JOIN file ON '.
			'configuration.file_hash = file.hash AND '.
			'configuration.file_name = file.name '.
			'RIGHT JOIN category '.
			'ON category.id = configuration.category '.
			"WHERE category.name = ".
			$this->db->quote($category, 'text').") ".
			'GROUP BY manufacturer.name');
		$rows = $result->fetchAll();
		return $rows;
	}

	/**
	 * Get list of all manufacturers. Even those that don't have
	 * configurations in them.
	 *
	 * @return Two dimensional array with manufacturer rows.
	 */
	function getWholeManufacturerList()
	{
		IrrecoLog::$database->log('Get all manufacturers: "' . '".');
		$result = $this->query(
			'SELECT manufacturer.id AS id, '.
			'manufacturer.name AS name FROM manufacturer '.
			'GROUP BY manufacturer.name');
		$rows = $result->fetchAll();
		return $rows;
	}

	/**
	 * Get list of Models by manufacturer in selected category.
	 *
	 * @return Two dimensional array
	 */
	function getModelList($category,$manufacturer)
	{
		IrrecoLog::$database->log('Category name: "' .
					  $category . '".');
		IrrecoLog::$database->log('Manufacturer name: "' .
					  $manufacturer . '".');
		$result = $this->query(
			'SELECT configuration.id AS id, '.
			'configuration.model AS model FROM configuration '.
			'RIGHT JOIN file ON '.
			'configuration.file_hash = file.hash AND '.
			'configuration.file_name = file.name '.
			"WHERE configuration.category = ".
			"(SELECT id FROM category WHERE name = ".
			$this->db->quote($category, 'text').") ".
			"AND configuration.manufacturer = ".
			"(SELECT id FROM manufacturer WHERE name = ".
			$this->db->quote($manufacturer, 'text').") ".
			'GROUP BY configuration.model');
		$rows = $result->fetchAll();
		return $rows;
	}

	/**
	 * Get list of Configurations for Model
	 *
	 * @return Two dimensional array
	 */
	function getConfigs($model)
	{
		IrrecoLog::$database->log('Model name: "'.$model.'"');

		$result = $this->query(
			'SELECT configuration.id AS id, '.
			'user.name AS user FROM configuration '.
			'LEFT JOIN user ON user.id = configuration.user '.
			'RIGHT JOIN file ON '.
			'configuration.file_hash = file.hash AND '.
			'configuration.file_name = file.name '.
			"WHERE configuration.model = ".
			$this->db->quote($model, 'text')." ".
			'ORDER BY user.name, file.uploaded DESC');

		$rows = $result->fetchAll();

		return $rows;
	}

	/**
	 * Get list of Configurations for Model and manufacturer
	 *
	 * @return Two dimensional array
	 */
	function getConfigurations($manufacturer, $model)
	{
		IrrecoLog::$database->log('Model name: "'.$model.'"');

		$result = $this->query(
			'SELECT configuration.id AS id, '.
			'user.name AS user FROM configuration '.
			'LEFT JOIN user ON user.id = configuration.user '.
			'RIGHT JOIN file ON '.
			'configuration.file_hash = file.hash AND '.
			'configuration.file_name = file.name '.
			"WHERE configuration.model = ".
			$this->db->quote($model, 'text')." ".
			"AND configuration.manufacturer = ".
			"(SELECT id FROM manufacturer WHERE name = ".
			$this->db->quote($manufacturer, 'text').") ".
			'ORDER BY user.name, file.uploaded DESC');

		$rows = $result->fetchAll();

		return $rows;
	}

	/**
	 * Get all data from category
	 *
	 * @return Two dimensional array
	 */
	function getConfiguration($id)
	{
		IrrecoLog::$database->log('Category id: "'.$id.'"');

		$result = $this->query(
			'SELECT configuration.id AS id, '.
			'user.name AS user, '.
			'backend.name AS backend, '.
			'category.name AS category, '.
			'manufacturer.name AS manufacturer, '.
			'configuration.model AS model, '.
			'configuration.file_hash AS file_hash, '.
			'configuration.file_name AS file_name, '.
			'file.uploaded AS uploaded, '.
			'file.download_count AS download_count '.
			'FROM configuration RIGHT JOIN user '.
			'ON configuration.user = user.id '.
			'RIGHT JOIN backend ON '.
			'configuration.backend = backend.id '.
			'RIGHT JOIN category ON '.
			'configuration.category = category.id '.
			'RIGHT JOIN manufacturer ON '.
			'configuration.manufacturer = manufacturer.id '.
			'RIGHT JOIN file ON '.
			'configuration.file_hash = file.hash AND '.
			'configuration.file_name = file.name '.
			"WHERE configuration.id = ".
			$this->db->quote($id, 'text'));

		$rows = $result->fetchAll();

		return $rows;
	}

	/**
	 * Get configuration_id by name and date.
	 *
	 * Returns configuration_id or 0 if configuration does not exists.
	 * @param string file_hash sha1-hash of file_data
	 * @param string file_name Name for File
	 * @return int
	 */
	function getConfigIdByFilehashAndFilename($file_hash, $file_name)
	{
		$result = $this->query(
			'SELECT id AS id FROM configuration '.
			'WHERE file_hash = '.
			$this->db->quote($file_hash, 'text').' '.
			'AND file_name = '.
			$this->db->quote($file_name, 'text'));
		$rows = $result->fetchAll();
		$id = 0;
		if ($rows != NULL) {
			$id = intval($rows[0]->id);
		}
		return $id;
	}

	/**
	 * Get file data by hash and name
	 *
	 * @return array
	 */
	function getFileData($hash,$name)
	{
		IrrecoLog::$database->log('Hash: "' .
					  $hash . '".');
		IrrecoLog::$database->log('Name: "' .
					  $name . '".');
		$result = $this->query(
			'SELECT file.data AS data '.
			"FROM file WHERE file.hash = ".
			$this->db->quote($hash, 'text')." ".
			"AND file.name = ".
			$this->db->quote($name, 'text'));
		$rows = $result->fetchAll();

		//Update download_count. (timestamp will be updated
		//automatically)

		$this->lockTable('file');
		$this->query(
			"UPDATE file SET download_count = download_count + 1 ".
			"WHERE file.hash = ".
			$this->db->quote($hash, 'text')." ".
			"AND file.name = ".
			$this->db->quote($name, 'text'));
		$this->unlockTables();

		return $rows;
	}

	/**
	 * Create new Theme
	 *
	 *returns theme_id or 0
	 * @return int
	 */
	function createNewTheme($name, $comment, $preview_button,
				$folder, $user, $password)
	{
		IrrecoLog::$database->log('Theme name: "'.$name.'" '.
					  'Comment: "'.$comment.'" '.
					  'Previewbutton: "'.$preview_button.'" '.
					  'Folder: "'.$folder.'" '.
					  'User: "'.$user.'" ');

		/* Check if username matches up to password and get id */
		$user_id = $this->checkUsernameAndPassword($user, $password);
		if($user_id == NULL) {
			return intval(-1);
		}

		/* Check whether themename belongs to this user */
		$merrychristmas = $this->query(
			'SELECT id AS id FROM user '.
			'WHERE name LIKE '. $this->db->quote($user, 'text'));
		$merryrows = $merrychristmas->fetchAll();
		IrrecoLog::$database->log('user id: "'.$merryrows[0]->id.'"');
		$andhappynewyear = $this->query(
			'SELECT user_id AS user_id FROM theme '.
			'WHERE name LIKE '. $this->db->quote($name, 'text'));
		$happyrows = $andhappynewyear->fetchAll();
		IrrecoLog::$database->log('user id of themeowner: "'.$happyrows[0]->user_id.'"');

		if(!$happyrows[0]->user_id == NULL) {
			if($happyrows[0]->user_id != $merryrows[0]->id) {
				return intval(-2);
			}
		}

		$result = $this->query(
			'INSERT INTO irreco.theme ( '.
			'id, name, user_id, comment, preview_button, folder, '.
			'uploaded, modified, downloaded, download_count) '.
			'VALUES ( NULL, '.$this->db->quote($name, 'text').', '.
			$this->db->quote($user_id, 'text').', '.
			$this->db->quote($comment, 'text').', '.
			$this->db->quote($preview_button, 'text').', '.
			$this->db->quote($folder, 'text').', '.
			'NOW(), NOW(), NOW(), 0)');

		$theme = $this->query(
			'SELECT id AS id FROM irreco.theme '.
			'WHERE uploaded = (SELECT MAX(uploaded) '.
			'FROM irreco.theme)');
		$row = $theme->fetchAll();

		return intval($row[0]->id);
	}

	/**
	 * Set Theme downloadable
	 *
	 * @param int id id-number for theme.
	 * @param bool downloadable
	 * @param string user Username
	 * @param string password sha1-hash of password
	 * @return int
	 */
	function setThemeDownloadable($id, $downloadable, $user, $password)
	{
		/* Check if username matches up to password and get id */
		$user_id = $this->checkUsernameAndPassword($user, $password);
		if($user_id == NULL) {
			return('username and password mismatch.');
		}

		$success = $this->query('UPDATE theme SET downloadable = '.
				$this->db->quote($downloadable, 'text').
				' WHERE theme.id = '.
				$this->db->quote($id, 'text'));
		return 1;
	}

	/**
	 * Get list of themes
	 *
	 * @return Two dimensional array.
	 */
	function getThemeList()
	{
		$result = $this->query(
			'SELECT id AS id, name AS name FROM theme '.
			'WHERE downloadable = 1 AND '.
			'uploaded IN (SELECT MAX(uploaded) '.
			'FROM theme WHERE downloadable = 1 '.
			'GROUP BY name) ORDER BY name');
		$rows = $result->fetchAll();

		if ($rows[0]->name == NULL) {
			return('Database is empty.');
		}
		return $rows;
	}

	/**
	 * Get all data from theme
	 *
	 * Returns name, user, comment, preview_button, folder,
	 * uploaded, modified, downloaded, download_count
	 * @return Two dimensional array
	 */
	function getTheme($id)
	{
		IrrecoLog::$database->log('Theme id: "'.$id.'"');

		$result = $this->query(
			'SELECT theme.id AS id, '.
			'theme.name AS name, '.
			'user.name AS user, '.
			'theme.comment AS comment, '.
			'theme.preview_button AS preview_button, '.
			'theme.folder AS folder, '.
			'theme.uploaded AS uploaded, '.
			'theme.modified AS modified, '.
			'theme.downloaded AS downloaded, '.
			'theme.download_count AS download_count '.
			'FROM theme RIGHT JOIN user '.
			'ON theme.user_id = user.id '.
			"WHERE theme.id = ".
			$this->db->quote($id, 'text'));

		$rows = $result->fetchAll();

		return $rows;
	}

	/**
	 * Get Versions od Theme by name
	 *
	 * @return Two dimensional array.
	 */
	function getThemeVersionsByName($name)
	{
		$result = $this->query(
			'SELECT id AS id, uploaded AS uploaded FROM theme '.
			"WHERE name = ".$this->db->quote($name, 'text')." ".
			'AND downloadable = 1 '.
			'ORDER BY uploaded DESC');
		$rows = $result->fetchAll();
		return $rows;
	}

	/**
	 * Get theme_id by name and date.
	 *
	 * Returns theme_id or 0 if theme does not exists.
	 * @param string name Name for theme.
	 * @param string date theme creation date.
	 * @return int
	 */
	function getThemeIdByNameAndDate($name, $date)
	{
		$result = $this->query(
			'SELECT id AS id FROM theme '.
			'WHERE name = '.$this->db->quote($name, 'text').' '.
			'AND uploaded = '.$this->db->quote($date, 'text').' '.
			'AND downloadable = 1 ');
		$rows = $result->fetchAll();
		$id = 0;
		if ($rows != NULL) {
			$id = intval($rows[0]->id);
		}
		return $id;
	}

	/**
	 * Add button to Theme
	 *
	 *returns button_id or 0
	 * @return int
	 */
	function addButtonToTheme($name, $allow_text, $text_format_up,
				$text_format_down, $text_padding,
				$text_h_align, $text_v_align,
				$image_up_hash, $image_up_name, $image_up,
				$image_down_hash, $image_down_name, $image_down,
				$folder, $theme_id, $user, $password)
	{
		IrrecoLog::$database->log('Button name: "'.$name.'" '.
				'allow_text: "'.$allow_text.'" '.
				'text_format_up: "'.$text_format_up.'" '.
				'text_format_down: "'.$text_format_down.'" '.
				'text_padding: "'.$text_padding.'" '.
				'text_h_align: "'.$text_h_align.'" '.
				'text_v_align: "'.$text_v_align.'" '.
				'image_up_hash: "'.$image_up_hash.'" '.
				'image_up_name: "'.$image_up_name.'" '.
				'image_down_hash: "'.$image_down_hash.'" '.
				'image_down_name: "'.$image_down_name.'" '.
				'folder: "'.$folder.'" '.
				'theme_id: "'.$theme_id.'" '.
				'User: "'.$user.'" ');

		/* Check if username matches up to password and get id */
		$user_id = $this->checkUsernameAndPassword($user, $password);
		if($user_id == NULL) {
			return('username and password mismatch.');
		}

		/* Check if file hash matches with sha1 sum of file data */
		if(sha1($image_up) != $image_up_hash ||
		   sha1($image_down) != $image_down_hash) {
			return('file hash and file data mismatch.');
		}

		/* insert data */
		$this->lockTable('file');

		if (!$this->fileExists($image_up_hash, $image_up_name)) {
			$this->query('INSERT INTO file VALUES ( '.
			     $this->db->quote($image_up_hash, 'text'). ', '.
			     $this->db->quote($image_up_name, 'text'). ', '.
			     $this->db->quote($image_up, 'text'). ', '.
			     'NOW(), NOW(), 0)');
		}
		if (!$this->fileExists($image_down_hash, $image_down_name)) {
			$this->query('INSERT INTO file VALUES ( '.
			     $this->db->quote($image_down_hash, 'text'). ', '.
			     $this->db->quote($image_down_name, 'text'). ', '.
			     $this->db->quote($image_down, 'text'). ', '.
			     'NOW(), NOW(), 0)');
		}
		$allow_text_string = 'FALSE';
		if ($allow_text == TRUE) {
			$allow_text_string = 'TRUE';
		}

		/* Unlock table */
		$this->unlockTables();

		$button = $this->query(
			'SELECT id AS id FROM irreco.button '.
			'WHERE theme_id = '.
			$this->db->quote($theme_id, 'text').' AND '.
			'name = '.$this->db->quote($name, 'text'));

		$row = $button->fetchAll();

		if ($row[0]->id == NULL) {
			$result = $this->query(
			'INSERT INTO irreco.button ( '.
			'id, name, allow_text, text_format_up, '.
			'text_format_down, text_padding, '.
			'text_h_align, text_v_align, image_up_hash, '.
			'image_up_name, image_down_hash, image_down_name, '.
			'folder, theme_id) '.
			'VALUES ( NULL, '.$this->db->quote($name, 'text').', '.
			$allow_text_string.', '.
			$this->db->quote($text_format_up, 'text').', '.
			$this->db->quote($text_format_down, 'text').', '.
			$this->db->quote($text_padding, 'text').', '.
			$this->db->quote($text_h_align, 'text').', '.
			$this->db->quote($text_v_align, 'text').', '.
			$this->db->quote($image_up_hash, 'text').', '.
			$this->db->quote($image_up_name, 'text').', '.
			$this->db->quote($image_down_hash, 'text').', '.
			$this->db->quote($image_down_name, 'text').', '.
			$this->db->quote($folder, 'text').', '.
			$this->db->quote($theme_id, 'text').')');

			IrrecoLog::$database->log('Button added');

			$button = $this->query(
			'SELECT id AS id FROM irreco.button '.
			'WHERE theme_id = '.
			$this->db->quote($theme_id, 'text').' AND '.
			'name = '.$this->db->quote($name, 'text'));

			$row = $button->fetchAll();
		} else {
			$result = $this->query(
			'UPDATE irreco.button SET '.
			'allow_text = '.$allow_text_string.', '.
			'text_format_up = '.
			$this->db->quote($text_format_up, 'text').', '.
			'text_format_down = '.
			$this->db->quote($text_format_down, 'text').', '.
			'text_padding = '.
			$this->db->quote($text_padding, 'text').', '.
			'text_h_align = '.
			$this->db->quote($text_h_align, 'text').', '.
			'text_v_align = '.
			$this->db->quote($text_v_align, 'text').', '.
			'image_up_hash = '.
			$this->db->quote($image_up_hash, 'text').', '.
			'image_up_name = '.
			$this->db->quote($image_up_name, 'text').', '.
			'image_down_hash = '.
			$this->db->quote($image_down_hash, 'text').', '.
			'image_down_name = '.
			$this->db->quote($image_down_name, 'text').', '.
			'folder = '.$this->db->quote($folder, 'text').' '.
			'WHERE theme_id = '.
			$this->db->quote($theme_id, 'text').' AND '.
			'name = '.$this->db->quote($name, 'text'));

			IrrecoLog::$database->log('Button updated');
		}

		return intval($row[0]->id);
	}

	/**
	 * Get list of buttons for theme
	 *
	 * @return Two dimensional array.
	 */
	function getButtonList($theme_id)
	{
		$result = $this->query(
			'SELECT id AS id, name AS name FROM button '.
			'WHERE theme_id = '.
			$this->db->quote($theme_id, 'text').' '.
			'ORDER BY name');
		$rows = $result->fetchAll();
		return $rows;
	}

	/**
	 * Get all data from Button
	 *
	 * Returns name, allow_text, text_format_up, text_format_down,
	 * text_padding, image_up_hash, image_up_name, base64 encoded image_up,
         * image_down_hash, image_down_name, base64 encoded image_down, folder,
         * theme_id
	 * @param int id id-number for button.
	 * @return struct
	 */
	function getButton($id)
	{
		IrrecoLog::$database->log('Button id: "'.$id.'"');

		$result = $this->query('SELECT name AS name, '.
				       'allow_text AS allow_text, '.
				       'text_format_up AS text_format_up, '.
				       'text_format_down AS text_format_down, '.
				       'text_padding AS text_padding, '.
				       'text_h_align AS text_h_align, '.
				       'text_v_align AS text_v_align, '.
				       'image_up_hash AS image_up_hash, '.
				       'image_up_name AS image_up_name, '.
				       'image_down_hash AS image_down_hash, '.
				       'image_down_name AS image_down_name, '.
				       'folder AS folder, '.
                                       'theme_id AS theme_id '.
				       'FROM button WHERE id = '.
				        $this->db->quote($id, 'text'));
		$rows = $result->fetchAll();

		$image_up = $this->getFileData($rows[0]->image_up_hash,
					       $rows[0]->image_up_name);

		$image_down = $this->getFileData($rows[0]->image_down_hash,
						 $rows[0]->image_down_name);

		//Set the type of a variable for text-format
		settype($rows[0]->text_format_up, "string");
		settype($rows[0]->text_format_down, "string");

		settype ( $rows[0]->allow_text , "boolean");
		$array = array();
		$array['name'] = $rows[0]->name;
		$array['allow_text'] = $rows[0]->allow_text;
		$array['text_format_up'] = $rows[0]->text_format_up;
		$array['text_format_down'] = $rows[0]->text_format_down;
		$array['text_padding'] = intval($rows[0]->text_padding);
		$array['text_h_align'] = doubleval($rows[0]->text_h_align);
		$array['text_v_align'] = doubleval($rows[0]->text_v_align);
		$array['image_up_hash'] = $rows[0]->image_up_hash;
		$array['image_up_name'] = $rows[0]->image_up_name;
		$array['image_up'] = base64_encode($image_up[0]->data);
		$array['image_down_hash'] = $rows[0]->image_down_hash;
		$array['image_down_name'] = $rows[0]->image_down_name;
		$array['image_down'] = base64_encode($image_down[0]->data);
		$array['folder'] = $rows[0]->folder;

                $theme_id = $rows[0]->theme_id;

                $last_button = $this->query('SELECT MAX(id) AS id '.
                                            'FROM button WHERE theme_id = '.
                                            $theme_id);
                $button = $last_button->fetchAll();

                if($button[0]->id == $id)
                {
                        $this->query('UPDATE theme SET download_count = '.
                                     'download_count + 1 WHERE id = '.
                                     $theme_id);
                }

		return $array;
	}

	/**
	 * Get preview button
	 *
	 * Returns image-data
	 * @param int id id-number for theme.
	 * @return string
	 */
	function getPreviewButton($theme_id)
	{
		IrrecoLog::$database->log('Theme id: "'.$theme_id.'"');

		$result = $this->query(
			'SELECT button.image_up_hash AS hash, '.
			'button.image_up_name AS name FROM button '.
			'WHERE button.name = (SELECT preview_button '.
			'FROM theme WHERE id = '.
			$this->db->quote($theme_id, 'text').') AND '.
			'button.theme_id = '.
			$this->db->quote($theme_id, 'text'));

		$rows = $result->fetchAll();

		$data = $this->getFileData($rows[0]->hash, $rows[0]->name);

		return $data[0]->data;
	}
	/**
	 * Add background to Theme
	 *
	 *returns bg_id or 0
	 * @return int
	 */
	function addBgToTheme($name, $image_hash, $image_name, $image,
			      $folder, $theme_id, $user, $password)
	{
		IrrecoLog::$database->log('Bg name: "'.$name.'" '.
					  'image_hash: "'.$image_up_hash.'" '.
					  'image_name: "'.$image_up_name.'" '.
					  'folder: "'.$folder.'" '.
					  'theme_id: "'.$theme_id.'" '.
					  'User: "'.$user.'" ');

		/* Check if username matches up to password and get id */
		$user_id = $this->checkUsernameAndPassword($user, $password);
		if($user_id == NULL) {
			return('username and password mismatch.');
		}

		/* Check if file hash matches with sha1 sum of file data */
		if(sha1($image) != $image_hash) {
			return('file hash and file data mismatch.');
		}

		/* insert data */
		$this->lockTable('file');

		if (!$this->fileExists($image_hash, $image_name)) {
			$this->query('INSERT INTO file VALUES ( '.
			     $this->db->quote($image_hash, 'text'). ', '.
			     $this->db->quote($image_name, 'text'). ', '.
			     $this->db->quote($image, 'text'). ', '.
			     'NOW(), NOW(), 0)');
		}

		/* Unlock table */
		$this->unlockTables();

		$bg = $this->query(
			'SELECT id AS id FROM irreco.bg '.
			'WHERE theme_id = '.
			$this->db->quote($theme_id, 'text').' AND '.
			'name = '.$this->db->quote($name, 'text'));
		$row = $bg->fetchAll();

		if ($row[0]->id == NULL) {
			$result = $this->query(
			'INSERT INTO irreco.bg ( '.
			'id, name, image_hash, image_name, folder, theme_id) '.
			'VALUES ( NULL, '.$this->db->quote($name, 'text').', '.
			$this->db->quote($image_hash, 'text').', '.
			$this->db->quote($image_name, 'text').', '.
			$this->db->quote($folder, 'text').', '.
			$this->db->quote($theme_id, 'text').')');

			if ($result == 1) {
				IrrecoLog::$database->log('BG added');
			}

			$bg = $this->query(
			'SELECT id AS id FROM irreco.bg '.
			'WHERE theme_id = '.
			$this->db->quote($theme_id, 'text').' AND '.
			'name = '.$this->db->quote($name, 'text'));

			$row = $bg->fetchAll();

		} else {
			$result = $this->query(
			'UPDATE irreco.bg SET '.
			'image_hash = '.
			$this->db->quote($image_hash, 'text').', '.
			'image_name = '.
			$this->db->quote($image_name, 'text').', '.
			'folder = '.$this->db->quote($folder, 'text').' '.
			'WHERE theme_id = '.
			$this->db->quote($theme_id, 'text').' AND '.
			'name = '.$this->db->quote($name, 'text'));

			if ($result == 1) {
				IrrecoLog::$database->log('BG updated');
			}
		}



		return intval($row[0]->id);
	}

	/**
	 * Get list of backgrounds for theme
	 *
	 * @return Two dimensional array.
	 */
	function getBgList($theme_id)
	{
		$result = $this->query(
			'SELECT id AS id, name AS name FROM bg '.
			'WHERE theme_id = '.
			$this->db->quote($theme_id, 'text').' '.
			'ORDER BY name');
		$rows = $result->fetchAll();
		return $rows;
	}

	/**
	 * Get all data from Background
	 *
	 * Returns name, image_hash, image_name,
	 * folder and base64 encoded image_data
	 * @param int id id-number for background.
	 * @return struct
	 */
	function getBg($id)
	{
		IrrecoLog::$database->log('Bg id: "'.$id.'"');

		$result = $this->query('SELECT name AS name, '.
				       'image_hash AS hash, '.
				       'image_name AS file_name, '.
				       'folder AS folder '.
				       'FROM bg WHERE id = '.
				        $this->db->quote($id, 'text'));
		$rows = $result->fetchAll();

		$data = $this->getFileData($rows[0]->hash, $rows[0]->file_name);

		$array = array();
		$array['name'] = $rows[0]->name;
		$array['image_hash'] = $rows[0]->hash;
		$array['image_name'] = $rows[0]->file_name;
		$array['image_data'] = base64_encode($data[0]->data);
		$array['folder'] = $rows[0]->folder;

		return $array;
	}

	/**
	 * Login to database
	 *
	 * @param string user Username
	 * @param string password sha1-hash of password
	 * @return boolean
	 */
	function loginToDB($user, $password)
        {

                /* Check if username matches up to password and get id */
                $user_id = $this->checkUsernameAndPassword($user, $password);
                if($user_id == NULL)
                {
                        return FALSE;
                }

                return TRUE;
        }

	/**
	 * Create new Remote
	 *
	 *returns remote_id or 0
	 * @return int
	 */
	function createNewRemote($comment, $category, $manufacturer,
				 $model, $file_hash, $file_name, $file_data,
				 $user, $password)
	{
		IrrecoLog::$database->log('Remote model: "'.$model.'" '.
					  'Comment: "'.$comment.'" '.
					  'Category: "'.$category.'" '.
					  'Manufacturer: "'.$manufacturer);

		/* Check if username matches up to password and get id */
		$user_id = $this->checkUsernameAndPassword($user, $password);
		if($user_id == NULL) {
			return('username and password mismatch.');
		}

		/* Check if file hash matches with sha1 sum of file data */
		if(sha1($file_data) != $file_hash) {
			return('file hash and file data mismatch.');
		}

		/* get id for backend, category and manufacturer */
		/* create new if needed */
		$category_id = $this->createNameId('category', $category);
		$manufacturer_id = $this->createNameId('manufacturer',
						      $manufacturer);

		if (!$this->fileExists($file_hash, $file_name)) {
			$this->query('INSERT INTO file VALUES ( '.
				$this->db->quote($file_hash, 'text'). ', '.
				$this->db->quote($file_name, 'text'). ', '.
				$this->db->quote($file_data, 'text'). ', '.
				'NOW(), NOW(), 0)');
		}

		$result = $this->query(
			'INSERT INTO irreco.remote ( '.
			'id, user_id, comment, category, manufacturer, '.
			'model, file_hash, file_name, uploaded, modified, '.
			'downloaded, download_count, downloadable) '.
			'VALUES ( NULL, '.
			$this->db->quote($user_id, 'text').', '.
			$this->db->quote($comment, 'text').', '.
			$this->db->quote($category_id, 'text').', '.
			$this->db->quote($manufacturer_id, 'text').', '.
			$this->db->quote($model, 'text').', '.
			$this->db->quote($file_hash, 'text').', '.
			$this->db->quote($file_name, 'text').', '.
			'NOW(), NOW(), NOW(), 0, 0)');

		$theme = $this->query(
			'SELECT id AS id FROM irreco.remote '.
			'WHERE uploaded = (SELECT MAX(uploaded) '.
			'FROM irreco.remote)');
		$row = $theme->fetchAll();

		return intval($row[0]->id);
	}

	/**
	 * Set Remote downloadable
	 *
	 * @param int id id-number for remote.
	 * @param bool downloadable
	 * @param string user Username
	 * @param string password sha1-hash of password
	 * @return int
	 */
	function setRemoteDownloadable($id, $downloadable, $user, $password)
	{
		/* Check if username matches up to password and get id */
		$user_id = $this->checkUsernameAndPassword($user, $password);
		if($user_id == NULL) {
			return('username and password mismatch.');
		}

		$success = $this->query('UPDATE remote SET downloadable = '.
					$this->db->quote($downloadable, 'text').
					' WHERE remote.id = '.
					$this->db->quote($id, 'text'));
		return 1;
	}

	function addConfigurationToRemote($remote_id, $config_id,
					  $user, $password)
	{
		/* Check if username matches up to password and get id */
		$user_id = $this->checkUsernameAndPassword($user, $password);
		if($user_id == NULL) {
			return('username and password mismatch.');
		}

		$result = $this->query('SELECT id AS id '.
				'FROM remote_configuration WHERE '.
				'remote_id = '.
				$this->db->quote($remote_id, 'text').' AND '.
				'configuration_id = '.
				$this->db->quote($config_id, 'text'));
		$rows = $result->fetchAll();

		if ($rows == NULL) {
			$success = $this->query(
				'INSERT INTO remote_configuration '.
				'(remote_id, configuration_id) '.
				'VALUES ( '.
				$this->db->quote($remote_id, 'text').', '.
				$this->db->quote($config_id, 'text').')');
		}
		return 1;
	}

	function addThemeToRemote($remote_id, $theme_id, $user, $password)
	{
		/* Check if username matches up to password and get id */
		$user_id = $this->checkUsernameAndPassword($user, $password);
		if($user_id == NULL) {
			return('username and password mismatch.');
		}

		$result = $this->query('SELECT id AS id '.
				'FROM remote_theme WHERE '.
				'remote_id = '.
				$this->db->quote($remote_id, 'text').' AND '.
				'theme_id = '.
				$this->db->quote($theme_id, 'text'));
		$rows = $result->fetchAll();

		if ($rows == NULL) {
			$success = $this->query(
				'INSERT INTO irreco.remote_theme '.
				'(id, remote_id, theme_id) '.
				'VALUES ( NULL, '.
				$this->db->quote($remote_id, 'text').', '.
				$this->db->quote($theme_id, 'text').')');
		}
		return 1;
	}

	/**
	 * Get list of categories that have remotes in them.
	 *
	 * @return Two dimensional array with category rows.
	 */
	function getRemoteCategories()
	{
		$result = $this->query(
			'SELECT category.id AS id, '.
			'category.name AS name FROM category '.
			'RIGHT JOIN remote '.
			'ON category.id = remote.category '.
			'INNER JOIN file ON '.
			'remote.file_hash = file.hash AND '.
			'remote.file_name = file.name '.
			'AND remote.downloadable = TRUE '.
			'GROUP BY category.name');
		$rows = $result->fetchAll();
		return $rows;
	}

	/**
	 * Get list of manufacturers that have remotes in them.
	 *
	 * @return Two dimensional array with manufacturer rows for category.
	 */
	function getRemoteManufacturers($category)
	{
		IrrecoLog::$database->log('Category name: "' . $category . '".');
		$result = $this->query(
			'SELECT manufacturer.id AS id, '.
			'manufacturer.name AS name FROM manufacturer '.
			'RIGHT JOIN remote '.
			'ON manufacturer.id = remote.manufacturer '.
			'WHERE manufacturer.id IN '.
			'(SELECT manufacturer FROM remote '.
			'INNER JOIN file ON '.
			'remote.file_hash = file.hash AND '.
			'remote.file_name = file.name '.
			'RIGHT JOIN category '.
			'ON category.id = remote.category '.
			'WHERE category.name = '.
			$this->db->quote($category, 'text').') '.
			'AND remote.downloadable = TRUE '.
			'GROUP BY manufacturer.name');
		$rows = $result->fetchAll();
		return $rows;
	}

	/**
	 * Get list of Models by manufacturer in selected category.
	 *
	 * @return Two dimensional array
	 */
	function getRemoteModels($category,$manufacturer)
	{
		$result = $this->query(
			'SELECT remote.id AS id, '.
			'remote.model AS model FROM remote '.
			'RIGHT JOIN file ON '.
			'remote.file_hash = file.hash AND '.
			'remote.file_name = file.name '.
			'WHERE remote.category = '.
			'(SELECT id FROM category WHERE name = '.
			$this->db->quote($category, 'text').') '.
			'AND remote.manufacturer = '.
			'(SELECT id FROM manufacturer WHERE name = '.
			$this->db->quote($manufacturer, 'text').') '.
			'AND remote.downloadable = TRUE '.
			'GROUP BY remote.model');
		$rows = $result->fetchAll();
		return $rows;
	}

	/**
	 * Get Creators for model.
	 *
	 * @param string category Name for Category. e.g. "Tv"
	 * @param string manufacturer Name for Manufacturer. e.g. "Sony"
	 * @param string model Name for Model. e.g. "xyz"
	 * @return array
	 */
	function getRemoteCreators($category, $manufacturer, $model)
	{
		$result = $this->query(
			'SELECT remote.id AS id, '.
			'user.name AS user FROM remote '.
			'LEFT JOIN user ON user.id = remote.user_id '.
			'WHERE remote.model = '.
			$this->db->quote($model, 'text').' '.
			'AND remote.manufacturer = '.
			'(SELECT id FROM manufacturer WHERE name = '.
			$this->db->quote($manufacturer, 'text').') '.
			'AND remote.category = '.
			'(SELECT id FROM category WHERE name = '.
			$this->db->quote($category, 'text').') '.
			'AND remote.downloadable = TRUE '.
			'GROUP BY user '.
			'ORDER BY user.name');
		$rows = $result->fetchAll();
		return $rows;
	}

	/**
	 * Get Remotes
	 *
	 * Returns list of remote_id
	 * @param string category Name for Category. e.g. "Tv"
	 * @param string manufacturer Name for Manufacturer. e.g. "Sony"
	 * @param string model Name for Model. e.g. "xyz"
	 * @param string user Username of Remote creator
	 * @return Two dimensional array
	 */
	function getRemotes($category, $manufacturer, $model, $user)
	{
		$result = $this->query(
			'SELECT remote.id AS id FROM remote '.
			'WHERE remote.user_id = '.
			'(SELECT id FROM user WHERE name = '.
			$this->db->quote($user, 'text').') '.
			'AND remote.model = '.
			$this->db->quote($model, 'text').' '.
			'AND remote.manufacturer = '.
			'(SELECT id FROM manufacturer WHERE name = '.
			$this->db->quote($manufacturer, 'text').') '.
			'AND remote.category = '.
			'(SELECT id FROM category WHERE name = '.
			$this->db->quote($category, 'text').') '.
			'AND remote.downloadable = TRUE '.
			'ORDER BY remote.uploaded DESC');
		$rows = $result->fetchAll();

		return $rows;
	}

	/**
	 * Get all data from remote
	 *
	 * Returns name, user, comment,  category, manufacturer,
	 * model, file_hash, file_name, uploaded, modified, downloaded,
	 * and download_count
	 * @param int id id-number for remote.
	 * @return struct
	 */
	function getRemoteById($id)
	{
		$data = $this->query(
			'SELECT remote.id AS id, '.
			'user.name AS user, '.
			'remote.comment AS comment, '.
			'category.name AS category, '.
			'manufacturer.name AS manufacturer, '.
			'remote.model AS model, '.
			'remote.file_hash AS file_hash, '.
			'remote.file_name AS file_name, '.
			'remote.uploaded AS uploaded, '.
			'remote.modified AS modified, '.
			'remote.downloaded AS downloaded, '.
			'remote.download_count AS download_count '.
			'FROM remote RIGHT JOIN user '.
			'ON remote.user_id = user.id '.
			'RIGHT JOIN category ON '.
			'remote.category = category.id '.
			'RIGHT JOIN manufacturer ON '.
			'remote.manufacturer = manufacturer.id '.
			'WHERE remote.id = '.
			$this->db->quote($id, 'text'));

		$rows = $data->fetchAll();

		$array = array();

		$array['user'] = $rows[0]->user;
		$array['comment'] = $rows[0]->comment;
		$array['category'] = $rows[0]->category;
		$array['manufacturer'] = $rows[0]->manufacturer;
		$array['model'] = $rows[0]->model;
		$array['file_hash'] = $rows[0]->file_hash;
		$array['file_name'] = $rows[0]->file_name;
		$array['uploaded'] = $rows[0]->uploaded;
		$array['modified'] = $rows[0]->modified;
		$array['downloaded'] = $rows[0]->downloaded;
		$array['download_count'] = intval($rows[0]->download_count);

		return $array;
	}

	/**
	 * Get themes of remote
	 *
	 * @param int remote_id id-number for remote.
	 * @return array
	 */
	function getThemesOfRemote($remote_id)
	{
		$result = $this->query(
			'SELECT remote_theme.theme_id AS id, '.
			'theme.name AS name FROM remote_theme '.
			'RIGHT JOIN theme ON '.
			'remote_theme.theme_id = theme.id '.
			'WHERE remote_theme.remote_id = '.
			$this->db->quote($remote_id, 'text').' '.
			'ORDER BY theme.name');
		$rows = $result->fetchAll();

		if ($rows == NULL) {
			return("Can't find any theme of remote.");
		}
		return $rows;
	}

	/**
	 * Get configurations of remote
	 *
	 * @param int remote_id id-number for remote.
	 * @return array
	 */
	function getConfigurationsOfRemote($remote_id)
	{
		$result = $this->query(
			'SELECT remote_configuration.configuration_id AS id, '.
			'configuration.model AS model '.
			'FROM remote_configuration '.
			'RIGHT JOIN configuration ON '.
			'remote_configuration.configuration_id = '.
			'configuration.id '.
			'WHERE remote_configuration.remote_id = '.
			$this->db->quote($remote_id, 'text').' '.
			'ORDER BY configuration.model');
		$rows = $result->fetchAll();

		if ($rows == NULL) {
			return("Can't find any configuration of remote.");
		}
		return $rows;
	}

	/**
	 * Increase Remote counter
	 *
	 * @param int remote_id id-number for remote.
	 */
	function increaseRemoteCounter($remote_id)
	{
		$this->query('UPDATE remote '.
			     'SET download_count = download_count + 1, '.
			     'downloaded = NOW() '.
			     'WHERE id = '.
			      $this->db->quote($remote_id, 'text'));
	}
}

?>

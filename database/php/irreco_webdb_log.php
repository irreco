<?php

require_once 'PEAR.php';
require_once 'Log.php';

class IrrecoLog
{
	private static $irreco_log;
	private static $php_level;

	public static $critical;
	public static $php;
	public static $exception;
	public static $pear;
	public static $index;
	public static $database;
	public static $server;

	public static function getVarDump(&$var)
	{
		ob_start();
		var_dump($var);
		return ob_get_clean();
	}

	public static function init()
	{
		$file = 'webdb.log';
		$conf = array('mode' => 0644,
			      'timeFormat' => '%Y-%m-%d %T');

		/* Critical. */
		if (!is_object(self::$critical)) {
			self::$critical = Log::singleton(
				'file', $file, NULL, $conf);
		}

		/* Setup PHP error logging. */
		if (!is_object(self::$php)) {
			self::$php = Log::singleton(
				'file', $file, 'PHP', $conf);
			set_error_handler(
				array('IrrecoLog', 'phpErrorHandler'));
		}

		/* Setup PHP exception error logging. */
		if (!is_object(self::$exception)) {
			self::$exception = Log::singleton(
				'file', $file, 'EXCEPTION', $conf);
			set_exception_handler(
				array('IrrecoLog', 'phpExceptionHandler'));
		}

		/* Setup PEAR error logging. */
		if (!is_object(self::$pear)) {
			self::$pear = Log::singleton(
				'file', $file, 'PEAR', $conf);
			PEAR::setErrorHandling(PEAR_ERROR_CALLBACK,
				array('IrrecoLog', 'pearErrorHandler'));
		}

		/* Index */
		if (!is_object(self::$index)) {
			self::$index = Log::singleton(
				'file', $file, 'Index', $conf);
		}

		/* Database */
		if (!is_object(self::$database)) {
			self::$database = Log::singleton(
				'file', $file, 'Database', $conf);
		}

		/* Server */
		if (!is_object(self::$server)) {
			self::$server = Log::singleton(
				'file', $file, 'Server', $conf);
		}
	}

	/**
	 * Log critical error, and exit.
	 */
	public static function critical($message)
	{
		IrrecoLog::$critical->log($message, PEAR_LOG_CRIT);
		die($message);
	}

	/**
	 * PHP error handler for saving errors to Log.
	 */
	public static function phpErrorHandler($code, $message, $file, $line)
	{
		if ($code > self::$php_level) return;

		/* Map the PHP error to a Log priority. */
		switch ($code) {
		case E_WARNING:
		case E_USER_WARNING:
			$priority = PEAR_LOG_WARNING;
			break;

		case E_NOTICE:
		case E_USER_NOTICE:
			$priority = PEAR_LOG_NOTICE;
			break;

		case E_ERROR:
		case E_USER_ERROR:
			$priority = PEAR_LOG_ERR;
			break;

		default:
			$priority = PEAR_LOG_INFO;
			break;
		}

		IrrecoLog::$php->log('[' . $code . '] ' . $message . ' in ' .
				     $file . ' at line ' . $line . '.',
				     $priority);
	}

	/**
	 * PHP exception handler for saving uncaught exceptions to log.
	 */
	public static function phpExceptionHandler($exception)
	{
		IrrecoLog::$exception->log($exception->getMessage(),
					   PEAR_LOG_ALERT);
	}

	/**
	 * PEAR error handler for saving errors to Log.
	 */
	public static function pearErrorHandler($error)
	{
		$message = $error->getMessage();

		if (!empty($error->backtrace[1]['file'])) {
			$message .= ' (' . $error->backtrace[1]['file'];
			if (!empty($error->backtrace[1]['line'])) {
				$message .= ' at line ';
				$message .= $error->backtrace[1]['line'];
			}
			$message .= ')';
		}

		$log = IrrecoLog::$pear;

		/* Make sure the error code is a valid PEAR error code. */
		switch ($error->code) {
		case PEAR_LOG_EMERG:
		case PEAR_LOG_ALERT:
		case PEAR_LOG_CRIT:
		case PEAR_LOG_ERR:
		case PEAR_LOG_WARNING:
		case PEAR_LOG_NOTICE:
		case PEAR_LOG_INFO:
		case PEAR_LOG_DEBUG:
			$code = $error->code;
			break;

		default:
			$message = '[' . $error->code . '] ' . $message;
			$code = PEAR_LOG_NOTICE;
		}

		$log->log($message, $code);
	}

	/**
	 * Set PEAR error reporting level.
	 *
	 * @param $level Errors of this level or more signifacant are logged.
	 */
	public static function pearLevel($level)
	{
		$mask = IrrecoLog::$database->MAX($level);

		IrrecoLog::$critical->setMask($mask);
		IrrecoLog::$php->setMask($mask);
		IrrecoLog::$exception->setMask($mask);
		IrrecoLog::$pear->setMask($mask);
		IrrecoLog::$index->setMask($mask);
		IrrecoLog::$database->setMask($mask);
		IrrecoLog::$server->setMask($mask);
	}

	/**
	 * PHP logging level.
	 *
	 * @param $level Errors of this level or more signifacant are logged.
	 */
	public static function phpLevel($level)
	{
		self::$php_level = $level;
		error_reporting($level);
	}
}

?>

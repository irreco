<?php



/**
 * Make sure that HTTP_RAW_POST_DATA variable is available.
 */
ini_set('always_populate_raw_post_data', TRUE);



/**
 * Require modern PHP.
 *
 * http://gophp5.org/
 */
$version = "5.2.0";
if (!version_compare(phpversion(), "5.2.0", ">=")) {
	die("PHP " . $version . ' or later is required.');
}



/**
 * Setup logging.
 */
require_once 'irreco_webdb_log.php';
IrrecoLog::init();
IrrecoLog::phpLevel(2047);
//IrrecoLog::pearLevel(PEAR_LOG_NOTICE);
IrrecoLog::$index->log('Start >>>>>>>>>>>>>>>>>>');





/**
 * Some versions of PHP have a bug with setting HTTP_RAW_POST_DATA variable.
 */
if (!isset($GLOBALS['HTTP_RAW_POST_DATA'])) {
	IrrecoLog::$index->log('Fixing $HTTP_RAW_POST_DATA');
	$GLOBALS['HTTP_RAW_POST_DATA'] = file_get_contents('php://input');
}


/**
 * Create XML-RPC server.
 */

require_once 'irreco_webdb_database.php';
require_once 'irreco_webdb_server.php';

$database = new IrrecoWebdbDatabase('mysqli://irreco:abcd@localhost/irreco');
$server = new IrrecoWebdbServer($database);


/**
 * Get irreco version
 */
if (array_key_exists("HTTP_USER_AGENT", $_SERVER)) {
	$client = $_SERVER['HTTP_USER_AGENT'];
} else {
	$client = "unknown";
}
$database->addUserAgentData($client);


/**
 * Run XML-RPC server.
 */

IrrecoLog::$server->log("Request:\n". $GLOBALS['HTTP_RAW_POST_DATA']);
ob_start();

$options = array('encoding' => 'UTF-8');
$xmlrpc2 = XML_RPC2_Server::create($server, $options);
$xmlrpc2->handleCall();
$response = ob_get_clean();

$len = strlen($response);
if($len > 200) {
	IrrecoLog::$server->log("Response:\n". substr($response, 0, 200).
				" ...Listing cut for brevity...");
}
else {
	IrrecoLog::$server->log("Response:\n". $response);
}

echo $response;

IrrecoLog::$index->log('End <<<<<<<<<<<<<<<<<<<<');












?>

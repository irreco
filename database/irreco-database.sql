
CREATE DATABASE IF NOT EXISTS irreco CHARACTER SET utf8;

/*
 * List of categories.
 */
CREATE TABLE IF NOT EXISTS `irreco`.`category` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Device type categories';

/*
 * List of backends.
 */
CREATE TABLE IF NOT EXISTS `irreco`.`backend` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Irreco Backends';

/*
 * List of manufacturers.
 */
CREATE TABLE IF NOT EXISTS `irreco`.`manufacturer` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Device manufacturers';

/*
 * File storage table.
 */
CREATE TABLE IF NOT EXISTS `irreco`.`file` (
  `hash` varchar(40) NOT NULL,
  `name` varchar(60) NOT NULL,
  `data` LONGBLOB NOT NULL,
  `uploaded` datetime NOT NULL,
  `downloaded` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `download_count` int(11) NOT NULL default '0',
  PRIMARY KEY  (`hash`,`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='File storage';

/*
 * File storage update
 * Filedata max size is now 4 Gt
 * Old size was 64 kt which was too small for some images
 */
ALTER TABLE `irreco`.`file` CHANGE `data` `data` LONGBLOB NOT NULL;

/*
 * Users
 */
CREATE TABLE IF NOT EXISTS `irreco`.`user` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(30) NOT NULL,
  `email` varchar(60) NOT NULL,
  `password` varchar(40) NOT NULL,
  `account_registered` datetime NOT NULL,
  `previous_login` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Database users';

/*
 * Configuration map.
 */
CREATE TABLE IF NOT EXISTS `irreco`.`configuration` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `user` int(10) unsigned NOT NULL,
  `backend` int(10) unsigned NOT NULL,
  `category` int(10) unsigned NOT NULL,
  `manufacturer` int(10) unsigned NOT NULL,
  `model` varchar(30) NOT NULL,
  `file_hash` varchar(40) NOT NULL,
  `file_name` varchar(60) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Backend Configuration file index';

/*
 * Buttons
 */
CREATE TABLE IF NOT EXISTS `irreco`.`button` (
`id` int(10) unsigned NOT NULL auto_increment,
`name` varchar(50) NOT NULL,
`allow_text` tinyint(1) NOT NULL,
`text_format_up` varchar(120) default NULL,
`text_format_down` varchar(120) default NULL,
`text_padding` int(11) NOT NULL,
`text_h_align` double NOT NULL,
`text_v_align` double NOT NULL,
`image_up_hash` varchar(40) NOT NULL,
`image_up_name` varchar(60) NOT NULL,
`image_down_hash` varchar(40) NOT NULL,
`image_down_name` varchar(60) NOT NULL,
`folder` varchar(50) NOT NULL,
`theme_id` int(11) NOT NULL,
PRIMARY KEY ( `id` )
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Table for buttons';

/*
 * Themes
 */
CREATE TABLE IF NOT EXISTS `irreco`.`theme` (
`id` int(10) unsigned NOT NULL auto_increment,
`name` varchar(50) NOT NULL,
`user_id` int(11) NOT NULL,
`comment` text default NULL,
`preview_button` varchar(50) NOT NULL,
`folder` varchar(50) NOT NULL,
`uploaded` datetime NOT NULL,
`modified` datetime NOT NULL,
`downloaded` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
`download_count` int(11) NOT NULL,
`downloadable` tinyint(1) NOT NULL default '0',
PRIMARY KEY ( `id` )
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Table for themes';

/*
 * Backgrounds
 */
CREATE TABLE IF NOT EXISTS `irreco`.`bg` (
`id` int(10) unsigned NOT NULL auto_increment,
`name` varchar(50) NOT NULL,
`image_hash` varchar(40) NOT NULL,
`image_name` varchar(60) NOT NULL,
`folder` varchar(50) NOT NULL,
`theme_id` int(11) NOT NULL,
PRIMARY KEY ( `id` )
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Table for Background-images';

/*
 * User agents
 */
CREATE TABLE IF NOT EXISTS `irreco`.`user_agent` (
  `user_agent` char(200) NOT NULL,
  `date` date NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY  (`user_agent`,`date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Table for user agents';

/*
 * Remotes
 */
CREATE TABLE IF NOT EXISTS `irreco`.`remote` (
  `id` int(10) NOT NULL auto_increment,
  `user_id` int(10) NOT NULL,
  `comment` text,
  `category` int(10) NOT NULL,
  `manufacturer` int(10) NOT NULL,
  `model` varchar(30) NOT NULL,
  `file_hash` varchar(40) NOT NULL,
  `file_name` varchar(60) NOT NULL,
  `uploaded` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `downloaded` datetime NOT NULL,
  `download_count` int(11) NOT NULL default '0',
  `downloadable` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 COMMENT='Table for remotes';

/*
 * Configurations of remote
 */
CREATE TABLE IF NOT EXISTS `irreco`.`remote_configuration` (
  `id` int(10) NOT NULL auto_increment,
  `remote_id` int(10) NOT NULL,
  `configuration_id` int(10) NOT NULL,
  FOREIGN KEY(remote_id) REFERENCES remote(id) ON DELETE CASCADE,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 COMMENT='Configurations of remote';

/*
 * Themes of remote
 */
CREATE TABLE IF NOT EXISTS `irreco`.`remote_theme` (
  `id` int(10) NOT NULL auto_increment,
  `remote_id` int(10) NOT NULL,
  `theme_id` int(10) NOT NULL,
  FOREIGN KEY(remote_id) REFERENCES remote(id) ON DELETE CASCADE,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 COMMENT='Themes of remote';





#!/bin/sh
cd `dirname "$0"`

# Remove stuff created by autogen

rm -fv aclocal.m4
rm -fv compile
rm -fv config.guess
rm -fv config.h
rm -fv config.h.in
rm -fv config.log
rm -fv config.status
rm -fv config.sub
rm -fv configure
rm -fv depcomp
rm -fv data/images/bg/Makefile
rm -fv data/images/bg/Makefile.in
rm -fv .exit_code
rm -fv install-sh
rm -fv intltool-extract
rm -fv intltool-extract.in
rm -fv intltool-merge
rm -fv intltool-merge.in
rm -fv intltool-update
rm -fv intltool-update.in
rm -fv irreco-util
rm -fv libtool
rm -fv ltmain.sh
rm -fv Makefile
rm -fv Makefile.in
rm -fv make_log
rm -fv missing
rm -fv scratchbox_target
rm -fv stamp-h1
rm -fv src/util/.deps
rm -fv src/util/.libs
rm -fv src/util/Makefile
rm -fv src/util/Makefile.in
rm -fv src/Makefile.in
rm -fv src/api/Makefile.in
rm -fv src/util/Makefile.in
rm -rfv autom4te.cache

echo done


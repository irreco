#!/bin/sh
set -x
cd `dirname "$0"` \
&& autoreconf --install --force \
&& intltoolize --automake --copy --force



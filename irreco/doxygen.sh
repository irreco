#!/bin/bash

#
# Doxygen will not generate the documentation correctly if doxygen is not
# executed from the correct directory.
#
cd "$( dirname "$0" )"

#
# Generate backend template.
#
mkdir -p ./example
./irreco-backend-maker.sh ./example/irreco_backend_template.c

#
# Run Doxygen.
#
doxygen doxygen/Doxyfile

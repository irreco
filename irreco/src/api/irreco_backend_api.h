/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi),
 * Joni Kokko (t5kojo01@students.oamk.fi),
 * Sami Mäki (kasmra@xob.kapsi.fi),
 * Harri Vattulainen (t5vaha01@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @page PageIrrecoBackendTemplate Irreco Backend Template
 * @include irreco_backend_template.c
 */

/**
 * @addtogroup IrrecoBackendApi Irreco Backend API
 *
 * Irreco Backend API is what allows Irreco to communicate with miscellaneous
 * remote control systems using Backends.
 *
 * The idea is that one Backend @b Library will provide ways to communicate to
 * one @b type of remote control system, while one @b Instance of given backend
 * will control one @b system of that type.
 *
 * For example, the could be a backend for Lirc server, and there could be one
 * instance per each Lirc server the user wants to use.
 *
 * Each intance should contain a list of @b Devices and each device should have
 * a list of @b Commands that it provides.
 *
 * For example, if the Lirc server has been configured with the the ir-codes
 * of two remotes, then those two remotes would be the @b Devices, and the
 * ir-signals the remotes send would be the @b Commands.
 *
 * So the structure would be like this:
 *
 * @code
 * Lirc Server               Lirc backend instance 1
 *     Yamaha amplifier      Lirc backend instance 1, Device 1
 *         Volume up         Lirc backend instance 1, Device 1, Command 1
 *         Volume down       Lirc backend instance 1, Device 1, Command 2
 *         Power             Lirc backend instance 1, Device 1, Command 3
 *     CD player             Lirc backend instance 1, Device 2
 *         Next track        Lirc backend instance 1, Device 2, Command 1
 *         Previous track    Lirc backend instance 1, Device 2, Command 2
 *         Play              Lirc backend instance 1, Device 2, Command 3
 * Lirc Server 2             Lirc backend instance 2
 *     Sony TV               Lirc backend instance 2, Device 1
 *         Power             Lirc backend instance 2, Device 1, Command 1
 *         Previous channel  Lirc backend instance 2, Device 1, Command 2
 *         Next channel      Lirc backend instance 2, Device 1, Command 3
 * @endcode
 *
 * .... and so on until the world runs out of Lirc servers ....
 *
 * In the user interface backend instance is called device controller. Which
 * hopefully makes more sense to the user.
 *
 * @sa @ref PageIrrecoBackendTemplate
 *
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoBackendApi.
 */

#ifndef __IRRECO_BACKEND_API__
#define __IRRECO_BACKEND_API__

#include <glib.h>
#include <gtk/gtk.h>
#include "irreco_backend_file_container.h"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Implemented by Irreco.                                                     */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Implemented by Irreco
 * @{
 */

/**
 * Get device callback. The backend should call this once per device.
 *
 * @sa IrrecoBackendGetDevices
 * @param name User friendly name to be displayed to the user.
 *             Irreco will make a copy of the name buffer, and will not
 *             free it.
 * @param command_contex An user data type of pointer. Irreco will not use this
 *                       pointer, if will simply be passed back to the backend
 *                       as a way of identifying the device.
 */
typedef void (*IrrecoGetDeviceCallback) (const gchar * name,
					 gpointer device_contex);

/**
 * Get command callback. The backend should call this once per command.
 *
 * @sa IrrecoBackendGetCommands
 * @param name User friendly name to be displayed to the user.
 *             Irreco will make a copy of the name buffer, and will not
 *             free it.
 * @param command_contex An user data type of pointer. Irreco will not use this
 *                       pointer, if will simply be passed back to the backend
 *                       as a way of identifying the command.
 */
typedef void (*IrrecoGetCommandCallback) (const gchar * name,
					  gpointer command_contex);

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Error handling.                                                            */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Error Handling
 * @{
 */

/**
 * Most backend functions must return a status code. If this status code is
 * #IRRECO_BACKEND_OK then Irreco assumes that the function succeeded in
 * whatever it was doing.
 *
 * However, if the status code is anything else than IRRECO_BACKEND_OK then
 * Irreco will assume that the function failed, and asks for a more descriptive
 * error message with IrrecoBackendGetErrorMsg. Then the error message and
 * the code will be displayed to the user.
 */
typedef enum {
	IRRECO_BACKEND_OK = 0
} IrrecoBackendStatus;

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* To be Implemented by Backend.                                              */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Implemented by Backend
 * @{
 */

/**
 * Create new instance of the backend.
 *
 * The backend should be able to create multiple instances of itself.
 * Returns a new instance_context of the backend.
 *
 * @par Implement like this
 * @code
 * static gpointer
 * backend_create()
 * {
 * 	MyBackend *self = NULL;
 * 	self = g_slice_new0(MyBackend);
 * 	self->important_variable = 42;
 *	/_* @todo Implement initialization code. _/
 *	return self;
 * }
 * @endcode
 * @return New backend instance.
 */
typedef gpointer(*IrrecoBackendCreate) ();

/**
 * Destroy instance of the backend.
 *
 * If 'permanently' argument is FALSE, the backend instance is to be reloaded
 * from the configuration file the next time Irreco is started. If 'permanently'
 * is TRUE, the configuration file will be deleted by Irreco, and this instace
 * will be destroyd permanently.
 *
 * If Irreco is closed, and backend instace is destroyed, then permanently is
 * FALSE. If user chooses to delete controller, then permanently is TRUE.
 *
 * @par Implement like this
 * @code
 * static void
 * backend_destroy(gpointer instance_context,
 *                 gboolean permanently)
 * {
 * 	MyBackend *self = (MyBackend *) instance_context;
 *	/_* @todo Implement cleanup code. _/
 * 	g_slice_free(MyBackend, self);
 * }
 * @endcode
 * @param permanently Will this backend instance be permanently destroyed.
 */
typedef void
(*IrrecoBackendDestroy) (gpointer instance_context,
			 gboolean permanently);

/**
 * Return error message which corresponds to the backend status code.
 *
 * @par Implement like this
 * @code
 * static const gchar *
 * backend_get_error_msg(gpointer instance_context,
 *                       IrrecoBackendStatus code)
 * {
 * 	MyBackend *self = (MyBackend *) instance_context;
 *	/_* @todo Implement. _/
 * }
 * @endcode
 * @return Error message.
 */
typedef const gchar *
(*IrrecoBackendGetErrorMsg) (gpointer instance_context,
			     IrrecoBackendStatus code);

/**
 * Create new instance of the backend.
 *
 * The backend should be able to read the configuration file and configure
 * itself accordingly.
 *
 * @par Implement like this
 * @code
 * static IrrecoBackendStatus
 * backend_read_from_conf(gpointer instance_context,
 *                        const gchar * config_file)
 * {
 * 	MyBackend *self = (MyBackend *) instance_context;
 *	/_* @todo Implement. _/
 * }
 * @endcode
 * @param config_file Filename of configuration file.
 * @return #IRRECO_BACKEND_OK on success, error code on failure
 */
typedef IrrecoBackendStatus
(*IrrecoBackendReadFromConf) (gpointer instance_context,
			      const gchar * config_file);

/**
 * Save backend configuration into the file.
 *
 * Irreco will assign the filename to use.
 *
 * @par Implement like this
 * @code
 * static IrrecoBackendStatus
 * backend_save_to_conf(gpointer instance_context,
 *                      const gchar * config_file)
 * {
 * 	MyBackend *self = (MyBackend *) instance_context;
 *	/_* @todo Implement. _/
 * }
 * @endcode
 * @param config_file Filename of configuration file.
 * @return #IRRECO_BACKEND_OK on success, error code on failure
 */
typedef IrrecoBackendStatus
(*IrrecoBackendSaveToConf) (gpointer instance_context,
			    const gchar * config_file);

/**
 * Get list of devices the current backend instance knows.
 *
 * Backend must call IrrecoGetDeviceCallback once per each Device.
 *
 * @par Implement like this
 * @code
 * static IrrecoBackendStatus
 * backend_get_devices(gpointer instance_context,
 *                     IrrecoGetDeviceCallback get_device)
 * {
 * 	MyBackend *self = (MyBackend *) instance_context;
 *
 *	/_ Add all devices to Irreco's device list. _/
 * 	for ( ?? ) {
 * 		get_device(name, user_data)
 * 	}
 *	/_* @todo Implement. _/
 * }
 * @endcode
 * @param get_device Used to report Devices to Irreco.
 * @return #IRRECO_BACKEND_OK on success, error code on failure
 */
typedef IrrecoBackendStatus
(*IrrecoBackendGetDevices) (gpointer instance_context,
			    IrrecoGetDeviceCallback get_device);

/**
 * Get list of commands the device supports.
 *
 * @par Implement like this
 * @code
 * static IrrecoBackendStatus
 * backend_get_commands(gpointer instance_context,
 *                      const gchar * device_name,
 *                      gpointer device_contex,
 *                      IrrecoGetCommandCallback get_command)
 * {
 * 	MyBackend *self = (MyBackend *) instance_context;
 *
 *	/_ Add all commands to Irreco's command list. _/
 * 	for ( ?? ) {
 * 		get_command(name, user_data)
 * 	}
 *	/_* @todo Implement. _/
 * }
 * @endcode
 * @param get_command Used to report Commands of Device to Irreco.
 * @return #IRRECO_BACKEND_OK on success, error code on failure
 */
typedef IrrecoBackendStatus
(*IrrecoBackendGetCommands) (gpointer instance_context,
			     const gchar * device_name,
			     gpointer device_contex,
			     IrrecoGetCommandCallback get_command);

/**
 * Send command to device.
 *
 * Irreco calls this function when user presses a button and triggers execution
 * of command chain. Then Irreco will call IrrecoBackendSendCommand once per
 * every command to be sent.
 *
 * @par Implement like this
 * @code
 * static IrrecoBackendStatus
 * backend_send_command(gpointer instance_context,
 *                      const gchar * device_name,
 *                      gpointer device_contex,
 *                      const gchar * command_name,
 *                      gpointer command_contex)
 * {
 * 	MyBackend *self = (MyBackend *) instance_context;
 *	/_* @todo Implement. _/
 * }
 * @endcode
 * @param device_name Name of Device, as reported to IrrecoGetDeviceCallback.
 * @param device_name Contex of Device, as reported to IrrecoGetDeviceCallback.
 * @param device_name Name of Command, as reported to IrrecoGetCommandCallback.
 * @param device_name Contex of Command, as reported to
 *                    IrrecoGetCommandCallback.
 * @return #IRRECO_BACKEND_OK on success, error code on failure
 */
typedef IrrecoBackendStatus
(*IrrecoBackendSendCommand) (gpointer instance_context,
			     const gchar * device_name,
			     gpointer device_contex,
			     const gchar * command_name,
			     gpointer command_contex);

/**
 * Configure the backend.
 *
 * The backend should pop up some kind of dialog where the user can input the
 * configuration if needed.
 *
 * @par Implement like this
 * @code
 * static IrrecoBackendStatus
 * backend_configure(gpointer instance_context,
 *                   GtkWindow * parent)
 * {
 * 	MyBackend *self = (MyBackend *) instance_context;
 * 	GtkWidget *dialog = NULL;
 *
 * 	dialog = gtk_dialog_new_with_buttons(
 * 		_("Configure MyBackend"), parent,
 * 		GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT |
 * 		GTK_DIALOG_NO_SEPARATOR,
 * 		GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
 * 		GTK_STOCK_OK, GTK_RESPONSE_OK,
 * 		NULL);
 *	/_* @todo Implement. _/
 * }
 * @endcode
 * @param parent Set as the parent window of configuration dialog.
 * @return #IRRECO_BACKEND_OK on success, error code on failure
 */
typedef IrrecoBackendStatus
(*IrrecoBackendConfigure) (gpointer instance_context,
			   GtkWindow * parent);

/**
 * Get a description of the instace.
 *
 * The description should be about 10 - 30 characters long string that
 * differentiates one instance from all the other instances. The description
 * is to be displayed to the user.
 *
 * @par Implement like this
 * @code
 * static gchar *
 * backend_get_description(gpointer instance_context)
 * {
 * 	MyBackend *self = (MyBackend *) instance_context;
 *	/_* @todo Implement. _/
 * }
 * @endcode
 * @return newly-allocated string.
 */
typedef gchar *(*IrrecoBackendGetDescription) (gpointer instance_context);

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Editable Devices Extension                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Extension: Editable Devices
 *
 * Implementing Editable
 * Devices functions and setting #IRRECO_BACKEND_EDITABLE_DEVICES flag on the
 * function table enables NEW, EDIT and DELETE buttons on Irreco Device dialog.
 *
 * This is an optional feature of IrrecoBackendApi.
 *
 * @{
 */

/**
 * Create a new device which can be controlled.
 *
 * The backend is expected to show appropriate dialog if needed.
 *
 * @par Implement like this
 * @code
 * static IrrecoBackendStatus
 * backend_create_device(gpointer instance_context,
 *                       GtkWindow * parent)
 * {
 * 	MyBackend *self = (MyBackend *) instance_context;
 *	/_* @todo Implement. _/
 * }
 * @endcode
 * @return #IRRECO_BACKEND_OK on success, error code on failure
 */
typedef IrrecoBackendStatus
(*IrrecoBackendCreateDevice) (gpointer instance_context,
			      GtkWindow * parent);

/**
 * Can the device be edited?
 *
 * Irreco uses this to determinate if the device can be edited. This is usefull
 * mainly if the backend supports both editable and uneditable devices.
 *
 * @par Implement like this
 * @code
 * static gboolean
 * backend_is_device_editable(gpointer instance_context,
 *                            const gchar * device_name,
 *                            gpointer device_contex)
 * {
 * 	MyBackend *self = (MyBackend *) instance_context;
 *	/_* @todo Implement. _/
 * }
 * @endcode
 * @return TRUE or FALSE
 */
typedef gboolean
(*IrrecoBackendIsDeviceEditable) (gpointer instance_context,
				  const gchar * device_name,
				  gpointer device_contex);

/**
 * Edit currently existing device.
 *
 * Basically this means, that the user should be able to add, remove and edit
 * commands the device has. The backend is expected to show appropriate dialog
 * if needed.
 *
 * @par Implement like this
 * @code
 * static IrrecoBackendStatus
 * backend_edit_device(gpointer instance_context,
 *                     const gchar * device_name,
 *                     gpointer device_contex,
 *                     GtkWindow * parent)
 * {
 * 	MyBackend *self = (MyBackend *) instance_context;
 *	/_* @todo Implement. _/
 * }
 * @endcode
 * @return #IRRECO_BACKEND_OK on success, error code on failure
 */
typedef IrrecoBackendStatus
(*IrrecoBackendEditDevice) (gpointer instance_context,
			    const gchar * device_name,
			    gpointer device_contex,
			    GtkWindow * parent);

/**
 * Destroy a device.
 *
 * The backend is expected to show appropriate dialog if needed.
 *
 * @par Implement like this
 * @code
 * static IrrecoBackendStatus
 * backend_delete_device(gpointer instance_context,
 *                       const gchar * device_name,
 *                       gpointer device_contex,
 *                       GtkWindow * parent)
 * {
 * 	MyBackend *self = (MyBackend *) instance_context;
 *	/_* @todo Implement. _/
 * }
 * @endcode
 * @return #IRRECO_BACKEND_OK on success, error code on failure
 */
typedef IrrecoBackendStatus
(*IrrecoBackendDeleteDevice) (gpointer instance_context,
			      const gchar * device_name,
			      gpointer device_contex,
			      GtkWindow * parent);

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Configuration Export Extension                                             */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Extension: Configuration Export
 *
 * Implementing Configuration
 * export functions and setting #IRRECO_BACKEND_CONFIGURATION_EXPORT flag on the
 * function table allows users to use Irreco Web Database to store
 * configurations used with the backend.
 *
 * This is an optional feature of IrrecoBackendApi.
 *
 * @{
 */

/**
 * Get configuration of backend.
 *
 * Returned filecontainer should include hash, name and data.
 *
 * @par Implement like this
 * @code
 * static IrrecoBackendStatus
 * backend_export_conf(gpointer instance_context,
 *                     const char * device_name,
 *                     IrrecoBackendFileContainer **file_container)
 * {
 * 	MyBackend *self = (MyBackend *) instance_context;
 *	/_* @todo Implement. _/
 * }
 * @endcode
 * @return #IRRECO_BACKEND_OK on success, error code on failure
 */
typedef IrrecoBackendStatus
(*IrrecoBackendExportConf) (gpointer instance_context,
			    const char * device_name,
			    IrrecoBackendFileContainer **file_container);

/**
 * Set configuration of backend.
 *
 * Set filecontainer should include hash, name and data.
 *
 * @par Implement like this
 * @code
 * static IrrecoBackendStatus
 * backend_import_conf(gpointer instance_context,
 *                     IrrecoBackendFileContainer *file_container)
 * {
 * 	MyBackend *self = (MyBackend *) instance_context;
 *	/_* @todo Implement. _/
 * }
 * @endcode
 * @return #IRRECO_BACKEND_OK on success, error code on failure
 */
typedef IrrecoBackendStatus
(*IrrecoBackendImportConf) (gpointer instance_context,
			    IrrecoBackendFileContainer *file_container);

/**
 * Is there already configuration like this?
 *
 * Set filecontainer should include hash, name and data, etc.
 *
 * @par Implement like this
 * @code
 * static IrrecoBackendStatus
 * backend_check_conf(gpointer instance_context,
 *                    IrrecoBackendFileContainer *file_container,
 *                    gboolean *configuration)
 * {
 * 	MyBackend *self = (MyBackend *) instance_context;
 *	/_* @todo Implement. _/
 * }
 * @endcode
 * @return #IRRECO_BACKEND_OK on success, error code on failure
 */
typedef IrrecoBackendStatus
(*IrrecoBackendCheckConf) (gpointer instance_context,
			   IrrecoBackendFileContainer *file_container,
			   gboolean *configuration);

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Function table structure.                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Function table
 * @{
 */

/**
 * Backend feature flags.
 */
typedef enum {
	IRRECO_BACKEND_EDITABLE_DEVICES		= 1 << 1,
	IRRECO_BACKEND_CONFIGURATION_EXPORT	= 1 << 2,
	IRRECO_BACKEND_MULTI_DEVICE_SUPPORT	= 1 << 3,
	IRRECO_BACKEND_NEW_INST_ON_CONF_IMPORT	= 1 << 4
} IrrecoBackendFlags;

/**
 * Current version of Irreco Backend API.
 *
 * Used to save compile time version of @ref IrrecoBackendApi to
 * IrrecoBackendFunctionTable. Irreco then uses the value to check if
 * the backend is compatible with the current version of Irreco.
 */
#define IRRECO_BACKEND_API_VERSION 3

/**
 * Function pointer table, aka vtable, of Irreco Backend.
 *
 * Please do not typecast functions. If you do, then GCC can not check
 * that function pointer and the function implementation match each other.
 *
 * If you need to typecast something, please typecast pointers inside
 * the function.
 *
 * @par Implement like this
 * @code
 * IrrecoBackendFunctionTable backend_function_table = {
 *	IRRECO_BACKEND_API_VERSION,	/_ backend_api_version		_/
 *	0,				/_ flags 			_/
 *	"My Backend",			/_ name				_/
 *
 *	backend_get_error_msg,		/_ get_error_msg 		_/
 *	backend_create,			/_ create			_/
 *	backend_destroy,		/_ destroy			_/
 *	backend_read_from_conf,		/_ from_conf			_/
 *	backend_save_to_conf,		/_ to_conf			_/
 *	backend_get_devices,		/_ get_devices			_/
 *	backend_get_commands,		/_ get_commands			_/
 *	backend_send_command,		/_ send_command			_/
 *	backend_configure,		/_ configure			_/
 *	backend_get_description,	/_ get_description    		_/
 *
 *	NULL,				/_ create_device,      optional	_/
 *	NULL,				/_ is_device_editable, optional	_/
 *	NULL,				/_ edit_device,        optional	_/
 *	NULL,				/_ delete_device,      optional	_/
 *
 *	NULL,				/_ export_conf,        optional	_/
 *	NULL,				/_ import_conf,        optional	_/
 *	NULL,				/_ check_conf,         optional	_/
 *
 *	NULL				/_ reserved _/
 * };
 * @endcode
 * @sa _IrrecoBackendFunctionTable
 */
typedef struct _IrrecoBackendFunctionTable IrrecoBackendFunctionTable;

/**
 * @sa IrrecoBackendFunctionTable
 */
struct _IrrecoBackendFunctionTable {
	guint				version;
	IrrecoBackendFlags		flags;
	const gchar *			name;

	IrrecoBackendGetErrorMsg	get_error_msg;
	IrrecoBackendCreate 		create;
	IrrecoBackendDestroy		destroy;
	IrrecoBackendReadFromConf	from_conf;
	IrrecoBackendSaveToConf		to_conf;
	IrrecoBackendGetDevices		get_devices;
	IrrecoBackendGetCommands	get_commands;
	IrrecoBackendSendCommand	send_command;
	IrrecoBackendConfigure		configure;
	IrrecoBackendGetDescription	get_description;

	IrrecoBackendCreateDevice	create_device;
	IrrecoBackendIsDeviceEditable	is_device_editable;
	IrrecoBackendEditDevice		edit_device;
	IrrecoBackendDeleteDevice	delete_device;

	IrrecoBackendExportConf		export_conf;
	IrrecoBackendImportConf		import_conf;
	IrrecoBackendCheckConf		check_conf;

	gpointer			reserved4;
};

/**
 * Get function table of the backend.
 *
 * @code
 * IrrecoBackendFunctionTable *get_irreco_backend_function_table()
 * {
 *	return &backend_function_table;
 * }
 * @endcode
 * @return Pointer to IrrecoBackendFunctionTable.
 */
typedef IrrecoBackendFunctionTable *(*GetIrrecoBackendFunctionTable) ();

/** @} */

/** @} */

#endif /* __IRRECO_BACKEND_API__ */


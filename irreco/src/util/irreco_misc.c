/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi),
 * Harri Vattulainen (t5vaha01@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */



#include "irreco_util.h"
#include <sys/socket.h>
#include <errno.h>

/**
 * @addtogroup IrrecoMisc
 * @ingroup IrrecoUtil
 *
 * All kinds of usefull stuff that isnt really part of Irreco. Does not
 * pull any other Irreco headers, so it can be safely included inside backends.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoMisc.
 */
/*
 * Utility functions.
 *
 * All kinds of usefull stuff that isnt really part of Irreco. Does not pull
 * any other Irreco headers, so it can be safely included inside backends.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
 void irreco_remove_layouts(IrrecoDirForeachData *dir_data);


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Public Functions                                                           */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Files and directories.                                                     */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Files and directories
 * @{
 */

/**
 * Check if the given filename is a directory.
 */
int irreco_is_dir(const char *filename)
{
	int rval;
	struct stat buf;
	char *realpath = canonicalize_file_name(filename);
	rval = (realpath != NULL) &&
	       (stat(filename, &buf) == 0) &&
	       S_ISDIR(buf.st_mode);
	free(realpath);
	return rval;
}

/**
 * Check if a given file is a regular file.
 */
int irreco_is_file(const char *filename)
{
	int rval;
	struct stat buf;
	char *realpath = canonicalize_file_name(filename);
	rval = (realpath != NULL) &&
	       (stat(filename, &buf) == 0) &&
	       S_ISREG(buf.st_mode);
	free(realpath);
	return rval;
}

/**
 * Does the the file or dir exists.
 */
int irreco_file_exists(const char *filename)
{
	struct stat struct_stat;
	if (stat(filename, &struct_stat) == 0) {
		return TRUE;
	}
	return FALSE;
}

/**
 * Return length of file
 * Return 0 if file doesn't exist
 */
gint irreco_file_length(const gchar *filename)
{
	struct stat struct_stat;

	if(!irreco_file_exists(filename)) {
		return 0;
	}

	stat(filename, &struct_stat);
	return struct_stat.st_size;
}

/**
 * Write data to file.
 */
gboolean irreco_write_file(const gchar * file, const gchar * data,
			   gsize data_size)
{
	FILE *handle;
	size_t written;
	IRRECO_ENTER

	if ((handle = fopen(file, "w")) == NULL) {
		IRRECO_ERROR("Failed to open \"%s\" for writing.\n", file);
		IRRECO_RETURN_BOOL(FALSE);
	}

	written = fwrite(data, sizeof(gchar), data_size, handle);
	fclose(handle);

	if (written != data_size) {
		IRRECO_ERROR("Failed to write data to \"%s\". "
			     "Data size \"%u\", wrote \"%u\".\n",
			     file, data_size, written);
		IRRECO_RETURN_BOOL(FALSE);
	}
	IRRECO_RETURN_BOOL(TRUE);
}

/**
 * Read contents of file into a buffer, or at least as much of the file the
 * buffer can contain.
 */
gboolean irreco_read_text_file(const gchar * file, gchar *buffer,
			       gsize buffer_size)
{
	gint count;
	FILE *fd;

	buffer[0] = '\0';
	if ((fd = fopen(file, "r")) == NULL) return FALSE;
	count = fread(buffer, 1, buffer_size, fd);
	buffer[count] = '\0';
	if (count < 1) return FALSE;
	return TRUE;
}

/**
 * Read contents of binary file into a buffer, or at least as much of the file the
 * buffer can contain.
 */
gboolean irreco_read_binary_file(const gchar *file, guchar *buffer,
			       gsize buffer_size, gint *binlen)
{
	gint count;
	FILE *fd;

	if ((fd = g_fopen(file, "rb")) == NULL) return FALSE;
	count = (gint) fread(buffer, 1, buffer_size, fd);
	fclose(fd);
	if (count < 1) return FALSE;
	*binlen = count;
	return TRUE;
}

/**
 * Read a line of text from file to buffer.
 */
gboolean irreco_read_line(const gchar * file, gchar *buffer,
			  gsize buffer_size)
{
	gint i;

	if (!irreco_read_text_file(file, buffer, buffer_size)) return FALSE;
	for (i = 0; i < buffer_size; i++) {
		if (buffer[i] == '\0' || buffer[i] == '\n') {
			buffer[i] = '\0';
			return TRUE;
		}
	}
	return TRUE;
}

/**
 * Write GKeyFile contents to file.
 */
gboolean irreco_write_keyfile(GKeyFile * keyfile, const gchar * file)
{
	gchar *data;
	gsize data_size;
	GError *error = NULL;
	gboolean success;
	IRRECO_ENTER

	data = g_key_file_to_data(keyfile, &data_size, &error);
	if (irreco_gerror_check_print(&error)) {
		IRRECO_RETURN_BOOL(FALSE);
	}

	success = irreco_write_file(file, data, data_size);
	g_free(data);
	IRRECO_RETURN_BOOL(success);
}

/**
 * Read all filenames that match suffix from directory.
 */
gboolean irreco_dir_foreach(IrrecoDirForeachData *dir_data,
			    IrrecoDirForeachCallback callback)
{
	GError *error = NULL;
	GDir* dir = NULL;
	IRRECO_ENTER

	/* Open Dir. */
	dir = g_dir_open(dir_data->directory, 0, &error);
	if (irreco_gerror_check_print(&error)) {
		IRRECO_ERROR("Could not read directory: \"%s\"\n",
			     dir_data->directory);
		if(dir != NULL){
			g_dir_close(dir);
		}
		IRRECO_RETURN_BOOL(FALSE);
	}

	/* Read dir. */
	while ((dir_data->filename = g_dir_read_name(dir)) != NULL) {
		if (g_str_has_suffix(dir_data->filename,
				     dir_data->filesuffix)) {
			dir_data->filepath = g_build_path("/",
				dir_data->directory, dir_data->filename, NULL);
			callback(dir_data);
			g_free((void*)dir_data->filepath);
		}
	}
	g_dir_close(dir);
	IRRECO_RETURN_BOOL(TRUE);
}



/**
 * Read all filenames that match suffix.
 * Searches ONLY thru subdirectories.
 */
gboolean irreco_dir_foreach_subdirectories(IrrecoDirForeachData *dir_data,
			    IrrecoDirForeachCallback callback)
{
	GError		*error = NULL;
	GDir		*dir = NULL;
	GDir		*subdir = NULL;
	gchar		*subpath = NULL;
	const gchar	*buttonsdir;
	const gchar	*directorykeeper = dir_data->directory;
	IRRECO_ENTER

	/* Open Dir. */
	dir = g_dir_open(dir_data->directory, 0, &error);
	if (irreco_gerror_check_print(&error)) {
		IRRECO_ERROR("Could not read directory: \"%s\"\n",
			     dir_data->directory);
		IRRECO_RETURN_BOOL(FALSE);
	}

	/* Read buttons dir file by file (also directories) */
	while ((buttonsdir = g_dir_read_name(dir)) != NULL) {

		/* Create possible subpath from readed files */
		subpath = g_build_path("/", dir_data->directory,
				       buttonsdir, NULL);

		/* Test if subpath is folder */
		if(g_file_test(subpath, G_FILE_TEST_IS_DIR)) {

			/* Create GDir from directory or multifail instantly */
			subdir = g_dir_open(subpath, 0, &error);
			if (irreco_gerror_check_print(&error)) {
				IRRECO_ERROR("Could not read dir: \"%s\"\n",
					     subpath);
				g_free(subpath);
				g_dir_close(dir);
				IRRECO_RETURN_BOOL(FALSE);
			}

			/* Start reading files from subdirectory */
			while ((dir_data->filename = g_dir_read_name(subdir))
								!= NULL) {

				/* Find files with wanted suffix */
				if (g_str_has_suffix(dir_data->filename,
							dir_data->filesuffix)) {

					dir_data->filepath = g_build_path("/",
							subpath,
							dir_data->filename,
							NULL);
					dir_data->directory = subpath;
					callback(dir_data);
					dir_data->directory = directorykeeper;
					g_free((void*)dir_data->filepath);
				}
			}
		}
		g_free(subpath);
		subpath = NULL;
		g_dir_close(subdir);
		subdir = NULL;
	}
	if(dir != NULL) g_dir_close(dir);
	if(subdir != NULL) g_dir_close(subdir);
	if(subpath != NULL) g_free(subpath);
	IRRECO_RETURN_BOOL(TRUE);
}

/**
 * Create path to directory $HOME/.APP_NAME, and attempt to create the
 * directory if it does not exists.
 *
 * @return Directory path inside a newly allocated string,
 *          or NULL if the directory could not be created.
 */
gchar * irreco_get_config_dir(const gchar * app_name)
{
	GString *app_name_with_dot;
	gchar *home;
	gchar *apphome;
	IRRECO_ENTER

	if ((home = getenv("HOME")) == NULL) IRRECO_RETURN_PTR(NULL);

	app_name_with_dot = g_string_new(".");
	g_string_append(app_name_with_dot, app_name);
	apphome = g_build_path("/", home, app_name_with_dot->str, NULL);
	g_string_free(app_name_with_dot, TRUE);

	if (irreco_is_dir(apphome) == TRUE
	    || g_mkdir(apphome, 0700) == 0)
		IRRECO_RETURN_PTR(apphome);

	g_free(apphome);
	IRRECO_RETURN_PTR(NULL);
}

/**
 * Get path to $HOME/.APP_NAME/FILE
 *
 * @return Newly allocated string which contains path to config file or NULL.
 */
gchar * irreco_get_config_file(const gchar * app_name, const gchar * file)
{
	gchar *config_dir;
	gchar *config_file;
	IRRECO_ENTER

	if ((config_dir = irreco_get_config_dir(app_name)) != NULL) {
		config_file = g_build_path("/", config_dir, file, NULL);
		g_free(config_dir);
		IRRECO_RETURN_PTR(config_file);
	}
	IRRECO_RETURN_PTR(NULL);
}

/**
 * Check that given filename is valid layout conf filename or create new one
 *
 * @return Newly allocated string containing valid layout filename
 */
gchar *irreco_create_uniq_layout_filename(const gchar* filename)
{
	gint i;

	IRRECO_ENTER

	/* Check existing file */
	if(strlen(filename) > 11) {
		gchar *fname;
		fname = irreco_get_config_file("irreco",
						filename);
		if(irreco_file_exists(fname)) {
			IRRECO_DEBUG("Valid and existing\n");
			g_free(fname);
			IRRECO_RETURN_PTR(g_strdup(filename));
		} else {
			g_free(fname);
			/* Should never happen */
			/* Conf removed while using irreco */
		}
	}

	/* Create new layout[n].conf filename that doesn't already exist */
	for(i=1; i<100; i++) {
		gchar *fname = g_malloc0(sizeof(gchar)*25);
		gchar *fullfname;
		g_sprintf(fname, "layout%d.conf", i);
		fullfname = irreco_get_config_file("irreco", fname);
		if(irreco_file_exists(fullfname)) {
			/* File exists */
			g_free(fullfname);
			g_free(fname);
		} else {
			IRRECO_DEBUG("Created uniq name: %s\n", fname);
			g_free(fullfname);
			IRRECO_RETURN_PTR(fname);
		}
	}

	IRRECO_RETURN_PTR(NULL)
}

/**
 * Removes layout*.conf files, exept those on Glist
 * Frees gchar* type data from list, not the list itself
 */
gboolean irreco_remove_layouts_exept_glist(GList *list)
{
	IrrecoDirForeachData dir_data;

	IRRECO_ENTER

	dir_data.directory = irreco_get_config_dir("irreco");
	dir_data.filesuffix = ".conf";
	dir_data.user_data_1 = list;

	irreco_dir_foreach(&dir_data, irreco_remove_layouts);

	/* Release data in glist */
	list = g_list_first(list);
	while(list) {
		g_free(list->data);
		list = list->next;
	}

	/* TODO get return value from somewhere */
	IRRECO_RETURN_BOOL(TRUE)
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Strings.                                                                   */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Strings
 * @{
 */

/**
 * Replace characters.
 */
void irreco_char_replace(gchar * string, gchar what, gchar with)
{
	gint i;
	IRRECO_ENTER
	for (i = 0; string[i] != '\0'; i++) {
		if (string[i] == what) {
			string[i] = with;
		}
	}
	IRRECO_RETURN
}

/**
 * Find first occurrence of character inside string.
 *
 * @return Position of character or -1.
 */
gint irreco_char_pos(const gchar * string, gchar what)
{
	gint i;
	IRRECO_ENTER

	for (i = 0; string[i] != '\0'; i++) {
		if (string[i] == what) {
			IRRECO_RETURN_INT(i);
		}
	}
	IRRECO_RETURN_INT(-1);
}

/**
 * Check if the string is empty.
 *
 * String is empty if it is:
 * @li NULL
 * @li ""
 * @li Full of space characters.
 */
gboolean irreco_str_isempty(const gchar * string)
{
	IRRECO_ENTER

	if (string == NULL || string[0] == '\0') IRRECO_RETURN_BOOL(TRUE);
	do {
		if (g_unichar_isspace(g_utf8_get_char(string)) == FALSE) {
			IRRECO_RETURN_BOOL(FALSE);
		}
		printf("\"%s\" %p\n", string, string);
	} while ((string = g_utf8_find_next_char(string, NULL)) != NULL
		&& string[0] != '\0');

	IRRECO_RETURN_BOOL(TRUE);
}

/**
 * Copy C-string into GString.
 */
void irreco_gstring_set(GString * g_str, const gchar * c_str)
{
	IRRECO_ENTER

	if (c_str == NULL) {
		g_string_assign(g_str, "");
	} else {
		g_string_assign(g_str, c_str);
	}

	IRRECO_RETURN
}

/**
 * Copy C-string into GString and free C-string.
 */
void irreco_gstring_set_and_free(GString * g_str, gchar * c_str)
{
	IRRECO_ENTER

	if (c_str == NULL) {
		g_string_assign(g_str, "");
	} else {
		g_string_assign(g_str, c_str);
		g_free(c_str);
	}

	IRRECO_RETURN
}

/** @} */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Error handling.                                                            */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Error handling
 * @{
 */


/**
 * If GError is set, print message, release GError, and return TRUE.
 * Otherwise returns FALSE.
 */
gboolean irreco_gerror_check_print(GError ** error)
{
	if (*error != NULL) {
		IRRECO_PRINTF("GError: %s\n", (*error)->message);
		g_error_free(*error);
		*error = NULL;
		return TRUE;
	}
	return FALSE;
}

/**
 * If GError is set, free it, and return TRUE.
 * Otherwise returns FALSE.
 */
gboolean irreco_gerror_check_free(GError ** error)
{
	if (*error != NULL) {
		g_error_free(*error);
		*error = NULL;
		return TRUE;
	}
	return FALSE;
}

/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* GTK.                                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name GTK
 * @{
 */

/**
 * Show info popup.
 */
void irreco_info_dlg(GtkWindow * parent_window, const gchar * message)
{
	GtkWidget* dialog;
	IRRECO_ENTER

	dialog = gtk_message_dialog_new(parent_window,
		GTK_DIALOG_DESTROY_WITH_PARENT,
		GTK_MESSAGE_INFO, GTK_BUTTONS_OK, "%s", message);
	gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);

	IRRECO_RETURN
}

/**
 * Show error popup.
 */
void irreco_error_dlg(GtkWindow * parent_window, const gchar * message)
{
	GtkWidget* dialog;
	IRRECO_ENTER

	dialog = gtk_message_dialog_new(parent_window,
		GTK_DIALOG_DESTROY_WITH_PARENT,
		GTK_MESSAGE_ERROR, GTK_BUTTONS_OK, "%s", message);
	gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);

	IRRECO_RETURN
}

/**
 * Format message, and show error popup.
 */
void irreco_info_dlg_printf(GtkWindow * parent_window,
			    const gchar * format, ...)
{
	gint rvalue;
	va_list args;
	gchar *message = NULL;
	IRRECO_ENTER

	va_start(args, format);
	rvalue = g_vasprintf(&message, format, args);
	va_end(args);

	if (rvalue > 0) {
		irreco_info_dlg(parent_window, message);
		g_free(message);
	} else {
		IRRECO_ERROR("Could not format message.\n");
	}

	IRRECO_RETURN
}

/**
 * Format message, and show error popup.
 */
void irreco_error_dlg_printf(GtkWindow * parent_window,
			     const gchar * format, ...)
{
	gint rvalue;
	va_list args;
	gchar *message = NULL;
	IRRECO_ENTER

	va_start(args, format);
	rvalue = g_vasprintf(&message, format, args);
	va_end(args);

	if (rvalue > 0) {
		irreco_error_dlg(parent_window, message);
		g_free(message);
	} else {
		IRRECO_ERROR("Could not format message.\n");
	}

	IRRECO_RETURN
}

/**
 * Popup yes / no dialog.
 *
 * Returns: TRUE if user click YES, FALSE otherwise.
 */
gboolean irreco_yes_no_dlg(GtkWindow * parent_window, const gchar * message)
{
	gint responce;
	GtkWidget* dialog;
	IRRECO_ENTER

	dialog = gtk_message_dialog_new(parent_window,
		GTK_DIALOG_DESTROY_WITH_PARENT, GTK_MESSAGE_QUESTION,
		GTK_BUTTONS_YES_NO, "%s", message);
	responce = gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);
	IRRECO_RETURN_BOOL(responce == GTK_RESPONSE_YES);
}

/**
 * Convenience frapper of gtk_alignment_new().
 */
GtkWidget *irreco_gtk_align(GtkWidget *child,
			    gfloat xalign,
			    gfloat yalign,
			    gfloat xscale,
			    gfloat yscale,
			    guint padding_top,
			    guint padding_bottom,
			    guint padding_left,
			    guint padding_right)
{
	GtkWidget *align;
	IRRECO_ENTER

	align = gtk_alignment_new(xalign, yalign, xscale, yscale);
	gtk_alignment_set_padding(GTK_ALIGNMENT(align), padding_top,
				  padding_bottom, padding_left, padding_right);
	gtk_container_add(GTK_CONTAINER(align), child);
	IRRECO_RETURN_PTR(align);
}

/**
 * Create, align, and pad label.
 */
GtkWidget *irreco_gtk_label(const gchar * str,
			    gfloat xalign,
			    gfloat yalign,
			    guint padding_top,
			    guint padding_bottom,
			    guint padding_left,
			    guint padding_right)
{
	GtkWidget *label;
	IRRECO_ENTER

	label = gtk_label_new(str);
	gtk_misc_set_alignment(GTK_MISC(label), xalign, yalign);
	IRRECO_RETURN_PTR(irreco_gtk_pad(label, padding_top, padding_bottom,
					 padding_left, padding_right));
}

/**
 * Create, align, and pad a label with bold text.
 */
GtkWidget *irreco_gtk_label_bold(const gchar * str,
				 gfloat xalign,
				 gfloat yalign,
				 guint padding_top,
				 guint padding_bottom,
				 guint padding_left,
				 guint padding_right)
{
	gchar* markup;
	GtkWidget *label;
	IRRECO_ENTER

	label = irreco_gtk_label(NULL, xalign, yalign, padding_top,
				 padding_bottom, padding_left, padding_right);
	markup = g_markup_printf_escaped("<b>%s</b>", str);
	gtk_label_set_markup(GTK_LABEL(gtk_bin_get_child(GTK_BIN(label))),
			     markup);
	g_free(markup);
	IRRECO_RETURN_PTR(label);
}

/**
 * Get button widget for dialog buttons.
 *
 * @param dialog	GtkDialog
 * @param n		Index of button to get.
 */
GtkWidget *irreco_gtk_dialog_get_button(GtkWidget *dialog, guint n)
{

	GtkBox *action_area;
	GtkBoxChild *box_child;
	guint length;
	IRRECO_ENTER

	action_area = GTK_BOX(GTK_DIALOG(dialog)->action_area);
	length = g_list_length(action_area->children);

	if (n >= length) {
		IRRECO_ERROR("Cant get button \"%i\". "
			     "Dialog has only \"%i\" buttons", n, length);
		IRRECO_RETURN_PTR(NULL);
	}

	box_child = (GtkBoxChild *) g_list_nth_data(g_list_first(
		action_area->children), n);
	IRRECO_RETURN_PTR(box_child->widget);
}

GtkWindow *irreco_gtk_get_parent_window(GtkWidget *widget)
{
	GtkWidget *parent;
	IRRECO_ENTER

	parent = gtk_widget_get_toplevel(widget);
	if (GTK_WIDGET_TOPLEVEL(parent) != TRUE
	    || GTK_IS_WINDOW(parent) != TRUE) {
		IRRECO_RETURN_PTR(GTK_WINDOW(parent));
	}
	IRRECO_RETURN_PTR(NULL);
}

/**
 * This function is a modified version of gtk_dialog_new_empty() from GTK
 * sources, which is used to by gtk_dialog_new_with_buttons() to set the
 * settings of a dialog.
 */
void irreco_gtk_dialog_set(GtkDialog *dialog,
			   const gchar *title,
			   GtkWindow *parent,
			   GtkDialogFlags flags)
{
	IRRECO_ENTER

	if (title)
		gtk_window_set_title(GTK_WINDOW (dialog), title);

	if (parent)
		gtk_window_set_transient_for(GTK_WINDOW (dialog), parent);

	if (flags & GTK_DIALOG_MODAL)
		gtk_window_set_modal(GTK_WINDOW (dialog), TRUE);

	if (flags & GTK_DIALOG_DESTROY_WITH_PARENT)
		gtk_window_set_destroy_with_parent(GTK_WINDOW (dialog), TRUE);

	if (flags & GTK_DIALOG_NO_SEPARATOR)
		gtk_dialog_set_has_separator(dialog, FALSE);

	IRRECO_RETURN
}

/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Time.                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Time
 * @{
 */


/**
 * Do not use, use GTimer instead.
 *
 * @deprecated
 *
 * Difference of time in microseconds between two GTimeVals.
 * This works fine as long as the difference is no more than 2146 seconds.
 */
glong irreco_time_diff(GTimeVal *start, GTimeVal *end)
{
	GTimeVal diff;
	IRRECO_ENTER

	diff.tv_sec = end->tv_sec - start->tv_sec;
	diff.tv_usec = end->tv_usec - start->tv_usec;

	/* We run out of space in ulong after 2147 seconds. */
	if (diff.tv_sec >= G_MAXLONG / IRRECO_SECOND_IN_USEC) {
		IRRECO_RETURN_LONG((G_MAXLONG / IRRECO_SECOND_IN_USEC)
				   * IRRECO_SECOND_IN_USEC);
	}

	IRRECO_RETURN_LONG(diff.tv_sec * IRRECO_SECOND_IN_USEC + diff.tv_usec);
}

/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Socket.                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Socket
 * @{
 */

/**
 * Check if the socket is valid.
 */
gboolean irreco_is_socket_valid(int socket)
{
	gint optval;
	socklen_t optlen;
	gint rvalue;
	IRRECO_ENTER

	/* This should succeed if socket is valid. */
	rvalue = getsockopt(socket, SOL_SOCKET, SOCK_STREAM, &optval, &optlen);

	if (rvalue == 0) {
		IRRECO_RETURN_BOOL(TRUE);
	} else {

		/* glibc docs say these are the possible error values. */
		switch (errno) {
		case EBADF: IRRECO_PRINTF("Error: EBADF\n"); break;
		case ENOTSOCK: IRRECO_PRINTF("Error: ENOTSOCK\n"); break;
		case ENOPROTOOPT: IRRECO_PRINTF("Error: ENOPROTOOPT\n"); break;
		default: IRRECO_PRINTF("Error: Unknown\n"); break;
		};

		IRRECO_RETURN_BOOL(FALSE);
	}

}
/** @} */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/**
 * @name Private
 * @{
 */

/**
 * Removes layout* files that are not in (GList) dir_data->user_data_1
 *
 */
void irreco_remove_layouts(IrrecoDirForeachData *dir_data)
{
	GList *list = NULL;
	gchar *rm_cmd;

	IRRECO_ENTER

	if(!g_str_has_prefix(dir_data->filename, "layout")) {
		IRRECO_DEBUG("Not layout file: %s\n", dir_data->filename);
		IRRECO_RETURN
	}

	list = g_list_first(dir_data->user_data_1);

	while(list) {
		IRRECO_DEBUG("file in list: %s file to remove: %s\n",
			     (gchar*) list->data,
			     dir_data->filename);
		if(strcmp((gchar*) list->data, dir_data->filename) == 0) {
			IRRECO_DEBUG("File in use, break\n");
			IRRECO_RETURN
		}
		list = list->next;
	}

	IRRECO_DEBUG("Removing unused conf: %s\n", dir_data->filename);

	/* Build remove command */
	rm_cmd = g_strconcat("rm -r ",
			     irreco_get_config_dir("irreco"),
			     "/",
			     dir_data->filename,
			     NULL);

	/* Execute remove */
	system(rm_cmd);

	g_free(rm_cmd);

	IRRECO_RETURN
}

/** @} */

/** @} */

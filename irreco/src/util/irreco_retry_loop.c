/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007,2008  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_retry_loop.h"

/**
 * @addtogroup IrrecoRetryLoop
 * @ingroup IrrecoUtil
 *
 * Loop for retrying operations that may fail sometimes.
 *
 * @sa IRRECO_RETRY_LOOP_START
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoRetryLoop.
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * Private data structure.
 */
struct _IrrecoRetryLoop {
	gint    recursion;	/* Counter of recursion. */
	gint    interval;	/* Time between retry attempts. */
	gint    timeout;	/* Max retry time. */

	gint    time_used;      /* How log has retry loop been running. */
	GTimer *timer;		/* Timer. */
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

/**
 * Create new IrrecoRetryLoop.
 *
 * @sa IRRECO_RETRY_LOOP_START
 * @param interval      Sleeping time in usec between retry attempts.
 * @param timeout       Timeout in usec. Retry loop will be broken if this
 *                      value is passed.
 */
IrrecoRetryLoop *irreco_retry_loop_new(gint interval, gint timeout)
{
	IrrecoRetryLoop *self;
	IRRECO_ENTER

	self = g_slice_new0(IrrecoRetryLoop);
	self->interval = interval;
	self->timeout = timeout;
	self->timer = g_timer_new();

	IRRECO_DEBUG("Interval: %i\n", interval);
	IRRECO_DEBUG("Timeout: %i\n", timeout);
	IRRECO_RETURN_PTR(self);
}

/**
 * @sa IRRECO_RETRY_LOOP_START
 */
void irreco_retry_loop_free(IrrecoRetryLoop *self)
{
	IRRECO_ENTER
	g_timer_destroy(self->timer);
	g_slice_free(IrrecoRetryLoop, self);
	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Functions                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */

/**
 * @sa IRRECO_RETRY_LOOP_START
 */
void irreco_retry_loop_enter(IrrecoRetryLoop *self)
{
	IRRECO_ENTER
	self->recursion = self->recursion + 1;
	if (self->recursion == 1) {
		self->time_used = 0;
		g_timer_start(self->timer);
	} else {
		IRRECO_DEBUG("Retry loop recursion: %i\n", self->recursion);
	}
	IRRECO_RETURN
}

/**
 * Check if loop should timeout.
 *
 * @sa IRRECO_RETRY_LOOP_START
 * @return TRUE on timeout, FALSE otherwise.
 */
gboolean irreco_retry_loop_timeout(IrrecoRetryLoop *self)
{
	IRRECO_ENTER
	self->time_used = IRRECO_SECONDS_TO_USEC(
		g_timer_elapsed(self->timer, NULL));
	IRRECO_PRINTF("Loop has been running for: %f seconds.\n",
		      IRRECO_USEC_TO_SECONDS(self->time_used));
	if (self->time_used >= self->timeout) IRRECO_RETURN_BOOL(TRUE);
	g_usleep(self->interval);
	IRRECO_RETURN_BOOL(FALSE);
}

/**
 * @sa IRRECO_RETRY_LOOP_START
 */
void irreco_retry_loop_leave(IrrecoRetryLoop *self)
{
	IRRECO_ENTER
	self->recursion = self->recursion - 1;
	if (self->recursion == 0) {
		self->time_used = IRRECO_SECONDS_TO_USEC(
			g_timer_elapsed(self->timer, NULL));
		g_timer_stop(self->timer);
		IRRECO_PRINTF("Time used: %f seconds.\n",
			      IRRECO_USEC_TO_SECONDS(self->time_used));
	}
	IRRECO_RETURN
}

/**
 * How long did the loop take to run or how log has the loop been runnig.
 *
 * @return Time used in usec.
 */
gint irreco_retry_loop_get_time_used(IrrecoRetryLoop *self)
{
	IRRECO_ENTER
	IRRECO_RETURN_INT(self->time_used);
}

/** @} */
/** @} */

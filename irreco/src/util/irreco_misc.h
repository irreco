/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi),
 * Harri Vattulainen (t5vaha01@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoMisc
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoMisc.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */

#ifndef __IRRECO_MISC_H_TYPEDEF__
typedef struct _IrrecoDirForeachData IrrecoDirForeachData;
typedef void (*IrrecoDirForeachCallback)(IrrecoDirForeachData * dir_data);
#define __IRRECO_MISC_H_TYPEDEF__

#endif /* __IRRECO_MISC_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_MISC_H__
#define __IRRECO_MISC_H__




/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/



/**
 * To be used with irreco_dir_foreach().
 */
struct _IrrecoDirForeachData {

	/* Set these before calling irreco_dir_foreach() */
	const gchar *directory;
	const gchar *filesuffix;

	/* irreco_dir_foreach() will set these on each iteration. */
	const gchar *filename;
	const gchar *filepath;

	/* For carrying whaterver user data the callback may need. */
	gpointer user_data_1;
	gpointer user_data_2;
	gpointer user_data_3;
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * Pad a widget.
 *
 * Calls irreco_gtk_align with default values which align the widget
 * to the left top corner.
 */
#define irreco_gtk_pad(_child, _padding_top, _padding_bottom, \
		       _padding_left, _padding_right) \
		irreco_gtk_align(_child, 0, 0, 1, 1, _padding_top, \
				 _padding_bottom, _padding_left, _padding_right)

/**
 * Microsecond, one millionth of a second, usec.
 *
 * @sa IRRECO_SECOND_IN_MSEC
 * @sa IRRECO_SECONDS_TO_USEC
 * @sa IRRECO_USEC_TO_SECONDS
 */
#define IRRECO_SECOND_IN_USEC 1000000

/**
 * Convert seconds to microseconds.
 *
 * @sa IRRECO_SECOND_IN_USEC
 */
#define IRRECO_SECONDS_TO_USEC(__sec) \
		((__sec) * IRRECO_SECOND_IN_USEC)

/**
 * Convert microseconds to seconds.
 *
 * @sa IRRECO_SECOND_IN_USEC
 * @return float
 */
#define IRRECO_USEC_TO_SECONDS(__sec)	\
		((float)(__sec) / (float) IRRECO_SECOND_IN_USEC)

/**
 * Millisecond, one thousandth of a second, msec.
 *
 * @sa IRRECO_SECOND_IN_USEC
 * @sa IRRECO_SECONDS_TO_MSEC
 * @sa IRRECO_MSEC_TO_SECONDS
 */
#define IRRECO_SECOND_IN_MSEC 1000

/**
 * Convert seconds to milliseconds.
 *
 * @sa IRRECO_SECOND_IN_MSEC
 */
#define IRRECO_SECONDS_TO_MSEC(__sec) \
		((__sec) * IRRECO_SECOND_IN_MSEC)

/**
 * Convert milliseconds to seconds.
 *
 * @sa IRRECO_SECOND_IN_MSEC
 * @return float
 */
#define IRRECO_MSEC_TO_SECONDS(__sec) \
		((float)(__sec) / (float) IRRECO_SECOND_IN_MSEC)



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
int irreco_is_dir(const char *filename);
int irreco_is_file(const char *filename);
int irreco_file_exists(const char *filename);
gint irreco_file_length(const gchar *filename);
gboolean irreco_write_file(const gchar * file, const gchar * data,
			   gsize data_size);
gboolean irreco_read_text_file(const gchar * file, gchar *buffer,
			       gsize buffer_size);
gboolean irreco_read_binary_file(const gchar * file, guchar *buffer,
				 gsize buffer_size, gint *binlen);
gboolean irreco_read_line(const gchar * file, gchar *buffer,
			  gsize buffer_size);
gboolean irreco_write_keyfile(GKeyFile * keyfile, const gchar * file);
gboolean irreco_dir_foreach(IrrecoDirForeachData *dir_data,
			 IrrecoDirForeachCallback callback);
gboolean irreco_dir_foreach_subdirectories(IrrecoDirForeachData *dir_data,
					 IrrecoDirForeachCallback callback);
gchar* irreco_get_config_dir(const gchar * app_name);
gchar* irreco_get_config_file(const gchar * app_name, const gchar * file);
gchar *irreco_create_uniq_layout_filename(const gchar* filename);
gboolean irreco_remove_layouts_exept_glist(GList *list);

void irreco_char_replace(gchar * string, gchar what, gchar with);
gint irreco_char_pos(const gchar * string, gchar what);
gboolean irreco_str_isempty(const gchar * string);
void irreco_gstring_set(GString * g_str, const gchar * c_str);
void irreco_gstring_set_and_free(GString * g_str, gchar * c_str);

gboolean irreco_gerror_check_print(GError ** error);
gboolean irreco_gerror_check_free(GError ** error);

void irreco_info_dlg(GtkWindow * parent_window, const gchar * message);
void irreco_error_dlg(GtkWindow * parent_window, const gchar * message);
void irreco_info_dlg_printf(GtkWindow * parent_window,
			    const gchar * format, ...) G_GNUC_PRINTF (2, 3);
void irreco_error_dlg_printf(GtkWindow * parent_window,
			     const gchar * format, ...) G_GNUC_PRINTF (2, 3);
gboolean irreco_yes_no_dlg(GtkWindow * parent_window, const gchar * message);
GtkWidget *irreco_gtk_align(GtkWidget *child,
			    gfloat xalign,
			    gfloat yalign,
			    gfloat xscale,
			    gfloat yscale,
			    guint padding_top,
			    guint padding_bottom,
			    guint padding_left,
			    guint padding_right);
GtkWidget *irreco_gtk_label(const gchar * str,
			    gfloat xalign,
			    gfloat yalign,
			    guint padding_top,
			    guint padding_bottom,
			    guint padding_left,
			    guint padding_right);
GtkWidget *irreco_gtk_label_bold(const gchar * str,
				 gfloat xalign,
				 gfloat yalign,
				 guint padding_top,
				 guint padding_bottom,
				 guint padding_left,
				 guint padding_right);
GtkWidget *irreco_gtk_dialog_get_button(GtkWidget *dialog, guint n);
GtkWindow *irreco_gtk_get_parent_window(GtkWidget *widget);
void irreco_gtk_dialog_set(GtkDialog *dialog,
			   const gchar *title,
			   GtkWindow *parent,
			   GtkDialogFlags flags);
glong irreco_time_diff(GTimeVal *start, GTimeVal *end);
gboolean irreco_is_socket_valid(int socket);







#endif /* __IRRECO_MISC_H__ */

/** @} */

/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007,2008  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoRetryLoop
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoRetryLoop.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_RETRY_LOOP_H_TYPEDEF__
#define __IRRECO_RETRY_LOOP_H_TYPEDEF__
typedef struct _IrrecoRetryLoop IrrecoRetryLoop;
#endif /* __IRRECO_RETRY_LOOP_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_RETRY_LOOP_H__
#define __IRRECO_RETRY_LOOP_H__
#include "irreco_util.h"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * Start IrrecoRetryLoop
 *
 * IrrecoRetryLoop will execute the code inside start and end macros until the
 * action succeeds and break is called, or timeout happens. Timeout is cheked
 * after the calls inside IrrecoRetryLoop, so the stuff inside IrrecoRetryLoop
 * will always be executed atleast once.
 *
 * The main intended usage of IrrecoRetryLoop is to sandwich different network
 * calls, so that the user of Irreco will not be bothered by random failures,
 * or network errors.
 *
 * IrrecoRetryLoop is also supports recurssion, so two loops inside one another
 * can use the same IrrecoRetryLoop object without the danger of timeout being
 * ignored.
 *
 * Example:
 * @code
 * gboolean success;
 * IrrecoRetryLoop *loop;
 * loop = irreco_retry_loop_new(IRRECO_SECONDS_TO_USEC(0.1),
 *                              IRRECO_SECONDS_TO_USEC(3));
 *
 * IRRECO_RETRY_LOOP_START(loop)
 *         success = some_function();
 *         if (success) break;
 * IRRECO_RETRY_LOOP_END(loop)
 *
 * if (success) {
 *         IRRECO_PRINTF("Success!!\n");
 * } else {
 *         IRRECO_PRINTF("Failure!!\n");
 * }
 *
 * irreco_retry_loop_free(loop);
 * @endcode
 *
 * @sa irreco_retry_loop_new
 * @sa irreco_retry_loop_free
 * @sa IRRECO_SECOND_IN_USEC
 * @param _self IrrecoRetryLoop object.
 */
#define IRRECO_RETRY_LOOP_START(_self) \
	irreco_retry_loop_enter(_self); \
	do {

/**
 * @sa IRRECO_RETRY_LOOP_START
 */
#define IRRECO_RETRY_LOOP_END(_self) \
	} while (!irreco_retry_loop_timeout(_self)); \
	irreco_retry_loop_leave(_self);



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
IrrecoRetryLoop *irreco_retry_loop_new(gint interval, gint timeout);
void irreco_retry_loop_free(IrrecoRetryLoop *self);
void irreco_retry_loop_enter(IrrecoRetryLoop *self);
gboolean irreco_retry_loop_timeout(IrrecoRetryLoop *self);
void irreco_retry_loop_leave(IrrecoRetryLoop *self);
gint irreco_retry_loop_get_time_used(IrrecoRetryLoop *self);



#endif /* __IRRECO_RETRY_LOOP_H__ */

/** @} */

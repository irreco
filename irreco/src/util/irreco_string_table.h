/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoStringTable
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoStringTable.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_STRING_TABLE_H_TYPEDEF__
#define __IRRECO_STRING_TABLE_H_TYPEDEF__
typedef struct _IrrecoStringTable IrrecoStringTable;
typedef struct _IrrecoStringTableItem IrrecoStringTableItem;
#endif /* __IRRECO_STRING_TABLE_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_STRING_TABLE_H__
#define __IRRECO_STRING_TABLE_H__
#include "irreco_util.h"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
typedef void (*IrrecoKeySetNotify)(gpointer data, const gchar *key);
#define IRRECO_KEY_SET_NOTIFY(f) ((IrrecoKeySetNotify) (f))

struct _IrrecoStringTable {
	GList			*list;

	/* Called when data pointer is removed from the table.*/
	GDestroyNotify		data_destroy_func;

	/* Called when data pointer is assosiated with a key. */
	IrrecoKeySetNotify	key_set_func;
};

struct _IrrecoStringTableItem {
	gchar *key;		/* Copy of key string.       */
	gchar *collate_key;	/* Used for sorting strings. */
	guint hash;		/* Used for fast searching.  */
	gpointer data;		/* And user data, ofcourse.  */
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * Check if the table is empty.
 *
 * @return TRUE if empty, FALSE otherwise.
 */
#define irreco_string_table_is_empty(str_table) \
	((str_table)->list == NULL)

/**
 * Macro for looping trough IrrecoStringTable.
 *
 * Items are returned in the order they are stored in the internal GList.
 * Use irreco_string_table_sort_abc() before foreach macro to get items in
 * alphabetical order.
 *
 * Example:
 * @code
 * IRRECO_STRING_TABLE_FOREACH(table, key, void *, data)
 * 	g_printf("%s %p\n", key, data);
 * IRRECO_STRING_TABLE_FOREACH_END
 * @endcode
 *
 * @sa IRRECO_STRING_TABLE_FOREACH_DATA
 * @sa IRRECO_STRING_TABLE_FOREACH_KEY
 *
 * @param _str_table      Pointer to IrrecoStringTable.
 * @param _key_var_name   Name of key string variable.
 * @param _data_var_type  Create a variable of this type for the data.
 * @param _data_var_name  Name of data variable.
 */
#define IRRECO_STRING_TABLE_FOREACH(_str_table, _key_var_name,		       \
				    _data_var_type, _data_var_name)	       \
	{ gpointer _foreach_pos = NULL;					       \
	const gchar * _key_var_name = NULL;				       \
	_data_var_type _data_var_name = NULL;				       \
	while (irreco_string_table_foreach(				       \
		_str_table, &_foreach_pos, &_key_var_name, 		       \
		(gpointer *) &_data_var_name)) {
/*
#define IRRECO_STRING_TABLE_FOREACH(_str_table, _key_var_name,		       \
				    _data_var_type, _data_var_name)	       \
	{ GList *_list;							       \
	IrrecoStringTableItem *_item;					       \
	_data_var_type _data_var_name;					       \
	const gchar * _key_var_name;					       \
	if ((_list = g_list_first(_str_table->list)) != NULL) do {	       \
		_item = (IrrecoStringTableItem *) _list->data;		       \
		_data_var_name = (_data_var_type) _item->data;		       \
		_key_var_name = _item->key; {
*/




/**
 * Macro for looping trough data stored in IrrecoStringTable.
 *
 * Example:
 * @code
 * IRRECO_STRING_TABLE_FOREACH_DATA(table, void *, data)
 * 	g_printf("%s %p\n", key, data);
 * IRRECO_STRING_TABLE_FOREACH_END
 * @endcode
 *
 * @sa IRRECO_STRING_TABLE_FOREACH for more information.
 * @param _str_table      Pointer to IrrecoStringTable.
 * @param _data_var_type  Create a variable of this type for the data.
 * @param _data_var_name  Name of data variable.
 */
#define IRRECO_STRING_TABLE_FOREACH_DATA(_str_table, _data_var_type,	       \
					 _data_var_name)		       \
	{ gpointer _foreach_pos = NULL;					       \
	_data_var_type _data_var_name = NULL;				       \
	while (irreco_string_table_foreach(				       \
		_str_table, &_foreach_pos, NULL, 			       \
		(gpointer *) &_data_var_name)) {
/*
#define IRRECO_STRING_TABLE_FOREACH_DATA(_str_table, _data_var_type,	       \
					 _data_var_name)		       \
	{ GList *_list;							       \
	IrrecoStringTableItem *_item;					       \
	_data_var_type _data_var_name;					       \
	if ((_list = g_list_first(_str_table->list)) != NULL) do {	       \
		_item = (IrrecoStringTableItem *) _list->data;		       \
		_data_var_name = (_data_var_type) _item->data; {
*/

/**
 * Macro for looping trough keys of data in IrrecoStringTable.
 *
 * Example:
 * @code
 * IRRECO_STRING_TABLE_FOREACH_KEY(table, key)
 * 	g_printf("%s %p\n", key, data);
 * IRRECO_STRING_TABLE_FOREACH_END
 * @endcode
 *
 * @sa IRRECO_STRING_TABLE_FOREACH for more information.
 * @param _str_table      Pointer to IrrecoStringTable.
 * @param _key_var_name   Name of key string variable.
 */
#define IRRECO_STRING_TABLE_FOREACH_KEY(_str_table, _key_var_name)	       \
	{ gpointer _foreach_pos = NULL;					       \
	const gchar * _key_var_name = NULL;				       \
	while (irreco_string_table_foreach(				       \
		_str_table, &_foreach_pos, &_key_var_name, NULL)) {
/*
#define IRRECO_STRING_TABLE_FOREACH_KEY(_str_table, _key_var_name)	       \
	{ GList *_list;							       \
	IrrecoStringTableItem *_item;					       \
	const gchar * _key_var_name;					       \
	if ((_list = g_list_first(_str_table->list)) != NULL) do {	       \
		_item = (IrrecoStringTableItem *) _list->data;		       \
		_key_var_name = _item->key; {
*/

/**
 * @brief End of IrrecoStringTableItem foreach block.
 *
 * @sa IRRECO_STRING_TABLE_FOREACH for more information.
 */
#define IRRECO_STRING_TABLE_FOREACH_END }}
/*
#define IRRECO_STRING_TABLE_FOREACH_END					       \
	}} while ((_list = g_list_next(_list)) != NULL); }
*/


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
IrrecoStringTable* irreco_string_table_new(GDestroyNotify data_destroy_func,
					   IrrecoKeySetNotify key_set_func);
void irreco_string_table_free(IrrecoStringTable *self);
gboolean irreco_string_table_exists(IrrecoStringTable *self,
				    const gchar *key);
gboolean irreco_string_table_add(IrrecoStringTable *self,
				 const gchar *key,
				 gpointer data);
gboolean irreco_string_table_add_first(IrrecoStringTable *self,
				 const gchar *key,
				 gpointer data);
gboolean irreco_string_table_change_key(IrrecoStringTable *self,
					const gchar *old_key,
					const gchar *new_key);
gboolean irreco_string_table_change_data(IrrecoStringTable *self,
					 const gchar *key,
					 gpointer data);
gboolean irreco_string_table_steal_by_data(IrrecoStringTable *self,
					   gpointer data);
gboolean irreco_string_table_remove(IrrecoStringTable *self,
				    const gchar *key);
void irreco_string_table_remove_all(IrrecoStringTable *self);
gboolean irreco_string_table_remove_by_data(IrrecoStringTable *self,
					    gpointer data);
gpointer irreco_string_table_get_as_rvalue(IrrecoStringTable *self,
				 const gchar *key);
gboolean irreco_string_table_get(IrrecoStringTable *self,
				   const gchar *key,
				   gpointer *data);
gboolean irreco_string_table_index(IrrecoStringTable *self,
				   guint index,
				   const gchar **key,
				   gpointer *data);
guint irreco_string_table_lenght(IrrecoStringTable *self);
gboolean irreco_string_table_get_index(IrrecoStringTable *self,
				       gpointer data,
				       guint *index);
void irreco_string_table_sort_abc(IrrecoStringTable *self);
void irreco_string_table_sort_123(IrrecoStringTable *self);
void irreco_string_table_print(IrrecoStringTable *self);
gboolean irreco_string_table_foreach(IrrecoStringTable *self, gpointer *pos,
				     const gchar **key, gpointer *data);

#endif /* __IRRECO_STRING_TABLE_H__ */

/** @} */

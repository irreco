/*
 * irreco - Ir Remote Control
 * Copyright (C) 2008  Harri Vattulainen (t5vaha01@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <config.h>
#include <string.h>
#include "glibconfig.h"
#include "irreco_sha1.h"
#include "glib.h"

#define IS_VALID_TYPE(type)     ((type) == G_CHECKSUM_SHA1)

static const gchar hex_digits[] = "0123456789abcdef";

#define SHA1_DATASIZE   64
#define SHA1_DIGEST_LEN 20

typedef struct
{
  guint32 buf[5];
  guint32 bits[2];

  /* we pack 64 unsigned chars into 16 32-bit unsigned integers */
  guint32 data[16];

  guchar digest[SHA1_DIGEST_LEN];
} Sha1sum;

struct _GChecksum
{
  GChecksumType type;

  gchar *digest_str;

  union {
    Sha1sum sha1;
  } sum;
};

/* we need different byte swapping functions because MD5 expects buffers
 * to be little-endian, while SHA1 and SHA256 expect them in big-endian
 * form.
 */

#if G_BYTE_ORDER == G_LITTLE_ENDIAN
#define md5_byte_reverse(buffer,length)
#else
/* assume that the passed buffer is integer aligned */
static inline void
md5_byte_reverse (guchar *buffer,
                  gulong  length)
{
  guint32 bit;

  do
    {
      bit = (guint32) ((unsigned) buffer[3] << 8 | buffer[2]) << 16 |
                      ((unsigned) buffer[1] << 8 | buffer[0]);
      * (guint32 *) buffer = bit;
      buffer += 4;
    }
  while (--length);
}
#endif /* G_BYTE_ORDER == G_LITTLE_ENDIAN */

#if G_BYTE_ORDER == G_BIG_ENDIAN
#define sha_byte_reverse(buffer,length)
#else
static inline void
sha_byte_reverse (guint32 *buffer,
                  gint     length)
{
  length /= sizeof (guint32);
  while (length--)
    {
      *buffer = ((guint32) (((*buffer & (guint32) 0x000000ffU) << 24) |
                            ((*buffer & (guint32) 0x0000ff00U) <<  8) |
                            ((*buffer & (guint32) 0x00ff0000U) >>  8) |
                            ((*buffer & (guint32) 0xff000000U) >> 24)));
      ++buffer;
    }
}
#endif /* G_BYTE_ORDER == G_BIG_ENDIAN */

static gchar *
digest_to_string (guint8 *digest,
                  gsize   digest_len)
{
  gint len = digest_len * 2;
  gint i;
  gchar *retval;

  retval = g_new (gchar, len + 1);

  for (i = 0; i < digest_len; i++)
    {
      guint8 byte = digest[i];

      retval[2 * i] = hex_digits[byte >> 4];
      retval[2 * i + 1] = hex_digits[byte & 0xf];
    }

  retval[len] = 0;

  return retval;
}



/*
 * SHA-1 Checksum
 */

/* The following comments have the history of where this code
 * comes from. I copied it from maemo dev SVN.
 * https://stage.maemo.org/svn/maemo/projects/haf/trunk/glib/glib/
 * I also removed md5 and sha256 functions.
 * - Harri Vattulainen
 */

/* The following implementation comes from D-Bus dbus-sha.c. I've changed
 * it to use GLib types and to work more like the MD5 implementation above.
 * I left the comments to have an history of this code.
 *      -- Emmanuele Bassi, ebassi@gnome.org
 */

/* The following comments have the history of where this code
 * comes from. I actually copied it from GNet in GNOME CVS.
 * - hp@redhat.com
 */

/*
 *  sha.h : Implementation of the Secure Hash Algorithm
 *
 * Part of the Python Cryptography Toolkit, version 1.0.0
 *
 * Copyright (C) 1995, A.M. Kuchling
 *
 * Distribute and use freely; there are no restrictions on further
 * dissemination and usage except those imposed by the laws of your
 * country of residence.
 *
 */

/* SHA: NIST's Secure Hash Algorithm */

/* Based on SHA code originally posted to sci.crypt by Peter Gutmann
   in message <30ajo5$oe8@ccu2.auckland.ac.nz>.
   Modified to test for endianness on creation of SHA objects by AMK.
   Also, the original specification of SHA was found to have a weakness
   by NSA/NIST.  This code implements the fixed version of SHA.
*/

/* Here's the first paragraph of Peter Gutmann's posting:

The following is my SHA (FIPS 180) code updated to allow use of the "fixed"
SHA, thanks to Jim Gillogly and an anonymous contributor for the information on
what's changed in the new version.  The fix is a simple change which involves
adding a single rotate in the initial expansion function.  It is unknown
whether this is an optimal solution to the problem which was discovered in the
SHA or whether it's simply a bandaid which fixes the problem with a minimum of
effort (for example the reengineering of a great many Capstone chips).
*/

static void
sha1_sum_init (Sha1sum *sha1)
{
  /* initialize constants */
  sha1->buf[0] = 0x67452301L;
  sha1->buf[1] = 0xEFCDAB89L;
  sha1->buf[2] = 0x98BADCFEL;
  sha1->buf[3] = 0x10325476L;
  sha1->buf[4] = 0xC3D2E1F0L;

  /* initialize bits */
  sha1->bits[0] = sha1->bits[1] = 0;
}

/* The SHA f()-functions. */

#define f1(x,y,z)       (z ^ (x & (y ^ z)))             /* Rounds  0-19 */
#define f2(x,y,z)       (x ^ y ^ z)                     /* Rounds 20-39 */
#define f3(x,y,z)       (( x & y) | (z & (x | y)))      /* Rounds 40-59 */
#define f4(x,y,z)       (x ^ y ^ z)                     /* Rounds 60-79 */

/* The SHA Mysterious Constants */
#define K1  0x5A827999L                                 /* Rounds  0-19 */
#define K2  0x6ED9EBA1L                                 /* Rounds 20-39 */
#define K3  0x8F1BBCDCL                                 /* Rounds 40-59 */
#define K4  0xCA62C1D6L                                 /* Rounds 60-79 */

/* 32-bit rotate left - kludged with shifts */
#define ROTL(n,X) (((X) << n ) | ((X) >> (32 - n)))

/* The initial expanding function.  The hash function is defined over an
   80-word expanded input array W, where the first 16 are copies of the input
   data, and the remaining 64 are defined by

        W[ i ] = W[ i - 16 ] ^ W[ i - 14 ] ^ W[ i - 8 ] ^ W[ i - 3 ]

   This implementation generates these values on the fly in a circular
   buffer - thanks to Colin Plumb, colin@nyx10.cs.du.edu for this
   optimization.

   The updated SHA changes the expanding function by adding a rotate of 1
   bit.  Thanks to Jim Gillogly, jim@rand.org, and an anonymous contributor
   for this information */

#define expand(W,i) (W[ i & 15 ] = ROTL (1, (W[ i       & 15] ^ \
                                             W[(i - 14) & 15] ^ \
                                             W[(i -  8) & 15] ^ \
                                             W[(i -  3) & 15])))


/* The prototype SHA sub-round.  The fundamental sub-round is:

        a' = e + ROTL( 5, a ) + f( b, c, d ) + k + data;
        b' = a;
        c' = ROTL( 30, b );
        d' = c;
        e' = d;

   but this is implemented by unrolling the loop 5 times and renaming the
   variables ( e, a, b, c, d ) = ( a', b', c', d', e' ) each iteration.
   This code is then replicated 20 times for each of the 4 functions, using
   the next 20 values from the W[] array each time */

#define subRound(a, b, c, d, e, f, k, data) \
   (e += ROTL (5, a) + f(b, c, d) + k + data, b = ROTL (30, b))

static void
sha1_transform (guint32  buf[5],
                guint32  in[16])
{
  guint32 A, B, C, D, E;

  A = buf[0];
  B = buf[1];
  C = buf[2];
  D = buf[3];
  E = buf[4];

  /* Heavy mangling, in 4 sub-rounds of 20 interations each. */
  subRound (A, B, C, D, E, f1, K1, in[0]);
  subRound (E, A, B, C, D, f1, K1, in[1]);
  subRound (D, E, A, B, C, f1, K1, in[2]);
  subRound (C, D, E, A, B, f1, K1, in[3]);
  subRound (B, C, D, E, A, f1, K1, in[4]);
  subRound (A, B, C, D, E, f1, K1, in[5]);
  subRound (E, A, B, C, D, f1, K1, in[6]);
  subRound (D, E, A, B, C, f1, K1, in[7]);
  subRound (C, D, E, A, B, f1, K1, in[8]);
  subRound (B, C, D, E, A, f1, K1, in[9]);
  subRound (A, B, C, D, E, f1, K1, in[10]);
  subRound (E, A, B, C, D, f1, K1, in[11]);
  subRound (D, E, A, B, C, f1, K1, in[12]);
  subRound (C, D, E, A, B, f1, K1, in[13]);
  subRound (B, C, D, E, A, f1, K1, in[14]);
  subRound (A, B, C, D, E, f1, K1, in[15]);
  subRound (E, A, B, C, D, f1, K1, expand (in, 16));
  subRound (D, E, A, B, C, f1, K1, expand (in, 17));
  subRound (C, D, E, A, B, f1, K1, expand (in, 18));
  subRound (B, C, D, E, A, f1, K1, expand (in, 19));

  subRound (A, B, C, D, E, f2, K2, expand (in, 20));
  subRound (E, A, B, C, D, f2, K2, expand (in, 21));
  subRound (D, E, A, B, C, f2, K2, expand (in, 22));
  subRound (C, D, E, A, B, f2, K2, expand (in, 23));
  subRound (B, C, D, E, A, f2, K2, expand (in, 24));
  subRound (A, B, C, D, E, f2, K2, expand (in, 25));
  subRound (E, A, B, C, D, f2, K2, expand (in, 26));
  subRound (D, E, A, B, C, f2, K2, expand (in, 27));
  subRound (C, D, E, A, B, f2, K2, expand (in, 28));
  subRound (B, C, D, E, A, f2, K2, expand (in, 29));
  subRound (A, B, C, D, E, f2, K2, expand (in, 30));
  subRound (E, A, B, C, D, f2, K2, expand (in, 31));
  subRound (D, E, A, B, C, f2, K2, expand (in, 32));
  subRound (C, D, E, A, B, f2, K2, expand (in, 33));
  subRound (B, C, D, E, A, f2, K2, expand (in, 34));
  subRound (A, B, C, D, E, f2, K2, expand (in, 35));
  subRound (E, A, B, C, D, f2, K2, expand (in, 36));
  subRound (D, E, A, B, C, f2, K2, expand (in, 37));
  subRound (C, D, E, A, B, f2, K2, expand (in, 38));
  subRound (B, C, D, E, A, f2, K2, expand (in, 39));

  subRound (A, B, C, D, E, f3, K3, expand (in, 40));
  subRound (E, A, B, C, D, f3, K3, expand (in, 41));
  subRound (D, E, A, B, C, f3, K3, expand (in, 42));
  subRound (C, D, E, A, B, f3, K3, expand (in, 43));
  subRound (B, C, D, E, A, f3, K3, expand (in, 44));
  subRound (A, B, C, D, E, f3, K3, expand (in, 45));
  subRound (E, A, B, C, D, f3, K3, expand (in, 46));
  subRound (D, E, A, B, C, f3, K3, expand (in, 47));
  subRound (C, D, E, A, B, f3, K3, expand (in, 48));
  subRound (B, C, D, E, A, f3, K3, expand (in, 49));
  subRound (A, B, C, D, E, f3, K3, expand (in, 50));
  subRound (E, A, B, C, D, f3, K3, expand (in, 51));
  subRound (D, E, A, B, C, f3, K3, expand (in, 52));
  subRound (C, D, E, A, B, f3, K3, expand (in, 53));
  subRound (B, C, D, E, A, f3, K3, expand (in, 54));
  subRound (A, B, C, D, E, f3, K3, expand (in, 55));
  subRound (E, A, B, C, D, f3, K3, expand (in, 56));
  subRound (D, E, A, B, C, f3, K3, expand (in, 57));
  subRound (C, D, E, A, B, f3, K3, expand (in, 58));
  subRound (B, C, D, E, A, f3, K3, expand (in, 59));

  subRound (A, B, C, D, E, f4, K4, expand (in, 60));
  subRound (E, A, B, C, D, f4, K4, expand (in, 61));
  subRound (D, E, A, B, C, f4, K4, expand (in, 62));
  subRound (C, D, E, A, B, f4, K4, expand (in, 63));
  subRound (B, C, D, E, A, f4, K4, expand (in, 64));
  subRound (A, B, C, D, E, f4, K4, expand (in, 65));
  subRound (E, A, B, C, D, f4, K4, expand (in, 66));
  subRound (D, E, A, B, C, f4, K4, expand (in, 67));
  subRound (C, D, E, A, B, f4, K4, expand (in, 68));
  subRound (B, C, D, E, A, f4, K4, expand (in, 69));
  subRound (A, B, C, D, E, f4, K4, expand (in, 70));
  subRound (E, A, B, C, D, f4, K4, expand (in, 71));
  subRound (D, E, A, B, C, f4, K4, expand (in, 72));
  subRound (C, D, E, A, B, f4, K4, expand (in, 73));
  subRound (B, C, D, E, A, f4, K4, expand (in, 74));
  subRound (A, B, C, D, E, f4, K4, expand (in, 75));
  subRound (E, A, B, C, D, f4, K4, expand (in, 76));
  subRound (D, E, A, B, C, f4, K4, expand (in, 77));
  subRound (C, D, E, A, B, f4, K4, expand (in, 78));
  subRound (B, C, D, E, A, f4, K4, expand (in, 79));

  /* Build message digest */
  buf[0] += A;
  buf[1] += B;
  buf[2] += C;
  buf[3] += D;
  buf[4] += E;
}

#undef K1
#undef K2
#undef K3
#undef K4
#undef f1
#undef f2
#undef f3
#undef f4
#undef ROTL
#undef expand
#undef subRound

static void
sha1_sum_update (Sha1sum      *sha1,
                 const guchar *buffer,
                 gsize         count)
{
  guint32 tmp;
  guint dataCount;

  /* Update bitcount */
  tmp = sha1->bits[0];
  if ((sha1->bits[0] = tmp + ((guint32) count << 3) ) < tmp)
    sha1->bits[1] += 1;             /* Carry from low to high */
  sha1->bits[1] += count >> 29;

  /* Get count of bytes already in data */
  dataCount = (guint) (tmp >> 3) & 0x3F;

  /* Handle any leading odd-sized chunks */
  if (dataCount)
    {
      guchar *p = (guchar *) sha1->data + dataCount;

      dataCount = SHA1_DATASIZE - dataCount;
      if (count < dataCount)
        {
          memcpy (p, buffer, count);
          return;
        }

      memcpy (p, buffer, dataCount);

      sha_byte_reverse (sha1->data, SHA1_DATASIZE);
      sha1_transform (sha1->buf, sha1->data);

      buffer += dataCount;
      count -= dataCount;
    }

  /* Process data in SHA1_DATASIZE chunks */
  while (count >= SHA1_DATASIZE)
    {
      memcpy (sha1->data, buffer, SHA1_DATASIZE);

      sha_byte_reverse (sha1->data, SHA1_DATASIZE);
      sha1_transform (sha1->buf, sha1->data);

      buffer += SHA1_DATASIZE;
      count -= SHA1_DATASIZE;
    }

  /* Handle any remaining bytes of data. */
  memcpy (sha1->data, buffer, count);
}

/* Final wrapup - pad to SHA_DATASIZE-byte boundary with the bit pattern
   1 0* (64-bit count of bits processed, MSB-first) */
static void
sha1_sum_close (Sha1sum *sha1)
{
  gint count;
  guchar *data_p;

  /* Compute number of bytes mod 64 */
  count = (gint) ((sha1->bits[0] >> 3) & 0x3f);

  /* Set the first char of padding to 0x80.  This is safe since there is
     always at least one byte free */
  data_p = (guchar *) sha1->data + count;
  *data_p++ = 0x80;

  /* Bytes of padding needed to make 64 bytes */
  count = SHA1_DATASIZE - 1 - count;

  /* Pad out to 56 mod 64 */
  if (count < 8)
    {
      /* Two lots of padding:  Pad the first block to 64 bytes */
      memset (data_p, 0, count);

      sha_byte_reverse (sha1->data, SHA1_DATASIZE);
      sha1_transform (sha1->buf, sha1->data);

      /* Now fill the next block with 56 bytes */
      memset (sha1->data, 0, SHA1_DATASIZE - 8);
    }
  else
    {
      /* Pad block to 56 bytes */
      memset (data_p, 0, count - 8);
    }

  /* Append length in bits and transform */
  sha1->data[14] = sha1->bits[1];
  sha1->data[15] = sha1->bits[0];

  sha_byte_reverse (sha1->data, SHA1_DATASIZE - 8);
  sha1_transform (sha1->buf, sha1->data);
  sha_byte_reverse (sha1->buf, SHA1_DIGEST_LEN);

  memcpy (sha1->digest, sha1->buf, SHA1_DIGEST_LEN);

  /* Reset buffers in case they contain sensitive data */
  memset (sha1->buf, 0, sizeof (sha1->buf));
  memset (sha1->data, 0, sizeof (sha1->data));
}

static gchar *
sha1_sum_to_string (Sha1sum *sha1)
{
  return digest_to_string (sha1->digest, SHA1_DIGEST_LEN);
}

static void
sha1_sum_digest (Sha1sum *sha1,
                 guint8  *digest)
{
  gint i;

  for (i = 0; i < SHA1_DIGEST_LEN; i++)
    digest[i] = sha1->digest[i];
}



/*
 * Public API
 */

/**
 * g_checksum_type_get_length:
 * @checksum_type: a #GChecksumType
 *
 * Gets the length in bytes of digests of type @checksum_type
 *
 * Return value: the checksum length, or -1 if @checksum_type is
 * not supported.
 *
 * Since: 2.16
 */
gssize sha_checksum_type_get_length (GChecksumType checksum_type)
{
  gssize len = -1;

	len = SHA1_DIGEST_LEN;

  return len;
}

/**
 * g_checksum_new:
 * @checksum_type: the desired type of checksum
 *
 * Creates a new #GChecksum, using the checksum algorithm @checksum_type.
 * If the @checksum_type is not known, %NULL is returned.
 * A #GChecksum can be used to compute the checksum, or digest, of an
 * arbitrary binary blob, using different hashing algorithms.
 *
 * A #GChecksum works by feeding a binary blob through g_checksum_update()
 * until there is data to be checked; the digest can then be extracted
 * using g_checksum_get_string(), which will return the checksum as a
 * hexadecimal string; or g_checksum_get_digest(), which will return a
 * vector of raw bytes. Once either g_checksum_get_string() or
 * g_checksum_get_digest() have been called on a #GChecksum, the checksum
 * will be closed and it won't be possible to call g_checksum_update()
 * on it anymore.
 *
 * Return value: the newly created #GChecksum, or %NULL.
 *   Use g_checksum_free() to free the memory allocated by it.
 *
 * Since: 2.16
 */
GChecksum *sha_checksum_new (GChecksumType checksum_type)
{
  GChecksum *checksum;

  if (! IS_VALID_TYPE (checksum_type))
    return NULL;

  checksum = g_slice_new0 (GChecksum);
  checksum->type = checksum_type;

      sha1_sum_init (&(checksum->sum.sha1));

  return checksum;
}

/**
 * g_checksum_copy:
 * @checksum: the #GChecksum to copy
 *
 * Copies a #GChecksum. If @checksum has been closed, by calling
 * g_checksum_get_string() or g_checksum_get_digest(), the copied
 * checksum will be closed as well.
 *
 * Return value: the copy of the passed #GChecksum. Use g_checksum_free()
 *   when finished using it.
 *
 * Since: 2.16
 */
GChecksum *sha_checksum_copy (const GChecksum *checksum)
{
  GChecksum *copy;

  g_return_val_if_fail (checksum != NULL, NULL);

  copy = g_slice_new (GChecksum);
  *copy = *checksum;

  copy->digest_str = g_strdup (checksum->digest_str);

  return copy;
}

/**
 * g_checksum_free:
 * @checksum: a #GChecksum
 *
 * Frees the memory allocated for @checksum.
 *
 * Since: 2.16
 */
void sha_checksum_free (GChecksum *checksum)
{
  if (G_LIKELY (checksum))
    {
      g_free (checksum->digest_str);

      g_slice_free (GChecksum, checksum);
    }
}

/**
 * g_checksum_update:
 * @checksum: a #GChecksum
 * @data: buffer used to compute the checksum
 * @length: size of the buffer, or -1 if it is a null-terminated string.
 *
 * Feeds @data into an existing #GChecksum. The checksum must still be
 * open, that is g_checksum_get_string() or g_checksum_get_digest() must
 * not have been called on @checksum.
 *
 * Since: 2.16
 */
void sha_checksum_update (GChecksum    *checksum,
			const guchar *data,
			gssize        length)
{
  g_return_if_fail (checksum != NULL);
  g_return_if_fail (data != NULL);

  /* TODO FIXME */
/*
  if (length < 0)
    length = strlen (data);
  */


  if (checksum->digest_str)
    {
      g_warning ("The checksum `%s' has been closed and cannot be updated "
                 "anymore.",
                 checksum->digest_str);
      return;
    }

      sha1_sum_update (&(checksum->sum.sha1), data, length);
}

/**
 * g_checksum_get_string:
 * @checksum: a #GChecksum
 *
 * Gets the digest as an hexadecimal string.
 *
 * Once this function has been called the #GChecksum can no longer be
 * updated with g_checksum_update().
 *
 * Return value: the hexadecimal representation of the checksum. The
 *   returned string is owned by the checksum and should not be modified
 *   or freed.
 *
 * Since: 2.16
 */
G_CONST_RETURN gchar *sha_checksum_get_string (GChecksum *checksum)
{
  gchar *str = NULL;

  g_return_val_if_fail (checksum != NULL, NULL);

  if (checksum->digest_str)
    return checksum->digest_str;

      sha1_sum_close (&(checksum->sum.sha1));
      str = sha1_sum_to_string (&(checksum->sum.sha1));

  checksum->digest_str = str;

  return checksum->digest_str;
}

/**
 * g_checksum_get_digest:
 * @checksum: a #GChecksum
 * @buffer: output buffer
 * @digest_len: an inout parameter. The caller initializes it to the size of @buffer.
 *   After the call it contains the length of the digest.
 *
 * Gets the digest from @checksum as a raw binary vector and places it
 * into @buffer. The size of the digest depends on the type of checksum.
 *
 * Once this function has been called, the #GChecksum is closed and can
 * no longer be updated with g_checksum_update().
 *
 * Since: 2.16
 */
void sha_checksum_get_digest (GChecksum  *checksum,
				guint8     *buffer,
				gsize      *digest_len)
{
  gboolean checksum_open = FALSE;
  gchar *str = NULL;
  gsize len;

  g_return_if_fail (checksum != NULL);

  len = sha_checksum_type_get_length (checksum->type);
  g_return_if_fail (*digest_len >= len);

  checksum_open = !!(checksum->digest_str == NULL);

      if (checksum_open)
        {
          sha1_sum_close (&(checksum->sum.sha1));
          str = sha1_sum_to_string (&(checksum->sum.sha1));
        }
      sha1_sum_digest (&(checksum->sum.sha1), buffer);


  if (str)
    checksum->digest_str = str;

  *digest_len = len;
}

/**
 * g_compute_checksum_for_data:
 * @checksum_type: a #GChecksumType
 * @data: binary blob to compute the digest of
 * @length: length of @data
 *
 * Computes the checksum for a binary @data of @length. This is a
 * convenience wrapper for g_checksum_new(), g_checksum_get_string()
 * and g_checksum_free().
 *
 * Return value: the digest of the binary data as a string in hexadecimal.
 *   The returned string should be freed with g_free() when done using it.
 *
 * Since: 2.16
 */
gchar *sha_compute_checksum_for_data (GChecksumType  checksum_type,
				      const guchar  *data,
				      gsize length)
{
  GChecksum *checksum;
  gchar *retval;

  g_return_val_if_fail (IS_VALID_TYPE (checksum_type), NULL);
  g_return_val_if_fail (data != NULL, NULL);
  g_return_val_if_fail (length > 1, NULL);

  checksum = sha_checksum_new (checksum_type);
  if (!checksum)
    return NULL;

  sha_checksum_update (checksum, data, length);
  retval = g_strdup (sha_checksum_get_string (checksum));
  sha_checksum_free (checksum);

  return retval;
}

/**
 * g_compute_checksum_for_string:
 * @checksum_type: a #GChecksumType
 * @str: the string to compute the checksum of
 * @length: the length of the string, or -1 if the string is null-terminated.
 *
 * Computes the checksum of a string.
 *
 * Return value: the checksum as a hexadecimal string. The returned string
 *   should be freed with g_free() when done using it.
 *
 * Since: 2.16
 */
gchar *sha_compute_checksum_for_string (GChecksumType  checksum_type,
                               const gchar   *str,
                               gssize         length)
{
  g_return_val_if_fail (IS_VALID_TYPE (checksum_type), NULL);
  g_return_val_if_fail (str != NULL, NULL);

  if (length < 0)
    length = strlen (str);

  return sha_compute_checksum_for_data (checksum_type,
					(const guchar *) str,
					length);
}

#define __G_CHECKSUM_C__

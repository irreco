/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_keyfile.h"

/**
 * @addtogroup IrrecoKeyFile
 * @ingroup IrrecoUtil
 *
 * @todo PURPOCE OF CLASS.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoKeyFile.
 */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* GKeyFile utitilities                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name GKeyFile utitilities
 * @{
 */

/**
 * Write GKeyFile to file.
 */
gboolean irreco_gkeyfile_write_to_file(GKeyFile * keyfile,
				       const gchar * file)
{
	gchar *data;
	gsize data_size;
	GError *error = NULL;
	gboolean success;
	IRRECO_ENTER

	data = g_key_file_to_data(keyfile, &data_size, &error);
	if (irreco_gerror_check_print(&error)) {
		IRRECO_RETURN_BOOL(FALSE);
	}

	success = irreco_write_file(file, data, data_size);
	g_free(data);
	IRRECO_RETURN_BOOL(success);
}

/**
 * Write GKeyFile to $HOME/.APP_NAME/FILE
 */
gboolean irreco_gkeyfile_write_to_config_file(GKeyFile * keyfile,
					      const gchar * app_name,
					      const gchar * file)
{
	gboolean success;
	gchar *config_file;
	IRRECO_ENTER

	config_file = irreco_get_config_file(app_name, file);
	if (config_file == NULL) IRRECO_RETURN_BOOL(FALSE);

	success = irreco_gkeyfile_write_to_file(keyfile, config_file);
	g_free(config_file);
	IRRECO_RETURN_BOOL(success);
}

/**
 * Like g_key_file_set_string(), but instead of failing on NULL string pointer,
 * writes empty string instead.
 */
void irreco_gkeyfile_set_string(GKeyFile * keyfile,
				const gchar * group,
				const gchar * key,
				const gchar * string)
{
	IRRECO_ENTER
	if (string == NULL) {
		g_key_file_set_string(keyfile, group, key, "");
	} else {
		g_key_file_set_string(keyfile, group, key, string);
	}
	IRRECO_RETURN
}

/**
 * Save unsigned integer value to GKeyFile.
 */
void irreco_gkeyfile_set_guint(GKeyFile * keyfile,
			       const gchar * group,
			       const gchar * key,
			       guint value)
{
	GString *string;
	IRRECO_ENTER

	string = g_string_new(NULL);
	g_string_printf(string, "%u", value);
	g_key_file_set_string(keyfile, group, key, string->str);
	g_string_free(string, TRUE);
	IRRECO_RETURN
}

/**
 * Save long signed interger value to GkeyFile
 */
void irreco_gkeyfile_set_glong(GKeyFile * keyfile,
			       const gchar * group,
			       const gchar * key,
			       glong value)
{
	GString *string;
	IRRECO_ENTER

	string = g_string_new(NULL);
	g_string_printf(string, "%li", value);
	g_key_file_set_string(keyfile, group, key, string->str);
	g_string_free(string, TRUE);
	IRRECO_RETURN
}


/**
 * Save float value to GKeyFile
 */
void irreco_gkeyfile_set_gfloat(GKeyFile *keyfile,
				const gchar *group,
				const gchar *key,
				gfloat value)
{
	GString *string;
	IRRECO_ENTER

	string = g_string_new(NULL);
	g_string_printf(string, "%f", value);
	g_key_file_set_string(keyfile, group, key, string->str);
	g_string_free(string, TRUE);

	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* IrrecoKeyFile                                                              */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name IrrecoKeyFile
 * @{
 */

/**
 * @typedef IrrecoKeyFile
 *
 * Frapper around GKeyFile. Main point is to check GError's inside these
 * frappers which makes them easier to use than plain GKeyFile.
 *
 * Also success is indicated by return value, instead of GError object,
 * which makes it easier to use these functions inside \c if statements.
 *
 * Get functions always read values from current group, use
 * irreco_keyfile_set_group() to change the current group.
 */


/**
 * Create new IrrecoKeyFile.
 *
 * This function DOES NOT make copies of input strings, so keep them around.
 */
IrrecoKeyFile *irreco_keyfile_create(const gchar *dir, const gchar *file,
				     const gchar *group)
{
	GKeyFile *g_keyfile;
	GError *error = NULL;
	IrrecoKeyFile *irreco_keyfile;
	IRRECO_ENTER


	if (dir == NULL || file == NULL) IRRECO_RETURN_PTR(NULL);

	IRRECO_DEBUG("Opening keyfile \"%s\"\n", file);
	g_keyfile = g_key_file_new();
	g_key_file_load_from_file(g_keyfile, file, 0, &error);

	if (irreco_gerror_check_print(&error)) {
		g_key_file_free(g_keyfile);
		IRRECO_RETURN_PTR(NULL);
	}

	if (group != NULL) {
		if (!g_key_file_has_group(g_keyfile, group)) {
			IRRECO_PRINTF("Group \"%s\" does not exist.\n",
				      group);
			g_key_file_free(g_keyfile);
			IRRECO_RETURN_PTR(NULL);
		}
	}

	irreco_keyfile = g_slice_new0(IrrecoKeyFile);
	irreco_keyfile->file = file;
	irreco_keyfile->dir = dir;
	irreco_keyfile->keyfile = g_keyfile;
	irreco_keyfile->group = group;
	IRRECO_RETURN_PTR(irreco_keyfile);
}

void irreco_keyfile_destroy(IrrecoKeyFile* keyfile)
{
	IRRECO_ENTER
	if (keyfile == NULL) IRRECO_RETURN
	g_key_file_free(keyfile->keyfile);
	g_slice_free(IrrecoKeyFile, keyfile);
	IRRECO_RETURN
}

gboolean irreco_keyfile_set_group(IrrecoKeyFile* keyfile, const gchar *group)
{
	IRRECO_ENTER
	if (!g_key_file_has_group(keyfile->keyfile, group)) {
		IRRECO_RETURN_BOOL(FALSE);
	} else {
		keyfile->group = group;
		IRRECO_RETURN_BOOL(TRUE);
	}
}

const gchar *irreco_keyfile_get_group(IrrecoKeyFile* keyfile)
{
	IRRECO_ENTER
	IRRECO_RETURN_CONST_STR(keyfile->group);
}

/*
gboolean irreco_keyfile_get_keys(IrrecoKeyFile* keyfile, const gchar *group, gsize **length, gchar ***keys)
{
	char **keys;
	gsize *length;
	GError *error = NULL;


	keys = g_key_file_get_keys(keyfile->keyfile, group,
				   length,&error);

	IRRECO_ENTER
	if (!g_key_file_has_group(keyfile->keyfile, group)) {
		IRRECO_RETURN_BOOL(FALSE);
	} else {
		keyfile->group = group;
		IRRECO_RETURN_BOOL(TRUE);
	}
}
*/
gboolean irreco_keyfile_get_str(IrrecoKeyFile* keyfile, const gchar *key,
				gchar ** value)
{
	gchar *string;
	GError *error = NULL;
	IRRECO_ENTER

	string = g_key_file_get_string(keyfile->keyfile, keyfile->group,
				       key, &error);
	if (irreco_gerror_check_print(&error)) {
		IRRECO_RETURN_BOOL(FALSE);
	} else {
		*value = string;
		IRRECO_RETURN_BOOL(TRUE);
	}
}

gboolean irreco_keyfile_get_path(IrrecoKeyFile* keyfile, const gchar *key,
				 gchar ** value)
{
	gchar *string;
	GError *error = NULL;
	IRRECO_ENTER

	string = g_key_file_get_string(keyfile->keyfile, keyfile->group,
				       key, &error);
	if (irreco_gerror_check_print(&error)) {
		IRRECO_RETURN_BOOL(FALSE);
	} else {
		*value = g_build_path("/", keyfile->dir, string, NULL);
		g_free(string);
		IRRECO_RETURN_BOOL(TRUE);
	}
}

gboolean irreco_keyfile_get_int(IrrecoKeyFile* keyfile, const gchar *key,
				gint * value)
{
	gint integer;
	GError *error = NULL;
	IRRECO_ENTER

	integer = g_key_file_get_integer(keyfile->keyfile, keyfile->group,
					 key, &error);
	if (irreco_gerror_check_print(&error)) {
		IRRECO_RETURN_BOOL(FALSE);
	} else {
		*value = integer;
		IRRECO_RETURN_BOOL(TRUE);
	}
}

gboolean irreco_keyfile_get_uint(IrrecoKeyFile* keyfile, const gchar *key,
				 guint * value)
{
	gchar *str_val;
	gint rvalue;
	IRRECO_ENTER

	if (irreco_keyfile_get_str(keyfile, key, &str_val)) {
		rvalue = sscanf(str_val, "%u", value);
		if(rvalue) {
			g_free(str_val);
			IRRECO_RETURN_BOOL(TRUE);
		}

		IRRECO_ERROR("Failed to parse uint value from string \"%s\".\n",
			     str_val);
		g_free(str_val);
	}
	IRRECO_RETURN_BOOL(FALSE);
}

/*
gboolean irreco_keyfile_get_uint(IrrecoKeyFile* keyfile, const gchar *key,
				 guint * value)
{
	gint int_val;
	IRRECO_ENTER

	if (irreco_keyfile_get_int(keyfile, key, &int_val)) {
		if (int_val >= 0) {
			*value = int_val;
			IRRECO_RETURN_BOOL(TRUE);
		}
		IRRECO_ERROR("Integer value \"%i\" for key \"%s\" "
			     "is out of bounds of uint.", int_val, key);
	}
	IRRECO_RETURN_BOOL(FALSE);
}
*/

gboolean irreco_keyfile_get_uint16(IrrecoKeyFile* keyfile, const gchar *key,
				   guint16 * value)
{
	gint int_val;
	IRRECO_ENTER

	if (irreco_keyfile_get_int(keyfile, key, &int_val)) {
		if (int_val >= 0 && int_val <= 0xFFFF) {
			*value = int_val;
			IRRECO_RETURN_BOOL(TRUE);
		}
		IRRECO_ERROR("Integer value \"%i\" for key \"%s\" "
			     "is out of bounds of uint16.", int_val, key);
	}
	IRRECO_RETURN_BOOL(FALSE);
}

gboolean irreco_keyfile_get_double(IrrecoKeyFile* keyfile, const gchar *key,
				   gdouble * value)
{
	gdouble doublee;
	GError *error = NULL;
	IRRECO_ENTER

	doublee = g_key_file_get_double(keyfile->keyfile, keyfile->group,
					key, &error);
	if (irreco_gerror_check_print(&error)) {
		IRRECO_RETURN_BOOL(FALSE);
	} else {
		*value = doublee;
		IRRECO_RETURN_BOOL(TRUE);
	}

	IRRECO_RETURN_BOOL(FALSE);
}

gboolean irreco_keyfile_get_float(IrrecoKeyFile* keyfile, const gchar *key,
				  gfloat * value)
{
	gdouble doublee;
	GError *error = NULL;
	IRRECO_ENTER

	doublee = g_key_file_get_double(keyfile->keyfile, keyfile->group,
					key, &error);
	if (irreco_gerror_check_print(&error)) {
		IRRECO_RETURN_BOOL(FALSE);
	} else {
		*value = doublee;
		IRRECO_RETURN_BOOL(TRUE);
	}

	IRRECO_RETURN_BOOL(FALSE);
}

gboolean irreco_keyfile_get_glong(IrrecoKeyFile* keyfile, const gchar *key,
				 glong * value)
{
	gchar *str_val;
	gint rvalue;
	IRRECO_ENTER

	if (irreco_keyfile_get_str(keyfile, key, &str_val)) {
		rvalue = sscanf(str_val, "%li", value);
		if(rvalue) {
			g_free(str_val);
			IRRECO_RETURN_BOOL(TRUE);
		}

		IRRECO_ERROR("Failed to parse glong value from string \"%s\".\n",
			     str_val);
		g_free(str_val);
	}
	IRRECO_RETURN_BOOL(FALSE);
}

gboolean irreco_keyfile_get_bool(IrrecoKeyFile* keyfile, const gchar *key,
				 gboolean * value)
{
	gboolean boolean;
	GError *error = NULL;
	IRRECO_ENTER

	boolean = g_key_file_get_boolean(keyfile->keyfile, keyfile->group,
					 key, &error);
	if (irreco_gerror_check_print(&error)) {
		IRRECO_RETURN_BOOL(FALSE);
	} else {
		*value = boolean;
		IRRECO_RETURN_BOOL(TRUE);
	}
}
gboolean irreco_keyfile_get_gkeyfile(IrrecoKeyFile* keyfile, GKeyFile **gkeyfile)
{
	GKeyFile *key;
	IRRECO_ENTER
	if( keyfile->keyfile == NULL){

		IRRECO_RETURN_BOOL(FALSE);
	}else{


		key = keyfile->keyfile;
		*gkeyfile = key;
		IRRECO_RETURN_BOOL(TRUE);

	}

}
/** @} */
/** @} */

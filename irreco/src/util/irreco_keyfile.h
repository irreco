/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoKeyFile
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoKeyFile.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_KEYFILE_H_TYPEDEF__
#define __IRRECO_KEYFILE_H_TYPEDEF__
typedef struct _IrrecoKeyFile IrrecoKeyFile;
#endif /* __IRRECO_KEYFILE_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_KEYFILE_H__
#define __IRRECO_KEYFILE_H__
#include "irreco_util.h"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
struct _IrrecoKeyFile {
	const gchar *file;	/* Path to keyfile. */
	const gchar *dir;	/* Directory where the keyfile is located. */
	GKeyFile *keyfile;
	const gchar *group;	/* Group where to read values.*/
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
gboolean irreco_gkeyfile_write_to_file(GKeyFile * keyfile,
				       const gchar * file);
gboolean irreco_gkeyfile_write_to_config_file(GKeyFile * keyfile,
					      const gchar * app_name,
					      const gchar * file);
void irreco_gkeyfile_set_string(GKeyFile * keyfile,
				const gchar * group,
				const gchar * key,
				const gchar * string);
void irreco_gkeyfile_set_guint(GKeyFile * keyfile,
			       const gchar * group,
			       const gchar * key,
			       guint value);
void irreco_gkeyfile_set_glong(GKeyFile * keyfile,
			       const gchar * group,
			       const gchar * key,
			       glong value);
void irreco_gkeyfile_set_gfloat(GKeyFile *keyfile,
				const gchar *group,
				const gchar *key,
				gfloat value);
IrrecoKeyFile *irreco_keyfile_create(const gchar *dir, const gchar *file,
				     const gchar *group);
void irreco_keyfile_destroy(IrrecoKeyFile* keyfile);
gboolean irreco_keyfile_set_group(IrrecoKeyFile* keyfile, const gchar *group);
const gchar *irreco_keyfile_get_group(IrrecoKeyFile* keyfile);
gboolean irreco_keyfile_get_str(IrrecoKeyFile* keyfile, const gchar *key,
				gchar ** value);
gboolean irreco_keyfile_get_path(IrrecoKeyFile* keyfile, const gchar *key,
				 gchar ** value);
gboolean irreco_keyfile_get_int(IrrecoKeyFile* keyfile, const gchar *key,
				gint * value);
gboolean irreco_keyfile_get_uint(IrrecoKeyFile* keyfile, const gchar *key,
				 guint * value);
gboolean irreco_keyfile_get_uint16(IrrecoKeyFile* keyfile, const gchar *key,
				   guint16 * value);
gboolean irreco_keyfile_get_glong(IrrecoKeyFile* keyfile, const gchar *key,
				  glong * value);
gboolean irreco_keyfile_get_double(IrrecoKeyFile* keyfile, const gchar *key,
				   gdouble * value);
gboolean irreco_keyfile_get_float(IrrecoKeyFile* keyfile, const gchar *key,
				  gfloat * value);
gboolean irreco_keyfile_get_bool(IrrecoKeyFile* keyfile, const gchar *key,
				 gboolean * value);
/*
gboolean irreco_keyfile_get_keys(IrrecoKeyFile* keyfile, const gchar *group, gsize **length, gchar ***keys);
*/
gboolean irreco_keyfile_get_gkeyfile(IrrecoKeyFile* keyfile, GKeyFile **gkeyfile);
#endif /* __IRRECO_KEYFILE_H__ */

/** @} */



/*
 * irreco - Ir Remote Control
 * Copyright (C) 2008 Joni Kokko (t5kojo01@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_backend_file_container.h"

/**
 * @addtogroup IrrecoBackendFileContainer
 * @ingroup IrrecoUtil
 *
 * @todo PURPOCE OF CLASS.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoBackendFileContainer.
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

IrrecoBackendFileContainer *irreco_backend_file_container_new()
{
	IrrecoBackendFileContainer *self;
	IRRECO_ENTER

	self = g_slice_new0(IrrecoBackendFileContainer);

	self->backend		= g_string_new("");
	self->category		= g_string_new("");
	self->manufacturer	= g_string_new("");
	self->model		= g_string_new("");
	self->hash		= g_string_new("");
	self->name		= g_string_new("");
	self->data		= g_string_new("");

	IRRECO_RETURN_PTR(self);
}

void irreco_backend_file_container_free(IrrecoBackendFileContainer *self)
{
	IRRECO_ENTER

	g_assert(self != NULL);

	g_string_free(self->backend, TRUE);
	self->backend = NULL;

	g_string_free(self->category, TRUE);
	self->category = NULL;

	g_string_free(self->manufacturer, TRUE);
	self->manufacturer = NULL;

	g_string_free(self->model, TRUE);
	self->model = NULL;

	g_string_free(self->hash, TRUE);
	self->hash = NULL;

	g_string_free(self->name, TRUE);
	self->name = NULL;

	g_string_free(self->data, TRUE);
	self->data = NULL;

	g_slice_free(IrrecoBackendFileContainer, self);
	self = NULL;

	IRRECO_RETURN
}

/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Functions                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Private Functions
 * @{
 */

void irreco_backend_file_container_set(IrrecoBackendFileContainer *self,
				       const char *backend,
				       const char *category,
				       const char *manufacturer,
				       const char *model,
				       const char *file_name,
				       const char *file_data)
{
	const char *file_hash;
	IRRECO_ENTER

	if (file_data != NULL) {
		file_hash = g_compute_checksum_for_string(G_CHECKSUM_SHA1,
							    file_data,
							    strlen(file_data));
		g_string_printf(self->hash,"%s", file_hash);
		g_string_printf(self->data,"%s", file_data);
	}

	if (backend != NULL) g_string_printf(self->backend,"%s", backend);
	if (category != NULL) g_string_printf(self->category,"%s", category);
	if (manufacturer != NULL) g_string_printf(self->manufacturer,"%s",
	    					  manufacturer);
	if (model != NULL) g_string_printf(self->model,"%s", model);
	if (file_name != NULL) g_string_printf(self->name,"%s", file_name);

	IRRECO_RETURN
}

void irreco_backend_file_container_set_model(IrrecoBackendFileContainer *self,
					     const char *model)
{
	IRRECO_ENTER

	g_string_printf(self->model,"%s", model);

	IRRECO_RETURN
}

/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Events and Callbacks                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/** @} */

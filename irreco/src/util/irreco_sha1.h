/*
 * irreco - Ir Remote Control
 * Copyright (C) 2008  Harri Vattulainen (t5vaha01@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef __G_CHECKSUM_H__
#define __G_CHECKSUM_H__

#include <glib/gtypes.h>

G_BEGIN_DECLS

/**
 * GChecksumType:
 * @G_CHECKSUM_SHA1: Use the SHA-1 hashing algorithm
 *
 * The hashing algorithm to be used by #GChecksum when performing the
 * digest of some data.
 *
 * Note that the #GChecksumType enumeration may be extended at a later
 * date to include new hashing algorithm types.
 *
 * Since: 2.16
 */
typedef enum {
  G_CHECKSUM_SHA1
} GChecksumType;

typedef struct _GChecksum       GChecksum;

gssize                sha_checksum_type_get_length (GChecksumType checksum_type);

GChecksum *           sha_checksum_new        (GChecksumType     checksum_type);
GChecksum *           sha_checksum_copy       (const GChecksum  *checksum);
void                  sha_checksum_free       (GChecksum        *checksum);
void                  sha_checksum_update     (GChecksum        *checksum,
                                             const guchar     *data,
                                             gssize            length);
G_CONST_RETURN gchar *sha_checksum_get_string (GChecksum        *checksum);
void                  sha_checksum_get_digest (GChecksum        *checksum,
                                             guint8           *buffer,
                                             gsize            *digest_len);

gchar *sha_compute_checksum_for_data   (GChecksumType  checksum_type,
                                      const guchar  *data,
                                      gsize          length);
gchar *sha_compute_checksum_for_string (GChecksumType  checksum_type,
                                      const gchar   *str,
                                      gssize         length);

G_END_DECLS

#endif /* __G_CHECKSUM_H__ */

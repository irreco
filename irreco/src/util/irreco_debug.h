/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

 /**
 * @addtogroup IrrecoDebug
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoDebug.
 */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_DEBUG_H__
#define __IRRECO_DEBUG_H__
#include "irreco_util.h"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Debug variables.                                                           */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

#ifdef IRRECO_UTIL_BUILD
#define IRRECO_DEBUG_PREFIX "UTIL"
#endif

#ifndef IRRECO_DEBUG_PREFIX
#error "IRRECO_DEBUG_PREFIX not set"
#endif

#ifndef IRRECO_DEBUG_LEVEL_VAR
#define IRRECO_DEBUG_LEVEL_VAR irreco_util_debug_level
#endif

#ifndef IRRECO_DEBUG_INDENT_VAR
#define IRRECO_DEBUG_INDENT_VAR irreco_util_debug_indent
#endif

extern gint IRRECO_DEBUG_LEVEL_VAR;
extern gint IRRECO_DEBUG_INDENT_VAR;

#ifdef DEBUG
	#ifndef IRRECO_DEBUG_LEVEL
		#define IRRECO_DEBUG_LEVEL 99
	#endif
#else
	#ifndef IRRECO_DEBUG_LEVEL
		#define IRRECO_DEBUG_LEVEL 1
	#endif
#endif

#ifndef IRRECO_DEBUG_INDENT_WIDTH
#define IRRECO_DEBUG_INDENT_WIDTH 1
#endif


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Debug macros.                                                              */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * For outputting messages with different debug levels. Output is also indented
 * to match the indent of enter / exit macros.
 *
 * Example:
 * IRRECO_PRINTF("Print this to console");
 */
enum {
	IRRECO_DEBUG_LEVEL_ALWAYS = 0,
	IRRECO_DEBUG_LEVEL_PRINTF,
	IRRECO_DEBUG_LEVEL_DEBUG,
	IRRECO_DEBUG_LEVEL_FUNCTION
};

#define IRRECO_OUTPUT(_level, _prefix) \
	if (_level <= IRRECO_DEBUG_LEVEL_VAR && \
		irreco_debug_prefix(&IRRECO_DEBUG_INDENT_VAR, \
				    IRRECO_DEBUG_PREFIX, _prefix))

/**
 * Print message to STDOUT with debug level 0.
 *
 * @code
 * IRRECO_ERROR("Failure: value of int is %i\n", i);
 * @endcode
 */
#define IRRECO_ERROR \
	IRRECO_OUTPUT(IRRECO_DEBUG_LEVEL_ALWAYS, "!  Error: ") \
		irreco_debug_print

/**
 * Print message to STDOUT with debug level 1.
 *
 * @code
 * IRRECO_PRINTF("Value of int is %i\n", i);
 * @endcode
 */
#define IRRECO_PRINTF \
	IRRECO_OUTPUT(IRRECO_DEBUG_LEVEL_PRINTF, "   ") irreco_debug_print

/**
 * Print message to STDOUT with debug level 2.
 *
 * @code
 * IRRECO_DEBUG("Value of int is %i\n", i);
 * @endcode
 */
#define IRRECO_DEBUG \
	IRRECO_OUTPUT(IRRECO_DEBUG_LEVEL_DEBUG, "++ ") irreco_debug_print

/**
 * Print filename and line number to STDOUT.
 */
#define IRRECO_LINE \
	IRRECO_OUTPUT(IRRECO_DEBUG_LEVEL_ALWAYS, "@@ ") \
		irreco_debug_print("%s : %i\n", __FILE__, __LINE__);
#define IRRECO_DEBUG_LINE IRRECO_LINE

/**
 * Print line, and wait for user to press enter.
 */
#define IRRECO_PAUSE IRRECO_DEBUG_PAUSE
#define IRRECO_DEBUG_PAUSE \
	IRRECO_DEBUG_LINE; getchar();




/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Function enter / exit tracking macros.                                     */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/*
 * These macros can be used to track how the program travels through different
 * functions. In order for the indentation to work properly every place where
 * the function can be entred and exited must be frapped with these macros.
 *
 * This is because irreco counts the every time a function is entered or exited.
 * if this counter is not properly synchronized then the log will not properly
 * indented, and will not properly show where the debugging output is coming
 * from.
 *
 * Also all IRRECO_RETURN_* macros will print the value which the function is
 * returning, which is often helpfull while debugging.
 *
 * Example:
 * gboolean test(int arg) {
 *         IRRECO_ENTER
 *         if (arg == 123) {
 *                 IRRECO_RETURN_BOOL(TRUE);
 *         } else {
 *                 IRRECO_RETURN_BOOL(FALSE);
 *         }
 * }
 *
 */

#define IRRECO_ENTER \
	IRRECO_OUTPUT(IRRECO_DEBUG_LEVEL_FUNCTION, "-> ") \
		irreco_debug_print("%s\n", __func__); \
	IRRECO_DEBUG_INDENT_VAR++;

#define IRRECO_RETURN \
	{ IRRECO_DEBUG_INDENT_VAR--; \
	IRRECO_OUTPUT(IRRECO_DEBUG_LEVEL_FUNCTION, "<- ") \
		irreco_debug_print("%s\n", __func__); \
	return; }

#define IRRECO_RETURN_BOOL(__value) IRRECO_RETURN_INT(__value)
#define IRRECO_RETURN_INT(__value) \
	{ gboolean __int = __value; \
	IRRECO_DEBUG_INDENT_VAR--; \
	IRRECO_OUTPUT(IRRECO_DEBUG_LEVEL_FUNCTION, "<- ") \
		irreco_debug_print("%s %i\n", __func__, __int); \
	return __int; }

#define IRRECO_RETURN_UINT(__value) \
	{ gboolean __uint = __value; \
	IRRECO_DEBUG_INDENT_VAR--; \
	IRRECO_OUTPUT(IRRECO_DEBUG_LEVEL_FUNCTION, "<- ") \
		irreco_debug_print("%s %i\n", __func__, __uint); \
	return __uint; }

#define IRRECO_RETURN_LONG(__value) \
	{ gboolean __long = __value; \
	IRRECO_DEBUG_INDENT_VAR--; \
	IRRECO_OUTPUT(IRRECO_DEBUG_LEVEL_FUNCTION, "<- ") \
		irreco_debug_print("%s %i\n", __func__, __long); \
	return __long; }

#define IRRECO_RETURN_ULONG(__value) \
	{ gboolean __ulong = __value; \
	IRRECO_DEBUG_INDENT_VAR--; \
	IRRECO_OUTPUT(IRRECO_DEBUG_LEVEL_FUNCTION, "<- ") \
		irreco_debug_print("%s %i\n", __func__, __ulong); \
	return __ulong; }

#define IRRECO_RETURN_PTR(__value) \
	{ gpointer __ptr = __value; \
	IRRECO_DEBUG_INDENT_VAR--; \
	IRRECO_OUTPUT(IRRECO_DEBUG_LEVEL_FUNCTION, "<- ") \
		irreco_debug_print("%s %p\n", __func__, __ptr); \
	return __ptr; }

#define IRRECO_RETURN_CONST_PTR(__value) \
	{ const gpointer __ptr = __value; \
	IRRECO_DEBUG_INDENT_VAR--; \
	IRRECO_OUTPUT(IRRECO_DEBUG_LEVEL_FUNCTION, "<- ") \
		irreco_debug_print("%s %p\n", __func__, __ptr); \
	return __ptr; }

#define IRRECO_RETURN_STR(__value) \
	{ gchar *__str = __value; \
	IRRECO_DEBUG_INDENT_VAR--; \
	IRRECO_OUTPUT(IRRECO_DEBUG_LEVEL_FUNCTION, "<- ")	\
		irreco_debug_print("%s \"%s\"\n", __func__, __str); \
	return __str; }

#define IRRECO_RETURN_CONST_STR(__value) \
	{ const gchar *__str = __value; \
	IRRECO_DEBUG_INDENT_VAR--; \
	IRRECO_OUTPUT(IRRECO_DEBUG_LEVEL_FUNCTION, "<- ")	\
		irreco_debug_print("%s \"%s\"\n", __func__, __str); \
	return __str; }

#define IRRECO_RETURN_ENUM(__value) \
	{ gboolean __int = __value; \
	IRRECO_DEBUG_INDENT_VAR--; \
	IRRECO_OUTPUT(IRRECO_DEBUG_LEVEL_FUNCTION, "<- ") \
		irreco_debug_print("%s %i %s\n", __func__, __int, #__value); \
	return __int; }



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
gint irreco_debug_prefix(gint * indent, gchar const * common_prefix,
			 gchar const * msg_prefix);
gint irreco_debug_print(gchar const *format, ...) G_GNUC_PRINTF (1, 2);



#endif /* __IRRECO_DEBUG_H__ */

/** @} */








/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_util.h"

/**
 * @addtogroup IrrecoDebug
 * @ingroup IrrecoUtil
 *
 * @todo PURPOCE OF CLASS.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoDebug.
 */


gint irreco_util_debug_level	= IRRECO_DEBUG_LEVEL;
gint irreco_util_debug_indent	= 0;



/**
 * Print message and flush STDOUT.
 */
gint irreco_debug_prefix(gint * indent, gchar const * common_prefix,
			 gchar const * msg_prefix)
{
	gint rvalue;
	gsize size;
	gchar *indent_buf;

	if (*indent < 0) {
		g_printf("ERROR: INDENT COUNTER BELOW ZERO!!\n");
		*indent = 0;
	}

	/* Increasing last number increases indentation */
	rvalue=(*indent) * IRRECO_DEBUG_INDENT_WIDTH;

	size = sizeof(char) * rvalue + 1;
	indent_buf = g_slice_alloc(size);
	memset(indent_buf, ' ', size - 1);
	indent_buf[size - 1] = '\0';

	rvalue = g_printf("%s%s%s", common_prefix, indent_buf, msg_prefix);

	g_slice_free1(size, indent_buf);
	return rvalue;
}

/**
 * Print message and flush STDOUT.
 */
gint irreco_debug_print(gchar const *format, ...)
{
	gint rvalue;
	va_list args;
	va_start(args, format);
	rvalue = g_vprintf(format, args);
	va_end(args);
	fflush(stdout);
	return rvalue;
}

/** @} */

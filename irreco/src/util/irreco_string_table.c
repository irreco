/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007,2008  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_string_table.h"

/**
 * @addtogroup IrrecoStringTable
 * @ingroup IrrecoUtil
 *
 * A bit like GHashArray, but uses strings as keys.
 *
 * @par Features:
 * @li Sortable. Something GHashArray is not.
 * @li Based on GList.
 * @li No hash collisions. Something that might be problem with GHashArray.
 * @li All Key / data pairs have also an Index location which can also be used
 *     to retrieve data.
 *
 * @par IrrecoStringTableItem
 *
 * Internal structure used by IrrecoStringTable. Each item stores one
 * key / data pair.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoStringTable.
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
static void irreco_string_table_item_set(IrrecoStringTableItem *item,
					 const gchar *key,
					 gpointer data);
static void irreco_string_table_item_clean(IrrecoStringTableItem *item);
static gint irreco_string_table_sort_abc_compare(gconstpointer a, gconstpointer b);
static gint irreco_string_table_sort_123_compare(gconstpointer a, gconstpointer b);



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

/**
 * Create new IrrecoStringTable.
 *
 * data_destroy_func is not called if data pointer is NULL.
 *
 * @param data_destroy_func  Callback to be called when key / data pair is
 *                           removed from IrrecoStringTable.
 * @param key_set_func       Callback to be called when key is assosiated with
 *                           data. That is when data pointer is added, or
 *                           irreco_string_table_change_key() is called.
 */
IrrecoStringTable* irreco_string_table_new(GDestroyNotify data_destroy_func,
					   IrrecoKeySetNotify key_set_func)
{
	IrrecoStringTable* irreco_string_table;
	IRRECO_ENTER

	irreco_string_table = g_slice_new0(IrrecoStringTable);
	irreco_string_table->data_destroy_func = data_destroy_func;
	irreco_string_table->key_set_func = key_set_func;
	IRRECO_RETURN_PTR(irreco_string_table);
}

void irreco_string_table_free(IrrecoStringTable *self)
{
	IRRECO_ENTER

	g_assert(self != NULL);
	irreco_string_table_remove_all(self);
	g_list_free(self->list);
	g_slice_free(IrrecoStringTable, self);

	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Private Functions
 * @{
 */

static IrrecoStringTableItem *irreco_string_table_item_new(const gchar *key,
							   gpointer data)
{
	IrrecoStringTableItem* item;
	IRRECO_ENTER

	item = g_slice_new0(IrrecoStringTableItem);
	irreco_string_table_item_set(item, key, data);
	IRRECO_RETURN_PTR(item);
}

static void irreco_string_table_item_free(IrrecoStringTableItem *item)
{
	IRRECO_ENTER
	irreco_string_table_item_clean(item);
	g_slice_free(IrrecoStringTableItem, item);
	IRRECO_RETURN
}

/**
 * Release all resources reserved by the item.
 * The item can be reused after this.
 */
static void irreco_string_table_item_clean(IrrecoStringTableItem *item)
{
	IRRECO_ENTER
	g_free(item->key);
	item->key = NULL;
	g_free(item->collate_key);
	item->collate_key = NULL;
	item->data = NULL;
	item->hash = 0;
	IRRECO_RETURN
}

/**
 * Set item data.
 */
static void irreco_string_table_item_set(IrrecoStringTableItem *item,
					 const gchar *key,
					 gpointer data)
{
	gchar *canonical;
	IRRECO_ENTER

	irreco_string_table_item_clean(item);

	item->data = data;
	item->key = g_strdup(key);
	item->hash = g_str_hash(item->key);

	/* Collated string, used for sorting. */
	canonical = g_utf8_normalize(item->key, -1, G_NORMALIZE_ALL);
	item->collate_key = g_utf8_collate_key(canonical, -1);
	g_free(canonical);

	IRRECO_RETURN
}

/**
 * Remove item from IrrecoStringTable.
 *
 * @param item Item to be removed.
 * @param call_destroy_fun Should the destroy callback function be called?
 */
static void irreco_string_table_remove_item(IrrecoStringTable *self,
					    IrrecoStringTableItem *item,
					    gboolean call_destroy_fun)
{
	IRRECO_ENTER

	if (call_destroy_fun == TRUE &&
	    item->data != NULL &&
	    self->data_destroy_func != NULL) {
		self->data_destroy_func(item->data);
	}
	irreco_string_table_item_free(item);
	self->list = g_list_remove(self->list, item);

	IRRECO_RETURN
}

/**
 * Find IrrecoStringTableItem by key.
 *
 * @param key	Key to search for.
 * @return	TRUE if found, FALSE otherwise.
 */
static gboolean
irreco_string_table_get_item_by_key(IrrecoStringTable *self,
				    const gchar *key,
				    IrrecoStringTableItem **item)
{
	guint hash;
	GList* list;
	IrrecoStringTableItem *current_item;
	IRRECO_ENTER

	g_assert(key != NULL);
	g_assert(self != NULL);

	list = g_list_first(self->list);
	if (list == NULL) IRRECO_RETURN_BOOL(FALSE);

	hash = g_str_hash(key);
	do {
		current_item = (IrrecoStringTableItem *) list->data;
		if (current_item->hash == hash
		    && strcmp(key, current_item->key) == 0) {
			if (item != NULL) {
				*item = current_item;
			}
			IRRECO_RETURN_BOOL(TRUE);
		}
	} while ((list = g_list_next(list)) != NULL);
	IRRECO_RETURN_BOOL(FALSE);
}

/**
 * Find IrrecoStringTableItem by data.
 *
 * @param data	Data pointer to search for.
 * @return	TRUE if found, FALSE otherwise.
 */
static gboolean
irreco_string_table_get_item_by_data(IrrecoStringTable *self,
				     gpointer *data,
				     IrrecoStringTableItem **item)
{
	GList* list;
	IrrecoStringTableItem *current_item;
	IRRECO_ENTER

	list = g_list_first(self->list);
	if (list == NULL) IRRECO_RETURN_BOOL(FALSE);

	do {
		current_item = (IrrecoStringTableItem *) list->data;
		if (current_item->data == data) {
			if (item != NULL) {
				*item = current_item;
			}
			IRRECO_RETURN_BOOL(TRUE);
		}
	} while ((list = g_list_next(list)) != NULL);
	IRRECO_RETURN_BOOL(FALSE);
}

/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Functions                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */

/**
 * Check if key exists in the table.
 *
 * @return TRUE if exists, FALSE otherwise.
 */
gboolean irreco_string_table_exists(IrrecoStringTable *self,
				    const gchar *key)
{
	IRRECO_ENTER
	IRRECO_RETURN_BOOL(irreco_string_table_get_item_by_key(
		self, key, NULL));
}

/**
 * Add key / data pair.
 */
gboolean irreco_string_table_add(IrrecoStringTable *self,
				 const gchar *key,
				 gpointer data)
{
	IrrecoStringTableItem *item;
	IRRECO_ENTER

	g_assert(key != NULL);
	g_assert(self != NULL);

	if (irreco_string_table_get_item_by_key(self, key, NULL)) {
		IRRECO_RETURN_BOOL(FALSE);
	} else {
		item = irreco_string_table_item_new(key, data);
		self->list = g_list_append(self->list, item);
		if (self->key_set_func != NULL) {
			self->key_set_func(item->data, item->key);
		}
		IRRECO_RETURN_BOOL(TRUE);
	}
}

/**
 * Add key / data pair.
 */
gboolean irreco_string_table_add_first(IrrecoStringTable *self,
				 const gchar *key,
				 gpointer data)
{
	IrrecoStringTableItem *item;
	IRRECO_ENTER

	g_assert(key != NULL);
	g_assert(self != NULL);

	if (irreco_string_table_get_item_by_key(self, key, NULL)) {
		IRRECO_RETURN_BOOL(FALSE);
	} else {
		item = irreco_string_table_item_new(key, data);
		self->list = g_list_prepend(self->list, item);
		if (self->key_set_func != NULL) {
			self->key_set_func(item->data, item->key);
		}
		IRRECO_RETURN_BOOL(TRUE);
	}
}

/**
 * Change the key the data is associated with.
 *
 * @param old_key	A key which is currently in use.
 * @param new_key	A key which is not currently in use.
 * @return		TRUE on success, FALSE otherwise.
 */
gboolean irreco_string_table_change_key(IrrecoStringTable *self,
					const gchar *old_key,
					const gchar *new_key)
{
	IrrecoStringTableItem *item;
	IRRECO_ENTER

	g_assert(self != NULL);
	g_assert(old_key != NULL);
	g_assert(new_key != NULL);

	/* Special case, same key. */
	if (strcmp(old_key, new_key) == 0) IRRECO_RETURN_BOOL(TRUE);

	if (irreco_string_table_get_item_by_key(self, old_key, &item)) {
		irreco_string_table_item_set(item, new_key, item->data);
		if (self->key_set_func != NULL) {
			self->key_set_func(item->data, item->key);
		}
		IRRECO_RETURN_BOOL(TRUE);
	}
	IRRECO_RETURN_BOOL(FALSE);
}

/**
 * Change the data pointer assosiated with a key.
 *
 * @param key	A key which is currently in use.
 * @param data	Data pointer to assosiate with the key.
 * @return	TRUE on success, FALSE otherwise.
 */
gboolean irreco_string_table_change_data(IrrecoStringTable *self,
					 const gchar *key,
					 gpointer data)
{
	IrrecoStringTableItem *item;
	IRRECO_ENTER

	g_assert(self != NULL);
	g_assert(key != NULL);

	if (irreco_string_table_get_item_by_key(self, key, &item)) {

		if (item->data != NULL &&
		    self->data_destroy_func != NULL) {
			self->data_destroy_func(item->data);
		}

		item->data = data;
		IRRECO_RETURN_BOOL(TRUE);
	}
	IRRECO_RETURN_BOOL(FALSE);
}

/**
 * Remove data from string table without calling data_destroy_func.
 *
 * @param data	Data pointer to search for.
 */
gboolean irreco_string_table_steal_by_data(IrrecoStringTable *self,
					   gpointer data)
{
	IrrecoStringTableItem *item;
	IRRECO_ENTER

	g_assert(self != NULL);

	if (irreco_string_table_get_item_by_data(self, data, &item)) {
		irreco_string_table_remove_item(self, item, FALSE);
		IRRECO_RETURN_BOOL(TRUE);
	}
	IRRECO_RETURN_BOOL(FALSE);
}

gboolean irreco_string_table_remove(IrrecoStringTable *self,
				    const gchar *key)
{
	IrrecoStringTableItem *item;
	IRRECO_ENTER

	g_assert(key != NULL);
	g_assert(self != NULL);

	if (irreco_string_table_get_item_by_key(self, key, &item)) {
		irreco_string_table_remove_item(self, item, TRUE);
		IRRECO_RETURN_BOOL(TRUE);
	}
	IRRECO_RETURN_BOOL(FALSE);
}

gboolean irreco_string_table_remove_by_data(IrrecoStringTable *self,
					    gpointer data)
{
	IrrecoStringTableItem *item;
	IRRECO_ENTER

	g_assert(self != NULL);

	if (irreco_string_table_get_item_by_data(self, data, &item)) {
		irreco_string_table_remove_item(self, item, TRUE);
		IRRECO_RETURN_BOOL(TRUE);
	}
	IRRECO_RETURN_BOOL(FALSE);
}

void irreco_string_table_remove_all(IrrecoStringTable *self)
{
	IRRECO_ENTER

	g_assert(self != NULL);

	while (self->list != NULL) {
		irreco_string_table_remove_item(self,
			(IrrecoStringTableItem*) self->list->data, TRUE);
	}
	IRRECO_RETURN
}

gpointer irreco_string_table_get_as_rvalue(IrrecoStringTable *self,
					   const gchar *key)
{
	IrrecoStringTableItem *item;
	IRRECO_ENTER

	g_assert(key != NULL);
	g_assert(self != NULL);

	if (irreco_string_table_get_item_by_key(self, key, &item)) {
		IRRECO_RETURN_PTR(item->data);
	}
	IRRECO_RETURN_PTR(NULL);
}

/**
 * Get pointer assosiated with a key.
 *
 * @code
 * MyStruct *data;
 *
 * if (irreco_string_table_get(table, "key", (gpointer*) &data)) {
 *         IRRECO_PRINTF("Success!!\n");
 * } else {
 *         IRRECO_PRINTF("Key does not exits.\n");
 * }
 * @endcode
 *
 * @param	key	Key to search for.
 * @param	data	Will recieve data pointer.
 * @return	TRUE if key exists, FALSE otherwise.
 */
gboolean irreco_string_table_get(IrrecoStringTable *self,
				 const gchar *key,
				 gpointer *data)
{
	IrrecoStringTableItem *item;
	IRRECO_ENTER

	g_assert(self != NULL);
	g_assert(key != NULL);
	g_assert(data != NULL);

	if (irreco_string_table_get_item_by_key(self, key, &item)) {
		*data = item->data;
		IRRECO_RETURN_BOOL(TRUE);
	}
	IRRECO_RETURN_BOOL(FALSE);
}

/**
 * Gets the key / data pair at the given position in a IrrecoStringTable.
 *
 * Order of key / data pairs depend on current sorting of IrrecoStringTable.
 *
 * @code
 * const gchar *key;
 * MyStruct *data;
 *
 * if (irreco_string_table_index(table, 123, &key, (gpointer *) &data)) {
 *         IRRECO_PRINTF("Key: %s\n", key);
 * } else {
 *         IRRECO_PRINTF("Index does not exits.\n");
 * }
 * @endcode
 *
 * @sa irreco_string_table_lenght()
 * @param index	Zero-based index of key / data pair.
 * @param key	If set, will recieve key string. Memory managed by
 *              IrrecoStringTable, do not free.
 * @param data	If set, Will recieve data pointer.
 * @return	TRUE if index exists, FALSE otherwise.
 */
gboolean irreco_string_table_index(IrrecoStringTable *self,
				   guint index,
				   const gchar **key,
				   gpointer *data)
{
	GList* list;
	IrrecoStringTableItem *item;
	IRRECO_ENTER

	g_assert(self != NULL);
	g_assert(index >= 0);

	/* Get list item. */
	list = g_list_first(self->list);
	item = (IrrecoStringTableItem*) g_list_nth_data(list, index);
	if (item == NULL) IRRECO_RETURN_BOOL(FALSE);

	if (key)  *key  = item->key;
	if (data) *data = item->data;
	IRRECO_RETURN_BOOL(TRUE);
}

/**
 * Get number of items in IrrecoStringTable.
 */
guint irreco_string_table_lenght(IrrecoStringTable *self)
{
	IRRECO_ENTER

	g_assert(self != NULL);

	if (irreco_string_table_is_empty(self)) {
		IRRECO_RETURN_UINT(0);
	} else {
		IRRECO_RETURN_UINT(g_list_length(self->list));
	}
}

/**
 * Get the index of first occurrence of data.
 *
 * @return TRUE on success, FALSE otherwise.
 */
gboolean irreco_string_table_get_index(IrrecoStringTable *self,
				       gpointer data,
				       guint *index)
{
	guint i = 0;
	GList* list;
	IrrecoStringTableItem *item;
	IRRECO_ENTER

	g_assert(self != NULL);
	g_assert(index != NULL);

	list = g_list_first(self->list);
	if (list == NULL) IRRECO_RETURN_BOOL(FALSE);

	do {
		item = (IrrecoStringTableItem *) list->data;
		if (item->data == data) {
			*index = i;
			IRRECO_RETURN_BOOL(TRUE);
		}
		i++;
	} while ((list = g_list_next(list)) != NULL);
	IRRECO_RETURN_BOOL(FALSE);
}

/**
 * Sort IrrecoStringTable aplhabetically.
 */
void irreco_string_table_sort_abc(IrrecoStringTable *self)
{
	IRRECO_ENTER
	g_assert(self != NULL);
	self->list = g_list_sort(self->list,
				 irreco_string_table_sort_abc_compare);
	IRRECO_RETURN
}
static gint irreco_string_table_sort_abc_compare(gconstpointer a, gconstpointer b)
{
	IRRECO_ENTER
	IRRECO_RETURN_INT(strcmp(((IrrecoStringTableItem*)a)->collate_key,
				 ((IrrecoStringTableItem*)b)->collate_key));
}

/**
 * Sort IrrecoStringTable numerically.
 */
void irreco_string_table_sort_123(IrrecoStringTable *self)
{
	IRRECO_ENTER
	g_assert(self != NULL);
	self->list = g_list_sort(self->list,
				 irreco_string_table_sort_123_compare);
	IRRECO_RETURN
}
static gint irreco_string_table_sort_123_compare(gconstpointer a, gconstpointer b)
{
	IRRECO_ENTER

	if(atoi(((IrrecoStringTableItem*)a)->key) == atoi(((IrrecoStringTableItem*)b)->key)) {
		IRRECO_RETURN_INT(0);
	} else if(atoi(((IrrecoStringTableItem*)a)->key) > atoi(((IrrecoStringTableItem*)b)->key)) {
		IRRECO_RETURN_INT(1);
	} else {
		IRRECO_RETURN_INT(-1);
	}

}

/**
 * Prints indexes, keys, key-hashes and data pointers to console.
 *
 * Usefull for debugging.
 */
void irreco_string_table_print(IrrecoStringTable *self)
{
	guint i = 0;
	GList* list;
	IrrecoStringTableItem *item;
	IRRECO_ENTER

	g_assert(self != NULL);

	list = g_list_first(self->list);
	if (list == NULL) {
		IRRECO_PRINTF("String table is empty.\n");
		IRRECO_RETURN
	}

	IRRECO_PRINTF("Index: Hash, Data, Key\n");
	do {
		item = (IrrecoStringTableItem *) list->data;
		if (item == NULL) {
			IRRECO_PRINTF("%u: NULL\n", ++i);
		} else {
			IRRECO_PRINTF("%u: %u, %p, %s\n", ++i, item->hash,
				      item->data, item->key);
		}
	} while ((list = g_list_next(list)) != NULL);

	IRRECO_RETURN
}

/**
 * Used for looping through keys and data.
 *
 * This function should not be called directly, use IRRECO_STRING_TABLE_FOREACH
 * macro instead.
 *
 * @sa IRRECO_STRING_TABLE_FOREACH
 */
gboolean irreco_string_table_foreach(IrrecoStringTable *self, gpointer *pos,
				     const gchar **key, gpointer *data)
{
	GList *list;
	IrrecoStringTableItem *item;
	IRRECO_ENTER

	/* Get list pointer. */
	list = (GList *) *pos;

	/* If list is NULL, we are at the beginning of the table. */
	if (list == NULL) {
		list = g_list_first(self->list);

		/* If list is still NULL, then IrrecoStringTable
		   must be empty.*/
		if (list == NULL) IRRECO_RETURN_BOOL(FALSE);

	/* Are we at the end of the table? */
	} else if ((list = g_list_next(list)) == NULL) {
		*pos = NULL;
		if (key)  *key = NULL;
		if (data) *data = NULL;
		IRRECO_RETURN_BOOL(FALSE);
	}

	/* Set pointers. */
	item = (IrrecoStringTableItem *) list->data;
	*pos = list;
	if (key)  *key = item->key;
	if (data) *data = item->data;
	IRRECO_RETURN_BOOL(TRUE);
}


/** @} */



/** @} */

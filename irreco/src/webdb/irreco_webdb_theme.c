/*
 * irreco - Ir Remote Control
 * Copyright (C) 2008 Joni Kokko (t5kojo01@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_webdb_theme.h"

/**
 * @addtogroup IrrecoWebdbTheme
 * @ingroup IrrecoWebdb
 *
 * Contains information of theme.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoWebdbTheme.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
IrrecoWebdbTheme *irreco_webdb_theme_new()
{
	IrrecoWebdbTheme *self;
	IRRECO_ENTER

	self = g_slice_new0(IrrecoWebdbTheme);

	self->id = 0;
	self->name = g_string_new("");
	self->creator = g_string_new("");
	self->comment = g_string_new("");
	self->preview_button_name = g_string_new("");
	self->preview_button = NULL;
	self->folder = g_string_new("");
	self->uploaded = g_string_new("");
	self->modified = g_string_new("");
	self->downloaded = g_string_new("");
	self->download_count = 0;
	self->versions = NULL;

	IRRECO_RETURN_PTR(self);
}

void irreco_webdb_theme_free(IrrecoWebdbTheme *self)
{
	IRRECO_ENTER

	g_assert(self != NULL);

	g_string_free(self->name, TRUE);
	self->name = NULL;

	g_string_free(self->creator, TRUE);
	self->creator = NULL;

	g_string_free(self->comment, TRUE);
	self->comment = NULL;

	g_string_free(self->preview_button_name, TRUE);
	self->preview_button_name = NULL;

	if (self->preview_button != NULL) {
		g_object_unref(G_OBJECT(self->preview_button));
		self->preview_button = NULL;
	}

	g_string_free(self->folder, TRUE);
	self->folder = NULL;

	g_string_free(self->uploaded, TRUE);
	self->uploaded = NULL;

	g_string_free(self->modified, TRUE);
	self->modified = NULL;

	g_string_free(self->downloaded, TRUE);
	self->downloaded = NULL;

	if (self->versions != NULL) {
		irreco_string_table_free(self->versions);
	}

	g_slice_free(IrrecoWebdbTheme, self);
	self = NULL;

	IRRECO_RETURN
}

/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Functions                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

void irreco_webdb_theme_set(IrrecoWebdbTheme *self,
			    gint id,
			    const char *name,
			    const char *creator,
			    const char *comment,
			    const char *preview_button_name,
			    GdkPixbuf	*preview_button,
			    const char *folder,
			    const char *uploaded,
			    const char *modified,
			    const char *downloaded,
			    gint download_count)
{
	IRRECO_ENTER

	self->id = id;
	g_string_printf(self->name, "%s", name);
	g_string_printf(self->creator, "%s", creator);
	g_string_printf(self->comment, "%s", comment);
	g_string_printf(self->preview_button_name, "%s", preview_button_name);

	if (preview_button != NULL) {
		if (self->preview_button != NULL) {
			g_object_unref(G_OBJECT(self->preview_button));
		}
		self->preview_button = preview_button;
	}
	g_string_printf(self->folder, "%s", folder);
	g_string_printf(self->uploaded, "%s", uploaded);
	g_string_printf(self->modified, "%s", modified);
	g_string_printf(self->downloaded, "%s", downloaded);
	self->download_count = download_count;

	IRRECO_RETURN
}

void irreco_webdb_theme_print(IrrecoWebdbTheme *self)
{
	IRRECO_ENTER
	IRRECO_PRINTF("ID: %d\n", self->id);
	IRRECO_PRINTF("Name: %s\n", self->name->str);
	IRRECO_PRINTF("Creator: %s\n", self->creator->str);
	IRRECO_PRINTF("Comment: %s\n", self->comment->str);
	IRRECO_PRINTF("Preview-button: %s\n", self->preview_button_name->str);
	IRRECO_PRINTF("Folder: %s\n", self->folder->str);
	IRRECO_PRINTF("Uploaded: %s\n", self->uploaded->str);
	IRRECO_PRINTF("Modified: %s\n", self->modified->str);
	IRRECO_PRINTF("Downloaded: %s\n", self->downloaded->str);
	IRRECO_PRINTF("Download_count: %d\n", self->download_count);
	IRRECO_RETURN
}

void irreco_webdb_theme_set_preview_button(IrrecoWebdbTheme *self,
					   GdkPixbuf *preview_button)
{
	IRRECO_ENTER

	if (self->preview_button != NULL) {
		g_object_unref(G_OBJECT(self->preview_button));
	}

	self->preview_button = preview_button;

	IRRECO_RETURN
}

/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Events and Callbacks                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/** @} */

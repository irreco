/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007,2008  Arto Karppinen (arto.karppinen@iki.fi),
 * Joni Kokko (t5kojo01@students.oamk.fi),
 * Sami Mäki (kasmra@xob.kapsi.fi),
 * Harri Vattulainen (t5vaha01@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_webdb_client.h"
#include "config.h"

/**
 * @addtogroup IrrecoWebdbClient
 * @ingroup IrrecoWebdb
 *
 * Irreco Web Database API implementation. These functions connect to the
 * IrrecoWebdbClient trough XML-RPC connection, pass arguments, and decode
 * return values.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoWebdbClient.
 */

static const char *const value_type[] = {
	"BAD",
 "int",
 "boolean",
 "string",
 "double",
 "datetime",
 "base64",
 "struct",
 "array"
};

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

static void irreco_webdb_client_reset_env();
static gboolean do_xmlrpc (IrrecoWebdbClient *self, const char *method, GValue *retval, ...);

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

/**
 * Create new IrrecoWebdbClient Object
 */
IrrecoWebdbClient *irreco_webdb_client_new()
{
	IrrecoWebdbClient *self;
	IRRECO_ENTER

	self = g_slice_new0(IrrecoWebdbClient);

	self->error_msg = g_string_new(NULL);

	IRRECO_RETURN_PTR(self);
}

void irreco_webdb_client_free(IrrecoWebdbClient *self)
{
	IRRECO_ENTER

	g_assert(self != NULL);

	/* Free self->error_msg resource */
	g_string_free(self->error_msg, TRUE);
	self->error_msg = NULL;

	g_slice_free(IrrecoWebdbClient, self);
	self = NULL;

	IRRECO_RETURN
}

/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Private Functions
 * @{
 */

/**
 * Release of RPC results and errors.
 */
static void irreco_webdb_client_reset_env(IrrecoWebdbClient *self)
{
	IRRECO_ENTER

	self->error_msg = g_string_erase(self->error_msg, 0, -1);

	IRRECO_RETURN
}

static gboolean check_xmlrpc (GValue *value, GType type, ...)
{
	va_list args;

	IRRECO_ENTER

	if (!G_VALUE_HOLDS (value, type)) {
		g_printf ( "ERROR: could not parse response\n");
		g_value_unset (value);
		IRRECO_RETURN_BOOL(FALSE);
	}

	va_start (args, type);
	SOUP_VALUE_GETV (value, type, args);
	va_end (args);

	IRRECO_RETURN_BOOL(TRUE);
}


SoupSession *soup_session_new (GType type, ...)
{
	va_list args;
	const char *propname;
	SoupSession *session;

	IRRECO_ENTER

	va_start (args, type);
	propname = va_arg (args, const char *);
	session = (SoupSession *)g_object_new_valist (type, propname, args);
	va_end (args);

	IRRECO_RETURN_PTR(session);
}

void soup_session_abort_unref (SoupSession *session)
{
	IRRECO_ENTER

	g_object_add_weak_pointer (G_OBJECT (session), (gpointer *)&session);

	soup_session_abort (session);
	g_object_unref (session);

	if (session) {
		g_printf ("leaked SoupSession!\n");
		g_object_remove_weak_pointer (G_OBJECT (session), (gpointer *)&session);
	}

	IRRECO_RETURN
}

/**
 * Do the xmlrpc call
 *
 * @param method Methodname
 * @param retval Returned value
 * @param ... Parameters to send
 */
static gboolean do_xmlrpc (IrrecoWebdbClient *self, const char *method, GValue *retval, ...)
{
	SoupMessage *msg;
	va_list args;
	GValueArray *params;
	GError *err = NULL;
	char *body;
	static SoupSession *session;
	SoupLogger *souplogger;

	IRRECO_ENTER

	session = soup_session_new (SOUP_TYPE_SESSION_SYNC, NULL);

	souplogger = soup_logger_new(SOUP_LOGGER_LOG_BODY, -1);
	soup_logger_attach(souplogger, session);

	va_start (args, retval);
	params = soup_value_array_from_args (args);
	va_end (args);

	/*IRRECO_DEBUG("metodi: %s, param num: %d\n", method, params->n_values);*/
	body = soup_xmlrpc_build_method_call (method, params->values,
					      params->n_values);
	/*IRRECO_DEBUG("body: %s\n", body);*/
	g_value_array_free (params);

	if (!body)
		IRRECO_RETURN_BOOL(FALSE);

	msg = soup_message_new ("POST", IRRECO_WEBDB_URL);

	soup_message_set_request (msg, "text/xml", SOUP_MEMORY_TAKE,
				  body, strlen (body));

	soup_message_headers_append(msg->request_headers, "User-Agent", PACKAGE_NAME "/" VERSION);

	soup_session_send_message (session, msg);

	soup_session_abort_unref (session);

	if (!SOUP_STATUS_IS_SUCCESSFUL (msg->status_code)) {
		g_printf ( "ERROR: %d %s\n", msg->status_code,
			   msg->reason_phrase);
		g_string_printf(self->error_msg, "%s", msg->reason_phrase);
		g_object_unref (msg);
		IRRECO_RETURN_BOOL(FALSE);
	}

	if (!soup_xmlrpc_parse_method_response (msg->response_body->data,
	     msg->response_body->length, retval, &err)) {
	      if (err) {
			g_print("FAULT:\ncode: %d\nmessage: %s\n", err->code, err->message);
			g_string_printf(self->error_msg, "%s", err->message);
			g_error_free (err);
			g_object_unref (msg);
			IRRECO_RETURN_BOOL(FALSE);
		} else {
			g_printf ("ERROR: could not parse response\n");
			g_string_printf(self->error_msg, "ERROR: could not parse response");
			g_object_unref (msg);
			IRRECO_RETURN_BOOL(FALSE);
		}
	}
	g_object_unref (msg);

	IRRECO_RETURN_BOOL(TRUE);
}

/**
 * Get error message.
 *
 * @param msg GString object to recieve the error message.
 */
void irreco_webdb_client_get_error_msg(IrrecoWebdbClient *self, GString *msg)
{
	IRRECO_ENTER

	/* Clear and set msg */
	msg = g_string_erase(msg, 0, -1);
	msg = g_string_insert(msg, 0, self->error_msg->str);

	IRRECO_RETURN
}

/**@} */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Functions                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */

/**
 * Ask WebDB to sum two numbers.
 *
 * @param num_a	First number.
 * @param num_b Second number.
 * @param sum	Variable to receive the result.
 * @return TRUE on successfull XML-RPC call, FALSE otherwise.
 */
gboolean irreco_webdb_client_sum(IrrecoWebdbClient *self,
				 gint num_a,
				 gint num_b,
				 glong *sum)
{
	GValue		retval;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);

	if(do_xmlrpc (self, "sum", &retval, G_TYPE_INT, num_a, G_TYPE_INT, num_b, G_TYPE_INVALID)) {
		if(check_xmlrpc (&retval, G_TYPE_INT, sum)) {
			IRRECO_RETURN_BOOL(TRUE);
		} else {
			IRRECO_RETURN_BOOL(FALSE);
		}
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
}

gboolean irreco_webdb_client_upload_configuration(IrrecoWebdbClient *self,
							const gchar *backend,
							const gchar *category,
							const gchar *file_hash,
							const gchar *file_name,
							const gchar *manufacturer,
							const gchar *model,
							const gchar *password,
							const gchar *user,
							const gchar *data)
{
	GValue		retval;
	gchar		*msg;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);

	if(do_xmlrpc (self, "uploadConfiguration", &retval, G_TYPE_STRING, backend, G_TYPE_STRING, category,
	   G_TYPE_STRING, manufacturer, G_TYPE_STRING, model, G_TYPE_STRING, user, G_TYPE_STRING, password,
		G_TYPE_STRING, file_hash, G_TYPE_STRING, file_name, G_TYPE_STRING, data, G_TYPE_INVALID)) {
		if(check_xmlrpc (&retval, G_TYPE_STRING, &msg)) {
			IRRECO_RETURN_BOOL(TRUE);
		} else {
			IRRECO_RETURN_BOOL(FALSE);
		}
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
}


gboolean irreco_webdb_client_get_categories(IrrecoWebdbClient *self,
					    IrrecoStringTable **category_list)
{
	gint		i = 0;
	GValue		retval;
	GValueArray	*array;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);
	*category_list = irreco_string_table_new(NULL, NULL);

	if(do_xmlrpc (self, "getCategories", &retval, G_TYPE_INVALID)) {
		if(check_xmlrpc (&retval, G_TYPE_VALUE_ARRAY, &array)) {
		for(i = 0; i < array->n_values; i++) {
			g_print("%d value is: %s\n", i, G_VALUE_TYPE_NAME(
				g_value_array_get_nth(array, i)));
			irreco_string_table_add(*category_list,
					g_value_get_string(
					g_value_array_get_nth(array, i)),
					NULL);
		}
		IRRECO_RETURN_BOOL(TRUE);
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
}

gboolean irreco_webdb_client_get_all_categories(IrrecoWebdbClient *self,
						IrrecoStringTable **category_list)
{
	gint		i = 0;
	GValue		retval;
	GValueArray	*array;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);
	*category_list = irreco_string_table_new(NULL, NULL);

	if(do_xmlrpc (self, "getAllCategories", &retval, G_TYPE_INVALID)) {
	if(check_xmlrpc (&retval, G_TYPE_VALUE_ARRAY, &array)) {
		for(i = 0; i < array->n_values; i++) {
			g_print("%d value is: %s\n", i, G_VALUE_TYPE_NAME(
				g_value_array_get_nth(array, i)));
			irreco_string_table_add(*category_list,
					g_value_get_string(
					g_value_array_get_nth(array, i)),
					NULL);
		}
		IRRECO_RETURN_BOOL(TRUE);
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
}
gboolean irreco_webdb_client_get_manufacturers(IrrecoWebdbClient *self,
					const gchar *category,
					IrrecoStringTable **manufacturer_list)
{
	gint		i = 0;
	GValue		retval;
	GValueArray	*array;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);
	*manufacturer_list = irreco_string_table_new(NULL, NULL);

	if(do_xmlrpc (self, "getManufacturers", &retval, G_TYPE_STRING, category, G_TYPE_INVALID)) {
	if(check_xmlrpc (&retval, G_TYPE_VALUE_ARRAY, &array)) {
		for(i = 0; i < array->n_values; i++) {
			g_print("%d value is: %s\n", i, G_VALUE_TYPE_NAME(
				g_value_array_get_nth(array, i)));
			irreco_string_table_add(*manufacturer_list,
					g_value_get_string(
					g_value_array_get_nth(array, i)),
					NULL);
		}
		IRRECO_RETURN_BOOL(TRUE);
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
}

gboolean irreco_webdb_client_get_all_manufacturers(IrrecoWebdbClient *self,
					IrrecoStringTable **manufacturer_list)
{
	gint		i = 0;
	GValue		retval;
	GValueArray	*array;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);
	*manufacturer_list = irreco_string_table_new(NULL, NULL);

	if(do_xmlrpc (self, "getAllManufacturers", &retval, G_TYPE_INVALID)) {
	if(check_xmlrpc (&retval, G_TYPE_VALUE_ARRAY, &array)) {
		for(i = 0; i < array->n_values; i++) {
			g_print("%d value is: %s\n", i, G_VALUE_TYPE_NAME(
				g_value_array_get_nth(array, i)));
			irreco_string_table_add(*manufacturer_list,
					g_value_get_string(
					g_value_array_get_nth(array, i)),
					NULL);
		}
		IRRECO_RETURN_BOOL(TRUE);
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
}

gboolean irreco_webdb_client_get_models(IrrecoWebdbClient *self,
					const gchar *category,
					const gchar *manufacturer,
					IrrecoStringTable **model_list)
{
	gint		i = 0;
	GValue		retval;
	GValueArray	*array;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);
	*model_list = irreco_string_table_new(NULL, NULL);

	if(do_xmlrpc (self, "getModels", &retval, G_TYPE_STRING, category, G_TYPE_STRING, manufacturer, G_TYPE_INVALID)) {
	if(check_xmlrpc (&retval, G_TYPE_VALUE_ARRAY, &array)) {
		for(i = 0; i < array->n_values; i++) {
			g_print("%d value is: %s\n", i, G_VALUE_TYPE_NAME(
				g_value_array_get_nth(array, i)));
			irreco_string_table_add(*model_list,
					g_value_get_string(
					g_value_array_get_nth(array, i)),
					NULL);
		}
		IRRECO_RETURN_BOOL(TRUE);
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
}

gboolean irreco_webdb_client_get_configs(IrrecoWebdbClient *self,
					const gchar *manufacturer,
					const gchar *model,
					IrrecoStringTable **config_list)
{
	gint		i = 0;
	GValue		retval;
	GValueArray	*array;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);
	*config_list = irreco_string_table_new(NULL, NULL);

	if(do_xmlrpc (self, "getConfigurations", &retval, G_TYPE_STRING, manufacturer,
		G_TYPE_STRING, model, G_TYPE_INVALID)) {
	if(check_xmlrpc (&retval, G_TYPE_VALUE_ARRAY, &array)) {
		for(i = 0; i < array->n_values; i++) {
			g_print("%d value is: %s\n", i, G_VALUE_TYPE_NAME(
				g_value_array_get_nth(array, i)));
			irreco_string_table_add(*config_list,
					g_value_get_string(
					g_value_array_get_nth(array, i)),
					NULL);
		}
		IRRECO_RETURN_BOOL(TRUE);
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
}


gboolean irreco_webdb_client_get_configuration(IrrecoWebdbClient *self,
					       gint id,
					       IrrecoWebdbConf **configuration)
{
	GValue			retval;
	const gchar		*user = NULL;
	const gchar		*backend;
	const gchar		*category;
	const gchar		*manufacturer;
	const gchar		*model;
	const gchar		*file_hash;
	const gchar		*file_name;
	const gchar		*uploaded;
	const gchar		*download_count;
	GHashTable		*htable;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);

	if(do_xmlrpc (self, "getConfigurationById", &retval, G_TYPE_INT, id, G_TYPE_INVALID)) {
	if(check_xmlrpc (&retval, G_TYPE_HASH_TABLE, &htable)) {
		GValue *tmp;
		tmp = g_hash_table_lookup (htable, "user");
		user = g_value_get_string(tmp);
		tmp = g_hash_table_lookup (htable, "backend");
		backend = g_value_get_string(tmp);
		tmp = g_hash_table_lookup (htable, "category");
		category = g_value_get_string(tmp);
		tmp = g_hash_table_lookup (htable, "manufacturer");
		manufacturer = g_value_get_string(tmp);
		tmp = g_hash_table_lookup (htable, "model");
		model = g_value_get_string(tmp);
		tmp = g_hash_table_lookup (htable, "file_hash");
		file_hash = g_value_get_string(tmp);
		tmp = g_hash_table_lookup (htable, "file_name");
		file_name = g_value_get_string(tmp);
		tmp = g_hash_table_lookup (htable, "uploaded");
		uploaded = g_value_get_string(tmp);
		tmp = g_hash_table_lookup (htable, "download_count");
		download_count = g_value_get_string(tmp);

		IRRECO_DEBUG("Configuration: %d %s %s %s %s %s %s %s %s %s\n",
			id, user, backend, category, manufacturer,
			model, file_hash, file_name, uploaded, download_count);
		irreco_webdb_conf_set(*configuration, id, user, backend,
					category, manufacturer, model,
					file_hash, file_name, uploaded,
					download_count);
		IRRECO_RETURN_BOOL(TRUE);
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
}

gint irreco_webdb_client_get_config_id_by_file_hash_and_file_name(
						IrrecoWebdbClient *self,
						const gchar *file_hash,
						const gchar *file_name)
{
	GValue		retval;
	gint		msg;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);

	if(do_xmlrpc (self, "getConfigIdByFilehashAndFilename", &retval, G_TYPE_STRING, file_hash,
		G_TYPE_STRING, file_name, G_TYPE_INVALID)) {
		if(check_xmlrpc (&retval, G_TYPE_INT, &msg)) {
			IRRECO_RETURN_INT(msg);
		} else {
			IRRECO_RETURN_INT(0);
		}
	} else {
		IRRECO_RETURN_INT(0);
	}
}

gboolean irreco_webdb_client_get_file(IrrecoWebdbClient *self,
					const gchar *file_hash,
					const gchar *file_name,
					GString **file_data)
{
	GValue			retval;
	GHashTable		*htable;
	const gchar		*ret;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);

	if(do_xmlrpc (self, "getFile", &retval, G_TYPE_STRING, file_hash, G_TYPE_STRING, file_name, G_TYPE_INVALID)) {
		if(check_xmlrpc (&retval, G_TYPE_HASH_TABLE, &htable)) {
			GValue *tmp;
			tmp = g_hash_table_lookup (htable, "data");
			ret = g_value_get_string(tmp);
			*file_data = g_string_new(ret);
			g_hash_table_destroy (htable);
			IRRECO_RETURN_BOOL(TRUE)
		} else {
			IRRECO_RETURN_BOOL(FALSE);
		}
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
}


gboolean irreco_webdb_client_get_user_exists(IrrecoWebdbClient *self,
					     const gchar *name,
					     gboolean *user_exists)
{
	GValue		retval;
	gboolean	tmp;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);

	if(do_xmlrpc (self, "getUserExists", &retval, G_TYPE_STRING, name, G_TYPE_INVALID)) {
		if(check_xmlrpc (&retval, G_TYPE_BOOLEAN, &tmp)) {
			*user_exists = tmp;
			IRRECO_RETURN_BOOL(TRUE);
		} else {
			IRRECO_RETURN_BOOL(FALSE);
		}
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
}

gint irreco_webdb_client_get_max_image_size(IrrecoWebdbClient *self)
{
	GValue		retval;
	gint		max_image_size;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);

	if(do_xmlrpc (self, "getMaxImageSize", &retval, G_TYPE_INVALID)) {
		if(check_xmlrpc (&retval, G_TYPE_INT, &max_image_size)) {
			IRRECO_RETURN_INT(max_image_size);
		} else {
			IRRECO_RETURN_INT(0);
		}
	} else {
		IRRECO_RETURN_INT(0);
	}
}

gint irreco_webdb_client_create_theme(IrrecoWebdbClient *self,
				      const gchar *name,
				      const gchar *comment,
				      const gchar *preview_button,
				      const gchar *folder,
				      const gchar *user,
				      const gchar *password)
{
	GValue		retval;
	gint		msg;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);

	if(do_xmlrpc (self, "createNewTheme", &retval, G_TYPE_STRING, name, G_TYPE_STRING, comment,
		G_TYPE_STRING, preview_button, G_TYPE_STRING, folder,
		G_TYPE_STRING, user, G_TYPE_STRING, password, G_TYPE_INVALID)) {
		if(check_xmlrpc (&retval, G_TYPE_INT, &msg)) {
			IRRECO_RETURN_INT(msg);
		} else {
			IRRECO_RETURN_INT(0);
		}
	} else {
		IRRECO_RETURN_INT(0);
	}
}

gboolean irreco_webdb_client_set_theme_downloadable(IrrecoWebdbClient *self,
						    gint id,
						    gboolean downloadable,
						    const gchar *user,
						    const gchar *password)
{
	GValue		retval;
	gboolean	msg;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);

	if(do_xmlrpc (self, "setThemeDownloadable", &retval, G_TYPE_INT, id,
	   G_TYPE_BOOLEAN, downloadable, G_TYPE_STRING, user, G_TYPE_STRING, password, G_TYPE_INVALID)) {
		if(check_xmlrpc (&retval, G_TYPE_BOOLEAN, &msg)) {
			IRRECO_RETURN_BOOL(msg);
		} else {
			IRRECO_RETURN_BOOL(FALSE);
		}
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
}

gboolean irreco_webdb_client_get_themes(IrrecoWebdbClient *self,
					IrrecoStringTable **theme_list)
{
	gint		i = 0;
	GValue		retval;
	GValueArray	*array;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);
	*theme_list = irreco_string_table_new(NULL, NULL);

	if(do_xmlrpc (self, "getThemes", &retval, G_TYPE_INVALID)) {
	if(check_xmlrpc (&retval, G_TYPE_VALUE_ARRAY, &array)) {
		for(i = 0; i < array->n_values; i++) {
			g_print("%d value is: %s\n", i, G_VALUE_TYPE_NAME(
				g_value_array_get_nth(array, i)));
			/*g_print("value: %s\n", g_value_get_string(
				g_value_array_get_nth(array, i)));*/
			irreco_string_table_add(*theme_list,
					g_value_get_string(
					g_value_array_get_nth(array, i)),
					NULL);
		}
		IRRECO_RETURN_BOOL(TRUE);
	} else {
		IRRECO_RETURN_BOOL(FALSE); /* Failed check_xmlrpc */
	}
	} else {
		IRRECO_RETURN_BOOL(FALSE); /* Failed do_xmlrpc */
	}
}

gboolean irreco_webdb_client_get_theme_by_id(IrrecoWebdbClient *self,
					     gint theme_id,
					     IrrecoWebdbTheme **theme)
{
	GValue			retval;
	GHashTable		*htable;
	const gchar		*name		= NULL;
	const gchar		*user		= NULL;
	const gchar		*comment	= NULL;
	const gchar		*preview_button	= NULL;
	const gchar		*folder		= NULL;
	const gchar		*uploaded	= NULL;
	const gchar		*modified	= NULL;
	const gchar		*downloaded	= NULL;
	gint			download_count;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);

	if(do_xmlrpc (self, "getThemeById", &retval, G_TYPE_INT, theme_id, G_TYPE_INVALID)) {
	if(check_xmlrpc (&retval, G_TYPE_HASH_TABLE, &htable)) {
		GValue *tmp;
		tmp = g_hash_table_lookup (htable, "name");
		name = g_value_get_string(tmp);
		tmp = g_hash_table_lookup (htable, "user");
		user = g_value_get_string(tmp);
		tmp = g_hash_table_lookup (htable, "comment");
		comment = g_value_get_string(tmp);
		tmp = g_hash_table_lookup (htable, "preview_button");
		preview_button = g_value_get_string(tmp);
		tmp = g_hash_table_lookup (htable, "folder");
		folder = g_value_get_string(tmp);
		tmp = g_hash_table_lookup (htable, "uploaded");
		uploaded = g_value_get_string(tmp);
		tmp = g_hash_table_lookup (htable, "modified");
		modified = g_value_get_string(tmp);
		tmp = g_hash_table_lookup (htable, "downloaded");
		downloaded = g_value_get_string(tmp);
		tmp = g_hash_table_lookup (htable, "download_count");
		download_count = g_value_get_int(tmp);
		tmp = g_hash_table_lookup (htable, "modified");
		modified = g_value_get_string(tmp);
		tmp = g_hash_table_lookup (htable, "downloaded");
		downloaded = g_value_get_string(tmp);
		tmp = g_hash_table_lookup (htable, "download_count");
		download_count = g_value_get_int(tmp);

		*theme = irreco_webdb_theme_new();

		irreco_webdb_theme_set(*theme, theme_id, name, user, comment,
			preview_button, NULL, folder, uploaded, modified,
			downloaded, download_count);

		irreco_webdb_client_get_theme_versions_by_name(self, name, &(*theme)->versions);

		/* Get dates for versions */
		if ((*theme)->versions != NULL)
		{
		IRRECO_STRING_TABLE_FOREACH_KEY((*theme)->versions, key)
			gchar *date = irreco_webdb_client_get_theme_date_by_id(
							self, atoi(key));
			irreco_string_table_change_data ((*theme)->versions,
							key, (gpointer) date);
		IRRECO_STRING_TABLE_FOREACH_END
		}

		if (name != NULL) g_free((gchar*)name);
		if (user != NULL) g_free((gchar*)user);
		if (comment != NULL) g_free((gchar*)comment);
		if (preview_button != NULL) g_free((gchar*)preview_button);
		if (folder != NULL) g_free((gchar*)folder);
		if (uploaded != NULL) g_free((gchar*)uploaded);
		if (modified != NULL) g_free((gchar*)modified);
		if (downloaded != NULL) g_free((gchar*)downloaded);

		IRRECO_RETURN_BOOL(TRUE);
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
}

gboolean irreco_webdb_client_get_theme_versions_by_name(IrrecoWebdbClient *self,
							const char *name,
							IrrecoStringTable **theme_list)
{
	gint		i = 0;
	GValue		retval;
	GValueArray	*array;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);
	*theme_list = irreco_string_table_new(NULL, NULL);

	if(do_xmlrpc (self, "getThemeVersionsByName", &retval, G_TYPE_STRING, name, G_TYPE_INVALID)) {
	if(check_xmlrpc (&retval, G_TYPE_VALUE_ARRAY, &array)) {
		for(i = 0; i < array->n_values; i++) {
			g_print("%d value is: %s\n", i, G_VALUE_TYPE_NAME(
				g_value_array_get_nth(array, i)));
			/*g_print("value: %s\n", g_value_get_string(
				g_value_array_get_nth(array, i)));*/
			irreco_string_table_add(*theme_list,
					g_value_get_string(
					g_value_array_get_nth(array, i)),
					NULL);
		}
		IRRECO_RETURN_BOOL(TRUE);
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
}

gchar *irreco_webdb_client_get_theme_date_by_id(IrrecoWebdbClient *self,
						gint theme_id)
{
	GValue		retval;
	gchar		*date = NULL;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);

	if(do_xmlrpc (self, "getThemeDateById", &retval, G_TYPE_INT, theme_id, G_TYPE_INVALID)) {
		g_print("tyyppinen: %s\n", G_VALUE_TYPE_NAME(&retval));
		if(check_xmlrpc (&retval, G_TYPE_STRING, &date)) {
			IRRECO_RETURN_PTR(date);
		} else {
			IRRECO_RETURN_PTR(date);
		}
	} else {
		IRRECO_RETURN_PTR(date);
	}
}

gint irreco_webdb_client_get_theme_id_by_name_and_date(IrrecoWebdbClient *self,
							const gchar *name,
							const gchar *date)
{
	GValue		retval;
	gint		msg;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);

	if(do_xmlrpc (self, "getThemeIdByNameAndDate", &retval, G_TYPE_STRING, name,
		G_TYPE_STRING, date, G_TYPE_INVALID)) {
		if(check_xmlrpc (&retval, G_TYPE_INT, &msg)) {
			IRRECO_RETURN_INT(msg);
		} else {
			IRRECO_RETURN_INT(0);
		}
	} else {
		IRRECO_RETURN_INT(0);
	}
}

gint irreco_webdb_client_add_bg_to_theme(IrrecoWebdbClient *self,
					 const gchar *name,
					const gchar *image_hash,
					const gchar *image_name,
					const guchar *image,
					gint image_len,
					const gchar *folder,
					gint theme_id,
					const gchar *user,
					const gchar *password)
{
	GValue		retval;
	gint		msg;
	gchar *base64_image;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);

	base64_image = g_base64_encode(image, image_len);

	if(do_xmlrpc (self, "addBgToTheme", &retval, G_TYPE_STRING, name, G_TYPE_STRING, image_hash,
	   G_TYPE_STRING, image_name, G_TYPE_STRING, base64_image, G_TYPE_STRING, folder,
		G_TYPE_INT, theme_id, G_TYPE_STRING, user, G_TYPE_STRING, password, G_TYPE_INVALID)) {
		if(check_xmlrpc (&retval, G_TYPE_INT, &msg)) {
			IRRECO_RETURN_INT(msg);
		} else {
			IRRECO_RETURN_INT(0);
		}
	} else {
		IRRECO_RETURN_INT(0);
	}
}

gboolean irreco_webdb_client_get_backgrounds(IrrecoWebdbClient *self,
					     gint theme_id,
					     IrrecoStringTable **bg_list)
{
	gint			i = 0;
	GValue			retval;
	GValueArray		*array;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);
	*bg_list = irreco_string_table_new(NULL, NULL);

	if(do_xmlrpc (self, "getBackgrounds", &retval, G_TYPE_INT, theme_id, G_TYPE_INVALID)) {
	if(check_xmlrpc (&retval, G_TYPE_VALUE_ARRAY, &array)) {
		for(i = 0; i < array->n_values; i++) {
			g_print("%d value is: %s\n", i, G_VALUE_TYPE_NAME(
				g_value_array_get_nth(array, i)));
			/*g_print("value: %s\n", g_value_get_string(
				g_value_array_get_nth(array, i)));*/
			irreco_string_table_add(*bg_list,
					g_strdup_printf("%d", g_value_get_int(
					g_value_array_get_nth(array, i))),
					NULL);
		}
		IRRECO_RETURN_BOOL(TRUE);
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
}


gboolean irreco_webdb_client_get_bg_by_id(IrrecoWebdbClient *self,
					  gint bg_id,
					  const char *theme_bg_dir)
{
	GValue			retval;
	GHashTable		*htable;
	gchar			*name		= NULL;
	gchar			*image_hash	= NULL;
	gchar			*image_name	= NULL;
	gchar			*image_data	= NULL;
	gchar			*folder		= NULL;
	gchar			*base64_image	= NULL;
	GString			*image_path	= g_string_new(theme_bg_dir);
	GString			*keyfile_path	= g_string_new(theme_bg_dir);
	GKeyFile		*keyfile	= g_key_file_new();
	gsize 			base64_len;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);

	if(do_xmlrpc (self, "getBgById", &retval, G_TYPE_INT, bg_id, G_TYPE_INVALID)) {
	if(check_xmlrpc (&retval, G_TYPE_HASH_TABLE, &htable)) {
		GValue *tmp;
		tmp = g_hash_table_lookup (htable, "name");
		name = (gchar*) g_value_get_string(tmp);
		tmp = g_hash_table_lookup (htable, "image_hash");
		image_hash = (gchar*) g_value_get_string(tmp);
		tmp = g_hash_table_lookup (htable, "image_name");
		image_name = (gchar*) g_value_get_string(tmp);
		tmp = g_hash_table_lookup (htable, "image_data");
		base64_image = (gchar*) g_value_get_string(tmp);
		tmp = g_hash_table_lookup (htable, "folder");
		folder = (gchar*) g_value_get_string(tmp);

		/* Create Folder */
		g_string_append_printf(image_path, "/%s", folder);
		IRRECO_DEBUG("mkdir %s\n",image_path->str);
		g_mkdir(image_path->str, 0777);

		/* Save image to folder */
		image_data = (gchar*) g_base64_decode(base64_image, &base64_len);
		g_string_append_printf(image_path, "/%s", image_name);
		irreco_write_file(image_path->str, image_data, base64_len);

		/* Create keyfile and save it to folder*/
		irreco_gkeyfile_set_string(keyfile, "theme-bg" , "name", name);
		irreco_gkeyfile_set_string(keyfile, "theme-bg", "image", image_name);
		g_string_append_printf(keyfile_path, "/%s/bg.conf", folder);
		irreco_write_keyfile(keyfile, keyfile_path->str);

		if (name != NULL) g_free(name);
		if (image_hash != NULL) g_free(image_hash);
		if (image_name != NULL) g_free(image_name);
		if (image_data != NULL) g_free(image_data);
		if (base64_image != NULL) g_free(base64_image);
		if (folder != NULL) g_free(folder);

		g_key_file_free(keyfile);
		g_string_free(keyfile_path, TRUE);
		g_string_free(image_path, TRUE);
		IRRECO_RETURN_BOOL(TRUE);
	} else {
		g_key_file_free(keyfile);
		g_string_free(keyfile_path, TRUE);
		g_string_free(image_path, TRUE);
		IRRECO_RETURN_BOOL(FALSE);
	}
	} else {
		g_key_file_free(keyfile);
		g_string_free(keyfile_path, TRUE);
		g_string_free(image_path, TRUE);
		IRRECO_RETURN_BOOL(FALSE);
	}
}

gint irreco_webdb_client_add_button_to_theme(IrrecoWebdbClient *self,
					const gchar *name,
					gboolean allow_text,
					const gchar *text_format_up,
					const gchar *text_format_down,
					gint text_padding,
					gfloat text_h_align,
					gfloat text_v_align,
					const gchar *image_up_hash,
					const gchar *image_up_name,
					const guchar *image_up,
					gint image_up_len,
					const gchar *image_down_hash,
					const gchar *image_down_name,
					const guchar *image_down,
					gint image_down_len,
					const gchar *folder,
					gint theme_id,
					const gchar *user,
					const gchar *password)
{
	GValue		retval;
	gint		msg;
	gchar		*base64_image_up;
	gchar		*base64_image_down;

	IRRECO_ENTER

	base64_image_up = g_base64_encode(image_up, image_up_len);
	base64_image_down = g_base64_encode(image_down, image_down_len);

	irreco_webdb_client_reset_env(self);

	if(do_xmlrpc (self, "addButtonToTheme", &retval, G_TYPE_STRING, name, G_TYPE_BOOLEAN, allow_text,
	   G_TYPE_STRING, text_format_up, G_TYPE_STRING, text_format_down, G_TYPE_INT, text_padding,
    G_TYPE_DOUBLE, text_h_align, G_TYPE_DOUBLE, text_v_align, G_TYPE_STRING, image_up_hash,
    G_TYPE_STRING, image_up_name, G_TYPE_STRING, base64_image_up, G_TYPE_STRING, image_down_hash,
    G_TYPE_STRING, image_down_name, G_TYPE_STRING, base64_image_down, G_TYPE_STRING, folder,
    G_TYPE_INT, theme_id, G_TYPE_STRING, user, G_TYPE_STRING, password, G_TYPE_INVALID)) {
		if(check_xmlrpc (&retval, G_TYPE_INT, &msg)) {
			g_free(base64_image_up);
			g_free(base64_image_down);
			IRRECO_RETURN_INT(msg);
		} else {
			g_free(base64_image_up);
			g_free(base64_image_down);
			IRRECO_RETURN_INT(0);
		}
	} else {
		g_free(base64_image_up);
		g_free(base64_image_down);
		IRRECO_RETURN_INT(0);
	}
}

gboolean irreco_webdb_client_get_buttons(IrrecoWebdbClient *self,
					 gint theme_id,
					IrrecoStringTable **button_list)
{
	gint		i = 0;
	GValue		retval;
	gint		msg;
	GValueArray	*array;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);
	*button_list = irreco_string_table_new(NULL, NULL);

	if(do_xmlrpc (self, "getButtons", &retval, G_TYPE_INT, theme_id, G_TYPE_INVALID)) {
		if(check_xmlrpc (&retval, G_TYPE_VALUE_ARRAY, &array)) {
			for(i = 0; i < array->n_values; i++) {
			g_print("%d value is: %s\n", i, G_VALUE_TYPE_NAME(
				g_value_array_get_nth(array, i)));
			/*g_print("value: %s\n", g_value_get_string(
				g_value_array_get_nth(array, i)));*/
			irreco_string_table_add(*button_list,
					g_strdup_printf("%d", g_value_get_int(
					g_value_array_get_nth(array, i))),
					NULL);
		}
			IRRECO_RETURN_INT(msg);
	} else {
		IRRECO_RETURN_INT(0);
	}
	} else {
		IRRECO_RETURN_INT(0);
	}
}


gboolean irreco_webdb_client_get_button_by_id(IrrecoWebdbClient *self,
					      gint button_id,
					      const char *theme_button_dir)
{
	GValue			retval;
	GHashTable		*htable;
	gchar			*name			= NULL;
	gboolean		allow_text		= FALSE;
	gchar			*text_format_up		= NULL;
	gchar			*text_format_down	= NULL;
	glong			text_padding;
	gdouble			text_h_align;
	gdouble			text_v_align;
	gchar			*image_up_hash		= NULL;
	gchar			*image_up_name		= NULL;
	gchar			*image_up		= NULL;
	gchar			*base64_image_up	= NULL;
	gchar			*image_down_hash	= NULL;
	gchar			*image_down_name	= NULL;
	gchar			*image_down		= NULL;
	gchar			*base64_image_down	= NULL;
	gchar			*folder			= NULL;
	gchar			*image_down_hash_tmp	= NULL;
	GString			*file_path		= g_string_new("");
	GKeyFile		*keyfile		= g_key_file_new();
	gsize 			image_down_len;
	gsize 			image_up_len;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);

	if(do_xmlrpc (self, "getButtonById", &retval, G_TYPE_INT, button_id, G_TYPE_INVALID)) {
	if(check_xmlrpc (&retval, G_TYPE_HASH_TABLE, &htable)) {
		GValue *tmp;
		tmp = g_hash_table_lookup (htable, "name");
		name = (gchar*) g_value_get_string(tmp);
		tmp = g_hash_table_lookup (htable, "allow_text");
		allow_text = g_value_get_boolean(tmp);
		tmp = g_hash_table_lookup (htable, "text_format_up");
		text_format_up = (gchar*) g_value_get_string(tmp);
		tmp = g_hash_table_lookup (htable, "text_format_down");
		text_format_down = (gchar*) g_value_get_string(tmp);
		tmp = g_hash_table_lookup (htable, "text_padding");
		text_padding = g_value_get_int(tmp);
		tmp = g_hash_table_lookup (htable, "text_h_align");
		text_h_align = g_value_get_double(tmp);
		tmp = g_hash_table_lookup (htable, "text_v_align");
		text_v_align = g_value_get_double(tmp);
		tmp = g_hash_table_lookup (htable, "image_up_hash");
		image_up_hash = (gchar*) g_value_get_string(tmp);
		tmp = g_hash_table_lookup (htable, "image_up_name");
		image_up_name = (gchar*) g_value_get_string(tmp);
		tmp = g_hash_table_lookup (htable, "image_up");
		base64_image_up = (gchar*) g_value_get_string(tmp);
		tmp = g_hash_table_lookup (htable, "image_down_hash");
		image_down_hash = (gchar*) g_value_get_string(tmp);
		tmp = g_hash_table_lookup (htable, "image_down_name");
		image_down_name = (gchar*) g_value_get_string(tmp);
		tmp = g_hash_table_lookup (htable, "image_down");
		base64_image_down = (gchar*) g_value_get_string(tmp);
		tmp = g_hash_table_lookup (htable, "folder");
		folder = (gchar*) g_value_get_string(tmp);

		/* Create Folder */
		g_string_printf(file_path, "%s/%s", theme_button_dir, folder);
		IRRECO_DEBUG("mkdir %s\n",file_path->str);
		g_mkdir(file_path->str, 0777);

		/* Save image_up to folder */
		g_string_printf(file_path, "%s/%s/%s", theme_button_dir, folder,
				image_up_name);
		image_up = (gchar*) g_base64_decode(base64_image_up, &image_up_len);
		irreco_write_file(file_path->str, image_up, image_up_len);

		/* Save image_down to folder */
		g_string_printf(file_path, "%s/%s/%s", theme_button_dir, folder,
				image_down_name);

		/*Check image hash data*/
		image_down = (gchar*) g_base64_decode(base64_image_down,
			&image_down_len);
		irreco_write_file(file_path->str, image_down, image_down_len);
		image_down_hash_tmp = g_compute_checksum_for_string(G_CHECKSUM_SHA1,
							image_down, image_down_len);

		/* Create keyfile and save it to folder*/
		irreco_gkeyfile_set_string(keyfile, "theme-button" , "name", name);

		if (allow_text) {
			irreco_gkeyfile_set_string(keyfile, "theme-button",
						"allow-text", "true");
		} else {
			irreco_gkeyfile_set_string(keyfile, "theme-button",
						"allow-text", "false");
		}

		irreco_gkeyfile_set_string(keyfile, "theme-button",
					"up", image_up_name);

		irreco_gkeyfile_set_string(keyfile, "theme-button",
					"down", image_down_name);

		if (text_format_up != NULL && strlen(text_format_up) > 0) {
			irreco_gkeyfile_set_string(keyfile, "theme-button",
						"text-format-up", text_format_up);
		}

		if (text_format_down != NULL && strlen(text_format_down) > 0) {
			irreco_gkeyfile_set_string(keyfile, "theme-button",
						"text-format-down", text_format_down);
		}

		irreco_gkeyfile_set_glong(keyfile, "theme-button",
					"text-padding", (glong)text_padding);

		irreco_gkeyfile_set_gfloat(keyfile, "theme-button",
					"text-h-align", (gfloat) text_h_align);

		irreco_gkeyfile_set_gfloat(keyfile, "theme-button",
					"text-v-align", (gfloat) text_v_align);

		g_string_printf(file_path, "%s/%s/button.conf",
				theme_button_dir, folder);
		irreco_write_keyfile(keyfile, file_path->str);

		if (name != NULL) g_free(name);
		if (text_format_up != NULL) g_free(text_format_up);
		if (text_format_down != NULL) g_free(text_format_down);
		if (image_up_hash != NULL) g_free(image_up_hash);
		if (image_up_name != NULL) g_free(image_up_name);
		if (image_up != NULL) g_free(image_up);
		if (base64_image_up != NULL) g_free(base64_image_up);
		if (image_down_hash != NULL) g_free(image_down_hash);
		if (image_down_name != NULL) g_free(image_down_name);
		if (image_down != NULL) g_free(image_down);
		if (image_down_hash_tmp != NULL) g_free(image_down_hash_tmp);
		if (base64_image_down != NULL) g_free(base64_image_down);
		if (folder != NULL) g_free(folder);

		g_key_file_free(keyfile);
		g_string_free(file_path, TRUE);

		IRRECO_RETURN_BOOL(TRUE);
	} else {
		if (name != NULL) g_free(name);
		if (text_format_up != NULL) g_free(text_format_up);
		if (text_format_down != NULL) g_free(text_format_down);
		if (image_up_hash != NULL) g_free(image_up_hash);
		if (image_up_name != NULL) g_free(image_up_name);
		if (image_up != NULL) g_free(image_up);
		if (base64_image_up != NULL) g_free(base64_image_up);
		if (image_down_hash != NULL) g_free(image_down_hash);
		if (image_down_name != NULL) g_free(image_down_name);
		if (image_down != NULL) g_free(image_down);
		if (image_down_hash_tmp != NULL) g_free(image_down_hash_tmp);
		if (base64_image_down != NULL) g_free(base64_image_down);
		if (folder != NULL) g_free(folder);

		g_key_file_free(keyfile);
		g_string_free(file_path, TRUE);
		IRRECO_RETURN_BOOL(FALSE);
	}
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
}

gboolean irreco_webdb_client_get_preview_button(IrrecoWebdbClient *self,
						gint theme_id,
						GdkPixbuf **preview_button)
{
	GValue		retval;
	gchar		*base64_data = NULL;
	guchar		*data;
	gsize		len;
	GdkPixbufLoader	*pl;
	GError		*error = NULL;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);

	if(do_xmlrpc (self, "getPreviewButton", &retval, G_TYPE_INT, theme_id, G_TYPE_INVALID)) {
	if(check_xmlrpc (&retval, G_TYPE_STRING, &base64_data)) {
		data = g_base64_decode(base64_data, &len);
		pl = gdk_pixbuf_loader_new();
		gdk_pixbuf_loader_write(pl, data, len, &error);

		if(error != NULL)
		{
			/*g_string_printf(self->error_msg, "ERROR: %s", error->message);*/
			IRRECO_DEBUG("%s\n", error->message);
			if (base64_data != NULL) g_free(base64_data);
			IRRECO_RETURN_BOOL(FALSE);
		}

		gdk_pixbuf_loader_close(pl, NULL);
		*preview_button = gdk_pixbuf_loader_get_pixbuf(pl);

		if (base64_data != NULL) g_free(base64_data);

		IRRECO_RETURN_BOOL(TRUE);
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
}

gboolean irreco_webdb_client_add_user(IrrecoWebdbClient *self,
				      const gchar *name,
				      const gchar *email,
				      const gchar *passwd)
{
	GValue		retval;
	gboolean	msg;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);

	if(do_xmlrpc (self, "addUser", &retval, G_TYPE_STRING, name, G_TYPE_STRING, email,
	   G_TYPE_STRING, passwd, G_TYPE_INVALID)) {
		if(check_xmlrpc (&retval, G_TYPE_BOOLEAN, &msg)) {
			IRRECO_RETURN_BOOL(msg);
		} else {
			IRRECO_RETURN_BOOL(FALSE);
		}
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
}

/**
 * Login user to WebDB.
 *
 */
gboolean irreco_webdb_client_login(IrrecoWebdbClient *self,
				   const gchar *user,
				   const gchar *password)
{
	GValue		retval;
	gboolean	msg;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);

	if(do_xmlrpc (self, "loginToDB", &retval, G_TYPE_STRING, user, G_TYPE_STRING, password, G_TYPE_INVALID)) {
		if(check_xmlrpc (&retval, G_TYPE_BOOLEAN, &msg)) {
			IRRECO_RETURN_BOOL(msg);
		} else {
			IRRECO_RETURN_BOOL(FALSE);
		}
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
}

gint irreco_webdb_client_create_new_remote(IrrecoWebdbClient *self,
					const gchar *comment,
					const gchar *category,
					const gchar *manufacturer,
					const gchar *model,
					const gchar *file_name,
					const gchar *file_data,
					const gchar *user,
					const gchar *password)
{
	GValue		retval;
	gint		msg;
	gchar		*file_hash;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);
	file_hash = g_compute_checksum_for_string(G_CHECKSUM_SHA1, file_data, -1);

	if(do_xmlrpc (self, "createNewRemote", &retval, G_TYPE_STRING, comment, G_TYPE_STRING, category,
	   G_TYPE_STRING, manufacturer, G_TYPE_STRING, model, G_TYPE_STRING, file_hash,
    G_TYPE_STRING, file_name, G_TYPE_STRING, file_data, G_TYPE_STRING, user,
    G_TYPE_STRING, password, G_TYPE_INVALID)) {
		if(check_xmlrpc (&retval, G_TYPE_INT, &msg)) {
			IRRECO_RETURN_INT(msg);
		} else {
			IRRECO_RETURN_INT(0);
		}
	} else {
		IRRECO_RETURN_INT(0);
	}
}

gboolean irreco_webdb_client_set_remote_downloadable(IrrecoWebdbClient *self,
						gint id,
						gboolean downloadable,
						const gchar *user,
						const gchar *password)
{
	GValue		retval;
	gboolean	msg;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);

	if(do_xmlrpc (self, "setRemoteDownloadable", &retval, G_TYPE_INT, id,
	   G_TYPE_BOOLEAN, downloadable, G_TYPE_STRING, user, G_TYPE_STRING, password, G_TYPE_INVALID)) {
		if(check_xmlrpc (&retval, G_TYPE_BOOLEAN, &msg)) {
			IRRECO_RETURN_BOOL(msg);
		} else {
			IRRECO_RETURN_BOOL(FALSE);
		}
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
}

gboolean irreco_webdb_client_add_configuration_to_remote(
						IrrecoWebdbClient *self,
						gint remote_id,
						gint configuration_id,
						const gchar *user,
						const gchar *password)
{
	GValue		retval;
	gboolean	msg;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);

	if(do_xmlrpc (self, "addConfigurationToRemote", &retval, G_TYPE_INT, remote_id,
	   G_TYPE_INT, configuration_id, G_TYPE_STRING, user, G_TYPE_STRING, password, G_TYPE_INVALID)) {
		if(check_xmlrpc (&retval, G_TYPE_BOOLEAN, &msg)) {
			IRRECO_RETURN_BOOL(msg);
		} else {
			IRRECO_RETURN_BOOL(FALSE);
		}
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
}

gboolean irreco_webdb_client_add_theme_to_remote(IrrecoWebdbClient *self,
						gint remote_id,
						gint theme_id,
						const gchar *user,
						const gchar *password)
{
	GValue		retval;
	gboolean	msg;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);

	if(do_xmlrpc (self, "addThemeToRemote", &retval, G_TYPE_INT, remote_id,
	   G_TYPE_INT, theme_id, G_TYPE_STRING, user, G_TYPE_STRING, password, G_TYPE_INVALID)) {
		if(check_xmlrpc (&retval, G_TYPE_BOOLEAN, &msg)) {
			IRRECO_RETURN_BOOL(msg);
		} else {
			IRRECO_RETURN_BOOL(FALSE);
		}
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
}

gboolean irreco_webdb_client_get_remote_categories(IrrecoWebdbClient *self,
						IrrecoStringTable **category_list)
{
	gint		i = 0;
	GValue		retval;
	GValueArray	*array;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);
	*category_list = irreco_string_table_new(NULL, NULL);

	if(do_xmlrpc (self, "getRemoteCategories", &retval, G_TYPE_INVALID)) {
	if(check_xmlrpc (&retval, G_TYPE_VALUE_ARRAY, &array)) {
		for(i = 0; i < array->n_values; i++) {
			g_print("%d value is: %s\n", i, G_VALUE_TYPE_NAME(
				g_value_array_get_nth(array, i)));
			/*g_print("value: %s\n", g_value_get_string(
				g_value_array_get_nth(array, i)));*/
			irreco_string_table_add(*category_list,
					g_value_get_string(
					g_value_array_get_nth(array, i)),
					NULL);
		}
		IRRECO_RETURN_BOOL(TRUE);
	} else {
		IRRECO_RETURN_BOOL(FALSE); /* Failed check_xmlrpc */
	}
	} else {
		IRRECO_RETURN_BOOL(FALSE); /* Failed do_xmlrpc */
	}
}

gboolean irreco_webdb_client_get_remote_manufacturers(IrrecoWebdbClient *self,
						const gchar *category,
						IrrecoStringTable **manufacturer_list)
{
	gint		i = 0;
	GValue		retval;
	GValueArray	*array;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);
	*manufacturer_list = irreco_string_table_new(NULL, NULL);

	if(do_xmlrpc (self, "getRemoteManufacturers", &retval, G_TYPE_STRING, category, G_TYPE_INVALID)) {
	if(check_xmlrpc (&retval, G_TYPE_VALUE_ARRAY, &array)) {
		for(i = 0; i < array->n_values; i++) {
			g_print("%d value is: %s\n", i, G_VALUE_TYPE_NAME(
				g_value_array_get_nth(array, i)));
			/*g_print("value: %s\n", g_value_get_string(
				g_value_array_get_nth(array, i)));*/
			irreco_string_table_add(*manufacturer_list,
					g_value_get_string(
					g_value_array_get_nth(array, i)),
					NULL);
		}
		IRRECO_RETURN_BOOL(TRUE);
	} else {
		IRRECO_RETURN_BOOL(FALSE); /* Failed check_xmlrpc */
	}
	} else {
		IRRECO_RETURN_BOOL(FALSE); /* Failed do_xmlrpc */
	}
}

gboolean irreco_webdb_client_get_remote_models(IrrecoWebdbClient *self,
						const gchar *category,
						const gchar *manufacturer,
						IrrecoStringTable **model_list)
{
	gint		i = 0;
	GValue		retval;
	GValueArray	*array;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);
	*model_list = irreco_string_table_new(NULL, NULL);

	if(do_xmlrpc (self, "getRemoteModels", &retval, G_TYPE_STRING, category,
	   G_TYPE_STRING, manufacturer, G_TYPE_INVALID)) {
	if(check_xmlrpc (&retval, G_TYPE_VALUE_ARRAY, &array)) {
		for(i = 0; i < array->n_values; i++) {
			g_print("%d value is: %s\n", i, G_VALUE_TYPE_NAME(
				g_value_array_get_nth(array, i)));
			/*g_print("value: %s\n", g_value_get_string(
				g_value_array_get_nth(array, i)));*/
			irreco_string_table_add(*model_list,
					g_value_get_string(
					g_value_array_get_nth(array, i)),
					NULL);
		}
		IRRECO_RETURN_BOOL(TRUE);
	} else {
		IRRECO_RETURN_BOOL(FALSE); /* Failed check_xmlrpc */
	}
	} else {
		IRRECO_RETURN_BOOL(FALSE); /* Failed do_xmlrpc */
	}
}

gboolean irreco_webdb_client_get_remote_creators(IrrecoWebdbClient *self,
						const gchar *category,
						const gchar *manufacturer,
						const gchar *model,
						IrrecoStringTable **creators)
{
	gint		i = 0;
	GValue		retval;
	GValueArray	*array;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);
	*creators = irreco_string_table_new(NULL, NULL);

	if(do_xmlrpc (self, "getRemoteCreators", &retval, G_TYPE_STRING, category,
	   G_TYPE_STRING, manufacturer, G_TYPE_STRING, model, G_TYPE_INVALID)) {
	if(check_xmlrpc (&retval, G_TYPE_VALUE_ARRAY, &array)) {
		for(i = 0; i < array->n_values; i++) {
			g_print("%d value is: %s\n", i, G_VALUE_TYPE_NAME(
				g_value_array_get_nth(array, i)));
			/*g_print("value: %s\n", g_value_get_string(
				g_value_array_get_nth(array, i)));*/
			irreco_string_table_add(*creators,
					g_value_get_string(
					g_value_array_get_nth(array, i)),
					NULL);
		}
		IRRECO_RETURN_BOOL(TRUE);
	} else {
		IRRECO_RETURN_BOOL(FALSE); /* Failed check_xmlrpc */
	}
	} else {
		IRRECO_RETURN_BOOL(FALSE); /* Failed do_xmlrpc */
	}
}

gboolean irreco_webdb_client_get_remotes(IrrecoWebdbClient *self,
					 const gchar *category,
					const gchar *manufacturer,
					const gchar *model,
					const gchar *creator,
					GList **remote_list)
{
	gint		i = 0;
	GValue		retval;
	GValueArray	*array;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);
	*remote_list = NULL;

	if(do_xmlrpc (self, "getRemotes", &retval, G_TYPE_STRING, category, G_TYPE_STRING,
	   manufacturer, G_TYPE_STRING, model, G_TYPE_STRING, creator, G_TYPE_INVALID)) {
	if(check_xmlrpc (&retval, G_TYPE_VALUE_ARRAY, &array)) {
		for(i = 0; i < array->n_values; i++) {
			g_print("%d value is: %s\n", i, G_VALUE_TYPE_NAME(
				g_value_array_get_nth(array, i)));
			g_print("value: %d\n", g_value_get_int(
				g_value_array_get_nth(array, i)));
			*remote_list = g_list_append(*remote_list,
					GINT_TO_POINTER(g_value_get_int(
							g_value_array_get_nth(array, i))));
		}

		*remote_list = g_list_first(*remote_list);
		IRRECO_RETURN_BOOL(TRUE);
	} else {
		IRRECO_RETURN_BOOL(FALSE); /* Failed check_xmlrpc */
	}
	} else {
		IRRECO_RETURN_BOOL(FALSE); /* Failed do_xmlrpc */
	}
}

gboolean irreco_webdb_client_get_remote_by_id(IrrecoWebdbClient *self,
					      gint id,
					      IrrecoWebdbRemote **remote)
{
	GValue			retval;
	const gchar		*user		= NULL;
	const gchar		*comment	= NULL;
	const gchar		*category	= NULL;
	const gchar		*manufacturer	= NULL;
	const gchar		*model		= NULL;
	const gchar		*file_hash	= NULL;
	const gchar		*file_name	= NULL;
	const gchar		*uploaded	= NULL;
	const gchar		*modified	= NULL;
	const gchar		*downloaded	= NULL;
	gint			download_count;
	GHashTable		*htable;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);

	if(do_xmlrpc (self, "getRemoteById", &retval, G_TYPE_INT, id, G_TYPE_INVALID)) {
	if(check_xmlrpc (&retval, G_TYPE_HASH_TABLE, &htable)) {
		GValue *tmp;
		tmp = g_hash_table_lookup (htable, "user");
		user = g_value_get_string(tmp);
		tmp = g_hash_table_lookup (htable, "comment");
		comment = g_value_get_string(tmp);
		tmp = g_hash_table_lookup (htable, "category");
		category = g_value_get_string(tmp);
		tmp = g_hash_table_lookup (htable, "manufacturer");
		manufacturer = g_value_get_string(tmp);
		tmp = g_hash_table_lookup (htable, "model");
		model = g_value_get_string(tmp);
		tmp = g_hash_table_lookup (htable, "file_hash");
		file_hash = g_value_get_string(tmp);
		tmp = g_hash_table_lookup (htable, "file_name");
		file_name = g_value_get_string(tmp);
		tmp = g_hash_table_lookup (htable, "uploaded");
		uploaded = g_value_get_string(tmp);
		tmp = g_hash_table_lookup (htable, "modified");
		modified = g_value_get_string(tmp);
		tmp = g_hash_table_lookup (htable, "downloaded");
		downloaded = g_value_get_string(tmp);
		tmp = g_hash_table_lookup (htable, "download_count");
		download_count = g_value_get_int(tmp);

/*		g_print("blbbi: %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %d", user, comment, category,
		manufacturer, model, file_hash, file_name, uploaded, modified, downloaded, download_count);*/

		*remote = irreco_webdb_remote_new();
		irreco_webdb_remote_set(*remote, id, user, comment, category,
				manufacturer, model, file_hash, file_name,
				uploaded, modified, downloaded, download_count);

		if (user != NULL) g_free((gchar*)user);
		if (comment != NULL) g_free((gchar*)comment);
		if (category != NULL) g_free((gchar*)category);
		if (manufacturer != NULL) g_free((gchar*)manufacturer);
		if (model != NULL) g_free((gchar*)model);
		if (file_hash != NULL) g_free((gchar*)file_hash);
		if (file_name != NULL) g_free((gchar*)file_name);
		if (uploaded != NULL) g_free((gchar*)uploaded);
		if (modified != NULL) g_free((gchar*)modified);
		if (downloaded != NULL) g_free((gchar*)downloaded);

		IRRECO_RETURN_BOOL(TRUE);
	} else {

		IRRECO_RETURN_BOOL(FALSE);
	}
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
}


gboolean irreco_webdb_client_get_themes_of_remote(IrrecoWebdbClient *self,
						gint remote_id,
						GList **themes)
{
	gint		i = 0;
	GValue		retval;
	GValueArray	*array;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);
	*themes = NULL;

	if(do_xmlrpc (self, "getThemesOfRemote", &retval, G_TYPE_INT, remote_id, G_TYPE_INVALID)) {
	if(check_xmlrpc (&retval, G_TYPE_VALUE_ARRAY, &array)) {
		for(i = 0; i < array->n_values; i++) {
			g_print("%d value is: %s\n", i, G_VALUE_TYPE_NAME(
				g_value_array_get_nth(array, i)));
			g_print("value: %d\n", g_value_get_int(
				g_value_array_get_nth(array, i)));
			*themes = g_list_append(*themes,
					GINT_TO_POINTER(g_value_get_int(
							g_value_array_get_nth(array, i))));
		}

		*themes = g_list_first(*themes);
		IRRECO_RETURN_BOOL(TRUE);
	} else {
		IRRECO_RETURN_BOOL(FALSE); /* Failed check_xmlrpc */
	}
	} else {
		IRRECO_RETURN_BOOL(FALSE); /* Failed do_xmlrpc */
	}
}


gboolean irreco_webdb_client_get_configurations_of_remote(
						IrrecoWebdbClient *self,
						gint remote_id,
						GList **configs)
{
	gint		i = 0;
	GValue		retval;
	GValueArray	*array;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);
	*configs = NULL;

	if(do_xmlrpc (self, "getConfigurationsOfRemote", &retval, G_TYPE_INT, remote_id, G_TYPE_INVALID)) {
	if(check_xmlrpc (&retval, G_TYPE_VALUE_ARRAY, &array)) {
		for(i = 0; i < array->n_values; i++) {
			g_print("%d value is: %s\n", i, G_VALUE_TYPE_NAME(
				g_value_array_get_nth(array, i)));
			g_print("value: %d\n", g_value_get_int(
				g_value_array_get_nth(array, i)));
			*configs = g_list_append(*configs,
					GINT_TO_POINTER(g_value_get_int(
							g_value_array_get_nth(array, i))));
		}

		*configs = g_list_first(*configs);
		IRRECO_RETURN_BOOL(TRUE);
	} else {
		IRRECO_RETURN_BOOL(FALSE); /* Failed check_xmlrpc */
	}
	} else {
		IRRECO_RETURN_BOOL(FALSE); /* Failed do_xmlrpc */
	}
}

gboolean irreco_webdb_client_get_remote_data(IrrecoWebdbClient *self,
					     gint remote_id,
					     gchar **file_data)
{/* #define IRRECO_WEBDB_URL "http://localhost/irreco/webdb/" */

	GValue		retval;
	GHashTable		*htable;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);

	if(do_xmlrpc (self, "getRemoteData", &retval, G_TYPE_INT, remote_id, G_TYPE_INVALID)) {
		if(check_xmlrpc (&retval, G_TYPE_HASH_TABLE, &htable)) {
			GValue *tmp;
			tmp = g_hash_table_lookup (htable, "data");
			*file_data = (gchar*) g_value_get_string(tmp);
			IRRECO_RETURN_BOOL(TRUE);
		} else {
			IRRECO_RETURN_BOOL(FALSE);
		}
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
}

gboolean irreco_webdb_client_get_lirc_dirs(IrrecoWebdbClient *self,
					   IrrecoStringTable **dir_list)
{
	gint		i = 0;
	GValue		retval;
	GValueArray	*array;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);
	*dir_list = irreco_string_table_new(NULL, NULL);

	if(do_xmlrpc (self, "getLircDirs", &retval, G_TYPE_INVALID)) {
	if(check_xmlrpc (&retval, G_TYPE_VALUE_ARRAY, &array)) {
		for(i = 0; i < array->n_values; i++) {
			g_print("%d value type is: %s\n", i, G_VALUE_TYPE_NAME(
				g_value_array_get_nth(array, i)));
			irreco_string_table_add(*dir_list,
					g_value_get_string(
					g_value_array_get_nth(array, i)),
					NULL);
		}
		IRRECO_RETURN_BOOL(TRUE);
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
}

gboolean irreco_webdb_client_get_lirc_manufacturers(IrrecoWebdbClient *self,
						const gchar *range,
						IrrecoStringTable **manufacturer_list)
{
	gint		i = 0;
	GValue		retval;
	GValueArray	*array;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);
	*manufacturer_list = irreco_string_table_new(NULL, NULL);

	if(do_xmlrpc (self, "getLircManufacturers", &retval, G_TYPE_STRING, range, G_TYPE_INVALID)) {
	if(check_xmlrpc (&retval, G_TYPE_VALUE_ARRAY, &array)) {
		for(i = 0; i < array->n_values; i++) {
			g_print("%d value is: %s\n", i, G_VALUE_TYPE_NAME(
				g_value_array_get_nth(array, i)));
			irreco_string_table_add(*manufacturer_list,
					g_value_get_string(
					g_value_array_get_nth(array, i)),
					NULL);
		}
		IRRECO_RETURN_BOOL(TRUE);
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
}

gboolean irreco_webdb_client_get_lirc_models(IrrecoWebdbClient *self,
					     const gchar *manufacturer,
					     IrrecoStringTable **model_list)
{
	gint		i = 0;
	GValue		retval;
	GValueArray	*array;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);
	*model_list = irreco_string_table_new(NULL, NULL);

	if(do_xmlrpc (self, "getLircModels", &retval, G_TYPE_STRING, manufacturer, G_TYPE_INVALID)) {
	if(check_xmlrpc (&retval, G_TYPE_VALUE_ARRAY, &array)) {
		for(i = 0; i < array->n_values; i++) {
			g_print("%d value is: %s\n", i, G_VALUE_TYPE_NAME(
				g_value_array_get_nth(array, i)));
			irreco_string_table_add(*model_list,
					g_value_get_string(
					g_value_array_get_nth(array, i)),
					NULL);
		}
		IRRECO_RETURN_BOOL(TRUE);
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
}

gboolean irreco_webdb_client_get_lirc_file(IrrecoWebdbClient *self,
					   const gchar *model,
					   IrrecoStringTable **file)
{
	gint		i = 0;
	GValue		retval;
	GValueArray	*array;

	IRRECO_ENTER

	irreco_webdb_client_reset_env(self);
	*file = irreco_string_table_new(NULL, NULL);

	if(do_xmlrpc (self, "getLircFile", &retval, G_TYPE_STRING, model, G_TYPE_INVALID)) {
	if(check_xmlrpc (&retval, G_TYPE_VALUE_ARRAY, &array)) {
		for(i = 0; i < array->n_values; i++) {
			g_print("%d value is: %s\n", i, G_VALUE_TYPE_NAME(
				g_value_array_get_nth(array, i)));
			irreco_string_table_add(*file,
					g_value_get_string(
					g_value_array_get_nth(array, i)),
					NULL);
		}
		IRRECO_RETURN_BOOL(TRUE);
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
}

/** @} */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Events and Callbacks                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/** @} */

/*
 * irreco - Ir Remote Control
 * Copyright (C) 2008 Joni Kokko (t5kojo01@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoWebdbRemote
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoWebdbRemote.
 */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __WEBDB_REMOTE_H_TYPEDEF__
#define __WEBDB_REMOTE_H_TYPEDEF__
typedef struct _IrrecoWebdbRemote IrrecoWebdbRemote;
#endif /* __WEBDB_REMOTE_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __WEBDB_REMOTE_H__
#define __WEBDB_REMOTE_H__
#include "irreco_webdb.h"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

struct _IrrecoWebdbRemote
{
	gint id;
	GString *creator;
	GString *comment;
	GString *category;
	GString *manufacturer;
	GString *model;
	GString *file_hash;
	GString *file_name;
	GString *uploaded;
	GString *modified;
	GString *downloaded;
	gint download_count;
	GList *configurations;
	GList *themes;
};

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

IrrecoWebdbRemote *irreco_webdb_remote_new();
void irreco_webdb_remote_free(IrrecoWebdbRemote *self);

void irreco_webdb_remote_set(IrrecoWebdbRemote *self,
			    gint id,
			    const char *creator,
			    const char *comment,
			    const char *category,
			    const char *manufacturer,
       			    const char *model,
			    const char *file_hash,
			    const char *file_name,
			    const char *uploaded,
			    const char *modified,
			    const char *downloaded,
			    gint download_count);

void irreco_webdb_remote_add_configuration_id(IrrecoWebdbRemote *self, gint id);

void irreco_webdb_remote_add_theme_id(IrrecoWebdbRemote *self, gint id);

void irreco_webdb_remote_print(IrrecoWebdbRemote *self);

#endif /* __WEBDB_REMOTE_H__ */

/** @} */

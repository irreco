/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007,2008  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoWebdbCache
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoWebdbCache.
 */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_WEBDB_CACHE_H_TYPEDEF__
#define __IRRECO_WEBDB_CACHE_H_TYPEDEF__
typedef struct _IrrecoWebdbCache IrrecoWebdbCache;
#endif /* __IRRECO_WEBDB_CACHE_H_TYPEDEF__ */




/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_WEBDB_CACHE_H__
#define __IRRECO_WEBDB_CACHE_H__
#include "irreco_webdb.h"
#include <irreco_retry_loop.h>
#include "irreco_webdb_conf.h"
#include "irreco_webdb_theme.h"
#include "irreco_webdb_remote.h"
#include "irreco_webdb_client.h"

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

struct _IrrecoWebdbCache
{
	gpointer		 private;
	gboolean		 test_ok;
	IrrecoRetryLoop		*loop;
	IrrecoStringTable	*categories;
	IrrecoStringTable	*remote_categories;
	GString			*error_msg;
	GHashTable		*conf_hash;
	GHashTable		*theme_id_hash;
	GHashTable		*remote_id_hash;
};


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
IrrecoWebdbCache *irreco_webdb_cache_new();
void irreco_webdb_cache_free(IrrecoWebdbCache *self);
const gchar *irreco_webdb_cache_get_error(IrrecoWebdbCache *self);
gboolean irreco_webdb_cache_add_user(IrrecoWebdbCache *self,
				     const gchar *name,
				     const gchar *email,
				     const gchar *passwd);

gboolean irreco_webdb_cache_upload_configuration(IrrecoWebdbCache *self,
						 const gchar *backend,
						 const gchar *category,
						 const gchar *file_hash,
						 const gchar *file_name,
						 const gchar *manufacturer,
						 const gchar *model,
						 const gchar *password,
						 const gchar *user,
						 const gchar *data);

gboolean irreco_webdb_cache_get_categories(IrrecoWebdbCache *self,
					   IrrecoStringTable **categories);

gboolean irreco_webdb_cache_get_all_categories(IrrecoWebdbCache *self,
					       IrrecoStringTable **categories);

gboolean irreco_webdb_cache_get_manufacturers(IrrecoWebdbCache *self,
					const gchar *category,
					IrrecoStringTable **manufacturers);

gboolean irreco_webdb_cache_get_all_manufacturers(IrrecoWebdbCache *self,
					IrrecoStringTable **manufacturers);

gboolean irreco_webdb_cache_get_models(IrrecoWebdbCache *self,
				       const gchar *category,
				       const gchar *manufacturer,
				       IrrecoStringTable **models);

gboolean irreco_webdb_cache_get_configs(IrrecoWebdbCache *self,
					const gchar *category,
					const gchar *manufacturer,
					const gchar *model,
					IrrecoStringTable **configs);

gboolean irreco_webdb_cache_get_configuration(IrrecoWebdbCache *self,
				              gint id,
					      IrrecoWebdbConf **config);

gint irreco_webdb_cache_get_config_id(IrrecoWebdbCache *self,
				      const gchar *file_hash,
				      const gchar *file_name);

gboolean irreco_webdb_cache_get_file(IrrecoWebdbCache *self,
				     const gchar *file_hash,
	 			     const gchar *file_name,
				     GString **file_data);

gboolean irreco_webdb_cache_get_user_exists(IrrecoWebdbCache *self,
					    const gchar *name,
	 				    gboolean *user_exists);

gint irreco_webdb_cache_get_max_image_size(IrrecoWebdbCache *self);
gint irreco_webdb_cache_create_theme(IrrecoWebdbCache *self,
				     const gchar *name,
				     const gchar *comment,
				     const gchar *preview_button,
				     const gchar *folder,
				     const gchar *user,
				     const gchar *password);
gboolean irreco_webdb_cache_set_theme_downloadable(IrrecoWebdbCache *self,
						    gint id,
						    gboolean downloadable,
						    const gchar *user,
						    const gchar *password);
gboolean irreco_webdb_cache_login(IrrecoWebdbCache *self,
				  const gchar *user,
				  const gchar *password);
gboolean irreco_webdb_cache_get_themes(IrrecoWebdbCache *self,
				       IrrecoStringTable **theme);
gboolean irreco_webdb_cache_get_theme_by_id(IrrecoWebdbCache *self,
					    gint theme_id,
					    IrrecoWebdbTheme **theme);
gint irreco_webdb_cache_get_theme_id_by_name_and_date(IrrecoWebdbCache *self,
						      const gchar *name,
						      const gchar *date);
gboolean irreco_webdb_cache_get_preview_button(IrrecoWebdbCache *self,
					       gint theme_id,
					       IrrecoWebdbTheme **theme);
gint irreco_webdb_cache_add_bg_to_theme(IrrecoWebdbCache *self,
					 const gchar *name,
					 const gchar *image_hash,
					 const gchar *image_name,
					 const guchar *image,
					 gint image_len,
					 const gchar *folder,
					 gint theme_id,
					 const gchar *user,
					 const gchar *password);
gboolean irreco_webdb_cache_get_backgrounds(IrrecoWebdbCache *self,
					    gint theme_id,
					    IrrecoStringTable **bg_list);
gboolean irreco_webdb_cache_get_bg_by_id(IrrecoWebdbCache *self,
					 gint bg_id,
					 const char *theme_bg_dir);
gint irreco_webdb_cache_add_button_to_theme(IrrecoWebdbCache *self,
					     const gchar *name,
					     gboolean allow_text,
					     const gchar *text_format_up,
					     const gchar *text_format_down,
					     gint text_padding,
					     gfloat text_h_align,
					     gfloat text_v_align,
					     const gchar *image_up_hash,
					     const gchar *image_up_name,
					     const guchar *image_up,
					     gint image_up_len,
					     const gchar *image_down_hash,
					     const gchar *image_down_name,
					     const guchar *image_down,
					     gint image_down_len,
					     const gchar *folder,
					     gint theme_id,
					     const gchar *user,
					     const gchar *password);
gboolean irreco_webdb_cache_get_buttons(IrrecoWebdbCache *self,
					gint theme_id,
					IrrecoStringTable **button_list);
gboolean irreco_webdb_cache_get_button_by_id(IrrecoWebdbCache *self,
					     gint button_id,
					     const char *theme_button_dir);
gint irreco_webdb_cache_create_new_remote(IrrecoWebdbCache *self,
					  const gchar *comment,
					  const gchar *category,
					  const gchar *manufacturer,
					  const gchar *model,
					  const gchar *file_name,
					  const gchar *file_data,
					  const gchar *user,
					  const gchar *password);
gboolean irreco_webdb_cache_set_remote_downloadable(IrrecoWebdbCache *self,
						    gint id,
						    gboolean downloadable,
						    const gchar *user,
						    const gchar *password);
gboolean irreco_webdb_cache_add_configuration_to_remote(IrrecoWebdbCache *self,
							gint remote_id,
							gint configuration_id,
							const gchar *user,
							const gchar *password);
gboolean irreco_webdb_cache_add_theme_to_remote(IrrecoWebdbCache *self,
						 gint remote_id,
						 gint theme_id,
						 const gchar *user,
						 const gchar *password);
gboolean irreco_webdb_cache_get_remote_categories(IrrecoWebdbCache *self,
						IrrecoStringTable **categories);
gboolean irreco_webdb_cache_get_remote_manufacturers(IrrecoWebdbCache *self,
					const gchar *category,
					IrrecoStringTable **manufacturers);
gboolean irreco_webdb_cache_get_remote_models(IrrecoWebdbCache *self,
					      const gchar *category,
					      const gchar *manufacturer,
					      IrrecoStringTable **models);
gboolean irreco_webdb_cache_get_remote_creators(IrrecoWebdbCache *self,
						const gchar *category,
						const gchar *manufacturer,
						const gchar *model,
						IrrecoStringTable **creators);
gboolean irreco_webdb_cache_get_remotes(IrrecoWebdbCache *self,
					const gchar *category,
					const gchar *manufacturer,
					const gchar *model,
					const gchar *creator,
					GList **remote_list);
gboolean irreco_webdb_cache_get_remote_by_id(IrrecoWebdbCache *self,
					     gint id,
					     IrrecoWebdbRemote **remote);
gboolean irreco_webdb_cache_get_configurations_of_remote(IrrecoWebdbCache *self,
							 gint remote_id,
							 GList **configs);
gboolean irreco_webdb_cache_get_themes_of_remote(IrrecoWebdbCache *self,
						 gint remote_id,
						 GList **themes);
gboolean irreco_webdb_cache_get_remote_data(IrrecoWebdbCache *self,
					    gint remote_id,
					    gchar **file_data);
gboolean irreco_webdb_cache_get_lirc_dirs(IrrecoWebdbCache *self,
					  IrrecoStringTable **dir_list);
gboolean irreco_webdb_cache_get_lirc_manufacturers(IrrecoWebdbCache *self,
						const gchar *range,
						IrrecoStringTable **manufacturer_list);
gboolean irreco_webdb_cache_get_lirc_models(IrrecoWebdbCache *self,
					    const gchar *manufacturer,
					    IrrecoStringTable **model_list);
gboolean irreco_webdb_cache_get_lirc_file(IrrecoWebdbCache *self,
					  const gchar *model,
					  IrrecoStringTable **file);

#endif /* __IRRECO_WEBDB_CACHE_H__ */

/** @} */

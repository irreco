/*
 * irreco - Ir Remote Control
 * Copyright (C) 2008 Joni Kokko (t5kojo01@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_webdb_remote.h"

/**
 * @addtogroup IrrecoWebdbRemote
 * @ingroup IrrecoWebdb
 *
 * Contains information of remote.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoWebdbRemote.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
IrrecoWebdbRemote *irreco_webdb_remote_new()
{
	IrrecoWebdbRemote *self;
	IRRECO_ENTER

	self = g_slice_new0(IrrecoWebdbRemote);

	self->id = 0;
	self->creator = g_string_new("");
	self->comment = g_string_new("");
	self->category = g_string_new("");
	self->manufacturer = g_string_new("");
	self->model = g_string_new("");
	self->file_hash = g_string_new("");
	self->file_name = g_string_new("");
	self->uploaded = g_string_new("");
	self->modified = g_string_new("");
	self->downloaded = g_string_new("");
	self->download_count = 0;

	self->configurations = NULL;

	self->themes = NULL;

	IRRECO_RETURN_PTR(self);
}

void irreco_webdb_remote_free(IrrecoWebdbRemote *self)
{
	IRRECO_ENTER

	g_assert(self != NULL);

	g_string_free(self->creator, TRUE);
	self->creator = NULL;

	g_string_free(self->comment, TRUE);
	self->comment = NULL;

	g_string_free(self->category, TRUE);
	self->category = NULL;

	g_string_free(self->manufacturer, TRUE);
	self->manufacturer = NULL;

	g_string_free(self->model, TRUE);
	self->model = NULL;

	g_string_free(self->file_hash, TRUE);
	self->file_hash = NULL;

	g_string_free(self->file_name, TRUE);
	self->file_name = NULL;

	g_string_free(self->uploaded, TRUE);
	self->uploaded = NULL;

	g_string_free(self->modified, TRUE);
	self->modified = NULL;

	g_string_free(self->downloaded, TRUE);
	self->downloaded = NULL;

	if (self->configurations != NULL) {
		g_list_free(self->configurations);
		self->configurations = NULL;
	}
	if (self->themes != NULL) {
		g_list_free(self->themes);
		self->themes = NULL;
	}

	g_slice_free(IrrecoWebdbRemote, self);
	self = NULL;

	IRRECO_RETURN
}

/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Functions                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

void irreco_webdb_remote_set(IrrecoWebdbRemote *self,
			    gint id,
			    const char *creator,
			    const char *comment,
			    const char *category,
			    const char *manufacturer,
       			    const char *model,
			    const char *file_hash,
			    const char *file_name,
			    const char *uploaded,
			    const char *modified,
			    const char *downloaded,
			    gint download_count)
{
	IRRECO_ENTER

	g_assert(self != NULL);

	self->id = id;
	g_string_printf(self->creator, "%s", creator);
	g_string_printf(self->comment, "%s", comment);
	g_string_printf(self->category, "%s", category);
	g_string_printf(self->manufacturer, "%s", manufacturer);
	g_string_printf(self->model, "%s", model);
	g_string_printf(self->file_hash, "%s", file_hash);
	g_string_printf(self->file_name, "%s", file_name);
	g_string_printf(self->uploaded, "%s", uploaded);
	g_string_printf(self->modified, "%s", modified);
	g_string_printf(self->downloaded, "%s", downloaded);
	self->download_count = download_count;

	IRRECO_RETURN
}

void irreco_webdb_remote_add_configuration_id(IrrecoWebdbRemote *self, gint id)
{
	IRRECO_ENTER

	g_assert(self != NULL);

	self->configurations = g_list_append(self->configurations,
					     GINT_TO_POINTER(id));

	self->configurations = g_list_first(self->configurations);

	IRRECO_RETURN
}

void irreco_webdb_remote_add_theme_id(IrrecoWebdbRemote *self, gint id)
{
	IRRECO_ENTER

	g_assert(self != NULL);

	self->themes = g_list_append(self->themes, GINT_TO_POINTER(id));

	self->themes = g_list_first(self->themes);

	IRRECO_RETURN
}

void irreco_webdb_remote_print(IrrecoWebdbRemote *self)
{
	GList *configurations;
	GList *themes;
	IRRECO_ENTER

	g_assert(self != NULL);

	IRRECO_PRINTF("ID: %d\n", self->id);
	IRRECO_PRINTF("Creator: %s\n", self->creator->str);
	IRRECO_PRINTF("Comment: %s\n", self->comment->str);
	IRRECO_PRINTF("Category: %s\n", self->category->str);
	IRRECO_PRINTF("Manufacturer: %s\n", self->manufacturer->str);
	IRRECO_PRINTF("Model: %s\n", self->model->str);
	IRRECO_PRINTF("File hash: %s\n", self->file_hash->str);
	IRRECO_PRINTF("File name: %s\n", self->file_name->str);
	IRRECO_PRINTF("Uploaded: %s\n", self->uploaded->str);
	IRRECO_PRINTF("Modified: %s\n", self->modified->str);
	IRRECO_PRINTF("Last download: %s\n", self->downloaded->str);
	IRRECO_PRINTF("Download_count: %d\n", self->download_count);

	configurations = g_list_first(self->configurations);
	if (configurations != NULL) IRRECO_PRINTF("Configurations:");
	while(configurations) {
		g_print(" (%d)", GPOINTER_TO_INT(configurations->data));

		configurations = configurations->next;
	}

	themes = g_list_first(self->themes);
	if (themes != NULL) IRRECO_PRINTF("Themes:");
	while(themes) {
		g_print(" (%d)", GPOINTER_TO_INT(themes->data));

		themes = themes->next;
	}
	g_print("\n");

	IRRECO_RETURN
}

/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Events and Callbacks                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/** @} */

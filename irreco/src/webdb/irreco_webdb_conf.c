/*
 * irreco - Ir Remote Control
 * Copyright (C) 2008 Joni Kokko (t5kojo01@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_webdb_conf.h"

/**
 * @addtogroup IrrecoWebdbConf
 * @ingroup IrrecoWebdb
 *
 * @todo PURPOCE OF CLASS
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoWebdbConf.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
IrrecoWebdbConf *irreco_webdb_conf_new()
{
	IrrecoWebdbConf *self;
	IRRECO_ENTER

	self = g_slice_new0(IrrecoWebdbConf);

	self->id = 1;
	self->user = g_string_new("");
	self->backend = g_string_new("");
	self->category = g_string_new("");
	self->manufacturer = g_string_new("");
	self->model = g_string_new("");
	self->file_hash = g_string_new("");
	self->file_name = g_string_new("");
	self->file_uploaded = g_string_new("");
	self->file_download_count = g_string_new("");

	IRRECO_RETURN_PTR(self);
}

void irreco_webdb_conf_free(IrrecoWebdbConf *self)
{
	IRRECO_ENTER

	g_assert(self != NULL);

	g_string_free(self->user, TRUE);
	self->user = NULL;

	g_string_free(self->backend, TRUE);
	self->backend = NULL;

	g_string_free(self->category, TRUE);
	self->category = NULL;

	g_string_free(self->manufacturer, TRUE);
	self->manufacturer = NULL;

	g_string_free(self->model, TRUE);
	self->model = NULL;

	g_string_free(self->file_hash, TRUE);
	self->file_hash = NULL;

	g_string_free(self->file_name, TRUE);
	self->file_name = NULL;

	g_string_free(self->file_uploaded, TRUE);
	self->file_uploaded = NULL;

	g_string_free(self->file_download_count, TRUE);
	self->file_download_count = NULL;

	g_slice_free(IrrecoWebdbConf, self);
	self = NULL;

	IRRECO_RETURN
}

/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Functions                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

void irreco_webdb_conf_set(IrrecoWebdbConf *self,
			   gint       id,
			   const char *user,
			   const char *backend,
			   const char *category,
			   const char *manufacturer,
			   const char *model,
			   const char *file_hash,
			   const char *file_name,
			   const char *file_uploaded,
			   const char *file_download_count)
{
	IRRECO_ENTER

	self->id = id;
	g_string_printf(self->user,"%s",user);
	g_string_printf(self->backend,"%s",backend);
	g_string_printf(self->category,"%s",category);
	g_string_printf(self->manufacturer,"%s",manufacturer);
	g_string_printf(self->model,"%s",model);
	g_string_printf(self->file_hash,"%s",file_hash);
	g_string_printf(self->file_name,"%s",file_name);
	g_string_printf(self->file_uploaded,"%s",file_uploaded);
	g_string_printf(self->file_download_count,"%s",file_download_count);

	IRRECO_RETURN
}

void irreco_webdb_conf_print(IrrecoWebdbConf *self)
{
	IRRECO_PRINTF("Configuration: %d %s %s %s %s %s %s %s %s %s\n",
		     self->id, self->user->str, self->backend->str,
       		     self->category->str, self->manufacturer->str,
	             self->model->str, self->file_hash->str,
	      	     self->file_name->str, self->file_uploaded->str,
	     	     self->file_download_count->str);
}

/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Events and Callbacks                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/** @} */

/*
 * irreco - Ir Remote Control
 * Copyright (C) 2008 Joni Kokko (t5kojo01@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoWebdbConf
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoWebdbConf.
 */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __WEBDB_CONF_H_TYPEDEF__
#define __WEBDB_CONF_H_TYPEDEF__
typedef struct _IrrecoWebdbConf IrrecoWebdbConf;
#endif /* __WEBDB_CONF_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __WEBDB_CONF_H__
#define __WEBDB_CONF_H__
#include "irreco_webdb.h"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

struct _IrrecoWebdbConf
{
	gint	 id;
	GString	*user;
	GString *backend;
	GString *category;
	GString *manufacturer;
	GString *model;
	GString *file_hash;
	GString	*file_name;
	GString *file_uploaded;
	GString *file_download_count;
};

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

IrrecoWebdbConf *irreco_webdb_conf_new();
void irreco_webdb_conf_free(IrrecoWebdbConf *self);

void irreco_webdb_conf_set(IrrecoWebdbConf *self,
			   gint       id,
			   const char *user,
			   const char *backend,
			   const char *category,
			   const char *manufacturer,
			   const char *model,
			   const char *file_hash,
			   const char *file_name,
			   const char *file_uploaded,
			   const char *file_download_count);

void irreco_webdb_conf_print(IrrecoWebdbConf *self);
#endif /* __WEBDB_CONF_H__ */

/** @] */

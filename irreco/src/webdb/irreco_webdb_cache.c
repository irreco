/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007,2008  Arto Karppinen (arto.karppinen@iki.fi),
 * Joni Kokko (t5kojo01@students.oamk.fi),
 * Sami Mäki (kasmra@xob.kapsi.fi),
 * Harri Vattulainen (t5vaha01@students.oamk.fi),
 * Pekka Gehör (pegu6@msn.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_webdb_cache.h"
#include "irreco_webdb_client.h"

/**
 * @addtogroup IrrecoWebdbCache
 * @ingroup IrrecoWebdb
 *
 * Caches XML-RPC calls, enables error handling and retries failed calls.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoWebdbCache.
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

/**
 * Create new IrrecoWebdb Object
 */
IrrecoWebdbCache *irreco_webdb_cache_new()
{
	IrrecoWebdbCache *self;
	IRRECO_ENTER

	self = g_slice_new0(IrrecoWebdbCache);
	self->private = irreco_webdb_client_new();
	self->remote_categories = NULL;
	self->error_msg = g_string_new("");
	self->loop = irreco_retry_loop_new(IRRECO_SECONDS_TO_USEC(0.3),
					   IRRECO_SECONDS_TO_USEC(3));
	self->conf_hash = g_hash_table_new_full(g_int_hash, g_int_equal, NULL,
		(GDestroyNotify) irreco_webdb_conf_free);

	self->theme_id_hash = g_hash_table_new_full(g_int_hash, g_int_equal, NULL,
		(GDestroyNotify) irreco_webdb_theme_free);

	self->remote_id_hash = g_hash_table_new_full(g_int_hash, g_int_equal,
		NULL, (GDestroyNotify) irreco_webdb_remote_free);

	IRRECO_RETURN_PTR(self);
}

/**
 * Free resources of IrrecoWebdbCache Object.
 */
void irreco_webdb_cache_free(IrrecoWebdbCache *self)
{
	IRRECO_ENTER
	g_string_free(self->error_msg, TRUE);
	self->error_msg = NULL;
	irreco_retry_loop_free(self->loop);
	self->loop = NULL;
	if (self->remote_categories != NULL) {
		irreco_string_table_free(self->remote_categories);
		self->remote_categories = NULL;
	}
	irreco_webdb_client_free(self->private);
	self->private = NULL;
	g_slice_free(IrrecoWebdbCache, self);
	if (self->conf_hash != NULL) {
		g_hash_table_destroy(self->conf_hash);
		self->conf_hash = NULL;
	}
	if (self->theme_id_hash != NULL) {
		g_hash_table_destroy(self->theme_id_hash);
		self->theme_id_hash = NULL;
	}
	if (self->remote_id_hash != NULL) {
		g_hash_table_destroy(self->remote_id_hash);
		self->remote_id_hash = NULL;
	}
	IRRECO_RETURN
}

/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Private Functions
 * @{
 */

/**
 * A simple test on Irreco WebDB to test if WebDB returns correct and
 * meaningfull results.
 *
 * This function generates two numbers and asks WebDB to sum them. If the sum
 * if correct, then our connection to the WebDB seems to be working.
 *
 * @return TRUE on success, FALSE otherwise.
 */
static gboolean irreco_webdb_cache_test(IrrecoWebdbCache *self)
{
	IrrecoWebdbClient *client;
	GRand *rand;
	gint32 num_a;
	gint32 num_b;
	glong sum;
	IRRECO_ENTER

	if (self->test_ok) IRRECO_RETURN_BOOL(self->test_ok);

	rand = g_rand_new();
	client = (IrrecoWebdbClient *) self->private;

	IRRECO_RETRY_LOOP_START(self->loop)
		num_a = g_rand_int_range(rand, 1, 1000);
		num_b = g_rand_int_range(rand, 1, 1000);
		self->test_ok = irreco_webdb_client_sum(
			client, num_a, num_b, &sum);

		if (self->test_ok) {
			if (sum == num_a + num_b) {
				break;
			} else {
				g_string_printf(self->error_msg,
						"Got invalid result from "
						"sum method. %i + %i = %i. "
						"Expected %i.",
						num_a, num_b, (gint) sum,
						num_a + num_b);
				IRRECO_PRINTF("%s\n", self->error_msg->str);
				self->test_ok = FALSE;
			}
		} else {
			irreco_webdb_client_get_error_msg(
				client, self->error_msg);
		}
	IRRECO_RETRY_LOOP_END(self->loop)

	g_rand_free(rand);
	IRRECO_RETURN_BOOL(self->test_ok);
}

/**
 * Shows error message in case categories list is empty.
 *
 * @return TRUE if categories is not empty, FALSE otherwise.
 */
static gboolean irreco_webdb_cache_verify_category(IrrecoWebdbCache *self)
{
	IRRECO_ENTER
	if (self->categories == NULL) {
		g_string_printf(self->error_msg, "list of categories is NULL");
		IRRECO_PRINTF("%s\n", self->error_msg->str);
		IRRECO_RETURN_BOOL(FALSE)
	}
	IRRECO_RETURN_BOOL(TRUE)
}

/**
 * Shows error message in case manufacturers list is empty.
 *
 * @return TRUE if manufacturers list contains data, FALSE otherwise.
 */
static gboolean
irreco_webdb_cache_verify_manufacturer(IrrecoWebdbCache *self,
				       const gchar *category,
				       IrrecoStringTable **manufactures)
{
	IRRECO_ENTER
	if (!irreco_string_table_get(self->categories, category,
				     (gpointer*) manufactures)) {
		g_string_printf(self->error_msg,
				"list of manufacturers is NULL");
		IRRECO_PRINTF("%s\n", self->error_msg->str);
		IRRECO_RETURN_BOOL(FALSE)
	}
	IRRECO_RETURN_BOOL(TRUE)
}


/**
 * Shows error message in case models list is empty.
 *
 * @return TRUE if models list contains data, FALSE otherwise.
 */
static gboolean irreco_webdb_cache_verify_model(IrrecoWebdbCache *self,
						IrrecoStringTable *manuf_list,
						const gchar *manufacturer,
						IrrecoStringTable **models)
{
	IRRECO_ENTER
	if (!irreco_string_table_get(manuf_list, manufacturer,
				     (gpointer*) models)) {
		g_string_printf(self->error_msg, "list of models is NULL");
		IRRECO_PRINTF("%s\n", self->error_msg->str);
		IRRECO_RETURN_BOOL(FALSE)
	}
	IRRECO_RETURN_BOOL(TRUE)
}

/**
 * Shows error message in case configurations list is empty.
 *
 * @return TRUE if configurations list contains data, FALSE otherwise.
 */
static gboolean irreco_webdb_cache_verify_config(IrrecoWebdbCache *self,
						 IrrecoStringTable *models,
						 const gchar *model,
						 IrrecoStringTable **configs)
{
	IRRECO_ENTER
	if (!irreco_string_table_get(models, model, (gpointer*) configs)) {
		g_string_printf(self->error_msg,
				"list of configurations is NULL");
		IRRECO_PRINTF("%s\n", self->error_msg->str);
		IRRECO_RETURN_BOOL(FALSE)
	}
	IRRECO_RETURN_BOOL(TRUE)
}

/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Functions                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */


/**
 * Adds new user (name, email, password) to database.
 *
 * @return TRUE if user is added succesfully, FALSE otherwise.
 */
gboolean irreco_webdb_cache_add_user(IrrecoWebdbCache *self,
				     const gchar *name,
				     const gchar *email,
				     const gchar *passwd)
{
	IrrecoWebdbClient *client;

	IRRECO_ENTER

	client = (IrrecoWebdbClient *) self->private;

	IRRECO_RETRY_LOOP_START(self->loop)
		/* test connection to webdb */
		if (irreco_webdb_cache_test(self) == FALSE){
			g_string_printf(self->error_msg,
						"Failed cache self test.");
			IRRECO_PRINTF("%s\n", self->error_msg->str);
			IRRECO_RETURN_BOOL(FALSE);
		}

		if(irreco_webdb_client_add_user(client, name, email, passwd)) {
			IRRECO_RETURN_BOOL(TRUE);
		} else {
			/* irreco_webdb_client_add_user failed, get err msg */
			irreco_webdb_client_get_error_msg(client,
							  self->error_msg);
			IRRECO_RETURN_BOOL(FALSE);
		}
	IRRECO_RETRY_LOOP_END(self->loop)

	IRRECO_RETURN_BOOL(FALSE);
}

/**
 * Adds new configuration to database.
 *
 * @return TRUE if configuration is uploaded succesfully, FALSE otherwise.
 */
gboolean irreco_webdb_cache_upload_configuration(IrrecoWebdbCache *self,
						 const gchar *backend,
						 const gchar *category,
						 const gchar *file_hash,
						 const gchar *file_name,
						 const gchar *manufacturer,
						 const gchar *model,
						 const gchar *password,
						 const gchar *user,
						 const gchar *data)
{
	IrrecoWebdbClient *client;

	IRRECO_ENTER

	client = (IrrecoWebdbClient *) self->private;

	IRRECO_RETRY_LOOP_START(self->loop)
		/* test connection to webdb */
		if (irreco_webdb_cache_test(self) == FALSE){
			g_string_printf(self->error_msg,
						"Failed cache self test.");
			IRRECO_PRINTF("%s\n", self->error_msg->str);
			IRRECO_RETURN_BOOL(FALSE);
		}

		if(irreco_webdb_client_upload_configuration(client, backend,
					category, file_hash, file_name,
					manufacturer, model, password, user,
					data)) {

			IRRECO_RETURN_BOOL(TRUE);
		} else {
			/* irreco_webdb_client_upload_configuration failed,
			   get err msg */
			irreco_webdb_client_get_error_msg(client,
							  self->error_msg);
			IRRECO_RETURN_BOOL(FALSE);
		}
	IRRECO_RETRY_LOOP_END(self->loop)

	IRRECO_RETURN_BOOL(FALSE);
}

/**
 * Extracts error message from IrrecoWebdbCache object.
 *
 * @return error message as string.
 */
const gchar *irreco_webdb_cache_get_error(IrrecoWebdbCache *self)
{
	IRRECO_ENTER
	IRRECO_RETURN_STR(self->error_msg->str);
}

/**
 * Fetches categories from DB
 *
 * @return TRUE if categories are fetched succesfully, FALSE otherwise.
 */
gboolean irreco_webdb_cache_get_categories(IrrecoWebdbCache *self,
					   IrrecoStringTable **categories)
{
	IRRECO_ENTER

	if (self->categories == NULL) {
		gboolean success = FALSE;
		IrrecoWebdbClient *client = (IrrecoWebdbClient *) self->private;

		IRRECO_RETRY_LOOP_START(self->loop)
			if (irreco_webdb_cache_test(self) == FALSE) break;
			success = irreco_webdb_client_get_categories(
				client, &self->categories);
			if (success) {
			    break;
			}
			irreco_webdb_client_get_error_msg(
				client, self->error_msg);
		IRRECO_RETRY_LOOP_END(self->loop)

		if (success == FALSE) IRRECO_RETURN_BOOL(FALSE);

		irreco_string_table_sort_abc (self->categories);
	}

	*categories = self->categories;
	IRRECO_RETURN_BOOL(TRUE)
}

/**
 * Fetches all categories from DB.
 *
 * @param all_categories must be freed if return value is TRUE.
 * @return TRUE if categories are fetched succesfully, FALSE otherwise.
 */
gboolean irreco_webdb_cache_get_all_categories(IrrecoWebdbCache *self,
					   IrrecoStringTable **all_categories)
{
	gboolean success = FALSE;
	IrrecoWebdbClient *client = (IrrecoWebdbClient *) self->private;
	IRRECO_ENTER


	IRRECO_RETRY_LOOP_START(self->loop)
		if (irreco_webdb_cache_test(self) == FALSE) break;
		success = irreco_webdb_client_get_all_categories(
			client, all_categories);
		if (success) {
		    break;
		}
		irreco_webdb_client_get_error_msg(
			client, self->error_msg);
	IRRECO_RETRY_LOOP_END(self->loop)

	if (success == FALSE) IRRECO_RETURN_BOOL(FALSE);

	irreco_string_table_sort_abc(*all_categories);

	IRRECO_RETURN_BOOL(TRUE)
}

/**
 * Fetches manufacturers from DB.
 *
 * @param category contains category in which type manufacturers are returned.
 * @return TRUE if manufacturers are fetched succesfully, FALSE otherwise.
 */
gboolean irreco_webdb_cache_get_manufacturers(IrrecoWebdbCache *self,
					      const gchar *category,
					      IrrecoStringTable **manufacturers)
{
	IrrecoStringTable *manufacturer_list;
	IRRECO_ENTER

	if (!irreco_webdb_cache_verify_category(self) ||
	    !irreco_webdb_cache_verify_manufacturer(self, category,
						    &manufacturer_list)) {
		IRRECO_RETURN_BOOL(FALSE)
	}

	if(manufacturer_list == NULL) {
		gboolean success = FALSE;
		IrrecoWebdbClient *client = (IrrecoWebdbClient *) self->private;

		IRRECO_RETRY_LOOP_START(self->loop)
			if (irreco_webdb_cache_test(self) == FALSE) break;
			success = irreco_webdb_client_get_manufacturers(
			client, category, &manufacturer_list);

			if (success) break;
			irreco_webdb_client_get_error_msg(
				client, self->error_msg);
		IRRECO_RETRY_LOOP_END(self->loop)

		if (success == FALSE) IRRECO_RETURN_BOOL(FALSE);

		irreco_string_table_sort_abc (manufacturer_list);
		irreco_string_table_change_data(self->categories, category,
						manufacturer_list);
	}

	irreco_string_table_get(self->categories, category,
				(gpointer *) manufacturers);
	IRRECO_RETURN_BOOL(TRUE)
}

/**
 * Fetches all manufacturers from DB.
 *
 * @param all_manufacturers must be released if function returns TRUE.
 * @return TRUE if manufacturers are fetched succesfully, FALSE otherwise.
 */
gboolean irreco_webdb_cache_get_all_manufacturers(IrrecoWebdbCache *self,
					      IrrecoStringTable **all_manufacturers)
{
	gboolean success = FALSE;
	IrrecoWebdbClient *client = (IrrecoWebdbClient *) self->private;
	IRRECO_ENTER


	IRRECO_RETRY_LOOP_START(self->loop)
		if (irreco_webdb_cache_test(self) == FALSE) break;
		success = irreco_webdb_client_get_all_manufacturers(
			client, all_manufacturers);
		if (success) {
		    break;
		}
		irreco_webdb_client_get_error_msg(
			client, self->error_msg);
	IRRECO_RETRY_LOOP_END(self->loop)

	if (success == FALSE) IRRECO_RETURN_BOOL(FALSE);

	irreco_string_table_sort_abc(*all_manufacturers);

	IRRECO_RETURN_BOOL(TRUE)
}

/**
 * Fetches models belonging given category and manufacturer.
 *
 * @param models must be freed if function returns TRUE.
 * @return TRUE if models are fetched succesfully, FALSE otherwise.
 */
gboolean irreco_webdb_cache_get_models(IrrecoWebdbCache *self,
				       const gchar *category,
				       const gchar *manufacturer,
				       IrrecoStringTable **models)
{
	IrrecoStringTable *model_list;
	IrrecoStringTable *manufacturer_list;
	IRRECO_ENTER

	if (!irreco_webdb_cache_verify_category(self) ||
	    !irreco_webdb_cache_verify_manufacturer(self, category,
						    &manufacturer_list) ||
	    !irreco_webdb_cache_verify_model(self, manufacturer_list,
					     manufacturer, &model_list)) {
		IRRECO_RETURN_BOOL(FALSE)
	}

	if(model_list == NULL) {
		gboolean success = FALSE;
		IrrecoWebdbClient *client = (IrrecoWebdbClient *) self->private;

		IRRECO_RETRY_LOOP_START(self->loop)
			if (irreco_webdb_cache_test(self) == FALSE) break;
			success = irreco_webdb_client_get_models(
				client, category, manufacturer, &model_list);

			if (success) break;
			irreco_webdb_client_get_error_msg(
				client, self->error_msg);
		IRRECO_RETRY_LOOP_END(self->loop)

		if (success == FALSE) IRRECO_RETURN_BOOL(FALSE);

		irreco_string_table_sort_abc (model_list);
		irreco_string_table_change_data(manufacturer_list, manufacturer,
						model_list);
	}

	irreco_string_table_get(manufacturer_list, manufacturer,
				(gpointer *) models);
	IRRECO_RETURN_BOOL(TRUE)
}

/**
 * Fetches configs belonging given category, manufacturer and model.
 *
 * @return TRUE if configs are fetched succesfully, FALSE otherwise.
 */
gboolean irreco_webdb_cache_get_configs(IrrecoWebdbCache *self,
				        const gchar *category,
	   			        const gchar *manufacturer,
	      			        const gchar *model,
					IrrecoStringTable **configs)
{
	IrrecoStringTable * config_list;
	IrrecoStringTable * model_list;
	IrrecoStringTable * manufacturer_list;
	IRRECO_ENTER

	if (!irreco_webdb_cache_verify_category(self) ||
	    !irreco_webdb_cache_verify_manufacturer(self, category,
						    &manufacturer_list) ||
	    !irreco_webdb_cache_verify_model(self, manufacturer_list,
					     manufacturer, &model_list) ||
	    !irreco_webdb_cache_verify_config(self, model_list, model,
					      &config_list)) {
		IRRECO_RETURN_BOOL(FALSE)
	}

	if(config_list == NULL) {
		gboolean success = FALSE;
		IrrecoWebdbClient *client = (IrrecoWebdbClient *) self->private;

		IRRECO_RETRY_LOOP_START(self->loop)
			if (irreco_webdb_cache_test(self) == FALSE) break;
			success = irreco_webdb_client_get_configs(client,
				manufacturer, model, &config_list);

			if (success) break;
			irreco_webdb_client_get_error_msg(
				client, self->error_msg);
		IRRECO_RETRY_LOOP_END(self->loop)

		if (success == FALSE) IRRECO_RETURN_BOOL(FALSE);

		irreco_string_table_change_data(model_list, model, config_list);
	}

	irreco_string_table_get(model_list, model, (gpointer *) configs);
	IRRECO_RETURN_BOOL(TRUE)
}

/**
 * Fetches configuration by given ID.
 *
 * @return TRUE if configuration is fetched succesfully, FALSE otherwise.
 */
gboolean irreco_webdb_cache_get_configuration(IrrecoWebdbCache *self,
					      gint id,
					      IrrecoWebdbConf **config)
{
	IrrecoWebdbConf	* configuration;
	IRRECO_ENTER

	configuration = irreco_webdb_conf_new();

	if (g_hash_table_lookup(self->conf_hash, (gconstpointer) &id) == NULL) {
		gboolean success = FALSE;
		IrrecoWebdbClient *client = (IrrecoWebdbClient *) self->private;

		IRRECO_RETRY_LOOP_START(self->loop)
			if (irreco_webdb_cache_test(self) == FALSE) break;
			success = irreco_webdb_client_get_configuration(
				client, id, &configuration);

			if (success) break;
			irreco_webdb_client_get_error_msg(client,
							  self->error_msg);
		IRRECO_RETRY_LOOP_END(self->loop)

		if (success == FALSE) IRRECO_RETURN_BOOL(FALSE);

		g_hash_table_insert(self->conf_hash,
				    (gpointer) &configuration->id,
				    (gpointer) configuration);
	}

	*config = g_hash_table_lookup(self->conf_hash, (gconstpointer) &id);

	IRRECO_RETURN_BOOL(TRUE)
}

gint irreco_webdb_cache_get_config_id(IrrecoWebdbCache *self,
				      const gchar *file_hash,
				      const gchar *file_name)
{
	IRRECO_ENTER
	IRRECO_RETURN_INT(
		irreco_webdb_client_get_config_id_by_file_hash_and_file_name(
					self->private, file_hash, file_name));
}

/**
 * Fetches file by given hash and name.
 *.
 * @return TRUE if configuration is fetched succesfully, FALSE otherwise.
 */
gboolean irreco_webdb_cache_get_file(IrrecoWebdbCache *self,
				     const gchar *file_hash,
	 			     const gchar *file_name,
				     GString **file_data)
{
	GString *file;
	gboolean success = FALSE;
	IrrecoWebdbClient *client;
	IRRECO_ENTER

	client = (IrrecoWebdbClient *) self->private;

	IRRECO_RETRY_LOOP_START(self->loop)
		if (irreco_webdb_cache_test(self) == FALSE) break;
		success = irreco_webdb_client_get_file(
			client, file_hash, file_name, &file);

		if (success) break;
		irreco_webdb_client_get_error_msg(client, self->error_msg);
	IRRECO_RETRY_LOOP_END(self->loop)

	if (success == FALSE) IRRECO_RETURN_BOOL(FALSE);

	*file_data = file;

	IRRECO_RETURN_BOOL(TRUE)
}

/**
 * Check if user with given name is already at DB.
 *
 *.@param user_exists will contain info wether user name was at DB.
 * @return TRUE if test is done succesfully, FALSE otherwise.
 */
gboolean irreco_webdb_cache_get_user_exists(IrrecoWebdbCache *self,
					const gchar *name,
					gboolean *user_exists)
{
	IrrecoWebdbClient *client;
	gboolean success = FALSE;

	IRRECO_ENTER
	client = (IrrecoWebdbClient *) self->private;
	IRRECO_RETRY_LOOP_START(self->loop)
		/* test connection to webdb */
		if (irreco_webdb_cache_test(self) == FALSE) break;

		success = irreco_webdb_client_get_user_exists(client,
							      name,
							      user_exists);

		if (success) break;

		/* irreco_webdb_client_get_user_exists failed, get err msg */
		irreco_webdb_client_get_error_msg(client, self->error_msg);
	IRRECO_RETRY_LOOP_END(self->loop)

	if (success == FALSE) IRRECO_RETURN_BOOL(FALSE);

	IRRECO_RETURN_BOOL(TRUE)
}

/**
 * Get maximum image size accepted by database
 */
gint irreco_webdb_cache_get_max_image_size(IrrecoWebdbCache *self)
{
	IrrecoWebdbClient *client;
	IRRECO_ENTER
	client = (IrrecoWebdbClient *) self->private;
	IRRECO_RETURN_INT(irreco_webdb_client_get_max_image_size(client));
}

/**
 * Create theme
 */
gint irreco_webdb_cache_create_theme(IrrecoWebdbCache *self,
				     const gchar *name,
				     const gchar *comment,
				     const gchar *preview_button,
				     const gchar *folder,
				     const gchar *user,
				     const gchar *password)
{
	IrrecoWebdbClient *client;
	IRRECO_ENTER
	client = (IrrecoWebdbClient *) self->private;
	IRRECO_RETURN_INT(irreco_webdb_client_create_theme(
						client,
						name,
						comment,
						preview_button,
						folder,
						user,
						password));
}

/**
 * Set theme downloadable
 */
gboolean irreco_webdb_cache_set_theme_downloadable(IrrecoWebdbCache *self,
						    gint id,
						    gboolean downloadable,
						    const gchar *user,
						    const gchar *password)
{
	IrrecoWebdbClient *client;
	IRRECO_ENTER
	client = (IrrecoWebdbClient *) self->private;
	IRRECO_RETURN_BOOL(irreco_webdb_client_set_theme_downloadable(
								client,
								id,
								downloadable,
								user,
								password));
}

/**
 * Login to database.
 *
 * @return TRUE if login is succesful, FALSE otherwise.
 */
gboolean irreco_webdb_cache_login(IrrecoWebdbCache *self,
						 const gchar *user,
						 const gchar *password)
{
	IrrecoWebdbClient *client;

	IRRECO_ENTER

	client = (IrrecoWebdbClient *) self->private;

	IRRECO_RETRY_LOOP_START(self->loop)
		/* test connection to webdb */
		if (irreco_webdb_cache_test(self) == FALSE){
			g_string_printf(self->error_msg,
						"Failed cache self test.");
			IRRECO_PRINTF("%s\n", self->error_msg->str);
			IRRECO_RETURN_BOOL(FALSE);
		}

		if(irreco_webdb_client_login(client, user, password)) {

			IRRECO_RETURN_BOOL(TRUE);
		} else {
			/* irreco_webdb_client_login failed,
			   get err msg */
			irreco_webdb_client_get_error_msg(client,
							  self->error_msg);
			IRRECO_RETURN_BOOL(FALSE);
		}
	IRRECO_RETRY_LOOP_END(self->loop)

	IRRECO_RETURN_BOOL(FALSE);
}

gboolean irreco_webdb_cache_get_themes(IrrecoWebdbCache *self,
				       IrrecoStringTable **theme)
{
	gboolean success = FALSE;
	IrrecoWebdbClient *client = (IrrecoWebdbClient *) self->private;
	IRRECO_ENTER

	IRRECO_RETRY_LOOP_START(self->loop)
		if (irreco_webdb_cache_test(self) == FALSE) break;
		success = irreco_webdb_client_get_themes(client, theme);

		if (success) break;
		irreco_webdb_client_get_error_msg(client, self->error_msg);
	IRRECO_RETRY_LOOP_END(self->loop)

	IRRECO_RETURN_BOOL(success)
}

gboolean irreco_webdb_cache_get_theme_by_id(IrrecoWebdbCache *self,
					     gint theme_id,
					     IrrecoWebdbTheme **theme)
{
	IrrecoWebdbTheme *get_theme;
	IRRECO_ENTER


	if (g_hash_table_lookup(self->theme_id_hash,
			       (gconstpointer) &theme_id) == NULL) {
		gboolean success = FALSE;
		IrrecoWebdbClient *client = (IrrecoWebdbClient *) self->private;

		IRRECO_RETRY_LOOP_START(self->loop)
			if (irreco_webdb_cache_test(self) == FALSE) break;
			success = irreco_webdb_client_get_theme_by_id(
				client, theme_id, &get_theme);

			if (success) break;
			irreco_webdb_client_get_error_msg(client,
							  self->error_msg);
		IRRECO_RETRY_LOOP_END(self->loop)

		if (success == FALSE) IRRECO_RETURN_BOOL(FALSE);

		g_hash_table_insert(self->theme_id_hash,
				    (gpointer) &get_theme->id,
				    (gpointer) get_theme);
	}

	*theme = g_hash_table_lookup(self->theme_id_hash, (gconstpointer) &theme_id);

	IRRECO_RETURN_BOOL(TRUE)
}

gint irreco_webdb_cache_get_theme_id_by_name_and_date(IrrecoWebdbCache *self,
						      const gchar *name,
						      const gchar *date)
{
	IRRECO_ENTER
	IRRECO_RETURN_INT(irreco_webdb_client_get_theme_id_by_name_and_date(
			  self->private, name, date));
}


gboolean irreco_webdb_cache_get_preview_button(IrrecoWebdbCache *self,
						gint theme_id,
						IrrecoWebdbTheme **theme)
{
	gboolean success = TRUE;
	IrrecoWebdbClient *client = (IrrecoWebdbClient *) self->private;
	IRRECO_ENTER

	*theme = g_hash_table_lookup(self->theme_id_hash,
				    (gconstpointer) &theme_id);
	if (*theme == NULL) {
		success = irreco_webdb_cache_get_theme_by_id(self, theme_id,
							     theme);
		if (success == FALSE) {
			irreco_webdb_client_get_error_msg(client, self->error_msg);
			goto end;
		}
	}

	if ((*theme)->preview_button == NULL)
	{
		IRRECO_RETRY_LOOP_START(self->loop)

		if (irreco_webdb_cache_test(self) == FALSE) break;

		success = irreco_webdb_client_get_preview_button(client,
					theme_id, &(*theme)->preview_button);

		if (success) break;

		irreco_webdb_client_get_error_msg(client, self->error_msg);
		IRRECO_RETRY_LOOP_END(self->loop)
	}

	end:

	IRRECO_RETURN_BOOL(success)
}

/**
 * Add bg to theme
 */
gint irreco_webdb_cache_add_bg_to_theme(IrrecoWebdbCache *self,
					 const gchar *name,
					 const gchar *image_hash,
					 const gchar *image_name,
					 const guchar *image,
					 gint image_len,
					 const gchar *folder,
					 gint theme_id,
					 const gchar *user,
					 const gchar *password)
{
	IrrecoWebdbClient *client;
	IRRECO_ENTER
	client = (IrrecoWebdbClient *) self->private;
	IRRECO_RETURN_INT(irreco_webdb_client_add_bg_to_theme(
								client,
								name,
								image_hash,
								image_name,
								image,
								image_len,
								folder,
								theme_id,
								user,
								password));
}

gboolean irreco_webdb_cache_get_backgrounds(IrrecoWebdbCache *self,
					     gint theme_id,
					     IrrecoStringTable **bg_list){
	IrrecoWebdbClient *client;
	gboolean success = TRUE;
	IRRECO_ENTER

	client = (IrrecoWebdbClient *) self->private;
	IRRECO_RETRY_LOOP_START(self->loop)
		if (irreco_webdb_cache_test(self) == FALSE) break;
		success = irreco_webdb_client_get_backgrounds(client,
							      theme_id,
							      bg_list);
		if (success) break;
		irreco_webdb_client_get_error_msg(client, self->error_msg);
	IRRECO_RETRY_LOOP_END(self->loop)

	IRRECO_RETURN_BOOL(success)
}

gboolean irreco_webdb_cache_get_bg_by_id(IrrecoWebdbCache *self,
					 gint bg_id,
					 const char *theme_bg_dir)
{
	IrrecoWebdbClient *client = (IrrecoWebdbClient *) self->private;
	gboolean success = TRUE;
	IRRECO_ENTER

	IRRECO_RETRY_LOOP_START(self->loop)
		if (irreco_webdb_cache_test(self) == FALSE) break;
		success = irreco_webdb_client_get_bg_by_id(client,
							   bg_id,
							   theme_bg_dir);
		if (success) break;
		irreco_webdb_client_get_error_msg(client, self->error_msg);
	IRRECO_RETRY_LOOP_END(self->loop)
	IRRECO_RETURN_BOOL(success)
}

/**
 * Add button to theme
 */
gint irreco_webdb_cache_add_button_to_theme(IrrecoWebdbCache *self,
					     const gchar *name,
					     gboolean allow_text,
					     const gchar *text_format_up,
					     const gchar *text_format_down,
					     gint text_padding,
					     gfloat text_h_align,
					     gfloat text_v_align,
					     const gchar *image_up_hash,
					     const gchar *image_up_name,
					     const guchar *image_up,
					     gint image_up_len,
					     const gchar *image_down_hash,
					     const gchar *image_down_name,
					     const guchar *image_down,
					     gint image_down_len,
					     const gchar *folder,
					     gint theme_id,
					     const gchar *user,
					     const gchar *password)
{
	IrrecoWebdbClient *client;
	IRRECO_ENTER
	client = (IrrecoWebdbClient *) self->private;
	IRRECO_RETURN_INT(irreco_webdb_client_add_button_to_theme(
						client,
						name,
						allow_text,
						text_format_up,
						text_format_down,
						text_padding,
						text_h_align,
						text_v_align,
						image_up_hash,
						image_up_name,
						image_up,
						image_up_len,
						image_down_hash,
						image_down_name,
						image_down,
						image_down_len,
						folder,
						theme_id,
						user,
						password));
}

gboolean irreco_webdb_cache_get_buttons(IrrecoWebdbCache *self,
					 gint theme_id,
					 IrrecoStringTable **button_list)
{
	IrrecoWebdbClient *client = (IrrecoWebdbClient *) self->private;
	gboolean success = TRUE;
	IRRECO_ENTER

	IRRECO_RETRY_LOOP_START(self->loop)
		if (irreco_webdb_cache_test(self) == FALSE) break;
		success = irreco_webdb_client_get_buttons(client,
							  theme_id,
							  button_list);
		if (success) break;
		irreco_webdb_client_get_error_msg(client, self->error_msg);
	IRRECO_RETRY_LOOP_END(self->loop)
	IRRECO_RETURN_BOOL(success)
}

gboolean irreco_webdb_cache_get_button_by_id(IrrecoWebdbCache *self,
					     gint button_id,
					     const char *theme_button_dir)
{
	IrrecoWebdbClient *client = (IrrecoWebdbClient *) self->private;
	gboolean success = TRUE;
	IRRECO_ENTER

	IRRECO_RETRY_LOOP_START(self->loop)
		if (irreco_webdb_cache_test(self) == FALSE) break;
		success = irreco_webdb_client_get_button_by_id(client,
							button_id,
							theme_button_dir);
		if (success) break;
		irreco_webdb_client_get_error_msg(client, self->error_msg);
	IRRECO_RETRY_LOOP_END(self->loop)

	IRRECO_RETURN_BOOL(success)

}

gint irreco_webdb_cache_create_new_remote(IrrecoWebdbCache *self,
					  const gchar *comment,
					  const gchar *category,
					  const gchar *manufacturer,
					  const gchar *model,
					  const gchar *file_name,
					  const gchar *file_data,
					  const gchar *user,
					  const gchar *password)
{
	IRRECO_ENTER
	if (irreco_webdb_cache_test(self) == FALSE) IRRECO_RETURN_INT(0);
	IRRECO_RETURN_INT(irreco_webdb_client_create_new_remote(self->private,
			  comment, category, manufacturer, model, file_name,
			  file_data, user, password));
}

gboolean irreco_webdb_cache_set_remote_downloadable(IrrecoWebdbCache *self,
						    gint id,
						    gboolean downloadable,
						    const gchar *user,
						    const gchar *password)
{
	gboolean success;
	IRRECO_ENTER

	IRRECO_RETRY_LOOP_START(self->loop)
		if (irreco_webdb_cache_test(self) == FALSE) break;
		success = irreco_webdb_client_set_remote_downloadable(
			   self->private, id, downloadable, user, password);
		if (success) break;
		irreco_webdb_client_get_error_msg(self->private,
						  self->error_msg);
	IRRECO_RETRY_LOOP_END(self->loop)
	IRRECO_RETURN_BOOL(success);
}

gboolean irreco_webdb_cache_add_configuration_to_remote(IrrecoWebdbCache *self,
							gint remote_id,
							gint configuration_id,
							const gchar *user,
							const gchar *password)
{
	gboolean success;
	IRRECO_ENTER

	IRRECO_RETRY_LOOP_START(self->loop)
		if (irreco_webdb_cache_test(self) == FALSE) break;
		success = irreco_webdb_client_add_configuration_to_remote(
						self->private, remote_id,
						configuration_id, user,
						password);
		if (success) break;
		irreco_webdb_client_get_error_msg(self->private,
						  self->error_msg);
	IRRECO_RETRY_LOOP_END(self->loop)
	IRRECO_RETURN_BOOL(success);
}

gboolean irreco_webdb_cache_add_theme_to_remote(IrrecoWebdbCache *self,
						 gint remote_id,
						 gint theme_id,
						 const gchar *user,
						 const gchar *password)
{
	gboolean success;
	IRRECO_ENTER

	IRRECO_RETRY_LOOP_START(self->loop)
		if (irreco_webdb_cache_test(self) == FALSE) break;
		success = irreco_webdb_client_add_theme_to_remote(
						self->private, remote_id,
						theme_id, user, password);
		if (success) break;
		irreco_webdb_client_get_error_msg(self->private,
						  self->error_msg);
	IRRECO_RETRY_LOOP_END(self->loop)
	IRRECO_RETURN_BOOL(success);
}

/**
 * Fetches remote categories from DB
 *
 * @param categories points to internally allocated storage in the cache
 * and must not be freed
 * @return TRUE if categories are fetched succesfully, FALSE otherwise.
 */
gboolean irreco_webdb_cache_get_remote_categories(IrrecoWebdbCache *self,
						IrrecoStringTable **categories)
{
	IRRECO_ENTER

	if (self->remote_categories == NULL) {
		gboolean success = FALSE;

		IRRECO_RETRY_LOOP_START(self->loop)
			if (irreco_webdb_cache_test(self) == FALSE) break;
			success = irreco_webdb_client_get_remote_categories(
				self->private, &self->remote_categories);
			if (success) {
			    break;
			}
			irreco_webdb_client_get_error_msg(self->private,
							  self->error_msg);
		IRRECO_RETRY_LOOP_END(self->loop)

		if (success == FALSE) IRRECO_RETURN_BOOL(FALSE);

		irreco_string_table_sort_abc(self->remote_categories);
	}

	*categories = self->remote_categories;
	IRRECO_RETURN_BOOL(TRUE)
}

/**
 * Fetches remote manufacturers from DB.
 *
 * @param category contains category in which type manufacturers are returned.
 * @param manufacturers points to internally allocated storage in the cache
 * and must not be freed
 * @return TRUE if manufacturers are fetched succesfully, FALSE otherwise.
 */
gboolean irreco_webdb_cache_get_remote_manufacturers(IrrecoWebdbCache *self,
					const gchar *category,
					IrrecoStringTable **manufacturers)
{
	IrrecoStringTable *categories;
	IrrecoStringTable *manufacturer_list;
	IRRECO_ENTER

	if (!irreco_webdb_cache_get_remote_categories(self, &categories)) {
		IRRECO_RETURN_BOOL(FALSE)
	}

	if (!irreco_string_table_exists(categories, category)) {
		g_string_printf(self->error_msg, "%s",
				"Can't find category");
		IRRECO_RETURN_BOOL(FALSE)
	}

	irreco_string_table_get(categories, category,
				(gpointer *) &manufacturer_list);

	if (manufacturer_list == NULL) {
		gboolean success = FALSE;
		IRRECO_RETRY_LOOP_START(self->loop)
			if (irreco_webdb_cache_test(self) == FALSE) break;
			success = irreco_webdb_client_get_remote_manufacturers(
			self->private, category, &manufacturer_list);

			if (success) break;
			irreco_webdb_client_get_error_msg(self->private,
							  self->error_msg);
		IRRECO_RETRY_LOOP_END(self->loop)

		if (success == FALSE) IRRECO_RETURN_BOOL(FALSE);

		irreco_string_table_sort_abc (manufacturer_list);
		irreco_string_table_change_data(categories, category,
						manufacturer_list);
	}

	irreco_string_table_get(categories, category,
				(gpointer *) manufacturers);

	IRRECO_RETURN_BOOL(TRUE)
}

/**
 * Fetches remote models belonging given category and manufacturer.
 *
 * @param models points to internally allocated storage in the cache
 * and must not be freed
 * @return TRUE if models are fetched succesfully, FALSE otherwise.
 */
gboolean irreco_webdb_cache_get_remote_models(IrrecoWebdbCache *self,
					      const gchar *category,
					      const gchar *manufacturer,
					      IrrecoStringTable **models)
{
	IrrecoStringTable *manufacturer_list;
	IrrecoStringTable *model_list;
	IRRECO_ENTER

	if (!irreco_webdb_cache_get_remote_manufacturers(self, category,
							 &manufacturer_list)) {
		IRRECO_RETURN_BOOL(FALSE)
	}

	if (!irreco_string_table_exists(manufacturer_list, manufacturer)) {
		g_string_printf(self->error_msg, "%s",
				"Can't find manufacturer");
		IRRECO_RETURN_BOOL(FALSE)
	}

	irreco_string_table_get(manufacturer_list, manufacturer,
				(gpointer *) &model_list);

	if(model_list == NULL) {
		gboolean success = FALSE;

		IRRECO_RETRY_LOOP_START(self->loop)
			if (irreco_webdb_cache_test(self) == FALSE) break;
			success = irreco_webdb_client_get_remote_models(
					self->private, category, manufacturer,
					&model_list);

			if (success) break;
			irreco_webdb_client_get_error_msg(self->private,
							  self->error_msg);
		IRRECO_RETRY_LOOP_END(self->loop)

		if (success == FALSE) IRRECO_RETURN_BOOL(FALSE);

		irreco_string_table_sort_abc (model_list);
		irreco_string_table_change_data(manufacturer_list, manufacturer,
						model_list);
	}

	irreco_string_table_get(manufacturer_list, manufacturer,
				(gpointer *) models);
	IRRECO_RETURN_BOOL(TRUE)
}

/**
 * Fetches creators belonging given category, manufacturer and model.
 *
 * @param creators points to internally allocated storage in the cache
 * and must not be freed
 * @return TRUE if configs are fetched succesfully, FALSE otherwise.
 */
gboolean irreco_webdb_cache_get_remote_creators(IrrecoWebdbCache *self,
						const gchar *category,
						const gchar *manufacturer,
						const gchar *model,
						IrrecoStringTable **creators)
{
	IrrecoStringTable * model_list;
	IrrecoStringTable * creator_list;
	IRRECO_ENTER

	if (!irreco_webdb_cache_get_remote_models(self, category, manufacturer,
						 &model_list)) {
		IRRECO_RETURN_BOOL(FALSE)
	}

	if (!irreco_string_table_exists(model_list, model)) {
		g_string_printf(self->error_msg, "%s",
				"Can't find model");
		IRRECO_RETURN_BOOL(FALSE)
	}

	irreco_string_table_get(model_list, model, (gpointer *) &creator_list);

	if(creator_list == NULL) {
		gboolean success = FALSE;

		IRRECO_RETRY_LOOP_START(self->loop)
			if (irreco_webdb_cache_test(self) == FALSE) break;
			success = irreco_webdb_client_get_remote_creators(
					self->private, category, manufacturer,
					model, &creator_list);

			if (success) break;
			irreco_webdb_client_get_error_msg(self->private,
							  self->error_msg);
		IRRECO_RETRY_LOOP_END(self->loop)

		if (success == FALSE) IRRECO_RETURN_BOOL(FALSE);

		irreco_string_table_change_data(model_list, model,
						creator_list);
	}

	irreco_string_table_get(model_list, model, (gpointer *) creators);
	IRRECO_RETURN_BOOL(TRUE)
}

/**
 * Fetches creators belonging given category, manufacturer and model.
 *
 * @param creators points to internally allocated storage in the cache
 * and must not be freed
 * @return TRUE if configs are fetched succesfully, FALSE otherwise.
 */
gboolean irreco_webdb_cache_get_remotes(IrrecoWebdbCache *self,
					const gchar *category,
					const gchar *manufacturer,
					const gchar *model,
					const gchar *creator,
					GList **remote_list)
{
	IrrecoStringTable * creator_list;
	IRRECO_ENTER

	if (!irreco_webdb_cache_get_remote_creators(self, category,
						    manufacturer, model,
						    &creator_list)) {
		IRRECO_RETURN_BOOL(FALSE)
	}

	if (!irreco_string_table_exists(creator_list, creator)) {
		g_string_printf(self->error_msg, "%s",
				"Can't find creator");
		IRRECO_RETURN_BOOL(FALSE)
	}

	irreco_string_table_get(creator_list, creator,
				(gpointer *) remote_list);

	if(*remote_list == NULL) {
		gboolean success = FALSE;

		IRRECO_RETRY_LOOP_START(self->loop)
			if (irreco_webdb_cache_test(self) == FALSE) break;
			success = irreco_webdb_client_get_remotes(self->private,
						category, manufacturer, model,
						creator, remote_list);

			if (success) break;
			irreco_webdb_client_get_error_msg(self->private,
							  self->error_msg);
		IRRECO_RETRY_LOOP_END(self->loop)

		if (success == FALSE) IRRECO_RETURN_BOOL(FALSE);

		irreco_string_table_change_data(creator_list, creator,
						*remote_list);
	}

	irreco_string_table_get(creator_list, creator,
				(gpointer *) remote_list);
	IRRECO_RETURN_BOOL(TRUE)
}

/**
 * Fetches remote by given ID.
 *
 * @param remote points to internally allocated storage in the cache
 * and must not be freed
 * @return TRUE if remote is fetched succesfully, FALSE otherwise.
 */
gboolean irreco_webdb_cache_get_remote_by_id(IrrecoWebdbCache *self,
					     gint id,
					     IrrecoWebdbRemote **remote)
{
	IRRECO_ENTER

	if (g_hash_table_lookup(self->remote_id_hash,
				(gconstpointer) &id) == NULL) {
		gboolean success = FALSE;

		IRRECO_RETRY_LOOP_START(self->loop)
			if (irreco_webdb_cache_test(self) == FALSE) break;
			success = irreco_webdb_client_get_remote_by_id(
						self->private, id, remote);

			if (success) break;
			irreco_webdb_client_get_error_msg(self->private,
							  self->error_msg);
		IRRECO_RETRY_LOOP_END(self->loop)

		if (success == FALSE) IRRECO_RETURN_BOOL(FALSE);

		g_hash_table_insert(self->remote_id_hash,
				    (gpointer) &((*remote)->id),
				    (gpointer) *remote);
	}

	*remote = g_hash_table_lookup(self->remote_id_hash,
				      (gconstpointer) &id);

	IRRECO_RETURN_BOOL(TRUE)
}

/**
 * Fetches configurations belonging given remote_id.
 *
 * @param configs points to internally allocated storage in the cache
 * and must not be freed
 * @return TRUE if remote is fetched succesfully, FALSE otherwise.
 */
gboolean irreco_webdb_cache_get_configurations_of_remote(IrrecoWebdbCache *self,
							 gint remote_id,
							 GList **configs)
{
	IrrecoWebdbRemote *remote;
	GList *configuration_list = NULL;
	gboolean success = FALSE;
	IRRECO_ENTER

	if (!irreco_webdb_cache_get_remote_by_id(self, remote_id, &remote)) {
		IRRECO_RETURN_BOOL(FALSE)
	}


	if (remote->configurations != NULL) {
		goto end;
	}

	IRRECO_RETRY_LOOP_START(self->loop)
		if (irreco_webdb_cache_test(self) == FALSE) break;
		success = irreco_webdb_client_get_configurations_of_remote(
						self->private, remote_id,
						&configuration_list);

		if (success) break;
		irreco_webdb_client_get_error_msg(self->private,
						  self->error_msg);
	IRRECO_RETRY_LOOP_END(self->loop)

	if (success == FALSE) IRRECO_RETURN_BOOL(FALSE);

	configuration_list = g_list_first(configuration_list);
	while(configuration_list) {
		irreco_webdb_remote_add_configuration_id(remote,
				GPOINTER_TO_INT(configuration_list->data));
		configuration_list = configuration_list->next;
	}
	g_list_free(configuration_list);

	end:
	*configs = remote->configurations;

	IRRECO_RETURN_BOOL(TRUE)
}

/**
 * Fetches themes belonging given remote_id.
 *
 * @param themes points to internally allocated storage in the cache
 * and must not be freed
 * @return TRUE if remote is fetched succesfully, FALSE otherwise.
 */
gboolean irreco_webdb_cache_get_themes_of_remote(IrrecoWebdbCache *self,
						 gint remote_id,
						 GList **themes)
{
	IrrecoWebdbRemote *remote;
	GList *theme_list = NULL;
	gboolean success = FALSE;
	IRRECO_ENTER

	if (!irreco_webdb_cache_get_remote_by_id(self, remote_id, &remote)) {
		IRRECO_RETURN_BOOL(FALSE)
	}

	if (remote->themes != NULL) {
		goto end;
	}

	IRRECO_RETRY_LOOP_START(self->loop)
		if (irreco_webdb_cache_test(self) == FALSE) break;
		success = irreco_webdb_client_get_themes_of_remote(
						self->private, remote_id,
						&theme_list);

		if (success) break;
		irreco_webdb_client_get_error_msg(self->private,
						  self->error_msg);
	IRRECO_RETRY_LOOP_END(self->loop)

	if (success == FALSE) IRRECO_RETURN_BOOL(FALSE);

	theme_list = g_list_first(theme_list);
	while(theme_list) {
		irreco_webdb_remote_add_theme_id(remote, GPOINTER_TO_INT(
						 theme_list->data));
		theme_list = theme_list->next;
	}
	g_list_free(theme_list);

	end:
	*themes = remote->themes;

	IRRECO_RETURN_BOOL(TRUE)
}

/**
 * Fetches remote data by given ID.
 *
 * @return TRUE if data is fetched succesfully, FALSE otherwise.
 */
gboolean irreco_webdb_cache_get_remote_data(IrrecoWebdbCache *self,
					    gint remote_id,
					    gchar **file_data)
{
	gboolean success;
	IRRECO_ENTER

	IRRECO_RETRY_LOOP_START(self->loop)
		if (irreco_webdb_cache_test(self) == FALSE) break;
		success = irreco_webdb_client_get_remote_data(self->private,
							      remote_id,
							      file_data);
		if (success) break;
		irreco_webdb_client_get_error_msg(self->private,
						  self->error_msg);
	IRRECO_RETRY_LOOP_END(self->loop)
	IRRECO_RETURN_BOOL(success);
}

gboolean irreco_webdb_cache_get_lirc_dirs(IrrecoWebdbCache *self,
					   IrrecoStringTable **dir_list)
{
	gboolean success = FALSE;
	IrrecoWebdbClient *client = (IrrecoWebdbClient *) self->private;

	IRRECO_ENTER

	IRRECO_RETRY_LOOP_START(self->loop)
		if (irreco_webdb_cache_test(self) == FALSE) break;
		success = irreco_webdb_client_get_lirc_dirs(client,
							    dir_list);
		if (success) break;
		irreco_webdb_client_get_error_msg(client,
						  self->error_msg);
	IRRECO_RETRY_LOOP_END(self->loop)

	if(success == FALSE) IRRECO_RETURN_BOOL(FALSE);

	IRRECO_RETURN_BOOL(success);
}

gboolean irreco_webdb_cache_get_lirc_manufacturers(IrrecoWebdbCache *self,
						const gchar *range,
						IrrecoStringTable **manufacturer_list)
{
	gboolean success = FALSE;
	IrrecoWebdbClient *client = (IrrecoWebdbClient *) self->private;

	IRRECO_ENTER

	IRRECO_RETRY_LOOP_START(self->loop)
		if (irreco_webdb_cache_test(self) == FALSE) break;
		success = irreco_webdb_client_get_lirc_manufacturers(
				client,
				range,
				manufacturer_list);
		if (success) break;
		irreco_webdb_client_get_error_msg(client,
						  self->error_msg);
	IRRECO_RETRY_LOOP_END(self->loop)

	if(success == FALSE) IRRECO_RETURN_BOOL(FALSE);

	IRRECO_RETURN_BOOL(success);
}

gboolean irreco_webdb_cache_get_lirc_models(IrrecoWebdbCache *self,
					     const gchar *manufacturer,
					     IrrecoStringTable **model_list)
{
	gboolean success = FALSE;
	IrrecoWebdbClient *client = (IrrecoWebdbClient *) self->private;

	IRRECO_ENTER

	IRRECO_RETRY_LOOP_START(self->loop)
		if (irreco_webdb_cache_test(self) == FALSE) break;
		success = irreco_webdb_client_get_lirc_models(
				client,
				manufacturer,
				model_list);
		if (success) break;
		irreco_webdb_client_get_error_msg(client,
						  self->error_msg);
	IRRECO_RETRY_LOOP_END(self->loop)

	if(success == FALSE) IRRECO_RETURN_BOOL(FALSE);

	IRRECO_RETURN_BOOL(success);
}

gboolean irreco_webdb_cache_get_lirc_file(IrrecoWebdbCache *self,
					   const gchar *model,
					   IrrecoStringTable **file)
{
	gboolean success = FALSE;
	IrrecoWebdbClient *client = (IrrecoWebdbClient *) self->private;

	IRRECO_ENTER

	IRRECO_RETRY_LOOP_START(self->loop)
		if (irreco_webdb_cache_test(self) == FALSE) break;
		success = irreco_webdb_client_get_lirc_file(
				client,
				model,
				file);
		if (success) break;
		irreco_webdb_client_get_error_msg(client,
						  self->error_msg);
	IRRECO_RETRY_LOOP_END(self->loop)

	if(success == FALSE) IRRECO_RETURN_BOOL(FALSE);

	IRRECO_RETURN_BOOL(success);
}

/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Events and Callbacks                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/** @} */

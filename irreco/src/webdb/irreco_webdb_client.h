/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoWebdbClient
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoWebdbClient.
 */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_WEBDB_CLIENT_TYPEDEF__
#define __IRRECO_WEBDB_CLIENT_TYPEDEF__
typedef struct _IrrecoWebdbClient IrrecoWebdbClient;
#endif /* __IRRECO_WEBDB_CLIENT_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_WEBDB_CLIENT__
#define __IRRECO_WEBDB_CLIENT__
#include "irreco_webdb.h"

#include <stdlib.h>
#include <stdio.h>
#include "irreco_webdb_conf.h"
#include "irreco_webdb_theme.h"
#include "irreco_webdb_remote.h"
#include <libsoup/soup.h>
/*#include <libsoup/soup-xmlrpc.h>*/

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

struct _IrrecoWebdbClient
{
	GString			*error_msg;
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
IrrecoWebdbClient *irreco_webdb_client_new();
void irreco_webdb_client_free(IrrecoWebdbClient *self);
void irreco_webdb_client_get_error_msg(IrrecoWebdbClient *self, GString *msg);
gboolean irreco_webdb_client_add_user(IrrecoWebdbClient *self,
				      const gchar *name,
				      const gchar *email,
				      const gchar *passwd);
gboolean irreco_webdb_client_add_uber(IrrecoWebdbClient *self,
				      const gchar *name,
				      const gchar *email,
				      const gchar *passwd);
gboolean irreco_webdb_client_upload_configuration(IrrecoWebdbClient *self,
						  const gchar *backend,
						  const gchar *category,
						  const gchar *file_hash,
						  const gchar *file_name,
						  const gchar *manufacturer,
						  const gchar *model,
						  const gchar *password,
						  const gchar *user,
						  const gchar *data);

gboolean irreco_webdb_client_sum(IrrecoWebdbClient *self,
				 gint num_a,
				 gint num_b,
				 glong *sum);
gboolean irreco_webdb_client_get_categories(IrrecoWebdbClient *self,
					    IrrecoStringTable **category_list);

gboolean irreco_webdb_client_get_all_categories(IrrecoWebdbClient *self,
					    IrrecoStringTable **category_list);

gboolean irreco_webdb_client_get_manufacturers(IrrecoWebdbClient *self,
					const gchar *category,
					IrrecoStringTable **manufacturer_list);

gboolean irreco_webdb_client_get_all_manufacturers(IrrecoWebdbClient *self,
			    IrrecoStringTable **manufacturer_list);

gboolean irreco_webdb_client_get_models(IrrecoWebdbClient *self,
					const gchar *category,
					const gchar *manufacturer,
     					IrrecoStringTable **model_list);

gboolean irreco_webdb_client_get_configs(IrrecoWebdbClient *self,
					 const gchar *manufacturer,
					 const gchar *model,
					 IrrecoStringTable **config_list);

gboolean irreco_webdb_client_get_configuration(IrrecoWebdbClient *self,
					       gint id,
					       IrrecoWebdbConf **configuration);

gint irreco_webdb_client_get_config_id_by_file_hash_and_file_name(
							IrrecoWebdbClient *self,
							const gchar *file_hash,
							const gchar *file_name);

gboolean irreco_webdb_client_get_file(IrrecoWebdbClient *self,
				      const gchar *file_hash,
				      const gchar *file_name,
				      GString **file_data);

gboolean irreco_webdb_client_get_user_exists(IrrecoWebdbClient *self,
				const gchar *name, gboolean *user_exists);

gint irreco_webdb_client_get_max_image_size(IrrecoWebdbClient *self);

gint irreco_webdb_client_create_theme(IrrecoWebdbClient *self,
				      const gchar *name,
				      const gchar *comment,
				      const gchar *preview_button,
				      const gchar *folder,
				      const gchar *user,
				      const gchar *password);

gboolean irreco_webdb_client_set_theme_downloadable(IrrecoWebdbClient *self,
						    gint id,
						    gboolean downloadable,
						    const gchar *user,
						    const gchar *password);

gboolean irreco_webdb_client_get_themes(IrrecoWebdbClient *self,
					IrrecoStringTable **theme_list);

gboolean irreco_webdb_client_get_theme_by_id(IrrecoWebdbClient *self,
					     gint theme_id,
					     IrrecoWebdbTheme **theme);

gboolean irreco_webdb_client_get_theme_versions_by_name(IrrecoWebdbClient *self,
						const char *name,
						IrrecoStringTable **theme_list);

gchar *irreco_webdb_client_get_theme_date_by_id(IrrecoWebdbClient *self,
					        gint theme_id);

gint irreco_webdb_client_get_theme_id_by_name_and_date(IrrecoWebdbClient *self,
						       const gchar *name,
						       const gchar *date);

gint irreco_webdb_client_add_bg_to_theme(IrrecoWebdbClient *self,
					 const gchar *name,
					 const gchar *image_hash,
					 const gchar *image_name,
					 const guchar *image,
					 gint image_len,
					 const gchar *folder,
					 gint theme_id,
					 const gchar *user,
					 const gchar *password);

gboolean irreco_webdb_client_get_backgrounds(IrrecoWebdbClient *self,
					     gint theme_id,
					     IrrecoStringTable **bg_list);

gboolean irreco_webdb_client_get_bg_by_id(IrrecoWebdbClient *self,
					  gint bg_id,
					  const char *theme_bg_dir);

gint irreco_webdb_client_add_button_to_theme(IrrecoWebdbClient *self,
					     const gchar *name,
					     gboolean allow_text,
					     const gchar *text_format_up,
					     const gchar *text_format_down,
					     gint text_padding,
					     gfloat text_h_align,
					     gfloat text_v_align,
					     const gchar *image_up_hash,
					     const gchar *image_up_name,
					     const guchar *image_up,
					     gint image_up_len,
					     const gchar *image_down_hash,
					     const gchar *image_down_name,
					     const guchar *image_down,
					     gint image_down_len,
					     const gchar *folder,
					     gint theme_id,
					     const gchar *user,
					     const gchar *password);

gboolean irreco_webdb_client_get_buttons(IrrecoWebdbClient *self,
					 gint theme_id,
					 IrrecoStringTable **button_list);

gboolean irreco_webdb_client_get_button_by_id(IrrecoWebdbClient *self,
					      gint button_id,
					      const char *theme_button_dir);

gboolean irreco_webdb_client_get_preview_button(IrrecoWebdbClient *self,
						gint theme_id,
						GdkPixbuf **preview_button);

gboolean irreco_webdb_client_login(IrrecoWebdbClient *self,
						  const gchar *user,
						  const gchar *password);

gint irreco_webdb_client_create_new_remote(IrrecoWebdbClient *self,
					   const gchar *comment,
					   const gchar *category,
					   const gchar *manufacturer,
					   const gchar *model,
					   const gchar *file_name,
					   const gchar *file_data,
					   const gchar *user,
					   const gchar *password);

gboolean irreco_webdb_client_set_remote_downloadable(IrrecoWebdbClient *self,
						     gint id,
						     gboolean downloadable,
						     const gchar *user,
						     const gchar *password);

gboolean irreco_webdb_client_add_configuration_to_remote(
							IrrecoWebdbClient *self,
							gint remote_id,
							gint configuration_id,
							const gchar *user,
							const gchar *password);

gboolean irreco_webdb_client_add_theme_to_remote(IrrecoWebdbClient *self,
						 gint remote_id,
						 gint theme_id,
						 const gchar *user,
						 const gchar *password);

gboolean irreco_webdb_client_get_remote_categories(IrrecoWebdbClient *self,
					    IrrecoStringTable **category_list);

gboolean irreco_webdb_client_get_remote_manufacturers(IrrecoWebdbClient *self,
					const gchar *category,
					IrrecoStringTable **manufacturer_list);

gboolean irreco_webdb_client_get_remote_models(IrrecoWebdbClient *self,
					       const gchar *category,
					       const gchar *manufacturer,
					       IrrecoStringTable **model_list);

gboolean irreco_webdb_client_get_remote_creators(IrrecoWebdbClient *self,
						 const gchar *category,
						 const gchar *manufacturer,
						 const gchar *model,
						 IrrecoStringTable **creators);

gboolean irreco_webdb_client_get_remotes(IrrecoWebdbClient *self,
					 const gchar *category,
					 const gchar *manufacturer,
					 const gchar *model,
					 const gchar *creator,
					 GList **remote_list);

gboolean irreco_webdb_client_get_remote_by_id(IrrecoWebdbClient *self,
					      gint id,
					      IrrecoWebdbRemote **remote);

gboolean irreco_webdb_client_get_themes_of_remote(IrrecoWebdbClient *self,
						  gint remote_id,
						  GList **themes);

gboolean irreco_webdb_client_get_configurations_of_remote(
						  IrrecoWebdbClient *self,
						  gint remote_id,
						  GList **configs);

gboolean irreco_webdb_client_get_remote_data(IrrecoWebdbClient *self,
					     gint remote_id,
					     gchar **file_data);
gboolean irreco_webdb_client_get_lirc_dirs(IrrecoWebdbClient *self,
					   IrrecoStringTable **dir_list);
gboolean irreco_webdb_client_get_lirc_manufacturers(IrrecoWebdbClient *self,
						const gchar *range,
						IrrecoStringTable **manufacturer_list);
gboolean irreco_webdb_client_get_lirc_models(IrrecoWebdbClient *self,
					     const gchar *manufacturer,
					     IrrecoStringTable **model_list);
gboolean irreco_webdb_client_get_lirc_file(IrrecoWebdbClient *self,
					   const gchar *model,
					   IrrecoStringTable **file);


#endif /* __IRRECO_WEBDB_CLIENT__ */

/** @} */

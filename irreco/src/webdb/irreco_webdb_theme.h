/*
 * irreco - Ir Remote Control
 * Copyright (C) 2008 Joni Kokko (t5kojo01@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoWebdbTheme
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoWebdbTheme.
 */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __WEBDB_THEME_H_TYPEDEF__
#define __WEBDB_THEME_H_TYPEDEF__
typedef struct _IrrecoWebdbTheme IrrecoWebdbTheme;
#endif /* __WEBDB_THEME_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __WEBDB_THEME_H__
#define __WEBDB_THEME_H__
#include "irreco_webdb.h"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

struct _IrrecoWebdbTheme
{
	gint		id;
	GString		*name;
	GString		*creator;
	GString		*comment;
	GString		*preview_button_name;
	GdkPixbuf	*preview_button;
	GString		*folder;
	GString		*uploaded;
	GString		*modified;
	GString		*downloaded;
	gint		download_count;
	IrrecoStringTable *versions;
};

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

IrrecoWebdbTheme *irreco_webdb_theme_new();
void irreco_webdb_theme_free(IrrecoWebdbTheme *self);

void irreco_webdb_theme_set(IrrecoWebdbTheme *self,
			    gint id,
			    const char *name,
			    const char *creator,
			    const char *comment,
			    const char *preview_button_name,
			    GdkPixbuf	*preview_button,
			    const char *folder,
			    const char *uploaded,
			    const char *modified,
			    const char *downloaded,
			    gint download_count);

void irreco_webdb_theme_print(IrrecoWebdbTheme *self);
void irreco_webdb_theme_set_preview_button(IrrecoWebdbTheme *self,
					   GdkPixbuf *preview_button);
#endif /* __WEBDB_THEME_H__ */

/** @} */

/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *	      &  2008  Joni Kokko     (t5kojo01@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_backend_device.h"
#include "irreco_select_instance_dlg.h"

/**
 * @addtogroup IrrecoSelectInstanceDlg
 * @ingroup Irreco
 *
 * Dialog for creating backend devices.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoSelectInstanceDlg.
 */

G_DEFINE_TYPE (IrrecoSelectInstanceDlg, \
	       irreco_select_instance_dlg, \
	       IRRECO_TYPE_DLG)


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

static void
irreco_select_instance_dlg_dispose (GObject *object)
{
  if (G_OBJECT_CLASS (irreco_select_instance_dlg_parent_class)->dispose)
    G_OBJECT_CLASS (irreco_select_instance_dlg_parent_class)->dispose (object);
}

static void
irreco_select_instance_dlg_finalize (GObject *object)
{
  if (G_OBJECT_CLASS (irreco_select_instance_dlg_parent_class)->finalize)
    G_OBJECT_CLASS (irreco_select_instance_dlg_parent_class)->finalize (object);
}

static void
irreco_select_instance_dlg_class_init (IrrecoSelectInstanceDlgClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);


  object_class->dispose = irreco_select_instance_dlg_dispose;
  object_class->finalize = irreco_select_instance_dlg_finalize;
}

static void
irreco_select_instance_dlg_init (IrrecoSelectInstanceDlg *self)
{
	GtkWidget *padding;
	IRRECO_ENTER

	/* Construct dialog. */
	gtk_window_set_title(GTK_WINDOW(self), _("Select device controller"));
	gtk_window_set_modal(GTK_WINDOW(self), TRUE);
	gtk_window_set_destroy_with_parent(GTK_WINDOW(self), TRUE);
	gtk_dialog_set_has_separator(GTK_DIALOG(self), FALSE);
	gtk_dialog_add_buttons(GTK_DIALOG(self),
	  		       GTK_STOCK_OK, GTK_RESPONSE_OK,
			       NULL);

	/* Add widgets. */
	self->vbox = gtk_vbox_new(FALSE, 0);
	self->listbox = IRRECO_LISTBOX(
			irreco_listbox_text_new_with_autosize(300, 600,
							      100, 250));

	gtk_container_add(GTK_CONTAINER(self->vbox), GTK_WIDGET(self->listbox));
	padding = irreco_gtk_pad(self->vbox, 8, 8, 8, 8);
	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(self)->vbox), padding);

	IRRECO_RETURN
}

GtkWidget * irreco_select_instance_dlg_new (GtkWindow *parent)
{
	IrrecoSelectInstanceDlg *self;
	IRRECO_ENTER

	self = g_object_new(IRRECO_TYPE_SELECT_INSTANCE_DLG, NULL);
	irreco_dlg_set_parent(IRRECO_DLG(self), parent);

	IRRECO_RETURN_PTR(self);
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Functions                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */

gboolean irreco_show_select_instance_dlg(IrrecoData *irreco_data,
					 GtkWindow *parent,
      					 const char *lib_name,
					 IrrecoBackendInstance ** sel_instance)
{
	IrrecoSelectInstanceDlg *select_instance_dlg = NULL;
	const gchar             *name_desc           = NULL;
	gboolean                 rvalue              = -1;
	IRRECO_ENTER

	select_instance_dlg = IRRECO_SELECT_INSTANCE_DLG(
				irreco_select_instance_dlg_new (parent));

	IRRECO_BACKEND_MANAGER_FOREACH_INSTANCE(
	    irreco_data->irreco_backend_manager, instance)

		name_desc = irreco_backend_instance_get_name_and_description(
			instance);
	    	irreco_backend_instance_get_name_and_description(instance);
		IRRECO_DEBUG("Adding backend instance \"%s\" to list.\n",
			     name_desc);

		if (lib_name == NULL) {
			irreco_listbox_text_append(IRRECO_LISTBOX_TEXT(
						select_instance_dlg->listbox),
						name_desc,
						instance);
		}
		else if (g_utf8_collate(instance->lib->name, lib_name) == 0) {
			irreco_listbox_text_append(IRRECO_LISTBOX_TEXT(
						select_instance_dlg->listbox),
						name_desc,
						instance);
		}
	IRRECO_BACKEND_MANAGER_FOREACH_END

	gtk_widget_show_all(GTK_WIDGET(select_instance_dlg));

	while (rvalue == -1) {
		switch (gtk_dialog_run(GTK_DIALOG(select_instance_dlg))) {
		case GTK_RESPONSE_DELETE_EVENT:
			rvalue = FALSE;
			break;

		case GTK_RESPONSE_OK: {
			gint sel_index;
			gpointer sel_user_data;
			IrrecoBackendInstance *instance;

			irreco_listbox_get_selection(
				IRRECO_LISTBOX(select_instance_dlg->listbox),
				&sel_index, NULL,
				&sel_user_data);
			instance = (IrrecoBackendInstance *) sel_user_data;

			if (sel_index < 0) {
				irreco_info_dlg(GTK_WINDOW(select_instance_dlg),
					        _("Select device controller "
						  "before clicking OK."));
				break;
			}

			if (!instance->lib->api->flags &
			    IRRECO_BACKEND_EDITABLE_DEVICES) {
				irreco_info_dlg(GTK_WINDOW(select_instance_dlg),
					        _("Selected instace does "
						  "not support editing of "
						  "devices"));
				break;
			}

			*sel_instance = instance;
			rvalue = TRUE;
			} break;
		}
	}

	gtk_widget_destroy(GTK_WIDGET(select_instance_dlg));
	IRRECO_RETURN_BOOL(rvalue);
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Events and Callbacks                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/** @} */

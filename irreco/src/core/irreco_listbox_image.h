/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoListboxImage
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoListboxImage.
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_LISTBOX_IMAGE_H_TYPEDEF__
#define __IRRECO_LISTBOX_IMAGE_H_TYPEDEF__

typedef struct _IrrecoListboxImage		IrrecoListboxImage;
typedef struct _IrrecoListboxImageClass 	IrrecoListboxImageClass;

#define IRRECO_TYPE_LISTBOX_IMAGE irreco_listbox_image_get_type()

#define IRRECO_LISTBOX_IMAGE(obj) \
	(G_TYPE_CHECK_INSTANCE_CAST ((obj), \
	IRRECO_TYPE_LISTBOX_IMAGE, IrrecoListboxImage))

#define IRRECO_LISTBOX_IMAGE_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_CAST ((klass), \
	IRRECO_TYPE_LISTBOX_IMAGE, IrrecoListboxImageClass))

#define IRRECO_IS_LISTBOX_IMAGE(obj) \
	(G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
	IRRECO_TYPE_LISTBOX_IMAGE))

#define IRRECO_IS_LISTBOX_IMAGE_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_TYPE ((klass), \
	IRRECO_TYPE_LISTBOX_IMAGE))

#define IRRECO_LISTBOX_IMAGE_GET_CLASS(obj) \
	(G_TYPE_INSTANCE_GET_CLASS ((obj), \
	IRRECO_TYPE_LISTBOX_IMAGE, IrrecoListboxImageClass))

#endif /* __IRRECO_LISTBOX_IMAGE_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_LISTBOX_IMAGE_H__
#define __IRRECO_LISTBOX_IMAGE_H__
#include "irreco.h"
#include "irreco_listbox.h"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

struct _IrrecoListboxImage {
	IrrecoListbox parent;
};

struct _IrrecoListboxImageClass {
	IrrecoListboxClass parent_class;
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
GType		irreco_listbox_image_get_type	(void);
GtkWidget*	irreco_listbox_image_new	(void);
GtkWidget*	irreco_listbox_image_new_with_autosize	(gint min_width,
							 gint max_width,
							 gint min_height,
							 gint max_height);
void irreco_listbox_image_append	(IrrecoListboxImage *self,
					 const gchar *label,
					 gpointer user_data,
					 const gchar *image);

void irreco_listbox_image_append_with_size(IrrecoListboxImage *self,
					   const gchar *label,
					   gpointer user_data,
					   const gchar *image,
					   gint image_width,
					   gint image_height);




#endif /* __IRRECO_LISTBOX_IMAGE_H__ */

/** @} */



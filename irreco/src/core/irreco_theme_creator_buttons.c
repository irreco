/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * irreco - Ir Remote Control
 * Copyright (C) 2008 Pekka Gehör (pegu6@msn.com)
 *
 * irreco_theme_creator_buttons.c is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * irreco_theme_creator_buttons.c is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "irreco_theme_creator_buttons.h"
#include "irreco_theme_creator_dlg.h"
#include "irreco_background_creator_dlg.h"
#include "irreco_button_creator_dlg.h"

/**
 * @addtogroup IrrecoThemeCreatorBackgrounds
 * @ingroup Irreco
 *
 * User interface for IrrecoThemeCreatorDlgBackgrounds.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoThemeCreatorBackgrounds.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

#define IRRECO_BUTTON_PREVIEW_WIDHT		(IRRECO_SCREEN_HEIGHT / 5)
#define IRRECO_BUTTON_PREVIEW_HEIGHT		(IRRECO_SCREEN_HEIGHT / 5)

enum
{
	DATA_COL,
	TEXT_COL,
	UNPIXBUF_COL,
	PIXBUF_COL,
	N_COLUMNS
};
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes.                                                                */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
static void _image_selection_changed(GtkTreeSelection * selection,
				     IrrecoThemeCreatorButtons *self);

gboolean
irreco_theme_creator_buttons_get_iter(IrrecoThemeCreatorButtons *self,
					  gint index, GtkTreeIter * iter);
gboolean
irreco_theme_creator_buttons_set_selection(IrrecoThemeCreatorButtons
						*self, gint index);
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

G_DEFINE_TYPE (IrrecoThemeCreatorButtons, irreco_theme_creator_buttons,
			   IRRECO_TYPE_INTERNAL_WIDGET)

static void
irreco_theme_creator_buttons_finalize (GObject *object)
{
	/* TODO: Add deinitalization code here */
	IRRECO_ENTER
	G_OBJECT_CLASS (
		irreco_theme_creator_buttons_parent_class)->finalize (object);
	IRRECO_RETURN
}

static void
irreco_theme_creator_buttons_class_init (IrrecoThemeCreatorButtonsClass *klass)
{
	GObjectClass *object_class;
	IRRECO_ENTER

	object_class = G_OBJECT_CLASS (klass);
	object_class->finalize = irreco_theme_creator_buttons_finalize;
	IRRECO_RETURN

}

static void
irreco_theme_creator_buttons_init (IrrecoThemeCreatorButtons *self)
{
	/* TODO: Add initialization code here */

	IRRECO_ENTER

	gtk_box_set_spacing(GTK_BOX(self), 1);

	/*BUTTONS*/
	self->scroll_buttons = gtk_scrolled_window_new(NULL, NULL);
	self->store_buttons = gtk_list_store_new(N_COLUMNS, G_TYPE_POINTER,
						 G_TYPE_STRING, GDK_TYPE_PIXBUF,
       						 GDK_TYPE_PIXBUF);
	self->view_buttons = gtk_tree_view_new();
	gtk_box_pack_start_defaults(GTK_BOX(self), self->scroll_buttons);


	/*BUTTONS COLUMNS*/

	gtk_tree_view_set_model(GTK_TREE_VIEW(self->view_buttons),
				GTK_TREE_MODEL(self->store_buttons));
	g_object_unref(self->store_buttons);

	/* Create pixbuf column. */
	self->renderer_buttons = gtk_cell_renderer_pixbuf_new();
	self->column_buttons = gtk_tree_view_column_new_with_attributes(
				"Unpressed	  ", self->renderer_buttons,
    				"pixbuf", UNPIXBUF_COL, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(self->view_buttons),
				    self->column_buttons);

	self->renderer_buttons = gtk_cell_renderer_pixbuf_new();
	self->column_buttons = gtk_tree_view_column_new_with_attributes(
						"Pressed		  ",
      						self->renderer_buttons,
	    					"pixbuf", PIXBUF_COL, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(self->view_buttons),
				    self->column_buttons);

	/* Create text column. */
	self->renderer_buttons = gtk_cell_renderer_text_new();
	self->column_buttons = gtk_tree_view_column_new_with_attributes(
							"Name	   ",
       							self->renderer_buttons,
							"text", TEXT_COL, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(self->view_buttons),
				    self->column_buttons);
  	gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(self->view_buttons),
					  TRUE);

	/* Scroll_buttons*/

	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(self->scroll_buttons),
				       GTK_POLICY_AUTOMATIC,
	   			       GTK_POLICY_AUTOMATIC);
	gtk_container_add(GTK_CONTAINER(self->scroll_buttons),
			  self->view_buttons);
	self->tree_selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(
							   self->view_buttons));
	gtk_tree_selection_set_mode(self->tree_selection, GTK_SELECTION_SINGLE);


	/* Signal handlers. */
	g_signal_connect(G_OBJECT(self->tree_selection),
			 "changed",
    			 G_CALLBACK(_image_selection_changed),
			 self);
	IRRECO_RETURN
}

GtkWidget* irreco_theme_creator_buttons_new(GtkWindow *parent,
					    IrrecoData *irreco_data,
	 				    IrrecoTheme * irreco_theme)
{
	IrrecoThemeCreatorButtons *self;
	IRRECO_ENTER

	self = g_object_new(IRRECO_TYPE_THEME_CREATOR_BUTTONS,
			    "irreco-data", irreco_data,
			    NULL);

	irreco_theme_creator_buttons_set_parent_window(self, parent);
	self->parent_window = GTK_WINDOW(parent);
	self->irreco_data = irreco_data;
	self->theme = irreco_theme;
	irreco_theme_creator_buttons_refresh(self);
	IRRECO_RETURN_PTR(GTK_WIDGET(self));
}

/** @} */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Private Functions
 * @{
 */

static void _image_selection_changed(GtkTreeSelection * selection,
				     IrrecoThemeCreatorButtons *self)
{
	GtkTreeIter       iter;
        GtkTreeModel     *model = NULL;
	GtkTreePath      *path          = NULL;
	gint             *path_indices  = NULL;
	IRRECO_ENTER

	/* Get currect selection, if set. */
	selection = gtk_tree_view_get_selection(
					     GTK_TREE_VIEW(self->view_buttons));
	if (gtk_tree_selection_get_selected(self->tree_selection,
					    &model, &iter)) {
		path = gtk_tree_model_get_path(model, &iter);
		path_indices = gtk_tree_path_get_indices(path);
		self->sel_index = path_indices[0];
		gtk_tree_path_free(path);

		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter,
                   		   DATA_COL, &self->current_button,
				   -1);
	} else {
		self->current_button = NULL;
	}

	IRRECO_RETURN

}

gboolean
irreco_theme_creator_buttons_get_iter(IrrecoThemeCreatorButtons *self,
					  gint index, GtkTreeIter * iter)
{
	gboolean rvalue;
	GtkTreeModel *model;
	GtkTreePath *path;
	IRRECO_ENTER

	model = gtk_tree_view_get_model(GTK_TREE_VIEW(
					self->view_buttons));
	path = gtk_tree_path_new_from_indices(index, -1);
	rvalue = gtk_tree_model_get_iter(model, iter, path);
	gtk_tree_path_free(path);
	IRRECO_RETURN_INT(rvalue);
}

gboolean
irreco_theme_creator_buttons_set_selection(IrrecoThemeCreatorButtons
						*self, gint index)
{
	GtkTreeIter iter;
	IRRECO_ENTER

	if (irreco_theme_creator_buttons_get_iter(self, index, &iter)) {
		gtk_tree_selection_select_iter(self->tree_selection, &iter);
		IRRECO_RETURN_BOOL(TRUE);
	}
	IRRECO_RETURN_BOOL(FALSE);
}
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Public Functions                                                           */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */

/**
 * Set parent window for popup dialogs.
 */
void
irreco_theme_creator_buttons_set_parent_window(IrrecoThemeCreatorButtons *self,
					       GtkWindow *parent)
{
	IRRECO_ENTER
	self->parent_window = parent;
	IRRECO_RETURN
}

void
irreco_theme_creator_buttons_refresh(IrrecoThemeCreatorButtons *self)
{

	GdkPixbuf			*unpixbuf = NULL;
	GdkPixbuf			*pixbuf = NULL;
	GError				*error = NULL;
	IrrecoData			*irreco_data = NULL;
	IrrecoThemeManager		*manager = NULL;
	IrrecoTheme			*theme;
	GtkTreeIter			iter;
	IRRECO_ENTER

	gtk_list_store_clear(self->store_buttons);
	irreco_data = self->irreco_data;
	manager = irreco_data->theme_manager;

	theme = self->theme;


	IRRECO_STRING_TABLE_FOREACH_DATA(theme->buttons,
					 IrrecoThemeButton *, button)

		irreco_theme_button_print(button);
		unpixbuf = gdk_pixbuf_new_from_file_at_scale(
						button->image_up->str,
						IRRECO_BUTTON_PREVIEW_WIDHT,
						IRRECO_BUTTON_PREVIEW_HEIGHT,
						GDK_INTERP_NEAREST, &error);


		if (irreco_gerror_check_print(&error)) {
			IRRECO_RETURN
		}

		pixbuf = gdk_pixbuf_new_from_file_at_scale(
					      button->image_down->str,
	   				      IRRECO_BUTTON_PREVIEW_WIDHT,
	      				      IRRECO_BUTTON_PREVIEW_HEIGHT,
		 			      GDK_INTERP_NEAREST, &error);
		if (irreco_gerror_check_print(&error)) {
			IRRECO_RETURN
		}

		gtk_list_store_append(self->store_buttons, &iter);

		gtk_list_store_set(self->store_buttons, &iter,
				   DATA_COL, button,
	 			   UNPIXBUF_COL, unpixbuf,
	  			   PIXBUF_COL, pixbuf,
	   			   TEXT_COL,
	   			   button->name->str, -1);
		gtk_tree_view_columns_autosize(GTK_TREE_VIEW(
					       self->view_buttons));
	IRRECO_STRING_TABLE_FOREACH_END


	if (unpixbuf != NULL) g_object_unref(G_OBJECT(unpixbuf));
	if (pixbuf != NULL) g_object_unref(G_OBJECT(pixbuf));

	IRRECO_RETURN
}

IrrecoThemeButton
*irreco_theme_creator_buttons_get_selected_button(IrrecoThemeCreatorButtons
						  *self)
{
	IRRECO_ENTER
	IRRECO_RETURN_PTR(self->current_button);
}

gboolean
irreco_theme_creator_buttons_remove_selected(IrrecoThemeCreatorButtons *self)
{
	gint index;
	GtkTreeIter iter;
	IRRECO_ENTER

	_image_selection_changed(self->tree_selection, self);
	if ((index = self->sel_index) == -1
	    || irreco_theme_creator_buttons_get_iter(self, index, &iter) == FALSE) {
		IRRECO_RETURN_BOOL(FALSE);
	}
	if(!irreco_theme_creator_buttons_set_selection(self, index - 1)) {
		irreco_theme_creator_buttons_set_selection(self, index + 1);
	}

	gtk_list_store_remove(self->store_buttons, &iter);
	IRRECO_RETURN_BOOL(TRUE);
}

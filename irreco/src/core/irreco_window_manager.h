/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoWindowManager
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoWindowManager.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_WINDOW_MANAGER_H_TYPEDEF__
#define __IRRECO_WINDOW_MANAGER_H_TYPEDEF__
typedef struct _IrrecoWindowManager IrrecoWindowManager;
typedef enum {
	IRRECO_WINDOW_USER = 1,
	IRRECO_WINDOW_EDIT
} IrrecoWindowId;
#endif /* __IRRECO_WINDOW_MANAGER_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_WINDOW_MANAGER_H__
#define __IRRECO_WINDOW_MANAGER_H__
#include "irreco.h"
#include "irreco_window_edit.h"
#include "irreco_window_user.h"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
struct _IrrecoWindowManager {

	IrrecoData 		*irreco_data;

	IrrecoWindowEdit	*edit_window;
	IrrecoWindowUser	*user_window;

	IrrecoWindowId		current_window;
	IrrecoButtonLayout	*current_layout;

	IrrecoWindowId		to_be_destroyed;
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
IrrecoWindowManager *irreco_window_manager_create(IrrecoData * irreco_data);
void irreco_window_manager_destroy(IrrecoWindowManager * manager);
void irreco_window_manager_show(IrrecoWindowManager * manager,
				IrrecoWindowId id);
void irreco_window_manager_set_layout(IrrecoWindowManager * manager,
				      IrrecoButtonLayout * layout);
GtkWindow *irreco_window_manager_get_gtk_window(IrrecoWindowManager * manager);


#endif /* __IRRECO_WINDOW_MANAGER_H__ */

/** @} */

/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_window_edit.h"
#include "irreco_window_user.h"
#include "irreco_button_dlg.h"
#include "irreco_config.h"
#include "irreco_input_dlg.h"
#include "irreco_backend_dlg.h"
#include "irreco_background_dlg.h"
#include "irreco_device_dlg.h"
#include "irreco_hardkey_dlg.h"
#include "irreco_webdb_dlg.h"
#include "irreco_theme_manager_dlg.h"
#include "irreco_lircdb_dlg.h"
#include </usr/include/hildon-1/hildon/hildon-button.h>

/**
 * @addtogroup IrrecoWindowEdit
 * @ingroup Irreco
 *
 * User interface for editing button layouts.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoWindowEdit.
 */


typedef struct _ButtonMenuForeachData ButtonMenuForeachData;
struct _ButtonMenuForeachData {
	GtkWidget *button_menu;
	gint i;
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes.                                                                */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
void irreco_window_edit_controller_help(IrrecoWindowEdit * edit_window);
gboolean irreco_window_edit_expose(GtkWidget * widget, GdkEventExpose * event,
				   IrrecoWindowEdit * edit_window);
static void irreco_window_edit_draw_background(IrrecoWindowEdit * edit_window);
static void irreco_window_load_background(IrrecoWindowEdit * edit_window,
					  const gchar * grid_image_filename,
					  GdkPixbuf ** grid_pixbuf_pointer);

void irreco_window_edit_button_motion_callback(IrrecoButton * irreco_button,
					       IrrecoWindowEdit * edit_window);
gboolean irreco_window_edit_drag_handler(IrrecoWindowEdit * edit_window);

void irreco_window_edit_button_press_callback(IrrecoButton * irreco_button,
					      IrrecoWindowEdit * edit_window);
void irreco_window_edit_button_release_callback(IrrecoButton * irreco_button,
						IrrecoWindowEdit * edit_window);

void irreco_window_edit_main_menu_create(IrrecoWindowEdit * edit_window);
void irreco_window_edit_main_menu_save(GtkMenuItem * menuitem,
				       IrrecoWindowEdit * edit_window);
void irreco_window_edit_main_menu_background(GtkMenuItem * menuitem,
					     IrrecoWindowEdit * edit_window);
void irreco_window_edit_main_menu_hardkeys(GtkMenuItem * menuitem,
					   IrrecoWindowEdit * edit_window);
void irreco_window_edit_main_menu_controllers(GtkMenuItem * menuitem,
					      IrrecoWindowEdit * edit_window);
void irreco_window_edit_main_menu_devices(GtkMenuItem * menuitem,
				          IrrecoWindowEdit * edit_window);
void irreco_window_edit_main_menu_download_irtrans(GtkMenuItem * menuitem,
						IrrecoWindowEdit * edit_window);
void irreco_window_edit_main_menu_download_lirc(GtkMenuItem * menuitem,
						IrrecoWindowEdit * edit_window);
void irreco_window_edit_main_menu_newbutton(GtkMenuItem * menuitem,
					    IrrecoWindowEdit * edit_window);
void irreco_window_edit_main_menu_select_button(GtkMenuItem * menuitem,
						IrrecoButton * irreco_button);
void irreco_window_edit_main_menu_theme_manager(GtkMenuItem * menuitem,
					        IrrecoWindowEdit
					        *edit_window);
void irreco_window_edit_button_menu_create(IrrecoWindowEdit * edit_window);
void irreco_window_edit_buttons_menu_update(IrrecoWindowEdit * edit_window);
void irreco_window_edit_buttons_menu_update_foreach(IrrecoButton * button,
						  ButtonMenuForeachData * data);
void irreco_window_edit_button_menu_show(IrrecoButton * irreco_button,
					 IrrecoWindowEdit * edit_window);
void irreco_window_edit_button_menu_show_position(GtkMenu * menu,
						  gint * x, gint * y,
						  gboolean * push_in,
						IrrecoWindowEdit * edit_window);
void irreco_window_edit_button_menu_front(GtkMenuItem * menuitem,
					  IrrecoWindowEdit * edit_window);
void irreco_window_edit_button_menu_edit(GtkMenuItem * menuitem,
					 IrrecoWindowEdit * edit_window);
void irreco_window_edit_button_menu_delete(GtkMenuItem * menuitem,
					   IrrecoWindowEdit * edit_window);
void irreco_window_edit_button_menu_done(GtkMenuItem * menuitem,
					 IrrecoWindowEdit * edit_window);


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Public functions.                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */

/**
 * Crate and display IrrecoWindowEdit.
 */
IrrecoWindowEdit *irreco_window_edit_create(IrrecoWindowManager * manager)
{
	IrrecoData *irreco_data = manager->irreco_data;
	IrrecoWindowEdit *edit_window;
	IRRECO_ENTER

	g_assert(manager != NULL);
	edit_window = g_slice_new0(IrrecoWindowEdit);
	edit_window->window		= irreco_window_create();
	edit_window->irreco_data	= irreco_data;
	edit_window->manager		= manager;
	edit_window->drag_grid		= 8;
	edit_window->drag_handler_set 	= FALSE;

	edit_window->expose_handler_id = g_signal_connect(
		G_OBJECT(irreco_window_get_gtk_window(edit_window->window)),
		"expose-event", G_CALLBACK(irreco_window_edit_expose),
		edit_window);

	irreco_window_edit_set_layout(edit_window, manager->current_layout);
	irreco_window_edit_main_menu_create(edit_window);
	irreco_window_edit_button_menu_create(edit_window);

	gtk_window_set_title(irreco_window_get_gtk_window(edit_window->window),
			     _("Edit remote"));
	IRRECO_RETURN_PTR(edit_window);
}

void irreco_window_edit_destroy(IrrecoWindowEdit *edit_window)
{
	IRRECO_ENTER

	/* Release background images. */
	if (edit_window->image_background != NULL) {
		g_object_unref(G_OBJECT(edit_window->image_background));
	}
	if (edit_window->image_grid_1 != NULL) {
		g_object_unref(G_OBJECT(edit_window->image_grid_1));
	}
	if (edit_window->image_grid_2 != NULL) {
		g_object_unref(G_OBJECT(edit_window->image_grid_2));
	}
	if (edit_window->image_grid_4 != NULL) {
		g_object_unref(G_OBJECT(edit_window->image_grid_4));
	}
	if (edit_window->image_grid_8 != NULL) {
		g_object_unref(G_OBJECT(edit_window->image_grid_8));
	}
	if (edit_window->image_grid_16 != NULL) {
		g_object_unref(G_OBJECT(edit_window->image_grid_16));
	}

	if (edit_window->irreco_layout != NULL) {
		irreco_button_layout_reset(edit_window->irreco_layout);
	}

	gtk_widget_destroy(edit_window->button_menu);
	irreco_window_destroy(edit_window->window);
	g_slice_free(IrrecoWindowEdit, edit_window);
	IRRECO_RETURN
}

void irreco_window_edit_set_layout(IrrecoWindowEdit * edit_window,
				   IrrecoButtonLayout * irreco_layout)
{
	IRRECO_ENTER

	if (edit_window->irreco_layout != NULL) {
		irreco_button_layout_reset(edit_window->irreco_layout);
	}

	if (irreco_layout == NULL) {
		edit_window->irreco_layout = NULL;
		IRRECO_RETURN
	}

	irreco_button_layout_reset(irreco_layout);
	edit_window->irreco_layout = irreco_layout;
	irreco_button_layout_set_motion_callback(
		irreco_layout, irreco_window_edit_button_motion_callback);
	irreco_button_layout_set_press_callback(
		irreco_layout, irreco_window_edit_button_press_callback);
	irreco_button_layout_set_release_callback(
		irreco_layout, irreco_window_edit_button_release_callback);
	irreco_button_layout_set_callback_data(irreco_layout, edit_window);

	irreco_button_layout_set_container(irreco_layout,
					   edit_window->window->layout);
	irreco_button_layout_create_widgets(irreco_layout);
	irreco_button_layout_set_size(irreco_layout,
				      IRRECO_LAYOUT_WIDTH,
				      IRRECO_LAYOUT_HEIGHT);

	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Help.                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Help
 * @{
 */

void irreco_window_edit_device_help(IrrecoWindowEdit * edit_window)
{
	IrrecoBackendManager *manager;
	IRRECO_ENTER

	manager = edit_window->irreco_data->irreco_backend_manager;
	IRRECO_BACKEND_MANAGER_FOREACH_INSTANCE(manager, instace)
		if (!irreco_string_table_is_empty(instace->device_list)) {
			IRRECO_RETURN
		}
	IRRECO_BACKEND_MANAGER_FOREACH_END

	/*irreco_info_dlg(irreco_window_get_gtk_window(edit_window->window),
			_(IRRECO_NO_DEVICE_HELP));*/
	IRRECO_RETURN
}

void irreco_window_edit_controller_help(IrrecoWindowEdit * edit_window)
{
	IrrecoBackendManager *manager;
	IRRECO_ENTER

	manager = edit_window->irreco_data->irreco_backend_manager;
	if (irreco_string_table_is_empty(manager->instance_table)) {
		irreco_info_dlg(irreco_window_get_gtk_window(
				edit_window->window),
				_(IRRECO_NO_CONTROLLER_HELP));
	}
	IRRECO_RETURN
}

gboolean irreco_window_edit_expose(GtkWidget * widget, GdkEventExpose * event,
				   IrrecoWindowEdit * edit_window)
{
	IRRECO_ENTER
	g_signal_handler_disconnect(
		G_OBJECT(irreco_window_get_gtk_window(edit_window->window)),
		edit_window->expose_handler_id);
	irreco_window_edit_controller_help(edit_window);
	IRRECO_RETURN_BOOL(FALSE);
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private functions.                                                         */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Private Functions
 * @{
 */

static void irreco_window_edit_draw_background(IrrecoWindowEdit * edit_window)
{
	gchar *image = NULL;
	GdkPixbuf **grid_pixbuf = NULL;
	IRRECO_ENTER

	switch (edit_window->drag_grid) {
	case 1:
		image = NULL;
		grid_pixbuf = NULL;
		break;

	case 2:
		image = "grid-2x2.png";
		grid_pixbuf = &edit_window->image_grid_2;
		break;

	case 4:
		image = "grid-4x4.png";
		grid_pixbuf = &edit_window->image_grid_4;
		break;

	case 8:
		image = "grid-8x8.png";
		grid_pixbuf = &edit_window->image_grid_8;
		break;

	case 16:
		image = "grid-16x16.png";
		grid_pixbuf = &edit_window->image_grid_16;
		break;

	default:
		IRRECO_ERROR("Unknown grid value \"%i\"\n",
			     edit_window->drag_grid);
		IRRECO_RETURN
	}

	irreco_window_load_background(edit_window, image, grid_pixbuf);
	IRRECO_RETURN
}

/**
 * Load window background images, and draw them.
 *
 * If the *grid_pixbuf_pointer is NULL this function will load the image into
 * a pixbuf and store it into the *grid_pixbuf_pointer.
 *
 * Or if *grid_pixbuf_pointer is set, then uses the existing pixbuf.
 *
 * Basically the grid_pixbuf_pointer is used to cache the image data.
 */
static void irreco_window_load_background(IrrecoWindowEdit * edit_window,
					  const gchar * grid_image_filename,
					  GdkPixbuf ** grid_pixbuf_pointer)
{
	const gchar *image;
	const GdkColor *color;
	GError *error = NULL;
	IRRECO_ENTER

	/* Load grid image. */
	if (grid_image_filename != NULL
	    && grid_pixbuf_pointer != NULL
	    && *grid_pixbuf_pointer == NULL) {
		gchar *image_path = g_build_path("/", IRRECO_IMAGE_DIR,
						 grid_image_filename, NULL);
		*grid_pixbuf_pointer = gdk_pixbuf_new_from_file_at_scale(
			image_path, IRRECO_SCREEN_WIDTH,
			IRRECO_SCREEN_HEIGHT, FALSE, &error);
		g_free(image_path);
		irreco_gerror_check_print(&error);
	}

	/* Load background image. */
	irreco_button_layout_get_bg(edit_window->irreco_layout, &image, &color);
	if (edit_window->image_background == NULL && image != NULL) {
		edit_window->image_background =
			gdk_pixbuf_new_from_file_at_scale(image,
			IRRECO_SCREEN_WIDTH, IRRECO_SCREEN_HEIGHT,
			FALSE, &error);
		irreco_gerror_check_print(&error);
	}

	if (grid_pixbuf_pointer == NULL || grid_image_filename == NULL) {
		irreco_window_set_background_buf(edit_window->window, color,
						 edit_window->image_background,
						 NULL);
	} else {
		irreco_window_set_background_buf(edit_window->window, color,
						 edit_window->image_background,
						 *grid_pixbuf_pointer);
	}
	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* IrrecoButton callbacks.                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name IrrecoButton callbacks
 * @{
 */

/**
 * Instead of handling the movement of mouse immediately here, in the
 * high priority event handler, we set up a low priority callback.
 * Basically this should make sure that if resoirrecoes are tight, some
 * movements will be ignored.
 */
void irreco_window_edit_button_motion_callback(IrrecoButton * irreco_button,
					       IrrecoWindowEdit * edit_window)
{
	GTimeVal now;
	IRRECO_ENTER

	if (edit_window->drag_target == NULL) {
		IRRECO_RETURN
	}

	/* Save pointer location at the time of the event. */
	gdk_window_get_pointer(edit_window->window->event_box->window,
			       &edit_window->drag_pointer_x,
			       &edit_window->drag_pointer_y,
			       &edit_window->drag_pointer_mask);

	/* Dont move button right away, this removes most jitters from fast
	   click button location reset code. */
	g_get_current_time(&now);
	if (irreco_time_diff(&edit_window->drag_start_time, &now)
	    <= 0.05 * IRRECO_SECOND_IN_USEC) {
		IRRECO_RETURN
	}

	/* Somethimes we get several drag events before we have time to
	   actually move the button. */
	if (edit_window->drag_handler_set != TRUE) {
		g_idle_add(G_SOURCEFUNC(irreco_window_edit_drag_handler),
			   edit_window);
		edit_window->drag_handler_set = TRUE;
	}
	IRRECO_RETURN
}
gboolean irreco_window_edit_drag_handler(IrrecoWindowEdit * edit_window)
{
	gint grid_x, grid_y;
	gint pointer_x, pointer_y;
	IrrecoButton *irreco_button = edit_window->drag_target;
	IRRECO_ENTER

	if (edit_window->drag_target == NULL) {
		edit_window->drag_handler_set = FALSE;
		IRRECO_RETURN_BOOL(FALSE);
	}

	/* New coordinates. */
	pointer_x = edit_window->drag_pointer_x
		    - edit_window->drag_pointer_offset_x;
	pointer_y = edit_window->drag_pointer_y
		    - edit_window->drag_pointer_offset_y;
	grid_x = pointer_x - (pointer_x % edit_window->drag_grid);
	grid_y = pointer_y - (pointer_y % edit_window->drag_grid);

	/* Move the widget only if the widget has moved in relation to the grid
	   and in relation of the old coordinates. */
	if ((pointer_x != irreco_button->x || pointer_y != irreco_button->y) &&
	    (grid_x != irreco_button->x || grid_y != irreco_button->y)) {
		edit_window->drag_button_moved = TRUE;
		irreco_button_move(edit_window->drag_target, &grid_x, &grid_y);
		IRRECO_DEBUG("Widget moved to: x%i y%i\n",
			     edit_window->drag_target->x,
			     edit_window->drag_target->y);
	}

	edit_window->drag_handler_set = FALSE;
	IRRECO_RETURN_BOOL(FALSE);
}

void irreco_window_edit_button_press_callback(IrrecoButton * irreco_button,
					      IrrecoWindowEdit * edit_window)
{
	gint x, y;
	GdkModifierType mask;
	IRRECO_ENTER

	/* Save start data. */
	g_get_current_time(&edit_window->drag_start_time);
	edit_window->drag_start_x = irreco_button->x;
	edit_window->drag_start_y = irreco_button->y;

	/* Set button as drag target. */
	edit_window->drag_button_moved = FALSE;
	edit_window->drag_target = irreco_button;
	gdk_window_get_pointer(edit_window->window->event_box->window,
			       &x, &y, &mask);
	edit_window->drag_pointer_offset_x = x - irreco_button->x;
	edit_window->drag_pointer_offset_y = y - irreco_button->y;
	IRRECO_DEBUG("Drag Offset: x%i y%i\n",
		     edit_window->drag_pointer_offset_x,
		     edit_window->drag_pointer_offset_y);
	IRRECO_DEBUG("Widget size: w%i h%i\n",
		     irreco_button->width, irreco_button->height);
	irreco_button_down(irreco_button);

	IRRECO_RETURN
}

void irreco_window_edit_button_release_callback(IrrecoButton * irreco_button,
						IrrecoWindowEdit * edit_window)
{
	GTimeVal now;
	IRRECO_ENTER

	/* Because of the impresision of the touch screen in N8xx tables,
	   a simple click on the screen may end up moving the button when
	   the user simply want's to see the popup menu. We try to fix this by
	   resetting the button location if the click is fast enough.*/
	g_get_current_time(&now);
	if (irreco_time_diff(&edit_window->drag_start_time, &now)
	    <= 0.1 * IRRECO_SECOND_IN_USEC) {
		irreco_button_move(edit_window->drag_target,
				   &edit_window->drag_start_x,
				   &edit_window->drag_start_y);
		IRRECO_DEBUG("Widget moved to: x%i y%i\n",
			     edit_window->drag_start_x,
			     edit_window->drag_start_y);
		edit_window->drag_button_moved = FALSE;
	}

	edit_window->drag_target = NULL;
	irreco_button_up(irreco_button);
	if (edit_window->drag_button_moved == FALSE) {
		irreco_window_edit_button_menu_show(irreco_button, edit_window);
	}
	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Main menu construction.                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Main menu construction
 * @{
 */

void irreco_window_edit_main_menu_create(IrrecoWindowEdit * edit_window)
{
	IRRECO_ENTER

	/* Build menu. */
	edit_window->menu = GTK_WIDGET( hildon_app_menu_new() );
	edit_window->menu_save = hildon_button_new_with_text(2 | 4, 0, NULL, "Save remote");
	edit_window->menu_background = hildon_button_new_with_text(2 | 4, 0, NULL, "Background");
	edit_window->menu_hardkeys = hildon_button_new_with_text(2 | 4, 0, NULL, "Hardware keys");
	edit_window->menu_controllers = hildon_button_new_with_text(2 | 4, 0, NULL, "Device Controllers");
	edit_window->menu_devices = hildon_button_new_with_text(2 | 4, 0, NULL, "Devices");
	edit_window->menu_download_irtrans = hildon_button_new_with_text(
						2 | 4, 0, NULL, "Download from IrrecoDB");
	edit_window->menu_download_lirc = hildon_button_new_with_text(
						2 | 4, 0, NULL, "Download from LircDB");
	edit_window->menu_newbutton = hildon_button_new_with_text(2 | 4, 0, NULL, "New button");
	edit_window->menu_theme_manager = hildon_button_new_with_text(2 | 4, 0, NULL, "Theme Manager");

	hildon_app_menu_append(HILDON_APP_MENU(edit_window->menu),
			       GTK_BUTTON(edit_window->menu_save));
	hildon_app_menu_append(HILDON_APP_MENU(edit_window->menu),
			       GTK_BUTTON(edit_window->menu_controllers));
	/* Enable back when hardkeys are fixed */
	/*hildon_app_menu_append(HILDON_APP_MENU(edit_window->menu),
			       GTK_BUTTON(edit_window->menu_hardkeys));*/
	hildon_app_menu_append(HILDON_APP_MENU(edit_window->menu),
			       GTK_BUTTON(edit_window->menu_background));
	/* Uncomment when..   never? */
	/*hildon_app_menu_append(HILDON_APP_MENU(edit_window->menu),
			       GTK_BUTTON(edit_window->menu_devices));*/
	hildon_app_menu_append(HILDON_APP_MENU(edit_window->menu),
			       GTK_BUTTON(edit_window->menu_download_lirc));
	hildon_app_menu_append(HILDON_APP_MENU(edit_window->menu),
			       GTK_BUTTON(edit_window->menu_newbutton));
	hildon_app_menu_append(HILDON_APP_MENU(edit_window->menu),
			       GTK_BUTTON(edit_window->menu_theme_manager));
	/* Enable when irtrans backend is ported */
	/*hildon_app_menu_append(HILDON_APP_MENU(edit_window->menu),
			       GTK_BUTTON(edit_window->menu_download_irtrans));*/

	/* Show menu. */
	hildon_window_set_app_menu(HILDON_WINDOW(edit_window->window),
			       HILDON_APP_MENU(edit_window->menu));

	gtk_widget_show_all(edit_window->menu);

	/* Connect signals. */
	g_signal_connect(G_OBJECT(edit_window->menu_save), "clicked",
			 G_CALLBACK(irreco_window_edit_main_menu_save),
			 edit_window);
	g_signal_connect(G_OBJECT(edit_window->menu_background), "clicked",
			 G_CALLBACK(irreco_window_edit_main_menu_background),
			 edit_window);
	g_signal_connect(G_OBJECT(edit_window->menu_hardkeys), "clicked",
			 G_CALLBACK(irreco_window_edit_main_menu_hardkeys),
			 edit_window);
	g_signal_connect(G_OBJECT(edit_window->menu_controllers), "clicked",
			 G_CALLBACK(irreco_window_edit_main_menu_controllers),
			 edit_window);
	g_signal_connect(G_OBJECT(edit_window->menu_devices), "clicked",
			 G_CALLBACK(irreco_window_edit_main_menu_devices),
			 edit_window);
	g_signal_connect(G_OBJECT(edit_window->menu_download_irtrans), "clicked",
			 G_CALLBACK(irreco_window_edit_main_menu_download_irtrans),
			 edit_window);
	g_signal_connect(G_OBJECT(edit_window->menu_download_lirc), "clicked",
			 G_CALLBACK(irreco_window_edit_main_menu_download_lirc),
			 edit_window);
	g_signal_connect(G_OBJECT(edit_window->menu_newbutton), "clicked",
			 G_CALLBACK(irreco_window_edit_main_menu_newbutton),
			 edit_window);
	g_signal_connect(G_OBJECT(edit_window->menu_theme_manager), "clicked",
			 G_CALLBACK(irreco_window_edit_main_menu_theme_manager),
			 edit_window);


	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Main menu callback functions.                                              */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Main menu callback functions
 * @{
 */

void irreco_window_edit_main_menu_save(GtkMenuItem * menuitem,
				       IrrecoWindowEdit * edit_window)
{
	IrrecoData *irreco_data = edit_window->irreco_data;
	IRRECO_ENTER

	irreco_config_save_layouts(irreco_data);
	irreco_window_manager_show(irreco_data->window_manager,
				   IRRECO_WINDOW_USER);
	IRRECO_RETURN
}

void irreco_window_edit_main_menu_background(GtkMenuItem * menuitem,
					     IrrecoWindowEdit * edit_window)
{
	IrrecoBackgroundDlg *background_dlg;
	IrrecoData *irreco_data = edit_window->irreco_data;
	IRRECO_ENTER
	background_dlg = irreco_background_dlg_create(irreco_data,
		irreco_window_get_gtk_window(edit_window->window));

	/* Ask for new background config, and cleanup old config if user
	   clicks ok. */
	if (irreco_background_dlg_run(background_dlg,
				      edit_window->irreco_layout)) {
		if (edit_window->image_background != NULL) {
			g_object_unref(G_OBJECT(edit_window->image_background));
			edit_window->image_background = NULL;
		}
		irreco_window_edit_draw_background(edit_window);
	}
	irreco_background_dlg_destroy(background_dlg);
	IRRECO_RETURN
}

void irreco_window_edit_main_menu_hardkeys(GtkMenuItem * menuitem,
					   IrrecoWindowEdit * edit_window)
{
	GtkWidget *dlg;
	IRRECO_ENTER

	dlg = irreco_hardkey_dlg_new(irreco_window_manager_get_gtk_window(
				     edit_window->irreco_data->window_manager),
				     edit_window->irreco_data,
				     edit_window->irreco_layout->hardkey_map);
	gtk_dialog_run(GTK_DIALOG(dlg));
	gtk_widget_destroy(dlg);
	IRRECO_RETURN
}

void irreco_window_edit_main_menu_controllers(GtkMenuItem * menuitem,
					      IrrecoWindowEdit * edit_window)
{
	IRRECO_ENTER
	irreco_show_backend_dlg(edit_window->irreco_data,
			       irreco_window_get_gtk_window(
			       edit_window->window));
	irreco_window_edit_device_help(edit_window);
	IRRECO_RETURN
}

void irreco_window_edit_main_menu_devices(GtkMenuItem * menuitem,
				          IrrecoWindowEdit * edit_window)
{
	IRRECO_ENTER
	irreco_show_device_dlg(edit_window->irreco_data,
			       irreco_window_get_gtk_window(
			       edit_window->window));
	IRRECO_RETURN
}

void irreco_window_edit_main_menu_download_irtrans(GtkMenuItem * menuitem,
						IrrecoWindowEdit * edit_window)
{
	IRRECO_ENTER
	irreco_show_webdb_dlg(edit_window->irreco_data,
			      irreco_window_get_gtk_window(
			      edit_window->window));
	IRRECO_RETURN
}

void irreco_window_edit_main_menu_download_lirc(GtkMenuItem * menuitem,
						IrrecoWindowEdit * edit_window)
{
	IRRECO_ENTER
	irreco_show_lircdb_dlg(edit_window->irreco_data,
			      irreco_window_get_gtk_window(
			      edit_window->window));
	IRRECO_RETURN
}

void irreco_window_edit_main_menu_newbutton(GtkMenuItem * menuitem,
					    IrrecoWindowEdit * edit_window)
{
	IRRECO_ENTER
	if (irreco_button_dlg_run(edit_window->irreco_data->new_button_dlg,
				  edit_window->window,
				  _("New button"))){
		IrrecoButton *button = irreco_button_create(
			edit_window->irreco_layout, 100, 100, NULL, NULL,
			edit_window->irreco_data->cmd_chain_manager);
		irreco_button_dlg_data_to_button(
			edit_window->irreco_data->new_button_dlg, button);
		irreco_button_create_widget(button);
	}
	IRRECO_RETURN
}

void irreco_window_edit_main_menu_theme_manager(GtkMenuItem * menuitem,
					     IrrecoWindowEdit * edit_window)
{
	IRRECO_ENTER
	irreco_show_theme_manager_dlg(edit_window->irreco_data,
			      irreco_window_get_gtk_window(
			      edit_window->window));


	IRRECO_RETURN

}

/** @} */




/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Button menu construction.                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Button menu construction
 * @{
 */

/**
 * Create edit menu.
 */
void irreco_window_edit_button_menu_create(IrrecoWindowEdit * edit_window)
{
	GtkWidget *button_menu;
	GtkWidget *button_menu_front;
	GtkWidget *button_menu_edit;
	GtkWidget *button_menu_delete;

	IRRECO_ENTER

	/* Create menu. */
	button_menu = gtk_menu_new();
	button_menu_front = gtk_menu_item_new_with_label(_("Bring to front"));
	button_menu_edit = gtk_menu_item_new_with_label(_("Edit button"));
	button_menu_delete = gtk_menu_item_new_with_label(_("Delete button"));

	gtk_menu_append(button_menu, button_menu_front);
	gtk_menu_append(button_menu, button_menu_edit);
	gtk_menu_append(button_menu, button_menu_delete);

	/* Connect signals. */
	g_signal_connect(G_OBJECT(button_menu_front), "activate",
			 G_CALLBACK(irreco_window_edit_button_menu_front),
			 edit_window);
	g_signal_connect(G_OBJECT(button_menu_edit), "activate",
			 G_CALLBACK(irreco_window_edit_button_menu_edit),
			 edit_window);
	g_signal_connect(G_OBJECT(button_menu_delete), "activate",
			 G_CALLBACK(irreco_window_edit_button_menu_delete),
			 edit_window);

	edit_window->button_menu = button_menu;
	gtk_widget_show_all(GTK_WIDGET(button_menu));

	IRRECO_RETURN
}

/**
 * Show button context menu which the user can use to edit the button.
 */
void irreco_window_edit_button_menu_show(IrrecoButton * irreco_button,
					 IrrecoWindowEdit * edit_window)
{
	IRRECO_ENTER

	/* Make sure that the whole button is visible. */
	irreco_window_scroll_visible(edit_window->window,
				     irreco_button->x + irreco_button->width,
				     irreco_button->y + irreco_button->height);
	irreco_window_scroll_visible(edit_window->window,
				     irreco_button->x,
				     irreco_button->y);

	/* Show popup menu. */
	edit_window->button_menu_target = irreco_button;
	gtk_menu_popup(GTK_MENU(edit_window->button_menu), NULL, NULL,
		G_MENU_POS_FUNC(irreco_window_edit_button_menu_show_position),
		edit_window, 0, gtk_get_current_event_time());

	IRRECO_RETURN
}

/**
 * Calculate a new position for the popup that is right next to the button.
 */
void irreco_window_edit_button_menu_show_position(GtkMenu * menu,
						  gint * x, gint * y,
						  gboolean * push_in,
						 IrrecoWindowEdit * edit_window)
{
	IrrecoButton * irreco_button = edit_window->button_menu_target;
	gint window_x, window_y;
	gdouble scroll_x, scroll_y;
	GdkWindow *window;
	GtkRequisition requisition;
	IRRECO_ENTER

	gtk_widget_size_request(GTK_WIDGET(menu), &requisition);
	IRRECO_DEBUG("Button menu size: w%i h%i\n",
		     requisition.width, requisition.height);

	window = edit_window->window->event_box->window;
	gdk_window_get_origin(window, &window_x, &window_y);
	IRRECO_DEBUG("Eventbox offset: x%i y%i\n", window_x, window_y);

	/* Calculate a position to the right side of the button. */
	irreco_window_get_scroll_offset(edit_window->window,
					&scroll_x, &scroll_y);
	*x = window_x + irreco_button->x + irreco_button->width - scroll_x;
	*y = window_y + irreco_button->y - scroll_y;

	/* If there is not enough space, move popup to the left side of the
	   button. */
	if (*x + requisition.width > IRRECO_SCREEN_WIDTH) {
		*x = window_x + irreco_button->x - requisition.width - scroll_x;
	}
	if (*y + requisition.height > IRRECO_SCREEN_HEIGHT) {
		*y -= *y + requisition.height - IRRECO_LAYOUT_HEIGHT - scroll_y;
	}
	IRRECO_DEBUG("Button menu position: x%i y%i\n", *x, *y);

	IRRECO_RETURN
}

/** @} */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Button menu callbacks.                                                     */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Button menu callbacks
 * @{
 */

void irreco_window_edit_button_menu_front(GtkMenuItem * menuitem,
					  IrrecoWindowEdit * edit_window)
{
	IRRECO_ENTER
	irreco_button_to_front(edit_window->button_menu_target);

	IRRECO_RETURN
}

void irreco_window_edit_button_menu_edit(GtkMenuItem * menuitem,
					 IrrecoWindowEdit * edit_window)
{
	IrrecoButtonDlg* button_dlg;
	IrrecoButton* button = edit_window->button_menu_target;
	IRRECO_ENTER

	button_dlg = irreco_button_dlg_create(edit_window->irreco_data);
	irreco_button_dlg_data_from_button(button_dlg, button);
	irreco_button_dlg_print(button_dlg);
	irreco_button_dlg_run(button_dlg, edit_window->window,
			      _("Edit button"));
	irreco_button_dlg_print(button_dlg);
	irreco_button_dlg_data_to_button(button_dlg, button);
	irreco_button_create_widget(button);
	irreco_button_dlg_destroy(button_dlg);

	IRRECO_RETURN
}

void irreco_window_edit_button_menu_delete(GtkMenuItem * menuitem,
					   IrrecoWindowEdit * edit_window)
{
	IRRECO_ENTER
	if (irreco_yes_no_dlg(irreco_window_get_gtk_window(edit_window->window),
							 _("Delete button?"))) {
		irreco_button_destroy(edit_window->button_menu_target);
	}
	IRRECO_RETURN
}

void irreco_window_edit_button_menu_done(GtkMenuItem * menuitem,
					 IrrecoWindowEdit * edit_window)
{
	IRRECO_ENTER
	IRRECO_RETURN
}

/** @} */
/** @} */


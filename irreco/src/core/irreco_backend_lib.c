/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include "irreco_backend_lib.h"
#include <gmodule.h>


/**
 * @addtogroup IrrecoBackendLib
 * @ingroup Irreco
 *
 * Load backend libraries and stores information about them.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoBackendLib.
 */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

IrrecoBackendLib* irreco_backend_lib_load_with_name(const gchar *filepath,
						  const gchar *filename);
void irreco_backend_lib_load_dir_foreach(IrrecoDirForeachData * dir_data);




/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Function pointer cheking.                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Function pointer cheking
 * @{
 */

/*
 * Check function pointers using a way that does not create errors in GCC -Wall.
 */
typedef void (*IrrecoVoidFunction) (void);
typedef union {
	IrrecoVoidFunction function;
	gpointer pointer;
} IrrecoVoidFunctionUnion;

#define HAS_FUNCTION(__lib, __name) irreco_backend_lib_has_function(\
		(IrrecoVoidFunction) __lib->api->__name, #__name )

gboolean irreco_backend_lib_has_function(IrrecoVoidFunction function,
					  const gchar * name)
{
	IrrecoVoidFunctionUnion function_union;
	IRRECO_ENTER

	function_union.function = function;
	if (function_union.pointer == NULL) {
		IRRECO_ERROR("Could not get function pointer for \"%s\".\n",
			     name);
		IRRECO_RETURN_BOOL(FALSE);
	}
	IRRECO_RETURN_BOOL(TRUE);
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Backend loading.                                                           */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Backend loading
 * @{
 */


/*
 * Fix "warning: dereferencing type-punned pointer will break strict-aliasing
 * rules" warning during g_module_symbol() call the way GCC docs suggest.
 */
typedef union {
	GetIrrecoBackendFunctionTable function;
	gpointer pointer;
} GetFunctionTableUnion;

/*
 * Load backend from file.
 *
 * Args:
 *     filepath  Complete path to library.
 *     filename  Basename of the library.
 */
IrrecoBackendLib* irreco_backend_lib_load_with_name(const gchar *filepath,
						    const gchar *filename)
{
	GetFunctionTableUnion get_irreco_backend_function_table;
	IrrecoBackendLib* backend_lib;
	IRRECO_ENTER

	IRRECO_PRINTF("Loading backend lib: \"%s\"\n", filename);
	IRRECO_PRINTF("Path: \"%s\"\n", filepath);
	backend_lib = g_slice_new0(IrrecoBackendLib);

	backend_lib->module = g_module_open(filepath, G_MODULE_BIND_LOCAL);
	if (backend_lib->module == NULL) {
		IRRECO_ERROR("Failed to open library \"%s\" with "
			     "g_module_open().\n", filepath);
		goto error;
	}

	if (!g_module_symbol(backend_lib->module,
			     "get_irreco_backend_function_table",
			     &get_irreco_backend_function_table.pointer)) {
		IRRECO_ERROR("Could not find function "
			     "\"get_irreco_backend_function_table\""
			     "in backend \"%s\"\n", filepath);
		goto error;
	}

	IRRECO_PRINTF("Getting function table.\n");
	backend_lib->api = get_irreco_backend_function_table.function();
	if (backend_lib->api == NULL) {
		IRRECO_ERROR("Could not get function table "
			     "from backend \"%s\"\n", filepath);
		goto error;
	}

	/* Check api version. */
	if (backend_lib->api->version < IRRECO_BACKEND_API_VERSION) {
		IRRECO_ERROR("Backend must implement atleast version %i of "
			     "backend api. Only version %i implemented by"
			     "backend \"%s\"\n",
			     IRRECO_BACKEND_API_VERSION,
			     backend_lib->api->version, filepath);
		goto error;
	}

	if (!HAS_FUNCTION(backend_lib, get_error_msg)
	    || !HAS_FUNCTION(backend_lib, create)
	    || !HAS_FUNCTION(backend_lib, destroy)
	    || !HAS_FUNCTION(backend_lib, from_conf)
	    || !HAS_FUNCTION(backend_lib, to_conf)
	    || !HAS_FUNCTION(backend_lib, get_devices)
	    || !HAS_FUNCTION(backend_lib, get_commands)
	    || !HAS_FUNCTION(backend_lib, send_command)
	    || !HAS_FUNCTION(backend_lib, configure)) {
		IRRECO_ERROR("Could not get complete function table "
			     "from backend \"%s\"\n", filepath);
		goto error;
	}

	if ((backend_lib->api->flags & IRRECO_BACKEND_EDITABLE_DEVICES) && (
	    !HAS_FUNCTION(backend_lib, create_device)
	    || !HAS_FUNCTION(backend_lib, is_device_editable)
	    || !HAS_FUNCTION(backend_lib, edit_device)
	    || !HAS_FUNCTION(backend_lib, delete_device)
	    )) {
		IRRECO_ERROR("Backend \"%s\" claims to support "
			     "editable devices, but does not provide "
			     "the needed functions.\n", filepath);
		goto error;
	}

	if ((backend_lib->api->flags & IRRECO_BACKEND_CONFIGURATION_EXPORT) && (
	    !HAS_FUNCTION(backend_lib, export_conf)
	    || !HAS_FUNCTION(backend_lib, import_conf)
	    || !HAS_FUNCTION(backend_lib, check_conf)
	    )) {
		IRRECO_ERROR("Backend \"%s\" claims to support "
			     "configuration export, but does not provide "
			     "the needed functions.\n", filepath);
		goto error;
	}
	if (backend_lib->api->name == NULL) {
		IRRECO_ERROR("Backend name is not set in function table.\n");
		goto error;
	}

	backend_lib->filepath = g_strdup(filepath);
	backend_lib->filename = g_strdup(filename);
	backend_lib->name = g_strdup(backend_lib->api->name);
	g_module_make_resident(backend_lib->module);

	IRRECO_PRINTF("Backend \"%s\" loaded successfully\n",
		      backend_lib->name);
	IRRECO_RETURN_PTR(backend_lib);


	/* Loading of module failed. Close module and free memory. */
	error:
	if (backend_lib->module) g_module_close(backend_lib->module);
	g_slice_free(IrrecoBackendLib, backend_lib);
	IRRECO_RETURN_PTR(NULL);
}

void irreco_backend_lib_close(IrrecoBackendLib * backend_lib)
{
	IRRECO_ENTER
	if (backend_lib == NULL) IRRECO_RETURN

	g_module_close(backend_lib->module);
	backend_lib->module = NULL;
	g_free(backend_lib->name);
	backend_lib->name = NULL;
	g_free(backend_lib->filename);
	backend_lib->filename = NULL;
	g_free(backend_lib->filepath);
	backend_lib->filepath = NULL;

	g_slice_free(IrrecoBackendLib, backend_lib);

	IRRECO_RETURN
}

/** @} */
/** @} */












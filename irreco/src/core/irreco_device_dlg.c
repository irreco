/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *	      &  2008  Joni Kokko     (t5kojo01@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_device_dlg.h"
#include "irreco_backend_device.h"
#include "irreco_select_instance_dlg.h"
#include "irreco_webdb_upload_dlg.h"

/**
 * @addtogroup IrrecoDeviceDlg
 * @ingroup Irreco
 *
 * Dialog for creating, deleting and editing backend devices.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoDeviceDlg.
 */


G_DEFINE_TYPE (IrrecoDeviceDlg, irreco_device_dlg, IRRECO_TYPE_DLG)


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

enum {
	IRRECO_DEVICE_NEW,
	IRRECO_DEVICE_EDIT,
	IRRECO_DEVICE_DELETE,
	IRRECO_DEVICE_UPLOAD,
	IRRECO_DEVICE_REFERESH
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

static void irreco_device_dlg_populate(IrrecoDeviceDlg *self);
static void irreco_device_dlg_selection_changed(GtkTreeSelection * selection,
					 IrrecoDeviceDlg *self);



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

static void irreco_device_dlg_dispose (GObject *object)
{
  if (G_OBJECT_CLASS (irreco_device_dlg_parent_class)->dispose)
    G_OBJECT_CLASS (irreco_device_dlg_parent_class)->dispose (object);
}

static void irreco_device_dlg_finalize (GObject *object)
{
  if (G_OBJECT_CLASS (irreco_device_dlg_parent_class)->finalize)
    G_OBJECT_CLASS (irreco_device_dlg_parent_class)->finalize (object);
}

static void irreco_device_dlg_class_init (IrrecoDeviceDlgClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  object_class->dispose = irreco_device_dlg_dispose;
  object_class->finalize = irreco_device_dlg_finalize;
}

static void irreco_device_dlg_init (IrrecoDeviceDlg *self)
{
	GtkWidget *padding;
	GtkTreeSelection *select;
	GtkWidget *pannablearea;
	IRRECO_ENTER

	/* Construct dialog. */
	gtk_window_set_title(GTK_WINDOW(self), _("Devices"));
	gtk_window_set_modal(GTK_WINDOW(self), TRUE);
	gtk_window_set_destroy_with_parent(GTK_WINDOW(self), TRUE);
	gtk_dialog_set_has_separator(GTK_DIALOG(self), FALSE);
	gtk_dialog_add_buttons(GTK_DIALOG(self),
			       GTK_STOCK_NEW, IRRECO_DEVICE_NEW,
		               /*GTK_STOCK_EDIT, IRRECO_DEVICE_EDIT,
		               GTK_STOCK_DELETE, IRRECO_DEVICE_DELETE,*/
		 	       _("Upload"), IRRECO_DEVICE_UPLOAD,
			       NULL);

	/* Add widgets. */
	self->vbox = gtk_vbox_new(FALSE, 0);
	self->listbox = IRRECO_LISTBOX(
			irreco_listbox_text_new());/*_with_autosize(0, 700, 100, 250));*/
	pannablearea = hildon_pannable_area_new();

	/*gtk_container_add(GTK_CONTAINER(self->vbox), GTK_WIDGET(self->listbox));*/
	gtk_container_add(GTK_CONTAINER(self->vbox), GTK_WIDGET(pannablearea));
/*	gtk_container_add(GTK_CONTAINER(pannablearea), GTK_WIDGET(self->listbox));*/
	hildon_pannable_area_add_with_viewport(HILDON_PANNABLE_AREA(pannablearea),
					       GTK_WIDGET(self->listbox));

	padding = irreco_gtk_pad(self->vbox, 8, 8, 8, 8);
	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(self)->vbox), padding);

	/* Selection signal. */
	select = gtk_tree_view_get_selection(
			 GTK_TREE_VIEW(self->listbox->tree_view));
	g_signal_connect(G_OBJECT(select), "changed",
			 G_CALLBACK(irreco_device_dlg_selection_changed),
			 self);

	IRRECO_RETURN
}

GtkWidget * irreco_device_dlg_new(GtkWindow *parent)
{
	IrrecoDeviceDlg *self;
	IRRECO_ENTER

	self = g_object_new(IRRECO_TYPE_DEVICE_DLG, NULL);
	irreco_dlg_set_parent(IRRECO_DLG(self), parent);

	IRRECO_RETURN_PTR(self);
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Private Functions
 * @{
 */

/*
 * Populate listbox with devices.
 */
static void irreco_device_dlg_populate(IrrecoDeviceDlg *self)
{
	GString *str = NULL;
	IRRECO_ENTER

	str = g_string_new(NULL);

	irreco_listbox_clear(self->listbox);
	IRRECO_BACKEND_MANAGER_FOREACH_INSTANCE(
	    self->irreco_data->irreco_backend_manager, instance)
		irreco_backend_instance_get_name_and_description(instance);
		IRRECO_BACKEND_INSTANCE_FOREACH(instance, device)
			g_string_printf(str, "%s - %s",
			       irreco_backend_instance_get_name_and_description(
				       instance),
				device->name);
			IRRECO_DEBUG("Adding \"%s\" to listbox.\n", str->str);
			irreco_listbox_text_append(IRRECO_LISTBOX_TEXT(
						   self->listbox),
						   str->str, device);

		IRRECO_BACKEND_INSTANCE_FOREACH_END
	IRRECO_BACKEND_MANAGER_FOREACH_END

	g_string_free(str, TRUE);
	IRRECO_RETURN
}

void irreco_device_dlg_responce_new(IrrecoDeviceDlg *self)
{
	IrrecoBackendInstance *instance = NULL;
	IRRECO_ENTER

	if (irreco_show_select_instance_dlg(self->irreco_data,
					    GTK_WINDOW(self),
					    NULL,
					    &instance)) {
		if (irreco_backend_instance_api_edit(instance)) {
			irreco_backend_instance_create_device(instance,
							      GTK_WINDOW(self));
			irreco_backend_instance_get_devcmd_list(instance);
			irreco_device_dlg_populate(self);
		} else {
			const gchar *desc = NULL;
			desc = irreco_backend_instance_get_name_and_description(
				instance);
			irreco_info_dlg_printf( GTK_WINDOW( self ),
						_( "%s\ndoes not support\n"
					        "creating a new device." ),
						desc);
		}
	}

	IRRECO_RETURN
}

void irreco_device_dlg_responce_edit(IrrecoDeviceDlg *self)
{
	IrrecoBackendDevice *device = NULL;
	IRRECO_ENTER

	device = (IrrecoBackendDevice *) irreco_listbox_get_selection_data(
		IRRECO_LISTBOX(self->listbox));
	if (device == NULL) IRRECO_RETURN;
	if (!irreco_backend_device_is_editable(device)) IRRECO_RETURN;

	irreco_backend_device_edit(device, GTK_WINDOW(self));
	irreco_backend_instance_save_to_conf(device->backend_instance);
	irreco_backend_instance_get_devcmd_list(device->backend_instance);
	irreco_device_dlg_populate(self);

	IRRECO_RETURN
}

void irreco_device_dlg_responce_delete(IrrecoDeviceDlg *self)
{
	IrrecoBackendDevice *device = NULL;
	IRRECO_ENTER

	device = (IrrecoBackendDevice *) irreco_listbox_get_selection_data(
		IRRECO_LISTBOX(self->listbox));
	if (device == NULL) IRRECO_RETURN;
	if (!irreco_backend_device_is_editable(device)) IRRECO_RETURN;

	irreco_backend_device_delete(device, GTK_WINDOW(self));
	irreco_backend_instance_get_devcmd_list(device->backend_instance);
	irreco_device_dlg_populate(self);

	IRRECO_RETURN
}

void irreco_device_dlg_responce_upload(IrrecoDeviceDlg *self)
{
	IrrecoBackendFileContainer *file_container = NULL;
	IrrecoBackendDevice        *device         = NULL;
	IRRECO_ENTER

	device = (IrrecoBackendDevice *) irreco_listbox_get_selection_data(
		IRRECO_LISTBOX(self->listbox));
	if (device == NULL) IRRECO_RETURN;

	if (irreco_backend_instance_export_conf(device->backend_instance,
						device->name,
						&file_container)) {
		irreco_show_webdb_upload_dlg(self->irreco_data,
					     GTK_WINDOW(self),
					     file_container,
					     device->name,
					     NULL, NULL);
		irreco_backend_file_container_free(file_container);
	}

	IRRECO_RETURN
}

void irreco_device_dlg_responce_referesh(IrrecoDeviceDlg *self)
{
	IRRECO_ENTER

	irreco_backend_manager_get_devcmd_lists(
		self->irreco_data->irreco_backend_manager);
	irreco_device_dlg_populate(self);

	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Functions                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */

void irreco_show_device_dlg(IrrecoData *irreco_data,
			    GtkWindow *parent)
{
	IrrecoDeviceDlg     *self           = NULL;
	gboolean             loop           = TRUE;
	IRRECO_ENTER

	self = IRRECO_DEVICE_DLG( irreco_device_dlg_new( parent ));
	self->irreco_data = irreco_data;
	irreco_device_dlg_populate( self );
	irreco_device_dlg_responce_referesh( self );
	gtk_widget_show_all( GTK_WIDGET( self ));

	while (loop == TRUE)
	{
		switch ( gtk_dialog_run( GTK_DIALOG( self ))) {
		case IRRECO_DEVICE_NEW:
			irreco_device_dlg_responce_new( self );
			break;

		case IRRECO_DEVICE_EDIT:
			irreco_device_dlg_responce_edit( self );
			break;

		case IRRECO_DEVICE_DELETE:
			irreco_device_dlg_responce_delete( self );
			break;

		case IRRECO_DEVICE_UPLOAD:
			irreco_device_dlg_responce_upload( self );
			break;

/*		case IRRECO_DEVICE_REFERESH:
			irreco_device_dlg_responce_referesh( self );
			break;*/

		case GTK_RESPONSE_DELETE_EVENT:
			loop = FALSE;
			break;
		}

		gtk_widget_grab_focus( self->listbox->tree_view );

	}
	gtk_widget_destroy( GTK_WIDGET( self ));
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Events and Callbacks                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Events and Callbacks
 * @{
 */

/*
 * Enable / disable buttons depending on editability.
 */
static void irreco_device_dlg_selection_changed(GtkTreeSelection *selection,
					 	IrrecoDeviceDlg  *self)
{
/*	IrrecoBackendDevice *device        = NULL;
	GtkWidget           *edit_button   = NULL;
	GtkWidget           *delete_button = NULL;
	GtkWidget           *upload_button = NULL;*/
	IRRECO_ENTER

/*	device = (IrrecoBackendDevice *) irreco_listbox_get_selection_data(
		IRRECO_LISTBOX(self->listbox));
	if (device == NULL) IRRECO_RETURN;

	edit_button   = irreco_gtk_dialog_get_button(GTK_WIDGET(self), 1);
	delete_button = irreco_gtk_dialog_get_button(GTK_WIDGET(self), 2);
	upload_button = irreco_gtk_dialog_get_button(GTK_WIDGET(self), 3);

	if (irreco_backend_device_is_editable(device)) {
		gtk_widget_set_sensitive(edit_button, TRUE);
		gtk_widget_set_sensitive(delete_button, TRUE);
	} else {
		gtk_widget_set_sensitive(edit_button, FALSE);
		gtk_widget_set_sensitive(delete_button, FALSE);
	}

	if (irreco_backend_device_is_exportable(device)) {
		gtk_widget_set_sensitive(upload_button, TRUE);
	} else {
		gtk_widget_set_sensitive(upload_button, FALSE);
	}*/

	IRRECO_RETURN
}

/** @} */
/** @} */

/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_button.h"

/**
 * @addtogroup IrrecoButtonLayout
 * @ingroup Irreco
 *
 * Buttonlayout is mainly an array of buttons. Since all buttons will be
 * allocated dynamically, is is a good idea to have an object that will
 * manage all those allocations, and can free them when it is destroyed.
 *
 * Also IRRECO must support several button layouts for different devices, so
 * this object provides that too.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoButtonLayout.
 */


const gchar *irreco_button_layout_default_image =
	IRRECO_BG_IMAGE_DIR "/" IRRECO_DEFAULT_BG_IMAGE;
const GdkColor irreco_button_layout_default_color =
	{0, 0x946B, 0x946B, 0x946B};

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

IrrecoButtonLayout *irreco_button_layout_create(GtkWidget * container,
						IrrecoCmdChainManager *manager)
{
	IrrecoButtonLayout *irreco_layout;
	IRRECO_ENTER

	irreco_layout			= g_slice_new0(IrrecoButtonLayout);
	irreco_layout->name		= g_string_new(NULL);
	irreco_layout->background_image = g_string_new(NULL);
	irreco_layout->button_array	= g_ptr_array_new();
	irreco_layout->filename		= g_string_new(NULL);

	if (manager != NULL) {
		irreco_layout->hardkey_map = irreco_hardkey_map_new(manager);
	}

	irreco_button_layout_set_bg_color(irreco_layout,
					  &irreco_button_layout_default_color);
	irreco_button_layout_set_container(irreco_layout, container);
	IRRECO_RETURN_PTR(irreco_layout);
}

void irreco_button_layout_destroy(IrrecoButtonLayout * irreco_layout)
{
	IRRECO_ENTER
	g_assert(irreco_layout != NULL);

	irreco_hardkey_map_free(irreco_layout->hardkey_map);
	irreco_button_layout_destroy_buttons(irreco_layout);
	g_ptr_array_free(irreco_layout->button_array, TRUE);
	g_string_free(irreco_layout->name, TRUE);
	g_string_free(irreco_layout->background_image, TRUE);
	g_slice_free(IrrecoButtonLayout, irreco_layout);
	if(irreco_layout->filename != NULL)
	{
		g_string_free(irreco_layout->filename, TRUE);
	}

	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Public Functions                                                           */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */
/*
 * Set filename of layout.
 *
 */
void irreco_button_layout_set_filename(IrrecoButtonLayout * irreco_layout,
				   const gchar *filename)
{
	IRRECO_ENTER
	g_assert(irreco_layout != NULL);
	g_string_assign(irreco_layout->filename, filename);
	IRRECO_RETURN
}

/*
 * Set name of layout.
 *
 * This is basically a way to store the Remote name displayend in the UI.
 */
void irreco_button_layout_set_name(IrrecoButtonLayout * irreco_layout,
				   const gchar * name)
{
	IRRECO_ENTER
	g_assert(irreco_layout != NULL);
	g_string_assign(irreco_layout->name, name);
	IRRECO_RETURN
}

/*
 * Set background style.
 */
void irreco_button_layout_set_bg_type(IrrecoButtonLayout * irreco_layout,
				      IrrecoButtonLayoutBgType type)
{
	IRRECO_ENTER
	irreco_layout->background_type = type;
	IRRECO_RETURN
}

/*
 * Set background image of layout.
 *
 * This is basically a way to store the Remote name displayend in the UI.
 */
void irreco_button_layout_set_bg_image(IrrecoButtonLayout * irreco_layout,
				       const gchar * background_image)
{
	IRRECO_ENTER
	g_assert(irreco_layout != NULL);
	g_string_set_size(irreco_layout->background_image, 0);
	g_string_append(irreco_layout->background_image, background_image);
	IRRECO_RETURN
}

/*
 * Get background image
 *
 * Returns: Image location or NULL.
 */
const gchar *
irreco_button_layout_get_bg_image(IrrecoButtonLayout * irreco_layout)
{
	IRRECO_ENTER
	if (irreco_str_isempty(irreco_layout->background_image->str)) {
		IRRECO_RETURN_PTR(NULL);
	}
	IRRECO_RETURN_STR(irreco_layout->background_image->str);
}

/*
 * Set background color.
 */
void irreco_button_layout_set_bg_color(IrrecoButtonLayout * irreco_layout,
				       const GdkColor * color)
{
	IRRECO_ENTER
	memcpy(&irreco_layout->background_color, color, sizeof(GdkColor));
	irreco_layout->background_color.pixel = 0;
	IRRECO_RETURN
}

/*
 * Get bg variables according to IrrecoButtonLayout->background_type.
 */
void irreco_button_layout_get_bg(IrrecoButtonLayout * irreco_layout,
				 const gchar ** image,
				 const GdkColor ** color)
{
	IRRECO_ENTER

	*image = NULL;
	*color = NULL;

	switch (irreco_layout->background_type) {
	case IRRECO_BACKGROUND_DEFAULT:
		*image = irreco_button_layout_default_image;
		*color = &irreco_button_layout_default_color;
		break;

	case IRRECO_BACKGROUND_IMAGE:
		*image = irreco_button_layout_get_bg_image(irreco_layout);
	case IRRECO_BACKGROUND_COLOR:
		*color = irreco_button_layout_get_bg_color(irreco_layout);
		break;
	}

	IRRECO_RETURN
}

void irreco_button_layout_set_size(IrrecoButtonLayout * irreco_layout,
				   gint width, gint height)
{
	IRRECO_ENTER
	g_assert(irreco_layout != NULL);

	irreco_layout->width = width;
	irreco_layout->height = height;
	IRRECO_DEBUG("Layout size: w%i h%i\n", irreco_layout->width,
		     irreco_layout->height);
	irreco_button_layout_validate_positions(irreco_layout);

	IRRECO_RETURN
}

/*
 * Set the container where the buttons are shown.
 * The container should be GtkFixed or GtkLayout.
 */
void irreco_button_layout_set_container(IrrecoButtonLayout * irreco_layout,
					GtkWidget * container)
{
	IRRECO_ENTER
	g_assert(irreco_layout != NULL);

	if (container == NULL){
		IRRECO_DEBUG("Container is NULL.\n");
	}else if (GTK_IS_FIXED(container)) {
		IRRECO_DEBUG("Container type is GtkFixed.\n");
	} else if (GTK_IS_LAYOUT(container)) {
		IRRECO_DEBUG("Container type is GtkLayout.\n");
	} else {
		IRRECO_ERROR("Container is not GtkFixed or GtkLayout\n");
	}

	irreco_button_layout_destroy_widgets(irreco_layout);
	irreco_layout->container = container;

	IRRECO_RETURN
}

/*
 * Call gtk_fixed_put() or gtk_layout_put() depending on container type.
 */
void irreco_button_layout_container_put(IrrecoButtonLayout * irreco_layout,
					GtkWidget *child_widget,
					gint x, gint y)
{
	IRRECO_ENTER
	if (irreco_layout->container == NULL) IRRECO_RETURN
	if (GTK_IS_FIXED(irreco_layout->container)) {
		gtk_fixed_put(GTK_FIXED(irreco_layout->container),
			      child_widget, x, y);
	} else if (GTK_IS_LAYOUT(irreco_layout->container)) {
		gtk_layout_put(GTK_LAYOUT(irreco_layout->container),
			       child_widget, x, y);
	}

	IRRECO_RETURN
}

/*
 * Call gtk_fixed_move() or gtk_layout_move() depending on container type.
 */
void irreco_button_layout_container_move(IrrecoButtonLayout * irreco_layout,
					 GtkWidget *child_widget,
					 gint x, gint y)
{
	IRRECO_ENTER
	if (irreco_layout->container == NULL) IRRECO_RETURN
	if (GTK_IS_FIXED(irreco_layout->container)) {
		gtk_fixed_move(GTK_FIXED(irreco_layout->container),
			       child_widget, x, y);
	} else if (GTK_IS_LAYOUT(irreco_layout->container)) {
		gtk_layout_move(GTK_LAYOUT(irreco_layout->container),
			        child_widget, x, y);
	}

	IRRECO_RETURN
}

/*
 * Destroy widgets and remove callbacks.
 */
void irreco_button_layout_reset(IrrecoButtonLayout * irreco_layout)
{
	IRRECO_ENTER
	if (irreco_layout == NULL) IRRECO_RETURN
	irreco_button_layout_destroy_widgets(irreco_layout);
	irreco_button_layout_set_motion_callback(irreco_layout, NULL);
	irreco_button_layout_set_press_callback(irreco_layout, NULL);
	irreco_button_layout_set_release_callback(irreco_layout, NULL);
	irreco_button_layout_set_callback_data(irreco_layout, NULL);
	irreco_button_layout_buttons_up(irreco_layout);
	IRRECO_RETURN
}


/*
 * Foreach frappers. These simply call the obvious function on all IrrecoButtons.
 */
void irreco_button_layout_destroy_buttons(IrrecoButtonLayout * irreco_layout)
{
	IRRECO_ENTER
	g_assert(irreco_layout != NULL);

	IRRECO_PTR_ARRAY_BACKWARDS(irreco_layout->button_array, IrrecoButton *,
				   irreco_button)
		irreco_button_destroy(irreco_button);
	IRRECO_PTR_ARRAY_BACKWARDS_END

	IRRECO_RETURN
}
void irreco_button_layout_destroy_widgets(IrrecoButtonLayout * irreco_layout)
{
	IRRECO_ENTER
	g_assert(irreco_layout != NULL);

	IRRECO_PTR_ARRAY_BACKWARDS(irreco_layout->button_array, IrrecoButton *,
				   irreco_button)
		irreco_button_destroy_widget(irreco_button);
	IRRECO_PTR_ARRAY_BACKWARDS_END

	IRRECO_RETURN
}
void irreco_button_layout_create_widgets(IrrecoButtonLayout * irreco_layout)
{
	IRRECO_ENTER
	g_assert(irreco_layout != NULL);

	IRRECO_PTR_ARRAY_FORWARDS(irreco_layout->button_array, IrrecoButton *,
				   irreco_button)
		irreco_button_create_widget(irreco_button);
	IRRECO_PTR_ARRAY_BACKWARDS_END

	IRRECO_RETURN
}
void irreco_button_layout_validate_positions(IrrecoButtonLayout * irreco_layout)
{
	gint x, y;
	IRRECO_ENTER
	g_assert(irreco_layout != NULL);

	IRRECO_PTR_ARRAY_BACKWARDS(irreco_layout->button_array, IrrecoButton *,
				   irreco_button)
		x = irreco_button->x;
		y = irreco_button->y;
		irreco_button_move(irreco_button, &x, &y);
	IRRECO_PTR_ARRAY_BACKWARDS_END

	IRRECO_RETURN
}
void irreco_button_layout_reindex(IrrecoButtonLayout * irreco_layout)
{
	guint index = 0;
	IRRECO_ENTER
	g_assert(irreco_layout != NULL);

	IRRECO_PTR_ARRAY_FORWARDS(irreco_layout->button_array, IrrecoButton *,
				   irreco_button)
		irreco_button->index = index++;
	IRRECO_PTR_ARRAY_BACKWARDS_END

	IRRECO_RETURN
}
void irreco_button_layout_buttons_up(IrrecoButtonLayout * irreco_layout)
{
	IRRECO_ENTER
	g_assert(irreco_layout != NULL);

	IRRECO_PTR_ARRAY_FORWARDS(irreco_layout->button_array, IrrecoButton *,
				   irreco_button)
		irreco_button_up(irreco_button);
	IRRECO_PTR_ARRAY_BACKWARDS_END

	IRRECO_RETURN
}
/** @} */
/** @} */

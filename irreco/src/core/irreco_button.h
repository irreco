/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoButton
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoButton.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_BUTTON_H_TYPEDEF__
#define __IRRECO_BUTTON_H_TYPEDEF__
typedef struct _IrrecoButton IrrecoButton;
typedef struct _IrrecoButtonLayout IrrecoButtonLayout;
typedef void (*IrrecoButtonCallback) (IrrecoButton * irreco_button,
				      void * user_data);
typedef enum {
	IRRECO_BACKGROUND_DEFAULT = 0,
	IRRECO_BACKGROUND_COLOR,
	IRRECO_BACKGROUND_IMAGE
} IrrecoButtonLayoutBgType;

#endif /* __IRRECO_BUTTON_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_BUTTON_H__
#define __IRRECO_BUTTON_H__
#include "irreco.h"
#include "irreco_data.h"
#include "irreco_button_layout.h"
#include "irreco_cmd_chain_manager.h"
#include "irreco_hardkey_map.h"
#include "irreco_theme_button.h"


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/*
 * Button types.
 */
typedef enum _IrrecoButtonType {
	IRRECO_BTYPE_NONE = 0,
	IRRECO_BTYPE_GTK_BUTTON,	/* Vanilla GTK button. */
	IRRECO_BTYPE_SINGLE_IMAGE, /* Image button without secondary image. */
	IRRECO_BTYPE_DOUBLE_IMAGE	/* Image button. */
} IrrecoButtonType;

/*
 * Button states.
 */
typedef enum _IrrecoButtonState {
	IRRECO_BSTATE_UP = 0,
	IRRECO_BSTATE_DOWN,
	IRRECO_BSTATE_FLASH
} IrrecoButtonState;

/*
 * Button specific data.
 */
struct _IrrecoButton {
	guint index;

	/* Button configuration. */
	IrrecoThemeButton *style;
	gchar *title;
	gchar *command;
	gint x;
	gint y;
	IrrecoCmdChainManager *cmd_chain_manager;
	IrrecoCmdChainId cmd_chain_id;
	/*IrrecoCmdChain *cmd_chain;*/

	/* Pointers. */
	IrrecoButtonLayout *irreco_layout;
	IrrecoButtonType type;
	GtkWidget *widget;
	GtkWidget *widget_up;
	GtkWidget *widget_down;
	GtkWidget *widget_label;

	/* Widget state. */
	IrrecoButtonState state;
	gint width;
	gint height;

	/* Handlers. */
	gulong motion_handler_id;
	gulong press_handler_id;
	gulong release_handler_id;
	gulong destroy_handler_id;
	gulong size_request_handler_id;
	gulong extra_handler_id;
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
IrrecoButton *irreco_button_create(IrrecoButtonLayout * irreco_layout,
				   gdouble x,
				   gdouble y,
				   const gchar * title,
				   IrrecoThemeButton * style,
				   IrrecoCmdChainManager * manager);
void irreco_button_destroy(IrrecoButton * irreco_button);
void irreco_button_set_title(IrrecoButton * irreco_button, const gchar * title);
void irreco_button_set_cmd_chain_id(IrrecoButton * irreco_button,
				    IrrecoCmdChainId cmd_chain_id);
void irreco_button_set_cmd_chain(IrrecoButton * irreco_button,
				 IrrecoCmdChain * cmd_chain);
IrrecoCmdChain *irreco_button_get_cmd_chain(IrrecoButton * irreco_button);
void irreco_button_set_style(IrrecoButton * irreco_button,
			     IrrecoThemeButton * style);
void irreco_button_create_widget(IrrecoButton * irreco_button);
void irreco_button_destroy_widget(IrrecoButton* irreco_button);
void irreco_button_down(IrrecoButton * irreco_button);
void irreco_button_up(IrrecoButton * irreco_button);
void irreco_button_move(IrrecoButton * irreco_button, gint * x, gint * y);
void irreco_button_to_front(IrrecoButton * irreco_button);
void irreco_button_flash(IrrecoButton * irreco_button);
void irreco_button_layout_set_name(IrrecoButtonLayout * irreco_layout,
				const gchar * name);
void irreco_button_layout_set_size(IrrecoButtonLayout * irreco_layout, gint width,
				gint height);


#endif /* __IRRECO_BUTTON_H__ */

/** @} */

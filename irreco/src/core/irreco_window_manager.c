/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_window_manager.h"

/**
 * @addtogroup IrrecoWindowManager
 * @ingroup Irreco
 *
 * Creates and destroys irreco windows, and stores some data for them.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoWindowManager.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

IrrecoWindowManager *irreco_window_manager_create(IrrecoData * irreco_data)
{
	IrrecoWindowManager *manager;
	IRRECO_ENTER

	manager = g_slice_new0(IrrecoWindowManager);
	manager->irreco_data = irreco_data;

	IRRECO_DEBUG("IrrecoData = %p\n", (void*) irreco_data);
	IRRECO_RETURN_PTR(manager);
}

void irreco_window_manager_destroy(IrrecoWindowManager * manager)
{
	IRRECO_ENTER
	g_slice_free(IrrecoWindowManager, manager);
	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Public Functions                                                           */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */
gboolean irreco_window_manager_clean(IrrecoWindowManager * manager);
void irreco_window_manager_show(IrrecoWindowManager * manager,
				IrrecoWindowId id)
{
	IRRECO_ENTER

	if (manager->current_layout != NULL) {
		irreco_button_layout_reset(manager->current_layout);
	}
	if (manager->to_be_destroyed != 0) {
		IRRECO_RETURN
	}

	switch (id) {
	case IRRECO_WINDOW_USER:

		if (manager->edit_window != NULL) {
			manager->to_be_destroyed = IRRECO_WINDOW_EDIT;
			irreco_window_edit_set_layout(manager->edit_window,
						      NULL);
			gtk_idle_add(GTK_FUNCTION(irreco_window_manager_clean),
				     manager);
		}

		manager->current_window = IRRECO_WINDOW_USER;
		manager->user_window = irreco_window_user_create(manager);
		irreco_window_user_set_layout(manager->user_window,
					      manager->current_layout);
		break;

	case IRRECO_WINDOW_EDIT:

		if (manager->user_window != NULL) {
			manager->to_be_destroyed = IRRECO_WINDOW_USER;
			irreco_window_user_set_layout(manager->user_window,
						      NULL);
			gtk_idle_add(GTK_FUNCTION(irreco_window_manager_clean),
				     manager);

		}

		manager->current_window = IRRECO_WINDOW_EDIT;
		manager->edit_window = irreco_window_edit_create(manager);
		break;

	default:
		IRRECO_ERROR("Unknown window id \"%i\".\n", id);
		break;
	}

	IRRECO_RETURN
}

/**
 * We use this to destroy the old window after the new one has been drawn
 * to the screen.
 */
gboolean irreco_window_manager_clean(IrrecoWindowManager * manager)
{
	IRRECO_ENTER

	switch (manager->to_be_destroyed) {
	case IRRECO_WINDOW_USER:
		irreco_window_user_destroy(manager->user_window);
		manager->user_window = NULL;
		break;

	case IRRECO_WINDOW_EDIT:
		irreco_window_edit_destroy(manager->edit_window);
		manager->edit_window = NULL;
		break;

	default:
		IRRECO_ERROR("Unknown window id \"%i\".\n",
			     manager->to_be_destroyed);
		break;
	}

	manager->to_be_destroyed = 0;
	IRRECO_RETURN_BOOL(FALSE);
}


void irreco_window_manager_set_layout(IrrecoWindowManager * manager,
				      IrrecoButtonLayout * layout)
{
	IRRECO_ENTER

	g_assert(manager != NULL);

	if (layout == NULL) {
		IRRECO_PRINTF("Layout is NULL.\n");
	} else if (layout == manager->current_layout) {
		IRRECO_PRINTF("Layout \"%s\" is already set.\n",
			      irreco_button_layout_get_name(layout));
		IRRECO_RETURN
	} else {
		IRRECO_PRINTF("Setting layout \"%s\".\n",
			      irreco_button_layout_get_name(layout));
	}

	irreco_button_layout_reset(layout);
	irreco_button_layout_reset(manager->current_layout);
	manager->current_layout = layout;

	switch (manager->current_window) {
	case IRRECO_WINDOW_USER:
		irreco_window_user_set_layout(manager->user_window, layout);
		break;

	case IRRECO_WINDOW_EDIT:
		irreco_window_edit_set_layout(manager->edit_window, layout);
		break;

	default:
		IRRECO_ERROR("Unknown window id \"%i\".\n",
			     manager->current_window);
		break;
	}

	IRRECO_RETURN
}

GtkWindow *irreco_window_manager_get_gtk_window(IrrecoWindowManager * manager)
{
	IRRECO_ENTER
	switch (manager->current_window) {
	case IRRECO_WINDOW_USER:
		IRRECO_DEBUG_LINE;
		IRRECO_RETURN_PTR(irreco_window_get_gtk_window(
				  manager->user_window->window));

	case IRRECO_WINDOW_EDIT:
		IRRECO_DEBUG_LINE;
		IRRECO_RETURN_PTR(irreco_window_get_gtk_window(
				  manager->edit_window->window));

	default:
		IRRECO_ERROR("Unknown window id \"%i\".\n",
			     manager->current_window);
		IRRECO_RETURN_PTR(NULL);
	}
}

/** @} */
/** @} */










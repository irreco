/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoButtonDlg
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoButtonDlg.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_BUTTON_DLG_H_TYPEDEF__
#define __IRRECO_BUTTON_DLG_H_TYPEDEF__
typedef struct _IrrecoButtonDlg IrrecoButtonDlg;
#endif /* __IRRECO_BUTTON_DLG_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_BUTTON_DLG_H__
#define __IRRECO_BUTTON_DLG_H__
#include "irreco.h"
#include "irreco_data.h"
#include "irreco_button.h"
#include "irreco_window.h"
#include "irreco_cmd.h"
#include "irreco_cmd_chain.h"
#include "irreco_theme_button.h"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
struct _IrrecoButtonDlg {
	IrrecoData		*irreco_data;
	GtkWidget 		*dialog;
	GtkWidget 		*editor;

	IrrecoButton 		*style_button;
	IrrecoButtonLayout 	*layout;

	/*
	GtkWidget 		*tree_view;
	GtkTreeStore 		*tree_store;
	gint			sel_index;
	*/

	/* New values. Used while the user is using the dialog. */
	GtkWidget 		*title_entry;
	/*IrrecoCmdChain		*new_cmd_chain;*/
	IrrecoThemeButton	*new_style;

	/* Accepted values. New values will be copied here if the user
	   clicks the OK button. */
	gchar			*accepted_title;
	IrrecoCmdChain		*accepted_cmd_chain;
	IrrecoThemeButton	*accepted_style;
	IrrecoThemeButton	*accepted_button_name;
	GString			*accepted_theme_name;
	guint			accepted_button_index;

};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
IrrecoButtonDlg* irreco_button_dlg_create(IrrecoData * irreco_data);
void irreco_button_dlg_destroy(IrrecoButtonDlg * button_dlg);


void irreco_button_dlg_set(IrrecoButtonDlg * button_dlg,
			   const gchar * title,
			   IrrecoCmdChain * cmd_chain,
			   IrrecoThemeButton * style);
gboolean irreco_button_dlg_run(IrrecoButtonDlg * button_dlg,
			       IrrecoWindow * parent,
			       const gchar * title);
void irreco_button_dlg_print(IrrecoButtonDlg * button_dlg);
void irreco_button_dlg_data_from_button(IrrecoButtonDlg * button_dlg,
				     IrrecoButton * irreco_button);
void irreco_button_dlg_data_to_button(IrrecoButtonDlg * button_dlg,
				   IrrecoButton * irreco_button);



#endif /* __IRRECO_BUTTON_DLG_H__ */

/** @} */

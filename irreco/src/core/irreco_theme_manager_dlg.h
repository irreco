/*
 * irreco - Ir Remote Control
 * Copyright (C) 2008 Pekka Gehör (pegu6@msn.com)
 * Joni Kokko (t5kojo01@students.oamk.fi),
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoThemeManagerDlg
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoThemeManagerDlg.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */

#ifndef __IRRECO_THEME_MANAGER_DLG_H_TYPEDEF__
#define __IRRECO_THEME_MANAGER_DLG_H_TYPEDEF__


/*
#define IRRECO_TYPE_THEME_MANAGER_DLG irreco_theme_manager_dlg_get_type()
#define IRRECO_THEME_MANAGER_DLG(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), IRRECO_TYPE_THEME_MANAGER_DLG,\ IrrecoThemeManagerDlg))
#define IRRECO_THEME_MANAGER_DLG_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), IRRECO_TYPE_THEME_MANAGER_DLG,\ IrrecoThemeManagerDlgClass))
#define IRRECO_IS_THEME_MANAGER_DLG(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), IRRECO_TYPE_THEME_MANAGER_DLG))
#define IRRECO_IS_THEME_MANAGER_DLG_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), IRRECO_TYPE_THEME_MANAGER_DLG))
#define IRRECO_THEME_MANAGER_DLG_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), IRRECO_TYPE_THEME_MANAGER_DLG, \ IrrecoThemeManagerDlgClass))

*/

#define IRRECO_TYPE_THEME_MANAGER_DLG     (irreco_theme_manager_dlg_get_type ())
#define IRRECO_THEME_MANAGER_DLG(obj)     (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
IRRECO_TYPE_THEME_MANAGER_DLG, IrrecoThemeManagerDlg))
#define IRRECO_THEME_MANAGER_DLG_CLASS(klass) \
(G_TYPE_CHECK_CLASS_CAST ((klass), IRRECO_TYPE_THEME_MANAGER_DLG, \
IrrecoThemeManagerDlgClass))
#define IRRECO_IS_THEME_MANAGER_DLG(obj)  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
IRRECO_TYPE_THEME_MANAGER_DLG))
#define IRRECO_IS_THEME_MANAGER_DLG_CLASS(klass)  \
(G_TYPE_CHECK_CLASS_TYPE ((klass), IRRECO_TYPE_THEME_MANAGER_DLG))
#define IRRECO_THEME_MANAGER_DLG_GET_CLASS(obj)   \
(G_TYPE_INSTANCE_GET_CLASS ((obj), IRRECO_TYPE_THEME_MANAGER_DLG, \
IrrecoThemeManagerDlgClass))





typedef struct _IrrecoThemeManagerDlg IrrecoThemeManagerDlg;
typedef struct _IrrecoThemeManagerDlgClass IrrecoThemeManagerDlgClass;

#endif /* __IRRECO_THEME_MANAGER_DLG_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_THEME_MANAGER_DLG_H__
#define __IRRECO_THEME_MANAGER_DLG_H__
#include "irreco.h"
/*#include "irreco_dlg.h"*/
#include "irreco_data.h"
#include "irreco_internal_dlg.h"
#include "irreco_internal_widget.h"
#include "irreco_theme_upload_dlg.h"
#include "irreco_webdb_cache.h"
#include "irreco_theme_creator_dlg.h"
#include "irreco_theme_save_dlg.h"

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

struct _IrrecoThemeManagerDlg {

	IrrecoInternalDlg 	parent;
	IrrecoTheme		*theme;
	IrrecoData 		*irreco_data;

	GtkWidget		*theme_image;
	GtkWidget		*banner;
	GtkTreeStore		*tree_store;
	GtkTreeView		*tree_view;
	GtkWidget 		*preview;
	GdkPixbuf 		*preview_button;

	/* For WebdbTheme */
	IrrecoWebdbTheme 	*webdb_theme;
	GtkTreeStore		*version_store;
	gint			selected_theme_id;
	gchar*			theme_folder;
	GtkWidget		*download_dialog;
	gint			theme_loader_index;

	/* For ThemeManager UI*/
	GtkWidget		*theme_comment;
	GtkWidget		*theme_creator;
	GtkWidget		*theme_downloaded;
	GtkWidget 		*theme_info_alignment;
	GtkWidget		*theme_name;
	GtkWidget		*theme_version;
	GtkWidget 		*label_creator;
	GtkWidget 		*label_download;
	GtkWidget               *combobox;
	/*Buttons*/
	GtkWidget		*refresh_button;
	GtkWidget		*upload_button;
	GtkWidget		*download_button;
	GtkWidget		*clear_button;
	GtkWidget		*new_button;
	GtkWidget		*edit_button;

	GtkWidget		*select_label;
	GtkWidget 		*label_combobox;
	GtkWidget		*config_uploaded;
	GtkWidget		*config_download_count;
	gint			 loader_func_id;
	gint			 loader_state;
	gint			 loader_pos;
	gchar			*preview_add;
	gchar			*delete_theme_name;

	/** The loaded items are childern of this iter.*/
	GtkTreeIter		*loader_parent_iter;
	GtkTreeIter		*loader_iter;
};



struct _IrrecoThemeManagerDlgClass {
	IrrecoInternalDlgClass parent_class;
};
/*
struct _IrrecoThemeManagerDlgClass {
	IrrecoInternalDlgClass parent_class;
};*/

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
GType irreco_theme_manager_dlg_get_type(void);
GtkWidget *irreco_theme_manager_dlg_new(IrrecoData *irreco_data,
				GtkWindow *parent);
void irreco_theme_manager_dlg_set_irreco_data(IrrecoThemeManagerDlg *self,
				      IrrecoData *irreco_data);
void irreco_show_theme_manager_dlg(IrrecoData *irreco_data, GtkWindow *parent);

#endif /* __IRRECO_THEME_MANAGER_DLG_H__ */

/** @} */

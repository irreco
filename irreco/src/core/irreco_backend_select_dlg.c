/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_backend_select_dlg.h"

/**
 * @addtogroup IrrecoBackendSelectDlg
 * @ingroup Irreco
 *
 * Shows a list of Backends to the user and ask the user to select one. This
 * dialog should be used trough irreco_show_backend_select_dlg().
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoBackendSelectDlg.
 */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

G_DEFINE_TYPE(IrrecoBackendSelectDlg, irreco_backend_select_dlg,
	      IRRECO_TYPE_DLG)

static void irreco_backend_select_dlg_finalize(GObject *object)
{
	G_OBJECT_CLASS(irreco_backend_select_dlg_parent_class)->
	    finalize(object);
}

static void
irreco_backend_select_dlg_class_init(IrrecoBackendSelectDlgClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS(klass);
	object_class->finalize = irreco_backend_select_dlg_finalize;
}

static void irreco_backend_select_dlg_init(IrrecoBackendSelectDlg *self)
{
	IRRECO_ENTER

	/* Construct dialog. */
	gtk_window_set_title(GTK_WINDOW(self),
			     _("What do you want to control?"));
	gtk_window_set_modal(GTK_WINDOW(self), TRUE);
	gtk_window_set_destroy_with_parent(GTK_WINDOW(self), TRUE);
	gtk_dialog_set_has_separator(GTK_DIALOG(self), FALSE);
	gtk_dialog_add_buttons(GTK_DIALOG(self),
			       GTK_STOCK_OK, GTK_RESPONSE_OK,
			       NULL);

	/* Create listbox. */
	self->listbox = irreco_listbox_text_new_with_autosize(360, 500,
							      100, 250);
	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(self)->vbox),
			  irreco_gtk_align(GTK_WIDGET(self->listbox),
			  0, 0, 1, 1, 8, 8, 8, 8));
	gtk_widget_show_all(GTK_WIDGET(self));
	IRRECO_RETURN
}

GtkWidget *irreco_backend_select_dlg_new(IrrecoData *irreco_data,
					 GtkWindow *parent)
{
	IrrecoBackendSelectDlg *self;
	IRRECO_ENTER

	self = g_object_new(IRRECO_TYPE_BACKEND_SELECT_DLG, NULL);
	irreco_dlg_set_parent(IRRECO_DLG(self), parent);
	irreco_backend_select_dlg_set_irreco_data(self, irreco_data);
	IRRECO_RETURN_PTR(self);
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Private Functions
 * @{
 */

/*
 * Generate the listbox.
 */
static void
irreco_backend_select_dlg_populate_listbox(IrrecoBackendSelectDlg * self)
{
	IRRECO_ENTER
	IRRECO_BACKEND_MANAGER_FOREACH_LIB(
	    self->irreco_data->irreco_backend_manager, lib)
		IRRECO_DEBUG("Adding backend lib \"%s\" to list.\n",
			     lib->filename);
		irreco_listbox_text_append(IRRECO_LISTBOX_TEXT(self->listbox),
					   lib->name, lib);
	IRRECO_STRING_TABLE_FOREACH_END
	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Functions                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */


void irreco_backend_select_dlg_set_irreco_data(IrrecoBackendSelectDlg *self,
					       IrrecoData *irreco_data)
{
	IRRECO_ENTER
	self->irreco_data = irreco_data;
	irreco_backend_select_dlg_populate_listbox(self);
	IRRECO_RETURN
}


/*
 * Show a list of backend and allow user to select one.
 *
 * Returns: TRUE if backend_lib is set, FALSE otherwise.
 */
gboolean irreco_show_backend_select_dlg(IrrecoData *irreco_data,
					GtkWindow *parent,
					IrrecoBackendLib ** backend_lib)
{
	gint rvalue = -1;
	gint sel_index;
	gpointer sel_user_data;
	IrrecoBackendSelectDlg *self;
	IRRECO_ENTER

	self = IRRECO_BACKEND_SELECT_DLG(irreco_backend_select_dlg_new(
		irreco_data, parent));

	do {
		switch (gtk_dialog_run(GTK_DIALOG(self))) {
		case GTK_RESPONSE_DELETE_EVENT:
			rvalue = FALSE;
			break;

		case GTK_RESPONSE_OK:
			irreco_listbox_get_selection(
				IRRECO_LISTBOX(self->listbox),
				&sel_index, NULL, &sel_user_data);

			if (sel_index >= 0) {
				*backend_lib =
					(IrrecoBackendLib *) sel_user_data;
				rvalue = TRUE;
			} else {
				irreco_info_dlg(GTK_WINDOW(self),
						_("Select backend type "
						"before clicking OK."));
			}
			break;
		}
	} while (rvalue == -1);

	gtk_widget_destroy(GTK_WIDGET(self));
	IRRECO_RETURN_BOOL(rvalue);
}

/** @} */
/** @} */




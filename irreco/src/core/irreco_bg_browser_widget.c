/*
 * irreco - Ir Remote Control
 * Copyright (C) 2008  Joni Kokko (t5kojo01@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_bg_browser_widget.h"
#include <hildon/hildon-banner.h>

/**
 * @addtogroup IrrecoBgBrowserWidget
 * @ingroup Irreco
 *
 * This widget helps selection of background.
 *
 * This widget allows background selection from themes
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoBgBrowserWidget.
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

GtkWidget *irreco_bg_browser_create_label(IrrecoBgBrowserWidget *self,
					  const gchar *text);

static gboolean irreco_bg_browser_widget_populate_themes(
						IrrecoBgBrowserWidget *self);

static gboolean irreco_bg_browser_widget_loader_images(
						IrrecoBgBrowserWidget *self);

static void irreco_bg_browser_widget_loader_start(IrrecoBgBrowserWidget *self,
						  GSourceFunc function,
						  GtkTreeIter *parent_iter);

static void irreco_bg_browser_widget_destroy_event(IrrecoBgBrowserWidget *self,
						   gpointer user_data);

static void irreco_bg_browser_widget_theme_selection_changed(
						GtkTreeSelection * selection,
					        IrrecoBgBrowserWidget *self);

static void irreco_bg_browser_widget_image_selection_changed(
						GtkTreeSelection * selection,
					        IrrecoBgBrowserWidget *self);

static gboolean irreco_bg_browser_widget_do_themes_exist(
						IrrecoBgBrowserWidget *self);



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/** Loader states. */
enum
{
	LOADER_STATE_INIT,
	LOADER_STATE_LOOP,
	LOADER_STATE_END,
	LOADER_STATE_CLEANUP
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

G_DEFINE_TYPE(IrrecoBgBrowserWidget, irreco_bg_browser_widget,
	      IRRECO_TYPE_INTERNAL_WIDGET)

static void irreco_bg_browser_widget_finalize(GObject *object)
{
	IRRECO_ENTER

	if (G_OBJECT_CLASS(irreco_bg_browser_widget_parent_class)->finalize)
	G_OBJECT_CLASS(irreco_bg_browser_widget_parent_class)->finalize (object);

	IRRECO_RETURN
}

static void irreco_bg_browser_widget_constructed(GObject *object)
{
	IrrecoBgBrowserWidget *self             = NULL;
	GtkWidget             *frame_for_themes = NULL;
	GtkWidget             *frame_for_images = NULL;
	const gchar           *theme_name       = NULL;
	IrrecoData            *irreco_data      = NULL;
	GtkWidget             *hbox             = NULL;
	IrrecoStringTable     *themes           = NULL;
	IRRECO_ENTER

	/* Constructed method boilerplate. */
	G_OBJECT_CLASS(irreco_bg_browser_widget_parent_class)->constructed(object);
	self = IRRECO_BG_BROWSER_WIDGET(object);

	/* Init variables. */
	hbox = gtk_hbox_new(0, 8);
	irreco_data = irreco_internal_widget_get_irreco_data(
		IRRECO_INTERNAL_WIDGET(self));
	themes = irreco_data->theme_manager->themes;
	self->current_theme = g_string_new("");
	self->current_image = NULL;

	/* Create themes */

	/* Create Frame for Themes */
	frame_for_themes = gtk_frame_new("");
	gtk_frame_set_label_widget(GTK_FRAME(frame_for_themes),
		irreco_gtk_label_bold("Themes", 0, 0, 0, 0, 0, 0));

	/* Create list of themes */
	self->themes = IRRECO_LISTBOX_TEXT(
		irreco_listbox_text_new_with_autosize(100, 200, 180, 180));
	gtk_container_add(GTK_CONTAINER(frame_for_themes),
			  irreco_gtk_align(GTK_WIDGET(self->themes),
		          0, 0, 1, 1, 1, 1, 1, 1));

	/* Create Images */

	/* Create frame for Images */
	frame_for_images = gtk_frame_new("");
	gtk_frame_set_label_widget(GTK_FRAME(frame_for_images),
		irreco_gtk_label_bold("Images", 0, 0, 0, 0, 0, 0));

	/* Create list of images */
	self->images = IRRECO_LISTBOX_IMAGE(
		irreco_listbox_image_new_with_autosize(100, 400, 180, 180));
	irreco_listbox_set_select_new_rows(IRRECO_LISTBOX(self->images), FALSE);
	gtk_box_pack_start(GTK_BOX(hbox), GTK_WIDGET(self->images),
			   TRUE, TRUE, 0);

	/* Create error text */
	self->error_text = irreco_bg_browser_create_label(self,
		"This theme does not contain any background images.");
	gtk_box_pack_start(GTK_BOX(hbox), GTK_WIDGET(self->error_text),
			   TRUE, TRUE, 0);
	gtk_container_add(GTK_CONTAINER(frame_for_images),
			  irreco_gtk_align(GTK_WIDGET(hbox),
		          0, 0, 1, 1, 1, 1, 1, 1));

	/* Signal handlers. */
	g_signal_connect(G_OBJECT(self), "destroy",
		G_CALLBACK(irreco_bg_browser_widget_destroy_event), NULL);

	g_signal_connect(G_OBJECT(IRRECO_LISTBOX(self->themes)->tree_selection),
		"changed",
		G_CALLBACK(irreco_bg_browser_widget_theme_selection_changed),
		self);

	g_signal_connect(G_OBJECT(IRRECO_LISTBOX(self->images)->tree_selection),
		"changed",
		G_CALLBACK(irreco_bg_browser_widget_image_selection_changed),
		self);

	/* Create hbox */
	self->hbox = g_object_new(GTK_TYPE_HBOX, NULL);
	gtk_box_set_spacing(GTK_BOX(self->hbox), 8);

	gtk_box_pack_start(GTK_BOX(self->hbox), GTK_WIDGET(frame_for_themes),
			   TRUE, TRUE, 0);

	gtk_box_pack_start(GTK_BOX(self->hbox), GTK_WIDGET(frame_for_images),
			   TRUE, TRUE, 0);

	gtk_box_pack_start(GTK_BOX(self), GTK_WIDGET(self->hbox),
			   FALSE, TRUE, 0);

	/* Fill widget. */
	if (irreco_bg_browser_widget_do_themes_exist(self)) {

		irreco_bg_browser_widget_populate_themes(self);
		irreco_listbox_set_selection(IRRECO_LISTBOX(self->themes), 0);

		irreco_string_table_index(themes, 0, &theme_name, NULL);

		g_string_printf(self->current_theme, "%s", theme_name);

		irreco_bg_browser_widget_loader_start(self,
		G_SOURCEFUNC(irreco_bg_browser_widget_loader_images), NULL);
	}

	IRRECO_RETURN
}

static void irreco_bg_browser_widget_class_init(IrrecoBgBrowserWidgetClass *klass)
{
	GObjectClass *object_class;
	IRRECO_ENTER

	object_class = G_OBJECT_CLASS (klass);
	object_class->finalize = irreco_bg_browser_widget_finalize;
	object_class->constructed = irreco_bg_browser_widget_constructed;

	IRRECO_RETURN
}

static void irreco_bg_browser_widget_init(IrrecoBgBrowserWidget *self)
{
}

IrrecoBgBrowserWidget *irreco_bg_browser_widget_new(IrrecoData *irreco_data)
{
	IrrecoBgBrowserWidget *self;
	IRRECO_ENTER

	self = g_object_new(IRRECO_TYPE_BG_BROWSER_WIDGET,
			    "irreco-data", irreco_data,
			    NULL);
	IRRECO_RETURN_PTR(GTK_WIDGET(self));
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Private Functions
 * @{
 */

/**
 * Create a center aligned label with text wrapping and marginals.
 */
GtkWidget *irreco_bg_browser_create_label(IrrecoBgBrowserWidget *self,
					  const gchar *text)
{
	GtkWidget *label = NULL;
	IRRECO_ENTER

	label = gtk_label_new(text);
	gtk_label_set_justify(GTK_LABEL(label), GTK_JUSTIFY_CENTER);
	gtk_label_set_line_wrap(GTK_LABEL(label), TRUE);
	gtk_label_set_line_wrap_mode(GTK_LABEL(label), PANGO_WRAP_WORD_CHAR);

	gtk_widget_set_size_request(GTK_WIDGET(label), 300, -1);
	label = irreco_gtk_align(label, 0.5, 0.5, 1, 1, 8, 8, 8, 8);
	IRRECO_RETURN_PTR(label);
}

/**
 * Show hildon progressbar banner.
 *
 * This function will create a new banner if one has not been created yet,
 * if banner already exists, it's properties will be changed.
 *
 * @param text		Text to show.
 * @param fraction	Value of progress.
 */
static void irreco_bg_browser_widget_set_banner(IrrecoBgBrowserWidget *self,
						const gchar *text,
						gdouble fraction)
{
	IRRECO_ENTER
	if (self->banner == NULL) {
		self->banner = hildon_banner_show_progress(
			GTK_WIDGET(self), NULL, "");
	}

	hildon_banner_set_text(HILDON_BANNER(self->banner), text);
	hildon_banner_set_fraction(HILDON_BANNER(self->banner), fraction);
	IRRECO_RETURN
}

/**
 * Destroy banner, if it exists.
 */
static void irreco_bg_browser_widget_hide_banner(IrrecoBgBrowserWidget *self)
{
	IRRECO_ENTER
	if (self->banner) {
		gtk_widget_destroy(self->banner);
		self->banner = NULL;
	}
	IRRECO_RETURN
}

/**
 * Start a loader state machine if one is not running already.
 */
static void irreco_bg_browser_widget_loader_start(IrrecoBgBrowserWidget *self,
						  GSourceFunc function,
						  GtkTreeIter *parent_iter)
{
	IRRECO_ENTER

	if (self->loader_func_id == 0) {
		if (function) {
			self->loader_func_id = g_idle_add(function, self);
		} else {
			IRRECO_ERROR("Loader function pointer not given.\n");
		}
	}

	IRRECO_RETURN
}

/**
 * Stop and cleanup loader if a loader is running.
 */
static void irreco_bg_browser_widget_loader_stop(IrrecoBgBrowserWidget *self)
{
	IRRECO_ENTER
	if (self->loader_func_id != 0) {
		g_source_remove(self->loader_func_id);
		self->loader_func_id = 0;
		self->loader_state = 0;
	}
	IRRECO_RETURN
}

/**
 * Theme loader.
 *
 * This loader will request a list of themes from the IrrecoThemeManager and
 * update the TreeView accordingly.
 */

static gboolean irreco_bg_browser_widget_populate_themes(
						IrrecoBgBrowserWidget *self)
{
	IrrecoStringTable  *themes        = NULL;
	IrrecoData         *irreco_data   = NULL;
	IrrecoThemeManager *theme_manager = NULL;
	IRRECO_ENTER

	irreco_data = irreco_internal_widget_get_irreco_data(
		IRRECO_INTERNAL_WIDGET(self));
	theme_manager = irreco_data->theme_manager;
	themes = irreco_theme_manager_get_themes(theme_manager);

	if (themes != NULL) {
		IRRECO_STRING_TABLE_FOREACH_KEY(themes, key)
			irreco_listbox_text_append(self->themes, key, NULL);
		IRRECO_STRING_TABLE_FOREACH_END
	} else {
		irreco_error_dlg(GTK_WINDOW(self),
			"There's no themes installed");
	}

	IRRECO_RETURN_BOOL(TRUE);
}

/**
 * Background-image loader.
 *
 * This loader will request a list of background-images from the
 * IrrecoThemeManager and update the TreeView accordingly.
 */
static gboolean irreco_bg_browser_widget_loader_images(
						IrrecoBgBrowserWidget *self)
{
	IrrecoData         *irreco_data = NULL;
	IrrecoThemeManager *manager     = NULL;
	IRRECO_ENTER

	irreco_data = irreco_internal_widget_get_irreco_data(
		IRRECO_INTERNAL_WIDGET(self));
	manager = irreco_data->theme_manager;

	switch (self->loader_state) {
	case LOADER_STATE_INIT:
		irreco_bg_browser_widget_set_banner(self, _("Loading ..."), 0);
		self->loader_state = LOADER_STATE_LOOP;
		self->loader_index = 0;
		irreco_listbox_clear(IRRECO_LISTBOX(self->images));
		IRRECO_RETURN_BOOL(TRUE);

	case LOADER_STATE_LOOP: {
		gint theme_bg_count;
		gfloat banner;
		const gchar *image_name;
		IrrecoTheme *theme;

		irreco_string_table_get(manager->themes,
					self->current_theme->str,
					(gpointer *) &theme);

		theme_bg_count = irreco_string_table_lenght(theme->backgrounds);

		if (theme_bg_count > 0) {
			IrrecoThemeBg *background_image;
			gtk_widget_show(GTK_WIDGET(self->images));
			gtk_widget_hide(GTK_WIDGET(self->error_text));
			irreco_string_table_index(theme->backgrounds,
						self->loader_index,
						&image_name,
						(gpointer *) &background_image);

			irreco_listbox_image_append_with_size(self->images,
					background_image->image_name->str,
					background_image,
					background_image->image_path->str,
					IRRECO_SCREEN_WIDTH / 6,
					IRRECO_SCREEN_HEIGHT / 6);
		} else {
			gtk_widget_show(GTK_WIDGET(self->error_text));
			gtk_widget_hide(GTK_WIDGET(self->images));
			self->current_image = NULL;
			theme_bg_count = 1;
		}

		self->loader_index++;
		banner = (gfloat)self->loader_index / (gfloat)theme_bg_count;
		irreco_bg_browser_widget_set_banner(self, _("Loading ..."),
						    banner);

		if(self->loader_index >= theme_bg_count) {
			self->loader_state = LOADER_STATE_END;
		}

		IRRECO_RETURN_BOOL(TRUE);
		}

	case LOADER_STATE_END:
		irreco_bg_browser_widget_hide_banner(self);
		irreco_bg_browser_widget_loader_stop(self);
	}

	IRRECO_RETURN_BOOL(FALSE);
}

static gboolean irreco_bg_browser_widget_do_themes_exist(
						IrrecoBgBrowserWidget *self)
{
	IrrecoData *irreco_data = NULL;
	gboolean    rvalue      = TRUE;
	IRRECO_ENTER

	irreco_data = irreco_internal_widget_get_irreco_data(
		IRRECO_INTERNAL_WIDGET(self));

	if (irreco_string_table_lenght(
			irreco_data->theme_manager->themes) == 0) {
		GtkWidget *error = irreco_bg_browser_create_label(self,
			"No themes have been installed.");

		gtk_container_remove(GTK_CONTAINER(self),
				     GTK_WIDGET(self->hbox));
		gtk_box_pack_start(GTK_BOX(self), GTK_WIDGET(error),
				   FALSE, TRUE, 0);
		rvalue = FALSE;
	}
	IRRECO_RETURN_BOOL(rvalue);
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Public Functions                                                           */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */

/* Add functions here. */

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Events and Callbacks                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Events and Callbacks
 * @{
 */

static void irreco_bg_browser_widget_destroy_event(IrrecoBgBrowserWidget *self,
						   gpointer user_data)
{
	IRRECO_ENTER
	irreco_bg_browser_widget_loader_stop(self);
	IRRECO_RETURN
}

static void irreco_bg_browser_widget_theme_selection_changed(
						GtkTreeSelection *selection,
					        IrrecoBgBrowserWidget *self)
{
	gint sel_index = -1;
	gchar *sel_label = NULL;
	gpointer sel_user_data = NULL;
	IRRECO_ENTER

	if (self->loader_func_id != 0) {
		irreco_bg_browser_widget_loader_stop(self);
	}

	irreco_listbox_get_selection(IRRECO_LISTBOX(self->themes),
				     &sel_index, &sel_label, &sel_user_data);

	g_string_printf(self->current_theme, "%s", sel_label);

	irreco_bg_browser_widget_loader_start(self,
		G_SOURCEFUNC(irreco_bg_browser_widget_loader_images), NULL);

	g_free(sel_label);
	IRRECO_RETURN
}

static void irreco_bg_browser_widget_image_selection_changed(
						GtkTreeSelection *selection,
					        IrrecoBgBrowserWidget *self)
{
	gint sel_index = -1;
	gchar *sel_label = NULL;
	IrrecoThemeBg *bg = NULL;
	IRRECO_ENTER

	if (irreco_listbox_get_selection(IRRECO_LISTBOX(self->images),
				&sel_index, &sel_label, (gpointer *)&bg)) {
		self->current_image = bg;
	} else {
		self->current_image = NULL;
	}

	g_free(sel_label);
	IRRECO_RETURN
}

/** @} */

/** @} */

/*
 * irreco - Ir Remote Control
 * Copyright (C) 2008  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoThemeDownloadDlg
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoThemeDownloadDlg.
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef _IRRECO_THEME_DOWNLOAD_DLG_H_TYPEDEF__
#define _IRRECO_THEME_DOWNLOAD_DLG_H_TYPEDEF__

#define IRRECO_TYPE_THEME_DOWNLOAD_DLG irreco_theme_download_dlg_get_type()

#define IRRECO_THEME_DOWNLOAD_DLG(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), IRRECO_TYPE_THEME_DOWNLOAD_DLG, \
IrrecoThemeDownloadDlg))

#define IRRECO_THEME_DOWNLOAD_DLG_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), IRRECO_TYPE_THEME_DOWNLOAD_DLG, \
IrrecoThemeDownloadDlgClass))

#define IRRECO_IS_THEME_DOWNLOAD_DLG(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), IRRECO_TYPE_THEME_DOWNLOAD_DLG))

#define IRRECO_IS_THEME_DOWNLOAD_DLG_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), IRRECO_TYPE_THEME_DOWNLOAD_DLG))

#define IRRECO_THEME_DOWNLOAD_DLG_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), IRRECO_TYPE_THEME_DOWNLOAD_DLG, \
IrrecoThemeDownloadDlgClass))

typedef struct _IrrecoThemeDownloadDlg IrrecoThemeDownloadDlg;
typedef struct _IrrecoThemeDownloadDlgClass IrrecoThemeDownloadDlgClass;

#endif	/* _IRRECO_THEME_DOWNLOAD_DLG_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_THEME_DOWNLOAD_DLG_H__
#define __IRRECO_THEME_DOWNLOAD_DLG_H__

#include <glib-object.h>
#include "irreco.h"
#include "irreco_dlg.h"
#include "irreco_data.h"

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

struct _IrrecoThemeDownloadDlg {
	IrrecoDlg		parent;
	GtkWidget		*radio1;
	GtkWidget		*radio2;
	GtkWidget		*banner;
	GtkWidget		*label;

	IrrecoData 		*irreco_data;
	IrrecoWebdbTheme 	*webdb_theme;
	gchar			*theme_folder;
	gint			loader_func_id;
	gint			theme_loader_index;
	gint			 loader_state;
	gboolean		theme_downloaded_successfully;

	GtkTreeIter		*loader_parent_iter;
	GtkTreeIter		*loader_iter;
};

struct _IrrecoThemeDownloadDlgClass {
	IrrecoDlgClass parent_class;
};

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

GType irreco_theme_download_dlg_get_type (void);
IrrecoThemeDownloadDlg *irreco_theme_download_dlg_new (GtkWindow *parent);
gboolean irreco_theme_download_dlg_run(IrrecoData *irreco_data,
				       gint theme_id,
				       GtkWindow *parent);




#endif /* _IRRECO_THEME_DOWNLOAD_DLG_H__ */

/** @} */

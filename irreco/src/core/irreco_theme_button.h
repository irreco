/*
 * irreco - Ir Remote Control
 * Copyright (C) 2008 Joni Kokko (t5kojo01@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

 /**
 * @addtogroup IrrecoThemeButton
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoThemeButton.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_THEME_BUTTON_H_TYPEDEF__
#define __IRRECO_THEME_BUTTON_H_TYPEDEF__
typedef struct _IrrecoThemeButton IrrecoThemeButton;
#endif /* __IRRECO_THEME_BUTTON_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_THEME_BUTTON_H__
#define __IRRECO_THEME_BUTTON_H__
#include "irreco.h"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

struct _IrrecoThemeButton
{
	GString *theme_name;
	GString *style_name;
	GString *name;
	gboolean allow_text;
	GString	*image_up;
	GString *image_down;
	GString *text_format_up;
	GString *text_format_down;
	gint	 text_padding;
	gfloat	 text_h_align;
	gfloat	 text_v_align;
};

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

IrrecoThemeButton *irreco_theme_button_new(const char *theme_name);
void irreco_theme_button_free(IrrecoThemeButton *self);

void irreco_theme_button_set(IrrecoThemeButton *self,
			     const char *style_name,
			     const char *name,
			     gboolean	 allow_text,
			     const char *image_up,
			     const char *image_down,
			     const char *text_format_up,
			     const char *text_format_down,
			     gint	 text_padding,
			     gfloat	 text_h_align,
			     gfloat	 text_v_align);
void irreco_theme_button_set_from_button(IrrecoThemeButton *self,
					 IrrecoThemeButton *button);
IrrecoThemeButton *
irreco_theme_button_new_from_dir(const gchar *dir, const gchar *theme_name);
void irreco_theme_button_read(IrrecoThemeButton *self, const gchar *dir);
void irreco_theme_button_print(IrrecoThemeButton *self);
IrrecoThemeButton *irreco_theme_button_copy(IrrecoThemeButton *self);
gboolean
irreco_theme_button_save(IrrecoThemeButton *self, const gchar *path);
#endif /* __IRRECO_THEME_BUTTON_H__ */

/** @} */

/*
 * irreco - Ir Remote Control
 * Copyright (C) 2008  Joni Kokko (t5kojo01@students.oamk.fi),
 * Harri Vattulainen (t5vaha01@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoButtonBrowserWidget
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoButtonBrowserWidget.
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_BUTTON_BROWSER_WIDGET_H_TYPEDEF__
#define __IRRECO_BUTTON_BROWSER_WIDGET_H_TYPEDEF__

typedef struct _IrrecoButtonBrowserWidget IrrecoButtonBrowserWidget;
typedef struct _IrrecoButtonBrowserWidgetClass IrrecoButtonBrowserWidgetClass;

#define IRRECO_TYPE_BUTTON_BROWSER_WIDGET irreco_button_browser_widget_get_type()

#define IRRECO_BUTTON_BROWSER_WIDGET(obj)		\
	(G_TYPE_CHECK_INSTANCE_CAST ((obj),	\
	IRRECO_TYPE_BUTTON_BROWSER_WIDGET,	IrrecoButtonBrowserWidget))

#define IRRECO_BUTTON_BROWSER_WIDGET_CLASS(klass)	\
	(G_TYPE_CHECK_CLASS_CAST ((klass),	\
	IRRECO_TYPE_BUTTON_BROWSER_WIDGET, IrrecoButtonBrowserWidgetClass))

#define IRRECO_IS_BUTTON_BROWSER_WIDGET(obj) 	\
	(G_TYPE_CHECK_INSTANCE_TYPE ((obj),	\
	IRRECO_TYPE_BUTTON_BROWSER_WIDGET))

#define IRRECO_IS_BUTTON_BROWSER_WIDGET_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), IRRECO_TYPE_BUTTON_BROWSER_WIDGET))

#define IRRECO_BUTTON_BROWSER_WIDGET_GET_CLASS(obj) \
	(G_TYPE_INSTANCE_GET_CLASS ((obj),	\
	IRRECO_TYPE_BUTTON_BROWSER_WIDGET, IrrecoButtonBrowserWidgetClass))

#endif /* __IRRECO_BUTTON_BROWSER_WIDGET_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_BUTTON_BROWSER_WIDGET_H__
#define __IRRECO_BUTTON_BROWSER_WIDGET_H__
#include "irreco.h"
#include "irreco_data.h"
#include "irreco_listbox_text.h"
#include "irreco_listbox_image.h"
#include <glib-object.h>



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

struct _IrrecoButtonBrowserWidget {
	GtkVBox			 parent;
	IrrecoData		*irreco_data;
	IrrecoListboxText	*themes;
	IrrecoListboxImage	*images;
	GString			*current_theme;
	IrrecoThemeButton	*current_image;
	GtkWidget		*banner;
	GtkWidget		*hbox;
	gint			 loader_index;
	gint			 loader_state;
	gint			 loader_func_id;

};

struct _IrrecoButtonBrowserWidgetClass {
  GtkVBoxClass parent_class;
};


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

GType irreco_button_browser_widget_get_type(void);
IrrecoButtonBrowserWidget *irreco_button_browser_widget_new(IrrecoData *irreco_data);
void irreco_button_browser_widget_destroy(IrrecoButtonBrowserWidget * self);

#endif /* __IRRECO_BUTTON_BROWSER_WIDGET_H__ */

/** @} */

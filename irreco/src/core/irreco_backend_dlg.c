/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_backend_dlg.h"
#include "irreco_config.h"
#include "irreco_backend_select_dlg.h"

/**
 * @addtogroup IrrecoBackendDlg
 * @ingroup Irreco
 *
 * Show a dialog where the user can create, destroy and configure backend
 * instances.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoBackendDlg.
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes & Datatypes                                                     */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/


enum {
	IRRECO_BACKEND_NEW = 1,
	IRRECO_BACKEND_DELETE,
	IRRECO_BACKEND_CONFIGURE
};

static void irreco_backend_dlg_response_new(IrrecoBackendDlg *self);
static void irreco_backend_dlg_response_delete(IrrecoBackendDlg *self);
static void irreco_backend_dlg_response_configure(IrrecoBackendDlg *self);



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

G_DEFINE_TYPE(IrrecoBackendDlg, irreco_backend_dlg, IRRECO_TYPE_DLG)

static void irreco_backend_dlg_finalize(GObject * object)
{
	G_OBJECT_CLASS(irreco_backend_dlg_parent_class)->finalize(object);
}

static void irreco_backend_dlg_class_init(IrrecoBackendDlgClass * klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS(klass);
	object_class->finalize = irreco_backend_dlg_finalize;
}

static void irreco_backend_dlg_init(IrrecoBackendDlg * self)
{
	GtkWidget *align;
	IRRECO_ENTER

	/* Construct dialog. */
	gtk_window_set_title(GTK_WINDOW(self), _("Device controllers"));
	gtk_window_set_modal(GTK_WINDOW(self), TRUE);
	gtk_window_set_destroy_with_parent(GTK_WINDOW(self), TRUE);
	gtk_dialog_set_has_separator(GTK_DIALOG(self), FALSE);
	gtk_dialog_add_buttons(GTK_DIALOG(self),
			       GTK_STOCK_NEW, IRRECO_BACKEND_NEW,
			       GTK_STOCK_DELETE, IRRECO_BACKEND_DELETE,
			       _("Configure"), IRRECO_BACKEND_CONFIGURE,
/*			       GTK_STOCK_OK, GTK_RESPONSE_OK,*/
			       NULL);

	/* Create new listbox and add it to the dialog. */
	self->listbox = irreco_listbox_text_new_with_autosize(0, 600, 250, 350);
	align = irreco_gtk_align(self->listbox, 0, 0, 1, 1, 8, 8, 8, 8);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(self)->vbox),
			   align, 0, 0, 0);
	gtk_widget_show_all(GTK_WIDGET(self));
	IRRECO_RETURN
}

GtkWidget *irreco_backend_dlg_new(IrrecoData *irreco_data,
				  GtkWindow *parent)
{
	IrrecoBackendDlg *self;
	IRRECO_ENTER

	self = g_object_new(IRRECO_TYPE_BACKEND_DLG, NULL);
	irreco_dlg_set_parent(IRRECO_DLG(self), parent);
	irreco_backend_dlg_set_irreco_data(self, irreco_data);
	IRRECO_RETURN_PTR(self);
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Private Functions
 * @{
 */

/*
 * Generate the listbox.
 */
static void irreco_backend_dlg_populate_listbox(IrrecoBackendDlg *self)
{
	IrrecoBackendManager *manager;
	IRRECO_ENTER

	irreco_listbox_clear(IRRECO_LISTBOX(self->listbox));
	manager = self->irreco_data->irreco_backend_manager;
	IRRECO_BACKEND_MANAGER_FOREACH_INSTANCE(manager, instance)
		IRRECO_DEBUG("Adding backend instance \"%s\" to list.\n",
			     irreco_backend_instance_get_name(instance));
		irreco_backend_instance_get_description(instance);
		irreco_listbox_text_append(IRRECO_LISTBOX_TEXT(self->listbox),
			irreco_backend_instance_get_name_and_description(
			instance), NULL);
	IRRECO_STRING_TABLE_FOREACH_END
	IRRECO_RETURN
}

/*
 * Get the IrrecoBackendInstance which matches the current selection.
 */
static gboolean
irreco_backend_dlg_get_instance(IrrecoBackendDlg *self,
				IrrecoBackendInstance **instance)
{
	gint sel_index;
	IrrecoBackendManager *manager;
	IRRECO_ENTER

	manager = self->irreco_data->irreco_backend_manager;
	sel_index = irreco_listbox_get_selection_index(
		IRRECO_LISTBOX(self->listbox));

	if (sel_index >= 0) {
		if (irreco_string_table_index(manager->instance_table,
					      sel_index, NULL,
					      (gpointer *)instance)) {
			IRRECO_RETURN_BOOL(TRUE);
		} else {
			IRRECO_ERROR("No instance found for index \"%i\"\n",
				     sel_index);
		}
	} else {
		irreco_info_dlg(GTK_WINDOW(self),
				_("Please select something."));
	}
	IRRECO_RETURN_BOOL(FALSE);
}

/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Functions                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */

void irreco_show_backend_dlg(IrrecoData *irreco_data,
			    GtkWindow *parent)
{
	gboolean loop = TRUE;
	IrrecoBackendDlg *self;
	IRRECO_ENTER

	self = IRRECO_BACKEND_DLG(irreco_backend_dlg_new(irreco_data, parent));
	do {
		switch (gtk_dialog_run(GTK_DIALOG(self))) {
		case IRRECO_BACKEND_NEW:
			irreco_backend_dlg_response_new(self);
			break;

		case IRRECO_BACKEND_DELETE:
			irreco_backend_dlg_response_delete(self);
			break;

		case IRRECO_BACKEND_CONFIGURE:
			irreco_backend_dlg_response_configure(self);
			break;

		/*case GTK_RESPONSE_OK:*/
		case GTK_RESPONSE_DELETE_EVENT:
			loop = FALSE;
			break;
		}
		gtk_widget_grab_focus(self->listbox);
	} while (loop == TRUE);

	gtk_widget_destroy(GTK_WIDGET(self));
	IRRECO_RETURN
}

void irreco_backend_dlg_set_irreco_data(IrrecoBackendDlg *self,
					IrrecoData *irreco_data)
{
	IRRECO_ENTER
	self->irreco_data = irreco_data;
	irreco_backend_dlg_populate_listbox(self);
	IRRECO_RETURN
}

/**@} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Events and Callbacks                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Events and Callbacks
 * @{
 */

static void irreco_backend_dlg_response_new(IrrecoBackendDlg *self)
{
	IrrecoBackendLib *lib;
	IrrecoBackendManager *manager;
	IrrecoBackendInstance *instance;
	IRRECO_ENTER

	manager = self->irreco_data->irreco_backend_manager;
	if (irreco_show_backend_select_dlg(self->irreco_data,
					   GTK_WINDOW(self),
					   &lib)) {
		instance = irreco_backend_manager_create_instance(
			manager, lib, NULL, NULL);
		if (instance != NULL) {
			irreco_backend_instance_configure(instance, GTK_WINDOW(
							  self));
			irreco_backend_instance_save_to_conf(instance);
			irreco_backend_instance_get_devcmd_list(instance);
			irreco_backend_dlg_populate_listbox(self);
			irreco_config_save_backends(manager);
		}
	}

	IRRECO_RETURN
}

static void irreco_backend_dlg_response_delete(IrrecoBackendDlg *self)
{
	IrrecoBackendManager *manager;
	IrrecoBackendInstance *instance;
	IRRECO_ENTER

	manager = self->irreco_data->irreco_backend_manager;
	if (irreco_backend_dlg_get_instance(self, &instance)) {

		GString *message = g_string_new(NULL);
		g_string_append_printf(
			message, _("Delete %s?\n\n%i commands in command chains"
			" depend on this backend. Are you sure you want to "
			"delete this backend."),
			irreco_backend_instance_get_name_and_description(
			instance), instance->irreco_cmd_dependencies->len);
		if (!irreco_yes_no_dlg(GTK_WINDOW(self),
				       message->str)) {
			g_string_free(message, TRUE);
			IRRECO_RETURN
		}
		g_string_free(message, TRUE);

		if (!irreco_backend_manager_destroy_instance(manager,
							     instance)) {
			irreco_error_dlg(GTK_WINDOW(self),
					 _("Delete failed."));
		} else {
			irreco_config_save_backends(manager);
			irreco_backend_dlg_populate_listbox(self);
		}
	}

	IRRECO_RETURN
}

static void irreco_backend_dlg_response_configure(IrrecoBackendDlg *self)
{
	IrrecoBackendManager *manager;
	IrrecoBackendInstance *instance;
	IRRECO_ENTER

	manager = self->irreco_data->irreco_backend_manager;
	if (irreco_backend_dlg_get_instance(self, &instance)) {
		irreco_backend_instance_configure(instance, GTK_WINDOW(
						  self));
		irreco_backend_instance_save_to_conf(instance);
		irreco_backend_instance_get_devcmd_list(instance);
		irreco_backend_dlg_populate_listbox(self);
		irreco_config_save_backends(manager);
	}

	IRRECO_RETURN
}

/** @} */
/** @} */


/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * irreco - Ir Remote Control
 * Copyright (C) 2008 Pekka Gehör (pegu6@msn.com)
 *
 * irreco_button_creator_dlg.c is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * irreco_button_creator_dlg.c is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "irreco_button_creator_dlg.h"
#include "irreco_theme_creator_backgrounds.h"
#include "irreco_theme_creator_dlg.h"
#include <hildon/hildon-banner.h>
#include <hildon/hildon-color-button.h>
#include <hildon/hildon-file-chooser-dialog.h>
#include <hildon/hildon-font-selection-dialog.h>
#include <hildon/hildon-program.h>
#include <hildon/hildon-color-chooser.h>
#include <hildon/hildon-color-chooser-dialog.h>




/**
 * @addtogroup IrrecoButtonCreatorDlg
 * @ingroup Irreco
 *
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoButtonCreatorDlg.
 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

#define IRRECO_BUTTON_PREVIEW_WIDHT	(IRRECO_SCREEN_WIDTH/5)
#define IRRECO_BUTTON_PREVIEW_HEIGHT (IRRECO_SCREEN_HEIGHT/5)
/*#define BUTTON_LIST_DIR "/usr/lib/irreco/buttonnames/button_list.conf"*/
#define BUTTON_LIST_DIR (BUTTONLIST"button_list.conf")
/** Loader states. */
enum
{
	LOADER_STATE_INIT,
	LOADER_STATE_THEME,
	LOADER_STATE_BUTTONS,
	LOADER_STATE_BACKGROUNDS,
	LOADER_STATE_END
};


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
static void
_signal_image_clicked(GtkButton *button, IrrecoButtonCreatorDlg *self);
static void
_unpressed_event_signal_image_clicked(GtkWidget *widget, GdkEventButton *event,
				      IrrecoButtonCreatorDlg *self);
static void
_pressed_event_signal_image_clicked(GtkWidget *widget, GdkEventButton *event,
				    IrrecoButtonCreatorDlg *self);
static gboolean _draw_preview_image(GtkButton *button,
				    IrrecoButtonCreatorDlg *self,
				    const gchar *image);
static void _toggle_button_toggled(GtkToggleButton *togglebutton,
				   IrrecoButtonCreatorDlg *self);
static void fill_buttons_combobox(IrrecoButtonCreatorDlg *self);
static void
_select_font_format(GtkButton *button, IrrecoButtonCreatorDlg *self);

static void
_set_font_format(IrrecoButtonCreatorDlg *self, const gchar *button_format,
			     const gchar *text);
void
_set_button_details(IrrecoButtonCreatorDlg *self, IrrecoTheme *irreco_theme,
		IrrecoThemeButton *button);
void
_unbutton_size_changed(GtkSpinButton *spinbutton, IrrecoButtonCreatorDlg*self);
void
_button_size_changed(GtkSpinButton *spinbutton, IrrecoButtonCreatorDlg*self);
void
_set_new_button_details(IrrecoButtonCreatorDlg *self, IrrecoThemeButton *button);
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

G_DEFINE_TYPE (IrrecoButtonCreatorDlg, irreco_button_creator_dlg,
			   IRRECO_TYPE_INTERNAL_DLG)
static void irreco_button_creator_dlg_constructed(GObject *object)
{
	/* TODO: Add initialization code here */

	IrrecoButtonCreatorDlg *self;
	GtkWidget		*scrolled_table;
	GtkWidget		*table;
	GtkWidget		*frame_unpressed;
	GtkWidget		*frame_pressed;
	GtkWidget		*label_name;
	GtkWidget		*label_select_unpressed;
	GtkWidget		*label_select_pressed;
	GtkWidget		*event_box_unpressed;
	GtkWidget		*vbox_unpressed_preview;
	GtkWidget		*vbox_pressed_preview;
	GtkWidget		*label_size;

	/* Settings */
	GtkWidget		*table_settings;
	GtkWidget		*label_allow_text;
	GtkWidget		*scrolled_table_settings;
	GtkWidget		*label_text_padding;
	GtkWidget		*label_text_h_align;
	GtkWidget		*label_text_v_align;
	GtkWidget		*label_text_format_up;
	GtkWidget		*label_text_format_down;
	IRRECO_ENTER

	G_OBJECT_CLASS(irreco_button_creator_dlg_parent_class)->constructed(object);

	self = IRRECO_BUTTON_CREATOR_DLG(object);
	/* Construct dialog. */
	gtk_window_set_title(GTK_WINDOW(self), _("Create a Button"));
	gtk_window_set_modal(GTK_WINDOW(self), TRUE);
	gtk_window_set_destroy_with_parent(GTK_WINDOW(self), TRUE);
	gtk_dialog_set_has_separator(GTK_DIALOG(self), FALSE);

	/*Buttons*/
	self->cancel_button= gtk_dialog_add_button(GTK_DIALOG(self),
						   _("Cancel"),
						   GTK_RESPONSE_CANCEL);
	self->add_button= gtk_dialog_add_button(GTK_DIALOG(self), _("Add"),
						GTK_RESPONSE_OK);
	gtk_widget_set_sensitive(self->add_button, FALSE);

	/* Create widgets. */
	self->notebook = gtk_notebook_new();

	/* Create widgets. */
	scrolled_table = gtk_scrolled_window_new(NULL, NULL);
	table = gtk_table_new(7, 9, TRUE);
	label_name = gtk_label_new(_("Name:"));
	label_select_unpressed = gtk_label_new(_("Unpressed:"));
	label_select_pressed = gtk_label_new(_("Pressed:"));
	self->event_box_pressed = gtk_event_box_new();
	event_box_unpressed = gtk_event_box_new();
	self->combobox_name = gtk_combo_box_entry_new_text();
	frame_unpressed = gtk_frame_new("");
	frame_pressed = gtk_frame_new("");
	vbox_unpressed_preview = gtk_vbox_new(FALSE, 8);
	vbox_pressed_preview = gtk_vbox_new(FALSE, 8);
	self->label_unpressed_size = gtk_label_new("");
	self->label_pressed_size = gtk_label_new("");
	self->preview_image_unpressed = gtk_image_new();
	self->preview_image_pressed = gtk_image_new();
	self->add_button_unpressed = gtk_button_new_with_label(_("Select"));
	self->add_button_pressed = gtk_button_new_with_label(_("Select"));

	/* Create settings widgets*/
	scrolled_table_settings = gtk_scrolled_window_new(NULL, NULL);
	table_settings = gtk_table_new(7, 9, TRUE);
	label_allow_text = gtk_label_new(_("AllowText:"));
	label_text_format_up = gtk_label_new(_("TextFormatUp:"));
	label_text_format_down = gtk_label_new(_("TextFormatDown:"));
	self->label_sample_text_format_up = gtk_label_new(_("Sample1"));
	self->label_sample_text_format_down = gtk_label_new(_("Sample2"));
	label_text_padding = gtk_label_new(_("TextPadding:"));
	label_text_h_align = gtk_label_new(_("TextHAlign:"));
	label_text_v_align = gtk_label_new(_("TextVAlign:"));
	label_size = gtk_label_new(_("ButtonSize:"));
	self->allow_text = gtk_toggle_button_new();
	self->text_format_up = gtk_button_new_with_label(_("Select"));
	self->text_format_down = gtk_button_new_with_label(_("Select"));
	self->text_padding = gtk_spin_button_new_with_range(0, 20, 1);
	self->text_h_align = gtk_spin_button_new_with_range(0, 1, 0.1);;
	self->text_v_align = gtk_spin_button_new_with_range(0, 1, 0.1);;
	self->unbutton_size = gtk_spin_button_new_with_range(2, 5, 0.1);
	self->button_size = gtk_spin_button_new_with_range(2, 5, 0.1);
	self->unpressed_path = g_string_new(NULL);
	self->pressed_path = g_string_new(NULL);

	/* set button name/sensitive */
	gtk_widget_set_name(self->add_button_unpressed,"unpressed");
	gtk_widget_set_name(self->add_button_pressed,"pressed");

	gtk_widget_set_sensitive(self->add_button_pressed, FALSE);
	gtk_widget_set_sensitive(self->event_box_pressed, FALSE);
	gtk_widget_set_sensitive(self->text_format_up, FALSE);
	gtk_widget_set_sensitive(self->text_format_down, FALSE);

	gtk_widget_set_sensitive(self->label_sample_text_format_up, FALSE);
	gtk_widget_set_sensitive(self->label_sample_text_format_down, FALSE);
	/* Set frame text bold */
	gtk_frame_set_label_widget(GTK_FRAME(frame_unpressed),
				   irreco_gtk_label_bold(
				   "Unpressed", 0, 0, 0, 0, 0, 0));
	gtk_frame_set_label_widget(GTK_FRAME(frame_pressed),
				   irreco_gtk_label_bold(
				   "Pressed", 0, 0, 0, 0, 0, 0));

	/* equal to the text of the left-side */
	gtk_misc_set_alignment(GTK_MISC(label_name), 0, 0.5);
	gtk_misc_set_alignment(GTK_MISC(label_select_unpressed), 0, 0.5);
	gtk_misc_set_alignment(GTK_MISC(label_select_pressed), 0, 0.5);

	gtk_misc_set_alignment(GTK_MISC(label_allow_text), 0, 0.5);
	gtk_misc_set_alignment(GTK_MISC(label_text_format_up), 0, 0.5);
	gtk_misc_set_alignment(GTK_MISC(label_text_format_down), 0, 0.5);
	gtk_misc_set_alignment(GTK_MISC(self->label_sample_text_format_up), 0, 0.5);
	gtk_misc_set_alignment(GTK_MISC(self->label_sample_text_format_down), 0, 0.5);
	gtk_misc_set_alignment(GTK_MISC(label_text_padding), 0, 0.5);
	gtk_misc_set_alignment(GTK_MISC(label_text_h_align), 0, 0.5);
	gtk_misc_set_alignment(GTK_MISC(label_text_v_align), 0, 0.5);
	gtk_misc_set_alignment(GTK_MISC(label_size), 0, 0.5);

	/* Set table on the scrolled */
	gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(
					      scrolled_table),
	   				      GTK_WIDGET(table));

	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolled_table),
				       GTK_POLICY_NEVER,
	   			       GTK_POLICY_AUTOMATIC);

	gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(
					      scrolled_table_settings),
	   				      GTK_WIDGET(table_settings));

	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(
				       scrolled_table_settings),
				       GTK_POLICY_NEVER,
	   			       GTK_POLICY_AUTOMATIC);

	/* Create Notebook tabs. */

	gtk_notebook_append_page(GTK_NOTEBOOK(self->notebook),
				 irreco_gtk_align(GTK_WIDGET(scrolled_table),
				 0, 0, 1, 1, 8, 8, 8, 8),
     				 gtk_label_new("Button"));
	gtk_notebook_append_page(GTK_NOTEBOOK(self->notebook),
				 irreco_gtk_align(GTK_WIDGET(
				 scrolled_table_settings),
				 0, 0, 1, 1, 8, 8, 8, 8),
     				 gtk_label_new("Settings"));

	gtk_box_pack_start_defaults(GTK_BOX(GTK_DIALOG(self)->vbox),
				    self->notebook);

	/* Set widgets on the table */

	gtk_table_set_row_spacings(GTK_TABLE(table), 6);
	gtk_table_set_col_spacings(GTK_TABLE(table), 6);

	gtk_table_attach_defaults(GTK_TABLE(table), label_name, 0, 3, 0, 1);
	gtk_table_attach_defaults(GTK_TABLE(table),
				  label_select_unpressed, 0, 3, 1, 2);
	gtk_table_attach_defaults(GTK_TABLE(table),
				  label_select_pressed, 0, 3, 2, 3);
	gtk_table_attach_defaults(GTK_TABLE(table),
				  self->combobox_name, 3, 9, 0, 1);
	gtk_table_attach_defaults(GTK_TABLE(table),
				  self->add_button_unpressed, 3, 9, 1, 2);
	gtk_table_attach_defaults(GTK_TABLE(table),
				  self->add_button_pressed, 3, 9, 2, 3);
	gtk_table_attach_defaults(GTK_TABLE(table),
				  frame_unpressed, 0, 4, 3, 7);
	gtk_table_attach_defaults(GTK_TABLE(table), frame_pressed, 5, 9, 3, 7);
	/* Set widgets on the table_settings */
	gtk_table_set_row_spacings(GTK_TABLE(table_settings), 6);
	gtk_table_set_col_spacings(GTK_TABLE(table_settings), 6);

	gtk_table_attach_defaults(GTK_TABLE(table_settings),
				  label_allow_text, 0, 4, 0, 1);
	gtk_table_attach_defaults(GTK_TABLE(table_settings),
				  label_text_format_up, 0, 4, 1, 2);
	gtk_table_attach_defaults(GTK_TABLE(table_settings),
				  label_text_format_down, 0, 4, 2, 3);
	gtk_table_attach_defaults(GTK_TABLE(table_settings),
				  self->label_sample_text_format_up, 4, 7, 1, 2);
	gtk_table_attach_defaults(GTK_TABLE(table_settings),
				  self->label_sample_text_format_down, 4, 7, 2, 3);
	gtk_table_attach_defaults(GTK_TABLE(table_settings),
				  label_text_padding, 0, 4, 3, 4);
	gtk_table_attach_defaults(GTK_TABLE(table_settings),
				  label_text_h_align, 0, 4, 4, 5);
	gtk_table_attach_defaults(GTK_TABLE(table_settings),
				  label_text_v_align, 0, 4, 5, 6);
	gtk_table_attach_defaults(GTK_TABLE(table_settings),
				  label_size, 0, 4, 6, 7);

	gtk_table_attach_defaults(GTK_TABLE(table_settings),
				  self->allow_text, 4, 9, 0, 1);
	gtk_table_attach_defaults(GTK_TABLE(table_settings),
				  self->text_format_up, 7, 9, 1, 2);
	gtk_table_attach_defaults(GTK_TABLE(table_settings),
				  self->text_format_down, 7, 9, 2, 3);
	gtk_table_attach_defaults(GTK_TABLE(table_settings),
				  self->text_padding, 4, 9, 3, 4);
	gtk_table_attach_defaults(GTK_TABLE(table_settings),
				  self->text_h_align, 4, 9, 4, 5);
	gtk_table_attach_defaults(GTK_TABLE(table_settings),
				  self->text_v_align, 4, 9, 5, 6);
	gtk_table_attach_defaults(GTK_TABLE(table_settings),
				  self->unbutton_size, 4, 6, 6, 7);
	gtk_table_attach_defaults(GTK_TABLE(table_settings),
				  self->button_size, 7, 9, 6, 7);
	/* set preview image*/

	gtk_container_add(GTK_CONTAINER(frame_unpressed), event_box_unpressed);
	gtk_container_add(GTK_CONTAINER(event_box_unpressed),
			  		vbox_unpressed_preview);
	gtk_container_add(GTK_CONTAINER(vbox_unpressed_preview),
			  		self->preview_image_unpressed);
	gtk_container_add(GTK_CONTAINER(vbox_unpressed_preview),
			  		self->label_unpressed_size);

	gtk_container_add(GTK_CONTAINER(frame_pressed), self->event_box_pressed);
	gtk_container_add(GTK_CONTAINER(self->event_box_pressed),
			  		vbox_pressed_preview);
	gtk_container_add(GTK_CONTAINER(vbox_pressed_preview),
			  		self->preview_image_pressed);
	gtk_container_add(GTK_CONTAINER(vbox_pressed_preview),
			  		self->label_pressed_size);




	/*Added button list on combobox*/
	fill_buttons_combobox(self);

	/* set default settings on tab */
	gtk_button_set_label(GTK_BUTTON(self->allow_text), "NO");

	gtk_spin_button_set_value(GTK_SPIN_BUTTON(self->text_padding), 5);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(self->text_h_align), 0.5);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(self->text_v_align), 0.5);
  	gtk_spin_button_set_value(GTK_SPIN_BUTTON(self->unbutton_size), 10);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(self->button_size), 10);

	/* Set button name */
	gtk_widget_set_name(self->text_format_up, "unpressed");
	gtk_widget_set_name(self->text_format_down, "pressed");
	/* Button signals. */
	g_signal_connect(G_OBJECT(self->add_button_unpressed), "clicked",
			 G_CALLBACK(_signal_image_clicked), self);
	g_signal_connect(G_OBJECT(self->add_button_pressed), "clicked",
			 G_CALLBACK(_signal_image_clicked), self);

	g_signal_connect(G_OBJECT(event_box_unpressed), "button-release-event",
			 G_CALLBACK(_unpressed_event_signal_image_clicked),
				    self);
	g_signal_connect(G_OBJECT(self->event_box_pressed),
			 "button-release-event",
    			 G_CALLBACK(_pressed_event_signal_image_clicked),
				    self);

	g_signal_connect(G_OBJECT(self->allow_text), "toggled",
			 G_CALLBACK(_toggle_button_toggled), self);

	g_signal_connect(G_OBJECT(self->text_format_up), "clicked",
			 G_CALLBACK(_select_font_format), self);
	g_signal_connect(G_OBJECT(self->text_format_down), "clicked",
			 G_CALLBACK(_select_font_format), self);
	g_signal_connect(G_OBJECT(self->unbutton_size), "value-changed",
			 G_CALLBACK(_unbutton_size_changed), self);
	g_signal_connect(G_OBJECT(self->button_size), "value-changed",
			 G_CALLBACK(_button_size_changed), self);

	gtk_widget_set_size_request(GTK_WIDGET(self), 696, 396);
	gtk_widget_show_all(GTK_WIDGET(self));

	/* Hide Button size widgets */
	gtk_widget_hide(label_size);
	gtk_widget_hide(self->unbutton_size);
	gtk_widget_hide(self->button_size);
	IRRECO_RETURN
}

static void
irreco_button_creator_dlg_init (IrrecoButtonCreatorDlg *object)
{
	IRRECO_ENTER
	IRRECO_RETURN
}

static void
irreco_button_creator_dlg_finalize (GObject *object)
{
	/* TODO: Add deinitalization code here */

	IrrecoButtonCreatorDlg *self;
	IRRECO_ENTER

	self = IRRECO_BUTTON_CREATOR_DLG(object);

	G_OBJECT_CLASS(irreco_button_creator_dlg_parent_class)->finalize(object);
	IRRECO_RETURN

}

static void
irreco_button_creator_dlg_class_init (IrrecoButtonCreatorDlgClass *klass)
{
	GObjectClass* object_class = G_OBJECT_CLASS (klass);

	object_class->finalize = irreco_button_creator_dlg_finalize;
	object_class->constructed = irreco_button_creator_dlg_constructed;
}

GtkWidget
*irreco_button_creator_dlg_new(IrrecoData *irreco_data, GtkWindow *parent)
{
	IrrecoButtonCreatorDlg *self;

	IRRECO_ENTER

	self = g_object_new(IRRECO_TYPE_BUTTON_CREATOR_DLG,
			    "irreco-data", irreco_data, NULL);
	irreco_dlg_set_parent(IRRECO_DLG(self), parent);

	IRRECO_RETURN_PTR(self);
}

/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/**
 * @name Private Functions
 * @{
 */

/**
 *Fill combobox from file
 */
static void fill_buttons_combobox(IrrecoButtonCreatorDlg *self)
{

	GKeyFile		*button_list;
	gchar 			**keys;
	gsize 			length;
	gint			i=0;
	GError			*error = NULL;

	IRRECO_ENTER

	if (irreco_file_exists(BUTTON_LIST_DIR)) {
		button_list = g_key_file_new();
		g_key_file_load_from_file(button_list, BUTTON_LIST_DIR,
					G_KEY_FILE_NONE, &error);
		keys = g_key_file_get_keys(button_list, "button-names", &length,
					&error);
		for(i=0; i<length; i++){
			gtk_combo_box_append_text(GTK_COMBO_BOX(self->combobox_name),
						g_key_file_get_string(button_list,
						"button-names", keys[i], &error));
		}
		g_key_file_free(button_list);
	}
	if(keys != NULL) g_free(keys);


	IRRECO_RETURN
}

void
_unbutton_size_changed(GtkSpinButton *spinbutton, IrrecoButtonCreatorDlg*self)
{
	GString	*image_path = g_string_new(self->unpressed_path->str);

	IRRECO_ENTER

	_draw_preview_image(GTK_BUTTON(self->text_format_up), self,
			    image_path->str);

	g_string_free(image_path, TRUE);
	IRRECO_RETURN
}
void
_button_size_changed(GtkSpinButton *spinbutton, IrrecoButtonCreatorDlg*self)
{
	GString	*image_path = g_string_new(self->unpressed_path->str);

	IRRECO_ENTER

	_draw_preview_image(GTK_BUTTON(self->text_format_down), self,
			    image_path->str);

	g_string_free(image_path, TRUE);
	IRRECO_RETURN
}

/**
 * Draw preview with current image.
 */
static gboolean
_draw_preview_image(GtkButton *button, IrrecoButtonCreatorDlg *self,
		    const gchar *image)
{
	GError			*error = NULL;
	GdkPixbuf 		*pixbuf = NULL;
	GString			*size = g_string_new(NULL);

	IRRECO_ENTER

	g_assert(self != NULL);
	/* compared with whichever image */
	if (g_str_equal("pressed", gtk_widget_get_name(GTK_WIDGET(button)))) {
		gint button_width = 0;
		gint button_height = 0;
		GdkPixbuf 	*pixbuf_size = NULL;

		if (image != NULL) {
			pixbuf = gdk_pixbuf_new_from_file_at_scale(image,
						IRRECO_BUTTON_PREVIEW_WIDHT,
      						IRRECO_BUTTON_PREVIEW_HEIGHT,
	    					TRUE, &error);
		/* set sensitive if image are selected */
			gtk_widget_set_sensitive(self->add_button, TRUE);

			if (irreco_gerror_check_print(&error)) {
				IRRECO_RETURN_BOOL(FALSE);
			}
		}

		g_string_printf(self->pressed_path, "%s", image);
		gtk_widget_realize(GTK_WIDGET(self->preview_image_pressed));
		gtk_image_set_from_pixbuf(GTK_IMAGE(self->preview_image_pressed),
					  GDK_PIXBUF(pixbuf));

		/* Show image real size */
		pixbuf_size =  gdk_pixbuf_new_from_file(image, &error);
		button_width = atoi(g_strdup_printf("%.0f",
				    (gdk_pixbuf_get_width(
		      		    pixbuf_size))/
				    (6-gtk_spin_button_get_value(GTK_SPIN_BUTTON(
				    self->button_size)))));
		button_height = atoi(g_strdup_printf("%.0f",
				     (gdk_pixbuf_get_height(
		      		     pixbuf_size)/
				     (6-gtk_spin_button_get_value(GTK_SPIN_BUTTON(
				     self->button_size))))));

		if (pixbuf_size != NULL) g_object_unref(G_OBJECT(pixbuf_size));

		g_string_printf(size, "%sx%s", g_strdup_printf("%d", button_width),
					g_strdup_printf("%d", button_height));

		gtk_label_set_text(GTK_LABEL(self->label_pressed_size),
				   size->str);

	} else {
		gint button_width = 0;
		gint button_height = 0;
		GdkPixbuf 	*pixbuf_size = NULL;

		if (image != NULL) {
			pixbuf = gdk_pixbuf_new_from_file_at_scale(image,
						IRRECO_BUTTON_PREVIEW_WIDHT,
      						IRRECO_BUTTON_PREVIEW_HEIGHT,
	    					TRUE, &error);
			if (irreco_gerror_check_print(&error)) {
				IRRECO_RETURN_BOOL(FALSE);
			}
		}
		gtk_widget_realize(GTK_WIDGET(self->preview_image_unpressed));
		gtk_image_set_from_pixbuf(GTK_IMAGE(
					  self->preview_image_unpressed),
       					  GDK_PIXBUF(pixbuf));

		/* Show image real size */
		pixbuf_size =  gdk_pixbuf_new_from_file(image, &error);
		button_width = atoi(g_strdup_printf("%.0f",
				    (gdk_pixbuf_get_width(
		      		    pixbuf_size))/
				    (6-gtk_spin_button_get_value(GTK_SPIN_BUTTON(
				    self->unbutton_size)))));
		button_height = atoi(g_strdup_printf("%.0f",
				     (gdk_pixbuf_get_height(
		      		     pixbuf_size)/
				     (6-gtk_spin_button_get_value(GTK_SPIN_BUTTON(
				     self->unbutton_size))))));
		if (pixbuf_size != NULL) g_object_unref(G_OBJECT(pixbuf_size));

		g_string_printf(size, "%sx%s", g_strdup_printf("%d", button_width),
					g_strdup_printf("%d", button_height));

		gtk_label_set_text(GTK_LABEL(self->label_unpressed_size),
				   size->str);
		g_string_printf(self->unpressed_path, "%s", image);

		/* Set button sensitive */
		gtk_widget_set_sensitive(self->add_button_pressed, TRUE);
		gtk_widget_set_sensitive(self->event_box_pressed, TRUE);
	}

	g_string_free(size, TRUE);
	if (pixbuf != NULL) g_object_unref(G_OBJECT(pixbuf));
	IRRECO_RETURN_BOOL(TRUE);
}

static void _select_image(GtkButton *button, IrrecoButtonCreatorDlg *self)
{

	GtkWidget		*file_dlg = NULL;
	gchar			*image_dir = NULL;
	IRRECO_ENTER

	/* Create image select dialog. */

	file_dlg = hildon_file_chooser_dialog_new(GTK_WINDOW(self),
						  GTK_FILE_CHOOSER_ACTION_OPEN);
	gtk_window_set_title(GTK_WINDOW(file_dlg),_("Select button image"));
	gtk_file_chooser_set_local_only(GTK_FILE_CHOOSER(file_dlg), TRUE);
	image_dir = g_build_path("/", getenv("HOME"), "MyDocs/.images/", NULL);
	gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(file_dlg),
					    image_dir);
	g_free(image_dir);

	/* Loop until user cancels or we get a valid image. */
	gtk_widget_show_all(GTK_WIDGET(file_dlg));
	while (gtk_dialog_run(GTK_DIALOG(file_dlg)) == GTK_RESPONSE_OK) {
		gchar		*filename;
		filename = gtk_file_chooser_get_filename(
						GTK_FILE_CHOOSER(file_dlg));

		/* Attempt to display the image. */
		if (_draw_preview_image(button, self, filename)) {
			irreco_gstring_set_and_free(self->filename, filename);
			filename = NULL;
			break;

		/* Cleanup */
		} else {
			gchar *basename = g_path_get_basename(filename);
			irreco_error_dlg_printf(GTK_WINDOW(file_dlg),
						_("Cannot open image \"%s\""),
						basename);
			g_free(basename);
			g_free(filename);
		}
	}

	gtk_widget_destroy(file_dlg);
	IRRECO_RETURN
}
gboolean irreco_button_creator_dlg_check_details(IrrecoButtonCreatorDlg	*self)
{
	gboolean	rvalue = TRUE;
	gchar		*name = NULL;
	gchar		*style_name = NULL;
	IRRECO_ENTER

	name = gtk_combo_box_get_active_text(GTK_COMBO_BOX(
					     self->combobox_name));
	style_name = g_strconcat(self->theme->name->str ,"/", name, NULL);
	/*check that it is not the same name button*/
	if (irreco_string_table_exists (self->theme->buttons, name)) {
		irreco_error_dlg(GTK_WINDOW(self),
				 "Button has already existed");
		rvalue = FALSE;
	} else if (irreco_string_table_exists(self->theme->buttons,
		   style_name)) {
		irreco_error_dlg(GTK_WINDOW(self),
				 "Button has already existed");
		rvalue = FALSE;
	}
	if (g_utf8_strlen(name, 1) ==0) {
		irreco_error_dlg(GTK_WINDOW(self),
				 "Set Button name");
		rvalue = FALSE;
	}

	if (name != NULL) g_free(name);
	if (style_name != NULL) g_free(style_name);
	IRRECO_RETURN_BOOL(rvalue);
}
/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Functions                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Public Functions                                                           */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

gboolean irreco_button_creator_dlg_run(IrrecoData *irreco_data,
					IrrecoTheme * irreco_theme,
					GtkWindow *parent_window,
					IrrecoThemeButton *button)
{
	IrrecoButtonCreatorDlg	*self;
	gint			response;
	gboolean		loop = TRUE;
	gboolean		rvalue = FALSE;
	gboolean		edit = FALSE;
	IRRECO_ENTER

	self = (IrrecoButtonCreatorDlg*)irreco_button_creator_dlg_new(
		irreco_data, parent_window);
	self->irreco_data = irreco_data;
	self->theme = irreco_theme;

	IRRECO_DEBUG("Button: %s\n", button->name->str);
	if (g_utf8_strlen(button->name->str, 1) >0) {
		/* Sets the button details */
		_set_button_details(self, irreco_theme, button);
		edit = TRUE;

	}

	do {
		response = gtk_dialog_run(GTK_DIALOG(self));
		switch (response) {
		case GTK_RESPONSE_OK:

			if (edit) {
				_set_new_button_details(self, button);
				irreco_theme_button_print(button);
				rvalue = TRUE;
				loop = FALSE;


			} else {
				/* Check button name */
				if (irreco_button_creator_dlg_check_details(self)) {
					_set_new_button_details(self, button);
					irreco_theme_button_print(button);
					rvalue = TRUE;
					loop = FALSE;
				} else {
					rvalue = FALSE;
					loop = TRUE;
				}

			}
			break;

		case GTK_RESPONSE_CANCEL:
			IRRECO_DEBUG("GTK_RESPONSE_CANCEL\n");
			rvalue = FALSE;
			loop = FALSE;
			break;

		default:
			IRRECO_DEBUG("default\n");
			break;
		}

	} while (loop);

	gtk_widget_destroy(GTK_WIDGET(self));
	IRRECO_RETURN_BOOL(rvalue);

}

void
_set_button_details(IrrecoButtonCreatorDlg *self, IrrecoTheme *irreco_theme,
		IrrecoThemeButton *button)
{
	IRRECO_ENTER

		/* Set button information on table */
		gtk_combo_box_prepend_text(GTK_COMBO_BOX(self->combobox_name),
                                                        button->name->str);
		gtk_combo_box_set_active(GTK_COMBO_BOX(self->combobox_name),
					 0);
		_draw_preview_image(GTK_BUTTON(self->add_button_unpressed),
				    self,
				    button->image_up->str);
		_draw_preview_image(GTK_BUTTON(self->add_button_pressed),
				    self,
				    button->image_down->str);
		if (button->allow_text) {
			gtk_button_clicked(GTK_BUTTON(self->allow_text));
		}

		gtk_spin_button_set_value(GTK_SPIN_BUTTON(self->text_padding),
					 button->text_padding);

		gtk_spin_button_set_value(GTK_SPIN_BUTTON(self->text_h_align),
					 button->text_h_align);
		gtk_spin_button_set_value(GTK_SPIN_BUTTON(self->text_v_align),
					 button->text_v_align);
		/*set text format */
		if (strlen(button->text_format_up->str) > 0) {

			_set_font_format(self, button->text_format_up->str,
					 gtk_label_get_text(GTK_LABEL(
					 self->label_sample_text_format_up)));
		}
		if (strlen(button->text_format_down->str) > 0) {

			_set_font_format(self, button->text_format_down->str,
					 gtk_label_get_text(GTK_LABEL(
					 self->label_sample_text_format_down)));
		}
		/* Set button label & windown title */
		gtk_button_set_label(GTK_BUTTON(self->add_button), "Save");
		gtk_window_set_title(GTK_WINDOW(self), _("Edit a Button"));
		gtk_widget_set_sensitive(self->combobox_name, FALSE);


	IRRECO_RETURN
}

void
_set_new_button_details(IrrecoButtonCreatorDlg *self, IrrecoThemeButton *button)
{


	gchar		*style_name = NULL;
	gboolean 	allow_text = FALSE;
	gint		text_padding = 0;
	gfloat		text_h_align = 0;
	gfloat		text_v_align = 0;
	IRRECO_ENTER

	style_name = gtk_combo_box_get_active_text(GTK_COMBO_BOX(
						   self->combobox_name));
	allow_text = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(
						  self->allow_text));
	text_padding = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(
							self->text_padding));
	text_h_align = gtk_spin_button_get_value(GTK_SPIN_BUTTON(
						 self->text_h_align));
	text_v_align = gtk_spin_button_get_value(GTK_SPIN_BUTTON(
						 self->text_v_align));

	irreco_theme_button_set(button,
				style_name,
    				style_name,
				allow_text,
    				self->unpressed_path->str,
    				self->pressed_path->str,
				self->unpressed_format,
    				self->pressed_format,
    			 	text_padding,
			     	text_h_align,
			     	text_v_align);

	if (style_name != NULL) g_free(style_name);
	IRRECO_RETURN

}
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Events and Callbacks                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Events and Callbacks
 * @{
 */

static void
_signal_image_clicked(GtkButton *button, IrrecoButtonCreatorDlg *self)
{
	IRRECO_ENTER
	_select_image(button, self);
	IRRECO_RETURN
}

static void _unpressed_event_signal_image_clicked(GtkWidget *widget,
						  GdkEventButton *event,
						  IrrecoButtonCreatorDlg *self)
{
	IRRECO_ENTER

	gtk_button_clicked(GTK_BUTTON(self->add_button_unpressed));

	IRRECO_RETURN
}

static void _pressed_event_signal_image_clicked(GtkWidget *widget,
						GdkEventButton *event,
      						IrrecoButtonCreatorDlg *self)
{
	IRRECO_ENTER

	gtk_button_clicked(GTK_BUTTON(self->add_button_pressed));

	IRRECO_RETURN
}

static void _toggle_button_toggled(GtkToggleButton *togglebutton,
				   IrrecoButtonCreatorDlg *self)
{
	IRRECO_ENTER

	if (gtk_toggle_button_get_active(togglebutton)) {
		gtk_button_set_label(GTK_BUTTON(self->allow_text), "YES");
		gtk_widget_set_sensitive(self->text_format_up, TRUE);
		gtk_widget_set_sensitive(self->text_format_down, TRUE);
	} else {
		gtk_button_set_label(GTK_BUTTON(self->allow_text), "NO");
		gtk_widget_set_sensitive(self->text_format_up, FALSE);
		gtk_widget_set_sensitive(self->text_format_down, FALSE);
	}
	IRRECO_RETURN

}
/**
*Select button font format
*/

static void _select_font_format(GtkButton *button, IrrecoButtonCreatorDlg *self)
{
	GtkWidget			*dialog;
	gchar				*font;
	GString				*format  = g_string_new(NULL);
	gchar				*span = ">%s</span>";

	IRRECO_ENTER
	dialog = gtk_font_selection_dialog_new("Button font");

	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_OK) {

		font = gtk_font_selection_dialog_get_font_name(
				GTK_FONT_SELECTION_DIALOG(dialog));

		g_string_printf(format,
				"<span font_desc=\"%s\" %s", font, span);


		if (strcmp("unpressed", gtk_widget_get_name(
		    GTK_WIDGET(button))) == 0) {
			self->unpressed_format = format->str;
			_set_font_format(self, format->str,
					 gtk_label_get_text(GTK_LABEL(
					 self->label_sample_text_format_up)));
		} else {
			self->pressed_format = format->str;
			_set_font_format(self, format->str,
					 gtk_label_get_text(GTK_LABEL(
					 self->label_sample_text_format_down)));
		}
	}
	gtk_widget_destroy(GTK_WIDGET(dialog));

	IRRECO_RETURN
}

/**
*	Set button text format
*/
static void
_set_font_format(IrrecoButtonCreatorDlg *self, const gchar *button_format,
		 const gchar *text)
{


	gchar			*markup;
	GString			*format  = g_string_new(NULL);

	IRRECO_ENTER

	g_string_printf(format, "%s", button_format);

	markup = g_markup_printf_escaped(format->str, text);

	IRRECO_DEBUG("TEXT: %s\n", markup);

	if (strcmp(text,  gtk_label_get_text(GTK_LABEL(
	    self->label_sample_text_format_up))) == 0) {
		gtk_label_set_markup(GTK_LABEL(self->label_sample_text_format_up),
				     markup);
		gtk_widget_set_sensitive(self->label_sample_text_format_up, TRUE);
	} else {

		gtk_label_set_markup(GTK_LABEL(self->label_sample_text_format_down),
				     markup);
		gtk_widget_set_sensitive(self->label_sample_text_format_down, TRUE);
	}

	if(markup != NULL) g_free(markup);
	g_string_free(format, TRUE);

	IRRECO_RETURN
}
/** @} */

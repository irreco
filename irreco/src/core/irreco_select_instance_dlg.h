/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *	      &  2008  Joni Kokko     (t5kojo01@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

 /**
 * @addtogroup IrrecoSelectInstanceDlg
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoSelectInstanceDlg.
 */

#ifndef _IRRECO_SELECT_INSTANCE_DLG_H_TYPEDEF__
#define _IRRECO_SELECT_INSTANCE_DLG_H_TYPEDEF__

#define IRRECO_TYPE_SELECT_INSTANCE_DLG irreco_select_instance_dlg_get_type()

#define IRRECO_SELECT_INSTANCE_DLG(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  IRRECO_TYPE_SELECT_INSTANCE_DLG, \
  IrrecoSelectInstanceDlg))

#define IRRECO_SELECT_INSTANCE_DLG_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  IRRECO_TYPE_SELECT_INSTANCE_DLG, \
  IrrecoSelectInstanceDlgClass))

#define IRRECO_IS_SELECT_INSTANCE_DLG(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  IRRECO_TYPE_SELECT_INSTANCE_DLG))

#define IRRECO_IS_SELECT_INSTANCE_DLG_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  IRRECO_TYPE_SELECT_INSTANCE_DLG))

#define IRRECO_SELECT_INSTANCE_DLG_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  IRRECO_TYPE_SELECT_INSTANCE_DLG, \
  IrrecoSelectInstanceDlgClass))

typedef struct _IrrecoSelectInstanceDlg IrrecoSelectInstanceDlg;
typedef struct _IrrecoSelectInstanceDlgClass IrrecoSelectInstanceDlgClass;

#endif /* _IRRECO_SELECT_INSTANCE_DLG_H_TYPEDEF__ */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef _IRRECO_SELECT_INSTANCE_DLG_H__
#define _IRRECO_SELECT_INSTANCE_DLG_H__
#include "irreco.h"
#include "irreco_dlg.h"
#include "irreco_data.h"
#include "irreco_listbox_text.h"

#include <glib-object.h>


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

struct _IrrecoSelectInstanceDlg{
	IrrecoDlg parent;
	GtkWidget * vbox;
	IrrecoListbox *listbox;
};

struct _IrrecoSelectInstanceDlgClass{
	IrrecoDlgClass parent_class;
};

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

GType irreco_select_instance_dlg_get_type (void);

GtkWidget * irreco_select_instance_dlg_new (GtkWindow *parent);

gboolean irreco_show_select_instance_dlg(IrrecoData *irreco_data,
					 GtkWindow *parent,
      					 const char *lib_name,
					 IrrecoBackendInstance ** sel_instance);

#endif /* _IRRECO_SELECT_INSTANCE_DLG_H__ */

/** @} */

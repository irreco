/*
 * irreco - Ir Remote Control
 * Copyright (C) 2008 Pekka Gehör (pegu6@msn.com)
 * Joni Kokko (t5kojo01@students.oamk.fi),
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_theme_manager_dlg.h"
#include <hildon/hildon-banner.h>
#include "irreco_webdb_register_dlg.h"
#include "irreco_select_instance_dlg.h"
#include "irreco_config.h"
#include "irreco_theme_download_dlg.h"
#include <hildon/hildon-gtk.h>
#include <hildon/hildon-pannable-area.h>
/**
 * @addtogroup IrrecoThemeManagerDlg
 * @ingroup Irreco
 *
 * @todo PURPOCE OF CLASS.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoThemeManagerDlg.
 */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#define IRRECO_BACKGROUND_PREVIEW_WIDHT		(IRRECO_SCREEN_WIDTH / 6)
#define IRRECO_BACKGROUND_PREVIEW_HEIGHT	(IRRECO_SCREEN_HEIGHT / 6)
#define IRRECO_DEFAULT_BG  (IRRECO_BG_IMAGE_DIR "/" IRRECO_DEFAULT_BG_IMAGE)

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

static void irreco_theme_manager_dlg_hide_banner(IrrecoThemeManagerDlg *self);

static void irreco_theme_manager_dlg_loader_stop(IrrecoThemeManagerDlg *self);

static void irreco_theme_manager_dlg_clean_details(IrrecoThemeManagerDlg *self);

static gboolean
irreco_theme_manager_dlg_loader_deb(IrrecoThemeManagerDlg *self);

static gboolean
irreco_theme_manager_dlg_loader_user(IrrecoThemeManagerDlg *self);

static gboolean
irreco_theme_manager_dlg_loader_web(IrrecoThemeManagerDlg *self);

static gboolean
irreco_theme_manager_dlg_loader_db(IrrecoThemeManagerDlg *self);

static gboolean
irreco_theme_manager_dlg_theme_db_detail_loader(IrrecoThemeManagerDlg *self);

static gboolean
irreco_theme_manager_dlg_display_theme_detail(IrrecoThemeManagerDlg *self,
					      GtkTreeIter * iter);

static gboolean irreco_theme_manager_dlg_map_event(IrrecoThemeManagerDlg
						   *self, GdkEvent  *event,
						   gpointer   user_data);

static void irreco_theme_manager_dlg_destroy_event(IrrecoThemeManagerDlg
						   *self,
						   gpointer user_data);

static void irreco_theme_manager_dlg_row_activated_event(GtkTreeView *tree_view,
						GtkTreePath *path,
						GtkTreeViewColumn *column,
						IrrecoThemeManagerDlg*self);

static void irreco_theme_manager_dlg_row_expanded_event(GtkTreeView *tree_view,
						GtkTreeIter *iter,
						GtkTreePath *path,
						IrrecoThemeManagerDlg *self);

static void irreco_theme_manager_dlg_row_collapsed_event(GtkTreeView *tree_view,
						GtkTreeIter *iter,
						GtkTreePath *path,
						IrrecoThemeManagerDlg *self);


static void irreco_theme_manager_dlg_row_selected_event(GtkTreeSelection *sel,
						IrrecoThemeManagerDlg *self);

gboolean irreco_theme_manager_dlg_preview_dlg(GtkWidget *widget,
					  GdkEventButton *event,
					  IrrecoThemeManagerDlg *self);

static void irreco_theme_manager_dlg_delete_theme(GtkButton *button,
                                                  IrrecoThemeManagerDlg *self);

static void irreco_theme_manager_dlg_selected_version(GtkComboBox *widget,
						IrrecoThemeManagerDlg *self);

void irreco_theme_manager_update_theme_manager(IrrecoThemeManager *self);

static void irreco_theme_manager_dlg_new_theme(GtkButton *button,
					      	IrrecoThemeManagerDlg *self);

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/** Button responce codes. */
enum {
	IRRECO_DEVICE_REFRESH,
	IRRECO_DOWNLOAD_THEME,
	IRRECO_UPLOAD_THEME
};

/** GtkTreeStore colums. */
enum
{
	TEXT_COL,
	FLAG_COL,
	DATA_COL,
	N_COLUMNS
};

/** Version colums. */
enum
{
	DATE_COL,
	ID_COL
};

/** Loader states. */
enum
{
	LOADER_STATE_INIT,
	LOADER_STATE_LOOP,
	LOADER_STATE_END,
	LOADER_STATE_CLEANUP
};

/** Row flags. */
enum
{
	ROW_CHILDREN_LOADED	= 1 << 1,
	ROW_TYPE_CATEGORY	= 1 << 2,
	ROW_TYPE_DEB		= 1 << 3,
	ROW_TYPE_USER		= 1 << 4,
 	ROW_TYPE_WEB		= 1 << 5,
	ROW_TYPE_DB		= 1 << 6,
	ROW_TYPE_LOADING	= 1 << 7
};

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

G_DEFINE_TYPE(IrrecoThemeManagerDlg, irreco_theme_manager_dlg, IRRECO_TYPE_DLG)

static void irreco_theme_manager_dlg_finalize(GObject *object)
{
	if (G_OBJECT_CLASS(irreco_theme_manager_dlg_parent_class)->finalize)
		G_OBJECT_CLASS(irreco_theme_manager_dlg_parent_class)->finalize(
				object);
}

static void
irreco_theme_manager_dlg_class_init(IrrecoThemeManagerDlgClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS(klass);
	object_class->finalize = irreco_theme_manager_dlg_finalize;
}

static void irreco_theme_manager_dlg_init(IrrecoThemeManagerDlg *self)
{
	GtkTreeViewColumn 	*column;
	GtkCellRenderer 	*renderer;
	GtkTreeSelection 	*select;
	GtkWidget 		*label_name;
	GtkWidget 		*label_description;
	GtkWidget 		*theme_info_frame;
	GtkWidget 		*tree_view_frame;
	GtkWidget 		*tree_view_hbox;
	GtkWidget 		*preview_frame;
	PangoFontDescription 	*initial_font;
	GtkWidget		*select_vbox;
	GtkWidget		*versions_hbox;
	GtkWidget		*comment_alignment;
	GtkWidget		*theme_info;
	GtkWidget		*table;
	GtkWidget		*info_vbox;
	GtkWidget		*theme_table;
	GtkWidget		*preview_event_box;
	GtkWidget		*tree_view_pannable;

	IRRECO_ENTER

	self->theme_folder = NULL;

	/* Construct dialog. */
	gtk_window_set_title(GTK_WINDOW(self),
			     _("Theme Manager "));
	gtk_window_set_modal(GTK_WINDOW(self), TRUE);
	gtk_window_set_destroy_with_parent(GTK_WINDOW(self), TRUE);
	gtk_dialog_set_has_separator(GTK_DIALOG(self), FALSE);

	/*Buttons*/
	self->refresh_button= gtk_dialog_add_button (GTK_DIALOG(self),
                                       _("Refresh"),
                                       IRRECO_DEVICE_REFRESH);
	/*self->new_button= gtk_dialog_add_button (GTK_DIALOG(self),
                                       _("New"), GTK_RESPONSE_NONE);*/
	self->edit_button= gtk_dialog_add_button (GTK_DIALOG(self),
                                       _("Edit"), GTK_RESPONSE_NONE);
	self->upload_button= gtk_dialog_add_button (GTK_DIALOG(self),
                                       _("Upload"), IRRECO_UPLOAD_THEME);
	self->download_button= gtk_dialog_add_button (GTK_DIALOG(self),
                                     _("Download"), IRRECO_DOWNLOAD_THEME);
	self->clear_button= gtk_dialog_add_button (GTK_DIALOG(self),
                                       _("Delete"),
                                       GTK_RESPONSE_NONE);

	/* Create theme table */
	table = gtk_table_new(7,7,TRUE);
	gtk_table_set_col_spacings(GTK_TABLE (table), 8);
	gtk_table_set_row_spacings(GTK_TABLE (table), 8);
	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(self)->vbox),
			  irreco_gtk_align(GTK_WIDGET(table),
					   0, 0, 1, 1, 8, 8, 8, 8));
	/* Create tree_view_hbox */
	tree_view_hbox = gtk_hbox_new(FALSE, 2);

	/* Create Treeview */
	self->tree_store = gtk_tree_store_new(
		N_COLUMNS, G_TYPE_STRING, G_TYPE_INT, G_TYPE_POINTER);
	self->tree_view = GTK_TREE_VIEW(hildon_gtk_tree_view_new_with_model(1,
		GTK_TREE_MODEL(self->tree_store)));

	renderer = gtk_cell_renderer_text_new();
	column = gtk_tree_view_column_new_with_attributes(
		NULL, renderer, "text", TEXT_COL, NULL);
	gtk_tree_view_append_column(self->tree_view, column);

	gtk_box_pack_start(GTK_BOX(tree_view_hbox), GTK_WIDGET(self->tree_view),
			   TRUE, TRUE, 0);

	/* Create Frame for Treeview */
	tree_view_frame = gtk_frame_new("");
	gtk_frame_set_label_widget(GTK_FRAME(tree_view_frame),
		irreco_gtk_label_bold("Themes", 0, 0, 0, 0, 0, 0));

	/* Create theme_info */

	theme_info = gtk_table_new(7, 4, TRUE);
	gtk_table_set_col_spacings(GTK_TABLE(theme_info), 8);
	gtk_table_set_row_spacings(GTK_TABLE(theme_info), 8);

	/* Create combobox*/
	self->version_store = gtk_tree_store_new(
				2, G_TYPE_STRING, G_TYPE_STRING);

	self->combobox = gtk_combo_box_new_text();
	gtk_combo_box_set_wrap_width(GTK_COMBO_BOX(self->combobox), 1);
	gtk_combo_box_set_model(GTK_COMBO_BOX(self->combobox),
				GTK_TREE_MODEL(self->version_store));
	gtk_widget_set_size_request(GTK_WIDGET(self->combobox), -1, 32);

	theme_table = gtk_table_new(5, 7, TRUE);
	gtk_table_set_col_spacings(GTK_TABLE(theme_table), 1);
	gtk_table_set_row_spacings(GTK_TABLE(theme_table), 1);

	info_vbox = gtk_vbox_new(FALSE, 1);

	/* Create pannable areas */
	tree_view_pannable = hildon_pannable_area_new();
	hildon_pannable_area_add_with_viewport(HILDON_PANNABLE_AREA(tree_view_pannable), tree_view_hbox);
	gtk_container_add(GTK_CONTAINER(tree_view_frame), tree_view_pannable);

	/*create eventbox*/
	preview_event_box = gtk_event_box_new();
	self->select_label = gtk_label_new("  Select \ncategory");

	/* Create frame for theme_info */
	select_vbox = gtk_vbox_new(FALSE, 2);
	versions_hbox = gtk_hbox_new(FALSE, 2);
	theme_info_frame = gtk_frame_new("");
	gtk_frame_set_label_widget(GTK_FRAME(theme_info_frame),
		irreco_gtk_label_bold("Info", 0, 0, 0, 0, 0, 0));
	self->theme_info_alignment = gtk_alignment_new(0, 0, 0, 0);
	gtk_alignment_set_padding(GTK_ALIGNMENT(self->theme_info_alignment),
				  0, 4, 8, 8);

	preview_frame = gtk_frame_new(NULL);
	gtk_frame_set_shadow_type(GTK_FRAME(preview_frame), GTK_SHADOW_OUT);

	self->theme_image = gtk_image_new();
	gtk_image_set_from_file(GTK_IMAGE(self->theme_image), NULL);

	/*Dialog label*/
	label_name = gtk_label_new("Name: ");
	self->label_creator = gtk_label_new("Author: ");
	self->label_download= gtk_label_new("Downloads: ");
	label_description = gtk_label_new("Comment: ");
	self->label_combobox = gtk_label_new("Version:");
	gtk_widget_set_size_request(GTK_WIDGET(self->label_download), 50, -1);

	self->theme_name = gtk_label_new("");
	self->theme_creator = gtk_label_new("");
	self->theme_downloaded = gtk_label_new("");
	self->theme_comment = gtk_label_new("");
	self->theme_version = gtk_label_new("");

	comment_alignment = gtk_alignment_new(0, 0, 0, 0);
	gtk_widget_set_size_request(self->theme_comment,
                                     310, -1);
	gtk_alignment_set_padding(GTK_ALIGNMENT(comment_alignment),
				  0, 0, 20, 0);
	gtk_label_set_line_wrap(GTK_LABEL(self->theme_comment), TRUE);
	gtk_label_set_line_wrap_mode(GTK_LABEL(self->theme_comment),
				     PANGO_WRAP_WORD);

	gtk_misc_set_alignment(GTK_MISC(label_name), 0, 0.5);
	gtk_misc_set_alignment(GTK_MISC(self->label_creator), 0, 0.5);
	gtk_misc_set_alignment(GTK_MISC(self->label_download), 0, 0.5);
	gtk_misc_set_alignment(GTK_MISC(label_description), 0, 0.5);
	gtk_misc_set_alignment(GTK_MISC(self->label_combobox), 0, 0.5);
	gtk_misc_set_alignment(GTK_MISC(self->theme_name), 0, 0.5);
	gtk_misc_set_alignment(GTK_MISC(self->theme_creator), 0, 0.5);
	gtk_misc_set_alignment(GTK_MISC(self->theme_downloaded), 0, 0.5);
	gtk_misc_set_alignment(GTK_MISC(self->theme_comment), 0, 0.5);
	gtk_misc_set_alignment(GTK_MISC(self->theme_version), 0, 0.5);


	/*Attach WidGets to tables*/
	gtk_table_attach_defaults(GTK_TABLE(table),
				  tree_view_frame, 0, 3, 0, 7);
	gtk_table_attach_defaults(GTK_TABLE(theme_info),
				  preview_event_box, 0, 2, 4, 7);
	gtk_table_attach_defaults(GTK_TABLE(theme_info),
				  info_vbox, 0, 4, 3, 4);
	gtk_table_attach_defaults(GTK_TABLE(theme_info),
				  theme_table, 0, 4, 0, 3);
	gtk_table_attach_defaults(GTK_TABLE(theme_table),
				  label_name, 0, 3, 0, 1);
	gtk_table_attach_defaults(GTK_TABLE(theme_table),
				  self->label_creator, 0, 3, 1, 2);
	gtk_table_attach_defaults(GTK_TABLE(theme_table),
				  self->label_download, 0, 3, 2, 3);
	gtk_table_attach_defaults(GTK_TABLE(theme_table),
				  label_description, 0, 3, 3, 4);
/*	gtk_table_attach_defaults(GTK_TABLE(theme_table),
				  self->label_combobox, 0, 3, 3, 4);
	gtk_table_attach_defaults(GTK_TABLE(theme_table),
				  versions_hbox, 3, 7, 3, 4);*/
	gtk_table_attach_defaults(GTK_TABLE(table),
				  theme_info_frame, 3, 7, 0, 7);
	gtk_table_attach(GTK_TABLE(theme_table), self->theme_name,
			 3, 7, 0, 1, GTK_FILL, GTK_FILL, 0, 0);
	gtk_table_attach(GTK_TABLE(theme_table), self->theme_creator,
			 3, 7, 1, 2, GTK_FILL, GTK_FILL, 0, 0);
	gtk_table_attach(GTK_TABLE(theme_table), self->theme_downloaded,
			 3, 7, 2, 3, GTK_FILL, GTK_FILL, 0, 0);

	/*Add WidGets*/
	gtk_container_add(GTK_CONTAINER(preview_frame),
			  self->theme_image);
	gtk_container_add(GTK_CONTAINER(preview_event_box),
			  preview_frame);
	gtk_container_add(GTK_CONTAINER(info_vbox), comment_alignment);
	gtk_container_add(GTK_CONTAINER(comment_alignment),
					self->theme_comment);
	gtk_container_add(GTK_CONTAINER(theme_info_frame),
			  GTK_WIDGET(select_vbox));
	gtk_container_add(GTK_CONTAINER(select_vbox),
			  GTK_WIDGET(self->theme_info_alignment));
	gtk_container_add(GTK_CONTAINER(select_vbox),
			  GTK_WIDGET(self->select_label));
	gtk_container_add(GTK_CONTAINER(self->theme_info_alignment),
			  GTK_WIDGET(theme_info));
	gtk_container_add(GTK_CONTAINER(versions_hbox),
			  GTK_WIDGET(self->theme_version));
	gtk_container_add(GTK_CONTAINER(versions_hbox),
			  GTK_WIDGET(self->combobox));

	/*Set font size and style*/
	/* Font size*/
	initial_font = pango_font_description_from_string ("Sans Bold 16");
	gtk_widget_modify_font (self->select_label, initial_font);

	initial_font = pango_font_description_from_string ("Sans Bold 12");
	gtk_widget_modify_font (label_name, initial_font);
	gtk_widget_modify_font (self->label_creator, initial_font);
	gtk_widget_modify_font (self->label_download, initial_font);
	gtk_widget_modify_font (label_description, initial_font);
	gtk_widget_modify_font (self->label_combobox, initial_font);
	/* Font size*/
	initial_font = pango_font_description_from_string ("Sans 11");
	gtk_widget_modify_font (self->theme_name, initial_font);
	gtk_widget_modify_font (self->theme_creator, initial_font);
	gtk_widget_modify_font (self->theme_downloaded, initial_font);
	gtk_widget_modify_font (self->theme_comment, initial_font);
	gtk_widget_modify_font (self->theme_version, initial_font);
	gtk_widget_modify_font (self->combobox, initial_font);

	/* Setup the selection handler for TREE	*/
	select = gtk_tree_view_get_selection(self->tree_view);
	gtk_tree_selection_set_mode(select, GTK_SELECTION_SINGLE);

	/* Signal handlers. */
	g_signal_connect(G_OBJECT(self), "map-event",
		 G_CALLBACK(irreco_theme_manager_dlg_map_event), NULL);
	g_signal_connect(G_OBJECT(self), "destroy",
		 G_CALLBACK(irreco_theme_manager_dlg_destroy_event), NULL);
	g_signal_connect(G_OBJECT(self->tree_view), "row-activated",
		 G_CALLBACK(irreco_theme_manager_dlg_row_activated_event),
			 self);
	g_signal_connect(G_OBJECT(self->tree_view), "row-expanded",
		 G_CALLBACK(irreco_theme_manager_dlg_row_expanded_event),
			 self);
	g_signal_connect(G_OBJECT(self->tree_view), "row-collapsed",
		G_CALLBACK(irreco_theme_manager_dlg_row_collapsed_event),
			 self);
	g_signal_connect(G_OBJECT (select), "changed",
		 G_CALLBACK (irreco_theme_manager_dlg_row_selected_event),
			 self);
	g_signal_connect(G_OBJECT (self->combobox), "changed",
		 G_CALLBACK (irreco_theme_manager_dlg_selected_version),
			 self);
	g_signal_connect(G_OBJECT(preview_event_box),
			 "button-release-event",
			 G_CALLBACK(irreco_theme_manager_dlg_preview_dlg),
			 self);
	g_signal_connect(G_OBJECT(self->clear_button),
			 "clicked",
			 G_CALLBACK(irreco_theme_manager_dlg_delete_theme),
			 self);
	g_signal_connect(G_OBJECT(self->edit_button),
			 "clicked",
			 G_CALLBACK(irreco_theme_manager_dlg_new_theme),
			 self);
	/*g_signal_connect(G_OBJECT(self->new_button),
			 "clicked",
			 G_CALLBACK(irreco_theme_manager_dlg_new_theme),
			 self);*/

	gtk_tree_view_set_enable_tree_lines(self->tree_view, TRUE);
	g_object_set (G_OBJECT (self->tree_view), "show-expanders", TRUE, NULL);
	g_object_set (G_OBJECT (self->tree_view), "level-indentation", 0, NULL);
	gtk_tree_view_set_rubber_banding(self->tree_view, FALSE);

	gtk_widget_set_size_request(GTK_WIDGET(self), 696, 306);
	gtk_widget_show_all(GTK_WIDGET(self));

	IRRECO_RETURN
}

GtkWidget *irreco_theme_manager_dlg_new(IrrecoData *irreco_data,
				GtkWindow *parent)
{
	IrrecoThemeManagerDlg *self;

	IRRECO_ENTER

	self = g_object_new(IRRECO_TYPE_THEME_MANAGER_DLG, NULL);
	irreco_dlg_set_parent(IRRECO_DLG(self), parent);
	irreco_theme_manager_dlg_set_irreco_data(self, irreco_data);

	IRRECO_RETURN_PTR(self);
}

/**
 * Show whole preview image
 *
 */

gboolean irreco_theme_manager_dlg_preview_dlg(GtkWidget *widget,
					      GdkEventButton *event,
					      IrrecoThemeManagerDlg *self)
{
	GtkWidget *image, *event_box;
	GtkWidget *preview_dialog;

	IRRECO_ENTER

	if(self->preview_add != NULL ||
	   self->webdb_theme->preview_button != NULL) {

	preview_dialog = irreco_dlg_new();
	gtk_window_set_title(GTK_WINDOW(preview_dialog), _("Preview"));
	gtk_window_set_modal(GTK_WINDOW(preview_dialog), TRUE);
	gtk_window_set_destroy_with_parent(GTK_WINDOW(preview_dialog), TRUE);
	gtk_dialog_set_has_separator(GTK_DIALOG(preview_dialog), FALSE);

	gtk_widget_set_size_request(preview_dialog, 185, -1);

	event_box = gtk_event_box_new();
	image = gtk_image_new();
	if(self->preview_add != NULL){
		gtk_image_set_from_file(GTK_IMAGE(image), self->preview_add);

	}else{
		gtk_image_set_from_pixbuf(GTK_IMAGE(image),
				 GDK_PIXBUF(self->webdb_theme->preview_button));
	}

	gtk_box_pack_start_defaults(GTK_BOX(GTK_DIALOG(preview_dialog)->vbox),
				    event_box);
	gtk_container_add(GTK_CONTAINER(event_box), image);

	g_signal_connect_swapped(G_OBJECT(event_box),
			 "button-release-event",
			 G_CALLBACK(gtk_widget_destroy),
			 preview_dialog);

	gtk_widget_show_all(GTK_WIDGET(preview_dialog));
	}
	IRRECO_RETURN_BOOL(TRUE);
}

/**
 * Combobox  versions
 *
 */

static void
irreco_theme_manager_dlg_selected_version(GtkComboBox *widget,
					  IrrecoThemeManagerDlg *self)
{
	GtkTreeIter iter;
	gchar *id;

	IRRECO_ENTER

	if(!gtk_combo_box_get_active_iter(widget, &iter)) {
		IRRECO_RETURN
	}
	gtk_tree_model_get(GTK_TREE_MODEL(self->version_store), &iter,
			   ID_COL, &id, -1);

	if (self->webdb_theme == NULL || self->webdb_theme->id != atoi(id)) {
		IRRECO_DEBUG("SELECTED ID: %d\n", atoi(id));
		self->selected_theme_id = atoi(id);
		self->loader_func_id = g_idle_add(G_SOURCEFUNC(
			irreco_theme_manager_dlg_theme_db_detail_loader),
			self);
	}

	IRRECO_RETURN
}

/**
 * Delete user theme
 */

static void irreco_theme_manager_dlg_delete_theme(GtkButton *button,
                                                  IrrecoThemeManagerDlg *self)
{

	const gchar *name = gtk_label_get_label(GTK_LABEL(self->theme_name));
	GString *message = g_string_new(NULL);
	IRRECO_ENTER

	g_string_append_printf(
			message, _("Are you sure you want to\n "
			"delete %s theme?"),name);

	if (irreco_yes_no_dlg(GTK_WINDOW(self), message->str)){

		irreco_theme_manager_remove_theme(
				self->irreco_data->theme_manager, name);
		gtk_button_clicked(GTK_BUTTON(self->refresh_button));
	}

	g_string_free(message, TRUE);
	IRRECO_RETURN
}


/**
 * Check layouts
 *
 */

void irreco_theme_manager_dlg_set_layout_button(IrrecoThemeManagerDlg *self)
{
	IrrecoStringTable *table;
	gchar		*un_mmc1 = NULL;
	gchar		*un_mmc2 = NULL;
	gchar		*pressed_mmc1 = NULL;
	gchar		*pressed_mmc2 = NULL;
	gchar		*button_dir = NULL;
	GString		*bg_mmc = NULL;

	IRRECO_ENTER
	bg_mmc = g_string_new(NULL);
	table = self->irreco_data->irreco_layout_array;

	/* Check layouts buttons */
	IRRECO_STRING_TABLE_FOREACH_DATA(self->theme->buttons,
					 IrrecoThemeButton *, temp_button)

		button_dir = g_strrstr(temp_button->image_up->str, "/irreco");
		un_mmc1 = g_strconcat("/home/user/MyDocs/irreco", button_dir, NULL);
		un_mmc2 = g_strconcat("/home/user/MyDocs/irreco", button_dir, NULL);

		button_dir = g_strrstr(temp_button->image_down->str, "/irreco");
		pressed_mmc1 = g_strconcat("/home/user/MyDocs/irreco", button_dir, NULL);
		pressed_mmc2 = g_strconcat("/home/user/MyDocs/irreco", button_dir, NULL);

		if (!irreco_is_file(temp_button->image_up->str)) {

			IRRECO_STRING_TABLE_FOREACH(table, key,
						    IrrecoButtonLayout *,
	  					    layout)

				IRRECO_PTR_ARRAY_FORWARDS(layout->button_array,
							IrrecoButton *, button)

					if (button->style != NULL &&
					    g_str_equal(button->style->style_name->str,
					    temp_button->style_name->str)) {

						if (irreco_is_file(un_mmc1)) {
							g_string_printf(
							button->style->image_up,
							"%s", un_mmc1);
							g_string_printf(
							button->style->image_down,
							"%s", pressed_mmc1);
						} else if (irreco_is_file(un_mmc2)) {
							g_string_printf(
							button->style->image_up,
							"%s", un_mmc2);
							g_string_printf(
							button->style->image_down,
							"%s", pressed_mmc2);
						} else {
							button->type = IRRECO_BTYPE_GTK_BUTTON;
							irreco_button_set_style(button, NULL);
							irreco_button_create_widget(button);
							}
						}

				IRRECO_PTR_ARRAY_FORWARDS_END

			IRRECO_STRING_TABLE_FOREACH_END
		}
	IRRECO_STRING_TABLE_FOREACH_END

	irreco_config_save_layouts(self->irreco_data);

	/* Check layouts bg:s */
	IRRECO_STRING_TABLE_FOREACH_DATA(self->theme->backgrounds,
					 IrrecoThemeBg *, temp_bg)

		if (!irreco_is_file(temp_bg->image_name->str)) {

			IRRECO_STRING_TABLE_FOREACH(table, key,
						    IrrecoButtonLayout *,
	  					    layout)
				g_string_printf(bg_mmc, "%s" ,
					irreco_button_layout_get_bg_image(
					layout));

				g_string_erase(bg_mmc, 10, 1);
				g_string_insert_c(bg_mmc, 10, 1);
				/* Check mmc1 */
				if (irreco_is_file(bg_mmc->str)) {

					irreco_button_layout_set_bg_image(
							layout, bg_mmc->str);
				}

				g_string_erase(bg_mmc, 10, 1);
				g_string_insert_c(bg_mmc, 10, 2);
				/* Check mmc2 */
				if (irreco_is_file(bg_mmc->str)) {

					irreco_button_layout_set_bg_image(
							layout, bg_mmc->str);
				} else {
					irreco_button_layout_set_bg_image(
							layout,
       							IRRECO_DEFAULT_BG);
				}
			IRRECO_STRING_TABLE_FOREACH_END
		}

	IRRECO_STRING_TABLE_FOREACH_END

	g_string_free(bg_mmc, TRUE);
	if (un_mmc1 != NULL) g_free(un_mmc1);
	if (un_mmc2 != NULL) g_free(un_mmc2);
	if (pressed_mmc1 != NULL) g_free(pressed_mmc1);
	if (pressed_mmc2 != NULL) g_free(pressed_mmc2);
	IRRECO_RETURN
}

/**
 *Create new theme or edit theme
*/
static void irreco_theme_manager_dlg_new_theme(GtkButton *button,
					      	IrrecoThemeManagerDlg *self)
{

	IRRECO_ENTER
	/* Check which button */
	/* Create new theme */
	if (g_str_equal("New", gtk_button_get_label(button))) {
		IrrecoTheme *new_theme = NULL;
		new_theme = irreco_theme_new();

		if (irreco_theme_creator_dlg_run(GTK_WINDOW(self),
		    self->irreco_data, new_theme)) {
			gtk_button_clicked(GTK_BUTTON(self->refresh_button));
		}
	/* Edit theme */
	} else {

		IrrecoTheme	*new_theme = NULL;
		gchar		*rm_dir;
		gchar		*temp_dir;
		IrrecoWindowManager * manager;
		/* Create temp theme folder */
		temp_dir = g_strconcat("cp -r ", self->theme->path->str, " ",
				       "/home/user/MyDocs/irreco/irreco_temp", NULL);
		rm_dir = g_strconcat("rm -r ",
				     "/home/user/MyDocs/irreco/irreco_temp", NULL);
		/* Delete temp dir */
		if (irreco_is_dir("/home/user/MyDocs/irreco/irreco_temp")) {
			system(rm_dir);
		}
		system(temp_dir);

		new_theme = irreco_theme_new_from_dir(
				"/home/user/MyDocs/irreco/irreco_temp");

		irreco_theme_set(new_theme, self->theme->name->str,
				 self->theme->path->str,
     				 self->theme->source->str,
     				 self->theme->author->str,
	  			 self->theme->comment->str,
	  			 self->theme->preview_button_name->str,
       				 self->theme->version->str);

		IRRECO_DEBUG("Pointer: %p \n", (void*) self->theme);
		IRRECO_DEBUG("New_Pointer: %p \n", (void*) new_theme);
		irreco_theme_print(new_theme);

		if (irreco_theme_creator_dlg_run(GTK_WINDOW(self),
	   			 self->irreco_data, new_theme)) {

			irreco_theme_manager_dlg_set_layout_button(self);

			self->theme = irreco_theme_copy(new_theme);

			irreco_config_save_layouts(self->irreco_data);

			gtk_button_clicked(GTK_BUTTON(self->refresh_button));
		}

		/* Updated layout */
		manager = self->irreco_data->window_manager;
		irreco_window_edit_set_layout(manager->edit_window,
					      manager->current_layout);

		/* remove temp theme */
		system(rm_dir);

		g_free(rm_dir);
		g_free(temp_dir);
		irreco_theme_free(new_theme);
	}
	IRRECO_RETURN
}

/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Private Functions
 * @{
 */

/**
 * Clean detail list
 *
 */
static void irreco_theme_manager_dlg_clean_details(IrrecoThemeManagerDlg *self)
{
	IRRECO_ENTER

	gtk_widget_show_all(GTK_WIDGET(self));
	gtk_label_set_text(
		GTK_LABEL(self->theme_name), "-");
	gtk_label_set_text(
		GTK_LABEL(self->theme_creator), "-");
	gtk_label_set_text(
		GTK_LABEL(self->theme_comment), "-");
	gtk_label_set_text(
		GTK_LABEL(self->theme_downloaded), "-");
	gtk_image_clear(GTK_IMAGE(self->theme_image));
	self->preview_add=NULL;

	gtk_widget_hide(self->upload_button);
	gtk_widget_hide(self->download_button);
	gtk_widget_hide(self->clear_button);
	gtk_widget_hide(self->edit_button);
	gtk_widget_hide(self->theme_info_alignment);
	gtk_widget_show(self->select_label);

	IRRECO_RETURN
}

/**
 * Have the childern of this item been loaded.
 *
 * @return TRUE if children have been loade, FALSE otherwise.
*/

static gboolean
irreco_theme_manager_dlg_row_is_loaded(IrrecoThemeManagerDlg *self,
				       GtkTreeIter *iter)
{
	gint i;
	IRRECO_ENTER

	gtk_tree_model_get(GTK_TREE_MODEL(self->tree_store),
			   iter, FLAG_COL, &i, -1);
	if (i & ROW_CHILDREN_LOADED) IRRECO_RETURN_BOOL(TRUE);

	IRRECO_RETURN_BOOL(FALSE);
}

/**
 * Get type of row.
 */

static gint irreco_theme_manager_dlg_row_get_type(IrrecoThemeManagerDlg *self,
					          GtkTreeIter *iter)
{
	gint i;

	IRRECO_ENTER

	gtk_tree_model_get(GTK_TREE_MODEL(self->tree_store),
			   iter, FLAG_COL, &i, -1);
	if (i & ROW_TYPE_CATEGORY)
		IRRECO_RETURN_ENUM(ROW_TYPE_CATEGORY);
	if (i & ROW_TYPE_DEB)
		IRRECO_RETURN_ENUM(ROW_TYPE_DEB);
	if (i & ROW_TYPE_USER)
		IRRECO_RETURN_ENUM(ROW_TYPE_USER);
	if (i & ROW_TYPE_WEB)
		IRRECO_RETURN_ENUM(ROW_TYPE_WEB);
	if (i & ROW_TYPE_DB)
		IRRECO_RETURN_ENUM(ROW_TYPE_DB);
	if (i & ROW_TYPE_LOADING)
		IRRECO_RETURN_ENUM(ROW_TYPE_LOADING);
	IRRECO_RETURN_INT(0);
}

/**
 * Show hildon progressbar banner.
 *
 * This function will create a new banner if one has not been created yet,
 * if banner already exists, it's properties will be changed.
 *
 * @param text		Text to show.
 * @param fraction	Value of progress.
 */

static void irreco_theme_manager_dlg_set_banner(IrrecoThemeManagerDlg *self,
						const gchar *text,
						gdouble fraction)
{
	IRRECO_ENTER

	if (self->banner == NULL) {
		self->banner = hildon_banner_show_progress(
			GTK_WIDGET(self), NULL, "");
	}

	hildon_banner_set_text(HILDON_BANNER(self->banner), text);
	hildon_banner_set_fraction(HILDON_BANNER(self->banner), fraction);

	IRRECO_RETURN
}

/**
 * Destroy banner, if it exists.
 */

static void irreco_theme_manager_dlg_hide_banner(IrrecoThemeManagerDlg *self)
{
	IRRECO_ENTER

	if (self->banner) {
		gtk_widget_destroy(self->banner);
		self->banner = NULL;
	}

	IRRECO_RETURN
}

/**
 * Start a loader state machine if one is not running already.
 */

static
gboolean irreco_theme_manager_dlg_loader_start(IrrecoThemeManagerDlg *self,
					       GSourceFunc function,
	    				       GtkTreeIter *parent_iter)
{
	IRRECO_ENTER

	if (self->loader_func_id == 0) {
		if (parent_iter) {
			self->loader_parent_iter = gtk_tree_iter_copy(
				parent_iter);
		}
		if (function) {
			self->loader_func_id = g_idle_add(function, self);
		} else {
			IRRECO_ERROR("Loader function pointer not given.\n");
		}
	}
	IRRECO_RETURN_BOOL(TRUE);

}

/**
 * Stop and cleanup loader if a loader is running.
 */

static void irreco_theme_manager_dlg_loader_stop(IrrecoThemeManagerDlg *self)
{
	IRRECO_ENTER

	if (self->loader_func_id != 0) {
		g_source_remove(self->loader_func_id);
		self->loader_func_id = 0;
		self->loader_state = 0;
		if (self->loader_parent_iter) {
			gtk_tree_iter_free(self->loader_parent_iter);
		}
		self->loader_parent_iter = NULL;
	}
	IRRECO_RETURN
}

static gboolean
irreco_theme_manager_dlg_loader_user(IrrecoThemeManagerDlg *self)
{
	IRRECO_ENTER

	switch (self->loader_state) {
	case LOADER_STATE_INIT: {
		GtkTreeIter iter;
		irreco_theme_manager_dlg_set_banner(self, _("Loading ..."),
						    0.5);
		self->loader_state = LOADER_STATE_LOOP;
		irreco_theme_manager_dlg_clean_details(self);
		if (!irreco_theme_manager_does_user_exist(
				self->irreco_data->theme_manager)) {
			self->loader_state = LOADER_STATE_END;
			self->loader_iter = NULL;
		} else {


			gtk_tree_store_append(self->tree_store,
					&iter, NULL);
			gtk_tree_store_set(self->tree_store,
					&iter,
					TEXT_COL, _("My Themes"),
					FLAG_COL, ROW_TYPE_CATEGORY,
					DATA_COL, NULL, -1);

			self->loader_iter = gtk_tree_iter_copy(&iter);

		}
		IRRECO_RETURN_BOOL(TRUE);

	}

	case LOADER_STATE_LOOP: {

		IrrecoData         *irreco_data   = NULL;
		IrrecoThemeManager *theme_manager = NULL;
		GtkTreeIter iter;
		IrrecoStringTable *categories = NULL;


		irreco_data = self->irreco_data;
		theme_manager = irreco_data->theme_manager;
		categories = irreco_theme_manager_get_themes(theme_manager);

		IRRECO_STRING_TABLE_FOREACH_DATA(categories,
						 IrrecoTheme *, theme)

			if (g_utf8_collate(theme->source->str, "user") == 0){

				/* Add loading item into category. */
				gtk_tree_store_append(self->tree_store,
							&iter,
							self->loader_iter);
				gtk_tree_store_set(self->tree_store,
							&iter,
							TEXT_COL,
							theme->name->str,
							FLAG_COL,
							ROW_TYPE_USER,
							DATA_COL, NULL, -1);
			}
		IRRECO_STRING_TABLE_FOREACH_END

		irreco_theme_manager_dlg_set_banner(self, _("Loading ..."),
						    1.0);
		self->loader_state = LOADER_STATE_END;
		IRRECO_RETURN_BOOL(TRUE);
		}

	case LOADER_STATE_END:
		if (self->loader_iter != NULL) {
			gtk_tree_iter_free(self->loader_iter);
		}
		irreco_theme_manager_dlg_hide_banner(self);
		irreco_theme_manager_dlg_loader_stop(self);

		irreco_theme_manager_dlg_loader_start(self,
		G_SOURCEFUNC(irreco_theme_manager_dlg_loader_deb),NULL);
	}

	IRRECO_RETURN_BOOL(FALSE);
}

/**
 * Category loader.
 *
 * This loader will request a list of categories from the theme and
 * update the TreeView accordingly. Every category row created by this loader
 * will have row type ROW_TYPE_DEB.
 */


static gboolean irreco_theme_manager_dlg_loader_deb(IrrecoThemeManagerDlg *self)
{
	IRRECO_ENTER

	switch (self->loader_state) {
	case LOADER_STATE_INIT: {
		GtkTreeIter iter;
		irreco_theme_manager_dlg_set_banner(self, _("Loading ..."),
						    0.5);
		self->loader_state = LOADER_STATE_LOOP;
		irreco_theme_manager_dlg_clean_details(self);

		if (!irreco_theme_manager_does_deb_exist(
				self->irreco_data->theme_manager)) {
			self->loader_state = LOADER_STATE_END;
			self->loader_iter = NULL;
		} else {

			gtk_tree_store_append(self->tree_store,
					&iter, NULL);
			gtk_tree_store_set(self->tree_store,
					&iter,
					TEXT_COL, _("Built In"),
					FLAG_COL, ROW_TYPE_CATEGORY,
					DATA_COL, NULL, -1);

			self->loader_iter = gtk_tree_iter_copy(&iter);
		}
		IRRECO_RETURN_BOOL(TRUE);

	}

	case LOADER_STATE_LOOP: {

		IrrecoData         *irreco_data   = NULL;
		IrrecoThemeManager *theme_manager = NULL;
		GtkTreeIter iter;
		IrrecoStringTable *categories = NULL;

		irreco_data = self->irreco_data;
		theme_manager = irreco_data->theme_manager;
		categories = irreco_theme_manager_get_themes(theme_manager);

		IRRECO_STRING_TABLE_FOREACH_DATA(categories,
						 IrrecoTheme *, theme)
			if (g_utf8_collate(theme->source->str, "deb") == 0){

				/* Add loading item into category. */
				gtk_tree_store_append(self->tree_store,
						&iter, self->loader_iter);
				gtk_tree_store_set(self->tree_store,
							&iter,
							TEXT_COL,
							theme->name->str,
							FLAG_COL, ROW_TYPE_DEB,
							DATA_COL, NULL, -1);
			}
		IRRECO_STRING_TABLE_FOREACH_END

		irreco_theme_manager_dlg_set_banner(self, _("Loading ..."),
						    1.0);
		self->loader_state = LOADER_STATE_END;
		IRRECO_RETURN_BOOL(TRUE);
		}

	case LOADER_STATE_END: {
		GtkTreeIter iter, iter_loading;
		if (self->loader_iter != NULL) {
			gtk_tree_iter_free(self->loader_iter);
		}
		irreco_theme_manager_dlg_hide_banner(self);
		irreco_theme_manager_dlg_loader_stop(self);

		/* Create database row */
		gtk_tree_store_append(self->tree_store, &iter, NULL);
		gtk_tree_store_set(self->tree_store, &iter,
				   TEXT_COL, _("Database"),
				   FLAG_COL, ROW_TYPE_CATEGORY,
				   DATA_COL, NULL, -1);

		/* Add loading item into manufacturer. */
		gtk_tree_store_append(self->tree_store, &iter_loading, &iter);
		gtk_tree_store_set(self->tree_store, &iter_loading,
				   TEXT_COL, _("Loading ..."),
				   FLAG_COL, ROW_TYPE_LOADING,
				   DATA_COL, NULL, -1);

		irreco_theme_manager_dlg_loader_start(self,
		G_SOURCEFUNC(irreco_theme_manager_dlg_loader_web), NULL);
		}
	}

	IRRECO_RETURN_BOOL(FALSE);
}

static gboolean irreco_theme_manager_dlg_loader_db(IrrecoThemeManagerDlg *self)
{
	IRRECO_ENTER

	switch (self->loader_state) {
	case LOADER_STATE_INIT:{
		GtkTreeIter iter;
		irreco_theme_manager_dlg_set_banner(self, _("Loading themes"),
						    0.0);
		self->loader_state = LOADER_STATE_LOOP;
		self->theme_loader_index = 0;
		irreco_theme_manager_dlg_clean_details(self);

		/* Check if loading item exist */
		gtk_tree_model_iter_nth_child(GTK_TREE_MODEL(self->tree_store),
					      &iter, self->loader_iter, 0);

		if(irreco_theme_manager_dlg_row_get_type(self,
						&iter) != ROW_TYPE_LOADING) {
			self->loader_state = LOADER_STATE_END;
		}

		IRRECO_RETURN_BOOL(TRUE);

	}

	case LOADER_STATE_LOOP: {
		GtkTreeIter iter;
		GtkTreeIter iter_loading;
		IrrecoStringTable	*themes = NULL;
		IrrecoWebdbTheme	*theme_by_id;
		IrrecoWebdbCache	*webdb_cache = NULL;
		const gchar *id;
		gfloat banner;

		webdb_cache = irreco_data_get_webdb_cache(self->irreco_data,
							  FALSE);
		/* Get themes */
		if (!irreco_webdb_cache_get_themes(webdb_cache, &themes)) {
			irreco_error_dlg(GTK_WINDOW(self),
					 irreco_webdb_cache_get_error(
					 webdb_cache));
			self->loader_state = LOADER_STATE_END;
			IRRECO_RETURN_BOOL(TRUE);
		}

		if (!irreco_string_table_index(themes,
				(guint)self->theme_loader_index, &id, NULL)) {
			irreco_error_dlg(GTK_WINDOW(self), "Can't find theme");
			self->loader_state = LOADER_STATE_END;
			IRRECO_RETURN_BOOL(TRUE);
		}

		if (!irreco_webdb_cache_get_theme_by_id(webdb_cache,
							atoi(id),
							&theme_by_id)) {
			irreco_error_dlg(GTK_WINDOW(self),
					 irreco_webdb_cache_get_error(
					 webdb_cache));
			self->loader_state = LOADER_STATE_END;
			IRRECO_RETURN_BOOL(TRUE);
		}

		gtk_tree_store_append(self->tree_store, &iter,
				      self->loader_iter);

		gtk_tree_store_set(self->tree_store, &iter,
				   TEXT_COL, theme_by_id->name->str,
				   FLAG_COL, ROW_TYPE_DB,
				   DATA_COL, id, -1);

		banner = ++self->theme_loader_index;
		banner /= (gfloat) irreco_string_table_lenght (themes);

		irreco_theme_manager_dlg_set_banner(self, _("Loading themes"),
						    banner);
		if (banner >= 1.0) {
			self->loader_state = LOADER_STATE_END;
		}

		/* Delete loading item */
		gtk_tree_model_iter_nth_child(GTK_TREE_MODEL(self->tree_store),
					      &iter_loading, self->loader_iter,
					      0);
		if(irreco_theme_manager_dlg_row_get_type(self,
		   &iter_loading) == ROW_TYPE_LOADING) {
			gtk_tree_store_remove(self->tree_store, &iter_loading);
		}

		IRRECO_RETURN_BOOL(TRUE);
	}

	case LOADER_STATE_END:
		if (self->loader_iter != NULL) {
			gtk_tree_iter_free(self->loader_iter);
		}
		irreco_theme_manager_dlg_hide_banner(self);
		irreco_theme_manager_dlg_loader_stop(self);

	}
	IRRECO_RETURN_BOOL(FALSE);

}

static gboolean irreco_theme_manager_dlg_loader_web(IrrecoThemeManagerDlg *self)
{
	IRRECO_ENTER

	switch (self->loader_state) {
	case LOADER_STATE_INIT: {
		GtkTreeIter iter;
		irreco_theme_manager_dlg_set_banner(self, _("Loading ..."),
						    0.5);
		self->loader_state = LOADER_STATE_LOOP;
		irreco_theme_manager_dlg_clean_details(self);
		if (!irreco_theme_manager_does_web_exist(
				self->irreco_data->theme_manager)) {
			self->loader_state = LOADER_STATE_END;
			self->loader_iter = NULL;
		} else {

			gtk_tree_store_append(self->tree_store,
					&iter, NULL);
			gtk_tree_store_set(self->tree_store,
					   &iter,
					   TEXT_COL, _("Downloaded"),
					   FLAG_COL, ROW_TYPE_CATEGORY,
					   DATA_COL, NULL, -1);

			self->loader_iter = gtk_tree_iter_copy(&iter);
		}
		IRRECO_RETURN_BOOL(TRUE);
	}
	case LOADER_STATE_LOOP: {

		IrrecoData         *irreco_data   = NULL;
		IrrecoThemeManager *theme_manager = NULL;
		GtkTreeIter iter;
		IrrecoStringTable *categories = NULL;

		irreco_data = self->irreco_data;
		theme_manager = irreco_data->theme_manager;
		categories = irreco_theme_manager_get_themes(theme_manager);

		IRRECO_STRING_TABLE_FOREACH_DATA(categories,
						 IrrecoTheme *, theme)

			if (g_utf8_collate(theme->source->str, "web") == 0){

				/* Add loading item into category. */
				gtk_tree_store_append(self->tree_store,
							&iter,
							self->loader_iter);
				gtk_tree_store_set(self->tree_store,
							&iter,
							TEXT_COL,
							theme->name->str,
							FLAG_COL,
							ROW_TYPE_WEB,
							DATA_COL, NULL, -1);
			}
		IRRECO_STRING_TABLE_FOREACH_END

		irreco_theme_manager_dlg_set_banner(self, _("Loading ..."),
						    1.0);
		self->loader_state = LOADER_STATE_END;
		IRRECO_RETURN_BOOL(TRUE);
		}

	case LOADER_STATE_END:
		if (self->loader_iter != NULL) {
			gtk_tree_iter_free(self->loader_iter);
		}
		irreco_theme_manager_dlg_hide_banner(self);
		irreco_theme_manager_dlg_loader_stop(self);
	}

	IRRECO_RETURN_BOOL(FALSE);
}

/**
 * Show theme details
 *
 * @todo
 */

static gboolean
irreco_theme_manager_dlg_display_theme_detail(IrrecoThemeManagerDlg *self,
					      GtkTreeIter * iter)
{
	IrrecoThemeManager *theme_manager = self->irreco_data->theme_manager;
	IrrecoThemeButton *button = NULL;
	const gchar *theme_name;

	IRRECO_ENTER

	gtk_image_clear(GTK_IMAGE(self->theme_image));

	gtk_tree_model_get(GTK_TREE_MODEL(self->tree_store),
				iter, TEXT_COL, &theme_name, -1);
	irreco_string_table_get(theme_manager->themes, theme_name,
				(gpointer *) &self->theme);

	gtk_label_set_text(GTK_LABEL(self->theme_name),
				self->theme->name->str);
	gtk_label_set_text(GTK_LABEL(self->theme_creator),
				self->theme->author->str);
	gtk_label_set_text(GTK_LABEL(self->theme_comment),
				self->theme->comment->str);
	gtk_label_set_text(GTK_LABEL(self->theme_version),
			   	self->theme->version->str);

	if (self->theme->preview_button_name->len > 0) {
		button =irreco_theme_get_button(self->theme,
				self->theme->preview_button_name->str);
	} else {
		const gchar *key;
		irreco_string_table_index(self->theme->buttons,
						0, &key, (gpointer *) &button);
	}

	if (button != NULL) {
		IRRECO_DEBUG("Preview-button: %s\n", button->image_up->str);

		gtk_image_set_from_file(GTK_IMAGE(self->theme_image),
					button->image_up->str);

		self->preview_add = button->image_up->str;
	} else {
		self->preview_add = NULL;
	}
	self->webdb_theme = NULL;

	IRRECO_RETURN_BOOL(TRUE);
}

static gboolean
irreco_theme_manager_dlg_theme_db_detail_loader(IrrecoThemeManagerDlg *self)
{
	IRRECO_ENTER

	switch (self->loader_state) {
	case LOADER_STATE_INIT:{
		irreco_theme_manager_dlg_set_banner(self,
						    _("Loading ..."), 0.5);
		self->loader_state = LOADER_STATE_LOOP;
		IRRECO_RETURN_BOOL(TRUE);
		}

	case LOADER_STATE_LOOP: {
		gchar *download_count;
		gint index = 0;
		IrrecoWebdbCache *webdb_cache = irreco_data_get_webdb_cache(
							self->irreco_data,
							FALSE);

		/* Get WebdbTheme with preview-button */
		if (!irreco_webdb_cache_get_preview_button(webdb_cache,
						self->selected_theme_id,
						&self->webdb_theme)) {
			irreco_error_dlg(GTK_WINDOW(self),
					 irreco_webdb_cache_get_error(
					 webdb_cache));

			self->loader_state = LOADER_STATE_END;
			IRRECO_RETURN_BOOL(TRUE);
		}

		gtk_image_clear(GTK_IMAGE(self->theme_image));

		gtk_label_set_text(GTK_LABEL(self->theme_name),
					self->webdb_theme->name->str);
		gtk_label_set_text(GTK_LABEL(self->theme_creator),
					self->webdb_theme->creator->str);
		gtk_label_set_text(GTK_LABEL(self->theme_comment),
					self->webdb_theme->comment->str);

		/* Clear combobox */
		gtk_tree_store_clear(self->version_store);

		IRRECO_STRING_TABLE_FOREACH(self->webdb_theme->versions,
					key, gchar *, date)
			GtkTreeIter iter;

			/* Append row */
			gtk_tree_store_append(self->version_store,
						&iter, NULL);

			/* Fill row */
			gtk_tree_store_set(self->version_store, &iter,
						DATE_COL, date,
						ID_COL, key, -1);

			if (g_utf8_collate (date,
				self->webdb_theme->uploaded->str) == 0) {
				gtk_combo_box_set_active(GTK_COMBO_BOX(
							 self->combobox),
							 index);
			}
			index++;
		IRRECO_STRING_TABLE_FOREACH_END

		/*scale_combobox(self);*/

		download_count = g_strdup_printf("%d",
					self->webdb_theme->download_count);

		gtk_label_set_text(GTK_LABEL(self->theme_downloaded),
					download_count);
		g_free(download_count);

		if (self->webdb_theme->preview_button != NULL) {
			gtk_image_set_from_pixbuf(GTK_IMAGE(self->theme_image),
					self->webdb_theme->preview_button);
		} else {
			IRRECO_DEBUG("preview_button is NULL\n");
		}
		self->preview_add = NULL;

		self->loader_state = LOADER_STATE_END;
		IRRECO_RETURN_BOOL(TRUE);
	}

	case LOADER_STATE_END:
		irreco_theme_manager_dlg_hide_banner(self);
		irreco_theme_manager_dlg_loader_stop(self);
	}
	IRRECO_RETURN_BOOL(FALSE);
}


/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Functions                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */

void irreco_theme_manager_dlg_set_irreco_data(IrrecoThemeManagerDlg *self,
				              IrrecoData *irreco_data)
{
	IRRECO_ENTER

	self->irreco_data = irreco_data;

	IRRECO_RETURN
}

void irreco_show_theme_manager_dlg(IrrecoData *irreco_data, GtkWindow *parent)
{
	IrrecoThemeManagerDlg	*self;
	gint		response;
	gboolean	loop = TRUE;
	gboolean	refresh = FALSE;
	IrrecoWebdbCache *webdb_cache = NULL;

	IRRECO_ENTER

	self = IRRECO_THEME_MANAGER_DLG(irreco_theme_manager_dlg_new(
					irreco_data, parent));

	webdb_cache = irreco_data_get_webdb_cache(self->irreco_data, FALSE);

	do {
		if (refresh) {
			response = IRRECO_DEVICE_REFRESH;
			refresh = FALSE;
		}
		else {
			response = gtk_dialog_run(GTK_DIALOG(self));
		}

		switch (response) {
		case IRRECO_DOWNLOAD_THEME:
			if (irreco_theme_download_dlg_run(
					self->irreco_data,
					self->selected_theme_id,
     					GTK_WINDOW(self))) {
				refresh = TRUE;
			}
			break;

		case IRRECO_UPLOAD_THEME:
			if (irreco_theme_upload_dlg_run(GTK_WINDOW(self),
						self->irreco_data,
      						self->theme,
						NULL,
						NULL)) {
				refresh = TRUE;
			}
			break;

		case IRRECO_DEVICE_REFRESH:
			if (self->loader_func_id != 0) {
				break;
			}
			IRRECO_DEBUG("IRRECO_DEVICE_REFRESH\n");
			gtk_tree_store_clear(self->tree_store);
                        irreco_theme_manager_update_theme_manager
                                (self->irreco_data->theme_manager);

			if (webdb_cache->theme_id_hash != NULL) {
				g_hash_table_remove_all(
						webdb_cache->theme_id_hash);
				self->webdb_theme = NULL;
			}

			IRRECO_STRING_TABLE_FOREACH_DATA(
					irreco_data->theme_manager->themes,
					IrrecoTheme *, theme)
				irreco_theme_check(theme);
			IRRECO_STRING_TABLE_FOREACH_END

			irreco_theme_manager_dlg_loader_start(self,
				G_SOURCEFUNC(
				irreco_theme_manager_dlg_loader_user),
				NULL);
			gtk_label_set_text(GTK_LABEL(self->select_label),
					   "  Select \ncategory");
			break;

		case GTK_RESPONSE_DELETE_EVENT:
			IRRECO_DEBUG("GTK_RESPONSE_DELETE_EVENT\n");
			loop = FALSE;
			break;
		}
	} while (loop);

	gtk_widget_destroy(GTK_WIDGET(self));

	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Events and Callbacks                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Events and Callbacks
 * @{
 */

static gboolean
irreco_theme_manager_dlg_map_event(IrrecoThemeManagerDlg *self,
				        GdkEvent *event,
					gpointer user_data)
{

	IRRECO_ENTER

	irreco_theme_manager_dlg_loader_start(
		self, G_SOURCEFUNC(irreco_theme_manager_dlg_loader_user),
				   NULL);

	IRRECO_RETURN_BOOL(FALSE);
}

static void
irreco_theme_manager_dlg_destroy_event(IrrecoThemeManagerDlg *self,
					    gpointer user_data)
{
	IRRECO_ENTER
	irreco_theme_manager_dlg_loader_stop(self);
	IRRECO_RETURN
}

static void
irreco_theme_manager_dlg_row_activated_event(GtkTreeView *tree_view,
						  GtkTreePath *path,
						  GtkTreeViewColumn *column,
						  IrrecoThemeManagerDlg *self)
{
	IRRECO_ENTER

	if (gtk_tree_view_row_expanded(tree_view, path)) {
		gtk_tree_view_expand_row(tree_view, path, FALSE);
	} else {
		gtk_tree_view_collapse_row(tree_view, path);
	}
	IRRECO_RETURN
}

static void
irreco_theme_manager_dlg_row_expanded_event(GtkTreeView *tree_view,
						 GtkTreeIter *iter,
						 GtkTreePath *path,
						 IrrecoThemeManagerDlg *self)
{
	gchar *row_name = NULL;
	IRRECO_ENTER

	irreco_theme_manager_dlg_clean_details(self);
	gtk_label_set_text(GTK_LABEL(self->select_label), "Select \ntheme");


	if (self->loader_func_id != 0) {
		gtk_tree_view_collapse_row(tree_view, path);
	}

	if (!irreco_theme_manager_dlg_row_is_loaded(self, iter)){
		switch (irreco_theme_manager_dlg_row_get_type(self, iter)){
		case ROW_TYPE_CATEGORY:
			gtk_tree_model_get(GTK_TREE_MODEL(self->tree_store),
					   iter, TEXT_COL, &row_name, -1);

			if(g_utf8_collate(row_name, "Database") != 0) {
				goto end;
			}

			self->loader_iter = gtk_tree_iter_copy(iter);
			irreco_theme_manager_dlg_loader_start(self,
			G_SOURCEFUNC(irreco_theme_manager_dlg_loader_db), NULL);
			break;
	}
	}
	end:
	if (row_name != NULL) g_free(row_name);
	IRRECO_RETURN
}

static void
irreco_theme_manager_dlg_row_collapsed_event(GtkTreeView *tree_view,
						  GtkTreeIter *iter,
						  GtkTreePath *path,
						  IrrecoThemeManagerDlg *self)
{

	IRRECO_ENTER

	gtk_label_set_text(GTK_LABEL(self->select_label), "  Select\ncategory");

	irreco_theme_manager_dlg_clean_details(self);

	IRRECO_RETURN
}

static void
irreco_theme_manager_dlg_row_selected_event(GtkTreeSelection *sel,
						 IrrecoThemeManagerDlg *self)
{
	GtkTreeIter iter;
	GtkTreeModel *model;
	const char *id;

	IRRECO_ENTER

	if (gtk_tree_selection_get_selected (sel, &model, &iter))
	{
		switch (irreco_theme_manager_dlg_row_get_type(self, &iter)) {

		case ROW_TYPE_DEB:
			IRRECO_DEBUG("ROW_TYPE_DEB\n");

			gtk_widget_hide(self->select_label);
			gtk_widget_show(self->theme_info_alignment);
			gtk_widget_hide(self->label_download);
			gtk_widget_hide(self->theme_downloaded);
			gtk_widget_hide(self->label_combobox);
			gtk_widget_hide(self->combobox);
			gtk_widget_hide(self->theme_version);
			gtk_widget_hide(self->theme_creator);
			gtk_widget_hide(self->label_creator);

			gtk_widget_hide(self->upload_button);
			gtk_widget_hide(self->download_button);
			gtk_widget_hide(self->clear_button);
			/*gtk_widget_show(self->new_button);*/
			gtk_widget_hide(self->edit_button);


			irreco_theme_manager_dlg_display_theme_detail(self,
								       &iter);

			break;
		case ROW_TYPE_USER:
			IRRECO_DEBUG("ROW_TYPE_USER\n");

			gtk_widget_hide(self->select_label);
			gtk_widget_show(self->theme_info_alignment);
			gtk_widget_hide(self->label_download);
			gtk_widget_hide(self->theme_downloaded);
			gtk_widget_show(self->theme_creator);
			gtk_widget_show(self->label_creator);

			gtk_widget_show(self->label_combobox);
			gtk_widget_hide(self->combobox);
			gtk_widget_show(self->theme_version);

			gtk_widget_show(self->upload_button);
			gtk_widget_show(self->clear_button);
			gtk_widget_hide(self->download_button);
			gtk_widget_show(self->edit_button);
			/*gtk_widget_hide(self->new_button);*/

			irreco_theme_manager_dlg_display_theme_detail(self,
								       &iter);

			break;
		case ROW_TYPE_WEB:
			IRRECO_DEBUG("ROW_TYPE_WEB\n");

			gtk_widget_show(self->clear_button);
			gtk_widget_hide(self->upload_button);
			gtk_widget_hide(self->download_button);
			/*gtk_widget_show(self->new_button);*/
			gtk_widget_hide(self->edit_button);


			gtk_widget_hide(self->select_label);
			gtk_widget_show(self->theme_creator);
			gtk_widget_show(self->label_creator);
			gtk_widget_hide(self->label_download);
			gtk_widget_hide(self->theme_downloaded);

			gtk_widget_hide(self->combobox);
			gtk_widget_show(self->label_combobox);
			gtk_widget_show(self->theme_version);

			gtk_widget_show(self->theme_info_alignment);

			irreco_theme_manager_dlg_display_theme_detail(self,
								       &iter);

			break;
		case ROW_TYPE_DB:
			IRRECO_DEBUG("ROW_TYPE_DB\n");

			if (self->loader_func_id != 0) {
				break;
			}

			gtk_tree_model_get(GTK_TREE_MODEL(self->tree_store),
					   &iter, DATA_COL, &id, -1);

			gtk_widget_hide(self->clear_button);
			gtk_widget_hide(self->upload_button);
			gtk_widget_show(self->download_button);
			/*gtk_widget_show(self->new_button);*/
			gtk_widget_hide(self->edit_button);


			gtk_widget_show(self->combobox);
			gtk_widget_show(self->label_combobox);
			gtk_widget_hide(self->theme_version);

			gtk_widget_hide(self->select_label);
			gtk_widget_show(self->theme_creator);
			gtk_widget_show(self->label_creator);
			gtk_widget_show(self->label_download);
			gtk_widget_show(self->theme_downloaded);

			gtk_widget_show(self->theme_info_alignment);

			IRRECO_DEBUG("SELECTED ID: %d\n", atoi(id));

			self->selected_theme_id = atoi(id);
			self->loader_func_id = g_idle_add(G_SOURCEFUNC(
			irreco_theme_manager_dlg_theme_db_detail_loader),
			self);

			break;
		default:
			irreco_theme_manager_dlg_clean_details(self);
			break;
		}
	}

	IRRECO_RETURN
}
/** @} */
/** @} */


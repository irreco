/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * irreco_theme_save_dlg.c
 * Copyright (C)  2008 Pekka Gehör (pegu6@msn.com)
 *
 * irreco_theme_save_dlg.c is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * irreco_theme_save_dlg.c is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "irreco_theme_save_dlg.h"
#include "irreco_theme_creator_backgrounds.h"
#include "irreco_theme_creator_dlg.h"
#include "irreco_theme_button.h"
#include <hildon/hildon-banner.h>
#include <hildon/hildon-color-button.h>
#include <hildon/hildon-file-chooser-dialog.h>

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
gboolean _save_theme_to_dir(IrrecoThemeSaveDlg *self);
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */


G_DEFINE_TYPE (IrrecoThemeSaveDlg, irreco_theme_save_dlg,
	       IRRECO_TYPE_INTERNAL_DLG)


static void irreco_theme_save_dlg_constructed(GObject *object)
{
	IrrecoThemeSaveDlg		*self;
	GtkWidget			*label;

	IRRECO_ENTER

	G_OBJECT_CLASS(
	irreco_theme_save_dlg_parent_class)->constructed(object);

	self = IRRECO_THEME_SAVE_DLG(object);

	/* Construct dialog. */
	gtk_window_set_title(GTK_WINDOW(self), _("Theme Save"));
	gtk_window_set_modal(GTK_WINDOW(self), TRUE);
	gtk_window_set_destroy_with_parent(GTK_WINDOW(self), TRUE);
	gtk_dialog_set_has_separator(GTK_DIALOG(self), FALSE);

	/*Buttons*/
	gtk_dialog_add_buttons(GTK_DIALOG(self),
			       GTK_STOCK_SAVE, GTK_RESPONSE_OK,
			       NULL);


	label = gtk_label_new("Select save location");

	self->radio1 = gtk_radio_button_new_with_label (NULL, "MMC1");
	self->radio2 = gtk_radio_button_new_with_label_from_widget(
							GTK_RADIO_BUTTON(
							self->radio1), "MMC2");
	self->radio3 = gtk_radio_button_new_with_label_from_widget(
							GTK_RADIO_BUTTON(
							self->radio2), "DEVICE");

	gtk_box_pack_start_defaults(GTK_BOX(GTK_DIALOG(self)->vbox), label);

	gtk_box_pack_start_defaults(GTK_BOX(
                              GTK_DIALOG(self)->vbox),
                                self->radio1);

	g_mkdir("/media/mmc1/irreco", 0777);

        if(!irreco_is_dir("/media/mmc1/irreco")) {
                gtk_widget_set_sensitive(self->radio1, FALSE);
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(self->radio2),
					     TRUE);

        }

        if(irreco_is_dir("/media/mmc2")) {

	        gtk_box_pack_start_defaults(GTK_BOX(
				    GTK_DIALOG(self)->vbox),
				    self->radio2);
        }




	gtk_widget_show_all(GTK_WIDGET(self));
	IRRECO_RETURN
}

static void
irreco_theme_save_dlg_init (IrrecoThemeSaveDlg *object)
{
	IRRECO_ENTER
	IRRECO_RETURN
}

static void
irreco_theme_save_dlg_finalize (GObject *object)
{
	/* TODO: Add deinitalization code here */
	IrrecoThemeSaveDlg *self;
	IRRECO_ENTER

	self = IRRECO_THEME_SAVE_DLG(object);
	G_OBJECT_CLASS (irreco_theme_save_dlg_parent_class)->finalize (object);
	IRRECO_RETURN
}

static void
irreco_theme_save_dlg_class_init (IrrecoThemeSaveDlgClass *klass)
{
	GObjectClass* object_class = G_OBJECT_CLASS (klass);
	object_class->finalize = irreco_theme_save_dlg_finalize;
	object_class->constructed = irreco_theme_save_dlg_constructed;
}

GtkWidget *
irreco_theme_save_dlg_new(IrrecoData *irreco_data, GtkWindow *parent)
{
	IrrecoThemeSaveDlg *self;

	IRRECO_ENTER

	self = g_object_new(IRRECO_TYPE_THEME_SAVE_DLG,
			    "irreco-data", irreco_data, NULL);
	irreco_dlg_set_parent(IRRECO_DLG(self), parent);

	IRRECO_RETURN_PTR(self);
}

IrrecoThemeSaveDlg *
irreco_theme_save_dlg_create(IrrecoData *irreco_data, GtkWindow *parent_window)
{
	IRRECO_ENTER
	IRRECO_RETURN_PTR(irreco_theme_save_dlg_new(
			  irreco_data, parent_window));
}

/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/**
 * @name Private Functions
 * @{
 */

gboolean _save_theme_to_dir(IrrecoThemeSaveDlg *self)
{
	gboolean		rvalue = FALSE;
	GString			*theme_path;
	gchar			*folder	= NULL;
	IrrecoThemeManager	*theme_manager = self->irreco_data->theme_manager;
	IrrecoTheme		*same_theme = NULL;
	gint			delete_mode = 0;
	gchar			*same_theme_folder = NULL;
	GString			*message = g_string_new(NULL);
	GString			*theme_name;
	gchar			*redy_theme_name;
	IRRECO_ENTER

	/* Create Theme Folder */
	theme_path = g_string_new("");
	theme_name = g_string_new(self->theme->name->str);

	if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(self->radio1))) {
		folder = "/media/mmc1/irreco/";
	} else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(self->radio2))) {
		folder = "/media/mmc2/irreco/";
	} else {
		folder = IRRECO_THEME_DIR;
	}

	/* Call IrrecoThemeSave */
	if(!irreco_is_dir(folder)) {
		g_mkdir(folder, 0777);
	} else {

	}

	/* parse the theme name */
	g_string_ascii_down(theme_name);
	redy_theme_name = g_strdup(theme_name->str);
	g_strdelimit(redy_theme_name, " ", '_');
	g_strdelimit(redy_theme_name, ",-|> <.", 'a');
	g_string_printf(theme_path, "%s%s", folder, redy_theme_name);

	IRRECO_STRING_TABLE_FOREACH_DATA(theme_manager->themes,
						 IrrecoTheme *, theme)
		/*Check theme name*/
		if (g_str_equal(self->theme->name->str,
		    theme->name->str)) {
			if (g_str_equal(theme->source->str, "deb")) {
				g_string_printf(message,
				_("The \"%s\" theme already exists.\n"
				"Can't overwrite \"Built In themes\"\n"
				"Please remove it first from the\n"
				"Application Manager or change the name of the theme"),
				theme->name->str);
				irreco_error_dlg(GTK_WINDOW(self),
						 message->str);
				goto end;
			}
			delete_mode += 1;
			same_theme = theme;
		}
		/*Check theme folder path*/
		else if (g_str_equal(theme_path->str,
			 g_strconcat(theme->path->str, "/", NULL))){
			if (g_str_equal(theme->source->str, "deb")) {
				g_string_printf(message,
				_("This theme replaces \"%s\" theme.\n"
				"Can't overwrite \"Built In themes\"\n"
				"Please remove it first from the\n"
				"Application Manager or change the name of the theme"),
				theme->name->str);

				irreco_error_dlg(GTK_WINDOW(self),
						 message->str);
				goto end;
			}
			delete_mode += 2;
			same_theme_folder = g_strdup(theme->name->str);
		}
	IRRECO_STRING_TABLE_FOREACH_END

	if (delete_mode == 1) {
		g_string_printf(message,
		_("The \"%s\" theme already exists.\n"
		"Do you want to edit this theme?"),
		same_theme->name->str);
	} else if (delete_mode == 2) {
		g_string_printf(message,
		_("This theme replaces \"%s\" theme.\n"
		"Do you want to continue?"),
       		same_theme_folder);
	} else if (delete_mode == 3) {
		g_string_printf(message,
		_("This theme replaces themes\n"
		"\"%s\" and \"%s\"\n"
		"Do you want to continue?"),
		same_theme->name->str,
		same_theme_folder);
	}
	/* Check whether a theme folder already exists */
	if (delete_mode != 0) {
		/* Create dialog*/
		if (!irreco_yes_no_dlg(
		    GTK_WINDOW(self), message->str)) {
			goto end;
			/*continue;*/
		} else {
			/* Check and remove theme folder*/
			if (delete_mode == 1 || delete_mode == 3) {
				gchar *rm_cmd;
				rm_cmd = g_strconcat("rm -r ",
						     "/media/mmc1/irreco/",
	   					     redy_theme_name, NULL);
				if (irreco_is_dir(g_strconcat(
				    "/media/mmc1/irreco/",
				    redy_theme_name, NULL))) {
					system(rm_cmd);
				}
				rm_cmd = g_strconcat("rm -r ",
						     "/media/mmc2/irreco/",
	   					     redy_theme_name, NULL);
				if (irreco_is_dir(g_strconcat(
				    "/media/mmc2/irreco/",
				    redy_theme_name, NULL))) {
					system(rm_cmd);
				}
				g_free(rm_cmd);
			}
			if (delete_mode == 2 || delete_mode == 3) {
				irreco_theme_manager_remove_theme(
						self->irreco_data->theme_manager,
      						same_theme_folder);
				g_free(same_theme_folder);
			}
		}
	}
	/*Create folder */
	g_mkdir(theme_path->str, 0777);
	/*Create new theme*/

	irreco_theme_save(self->theme, theme_path->str);

	rvalue = TRUE;

	end:
	g_string_free(theme_name, TRUE);
	g_string_free(message, TRUE);
	g_string_free(theme_path, TRUE);

	IRRECO_RETURN_BOOL(rvalue);
}

/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Functions                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Public Functions                                                           */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

gboolean
irreco_theme_save_dlg_run(IrrecoData *irreco_data, IrrecoTheme *irreco_theme,
			  GtkWindow *parent_window)
{
	IrrecoThemeSaveDlg		*self;
	gint				response;
	gboolean			loop = TRUE;
	gboolean			rvalue = FALSE;

	IRRECO_ENTER

	self = (IrrecoThemeSaveDlg*)irreco_theme_save_dlg_create(
		irreco_data, parent_window);
	self->irreco_data = irreco_data;
	self->theme = irreco_theme;

	do {
		response = gtk_dialog_run(GTK_DIALOG(self));
		switch (response) {
		case GTK_RESPONSE_OK:

			if (_save_theme_to_dir(self)) {
				loop = FALSE;
				rvalue = TRUE;
			} else {
				loop = TRUE;
			}

			break;
		case GTK_RESPONSE_DELETE_EVENT:
			IRRECO_DEBUG("GTK_RESPONSE_DELETE_EVENT\n");
			rvalue = FALSE;
			loop = FALSE;
			break;

		default:
			IRRECO_DEBUG("default\n");
			break;
		}

	} while (loop);

	gtk_widget_destroy(GTK_WIDGET(self));

	IRRECO_RETURN_BOOL(rvalue);
}



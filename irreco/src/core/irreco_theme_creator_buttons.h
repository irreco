/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
* irreco - Ir Remote Control
 * Copyright (C) 2008 Pekka Gehör (pegu6@msn.com)
 *
 * irreco_theme_creator_buttons.c is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * irreco_theme_creator_buttons.c is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @addtogroup IrrecoThemeCreatorButtons
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoThemeCreatorButtons.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_THEME_CREATOR_BUTTONS_H_TYPEDEF__
#define __IRRECO_THEME_CREATOR_BUTTONS_H_TYPEDEF__

typedef struct _IrrecoThemeCreatorButtonsClass IrrecoThemeCreatorButtonsClass;
typedef struct _IrrecoThemeCreatorButtons IrrecoThemeCreatorButtons;

#define IRRECO_TYPE_THEME_CREATOR_BUTTONS             (irreco_theme_creator_buttons_get_type ())
#define IRRECO_THEME_CREATOR_BUTTONS(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), IRRECO_TYPE_THEME_CREATOR_BUTTONS, IrrecoThemeCreatorButtons))
#define IRRECO_THEME_CREATOR_BUTTONS_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), IRRECO_TYPE_THEME_CREATOR_BUTTONS, IrrecoThemeCreatorButtonsClass))
#define IRRECO_IS_THEME_CREATOR_BUTTONS(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), IRRECO_TYPE_THEME_CREATOR_BUTTONS))
#define IRRECO_IS_THEME_CREATOR_BUTTONS_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), IRRECO_TYPE_THEME_CREATOR_BUTTONS))
#define IRRECO_THEME_CREATOR_BUTTONS_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), IRRECO_TYPE_THEME_CREATOR_BUTTONS, IrrecoThemeCreatorButtonsClass))


#endif /* __IRRECO_THEME_CREATOR_BUTTONS_H_TYPEDEF__ */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef _IRRECO_THEME_CREATOR_BUTTONS_H_TYPEDEF_
#define _IRRECO_THEME_CREATOR_BUTTONS_H_TYPEDEF_
#include "irreco.h"
#include "irreco_internal_widget.h"


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

struct _IrrecoThemeCreatorButtons
{
	IrrecoInternalWidget		parent;
	GtkWindow 			*parent_window;
	IrrecoTheme			*theme;
	IrrecoData			*irreco_data;

	/*Buttons widgets*/
	GtkWidget			*vbox_buttons;
	GtkWidget			*scroll_buttons;
	GtkCellRenderer			*renderer_buttons;
	GtkTreeViewColumn		*column_buttons;
	GtkListStore			*store_buttons;
/*	GtkTreeIter 			*iter_buttons;*/
	GtkWidget			*view_buttons;

	gint				loader_index;
	gint				loader_state;
	gint				loader_func_id;
	GtkTreeIter			*loader_iter;

	gint 				sel_index;
	GtkTreeSelection		*tree_selection;
	IrrecoThemeButton		*current_button;

};

struct _IrrecoThemeCreatorButtonsClass
{
	IrrecoInternalWidgetClass parent_class;
};

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

GType irreco_theme_creator_buttons_get_type (void);
GtkWidget* irreco_theme_creator_buttons_new(GtkWindow *parent,
					    IrrecoData *irreco_data,
	 				    IrrecoTheme * irreco_theme);
void
irreco_theme_creator_buttons_set_parent_window(IrrecoThemeCreatorButtons *self,
					       GtkWindow *parent);
IrrecoThemeButton
*irreco_theme_creator_buttons_get_selected_button(IrrecoThemeCreatorButtons
						  *self);
gboolean
irreco_theme_creator_buttons_remove_selected(IrrecoThemeCreatorButtons
					     *self);
void
irreco_theme_creator_buttons_refresh(IrrecoThemeCreatorButtons *self);

#endif /* __IRRECO_THEME_CREATOR_BUTTONS_H__ */

/** @} */

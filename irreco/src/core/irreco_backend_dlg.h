/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoBackendDlg
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoBackendDlg.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_BACKEND_DLG_H_TYPEDEF__
#define __IRRECO_BACKEND_DLG_H_TYPEDEF__

#define IRRECO_TYPE_BACKEND_DLG irreco_backend_dlg_get_type()
#define IRRECO_BACKEND_DLG(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  IRRECO_TYPE_BACKEND_DLG, IrrecoBackendDlg))
#define IRRECO_BACKEND_DLG_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  IRRECO_TYPE_BACKEND_DLG, IrrecoBackendDlgClass))
#define IRRECO_IS_BACKEND_DLG(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  IRRECO_TYPE_BACKEND_DLG))
#define IRRECO_IS_BACKEND_DLG_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  IRRECO_TYPE_BACKEND_DLG))
#define IRRECO_BACKEND_DLG_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  IRRECO_TYPE_BACKEND_DLG, IrrecoBackendDlgClass))

typedef struct _IrrecoBackendDlg IrrecoBackendDlg;
typedef struct _IrrecoBackendDlgClass IrrecoBackendDlgClass;

#endif /* __IRRECO_BACKEND_DLG_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_BACKEND_DLG_H__
#define __IRRECO_BACKEND_DLG_H__
#include "irreco.h"
#include "irreco_dlg.h"
#include "irreco_data.h"
#include "irreco_listbox_text.h"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

struct _IrrecoBackendDlg {
	IrrecoDlg parent;

	IrrecoData 	*irreco_data;
	GtkWidget	*listbox;
};

struct _IrrecoBackendDlgClass {
	IrrecoDlgClass parent_class;
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
GType		irreco_backend_dlg_get_type(void);
GtkWidget *	irreco_backend_dlg_new(IrrecoData *irreco_data,
				       GtkWindow *parent);
void 		irreco_show_backend_dlg(IrrecoData *irreco_data,
					GtkWindow *parent);
void 		irreco_backend_dlg_set_irreco_data(IrrecoBackendDlg *self,
						   IrrecoData *irreco_data);


#endif /* __IRRECO_BACKEND_DLG_H__ */

/** @} */

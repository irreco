/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *		       Harri Vattulainen (t5vaha01@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

 /**
 * @addtogroup IrrecoButtonLayout
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoButtonLayout.
 */

/*
 * DO NOT INCLUDE THIS FILE, INCLUDE BUTTON.H INSTEAD.
 */
#ifndef __IRRECO_BUTTON_H__
#include "irreco_button.h"
#endif



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_BUTTON_LAYOUT_H__
#define __IRRECO_BUTTON_LAYOUT_H__

extern const gchar *irreco_button_layout_default_image;
extern const GdkColor irreco_button_layout_default_color;

/*
 * Button layout data.
 *
 * Used mainly to store and manage an array of buttons.
 */
struct _IrrecoButtonLayout
{
	/* Layout name. */
	GString				*name;
	IrrecoButtonLayoutBgType	background_type;
	GString				*background_image;
	GdkColor			background_color;
	IrrecoHardkeyMap		*hardkey_map;
	GString				*filename;

	/* Widget area. */
	GtkWidget *container;
	gint width;
	gint height;

	/* Contains all IrrecoButton structures. */
	GPtrArray *button_array;

	/* Button event callbacks. */
	IrrecoButtonCallback button_motion_callback;
	IrrecoButtonCallback button_press_callback;
	IrrecoButtonCallback button_release_callback;
	gpointer callback_user_data;
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#define irreco_button_layout_set_motion_callback(layout, callback) \
	layout->button_motion_callback = IRRECO_BUTTON_CALLBACK(callback)
#define irreco_button_layout_set_press_callback(layout, callback) \
	layout->button_press_callback = IRRECO_BUTTON_CALLBACK(callback)
#define irreco_button_layout_set_release_callback(layout, callback) \
	layout->button_release_callback = IRRECO_BUTTON_CALLBACK(callback)
#define irreco_button_layout_set_callback_data(layout, data) \
	layout->callback_user_data = data;
#define irreco_button_layout_get_callback_data(layout) \
	((layout)->callback_user_data)
#define irreco_button_layout_get_name(layout) \
	((layout)->name->str)
#define irreco_button_layout_get_bg_type(layout) \
	((layout)->background_type)
#define irreco_button_layout_get_bg_color(layout) \
	(&(layout)->background_color)



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
IrrecoButtonLayout *irreco_button_layout_create(GtkWidget * container,
						IrrecoCmdChainManager *manager);
void irreco_button_layout_destroy(IrrecoButtonLayout * irreco_layout);
void irreco_button_layout_set_filename(IrrecoButtonLayout * irreco_layout,
				       const gchar *filename);
void irreco_button_layout_set_name(IrrecoButtonLayout * irreco_layout,
				   const gchar * name);
void irreco_button_layout_set_bg_type(IrrecoButtonLayout * irreco_layout,
				      IrrecoButtonLayoutBgType type);
void irreco_button_layout_set_bg_image(IrrecoButtonLayout * irreco_layout,
				       const gchar * background_image);
const gchar *
irreco_button_layout_get_bg_image(IrrecoButtonLayout * irreco_layout);
void irreco_button_layout_set_bg_color(IrrecoButtonLayout * irreco_layout,
				       const GdkColor * color);
void irreco_button_layout_get_bg(IrrecoButtonLayout * irreco_layout,
				 const gchar ** image,
				 const GdkColor ** color);
void irreco_button_layout_set_size(IrrecoButtonLayout * irreco_layout,
				   gint width, gint height);
void irreco_button_layout_set_container(IrrecoButtonLayout * irreco_layout,
					GtkWidget * container);
void irreco_button_layout_container_put(IrrecoButtonLayout * irreco_layout,
					GtkWidget *child_widget,
					gint x, gint y);
void irreco_button_layout_container_move(IrrecoButtonLayout * irreco_layout,
					 GtkWidget *child_widget,
					 gint x, gint y);
void irreco_button_layout_reset(IrrecoButtonLayout * irreco_layout);
void irreco_button_layout_destroy_buttons(IrrecoButtonLayout * irreco_layout);
void irreco_button_layout_destroy_widgets(IrrecoButtonLayout * irreco_layout);
void irreco_button_layout_create_widgets(IrrecoButtonLayout * irreco_layout);
void irreco_button_layout_validate_positions(IrrecoButtonLayout * irreco_layout);
void irreco_button_layout_reindex(IrrecoButtonLayout * irreco_layout);
void irreco_button_layout_buttons_up(IrrecoButtonLayout * irreco_layout);



#endif /* __IRRECO_BUTTON_LAYOUT_H__ */

/** @} */

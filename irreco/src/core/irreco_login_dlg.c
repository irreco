/*
 * irreco - Ir Remote Control
 * Copyright (C) 2008 Sami Mäki (kasmra@xob.kapsi.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_login_dlg.h"
#include "irreco_webdb_register_dlg.h"

/* Include the prototypes for GConf client functions. */
#include <gconf/gconf-client.h>

/**
 * @addtogroup IrrecoLoginDlg
 * @ingroup Irreco
 *
 * Allow user to login into Irreco WebDB.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoLoginDlg.
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

#define APP_NAME "irreco"
#define GC_ROOT  "/apps/Maemo/" APP_NAME "/"

/** Button response codes. */
enum {
	IRRECO_LOGIN_REGISTER
};


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

/* Add functions here. */

G_DEFINE_TYPE (IrrecoLoginDlg, irreco_login_dlg, IRRECO_TYPE_DLG)

static void irreco_login_dlg_dispose (GObject *object)
{
        if (G_OBJECT_CLASS (irreco_login_dlg_parent_class)->dispose)
                G_OBJECT_CLASS (
                irreco_login_dlg_parent_class)->dispose (object);
}

static void irreco_login_dlg_finalize (GObject *object)
{
        if (G_OBJECT_CLASS (irreco_login_dlg_parent_class)->finalize)
                G_OBJECT_CLASS (
                irreco_login_dlg_parent_class)->finalize (object);
}

static void irreco_login_dlg_class_init (IrrecoLoginDlgClass *klass)
{
        GObjectClass *object_class = G_OBJECT_CLASS (klass);

        object_class->dispose = irreco_login_dlg_dispose;
        object_class->finalize = irreco_login_dlg_finalize;
}

static void irreco_login_dlg_init (IrrecoLoginDlg *self)
{
	GtkWidget *table;
	GtkWidget *label_user;
	GtkWidget *label_password;
	GtkWidget *label_empty;
	GtkWidget *align;
	IRRECO_ENTER

	/* Build the dialog */
	gtk_window_set_title(GTK_WINDOW(self), _("Login to IrrecoDB"));
	gtk_window_set_modal(GTK_WINDOW(self), TRUE);
	gtk_window_set_destroy_with_parent(GTK_WINDOW(self), TRUE);
	gtk_dialog_set_has_separator(GTK_DIALOG(self), FALSE);
	gtk_dialog_add_buttons(GTK_DIALOG(self),
			       "Register", IRRECO_LOGIN_REGISTER,
			       GTK_STOCK_OK, GTK_RESPONSE_OK,
			       NULL);

	table = gtk_table_new(4, 2, FALSE);

	label_user = gtk_label_new("Nickname: ");
	label_password = gtk_label_new("Password: ");
	label_empty = gtk_label_new("");
	self->check_password = gtk_check_button_new_with_label(
			"Remember password");
	gtk_table_attach_defaults(GTK_TABLE(table), label_user, 0, 1, 0, 1);
	gtk_table_attach_defaults(GTK_TABLE(table), label_password, 0, 1, 1, 2);
	gtk_table_attach(GTK_TABLE(table), label_empty, 0, 2, 2, 3, GTK_EXPAND,
			GTK_EXPAND, 0, 10);
	gtk_table_attach(GTK_TABLE(table), self->check_password, 0, 2, 2, 3,
			GTK_FILL, GTK_SHRINK, 0, 0);

	gtk_misc_set_alignment(GTK_MISC(label_user), 0, 0.5);
	gtk_misc_set_alignment(GTK_MISC(label_password), 0, 0.5);

        self->entry_user = gtk_entry_new();
	self->entry_password = gtk_entry_new();
	gtk_entry_set_visibility(GTK_ENTRY(self->entry_password), FALSE);

	/* Get data from entries */
	self->user = gtk_entry_get_text(GTK_ENTRY(self->entry_user));
	self->password = gtk_entry_get_text(GTK_ENTRY(self->entry_password));

	gtk_table_attach(GTK_TABLE(table), self->entry_user, 1, 2, 0, 1,
			 GTK_FILL, GTK_FILL, 0, 0);
	gtk_table_attach(GTK_TABLE(table), self->entry_password, 1, 2, 1, 2,
			 GTK_FILL, GTK_FILL, 0, 0);

	align = gtk_alignment_new(0.5, 0.5, 1, 1);
	gtk_alignment_set_padding(GTK_ALIGNMENT(align), 12, 12, 12, 12);
	gtk_container_add(GTK_CONTAINER(align), GTK_WIDGET(table));
	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(self)->vbox),
		GTK_WIDGET(align));

	g_signal_connect(G_OBJECT(GTK_TOGGLE_BUTTON(self->check_password)),
			"toggled",
			G_CALLBACK(irreco_login_dlg_remember_password), self);

	g_signal_connect(G_OBJECT(GTK_ENTRY(self->entry_password)), "backspace",
                     G_CALLBACK(irreco_login_dlg_passwd_entry_messed_with),
				self);

	gtk_widget_show_all(GTK_WIDGET(self));

        IRRECO_RETURN
}

void irreco_login_dlg_store_user(const gchar *default_user)
{
        GConfClient* gcClient = NULL;
        IRRECO_ENTER

        g_assert(default_user);

        gcClient = gconf_client_get_default();

        g_assert(GCONF_IS_CLIENT(gcClient));

        if (!gconf_client_set_string(gcClient, GC_ROOT "prev_user",
				default_user, NULL)) {
                IRRECO_PRINTF("Error.\n");
        }

        g_object_unref(gcClient);
        gcClient = NULL;

        IRRECO_RETURN
}

const gchar* irreco_login_dlg_get_stored_user()
{
        GConfClient *gcClient = NULL;
        GConfValue *val = NULL;
        const gchar *previous_user;
        GString *prev_user;
        IRRECO_ENTER

        gcClient = gconf_client_get_default();
        g_assert(GCONF_IS_CLIENT(gcClient));

        val = gconf_client_get_without_default(gcClient, GC_ROOT
                        "prev_user", NULL);
	if (val == NULL) {
		IRRECO_PRINTF("prev_user not found\n");
		g_object_unref(gcClient);
		gcClient = NULL;
		IRRECO_RETURN_STR("");
	}

        if (val->type == GCONF_VALUE_STRING) {
                previous_user = gconf_value_get_string(val);
        } else {
                IRRECO_PRINTF("prev_user is not a string\n");
		g_object_unref(gcClient);
		gcClient = NULL;
                IRRECO_RETURN_STR("");
        }

        prev_user = g_string_new(previous_user);

        gconf_value_free(val);
        val = NULL;
        g_object_unref(gcClient);
        gcClient = NULL;

        IRRECO_RETURN_STR(prev_user->str);
}

void irreco_login_dlg_store_password(const gchar *default_password)
{
        GConfClient* gcClient = NULL;
        IRRECO_ENTER

        g_assert(default_password);

        gcClient = gconf_client_get_default();

        g_assert(GCONF_IS_CLIENT(gcClient));

        if (!gconf_client_set_string(gcClient, GC_ROOT "prev_password",
				default_password, NULL)) {
                IRRECO_PRINTF("Error.\n");
        }

        g_object_unref(gcClient);
        gcClient = NULL;

        IRRECO_RETURN
}

const gchar* irreco_login_dlg_get_stored_password()
{
        GConfClient *gcClient = NULL;
        GConfValue *val = NULL;
        const gchar *previous_password;
        GString *prev_password;
        IRRECO_ENTER

        gcClient = gconf_client_get_default();
        g_assert(GCONF_IS_CLIENT(gcClient));

        val = gconf_client_get_without_default(gcClient, GC_ROOT
                        "prev_password", NULL);
        if (val == NULL) {
                IRRECO_PRINTF("prev_password not found\n");
		g_object_unref(gcClient);
		gcClient = NULL;
                IRRECO_RETURN_STR("");
        }

        if (val->type == GCONF_VALUE_STRING) {
                previous_password = gconf_value_get_string(val);
        } else {
                IRRECO_PRINTF("prev_password is not a string\n");
		g_object_unref(gcClient);
		gcClient = NULL;
                IRRECO_RETURN_STR("");
        }

        prev_password = g_string_new(previous_password);

        gconf_value_free(val);
        val = NULL;
        g_object_unref(gcClient);
        gcClient = NULL;

        IRRECO_RETURN_STR(prev_password->str);
}

void irreco_login_dlg_store_toggle_boolean(const gchar *default_boolean)
{
        GConfClient* gcClient = NULL;
        IRRECO_ENTER

        g_assert(default_boolean);

        gcClient = gconf_client_get_default();

        g_assert(GCONF_IS_CLIENT(gcClient));

        if (!gconf_client_set_string(gcClient, GC_ROOT "prev_boolean",
				default_boolean, NULL)) {
                IRRECO_PRINTF("Error.\n");
        }

        g_object_unref(gcClient);
        gcClient = NULL;

        IRRECO_RETURN
}

const gchar* irreco_login_dlg_get_stored_toggle_boolean()
{
        GConfClient *gcClient = NULL;
        GConfValue *val = NULL;
        const gchar *previous_boolean;
        GString *prev_boolean;
        IRRECO_ENTER

        gcClient = gconf_client_get_default();
        g_assert(GCONF_IS_CLIENT(gcClient));

        val = gconf_client_get_without_default(gcClient, GC_ROOT
                        "prev_boolean", NULL);
        if (val == NULL) {
                IRRECO_PRINTF("prev_boolean not found\n");
		g_object_unref(gcClient);
		gcClient = NULL;
                IRRECO_RETURN_STR("");
        }

        if (val->type == GCONF_VALUE_STRING) {
                previous_boolean = gconf_value_get_string(val);
        } else {
                IRRECO_PRINTF("prev_boolean is not a string\n");
		g_object_unref(gcClient);
		gcClient = NULL;
                IRRECO_RETURN_STR("");
        }

        prev_boolean = g_string_new(previous_boolean);

        gconf_value_free(val);
        val = NULL;
        g_object_unref(gcClient);
        gcClient = NULL;

        IRRECO_RETURN_STR(prev_boolean->str);
}

void irreco_login_dlg_remember_password(GtkCheckButton *toggle_button,
		IrrecoLoginDlg *self)
{
	IRRECO_ENTER

	if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(toggle_button))) {
		irreco_login_dlg_store_toggle_boolean("TRUE");
	} else {
		irreco_login_dlg_store_toggle_boolean("FALSE");
		gtk_entry_set_text(GTK_ENTRY(self->entry_password), "");
	}

	IRRECO_RETURN
}

void irreco_login_dlg_passwd_entry_messed_with(GtkEntry *pw_entry,
		IrrecoLoginDlg *login_dlg)
{
	IRRECO_ENTER

	login_dlg->is_pw_entry_messed_with = TRUE;

	IRRECO_RETURN
}

gboolean irreco_login_dlg_login(IrrecoLoginDlg *self)
{
	const gchar *pwhash;
	const gchar *tmp_pwhash;
	const gchar *is_toggled;
	const gchar *stored_user;
	const gchar *stored_pw;
	const gchar *star_hash;
	IRRECO_ENTER

	/* TODO Make this horrible bit of code better FIXME */
	star_hash = g_compute_checksum_for_string(G_CHECKSUM_SHA1, "******", -1);

	is_toggled = irreco_login_dlg_get_stored_toggle_boolean();

	stored_user = irreco_login_dlg_get_stored_user();
	stored_pw = irreco_login_dlg_get_stored_password();

	if(g_str_equal(is_toggled, "TRUE") &&
			g_str_equal(stored_user, self->user) &&
			!g_str_equal(stored_pw, "") &&
			!self->is_pw_entry_messed_with &&
            (strlen(self->password) > 1)) {

	tmp_pwhash = g_compute_checksum_for_string(G_CHECKSUM_SHA1,
							self->password,
							-1);

		if(g_str_equal(tmp_pwhash, stored_pw)) {
			pwhash = irreco_login_dlg_get_stored_password();
			}
		else if(g_str_equal(tmp_pwhash, star_hash)) {
			pwhash = irreco_login_dlg_get_stored_password();
		}
        else {
            goto checkout;
        }
	} else {
checkout:
		/* Check some things */
		if(strlen(self->user) < 6) {
			irreco_error_dlg(GTK_WINDOW(self),
					"Invalid user name.");
			IRRECO_RETURN_BOOL(FALSE);
		}

		if(strlen(self->password) < 6) {
			irreco_error_dlg(GTK_WINDOW(self),
					"Invalid password.");
			IRRECO_RETURN_BOOL(FALSE);
		} else {
			pwhash = g_compute_checksum_for_string(
					G_CHECKSUM_SHA1, self->password, -1);
		}
	}

	/* Login to webdb */
	if(irreco_login_dlg_login_cache(self, pwhash)) {

                /* Save "default" username for the next time */
                irreco_login_dlg_store_user(self->user);

		if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(
						self->check_password))) {
			irreco_login_dlg_store_password(pwhash);
			irreco_login_dlg_store_toggle_boolean("TRUE");
		} else {
			irreco_login_dlg_store_toggle_boolean("FALSE");
		}
                /* Get username and password hash */
                self->userptr = g_strdup(self->user);
                self->passwdptr = g_strdup(pwhash);

		IRRECO_RETURN_BOOL(TRUE);
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
}

gboolean irreco_login_dlg_login_cache(
		IrrecoLoginDlg *self, const gchar *pwhash)
{
	IrrecoWebdbCache *webdb_cache = NULL;
	IRRECO_ENTER

	webdb_cache = irreco_data_get_webdb_cache(self->irreco_data, FALSE);

 	if(irreco_webdb_cache_login(webdb_cache, self->user, pwhash)) {
		IRRECO_RETURN_BOOL(TRUE);
	} else {
		irreco_error_dlg(GTK_WINDOW(self),
				irreco_webdb_cache_get_error(webdb_cache));
		IRRECO_RETURN_BOOL(FALSE);
	}
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Private Functions
 * @{
 */

/* Add functions here. */

IrrecoLoginDlg* irreco_login_dlg_new (IrrecoData *irreco_data,
                GtkWindow *parent)
{
        IrrecoLoginDlg *self;
        IRRECO_ENTER

        self = g_object_new(IRRECO_TYPE_LOGIN_DLG, NULL);
        irreco_dlg_set_parent(IRRECO_DLG(self), parent);
        self->irreco_data = irreco_data;

        IRRECO_RETURN_PTR(self);
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Public Functions                                                           */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */

/* Add functions here. */

gboolean irreco_show_login_dlg(IrrecoData *irreco_data, GtkWindow *parent,
				gchar **userptr, gchar **passwdptr)
{
	IrrecoLoginDlg *self;
	gint response;
	gboolean loop = TRUE;
        gboolean rvalue = TRUE;
        const gchar *temp_user;
	const gchar *is_toggled;

	IRRECO_ENTER

	/* Create login dialog */
        self = g_object_new(IRRECO_TYPE_LOGIN_DLG, NULL);
        irreco_dlg_set_parent(IRRECO_DLG(self), parent);
        self->irreco_data = irreco_data;

        /* Previously used username to text entry */
        temp_user = irreco_login_dlg_get_stored_user();
	is_toggled = irreco_login_dlg_get_stored_toggle_boolean();
	self->is_pw_entry_messed_with = FALSE;

	if(g_str_equal(is_toggled, "TRUE")) {
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(
					self->check_password), TRUE);
                gtk_entry_set_text(GTK_ENTRY(self->entry_password), "******");
	}

        if(temp_user != NULL) {
                gtk_entry_set_text(GTK_ENTRY(self->entry_user), temp_user);
        }

	do {
		/* Show login dlg */
		response = gtk_dialog_run(GTK_DIALOG(self));

		switch(response) {
                        case IRRECO_LOGIN_REGISTER:
                                irreco_show_webdb_register_dlg(
                                                self->irreco_data, GTK_WINDOW(
                                                        self));
                                break;
			case GTK_RESPONSE_DELETE_EVENT:
				is_toggled = irreco_login_dlg_get_stored_toggle_boolean();

				if(g_str_equal(is_toggled, "FALSE")) {
					gtk_entry_set_text(GTK_ENTRY
						(self->entry_password), "");
					irreco_login_dlg_store_password("");
				}

                                loop = FALSE;
                                rvalue = FALSE;
                                break;
                        case GTK_RESPONSE_OK:

                                if(irreco_login_dlg_login(self)) {
                                        /* Get username and pw-hash */
                                        *userptr = g_strdup(self->userptr);
                                        *passwdptr = g_strdup(self->passwdptr);
					is_toggled = irreco_login_dlg_get_stored_toggle_boolean();

					if(g_str_equal(is_toggled, "FALSE")) {
						gtk_entry_set_text(GTK_ENTRY
							(self->entry_password), "");
						irreco_login_dlg_store_password("");
					}

                                        irreco_info_dlg(GTK_WINDOW(self),
                                                        "Login successful.");
                                        rvalue = TRUE;
                                }  else {
                                        rvalue = FALSE;
                                        break;
                                }

                                loop = FALSE;
                                break;
                        default:
                                break;
                }
	} while(loop);

	gtk_widget_destroy(GTK_WIDGET(self));
	IRRECO_RETURN_BOOL(rvalue);
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Events and Callbacks                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Events and Callbacks
 * @{
 */

/* Add functions here. */

/** @} */

/** @} */

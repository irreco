/*
 * irreco - Ir Remote Control
 * Copyright (C) 2008  Joni Kokko (t5kojo01@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoRemoteUploadDlg
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoRemoteUploadDlg.
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_REMOTE_UPLOAD_DLG_H_TYPEDEF__
#define __IRRECO_REMOTE_UPLOAD_DLG_H_TYPEDEF__

#define IRRECO_TYPE_REMOTE_UPLOAD_DLG irreco_remote_upload_dlg_get_type()

#define IRRECO_REMOTE_UPLOAD_DLG(obj) \
	(G_TYPE_CHECK_INSTANCE_CAST ((obj), \
	IRRECO_TYPE_REMOTE_UPLOAD_DLG, IrrecoRemoteUploadDlg))

#define IRRECO_REMOTE_UPLOAD_DLG_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_CAST ((klass), \
	IRRECO_TYPE_REMOTE_UPLOAD_DLG, IrrecoRemoteUploadDlgClass))

#define IRRECO_IS_REMOTE_UPLOAD_DLG(obj) \
	(G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
	IRRECO_TYPE_REMOTE_UPLOAD_DLG))

#define IRRECO_IS_REMOTE_UPLOAD_DLG_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_TYPE ((klass), IRRECO_TYPE_REMOTE_UPLOAD_DLG))

#define IRRECO_REMOTE_UPLOAD_DLG_GET_CLASS(obj) \
	(G_TYPE_INSTANCE_GET_CLASS ((obj), \
	IRRECO_TYPE_REMOTE_UPLOAD_DLG, IrrecoRemoteUploadDlgClass))

typedef struct _IrrecoRemoteUploadDlgClass IrrecoRemoteUploadDlgClass;
typedef struct _IrrecoRemoteUploadDlg IrrecoRemoteUploadDlg;
#endif /* __IRRECO_REMOTE_UPLOAD_DLG_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_REMOTE_UPLOAD_DLG_H__
#define __IRRECO_REMOTE_UPLOAD_DLG_H__
#include "irreco.h"
#include "irreco_data.h"
#include "irreco_dlg.h"
#include <glib-object.h>



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

struct _IrrecoRemoteUploadDlg {
	IrrecoDlg	parent;
	IrrecoData	*irreco_data;
	GtkWidget	*banner;
	gint		loader_func_id;
	gint		loader_state;
	GtkWidget	*scroll;
	gint		cursor_position;

	/* data fields */
	GtkWidget	*category;
	GtkWidget	*manufacturer;
	GtkWidget	*model;
        GtkWidget	*comment;

	/* login data */
	gchar		*user_name;
	gchar		*password;

	/* remote information */
	gint		remote_id;
	gboolean	remote_uploaded;
};

struct _IrrecoRemoteUploadDlgClass {
	IrrecoDlgClass parent_class;
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

GType irreco_remote_upload_dlg_get_type (void);

IrrecoRemoteUploadDlg* irreco_remote_upload_dlg_new(IrrecoData *irreco_data,
						    GtkWindow *parent);

void irreco_show_remote_upload_dlg(IrrecoData *irreco_data, GtkWindow *parent);


#endif /* __IRRECO_REMOTE_UPLOAD_DLG_H__ */

/** @} */

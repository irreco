/*
 * irreco - Ir Remote Control
 * Copyright (C) 2008  Joni Kokko (t5kojo01@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_remote_download_dlg.h"
#include "irreco_select_instance_dlg.h"
#include "irreco_theme_download_dlg.h"
#include <hildon/hildon-banner.h>

/**
 * @addtogroup IrrecoRemoteDownloadDlg
 * @ingroup Irreco
 *
 * @todo PURPOCE OF CLASS.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoRemoteDownloadDlg.
 */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/** Button responce codes. */
enum {
	IRRECO_REMOTE_REFRESH,
	IRRECO_REMOTE_DOWNLOAD
};

/** GtkTreeStore colums. */
enum
{
	TEXT_COL,
	FLAG_COL,
	DATA_COL,
	N_COLUMNS
};

/** Loader states. */
enum
{
	LOADER_STATE_INIT,
	LOADER_STATE_LOOP,
	LOADER_STATE_END,
	LOADER_STATE_CLEANUP
};

/** Row flags. */
enum
{
	ROW_CHILDREN_LOADED	= 1 << 1,
	ROW_TYPE_CATEGORY	= 1 << 2,
	ROW_TYPE_MANUFACTURER	= 1 << 3,
	ROW_TYPE_MODEL		= 1 << 4,
 	ROW_TYPE_CREATOR	= 1 << 5,
 	ROW_TYPE_REMOTE		= 1 << 6,
	ROW_TYPE_LOADING	= 1 << 7
};

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

static gboolean irreco_remote_download_dlg_map_event(
						IrrecoRemoteDownloadDlg *self,
						GdkEvent *event,
						gpointer user_data);
static void irreco_remote_download_dlg_destroy_event(
						IrrecoRemoteDownloadDlg *self,
						gpointer user_data);
static void irreco_remote_download_dlg_row_activated_event(
						GtkTreeView *tree_view,
						GtkTreePath *path,
						GtkTreeViewColumn *column,
						IrrecoRemoteDownloadDlg *self);
static void irreco_remote_download_dlg_row_expanded_event(
						GtkTreeView *tree_view,
						GtkTreeIter *iter,
						GtkTreePath *path,
						IrrecoRemoteDownloadDlg *self);
static void irreco_remote_download_dlg_row_collapsed_event(
						GtkTreeView *tree_view,
						GtkTreeIter *iter,
						GtkTreePath *path,
						IrrecoRemoteDownloadDlg *self);
static void irreco_remote_download_dlg_row_selected_event(GtkTreeSelection *sel,
						IrrecoRemoteDownloadDlg *self);

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

G_DEFINE_TYPE(IrrecoRemoteDownloadDlg, irreco_remote_download_dlg, IRRECO_TYPE_DLG)

static void irreco_remote_download_dlg_dispose(GObject *object)
{
	G_OBJECT_CLASS(
		irreco_remote_download_dlg_parent_class)->dispose(object);
}

static void irreco_remote_download_dlg_finalize(GObject *object)
{
	G_OBJECT_CLASS(
		irreco_remote_download_dlg_parent_class)->finalize(object);
}

static void irreco_remote_download_dlg_class_init(
					IrrecoRemoteDownloadDlgClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);


	object_class->dispose = irreco_remote_download_dlg_dispose;
	object_class->finalize = irreco_remote_download_dlg_finalize;
}

static void irreco_remote_download_dlg_init(IrrecoRemoteDownloadDlg *self)
{
	GtkWidget *hbox;
	PangoFontDescription *title_font;
	PangoFontDescription *initial_font;
	PangoFontDescription *menu_font;

	/* Remotes */
	GtkWidget *scrollbar;
	GtkTreeViewColumn *column;
	GtkCellRenderer *renderer;
	GtkTreeSelection *select;
	GtkWidget *tree_view_frame;
	GtkWidget *tree_view_hbox;

	/* Details */
	GtkWidget *help_hbox = NULL;
	GtkWidget *details_alignment;
	GtkWidget *details_frame;
	GtkWidget *details_table;
	GtkWidget *label;
	IRRECO_ENTER

	self->remote = NULL;

	/* Create fonts */
	initial_font = pango_font_description_from_string("Sans 11");
	menu_font = pango_font_description_from_string("Sans 13");
	title_font = pango_font_description_from_string("Sans Bold 12");

	/* Build the dialog */
	gtk_window_set_title(GTK_WINDOW(self), _("Download Remote"));
	gtk_window_set_modal(GTK_WINDOW(self), TRUE);
	gtk_window_set_destroy_with_parent(GTK_WINDOW(self), TRUE);
	gtk_dialog_set_has_separator(GTK_DIALOG(self), FALSE);
	gtk_dialog_add_buttons(GTK_DIALOG(self),
			       GTK_STOCK_REFRESH, IRRECO_REMOTE_REFRESH,
			       _("Download"), IRRECO_REMOTE_DOWNLOAD,
			       NULL);

	/* Create hbox */
	hbox = g_object_new(GTK_TYPE_HBOX, NULL);
	gtk_box_set_spacing(GTK_BOX(hbox), 8);
	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(self)->vbox),
			  irreco_gtk_align(GTK_WIDGET(hbox),
					   0, 0, 1, 1, 8, 8, 8, 8));

	/* Create tree_view_hbox */
	tree_view_hbox = g_object_new(GTK_TYPE_HBOX, NULL);

	/* Create Treeview */
	self->tree_store = gtk_tree_store_new(
		N_COLUMNS, G_TYPE_STRING, G_TYPE_INT, G_TYPE_POINTER);
	self->tree_view = GTK_TREE_VIEW(gtk_tree_view_new_with_model(
		GTK_TREE_MODEL(self->tree_store)));

	renderer = gtk_cell_renderer_text_new();
	g_object_set(renderer, "font-desc", menu_font, NULL);

	column = gtk_tree_view_column_new_with_attributes(
		NULL, renderer, "text", TEXT_COL, NULL);
	gtk_tree_view_append_column(self->tree_view, column);

	gtk_box_pack_start(GTK_BOX(tree_view_hbox), GTK_WIDGET(self->tree_view),
			   TRUE, TRUE, 0);

	/* Create scrollbar for Treeview */
	scrollbar = gtk_vscrollbar_new(gtk_tree_view_get_vadjustment(
					     GTK_TREE_VIEW(self->tree_view)));
	gtk_box_pack_start(GTK_BOX(tree_view_hbox), GTK_WIDGET(scrollbar),
			   FALSE, TRUE, 0);

	/* Create Frame for Treeview */
	tree_view_frame = gtk_frame_new("");
	gtk_frame_set_label_widget(GTK_FRAME(tree_view_frame),
		irreco_gtk_label_bold("Remotes", 0, 0, 0, 0, 0, 0));

	gtk_container_add(GTK_CONTAINER(tree_view_frame),
			  GTK_WIDGET(tree_view_hbox));

	gtk_box_pack_start(GTK_BOX(hbox), GTK_WIDGET(tree_view_frame),
			   TRUE, TRUE, 0);

	/* Create details_table */
	details_table = gtk_table_new(7, 4, FALSE);
	gtk_table_set_row_spacings(GTK_TABLE(details_table), 4);

	label = gtk_label_new("Model: ");
	gtk_widget_modify_font(label, title_font);
	gtk_misc_set_alignment(GTK_MISC(label), 0, 0);
	gtk_table_attach(GTK_TABLE(details_table), label,
			 0, 1, 0, 1, GTK_FILL | GTK_SHRINK, GTK_FILL, 0, 0);

	label = gtk_label_new("Creator: ");
	gtk_widget_modify_font(label, title_font);
	gtk_misc_set_alignment(GTK_MISC(label), 0, 0);
	gtk_table_attach(GTK_TABLE(details_table), label,
			 0, 1, 1, 2, GTK_FILL | GTK_SHRINK, GTK_FILL, 0, 0);

	label = gtk_label_new("Backends: ");
	gtk_widget_modify_font(label, title_font);
	gtk_misc_set_alignment(GTK_MISC(label), 0, 0);
	gtk_table_attach(GTK_TABLE(details_table), label,
			 0, 1, 2, 3, GTK_FILL | GTK_SHRINK, GTK_FILL, 0, 0);

	label = gtk_label_new("Themes: ");
	gtk_widget_modify_font(label, title_font);
	gtk_misc_set_alignment(GTK_MISC(label), 0, 0);
	gtk_table_attach(GTK_TABLE(details_table), label,
			 0, 1, 3, 4, GTK_FILL | GTK_SHRINK, GTK_FILL, 0, 0);

	label = gtk_label_new("Downloaded: ");
	gtk_widget_modify_font(label, title_font);
	gtk_misc_set_alignment(GTK_MISC(label), 0, 0);
	gtk_table_attach(GTK_TABLE(details_table), label,
			 0, 1, 4, 5, GTK_FILL | GTK_SHRINK, GTK_FILL, 0, 0);

	label = gtk_label_new("Comment: ");
	gtk_widget_modify_font(label, title_font);
	gtk_misc_set_alignment(GTK_MISC(label), 0, 0);
	gtk_table_attach(GTK_TABLE(details_table), label,
			 0, 1, 5, 6, GTK_FILL | GTK_SHRINK, GTK_FILL, 0, 0);

	self->details_model = gtk_label_new("");
	gtk_widget_modify_font(self->details_model, initial_font);
	gtk_misc_set_alignment(GTK_MISC(self->details_model), 0, 0);
	gtk_table_attach(GTK_TABLE(details_table), self->details_model,
			 1, 2, 0, 1, GTK_FILL, GTK_FILL, 0, 0);

	self->details_creator = gtk_label_new("");
	gtk_widget_modify_font(self->details_creator, initial_font);
	gtk_misc_set_alignment(GTK_MISC(self->details_creator), 0, 0);
	gtk_table_attach(GTK_TABLE(details_table), self->details_creator,
			 1, 2, 1, 2, GTK_FILL, GTK_FILL, 0, 0);

	self->details_backends = gtk_label_new("");
	gtk_widget_modify_font(self->details_backends, initial_font);
	gtk_misc_set_alignment(GTK_MISC(self->details_backends), 0, 0);
	gtk_table_attach(GTK_TABLE(details_table), self->details_backends,
			 1, 2, 2, 3, GTK_FILL, GTK_FILL, 0, 0);

	self->details_themes = gtk_label_new("");
	gtk_widget_modify_font(self->details_themes, initial_font);
	gtk_misc_set_alignment(GTK_MISC(self->details_themes), 0, 0);
	gtk_table_attach(GTK_TABLE(details_table), self->details_themes,
			 1, 2, 3, 4, GTK_FILL, GTK_FILL, 0, 0);

	self->details_downloaded = gtk_label_new("");
	gtk_widget_modify_font(self->details_downloaded, initial_font);
	gtk_misc_set_alignment(GTK_MISC(self->details_downloaded), 0, 0);
	gtk_table_attach(GTK_TABLE(details_table), self->details_downloaded,
			 1, 2, 4, 5, GTK_FILL, GTK_FILL, 0, 0);

	self->details_comment = gtk_text_view_new();
	gtk_widget_modify_font(self->details_comment, initial_font);
	gtk_misc_set_alignment(GTK_MISC(self->details_comment), 0, 0);
	gtk_text_view_set_cursor_visible(GTK_TEXT_VIEW(self->details_comment),
					 FALSE);
	gtk_text_view_set_editable(GTK_TEXT_VIEW(self->details_comment),
				   FALSE);
	gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(self->details_comment),
				    GTK_WRAP_WORD_CHAR);
	gtk_table_attach(GTK_TABLE(details_table), self->details_comment,
			 0, 4, 6, 7, GTK_FILL, GTK_FILL, 0, 0);
	gtk_widget_set_size_request(GTK_WIDGET(self->details_comment), 241, -1);

	/* Details */
	self->details = gtk_scrolled_window_new(NULL, NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(self->details),
				       GTK_POLICY_AUTOMATIC,
				       GTK_POLICY_AUTOMATIC);

	gtk_widget_set_size_request(GTK_WIDGET(self->details), 285, -1);

	/* Create frame for Details */
	details_frame = gtk_frame_new("");
	gtk_frame_set_label_widget(GTK_FRAME(details_frame),
			irreco_gtk_label_bold("Details", 0, 0, 0, 0, 0, 0));

	gtk_box_pack_start(GTK_BOX(hbox), GTK_WIDGET(details_frame),
			   FALSE, TRUE, 0);

	/* Create alignment */
	details_alignment = gtk_alignment_new(0, 0, 0, 0);
	gtk_alignment_set_padding(GTK_ALIGNMENT(details_alignment), 0, 8, 8, 8);

	gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(
					      self->details),
					      details_alignment);

	gtk_container_add(GTK_CONTAINER(details_alignment), details_table);

	/* Help text */
	self->help_text = gtk_label_new("Select Remote");
	gtk_label_set_justify(GTK_LABEL(self->help_text), GTK_JUSTIFY_CENTER);
	gtk_label_set_line_wrap(GTK_LABEL(self->help_text), TRUE);
	self->help_text = irreco_gtk_align(self->help_text,
					   0.5, 0.5, 1, 1, 8, 8, 8, 8);

	help_hbox = gtk_hbox_new(0, 0);

	gtk_box_pack_start(GTK_BOX(help_hbox), GTK_WIDGET(self->details),
			   TRUE, TRUE, 0);
	gtk_box_pack_start(GTK_BOX(help_hbox), GTK_WIDGET(self->help_text),
			   TRUE, TRUE, 0);

	gtk_container_add(GTK_CONTAINER(details_frame), GTK_WIDGET(help_hbox));

	/* Setup the selection handler for TREE	*/
	select = gtk_tree_view_get_selection(self->tree_view);
	gtk_tree_selection_set_mode(select, GTK_SELECTION_SINGLE);

	/* Signal handlers. */

	g_signal_connect(G_OBJECT(self), "map-event", G_CALLBACK(
			 irreco_remote_download_dlg_map_event), NULL);
	g_signal_connect(G_OBJECT(self), "destroy", G_CALLBACK(
			 irreco_remote_download_dlg_destroy_event), NULL);
	g_signal_connect(G_OBJECT(self->tree_view), "row-activated", G_CALLBACK(
			 irreco_remote_download_dlg_row_activated_event), self);
	g_signal_connect(G_OBJECT(self->tree_view), "row-expanded", G_CALLBACK(
			 irreco_remote_download_dlg_row_expanded_event), self);
	g_signal_connect(G_OBJECT(self->tree_view), "row-collapsed", G_CALLBACK(
			 irreco_remote_download_dlg_row_collapsed_event), self);
	g_signal_connect(G_OBJECT(select), "changed", G_CALLBACK(
			 irreco_remote_download_dlg_row_selected_event), self);

	gtk_tree_view_set_enable_tree_lines(self->tree_view, TRUE);
	g_object_set (G_OBJECT (self->tree_view), "show-expanders", TRUE, NULL);
	g_object_set (G_OBJECT (self->tree_view), "level-indentation", 0, NULL);
	gtk_tree_view_set_rubber_banding(self->tree_view, FALSE);

	gtk_widget_set_size_request(GTK_WIDGET(self), -1, 300);

	gtk_widget_show_all(GTK_WIDGET(self));
	gtk_widget_hide(GTK_WIDGET(self->details));

	IRRECO_RETURN
}

IrrecoRemoteDownloadDlg* irreco_remote_download_dlg_new(IrrecoData *irreco_data,
							GtkWindow *parent)
{
	IrrecoRemoteDownloadDlg *self;
	IRRECO_ENTER

	self = g_object_new(IRRECO_TYPE_REMOTE_DOWNLOAD_DLG, NULL);
	irreco_dlg_set_parent(IRRECO_DLG(self), parent);
	self->irreco_data = irreco_data;
	IRRECO_RETURN_PTR(self);
}


/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Private Functions
 * @{
 */

/**
 * Clean detail list
 *
 */
static void irreco_remote_download_dlg_clean_details(
						IrrecoRemoteDownloadDlg *self)
{
	GtkTextBuffer *buffer;
	IRRECO_ENTER

	self->remote = NULL;
	gtk_label_set_text(GTK_LABEL(self->details_model), "");
	gtk_label_set_text(GTK_LABEL(self->details_creator), "");
	gtk_label_set_text(GTK_LABEL(self->details_backends), "");
	gtk_label_set_text(GTK_LABEL(self->details_themes), "");
	gtk_label_set_text(GTK_LABEL(self->details_downloaded), "");
	buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(self->details_comment));
	gtk_text_buffer_set_text(buffer, " ", 1);
	gtk_text_view_set_buffer(GTK_TEXT_VIEW(self->details_comment), buffer);

	IRRECO_RETURN
}

/**
 * Have the childern of this item been loaded.
 *
 * @return TRUE if children have been loade, FALSE otherwise.
 */
static gboolean irreco_remote_download_dlg_row_is_loaded(
						IrrecoRemoteDownloadDlg *self,
						GtkTreeIter *iter)
{
	gint i;
	IRRECO_ENTER

	gtk_tree_model_get(GTK_TREE_MODEL(self->tree_store),
			   iter, FLAG_COL, &i, -1);
	if (i & ROW_CHILDREN_LOADED) IRRECO_RETURN_BOOL(TRUE);
	IRRECO_RETURN_BOOL(FALSE);
}

/**
 * Enable / Disable ROW_CHILDREN_LOADED flag from a row.
 *
 * @param value If set, ROW_CHILDREN_LOADED will be enabled.
 */
static void irreco_remote_download_dlg_row_set_loaded(IrrecoRemoteDownloadDlg *self,
					    GtkTreeIter *iter,
					    gboolean value)
{
	gint i;
	IRRECO_ENTER

	gtk_tree_model_get(GTK_TREE_MODEL(self->tree_store),
			   iter, FLAG_COL, &i, -1);
	if (value) {
		i = i | ROW_CHILDREN_LOADED;
	} else {
		i = i & ~ROW_CHILDREN_LOADED;
	}
	gtk_tree_store_set(self->tree_store, iter, FLAG_COL, i, -1);

	IRRECO_RETURN
}

/**
 * Get type of row.
 */
static gint irreco_remote_download_dlg_row_get_type(
						IrrecoRemoteDownloadDlg *self,
						GtkTreeIter *iter)
{
	gint i;
	IRRECO_ENTER

	gtk_tree_model_get(GTK_TREE_MODEL(self->tree_store),
			   iter, FLAG_COL, &i, -1);
	if (i & ROW_TYPE_CATEGORY)
		IRRECO_RETURN_ENUM(ROW_TYPE_CATEGORY);
	if (i & ROW_TYPE_MANUFACTURER)
		IRRECO_RETURN_ENUM(ROW_TYPE_MANUFACTURER);
	if (i & ROW_TYPE_MODEL)
		IRRECO_RETURN_ENUM(ROW_TYPE_MODEL);
	if (i & ROW_TYPE_CREATOR)
		IRRECO_RETURN_ENUM(ROW_TYPE_CREATOR);
	if (i & ROW_TYPE_REMOTE)
		IRRECO_RETURN_ENUM(ROW_TYPE_REMOTE);
	if (i & ROW_TYPE_LOADING)
		IRRECO_RETURN_ENUM(ROW_TYPE_LOADING);
	IRRECO_RETURN_INT(0);
}

/**
 * Show hildon progressbar banner.
 *
 * This function will create a new banner if one has not been created yet,
 * if banner already exists, it's properties will be changed.
 *
 * @param text		Text to show.
 * @param fraction	Value of progress.
 */
static void irreco_remote_download_dlg_set_banner(IrrecoRemoteDownloadDlg *self,
						  const gchar *text,
						  gdouble fraction)
{
	IRRECO_ENTER
	if (self->banner == NULL) {
		self->banner = hildon_banner_show_progress(GTK_WIDGET(self),
							   NULL, "");
	}

	hildon_banner_set_text(HILDON_BANNER(self->banner), text);
	hildon_banner_set_fraction(HILDON_BANNER(self->banner), fraction);
	IRRECO_RETURN
}

/**
 * Destroy banner, if it exists.
 */
static void irreco_remote_download_dlg_hide_banner(
						IrrecoRemoteDownloadDlg *self)
{
	IRRECO_ENTER
	if (self->banner) {
		gtk_widget_destroy(self->banner);
		self->banner = NULL;
	}
	IRRECO_RETURN
}

/**
 * Start a loader state machine if one is not running already.
 */
static void irreco_remote_download_dlg_loader_start(
						IrrecoRemoteDownloadDlg *self,
						GSourceFunc function,
						GtkTreeIter *parent_iter)
{
	IRRECO_ENTER

	if (self->loader_func_id == 0) {
		if (parent_iter) {
			self->loader_parent_iter = gtk_tree_iter_copy(
								parent_iter);
		}

		if (function) {
			self->loader_func_id = g_idle_add(function, self);
		} else {
			IRRECO_ERROR("Loader function pointer not given.\n");
		}
	}

	IRRECO_RETURN
}

/**
 * Stop and cleanup loader if a loader is running.
 */
static void irreco_remote_download_dlg_loader_stop(
						IrrecoRemoteDownloadDlg *self)
{
	IRRECO_ENTER
	if (self->loader_func_id != 0) {
		g_source_remove(self->loader_func_id);
		self->loader_func_id = 0;
		self->loader_state = 0;
		if (self->loader_parent_iter) {
			gtk_tree_iter_free(self->loader_parent_iter);
		}
		self->loader_parent_iter = NULL;
	}
	IRRECO_RETURN
}

static gboolean irreco_remote_download_dlg_loader_categ(
						IrrecoRemoteDownloadDlg *self)
{
	IRRECO_ENTER

	switch (self->loader_state) {
	case LOADER_STATE_INIT:
		irreco_remote_download_dlg_set_banner(self, _("Loading ..."),
						      0);
		self->loader_state = LOADER_STATE_LOOP;
		irreco_remote_download_dlg_clean_details(self);
		IRRECO_RETURN_BOOL(TRUE);

	case LOADER_STATE_LOOP: {
		GtkTreeIter iter;
		GtkTreeIter iter_loading;
		IrrecoStringTable *categories;
		IrrecoWebdbCache *webdb_cache = NULL;

		webdb_cache = irreco_data_get_webdb_cache(self->irreco_data,
							  FALSE);
		if(irreco_webdb_cache_get_remote_categories(webdb_cache,
							    &categories)) {
			IRRECO_STRING_TABLE_FOREACH_KEY(categories, key)

				/* Append categogy. */
				gtk_tree_store_append(self->tree_store,
						      &iter, NULL);
				gtk_tree_store_set(self->tree_store,
						   &iter, TEXT_COL, key,
						   FLAG_COL, ROW_TYPE_CATEGORY,
						   DATA_COL, NULL, -1);

				/* Add loading item into category. */
				gtk_tree_store_append(self->tree_store,
						      &iter_loading, &iter);
				gtk_tree_store_set(self->tree_store,
						   &iter_loading,
						   TEXT_COL, _("Loading ..."),
						   FLAG_COL, ROW_TYPE_LOADING,
						   DATA_COL, NULL, -1);
			IRRECO_STRING_TABLE_FOREACH_END
		} else {
			irreco_error_dlg(GTK_WINDOW(self),
					 irreco_webdb_cache_get_error(
					 webdb_cache));
		}

		irreco_remote_download_dlg_set_banner(self, _("Loading ..."),
						      1);
		self->loader_state = LOADER_STATE_END;
		IRRECO_RETURN_BOOL(TRUE);
		}

	case LOADER_STATE_END:
		irreco_remote_download_dlg_hide_banner(self);
		irreco_remote_download_dlg_loader_stop(self);
	}

	IRRECO_RETURN_BOOL(FALSE);
}

static gboolean irreco_remote_download_dlg_loader_manuf(
						IrrecoRemoteDownloadDlg *self)
{
	IRRECO_ENTER

	switch (self->loader_state) {
	case LOADER_STATE_INIT:
		irreco_remote_download_dlg_set_banner(self, _("Loading ..."),
						      0);
		self->loader_state = LOADER_STATE_LOOP;
		IRRECO_RETURN_BOOL(TRUE);

	case LOADER_STATE_LOOP: {
		GtkTreeIter iter;
		GtkTreeIter iter_loading;
		IrrecoStringTable *manufacturers;
		IrrecoWebdbCache *webdb_cache = NULL;
		gchar *category;

		gtk_tree_model_get(GTK_TREE_MODEL(self->tree_store),
				   self->loader_parent_iter,
				   TEXT_COL, &category, -1);

		webdb_cache = irreco_data_get_webdb_cache(self->irreco_data,
							  FALSE);

		if(irreco_webdb_cache_get_remote_manufacturers(webdb_cache,
							category,
							&manufacturers)) {
			IRRECO_STRING_TABLE_FOREACH_KEY(manufacturers, key)

				/* Add manufacturer item into category. */
				gtk_tree_store_append(self->tree_store,
				       &iter, self->loader_parent_iter);
				gtk_tree_store_set(self->tree_store,
						&iter,
						TEXT_COL, key,
						FLAG_COL, ROW_TYPE_MANUFACTURER,
						DATA_COL, NULL, -1);

				/* Add loading item into manufacturer. */
				gtk_tree_store_append(self->tree_store,
						      &iter_loading, &iter);
				gtk_tree_store_set(self->tree_store,
						   &iter_loading,
						   TEXT_COL, _("Loading ..."),
						   FLAG_COL, ROW_TYPE_LOADING,
						   DATA_COL, NULL, -1);

			IRRECO_STRING_TABLE_FOREACH_END
		} else {
			irreco_error_dlg(GTK_WINDOW(self),
					 irreco_webdb_cache_get_error(
					 webdb_cache));
		}

		g_free (category);

		/* Delete loading item from category. */
		gtk_tree_model_iter_nth_child(GTK_TREE_MODEL(self->tree_store),
					      &iter,self->loader_parent_iter,0);
		if(irreco_remote_download_dlg_row_get_type(self,
						&iter) == ROW_TYPE_LOADING) {
			gtk_tree_store_remove(self->tree_store,&iter);
		}
		irreco_remote_download_dlg_set_banner(self, _("Loading ..."),
						      1);
		self->loader_state = LOADER_STATE_END;
		IRRECO_RETURN_BOOL(TRUE);
		}

	case LOADER_STATE_END:
		irreco_remote_download_dlg_row_set_loaded(
						self, self->loader_parent_iter,
						TRUE);
		irreco_remote_download_dlg_hide_banner(self);
		irreco_remote_download_dlg_loader_stop(self);
	}

	IRRECO_RETURN_BOOL(FALSE);
}

static gboolean irreco_remote_download_dlg_loader_model(
						IrrecoRemoteDownloadDlg *self)
{
	IRRECO_ENTER

	switch (self->loader_state) {
	case LOADER_STATE_INIT:
		irreco_remote_download_dlg_set_banner(self, _("Loading ..."),
						      0);
		self->loader_state = LOADER_STATE_LOOP;
		IRRECO_RETURN_BOOL(TRUE);

	case LOADER_STATE_LOOP: {
		GtkTreeIter iter;
		GtkTreeIter iter_loading;
		GtkTreeIter category_iter;
		IrrecoStringTable *models = NULL;
		IrrecoWebdbCache *webdb_cache = NULL;
		gchar *category;
		gchar *manufacturer;

		/*Get category and manufacturer*/

		gtk_tree_model_get(GTK_TREE_MODEL(self->tree_store),
				   self->loader_parent_iter,
				   TEXT_COL, &manufacturer, -1);

		gtk_tree_model_iter_parent(GTK_TREE_MODEL(self->tree_store),
					   &category_iter,
					   self->loader_parent_iter);

		gtk_tree_model_get(GTK_TREE_MODEL(self->tree_store),
				   &category_iter,
				   TEXT_COL, &category, -1);

		webdb_cache = irreco_data_get_webdb_cache(self->irreco_data,
							  FALSE);

		if(irreco_webdb_cache_get_remote_models(webdb_cache, category,
						 manufacturer, &models)){

			IRRECO_STRING_TABLE_FOREACH_KEY(models, key)

				/* Add model item into manufacturer. */
				gtk_tree_store_append(self->tree_store,
				       &iter, self->loader_parent_iter);
				gtk_tree_store_set(self->tree_store,
						&iter,
						TEXT_COL, key,
						FLAG_COL, ROW_TYPE_MODEL,
						DATA_COL, NULL, -1);

				/* Add loading item into model. */
				gtk_tree_store_append(self->tree_store,
						      &iter_loading, &iter);
				gtk_tree_store_set(self->tree_store,
						   &iter_loading,
						   TEXT_COL, _("Loading ..."),
						   FLAG_COL, ROW_TYPE_LOADING,
						   DATA_COL, NULL, -1);

			IRRECO_STRING_TABLE_FOREACH_END
		} else {
			irreco_error_dlg(GTK_WINDOW(self),
					 irreco_webdb_cache_get_error(
					 webdb_cache));
		}
		g_free (category);
		g_free (manufacturer);

		/* Delete loading item from manufacturer. */
		gtk_tree_model_iter_nth_child(GTK_TREE_MODEL(self->tree_store),
					      &iter, self->loader_parent_iter,
	   				      0);
		if(irreco_remote_download_dlg_row_get_type(self,
						&iter) == ROW_TYPE_LOADING) {
			gtk_tree_store_remove(self->tree_store, &iter);
		}

		irreco_remote_download_dlg_set_banner(self, _("Loading ..."),
						      1);
		self->loader_state = LOADER_STATE_END;

		IRRECO_RETURN_BOOL(TRUE);
	}
	case LOADER_STATE_END:
		irreco_remote_download_dlg_row_set_loaded(
						self, self->loader_parent_iter,
						TRUE);
		irreco_remote_download_dlg_hide_banner(self);
		irreco_remote_download_dlg_loader_stop(self);
	}

	IRRECO_RETURN_BOOL(FALSE);
}

static gboolean irreco_remote_download_dlg_loader_creators(
						IrrecoRemoteDownloadDlg *self)
{
	IRRECO_ENTER

	switch (self->loader_state) {
	case LOADER_STATE_INIT:
		irreco_remote_download_dlg_set_banner(self, _("Loading ..."),
						      0);
		self->loader_state = LOADER_STATE_LOOP;
		IRRECO_RETURN_BOOL(TRUE);

	case LOADER_STATE_LOOP: {
		IrrecoWebdbCache	*webdb_cache	= NULL;

		GtkTreeIter		category_iter;
		GtkTreeIter		manufacturer_iter;
		GtkTreeIter		iter;
		GtkTreeIter 		iter_loading;

		gchar			*category;
		gchar			*manufacturer;
		gchar			*model;

		IrrecoStringTable	*creators;

		/*Get manufacturer_iter and category_iter */

		gtk_tree_model_iter_parent(GTK_TREE_MODEL(self->tree_store),
					   &manufacturer_iter,
					   self->loader_parent_iter);

		gtk_tree_model_iter_parent(GTK_TREE_MODEL(self->tree_store),
					   &category_iter,
					   &manufacturer_iter);

		/*Get category, manufacturer and model*/

		gtk_tree_model_get(GTK_TREE_MODEL(self->tree_store),
				   &category_iter,
				   TEXT_COL, &category, -1);

		gtk_tree_model_get(GTK_TREE_MODEL(self->tree_store),
				   &manufacturer_iter,
				   TEXT_COL, &manufacturer, -1);

		gtk_tree_model_get(GTK_TREE_MODEL(self->tree_store),
				   self->loader_parent_iter,
				   TEXT_COL, &model, -1);


		webdb_cache = irreco_data_get_webdb_cache(self->irreco_data,
							  FALSE);

		if(irreco_webdb_cache_get_remote_creators(webdb_cache,
			category, manufacturer, model, &creators)){

			IRRECO_STRING_TABLE_FOREACH_KEY(creators, key)

				/* Add model item into manufacturer. */
				gtk_tree_store_append(self->tree_store,
				       &iter, self->loader_parent_iter);
				gtk_tree_store_set(self->tree_store,
						&iter,
						TEXT_COL, key,
						FLAG_COL, ROW_TYPE_CREATOR,
						DATA_COL, NULL, -1);

				/* Add loading item into model. */
				gtk_tree_store_append(self->tree_store,
						      &iter_loading, &iter);
				gtk_tree_store_set(self->tree_store,
						   &iter_loading,
						   TEXT_COL, _("Loading ..."),
						   FLAG_COL, ROW_TYPE_LOADING,
						   DATA_COL, NULL, -1);

			IRRECO_STRING_TABLE_FOREACH_END
		} else {
			irreco_error_dlg(GTK_WINDOW(self),
					 irreco_webdb_cache_get_error(
					 webdb_cache));
		}
		g_free (category);
		g_free (manufacturer);
		g_free (model);

		/* Delete loading item from manufacturer. */
		gtk_tree_model_iter_nth_child(GTK_TREE_MODEL(self->tree_store),
					      &iter, self->loader_parent_iter,
	   				      0);
		if(irreco_remote_download_dlg_row_get_type(self,
						&iter) == ROW_TYPE_LOADING) {
			gtk_tree_store_remove(self->tree_store, &iter);
		}

		irreco_remote_download_dlg_set_banner(self, _("Loading ..."),
						      1);
		self->loader_state = LOADER_STATE_END;

		IRRECO_RETURN_BOOL(TRUE);
	}
	case LOADER_STATE_END:
		irreco_remote_download_dlg_row_set_loaded(
						self, self->loader_parent_iter,
						TRUE);
		irreco_remote_download_dlg_hide_banner(self);
		irreco_remote_download_dlg_loader_stop(self);
	}

	IRRECO_RETURN_BOOL(FALSE);
}

static gboolean irreco_remote_download_dlg_loader_remotes(
						IrrecoRemoteDownloadDlg *self)
{
	IRRECO_ENTER

	switch (self->loader_state) {
	case LOADER_STATE_INIT:
		irreco_remote_download_dlg_set_banner(self, _("Loading ..."),
						      0);
		self->loader_state = LOADER_STATE_LOOP;
		self->loader_pos = 0;
		IRRECO_RETURN_BOOL(TRUE);

	case LOADER_STATE_LOOP: {
		IrrecoWebdbCache	*webdb_cache	= NULL;

		GtkTreeIter		category_iter;
		GtkTreeIter		manufacturer_iter;
		GtkTreeIter		model_iter;
		GtkTreeIter		iter;

		gchar			*category;
		gchar			*manufacturer;
		gchar			*model;
		gchar			*creator;

		/* Remote info */
		gint			remote_id;
		IrrecoWebdbRemote 	*remote;
		GList			*remotes	= NULL;
		GList			*configs;
		GList			*themes;

		/* Banner info */
		gint			remote_count	= 0;
		gfloat			banner;

		/*Get manufacturer_iter and category_iter */

		gtk_tree_model_iter_parent(GTK_TREE_MODEL(self->tree_store),
					   &model_iter,
					   self->loader_parent_iter);

		gtk_tree_model_iter_parent(GTK_TREE_MODEL(self->tree_store),
					   &manufacturer_iter,
					   &model_iter);

		gtk_tree_model_iter_parent(GTK_TREE_MODEL(self->tree_store),
					   &category_iter,
					   &manufacturer_iter);

		/*Get category, manufacturer and model*/

		gtk_tree_model_get(GTK_TREE_MODEL(self->tree_store),
				   &category_iter,
				   TEXT_COL, &category, -1);

		gtk_tree_model_get(GTK_TREE_MODEL(self->tree_store),
				   &manufacturer_iter,
				   TEXT_COL, &manufacturer, -1);

		gtk_tree_model_get(GTK_TREE_MODEL(self->tree_store),
				   &model_iter,
				   TEXT_COL, &model, -1);

		gtk_tree_model_get(GTK_TREE_MODEL(self->tree_store),
				   self->loader_parent_iter,
				   TEXT_COL, &creator, -1);


		webdb_cache = irreco_data_get_webdb_cache(self->irreco_data,
							  FALSE);

		if(!irreco_webdb_cache_get_remotes(webdb_cache,
			category, manufacturer, model, creator, &remotes)) {
			irreco_error_dlg(GTK_WINDOW(self),
					 irreco_webdb_cache_get_error(
					 webdb_cache));
			goto end;
		}

		remote_id = GPOINTER_TO_INT(g_list_nth_data(remotes,
					    self->loader_pos));

		if(!irreco_webdb_cache_get_remote_by_id(webdb_cache, remote_id,
							&remote)) {
			irreco_error_dlg(GTK_WINDOW(self),
					 irreco_webdb_cache_get_error(
					 webdb_cache));
			goto end;
		}

		/* Get information of configurations */
		if (!irreco_webdb_cache_get_configurations_of_remote(
		    webdb_cache, remote_id, &configs)) {
			irreco_error_dlg(GTK_WINDOW(self),
					 irreco_webdb_cache_get_error(
					 webdb_cache));
			goto end;
		}

		configs = g_list_first(configs);
		while(configs) {
			IrrecoWebdbConf *config;
			if (GPOINTER_TO_INT(configs->data) != 0 &&
			    !irreco_webdb_cache_get_configuration(webdb_cache,
			    GPOINTER_TO_INT(configs->data), &config)) {
				irreco_error_dlg(GTK_WINDOW(self),
						 irreco_webdb_cache_get_error(
						 webdb_cache));
			}
			configs = configs->next;
		}

		/* Get information of themes*/
		if (!irreco_webdb_cache_get_themes_of_remote(
		    webdb_cache, remote_id, &themes)) {
			irreco_error_dlg(GTK_WINDOW(self),
					 irreco_webdb_cache_get_error(
					 webdb_cache));
			goto end;
		}

		themes = g_list_first(themes);
		while(themes) {
			IrrecoWebdbTheme *theme;
			if (GPOINTER_TO_INT(themes->data) != 0 &&
			    !irreco_webdb_cache_get_theme_by_id(webdb_cache,
			    GPOINTER_TO_INT(themes->data), &theme)) {
				irreco_error_dlg(GTK_WINDOW(self),
						 irreco_webdb_cache_get_error(
						 webdb_cache));
			}
			themes = themes->next;
		}

		/* Add config item into model. */
		gtk_tree_store_append(self->tree_store, &iter,
				      self->loader_parent_iter);

		gtk_tree_store_set(self->tree_store, &iter,
				   TEXT_COL, remote->uploaded->str,
				   FLAG_COL, ROW_TYPE_REMOTE,
				   DATA_COL, GINT_TO_POINTER(remote_id), -1);

		/* Delete loading item from model. */
		gtk_tree_model_iter_nth_child(GTK_TREE_MODEL(self->tree_store),
					      &iter, self->loader_parent_iter,
					      0);

		if (irreco_remote_download_dlg_row_get_type(self,
		    &iter) == ROW_TYPE_LOADING) {
			gtk_tree_store_remove(self->tree_store, &iter);
		}

		end:
		g_free (category);
		g_free (manufacturer);
		g_free (model);
		g_free (creator);

		/* Banner */

		if (remotes != NULL) {
			remote_count = g_list_length(remotes);
		}

		banner = ((float) self->loader_pos + 1) / (float) remote_count;
		irreco_remote_download_dlg_set_banner(self, _("Loading ..."),
						      banner);

		if (self->loader_pos + 1 >= remote_count) {
			self->loader_state = LOADER_STATE_END;
			IRRECO_RETURN_BOOL(TRUE);
		} else {
			self->loader_pos++;
			IRRECO_RETURN_BOOL(TRUE);
		}


		IRRECO_RETURN_BOOL(TRUE);
	}
	case LOADER_STATE_END:
		irreco_remote_download_dlg_row_set_loaded(
						self, self->loader_parent_iter,
						TRUE);
		irreco_remote_download_dlg_hide_banner(self);
		irreco_remote_download_dlg_loader_stop(self);
	}

	IRRECO_RETURN_BOOL(FALSE);
}

static gboolean irreco_remote_download_dlg_load_configs(
						IrrecoRemoteDownloadDlg *self)
{
	IrrecoWebdbCache		*webdb_cache	= NULL;
	IrrecoBackendInstance		*inst		= NULL;
	GString				*error_msg	= g_string_new("");
	IrrecoBackendManager		*manager;
	IrrecoBackendFileContainer	*file_container	= NULL;
	GString				*file_data	= NULL;
	gchar				*sha1		= NULL;
	gboolean			success		= TRUE;
	gboolean			new_instance	= FALSE;
	GList				*configurations;
	IrrecoWebdbConf			*config;
	GString				*instance_number = g_string_new("");
	IRRECO_ENTER

	webdb_cache = irreco_data_get_webdb_cache(self->irreco_data, FALSE);

	configurations = g_list_first(self->remote->configurations);
	if (configurations == NULL ||
	    GPOINTER_TO_INT(configurations->data) == 0) {
		goto end;
	}
	while(configurations) {

		if (!irreco_webdb_cache_get_configuration(webdb_cache,
		    GPOINTER_TO_INT(configurations->data), &config)) {
			g_string_printf(error_msg, "%s",
					irreco_webdb_cache_get_error(
					webdb_cache));
		}

		if(config == NULL) {
			g_string_printf(error_msg, "Configuration is NULL");
			goto end;
		}

		/* Get file_data */
		if (!irreco_webdb_cache_get_file(webdb_cache,
						 config->file_hash->str,
						 config->file_name->str,
						 &file_data)) {
			g_string_printf(error_msg, "%s",
					irreco_webdb_cache_get_error(
					webdb_cache));
			goto end;
		}

		sha1 = g_compute_checksum_for_string(G_CHECKSUM_SHA1,
					       file_data->str, -1);

		if (g_utf8_collate(config->file_hash->str, sha1) != 0) {
			g_string_printf(error_msg, "sha1 checksum failed.");
			goto end;

		}

		/* Search backend */
		manager = self->irreco_data->irreco_backend_manager;
		IRRECO_BACKEND_MANAGER_FOREACH_LIB(manager, lib)
			if (!g_str_equal(lib->name, config->backend->str)) {
				continue;
			}
			if (!(lib->api->flags &
			    IRRECO_BACKEND_EDITABLE_DEVICES)) {
				g_string_printf(error_msg,
				"\"%s\" backend is not editable...",
				config->backend->str);
				goto end;
			}
			else if (!(lib->api->flags &
				 IRRECO_BACKEND_CONFIGURATION_EXPORT)) {
				g_string_printf(error_msg,
				"\"%s\" backend doesn't support "
				"exporting its configuration...",
				config->backend->str);
				goto end;
			}
			/* Check if many instances */
			IRRECO_BACKEND_MANAGER_FOREACH_INSTANCE(manager,
								instance)
				irreco_backend_instance_get_description(
								instance);
				if (g_str_equal(instance->lib->name,
				    config->backend->str)) {
					g_string_printf(instance_number, "%s",
							instance->name->str);
					g_string_erase(instance_number, 0,
						       instance->name->len - 1);
					if (g_str_equal(instance_number->str,
					    "1")) {
						inst = instance;
						IRRECO_DEBUG("SELECTED %s",
							instance->name->str);
						goto instance_ready;
					}
				}
			IRRECO_BACKEND_MANAGER_FOREACH_END

			/* Create new instance if it comes to the crunch */
			inst = irreco_backend_manager_create_instance(
						manager, lib, NULL, NULL);
			new_instance = TRUE;
			break;

		IRRECO_STRING_TABLE_FOREACH_END

		if (inst == NULL) {
			g_string_printf(error_msg,
					"\"%s\" backend is not installed...",
					config->backend->str);
			goto end;
		}

		irreco_backend_instance_configure(inst, GTK_WINDOW(self));
		irreco_backend_instance_save_to_conf(inst);
		irreco_config_save_backends(manager);

		instance_ready:
		file_container = irreco_backend_file_container_new();
		irreco_backend_file_container_set(file_container,
						  inst->lib->name,
						  config->category->str,
						  config->manufacturer->str,
						  config->model->str,
						  config->file_name->str,
						  file_data->str);

		if (irreco_backend_instance_check_conf(inst, file_container)) {
			g_string_printf(error_msg,
					"\"%s\" backend already contains\n"
					"device \"%s\".\n"
					"Do you want to overwrite?",
					inst->lib->name, config->model->str);
			success = irreco_yes_no_dlg(GTK_WINDOW(self),
						    error_msg->str);
			g_string_erase(error_msg, 0, -1);

			if(success == FALSE) {
				goto end;
			}
		}

		/* Send file_data for backend */
		if(irreco_backend_instance_import_conf(inst, file_container)) {
			irreco_backend_manager_get_devcmd_lists(
				self->irreco_data->irreco_backend_manager);
		} else {
			g_string_printf(error_msg, "Backend error");

			if (new_instance) {
				irreco_backend_manager_destroy_instance(manager,
									inst);
			}
			goto end;
		}
		configurations = configurations->next;
		irreco_backend_file_container_free(file_container);
		file_container = NULL;
		g_free(sha1);
		sha1 = NULL;
		g_string_free(file_data, FALSE);
		file_data = NULL;
	}

	end:
	if(error_msg->len > 0) {
		irreco_error_dlg(GTK_WINDOW(self), error_msg->str);
		success = FALSE;
	}
	g_string_free(error_msg, FALSE);
	if (sha1 != NULL) g_free(sha1);
	if (file_container != NULL) {
		irreco_backend_file_container_free(file_container);
	}
	if (file_data != NULL) g_string_free(file_data, FALSE);
	g_string_free(instance_number, FALSE);

	IRRECO_RETURN_BOOL(success);
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Public Functions                                                           */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */

gboolean irreco_show_remote_download_dlg(IrrecoData *irreco_data,
					 GtkWindow *parent)
{
	IrrecoRemoteDownloadDlg	*self;
	gboolean		loop = TRUE;
	IrrecoWebdbCache	*webdb_cache;
	gboolean downloaded	= FALSE;
	IRRECO_ENTER

	self = irreco_remote_download_dlg_new(irreco_data, parent);

	do {
		gint response = gtk_dialog_run(GTK_DIALOG(self));
		if (self->loader_func_id != 0) {
			continue;
		}
		switch (response) {
		case IRRECO_REMOTE_REFRESH:
			IRRECO_DEBUG("IRRECO_REMOTE_REFRESH\n");
			gtk_tree_store_clear(self->tree_store);
			webdb_cache = irreco_data_get_webdb_cache(
						self->irreco_data, FALSE);

			if (webdb_cache->remote_id_hash != NULL) {
				g_hash_table_remove_all(
						webdb_cache->remote_id_hash);
			}
			if (webdb_cache->remote_categories != NULL) {
				irreco_string_table_free(
						webdb_cache->remote_categories);
				webdb_cache->remote_categories = NULL;
			}

			irreco_remote_download_dlg_loader_start(self,
				G_SOURCEFUNC(
				irreco_remote_download_dlg_loader_categ), NULL);

			break;
		case IRRECO_REMOTE_DOWNLOAD: {
			IrrecoWebdbCache *webdb_cache;
			IrrecoWebdbRemote *remote = self->remote;
			IrrecoThemeManager *manager =
					self->irreco_data->theme_manager;
			GList *themes;
			gint layout_index = 1;
			GString *layout_filename = g_string_new("");
			gchar *layout_data = NULL;
			gchar *layout_name = NULL;
			IrrecoButtonLayout *layout = NULL;
			GString *bg_image = g_string_new("");
			GString *notification = g_string_new("");
			IrrecoDirForeachData dir_data;
			GString *tmp;
			GString *tmp2;

			IRRECO_DEBUG("IRRECO_REMOTE_DOWNLOAD\n");
			webdb_cache = irreco_data_get_webdb_cache(
						self->irreco_data, FALSE);

			if (self->remote == NULL) {
				irreco_error_dlg(GTK_WINDOW(self),
						 "Select Remote first");
				break;
			}

			g_string_printf(notification,
					"When downloading remote, system\n"
					"needs to download all the themes\n"
					"and devices that are used in remote:\n"
					"%s \n\nDo you wish to continue?",
					self->remote->model->str);

			if (!irreco_yes_no_dlg(GTK_WINDOW(self),
					       notification->str)) {
				goto end;
			}

			/* Download configurations */
			if (!irreco_remote_download_dlg_load_configs(self)) {
				goto end;
			}

			/* Download themes */
			themes = g_list_first(remote->themes);
			if (themes == NULL) {
				goto end;
			}
			while(themes) {
				IrrecoWebdbTheme *new_theme;
				IrrecoTheme *old_theme = NULL;
				if (GPOINTER_TO_INT(themes->data) == 0) {
					goto download_layout;
				}
				if (!irreco_webdb_cache_get_theme_by_id(
				    webdb_cache,
				    GPOINTER_TO_INT(themes->data),
				    &new_theme)) {
					irreco_error_dlg(GTK_WINDOW(self),
						irreco_webdb_cache_get_error(
						webdb_cache));
					goto end;
				}
				if (!irreco_string_table_get(manager->themes,
				    new_theme->name->str, (gpointer *)
				    &old_theme) || !g_str_equal(
				    old_theme->version->str,
				    new_theme->uploaded->str)) {
					if (!irreco_theme_download_dlg_run(
					    self->irreco_data,
					    GPOINTER_TO_INT(themes->data),
					    GTK_WINDOW(self))) {
						goto end;
					}
				    }
				themes = themes->next;
			}
			irreco_theme_manager_update_theme_manager(manager);

			/* Download layout */
			download_layout:
			/*Create filename */
			while(1) {
				g_string_printf(layout_filename,
						"%s/layout%d.conf",
						irreco_get_config_dir("irreco"),
						layout_index);
				if (!irreco_file_exists(layout_filename->str)) {
					IRRECO_DEBUG("%s\n",
						     layout_filename->str);
					break;
				}
				layout_index++;
			}

			/* Get layout_data */
			if (!irreco_webdb_cache_get_remote_data(webdb_cache,
								remote->id,
								&layout_data)) {
				irreco_error_dlg(GTK_WINDOW(self),
						 irreco_webdb_cache_get_error(
						 webdb_cache));
				goto end;
			}

			/* Copy layout_name */
			layout_name = g_strdup(remote->model->str);

			/* Check if there is same remotename already */
			if (irreco_string_table_exists(
			    irreco_data->irreco_layout_array,
			    remote->model->str)) {
				gint i = 2;
				gboolean run = TRUE;
				gchar *new_name;
				GKeyFile *keyfile = g_key_file_new();
				GError *error;
				GString *cmd;

				/* Create new name */
				while (run) {
					new_name = g_strdup_printf("%s_%d",
						remote->model->str, i++);
					if (!irreco_string_table_exists(
					    irreco_data->irreco_layout_array,
					    new_name)) {
						run = FALSE;
						IRRECO_DEBUG("NEW NAME: %s\n",
							     new_name);
					} else {
						g_free(new_name);
					}
				}

				/* Create key file */
				if (!g_key_file_load_from_data(keyfile,
				    layout_data, strlen(layout_data),
				    G_KEY_FILE_KEEP_TRANSLATIONS, &error)) {
					IRRECO_DEBUG("%s\n", error->message);
					g_error_free(error);
					goto end;
				}

				/* Set new name for remote */
				g_key_file_set_string(keyfile, "layout",
						      "name", new_name);

				/* Save keyfile */
				irreco_write_keyfile(keyfile,
						     layout_filename->str);

				/* Set new name for every button */
				cmd = g_string_new("");
				g_string_printf(cmd,
				"cat %s | sed s}'^layout=%s$'}'layout=%s'} > %s",
				layout_filename->str,
				remote->model->str, new_name,
				layout_filename->str);
				system(cmd->str);
				g_string_free(cmd, FALSE);

				/* Change layout_name */
				g_free(layout_name);
				layout_name = g_strdup(new_name);

			} else if (!irreco_write_file(layout_filename->str,
				layout_data, strlen(layout_data))) {
				irreco_error_dlg(GTK_WINDOW(self),
						 "Save file error!");
				goto end;
			}

			dir_data.directory = irreco_get_config_dir("irreco");
			dir_data.filesuffix = ".conf";
			g_string_printf(layout_filename, "layout%d.conf",
					layout_index);
			dir_data.filename = layout_filename->str;
			dir_data.user_data_1 = self->irreco_data;

			/* Get new layout to list */
			irreco_config_read_layouts(&dir_data);

			irreco_string_table_get(
					irreco_data->irreco_layout_array,
					layout_name, (gpointer *) &layout);

			if (layout == NULL) {
				irreco_error_dlg(GTK_WINDOW(self),
						 "Broken layout!");
				goto end;
			}

			g_string_printf(bg_image, "%s",
					irreco_button_layout_get_bg_image(
					layout));

			if (bg_image->len < 10) {
				goto no_bg;
			}

			tmp = g_string_new(bg_image->str);
			tmp = g_string_erase(tmp, 6, -1);
			tmp2 = g_string_new("/media");


			IRRECO_PRINTF("VERTAILU");
			IRRECO_PRINTF(tmp->str);

			if(g_string_equal(tmp, tmp2))
			{
				bg_image = g_string_erase(bg_image, 0, 11);
				bg_image = g_string_prepend(bg_image, "/home/user/MyDocs");

				IRRECO_PRINTF(bg_image->str);
			}

			IRRECO_PAUSE
			g_string_free(tmp, TRUE);
			g_string_free(tmp2, TRUE);

			/* Set bg-image to layout */
			irreco_button_layout_set_bg_image(layout,
							  bg_image->str);
			irreco_config_save_layouts(irreco_data);

			no_bg:

			irreco_window_manager_set_layout(
					self->irreco_data->window_manager,
					layout);

			irreco_info_dlg(GTK_WINDOW(self),
					"Remote downloaded successfully!");
			loop = FALSE;
			downloaded = TRUE;

			end:
			g_string_free(layout_filename, FALSE);
			g_string_free(bg_image, FALSE);
			if (layout_data != NULL) {
				g_free(layout_data);
			}
			if (layout_name != NULL) {
				g_free(layout_name);
			}
			g_string_free(notification, FALSE);
			break;
		}

		case GTK_RESPONSE_DELETE_EVENT:
			IRRECO_DEBUG("GTK_RESPONSE_DELETE_EVENT\n");
			loop = FALSE;
			break;

		}
	} while (loop);

	gtk_widget_destroy(GTK_WIDGET(self));
	IRRECO_RETURN_BOOL(downloaded);
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Events and Callbacks                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Events and Callbacks
 * @{
 */

static gboolean irreco_remote_download_dlg_map_event(
						IrrecoRemoteDownloadDlg *self,
						GdkEvent *event,
						gpointer user_data)
{
	IRRECO_ENTER
	irreco_remote_download_dlg_loader_start(self,
		G_SOURCEFUNC(irreco_remote_download_dlg_loader_categ), NULL);
	IRRECO_RETURN_BOOL(FALSE);
}

static void irreco_remote_download_dlg_destroy_event(
						IrrecoRemoteDownloadDlg *self,
						gpointer user_data)
{
	IRRECO_ENTER
	irreco_remote_download_dlg_loader_stop(self);
	IRRECO_RETURN
}

static void irreco_remote_download_dlg_row_activated_event(
						GtkTreeView *tree_view,
						GtkTreePath *path,
						GtkTreeViewColumn *column,
						IrrecoRemoteDownloadDlg *self)
{
	IRRECO_ENTER

	if (gtk_tree_view_row_expanded(tree_view, path)) {
		gtk_tree_view_expand_row(tree_view, path, FALSE);
	} else {
		gtk_tree_view_collapse_row(tree_view, path);
	}
	IRRECO_RETURN
}

static void irreco_remote_download_dlg_row_expanded_event(
						GtkTreeView *tree_view,
						GtkTreeIter *iter,
						GtkTreePath *path,
						IrrecoRemoteDownloadDlg *self)
{
	IRRECO_ENTER

	irreco_remote_download_dlg_clean_details(self);

	if (self->loader_func_id != 0) {
		gtk_tree_view_collapse_row(tree_view, path);
	}

	if (!irreco_remote_download_dlg_row_is_loaded(self, iter)) {
		switch (irreco_remote_download_dlg_row_get_type(self, iter)) {
		case ROW_TYPE_CATEGORY:
			irreco_remote_download_dlg_loader_start(self,
				G_SOURCEFUNC(
				irreco_remote_download_dlg_loader_manuf), iter);
			break;

		case ROW_TYPE_MANUFACTURER:
			irreco_remote_download_dlg_loader_start(self,
				G_SOURCEFUNC(
				irreco_remote_download_dlg_loader_model), iter);
			break;

		case ROW_TYPE_MODEL:
			irreco_remote_download_dlg_loader_start(self,
				G_SOURCEFUNC(
				irreco_remote_download_dlg_loader_creators),
				iter);
			break;

		case ROW_TYPE_CREATOR:
			irreco_remote_download_dlg_loader_start(self,
				G_SOURCEFUNC(
				irreco_remote_download_dlg_loader_remotes),
				iter);
			break;
		}
	}

	IRRECO_RETURN
}

static void irreco_remote_download_dlg_row_collapsed_event(
						GtkTreeView *tree_view,
						GtkTreeIter *iter,
						GtkTreePath *path,
						IrrecoRemoteDownloadDlg *self)
{
	IRRECO_ENTER

	irreco_remote_download_dlg_clean_details(self);

	IRRECO_RETURN
}

static void irreco_remote_download_dlg_row_selected_event(GtkTreeSelection *sel,
						IrrecoRemoteDownloadDlg *self)
{
	GtkTreeIter iter;
	GtkTreeModel *model;

	IRRECO_ENTER

	if (gtk_tree_selection_get_selected (sel, &model, &iter))
	{
		switch (irreco_remote_download_dlg_row_get_type(self, &iter)) {

		case ROW_TYPE_REMOTE:
			IRRECO_DEBUG("ROW_TYPE_REMOTE\n");
		{
			IrrecoWebdbCache *webdb_cache;
			IrrecoWebdbRemote *remote;
			GString *download_count;
			GString *theme_names = g_string_new("");
			GString *backend_names = g_string_new("");
			GList *configs;
			GList *backends = NULL;
			GList *dummy_list = NULL;
			GList *themes = NULL;
			GtkTextBuffer *buffer;
			gint id;

			/* Get remote_id */
			gtk_tree_model_get(GTK_TREE_MODEL(self->tree_store),
					   &iter, DATA_COL,(gpointer) &id, -1);

			/* Get remote by id */
			webdb_cache = irreco_data_get_webdb_cache(
						self->irreco_data, FALSE);
			if (!irreco_webdb_cache_get_remote_by_id(webdb_cache,
								 id,
								 &remote)) {
				irreco_error_dlg(GTK_WINDOW(self),
						 irreco_webdb_cache_get_error(
						 webdb_cache));
				goto end;

			}

			/* Model */
			gtk_label_set_text(GTK_LABEL(self->details_model),
					   remote->model->str);

			/* Creator */
			gtk_label_set_text(GTK_LABEL(self->details_creator),
					   remote->creator->str);

			/* Downloaded */
			download_count = g_string_new("");
			g_string_printf(download_count, "%d times",
					remote->download_count);
			gtk_label_set_text(GTK_LABEL(self->details_downloaded),
					   download_count->str);
			g_string_free(download_count, FALSE);

			/* Comment */
			buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(
							self->details_comment));
			gtk_text_buffer_set_text(buffer, remote->comment->str,
						 remote->comment->len);
			gtk_text_view_set_buffer(GTK_TEXT_VIEW(
						 self->details_comment),
						 buffer);

			/* Get configurations and get backends */
			if (!irreco_webdb_cache_get_configurations_of_remote(
			    webdb_cache, id, &configs)) {
				irreco_error_dlg(GTK_WINDOW(self),
						 irreco_webdb_cache_get_error(
						 webdb_cache));
				goto end;
			}
			configs = g_list_first(configs);
			while(configs) {
				IrrecoWebdbConf *config = NULL;
				if (GPOINTER_TO_INT(configs->data) != 0 &&
				    !irreco_webdb_cache_get_configuration(
				    webdb_cache, GPOINTER_TO_INT(configs->data),
				    &config)) {
					irreco_error_dlg(GTK_WINDOW(self),
						irreco_webdb_cache_get_error(
						webdb_cache));
					goto end;
				}

				if (config != NULL) {
					backends = g_list_append(backends,
							config->backend->str);
				}
				configs = configs->next;
			}

			backends = g_list_first(backends);
			dummy_list = backends;
			if (backends == NULL) {
				g_string_append_printf(backend_names, "-");
			} else {
				/* deletion of duplicates */
				while(backends) {
					const char *backend =  backends->data;
					GList *next = backends->next;
					while(next) {
						if (g_str_equal(backend,
								next->data)) {
							backends =
							g_list_remove(backends,
								next->data);
							dummy_list = backends;
							next = backends;
						}
						next = next->next;
					}
					backends = backends->next;
				}
			}

			/* Create Backends string */
			backends = g_list_first(dummy_list);
			while(backends) {
				const char *backend =  backends->data;
				g_string_append_printf(backend_names,
						       "%s", backend);
				backends = backends->next;
				if (backends) {
					if (!backends->next) {
						g_string_append_printf(
								backend_names,
								_(" and\n"));
					} else {
						g_string_append_printf(
								backend_names,
								",\n");
					}
				}

			}

			gtk_label_set_text(GTK_LABEL(self->details_backends),
					   backend_names->str);

			/* Create themes string */
			if (!irreco_webdb_cache_get_themes_of_remote(
			    webdb_cache, id, &themes)) {
				irreco_error_dlg(GTK_WINDOW(self),
						 irreco_webdb_cache_get_error(
						 webdb_cache));
				goto end;
			}
			themes = g_list_first(themes);
			if (themes == NULL ||
			    GPOINTER_TO_INT(themes->data) == 0) {
				g_string_append(theme_names, "-");
			}
			while(themes) {
				IrrecoWebdbTheme *theme = NULL;
				if (GPOINTER_TO_INT(themes->data) != 0 &&
				    !irreco_webdb_cache_get_theme_by_id(
				    webdb_cache, GPOINTER_TO_INT(themes->data),
				    &theme)) {
					irreco_error_dlg(GTK_WINDOW(self),
						irreco_webdb_cache_get_error(
						webdb_cache));
					goto end;
				}
				themes = themes->next;
				if (theme != NULL) {
					g_string_append_printf(theme_names,
							"%s", theme->name->str);
				}
				if (themes) {
					if (!themes->next) {
						g_string_append_printf(
								theme_names,
								_(" and\n"));
					} else {
						g_string_append_printf(
								theme_names,
								",\n");
					}
				}
			}
			gtk_label_set_text(GTK_LABEL(self->details_themes),
					   theme_names->str);

			self->remote = remote;
			gtk_widget_hide(GTK_WIDGET(self->help_text));
			gtk_widget_show(GTK_WIDGET(self->details));
			end:
			g_string_free(theme_names, TRUE);
			g_string_free(backend_names, TRUE);
		}
			break;

		default:
			irreco_remote_download_dlg_clean_details(self);
			gtk_widget_show(GTK_WIDGET(self->help_text));
			gtk_widget_hide(GTK_WIDGET(self->details));
			break;
		}
	}

	IRRECO_RETURN
}

/** @} */

/** @} */



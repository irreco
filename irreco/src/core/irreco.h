/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup Irreco
 * @{
 */

/**
 * @file
 * Header file of @ref Irreco.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include global depencies                                                   */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/*
 * Some people like to include heades everywhere in their project. I like
 * to make one central file which contains all the includes i need.
 *
 * Also this contains all structures, datatypes, macros, and such things that
 * are usefull to have available everywhere.
 */

#ifndef __IRRECO_H_INCLUDE__
#define __IRRECO_H_INCLUDE__

#define IRRECO_DEBUG_PREFIX "CORE"
#include <irreco_util.h>
#include "../../config.h"

#include <string.h>
#include <unistd.h>

#include <libintl.h>
#include <locale.h>

#include <glib.h>
#include <glib/gprintf.h>
#include <glib/gstdio.h>

#include <hildon/hildon-program.h>
#include <gtk/gtk.h>
#include <gtk/gtkmain.h>
#include <gtk/gtkbutton.h>


/*#include <cairo.h>
#include <cairo-svg.h>*/

#endif /* __IRRECO_H_INCLUDE__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_H_TYPEDEF__
#define __IRRECO_H_TYPEDEF__

#endif /* __IRRECO_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_H__
#define __IRRECO_H__
/*#include "irreco_data.h"*/



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro.                                                                     */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

#define IRRECO_SCREEN_WIDTH	800
#define IRRECO_SCREEN_HEIGHT	424

#define IRRECO_LAYOUT_WIDTH	IRRECO_SCREEN_WIDTH
#define IRRECO_LAYOUT_HEIGHT	IRRECO_SCREEN_HEIGHT

#define _(String) gettext (String)

#ifndef G_SOURCEFUNC
#define	G_SOURCEFUNC(f) ((GSourceFunc) (f))
#endif

#ifndef G_DESTROYNOTIFY
#define	G_DESTROYNOTIFY(f) ((GDestroyNotify) (f))
#endif

#ifndef G_HFUNC
#define	G_HFUNC(f) ((GHFunc) (f))
#endif

#ifndef G_FUNC
#define	G_FUNC(f) ((GFunc) (f))
#endif

#ifndef GTK_FUNCTION
#define	GTK_FUNCTION(f) ((GtkFunction) (f))
#endif

#ifndef G_MENU_POS_FUNC
#define	G_MENU_POS_FUNC(f) ((GtkMenuPositionFunc) (f))
#endif

#ifndef IRRECO_BUTTON_CALLBACK
#define	IRRECO_BUTTON_CALLBACK(f) ((IrrecoButtonCallback) (f))
#endif





/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Messages                                                                   */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Some often used strings. */
#define IRRECO_STARTUP_NO_REMOTE \
	"Welcome to Irreco. You have not created any remotes. "  \
	"You can download remote by selecting one from next dialog " \
	"or create new by tapping \"Cancel\" and selecting \"Menu\" " \
	"\"New remote\"."
#define IRRECO_NO_REMOTE_HELP \
	"Please select \"New Remote\" " \
	"or \"Download remote\" from the menu."
#define IRRECO_NO_CONTROLLER_HELP \
	"You have not configured any device controllers. " \
	"Please select \"Device Controllers\" from the menu."
#define IRRECO_NO_DEVICE_HELP \
	"You have not configured any devices. " \
	"Please select \"Devices\" from the menu."
#define IRRECO_LAYOUT_NAME_COLLISION \
	"You cannot have two remotes with the same name!"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macros                                                                     */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Usefull macros for iterating GPtrArray backwards. these macros dont require
 * callback functions, which is good.
 */

/*
 * Basically you cant foreach a GPtrArray and destroy the contents, because the
 * removal of one element from the array will change the indices of the rest.
 * But if we iterate the array backward, then we can drop element one at a time
 * from the end, and the other array indices should remain unchanged.
 */
#define IRRECO_PTR_ARRAY_BACKWARDS(_array, _var_type, _var_name)		       \
	{ _var_type _var_name;						       \
	guint _index = _array->len;					       \
	while (_index-- && (_var_name = 				       \
		(_var_type) g_ptr_array_index(_array, _index)) != NULL) {
#define IRRECO_PTR_ARRAY_BACKWARDS_END }}

/*
 * Should work like g_ptr_array_foreach(), but does not require callback functions.
 */
#define IRRECO_PTR_ARRAY_FORWARDS(_array, _var_type, _var_name)		       \
	{ _var_type _var_name;						       \
	guint _index = 0;						       \
	for (; _index < _array->len && (_var_name = (_var_type) 	       \
		g_ptr_array_index(_array, _index)) != NULL; _index++) {
#define IRRECO_PTR_ARRAY_FORWARDS_END }}



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
gboolean irreco_gerror_check_print(GError ** error);
gboolean irreco_gerror_check_free(GError ** error);




#endif /* __IRRECO_H__ */

/** @} */

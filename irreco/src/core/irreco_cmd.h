/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoCmd
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoCmd.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_COMMAND_H_TYPEDEF__
#define __IRRECO_COMMAND_H_TYPEDEF__

typedef union  _IrrecoCmd 		IrrecoCmd;
typedef struct _IrrecoCmdTypeBackend	IrrecoCmdTypeBackend;
typedef struct _IrrecoCmdTypeLayout	IrrecoCmdTypeLayout;
typedef struct _IrrecoCmdTypeWait	IrrecoCmdTypeWait;
typedef enum {
	IRRECO_COMMAND_NONE,
	IRRECO_COMMAND_NEXT_REMOTE,
	IRRECO_COMMAND_PREVIOUS_REMOTE,
	IRRECO_COMMAND_FULLSCREEN_TOGGLE,
	IRRECO_COMMAND_BACKEND,
	IRRECO_COMMAND_SHOW_LAYOUT,
	IRRECO_COMMAND_WAIT
} IrrecoCmdType;

#endif /* __IRRECO_COMMAND_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_COMMAND_H__
#define __IRRECO_COMMAND_H__
#include "irreco.h"
#include "irreco_data.h"
#include "irreco_backend_instance.h"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

struct _IrrecoCmdTypeBackend {
	IrrecoCmdType		type;
	GString			*title;
	IrrecoBackendInstance	*instance;
	gchar			*device_name;
	gchar			*command_name;
};

struct _IrrecoCmdTypeLayout {
	IrrecoCmdType		type;
	GString			*title;
	gchar			*name;
};

struct _IrrecoCmdTypeWait {
	IrrecoCmdType		type;
	GString			*title;
	gulong			delay;
};

union _IrrecoCmd {
	IrrecoCmdType		type;
	IrrecoCmdTypeBackend	backend;
	IrrecoCmdTypeLayout	layout;
	IrrecoCmdTypeWait	wait;
};


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Defines.                                                                   */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#define IRRECO_COMMAND_NONE_TITLE		"None"
#define IRRECO_COMMAND_NEXT_REMOTE_TITLE	"Next remote"
#define IRRECO_COMMAND_PREVIOUS_REMOTE_TITLE	"Previous remote"
#define IRRECO_COMMAND_FULLSCREEN_TOGGLE_TITLE	"Fullscreen toggle"
#define IRRECO_COMMAND_FULLSCREEN_TOGGLE_NAME	"Fullscreen"
#define IRRECO_COMMAND_SHOW_LAYOUT_TITLE_PREFIX	"Show remote: "
#define IRRECO_COMMAND_WAIT_TITLE_FORMAT	"Wait %.1f seconds"

#define IRRECO_COMMAND_NONE_STRING		"none"
#define IRRECO_COMMAND_NEXT_REMOTE_STRING	"next-remote"
#define IRRECO_COMMAND_PREVIOUS_REMOTE_STRING	"previous-remote"
#define IRRECO_COMMAND_FULLSCREEN_TOGGLE_STRING	"fullscreen-toggle"
#define IRRECO_COMMAND_SHOW_BACKEND_STRING	"backend"
#define IRRECO_COMMAND_SHOW_LAYOUT_STRING	"show-layout"
#define IRRECO_COMMAND_WAIT_STRING		"wait"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
IrrecoCmd *irreco_cmd_create();
void irreco_cmd_destroy(IrrecoCmd * irreco_cmd);
void irreco_cmd_set_builtin(IrrecoCmd * irreco_cmd,
			    IrrecoCmdType type);
void irreco_cmd_set_backend(IrrecoCmd * irreco_cmd,
			       IrrecoBackendInstance * instance,
			       const gchar * device_name,
			       const gchar * command_name);
void irreco_cmd_set_layout(IrrecoCmd * irreco_cmd,
			       const gchar * name);
void irreco_cmd_set_wait(IrrecoCmd * irreco_cmd, gulong delay);
void irreco_cmd_copy(IrrecoCmd * from, IrrecoCmd * to);
IrrecoCmd *irreco_cmd_dublicate(IrrecoCmd * old);
const gchar *irreco_cmd_get_long_name(IrrecoCmd * irreco_cmd);
const gchar *irreco_cmd_get_short_name(IrrecoCmd * irreco_cmd);
void irreco_cmd_print(IrrecoCmd * irreco_cmd);
gboolean irreco_cmd_execute(IrrecoCmd * irreco_cmd, IrrecoData * irreco_data);

const gchar *irreco_cmd_type_to_str(IrrecoCmdType type);
IrrecoCmdType irreco_cmd_str_to_type(const gchar * type);
void irreco_cmd_to_keyfile(IrrecoCmd   *command,
			   GKeyFile    *keyfile,
			   const gchar *group);
IrrecoCmd* irreco_cmd_from_keyfile(IrrecoData * irreco_data,
				   IrrecoKeyFile * keyfile);


#endif /* __IRRECO_COMMAND_H__ */

/** @} */

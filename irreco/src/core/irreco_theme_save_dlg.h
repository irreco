/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * irreco_theme_save_dlg.c
 * Copyright (C)  2008 <>
 *
 * irreco_theme_save_dlg.c is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * irreco_theme_save_dlg.c is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __IRRECO_THEME_SAVE_DLG_H_TYPEDEF__
#define __IRRECO_THEME_SAVE_DLG_H_TYPEDEF__

#define IRRECO_TYPE_THEME_SAVE_DLG             (irreco_theme_save_dlg_get_type ())
#define IRRECO_THEME_SAVE_DLG(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), IRRECO_TYPE_THEME_SAVE_DLG, IrrecoThemeSaveDlg))
#define IRRECO_THEME_SAVE_DLG_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), IRRECO_TYPE_THEME_SAVE_DLG, IrrecoThemeSaveDlgClass))
#define IRRECO_IS_THEME_SAVE_DLG(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), IRRECO_TYPE_THEME_SAVE_DLG))
#define IRRECO_IS_THEME_SAVE_DLG_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), IRRECO_TYPE_THEME_SAVE_DLG))
#define IRRECO_THEME_SAVE_DLG_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), IRRECO_TYPE_THEME_SAVE_DLG, IrrecoThemeSaveDlgClass))

typedef struct _IrrecoThemeSaveDlgClass IrrecoThemeSaveDlgClass;
typedef struct _IrrecoThemeSaveDlg IrrecoThemeSaveDlg;

#endif /* __IRRECO_THEME_SAVE_DLG_H_TYPEDEF__ */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_THEME_SAVE_DLG_H__
#define __IRRECO_THEME_SAVE_DLG_H__
#include "irreco.h"
#include "irreco_internal_dlg.h"
#include "irreco_bg_browser_widget.h"
#include "irreco_theme_creator_dlg.h"
#include "irreco_button.h"
#include "irreco_button_browser_widget.h"
#include "irreco_theme_button.h"

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/


struct _IrrecoThemeSaveDlgClass
{
	IrrecoInternalDlgClass parent_class;
};

struct _IrrecoThemeSaveDlg
{
	IrrecoInternalDlg	parent_instance;
	IrrecoTheme		*theme;
	IrrecoData		*irreco_data;
	GtkWindow		*parent_window;

	GtkWidget		*label;
	GtkWidget		*save_dialog;
	GtkWidget		*radio1, *radio2, *radio3;

	gint			loader_state;
	gint			loader_func_id;

};


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/


GType irreco_theme_save_dlg_get_type (void) G_GNUC_CONST;
gboolean
irreco_theme_save_dlg_run(IrrecoData *irreco_data, IrrecoTheme *irreco_theme,
			  GtkWindow *parent_window);

#endif /* _IRRECO_THEME_SAVE_DLG_H_ */
/** @} */

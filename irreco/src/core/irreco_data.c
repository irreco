/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_data.h"
#include "irreco_button.h"
#include "irreco_button_dlg.h"
#include <irreco_string_table.h>
#include "irreco_backend_lib.h"
#include "irreco_backend_instance.h"

/**
 * @addtogroup IrrecoData
 * @ingroup Irreco
 *
 * Main irreco data structure.
 *
 * IrrecoData should contain pointers to every piece of memory irreco has
 * allocated. Either directry, or via other structures.
 *
 * Also destroying IrrecoData should free pretty much everything elese the
 * program has allocated.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoData.
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

IrrecoData *irreco_data_new()
{
	IrrecoData *self;
	IRRECO_ENTER

	self = g_slice_new0(IrrecoData);
	/*self->button_style_array = irreco_string_table_new(
		G_DESTROYNOTIFY(irreco_button_style_destroy), NULL);*/
	self->irreco_layout_array = irreco_string_table_new(
		G_DESTROYNOTIFY(irreco_button_layout_destroy),
		IRRECO_KEY_SET_NOTIFY(irreco_button_layout_set_name));
	self->irreco_backend_manager =
		irreco_backend_manager_create(self);
	self->new_button_dlg = irreco_button_dlg_create(self);

	irreco_button_dlg_set(self->new_button_dlg, NULL, NULL, NULL);

	self->window_manager = irreco_window_manager_create(self);
	self->cmd_chain_manager = irreco_cmd_chain_manager_new();
	IRRECO_RETURN_PTR(self);
}

void irreco_data_free(IrrecoData *self)
{
	IRRECO_ENTER
	if (self->webdb_cache) irreco_webdb_cache_free(self->webdb_cache);
	irreco_window_manager_destroy(self->window_manager);
	irreco_string_table_free(self->irreco_layout_array);
	/*irreco_string_table_free(self->button_style_array);*/
	irreco_cmd_chain_manager_free(self->cmd_chain_manager);
	irreco_backend_manager_destroy(self->irreco_backend_manager);
	irreco_button_dlg_destroy(self->new_button_dlg);
	irreco_theme_manager_free(self->theme_manager);
	g_slice_free(IrrecoData, self);
	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Functions                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */

/**
 * Get IrrecoWebdbCache object.
 *
 * @param reset If TRUE, the old IrrecoWebdbCache object will be freed,
 *              and a new one will be created. This has the effect of releasing
 *              all cached data.
 */
IrrecoWebdbCache *irreco_data_get_webdb_cache(IrrecoData *self,
					      gboolean reset)
{
	IRRECO_ENTER

	if (reset == TRUE && self->webdb_cache != NULL) {
		irreco_webdb_cache_free(self->webdb_cache);
		self->webdb_cache = NULL;
	}
	if (self->webdb_cache == NULL) {
		self->webdb_cache = irreco_webdb_cache_new();
	}

	IRRECO_RETURN_PTR(self->webdb_cache);
}

/** @} */

/** @} */





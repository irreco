/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_backend_err_dlg.h"
#include "gtk/gtkmessagedialog.h"


/**
 * @addtogroup IrrecoBackendErrDlg
 * @ingroup Irreco
 *
 * For displaying error messages from backends.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoBackendErrDlg.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/


static gboolean irreco_backend_err_dlg_configure_event(GtkWidget *self,
						       GdkEventConfigure *event,
						       gpointer user_data);



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

G_DEFINE_TYPE(IrrecoBackendErrDlg, irreco_backend_err_dlg, IRRECO_TYPE_DLG)

static void irreco_backend_err_dlg_finalize(GObject *object)
{
	G_OBJECT_CLASS(irreco_backend_err_dlg_parent_class)->finalize (object);
}

static void irreco_backend_err_dlg_class_init(IrrecoBackendErrDlgClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	object_class->finalize = irreco_backend_err_dlg_finalize;
}

static GtkWidget* irreco_backend_err_dlg_create_label(const gchar * text)
{
	GtkWidget *label;
	IRRECO_ENTER

	label = gtk_label_new(text);
	gtk_label_set_line_wrap(GTK_LABEL(label), TRUE);
	gtk_label_set_selectable(GTK_LABEL(label), TRUE);
	gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.0);
	IRRECO_RETURN_PTR(label);
}

static void irreco_backend_err_dlg_init(IrrecoBackendErrDlg *self)
{
	GtkWidget *hbox, *vbox, *image, *table;
	IRRECO_ENTER

	/* Construct dialog. */
	gtk_window_set_title(GTK_WINDOW(self), _("Irreco Backend Error"));
	gtk_window_set_modal(GTK_WINDOW(self), TRUE);
	gtk_window_set_destroy_with_parent(GTK_WINDOW(self), TRUE);
	gtk_dialog_set_has_separator(GTK_DIALOG(self), FALSE);
	gtk_dialog_add_buttons(GTK_DIALOG(self),
			       GTK_STOCK_OK, GTK_RESPONSE_OK,
			       NULL);

	/* Create widgets. */
	hbox = gtk_hbox_new(FALSE, 12);
	vbox = gtk_vbox_new(FALSE, 12);
	table = gtk_table_new(4, 2, 0);
	gtk_table_set_col_spacings(GTK_TABLE(table), 8);
	gtk_table_set_row_spacings(GTK_TABLE(table), 5);

	image = gtk_image_new_from_stock(NULL, GTK_ICON_SIZE_DIALOG);
	gtk_image_set_from_stock(GTK_IMAGE(image), GTK_STOCK_DIALOG_ERROR,
				 GTK_ICON_SIZE_DIALOG);
	gtk_misc_set_alignment(GTK_MISC(image), 0.5, 0.0);

	self->backend = irreco_backend_err_dlg_create_label(NULL);
	self->instace = irreco_backend_err_dlg_create_label(NULL);
	self->code    = irreco_backend_err_dlg_create_label(NULL);
	self->message = irreco_backend_err_dlg_create_label(NULL);

	/* Build dialog. */
	gtk_table_attach_defaults(GTK_TABLE(table),
		irreco_backend_err_dlg_create_label(_("Backend:")), 0, 1, 0, 1);
	gtk_table_attach_defaults(GTK_TABLE(table), self->backend, 1, 2, 0, 1);

	gtk_table_attach_defaults(GTK_TABLE(table),
		irreco_backend_err_dlg_create_label(_("Instance:")), 0, 1, 1, 2);
	gtk_table_attach_defaults(GTK_TABLE(table), self->instace, 1, 2, 1, 2);

	gtk_table_attach_defaults(GTK_TABLE(table),
		irreco_backend_err_dlg_create_label(_("Code:")), 0, 1, 2, 3);
	gtk_table_attach_defaults(GTK_TABLE(table), self->code, 1, 2, 2, 3);

	gtk_table_attach_defaults(GTK_TABLE(table),
		irreco_backend_err_dlg_create_label(_("Message:")), 0, 1, 3, 4);
	gtk_table_attach_defaults(GTK_TABLE(table), self->message, 1, 2, 3, 4);

	gtk_box_pack_start(GTK_BOX(vbox), table,
			   TRUE, TRUE, 0);

	gtk_box_pack_start(GTK_BOX(hbox), image,
			   FALSE, FALSE, 0);

	gtk_box_pack_start(GTK_BOX(hbox), vbox,
			   TRUE, TRUE, 0);

	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(self)->vbox), hbox,
			   FALSE, FALSE, 0);

	gtk_container_set_border_width(GTK_CONTAINER(self), 5);
	gtk_container_set_border_width(GTK_CONTAINER(hbox), 5);
	gtk_box_set_spacing(GTK_BOX(GTK_DIALOG(self)->vbox), 14); /* 14 + 2 * 5 = 24 */
	gtk_container_set_border_width(GTK_CONTAINER(GTK_DIALOG(self)->action_area), 5);
	gtk_box_set_spacing(GTK_BOX(GTK_DIALOG(self)->action_area), 6);

	/* Signal handlers. */
	g_signal_connect(G_OBJECT(self), "configure-event",
			 G_CALLBACK(irreco_backend_err_dlg_configure_event),
			 self);
	IRRECO_RETURN
}

GtkWidget* irreco_backend_err_dlg_new(GtkWindow *parent,
				      const gchar *backend,
				      const gchar *instace,
				      const gchar *code,
				      const gchar *message)
{
	IrrecoBackendErrDlg *self;
	IRRECO_ENTER

	self = g_object_new(IRRECO_TYPE_BACKEND_ERR_DLG, NULL);
	irreco_dlg_set_parent(IRRECO_DLG(self), parent);

	gtk_label_set_text(GTK_LABEL(self->backend), backend);
	gtk_label_set_text(GTK_LABEL(self->instace), instace);
	gtk_label_set_text(GTK_LABEL(self->code), code);
	gtk_label_set_text(GTK_LABEL(self->message), message);
	IRRECO_RETURN_PTR(self);
}

/** @} */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Functions                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */

void irreco_show_backend_err_dlg(GtkWindow *parent,
				 const gchar *backend,
				 const gchar *instace,
				 const gchar *code,
				 const gchar *message)
{
	GtkWidget *self;
	IRRECO_ENTER

	self = irreco_backend_err_dlg_new(parent, backend, instace,
					  code, message);
	gtk_widget_show_all(GTK_WIDGET(self));
	gtk_dialog_run(GTK_DIALOG(self));
	gtk_widget_destroy(self);
	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Signal handlers                                                            */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

static gboolean irreco_backend_err_dlg_configure_event(GtkWidget *self,
						       GdkEventConfigure *event,
						       gpointer user_data)
{
/* 	gint x, y; */
	IRRECO_ENTER

/* 	IRRECO_PRINTF("Dialog size w%i h%i\n", event->width, event->height);

	x = (800 - event->width) / 2;
	y = (480 - event->height) / 2;

	if (x != event->x || y != event->y) {
		IRRECO_PRINTF("Moving dialog from x%i y%i to x%i y%i\n",
			      event->x, event->y, x, y);
		gtk_window_move(GTK_WINDOW(self), x, y);
	} else {
		IRRECO_PRINTF("Dialog position x%i y%i\n", x, y);
	} */

	IRRECO_RETURN_BOOL(FALSE);
}

/** @} */


























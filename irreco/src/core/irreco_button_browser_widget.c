/*
 * irreco - Ir Remote Control
 * Copyright (C) 2008  Joni Kokko (t5kojo01@students.oamk.fi),
 * Harri Vattulainen (t5vaha01@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_button_browser_widget.h"
#include <hildon/hildon-banner.h>

G_DEFINE_TYPE
(IrrecoButtonBrowserWidget, irreco_button_browser_widget, GTK_TYPE_VBOX)

/**
 * @addtogroup IrrecoButtonBrowserWidget
 * @ingroup Irreco
 *
 * This widget helps selection of button.
 *
 * This widget allows button selection from themes
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoButtonBrowserWidget.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

static gboolean irreco_button_browser_widget_populate_themes(
						IrrecoButtonBrowserWidget *self);

static gboolean irreco_button_browser_widget_loader_images(
						IrrecoButtonBrowserWidget *self);

static void
irreco_button_browser_widget_loader_start(IrrecoButtonBrowserWidget *self,
						  GSourceFunc function,
						  GtkTreeIter *parent_iter);

static void
irreco_button_browser_widget_destroy_event(IrrecoButtonBrowserWidget *self,
						   gpointer user_data);

static void irreco_button_browser_widget_theme_selection_changed(
						GtkTreeSelection * selection,
					        IrrecoButtonBrowserWidget *self);

static void irreco_button_browser_widget_image_selection_changed(
						GtkTreeSelection * selection,
					        IrrecoButtonBrowserWidget *self);

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/** Loader states. */
enum
{
	LOADER_STATE_INIT,
	LOADER_STATE_LOOP,
	LOADER_STATE_END,
	LOADER_STATE_CLEANUP
};

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

static void irreco_button_browser_widget_finalize (GObject *object)
{
	IRRECO_ENTER

	if (G_OBJECT_CLASS(irreco_button_browser_widget_parent_class)->finalize)
	G_OBJECT_CLASS(irreco_button_browser_widget_parent_class)->finalize (object);

	IRRECO_RETURN
}

static void irreco_button_browser_widget_class_init(IrrecoButtonBrowserWidgetClass *klass)
{
	GObjectClass *object_class;
	IRRECO_ENTER

	object_class = G_OBJECT_CLASS (klass);
	object_class->finalize = irreco_button_browser_widget_finalize;

	IRRECO_RETURN
}

static void irreco_button_browser_widget_init (IrrecoButtonBrowserWidget *self)
{
	GtkWidget *frame_for_themes;
	GtkWidget *frame_for_images;
	IRRECO_ENTER

	self->current_theme = g_string_new("");
	self->current_image = NULL;

	/* Create themes */

	/* Create Frame for Themes */
	frame_for_themes = gtk_frame_new("");
	gtk_frame_set_label_widget(GTK_FRAME(frame_for_themes),
		irreco_gtk_label_bold("Themes", 0, 0, 0, 0, 0, 0));

	/* Create list of themes */
	self->themes = IRRECO_LISTBOX_TEXT(
			irreco_listbox_text_new_with_autosize(200, 200,
							      200, 200));
	gtk_container_add(GTK_CONTAINER(frame_for_themes),
			  GTK_WIDGET(self->themes));

	/* Create Images */

	/* Create frame for Images */
	frame_for_images = gtk_frame_new("");
	gtk_frame_set_label_widget(GTK_FRAME(frame_for_images),
		irreco_gtk_label_bold("Images", 0, 0, 0, 0, 0, 0));

	/* Create list of images */
	self->images = IRRECO_LISTBOX_IMAGE(
			irreco_listbox_image_new_with_autosize(400, 400,
							       200, 200));
	irreco_listbox_set_select_new_rows(IRRECO_LISTBOX(self->images), FALSE);
	gtk_container_add(GTK_CONTAINER(frame_for_images),
			  GTK_WIDGET(self->images));

	/* Signal handlers. */
	g_signal_connect(G_OBJECT(self), "destroy",
		G_CALLBACK(irreco_button_browser_widget_destroy_event), NULL);

	g_signal_connect(G_OBJECT(IRRECO_LISTBOX(self->themes)->tree_selection),
		"changed",
		G_CALLBACK(irreco_button_browser_widget_theme_selection_changed),
		self);

	g_signal_connect(G_OBJECT(IRRECO_LISTBOX(self->images)->tree_selection),
		"changed",
		G_CALLBACK(irreco_button_browser_widget_image_selection_changed),
		self);

	/* Create hbox */
	self->hbox = g_object_new(GTK_TYPE_HBOX, NULL);
	gtk_box_set_spacing(GTK_BOX(self->hbox), 8);

	gtk_box_pack_start(GTK_BOX(self->hbox), GTK_WIDGET(frame_for_themes),
			   TRUE, TRUE, 0);

	gtk_box_pack_start(GTK_BOX(self->hbox), GTK_WIDGET(frame_for_images),
			   TRUE, TRUE, 0);

	gtk_box_pack_start(GTK_BOX(self), GTK_WIDGET(self->hbox),
			   FALSE, TRUE, 0);

	IRRECO_RETURN
}

IrrecoButtonBrowserWidget
*irreco_button_browser_widget_new(IrrecoData *irreco_data)
{
	IrrecoButtonBrowserWidget *self;
	IrrecoStringTable *themes = irreco_data->theme_manager->themes;
	IRRECO_ENTER

	self = IRRECO_BUTTON_BROWSER_WIDGET(
		g_object_new (IRRECO_TYPE_BUTTON_BROWSER_WIDGET, NULL));

	self->irreco_data = irreco_data;

	if (irreco_string_table_lenght(themes) > 0) {

		const gchar *theme_name;
		irreco_button_browser_widget_populate_themes(self);
		irreco_listbox_set_selection(IRRECO_LISTBOX(self->themes), 0);

		irreco_string_table_index(themes, 0, &theme_name, NULL);

		g_string_printf(self->current_theme, "%s", theme_name);

		irreco_button_browser_widget_loader_start(self,
		G_SOURCEFUNC(irreco_button_browser_widget_loader_images), NULL);
	}

	IRRECO_RETURN_PTR(GTK_WIDGET(self));
}

/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/**
 * @name Private Functions
 * @{
 */

/**
 * Show hildon progressbar banner.
 *
 * This function will create a new banner if one has not been created yet,
 * if banner already exists, it's properties will be changed.
 *
 * @param text		Text to show.
 * @param fraction	Value of progress.
 */
static void
irreco_button_browser_widget_set_banner(IrrecoButtonBrowserWidget *self,
						const gchar *text,
						gdouble fraction)
{
	IRRECO_ENTER
	if (self->banner == NULL) {
		self->banner = hildon_banner_show_progress(
			GTK_WIDGET(self), NULL, "");
	}

	hildon_banner_set_text(HILDON_BANNER(self->banner), text);
	hildon_banner_set_fraction(HILDON_BANNER(self->banner), fraction);
	IRRECO_RETURN
}

/**
 * Destroy banner, if it exists.
 */
static void
irreco_button_browser_widget_hide_banner(IrrecoButtonBrowserWidget *self)
{
	IRRECO_ENTER
	if (self->banner) {
		gtk_widget_destroy(self->banner);
		self->banner = NULL;
	}
	IRRECO_RETURN
}

/**
 * Start a loader state machine if one is not running already.
 */
static void
irreco_button_browser_widget_loader_start(IrrecoButtonBrowserWidget *self,
						  GSourceFunc function,
						  GtkTreeIter *parent_iter)
{
	IRRECO_ENTER

	if (self->loader_func_id == 0) {
		if (function) {
			self->loader_func_id = g_idle_add(function, self);
		} else {
			IRRECO_ERROR("Loader function pointer not given.\n");
		}
	}

	IRRECO_RETURN
}

/**
 * Stop and cleanup loader if a loader is running.
 */
static void
irreco_button_browser_widget_loader_stop(IrrecoButtonBrowserWidget *self)
{
	IRRECO_ENTER
	if (self->loader_func_id != 0) {
		g_source_remove(self->loader_func_id);
		self->loader_func_id = 0;
		self->loader_state = 0;
	}
	IRRECO_RETURN
}

/**
 * Theme loader.
 *
 * This loader will request a list of themes from the IrrecoThemeManager and
 * update the TreeView accordingly.
 */

static gboolean irreco_button_browser_widget_populate_themes(
						IrrecoButtonBrowserWidget *self)
{
	IrrecoStringTable *themes = NULL;
	IrrecoThemeManager *theme_manager = self->irreco_data->theme_manager;
	IRRECO_ENTER

	themes = irreco_theme_manager_get_themes(theme_manager);

	if (themes != NULL) {
		IRRECO_STRING_TABLE_FOREACH_KEY(themes, key)
			irreco_listbox_text_append(self->themes, key, NULL);
		IRRECO_STRING_TABLE_FOREACH_END
	} else {
		irreco_error_dlg(GTK_WINDOW(self),
			"There's no themes installed");
	}

	IRRECO_RETURN_BOOL(TRUE);
}

/**
 * Background-image loader.
 *
 * This loader will request a list of background-images from the
 * IrrecoThemeManager and update the TreeView accordingly.
 */
static gboolean irreco_button_browser_widget_loader_images(
						IrrecoButtonBrowserWidget *self)
{
	IRRECO_ENTER

	switch (self->loader_state) {
	case LOADER_STATE_INIT:
		irreco_button_browser_widget_set_banner(self, _("Loading ..."), 0);
		self->loader_state = LOADER_STATE_LOOP;
		self->loader_index = 0;
		irreco_listbox_clear(IRRECO_LISTBOX(self->images));
		IRRECO_RETURN_BOOL(TRUE);

	case LOADER_STATE_LOOP: {
		gint theme_button_count;
		gfloat banner;
		const gchar *image_name;
		IrrecoThemeManager *manager = self->irreco_data->theme_manager;
		IrrecoTheme *theme;

		irreco_string_table_get(manager->themes,
					self->current_theme->str,
					(gpointer *) &theme);

		theme_button_count = irreco_string_table_lenght(theme->buttons);

		if (theme_button_count > 0) {
			IrrecoThemeButton *button_image;
			irreco_string_table_index(theme->buttons,
						self->loader_index,
						&image_name,
						(gpointer *) &button_image);

			irreco_listbox_image_append(self->images,
					button_image->name->str,
					button_image,
					button_image->image_up->str);
		} else {
			self->current_image = NULL;
			theme_button_count = 1;
		}

		self->loader_index++;
		banner = (gfloat)self->loader_index / (gfloat)theme_button_count;
		irreco_button_browser_widget_set_banner(self, _("Loading ..."),
						    banner);

		if(self->loader_index >= theme_button_count) {
			self->loader_state = LOADER_STATE_END;
		}

		IRRECO_RETURN_BOOL(TRUE);
		}

	case LOADER_STATE_END:
		irreco_button_browser_widget_hide_banner(self);
		irreco_button_browser_widget_loader_stop(self);
	}

	IRRECO_RETURN_BOOL(FALSE);
}

/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Public Functions                                                           */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */

/* Add functions here. */

/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Events and Callbacks                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Events and Callbacks
 * @{
 */

static void
irreco_button_browser_widget_destroy_event(IrrecoButtonBrowserWidget *self,
						   gpointer user_data)
{
	IRRECO_ENTER
	irreco_button_browser_widget_loader_stop(self);
	IRRECO_RETURN
}

static void irreco_button_browser_widget_theme_selection_changed(
						GtkTreeSelection *selection,
					        IrrecoButtonBrowserWidget *self)
{
	gint sel_index = -1;
	gchar *sel_label = NULL;
	gpointer sel_user_data = NULL;
	IRRECO_ENTER

	if (self->loader_func_id != 0) {
		irreco_button_browser_widget_loader_stop(self);
	}

	irreco_listbox_get_selection(IRRECO_LISTBOX(self->themes),
				     &sel_index, &sel_label, &sel_user_data);

	g_string_printf(self->current_theme, "%s", sel_label);

	irreco_button_browser_widget_loader_start(self,
		G_SOURCEFUNC(irreco_button_browser_widget_loader_images), NULL);

	g_free(sel_label);
	IRRECO_RETURN
}

static void irreco_button_browser_widget_image_selection_changed(
						GtkTreeSelection *selection,
					        IrrecoButtonBrowserWidget *self)
{
	gint sel_index = -1;
	gchar *sel_label = NULL;
	IrrecoThemeButton *button = NULL;
	IRRECO_ENTER

	if (irreco_listbox_get_selection(IRRECO_LISTBOX(self->images),
				&sel_index, &sel_label, (gpointer *)&button)) {
		self->current_image = button;
	} else {
		self->current_image = NULL;
	}

	g_free(sel_label);
	IRRECO_RETURN
}

/** @} */

/** @} */

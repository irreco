/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoCmdChainManager
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoCmdChainManager.
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_CMD_CHAIN_MANAGER_H_TYPEDEF__
#define __IRRECO_CMD_CHAIN_MANAGER_H_TYPEDEF__

typedef struct _IrrecoCmdChainManager IrrecoCmdChainManager;

#endif /* __IRRECO_CMD_CHAIN_MANAGER_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_CMD_CHAIN_MANAGER_H__
#define __IRRECO_CMD_CHAIN_MANAGER_H__
#include "irreco.h"
#include "irreco_cmd_chain.h"
#include "irreco_string_table.h"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
struct _IrrecoCmdChainManager {
	GHashTable *table;
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
IrrecoCmdChainManager*	irreco_cmd_chain_manager_new();
void 			irreco_cmd_chain_manager_free(
						IrrecoCmdChainManager *self);
IrrecoCmdChainId 	irreco_cmd_chain_manager_new_chain(
						IrrecoCmdChainManager *self);
gboolean 		irreco_cmd_chain_manager_new_chain_with_id(
						IrrecoCmdChainManager *self,
						IrrecoCmdChainId id);
void 			irreco_cmd_chain_manager_free_chain(
						IrrecoCmdChainManager *self,
						IrrecoCmdChainId id);
IrrecoCmdChain*		irreco_cmd_chain_manager_get_chain(
						IrrecoCmdChainManager *self,
						IrrecoCmdChainId id);
IrrecoCmdChainId 	irreco_cmd_chain_manager_find_chain(
						IrrecoCmdChainManager *self,
						IrrecoCmdChain *chain);
gboolean 		irreco_cmd_chain_manager_to_config(
						IrrecoCmdChainManager *self);
gboolean		irreco_cmd_chain_manager_from_config(
						IrrecoDirForeachData *dir_data, 
						IrrecoStringTable *list);


#endif /* __IRRECO_CMD_CHAIN_MANAGER_H__ */

/** @} */


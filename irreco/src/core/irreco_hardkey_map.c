/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_hardkey_map.h"

/**
 * @addtogroup IrrecoHardkeyMap
 * @ingroup Irreco
 *
 * @todo PURPOCE OF CLASS.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoHardkeyMap.
 */

/**
 * IrrecoHardkey
 */

gchar *irreco_hardkey_to_str(guint keyval)
{
	gchar *name = NULL;
	GString *string;
	IRRECO_ENTER

	switch (keyval) {
	case IRRECO_HARDKEY_UP:         name = "UP"; break;
	case IRRECO_HARDKEY_DOWN:       name = "DOWN"; break;
	case IRRECO_HARDKEY_LEFT:       name = "LEFT"; break;
	case IRRECO_HARDKEY_RIGHT:      name = "RIGHT"; break;
	case IRRECO_HARDKEY_SELECT:     name = "SELECT"; break;
	case IRRECO_HARDKEY_BACK:       name = "BACK"; break;
	case IRRECO_HARDKEY_MENU:	name = "MENU"; break;
	case IRRECO_HARDKEY_HOME:	name = "HOME"; break;
	case IRRECO_HARDKEY_FULLSCREEN: name = "FULLSCREEN"; break;
	case IRRECO_HARDKEY_PLUS:       name = "PLUS"; break;
	case IRRECO_HARDKEY_MINUS:      name = "MINUS"; break;
	}

	string = g_string_new(NULL);
	if (name) {
		g_string_printf(string, "%u %s", keyval, name);
	} else {
		g_string_printf(string, "%u", keyval);
	}
	IRRECO_RETURN_STR(g_string_free(string, FALSE));
}


/**
 * @typedef IrrecoHardkeyMap
 *
 * Store data assosiated with device keys. That is only IrrecoCmdChains at the
 * moment.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
static gboolean irreco_hardkey_map_free_foreach(gpointer key,
						gpointer value,
						gpointer user_data);
static gboolean irreco_hardkey_map_find_cmd_chain_id_foreach(gpointer key,
							     gpointer value,
							     gpointer data);
static void irreco_hardkey_map_chain_foreach_frapper(gpointer key,
						     gpointer value,
						     gpointer user_data);



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

typedef struct _IrrecoHardkeyMapForeachData IrrecoHardkeyMapForeachData;
struct _IrrecoHardkeyMapForeachData {
	IrrecoHardkeyMap     *self;
	IrrecoHardkeyMapFunc  func;
	gpointer              user_data;
};

typedef struct _IrrecoHardkeyMapFindChainData IrrecoHardkeyMapFindChainData;
struct _IrrecoHardkeyMapFindChainData {
	guint            *keyval;
	IrrecoCmdChainId id;
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

IrrecoHardkeyMap *irreco_hardkey_map_new(IrrecoCmdChainManager *manager)
{
	IrrecoHardkeyMap *self;
	IRRECO_ENTER

	self = g_slice_new0(IrrecoHardkeyMap);
	self->manager = manager;
	self->table = g_hash_table_new(NULL, NULL);
	IRRECO_RETURN_PTR(self);
}

void irreco_hardkey_map_free(IrrecoHardkeyMap *self)
{
	IRRECO_ENTER
	if (self == NULL) IRRECO_RETURN;
	g_hash_table_foreach_remove(self->table,
				    irreco_hardkey_map_free_foreach, self);
	g_hash_table_destroy(self->table);
	g_slice_free(IrrecoHardkeyMap, self);
	IRRECO_RETURN
}
static gboolean irreco_hardkey_map_free_foreach(gpointer key,
						gpointer value,
						gpointer user_data)
{
	irreco_cmd_chain_manager_free_chain(
		((IrrecoHardkeyMap *) user_data )->manager,
		(IrrecoCmdChainId) value );
	return TRUE;
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Functions                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Private Functions
 * @{
 */

/**
 * Make sure some IrrecoCmdChain is assosiated with a keyval.
 */
void irreco_hardkey_map_assosiate_chain(IrrecoHardkeyMap *self,
					guint keyval)
{
	IrrecoCmdChainId id;
	IRRECO_ENTER

	g_assert(self != NULL);

	id = (IrrecoCmdChainId) g_hash_table_lookup(self->table,
						    (gpointer) keyval);
	if (id == 0) {
		gchar *hardkey_str;
		hardkey_str = irreco_hardkey_to_str(keyval);
		IRRECO_DEBUG("Creating new chain for key \"%s\".\n",
			     hardkey_str);
		g_free(hardkey_str);
		id = irreco_cmd_chain_manager_new_chain(self->manager);
		g_hash_table_insert(self->table,
				    (gpointer) keyval,
				    (gpointer) id);
	}
	IRRECO_RETURN
}

/**
 * Assosiate IrrecoCmdChain with a particular keyval.
 */
gboolean irreco_hardkey_map_assosiate_chain_with_id(IrrecoHardkeyMap *self,
						    guint keyval,
						    IrrecoCmdChainId id)
{
	guint old_keyval;
	gchar *hardkey_str;
	IRRECO_ENTER

	g_assert(self != NULL);
	g_assert(id > 0);

	if (irreco_hardkey_map_find_cmd_chain_id(self, id, &old_keyval)) {

		gchar *keyval_str = irreco_hardkey_to_str(keyval);
		gchar *old_keyval_str = irreco_hardkey_to_str(old_keyval);

		if (keyval == old_keyval) {
			IRRECO_DEBUG("Keyval \"%s\" is already assosiated "
				     "with command chain \"%i\".\n",
				     keyval_str, id);
		} else {

			IRRECO_DEBUG("Id \"%i\" cannot be assosiated with "
				     "keyval \"%s\" because is is already "
				     "assosited with keyval \"%s\".\n",
				     id, keyval_str, old_keyval_str);
		}

		g_free(keyval_str);
		g_free(old_keyval_str);
		IRRECO_RETURN_BOOL(FALSE);
	}

	hardkey_str = irreco_hardkey_to_str(keyval);
	IRRECO_DEBUG("Creating new chain for key \"%s\" with id \"%i\".\n",
		     hardkey_str, id);
	g_free(hardkey_str);

	irreco_cmd_chain_manager_new_chain_with_id(self->manager, id);
	g_hash_table_insert(self->table,
			    (gpointer) keyval,
			    (gpointer) id);
	IRRECO_RETURN_BOOL(TRUE);
}

/**
 * Get a IrrecoCmdChain assosiated with a keyval.
 */
IrrecoCmdChain *irreco_hardkey_map_get_cmd_chain(IrrecoHardkeyMap *self,
						 guint keyval)
{
	gchar *hardkey_str;
	IrrecoCmdChain *chain;
	IrrecoCmdChainId id;
	IRRECO_ENTER

	g_assert(self != NULL);

	id = (IrrecoCmdChainId) g_hash_table_lookup(self->table,
						    (gpointer) keyval);
	chain = irreco_cmd_chain_manager_get_chain(self->manager, id);

	hardkey_str = irreco_hardkey_to_str(keyval);
	if (id == 0) {
		IRRECO_DEBUG("Hardkey \"%s\" is not assositead with a command "
			     "chain.\n", hardkey_str);
	} else {
		IRRECO_DEBUG("Hardkey \"%s\" is assositead with command "
			     "chain \"%i\".\n", hardkey_str, id);
	}
	g_free(hardkey_str);

	IRRECO_RETURN_PTR(chain);
}

/**
 * Find out which keyval is assosiated with some IrrecoCmdChainId.
 *
 * The assosiated keyval will se stored to keyval pointer.
 *
 * @return TRUE if found. FALSE if not assosiated.
 */
gboolean irreco_hardkey_map_find_cmd_chain_id(IrrecoHardkeyMap *self,
					      IrrecoCmdChainId id,
					      guint *keyval)
{
	IrrecoHardkeyMapFindChainData find_data;
	IRRECO_ENTER

	find_data.id = id;
	find_data.keyval = keyval;
	IRRECO_RETURN_BOOL(g_hash_table_find(self->table,
			  irreco_hardkey_map_find_cmd_chain_id_foreach,
			  &find_data) != NULL);
}
static gboolean irreco_hardkey_map_find_cmd_chain_id_foreach(gpointer key,
							     gpointer value,
							     gpointer data)
{
	IrrecoHardkeyMapFindChainData *find_data = data;
	if ((IrrecoCmdChainId) value == find_data->id) {
		if (find_data->keyval != NULL) *find_data->keyval = (guint) key;
		return TRUE;
	}
	return FALSE;
}

/**
 * Check if a IrrecoCmdChain has been assosiated with a keyval.
 */
gboolean irreco_hardkey_map_cmd_chain_exists(IrrecoHardkeyMap *self, guint keyval)
{
	IRRECO_ENTER
	IRRECO_RETURN_BOOL(g_hash_table_lookup(self->table, (gpointer) keyval)
			   != NULL);
}

/**
 * For iterating trough the map.
 */
void irreco_hardkey_map_chain_foreach(IrrecoHardkeyMap *self,
				      IrrecoHardkeyMapFunc func,
				      gpointer user_data)
{
	IrrecoHardkeyMapForeachData data;
	IRRECO_ENTER

	data.self = self;
	data.func = func;
	data.user_data = user_data;
	g_hash_table_foreach(self->table,
			     irreco_hardkey_map_chain_foreach_frapper, &data);
	IRRECO_RETURN
}
static void irreco_hardkey_map_chain_foreach_frapper(gpointer key,
						     gpointer value,
						     gpointer user_data)
{
	IrrecoCmdChain *chain;
	IrrecoHardkeyMapForeachData *data = user_data;
	IRRECO_ENTER

	chain = irreco_cmd_chain_manager_get_chain(data->self->manager,
						   (IrrecoCmdChainId) value);
	if (chain == NULL) IRRECO_RETURN;
	data->func(data->self,
		   (guint) key,
		   (IrrecoCmdChainId) value,
		   chain,
		   data->user_data);
	IRRECO_RETURN
}

/** @} */
/** @} */



























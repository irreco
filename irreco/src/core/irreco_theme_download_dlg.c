/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007,2008  Sami Parttimaa (t5pasa02@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_theme_download_dlg.h"
#include <hildon/hildon-banner.h>

/**
 * @addtogroup IrrecoThemeDownloadDlg
 * @ingroup Irreco
 *
 * @todo PURPOCE OF CLASS.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoThemeDownloadDlg.
 */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

static gboolean
irreco_theme_download_dlg_button_loader(IrrecoThemeDownloadDlg *self);
static gboolean irreco_theme_download_dlg_bg_loader(
					IrrecoThemeDownloadDlg *self);
static void irreco_theme_download_dlg_set_banner(IrrecoThemeDownloadDlg *self,
					const gchar *text, gdouble fraction);
static void irreco_theme_download_dlg_hide_banner(IrrecoThemeDownloadDlg *self);
static void irreco_theme_download_dlg_loader_stop(
					IrrecoThemeDownloadDlg *self);

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/** Loader states. */
enum
{
	LOADER_STATE_INIT,
	LOADER_STATE_LOOP,
	LOADER_STATE_END,
	LOADER_STATE_CLEANUP
};

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

G_DEFINE_TYPE (IrrecoThemeDownloadDlg, irreco_theme_download_dlg,
	       IRRECO_TYPE_DLG)



static void irreco_theme_download_dlg_dispose(GObject *object)
{
	G_OBJECT_CLASS(irreco_theme_download_dlg_parent_class)->dispose(object);
}



static void irreco_theme_download_dlg_finalize(GObject *object)
{
	G_OBJECT_CLASS(
		irreco_theme_download_dlg_parent_class)->finalize(object);
}



static void irreco_theme_download_dlg_class_init(
					IrrecoThemeDownloadDlgClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	object_class->dispose = irreco_theme_download_dlg_dispose;
	object_class->finalize = irreco_theme_download_dlg_finalize;
}



static void irreco_theme_download_dlg_init(IrrecoThemeDownloadDlg *self)
{
	IRRECO_ENTER

	self->label = gtk_label_new(NULL);

	self->theme_downloaded_successfully = FALSE;
	gtk_widget_set_size_request(GTK_WIDGET(self), 340, 200);

	gtk_window_set_title(GTK_WINDOW(self),
			     _("Download theme"));
	gtk_window_set_modal(GTK_WINDOW(self), TRUE);
	gtk_window_set_destroy_with_parent(GTK_WINDOW(self),
					   TRUE);
	gtk_dialog_set_has_separator(GTK_DIALOG(self), FALSE);
	gtk_dialog_add_buttons(GTK_DIALOG(self),
			       GTK_STOCK_OK, GTK_RESPONSE_OK,
			       NULL);

/*	self->radio1 = gtk_radio_button_new_with_label (NULL, "MMC1");
	self->radio2 = gtk_radio_button_new_with_label_from_widget(
				GTK_RADIO_BUTTON(self->radio1), "MMC2");*/


	gtk_box_pack_start_defaults(GTK_BOX(GTK_DIALOG(self)->vbox),
				    self->label);

/*	gtk_box_pack_start_defaults(GTK_BOX(
                              GTK_DIALOG(self)->vbox),
                                self->radio1);*/

	g_mkdir("/home/user/MyDocs/irreco", 0777);

/*	if(!irreco_is_dir("/home/user/MyDocs/irreco")) {
                gtk_widget_set_sensitive(self->radio1, FALSE);

		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(self->radio2),
					     TRUE);
        }

        if(irreco_is_dir("/media/mmc2")) {
	        gtk_box_pack_start_defaults(GTK_BOX(
				    GTK_DIALOG(self)->vbox),
				    self->radio2);
        }*/

	gtk_widget_show_all(GTK_WIDGET(self));

	IRRECO_RETURN
}







/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/



static gboolean
irreco_theme_download_dlg_button_loader(IrrecoThemeDownloadDlg *self)
{
	IRRECO_ENTER

	switch (self->loader_state) {
	case LOADER_STATE_INIT:{
		GString *path = g_string_new("");
		irreco_theme_download_dlg_set_banner(self,
						    _("Loading buttons.."),
						    0.0);
		self->loader_state = LOADER_STATE_LOOP;
		self->theme_loader_index = 0;

		g_string_printf(path, "%s%s/buttons/", self->theme_folder,
		self->webdb_theme->folder->str);

		IRRECO_DEBUG("mkdir %s\n", path->str);
		g_mkdir(path->str, 0777);
		g_string_free(path, TRUE);

		IRRECO_RETURN_BOOL(TRUE);
		}

	case LOADER_STATE_LOOP: {
		IrrecoStringTable *button_list = NULL;
		GString *path = g_string_new("");
		gfloat banner;
		const gchar *id;
		IrrecoWebdbCache *webdb_cache = irreco_data_get_webdb_cache(
							self->irreco_data,
							FALSE);
		/* Get buttons */
		irreco_webdb_cache_get_buttons(webdb_cache,
					       self->webdb_theme->id,
					       &button_list);
		if (button_list == NULL) {
			self->loader_state = LOADER_STATE_END;
			goto end;
		}

		g_string_printf(path, "%s%s/buttons/", self->theme_folder,
				self->webdb_theme->folder->str);

		if (irreco_string_table_index(button_list,
				(guint)self->theme_loader_index, &id, NULL) &&
			!g_str_equal(id, "0")) {
			irreco_webdb_cache_get_button_by_id(webdb_cache,
							    atoi(id),
							    path->str);
		} else {
			self->loader_state = LOADER_STATE_END;
			goto end;
		}

		banner = ++self->theme_loader_index;
		banner /= (gfloat) irreco_string_table_lenght (button_list);

		irreco_theme_download_dlg_set_banner(self,
						    _("Loading buttons.."),
						    banner);
		if (banner >= 1.0) {
			self->loader_state = LOADER_STATE_END;
		}

		end:
		g_string_free(path, TRUE);
		IRRECO_RETURN_BOOL(TRUE);
	}
	case LOADER_STATE_END:
		irreco_theme_download_dlg_hide_banner(self);
		irreco_theme_download_dlg_loader_stop(self);

		/* Get backgrounds */
		self->loader_func_id = g_idle_add(G_SOURCEFUNC(
			irreco_theme_download_dlg_bg_loader), self);
	}
	IRRECO_RETURN_BOOL(FALSE);
}


static gboolean irreco_theme_download_dlg_bg_loader(IrrecoThemeDownloadDlg *self)
{
	IRRECO_ENTER

	switch (self->loader_state) {
	case LOADER_STATE_INIT:{
		GString *path = g_string_new("");
		irreco_theme_download_dlg_set_banner(self,
						    _("Loading backgrounds..."),
						    0.0);
		self->loader_state = LOADER_STATE_LOOP;
		self->theme_loader_index = 0;

		g_string_printf(path, "%s%s/bg/", self->theme_folder,
		self->webdb_theme->folder->str);

		IRRECO_DEBUG("mkdir %s\n", path->str);
		g_mkdir(path->str, 0777);
		g_string_free(path, TRUE);

		IRRECO_RETURN_BOOL(TRUE);
		}

	case LOADER_STATE_LOOP: {
		IrrecoStringTable *bg_list = NULL;
		GString *path = g_string_new("");
		gfloat banner;
		const gchar *id;
		IrrecoWebdbCache *webdb_cache = irreco_data_get_webdb_cache(
							self->irreco_data,
							FALSE);
		/* Get backrounds */
		irreco_webdb_cache_get_backgrounds(webdb_cache,
						   self->webdb_theme->id,
						   &bg_list);

		g_string_printf(path, "%s%s/bg/", self->theme_folder,
				self->webdb_theme->folder->str);

		if (irreco_string_table_index(bg_list,
		    (guint)self->theme_loader_index, &id, NULL) &&
		    !g_str_equal(id, "0")) {
			irreco_webdb_cache_get_bg_by_id(webdb_cache, atoi(id),
							path->str);
		} else {
			self->loader_state = LOADER_STATE_END;
			goto end;
		}

		banner = ++self->theme_loader_index;
		banner /= (gfloat) irreco_string_table_lenght (bg_list);

		irreco_theme_download_dlg_set_banner(self,
						    _("Loading backgrounds..."),
						    banner);
		if (banner >= 1.0) {
			self->loader_state = LOADER_STATE_END;
		}
		end:
		g_string_free(path, TRUE);
		IRRECO_RETURN_BOOL(TRUE);
	}
	case LOADER_STATE_END:
		irreco_theme_download_dlg_hide_banner(self);
		irreco_theme_download_dlg_loader_stop(self);
		self->theme_downloaded_successfully = TRUE;
		gtk_dialog_response(GTK_DIALOG(self),
				    GTK_RESPONSE_DELETE_EVENT);
	}
	IRRECO_RETURN_BOOL(FALSE);
}



static void irreco_theme_download_dlg_set_banner(IrrecoThemeDownloadDlg *self,
						const gchar *text,
						gdouble fraction)
{
	IRRECO_ENTER

	if (self->banner == NULL) {
		self->banner = hildon_banner_show_progress(
			GTK_WIDGET(self), NULL, "");
	}

	hildon_banner_set_text(HILDON_BANNER(self->banner), text);
	hildon_banner_set_fraction(HILDON_BANNER(self->banner), fraction);

	IRRECO_RETURN
}

/**
 * Destroy banner, if it exists.
 */

static void irreco_theme_download_dlg_hide_banner(IrrecoThemeDownloadDlg *self)
{
	IRRECO_ENTER

	if (self->banner) {
		gtk_widget_destroy(self->banner);
		self->banner = NULL;
	}

	IRRECO_RETURN
}

static void irreco_theme_download_dlg_loader_stop(IrrecoThemeDownloadDlg *self)
{
	IRRECO_ENTER

	if (self->loader_func_id != 0) {
		g_source_remove(self->loader_func_id);
		self->loader_func_id = 0;
		self->loader_state = 0;
		if (self->loader_parent_iter) {
			gtk_tree_iter_free(self->loader_parent_iter);
		}
		self->loader_parent_iter = NULL;
	}
	IRRECO_RETURN
}

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Public Functions                                                           */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
IrrecoThemeDownloadDlg *irreco_theme_download_dlg_new(GtkWindow *parent)
{
	IrrecoThemeDownloadDlg *self;

	IRRECO_ENTER

	self = g_object_new(IRRECO_TYPE_THEME_DOWNLOAD_DLG, NULL);

	IRRECO_RETURN_PTR(self);
}


gboolean irreco_theme_download_dlg_run(IrrecoData *irreco_data,
				       gint theme_id,
				       GtkWindow *parent)
{
	IrrecoThemeDownloadDlg *self;
	IrrecoWebdbCache *webdb_cache = NULL;
	IrrecoTheme	*theme;
        gboolean theme_is_loaded = FALSE;
	gint delete_mode = 0;
	IrrecoTheme *same_theme = NULL;
	gchar *same_theme_folder = NULL;
	IrrecoThemeManager *theme_manager = irreco_data->theme_manager;
	GString *message = g_string_new(NULL);
	GString	*path = g_string_new(NULL);
	GString	*text = g_string_new(NULL);

	IRRECO_ENTER

	self = irreco_theme_download_dlg_new(parent);
	self->irreco_data = irreco_data;
	webdb_cache = irreco_data_get_webdb_cache(irreco_data, FALSE);

	if (!irreco_webdb_cache_get_theme_by_id(webdb_cache,
						theme_id,
						&self->webdb_theme)) {
		irreco_error_dlg(GTK_WINDOW(self),
				irreco_webdb_cache_get_error(webdb_cache));
		goto end;

	}
	/*self->webdb_theme = webdb_theme;*/
	g_string_printf(text, "Selected theme: %s\nwill be saved under MyDocs/irreco",
			self->webdb_theme->name->str);
	gtk_label_set_text(GTK_LABEL(self->label), text->str);
	g_string_free(text, FALSE);

	while (1) {
		delete_mode = 0;

		if (gtk_dialog_run(
		    GTK_DIALOG(self)) == GTK_RESPONSE_DELETE_EVENT){
			break;
		}
		if (self->loader_func_id != 0) {
			continue;
		}

		self->theme_folder = "/home/user/MyDocs/irreco/";

                if(!irreco_is_dir(self->theme_folder)) {
                        g_mkdir(self->theme_folder, 0777);
                }
		/* Create theme */
		g_string_printf(path, "%s%s/", self->theme_folder,
				self->webdb_theme->folder->str);
		IRRECO_STRING_TABLE_FOREACH_DATA(theme_manager->themes,
						 IrrecoTheme *, theme)
		/*Check theme name*/
		if (g_str_equal(self->webdb_theme->name->str,
		    theme->name->str)) {
			if (g_str_equal(theme->source->str, "deb")) {
				g_string_printf(message,
				_("The \"%s\" theme already exists.\n"
				"Can't overwrite \"Built In themes\"\n"
				"Please remove it first from the\n"
				"Application Manager"),
				theme->name->str);
				irreco_error_dlg(GTK_WINDOW(self),
						message->str);
				goto end;
			}
			delete_mode += 1;
			same_theme = theme;
		}
		/*Check theme folder path*/
		else if (g_str_equal(path->str,
			 g_strconcat(theme->path->str, "/", NULL))){
			if (g_str_equal(theme->source->str, "deb")) {
				g_string_printf(message,
				_("This theme replaces \"%s\" theme.\n"
				"Can't overwrite \"Built In themes\"\n"
				"Please remove it first from the\n"
				"Application Manager"),
				theme->name->str);

				irreco_error_dlg(GTK_WINDOW(self),
						 message->str);
				goto end;
			}
			delete_mode += 2;
			same_theme_folder = g_strdup(theme->name->str);
		}

		IRRECO_STRING_TABLE_FOREACH_END

		if (delete_mode == 1) {
			g_string_printf(message,
			_("The \"%s\" theme already exists.\n"
			  "You are replacing version:\n"
			  "%s\nwith version:\n%s\n\n"
			  "Do you want to continue?"),
			  same_theme->name->str, same_theme->version->str,
			  self->webdb_theme->uploaded->str);
		} else if (delete_mode == 2) {
			g_string_printf(message,
			_("This theme replaces \"%s\" theme.\n"
			  "Do you want to continue?"),
       			  same_theme_folder);
		} else if (delete_mode == 3) {
			g_string_printf(message,
			_("This theme replaces themes\n"
			  "\"%s\" and \"%s\"\n"
			  "Do you want to continue?"),
			  same_theme->name->str,
			  same_theme_folder);
		}
		/* Check whether a theme folder already exists */
		if (delete_mode != 0) {
			/* Create dialog*/
			if (!irreco_yes_no_dlg(
			    GTK_WINDOW(self), message->str)) {
				continue;
			} else {
				/* Remove theme folder and then make it again*/
				if (delete_mode == 1 || delete_mode == 3) {
					gchar *rm_cmd = g_strconcat("rm -r ",
						same_theme->path->str, NULL);
					system(rm_cmd);
					g_free(rm_cmd);
				}
				if (delete_mode == 2 || delete_mode == 3) {
					irreco_theme_manager_remove_theme(
					irreco_data->theme_manager,
							same_theme_folder);
					g_free(same_theme_folder);
				}
			}
		}

		/*Create folder */
		g_mkdir(path->str, 0777);

		/*Create new theme*/
		if (delete_mode == 0 || delete_mode == 2) {
			theme = irreco_theme_new();
			irreco_theme_set(theme,
				self->webdb_theme->name->str,
				path->str, "web",
				self->webdb_theme->creator->str,
				self->webdb_theme->comment->str,
				self->webdb_theme->preview_button_name->str,
				self->webdb_theme->uploaded->str);

			irreco_theme_update_keyfile(theme);
			irreco_theme_free(theme);
		} else {
			irreco_theme_set(same_theme,
				self->webdb_theme->name->str,
				path->str, "web",
				self->webdb_theme->creator->str,
				self->webdb_theme->comment->str,
				self->webdb_theme->preview_button_name->str,
				self->webdb_theme->uploaded->str);
		}

		/* Get buttons and backgrounds */
		self->loader_func_id = g_idle_add(G_SOURCEFUNC(
			irreco_theme_download_dlg_button_loader), self);

	}

	end:
	g_string_free(message, TRUE);
	g_string_free(path, TRUE);

	irreco_theme_download_dlg_hide_banner(self);
	irreco_theme_download_dlg_loader_stop(self);

	/*if(self != NULL) {
		gtk_widget_destroy(GTK_WIDGET(self));
		self = NULL;

        }*/

	theme_is_loaded = self->theme_downloaded_successfully;
	gtk_widget_destroy(GTK_WIDGET(self));

	IRRECO_RETURN_BOOL(theme_is_loaded);
}



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Events and Callbacks                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/



/** @} */

/** @} */


/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_window_user.h"
#include "irreco_button.h"
#include "irreco_window_edit.h"
#include "irreco_input_dlg.h"
#include "irreco_config.h"
#include "irreco_webdb_upload_dlg.h"
#include </usr/include/hildon-1/hildon/hildon-button.h>
#include "irreco_select_remote_dlg.h"

/**
 * @addtogroup IrrecoWindowUser
 * @ingroup Irreco
 *
 * User interface for using button layouts.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoWindowUser.
 */


void irreco_window_user_draw_background(IrrecoWindowUser * user_window);
void irreco_window_user_main_menu_create(IrrecoWindowUser * user_window);
void irreco_window_user_main_menu_download(GtkMenuItem * menuitem,
					   IrrecoWindowUser * user_window);
void irreco_window_user_new_remote(GtkMenuItem * menuitem, IrrecoWindowUser * user_window);
void irreco_window_user_edit_remote(GtkMenuItem * menuitem, IrrecoWindowUser * user_window);
void irreco_window_user_main_menu_rename(GtkMenuItem * menuitem,
					 IrrecoWindowUser * user_window);
void irreco_window_user_main_menu_upload(GtkMenuItem * menuitem,
					 IrrecoWindowUser * user_window);
void irreco_window_user_delete_remote(GtkMenuItem * menuitem,
				      IrrecoWindowUser * user_window);
void irreco_window_user_show_remote(GtkButton *button, IrrecoWindowUser * user_window);
void irreco_window_user_menu_about(GtkMenuItem * menuitem, IrrecoWindowUser * user_window);
void irreco_window_user_button_release(IrrecoButton * irreco_button,
				       IrrecoWindowUser * user_window);
void irreco_window_user_execute_start(IrrecoWindowUser * user_window,
				      IrrecoCmdChain * cmd_chain,
				      IrrecoButton * irreco_button);
void irreco_window_user_execute_finish(IrrecoCmdChain * cmd_chain,
				       IrrecoWindowUser * user_window);
gboolean irreco_window_user_key_press(GtkWidget * widget, GdkEventKey * event,
				       IrrecoWindowUser * user_window);
gboolean irreco_window_user_window_state_event(GtkWidget *widget,
					       GdkEventWindowState *event,
					       IrrecoWindowUser *user_window);


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Public api.                                                                */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public api
 * @{
 */

/**
 * Crate and display IrrecoWindowUser.
 *
 * @param irreco_layout  IrrecoButtonLayout to display. If NULL, attempts to
 *                       display first layout from
 *                       irreco_data->irreco_layout_array.
 */
IrrecoWindowUser *irreco_window_user_create(IrrecoWindowManager * manager)
{
	IrrecoData *irreco_data = manager->irreco_data;
	IrrecoWindowUser *user_window;
	IRRECO_ENTER

	user_window = g_slice_new0(IrrecoWindowUser);
	user_window->manager = manager;
	user_window->irreco_data = irreco_data;
	user_window->window = irreco_window_create();

	irreco_window_user_main_menu_create(user_window);
/*	irreco_window_user_create_show_remote_menu(user_window);*/

	g_signal_connect(G_OBJECT(user_window->window),
			 "key-press-event",
			 G_CALLBACK(irreco_window_user_key_press),
			 user_window);
	g_signal_connect(G_OBJECT(user_window->window),
			 "window-state-event",
			 G_CALLBACK(irreco_window_user_window_state_event),
			 user_window);
	IRRECO_RETURN_PTR(user_window);
}

void irreco_window_user_destroy(IrrecoWindowUser * user_window)
{
	IRRECO_ENTER
	if (user_window->irreco_layout != NULL) {
		irreco_button_layout_reset(user_window->irreco_layout);
		IRRECO_DEBUG_LINE
	}
	irreco_window_destroy(user_window->window);
	g_slice_free(IrrecoWindowUser, user_window);
	IRRECO_RETURN
}

/**
 * Set and display layout, or, if NULL is given, unset layout.
 */
void irreco_window_user_set_layout(IrrecoWindowUser * user_window,
				   IrrecoButtonLayout * layout)
{
	IRRECO_ENTER

	if (user_window->irreco_layout != NULL) {
		IRRECO_DEBUG_LINE
		irreco_button_layout_reset(user_window->irreco_layout);
	}

	IRRECO_DEBUG_LINE

	if (irreco_string_table_is_empty(
		user_window->irreco_data->irreco_layout_array)) {
		gtk_widget_set_sensitive(
			GTK_WIDGET(user_window->menu_show_remote), FALSE);
	} else {
		gtk_widget_set_sensitive(
			GTK_WIDGET(user_window->menu_show_remote), TRUE);
	}

	if (layout == NULL) {
		IRRECO_PRINTF("Layout unset.\n");
		user_window->irreco_layout = NULL;

		/* Disable menu entries. */
		gtk_widget_set_sensitive(
			GTK_WIDGET(user_window->menu_edit_remote), FALSE);
		gtk_widget_set_sensitive(
			GTK_WIDGET(user_window->menu_rename_remote), FALSE);
		gtk_widget_set_sensitive(
			GTK_WIDGET(user_window->menu_upload_remote), FALSE);
		gtk_widget_set_sensitive(
			GTK_WIDGET(user_window->menu_delete_remote), FALSE);
		IRRECO_RETURN

	} else {
		/* Enable menu entries. */
		gtk_widget_set_sensitive(
			GTK_WIDGET(user_window->menu_edit_remote), TRUE);
		gtk_widget_set_sensitive(
			GTK_WIDGET(user_window->menu_rename_remote), TRUE);
		gtk_widget_set_sensitive(
			GTK_WIDGET(user_window->menu_upload_remote), TRUE);
		gtk_widget_set_sensitive(
			GTK_WIDGET(user_window->menu_delete_remote), TRUE);

	}

	IRRECO_PRINTF("Displaying layout \"%s\".\n",
		      irreco_button_layout_get_name(layout));
	irreco_button_layout_reset(layout);
	IRRECO_DEBUG_LINE
	user_window->irreco_layout = layout;
	IRRECO_DEBUG_LINE
	irreco_button_layout_set_container(layout, user_window->window->layout);
	IRRECO_DEBUG_LINE
	irreco_button_layout_create_widgets(layout);
	irreco_button_layout_set_press_callback(layout, irreco_button_down);
	irreco_button_layout_set_release_callback(
		layout, irreco_window_user_button_release);
	irreco_button_layout_set_callback_data(layout, user_window);
	irreco_window_user_draw_background(user_window);
	IRRECO_RETURN
}

void irreco_window_user_draw_background(IrrecoWindowUser * user_window)
{
	const gchar *image;
	const GdkColor *color;
	IRRECO_ENTER
	irreco_button_layout_get_bg(user_window->irreco_layout, &image, &color);
	irreco_window_set_background_image(user_window->window, color,
					   image, NULL);
	IRRECO_RETURN

}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Callback.                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Callback
 * @{
 */

/**
 * Button release callback handler.
 */
void irreco_window_user_button_release(IrrecoButton * irreco_button,
				       IrrecoWindowUser * user_window)
{
	IRRECO_ENTER
	irreco_window_user_execute_start(
		user_window,
		irreco_button_get_cmd_chain(irreco_button),
		irreco_button);

	IRRECO_RETURN
}

void irreco_window_user_execute_start(IrrecoWindowUser * user_window,
				      IrrecoCmdChain * cmd_chain,
				      IrrecoButton * irreco_button)
{
	IRRECO_ENTER
	if (irreco_cmd_chain_execute(cmd_chain, user_window->irreco_data,
				     G_FUNC(irreco_window_user_execute_finish),
				     user_window)) {
		user_window->execution_button = irreco_button;
		user_window->executing_cmd_chain = TRUE;
		gtk_widget_set_sensitive(user_window->menu, FALSE);
		gtk_widget_set_sensitive(user_window->window->scrolled_window,
					 FALSE);

	} else if (irreco_button != NULL) {
		irreco_button_up(irreco_button);
	}
	IRRECO_RETURN
}

void irreco_window_user_execute_finish(IrrecoCmdChain * cmd_chain,
				       IrrecoWindowUser * user_window)
{
	IRRECO_ENTER

	gtk_widget_set_sensitive(user_window->menu, TRUE);
	gtk_widget_set_sensitive(user_window->window->scrolled_window, TRUE);
	if (user_window->execution_button != NULL) {
		irreco_button_up(user_window->execution_button);
	}

	user_window->executing_cmd_chain = FALSE;
	user_window->execution_button = NULL;

	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Menu.                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Menu
 * @{
 */

void irreco_window_user_main_menu_create(IrrecoWindowUser * user_window)
{
	IRRECO_ENTER

	/* Build menu. */
	user_window->menu = GTK_WIDGET( hildon_app_menu_new() );
	user_window->menu_new_remote = hildon_button_new_with_text(2 | 4, 0, NULL, "New remote");
	user_window->menu_download_remote = hildon_button_new_with_text(2 | 4, 0, NULL, "Download remote");
	user_window->menu_edit_remote = hildon_button_new_with_text(2 | 4, 0, NULL, "Edit remote");
	user_window->menu_rename_remote = hildon_button_new_with_text(2 | 4, 0, NULL, "Rename remote");
	user_window->menu_upload_remote = hildon_button_new_with_text(2 | 4, 0, NULL, "Upload remote");
	user_window->menu_delete_remote = hildon_button_new_with_text(2 | 4, 0, NULL, "Delete remote");
	user_window->menu_show_remote = hildon_button_new_with_text(2 | 4, 0, NULL, "Select remote");
	user_window->menu_about = hildon_button_new_with_text(2 | 4, 0, NULL, "About Irreco");

	hildon_app_menu_append(HILDON_APP_MENU(user_window->menu),
				GTK_BUTTON(user_window->menu_new_remote));
	hildon_app_menu_append(HILDON_APP_MENU(user_window->menu),
				GTK_BUTTON(user_window->menu_download_remote));
	hildon_app_menu_append(HILDON_APP_MENU(user_window->menu),
				GTK_BUTTON(user_window->menu_edit_remote));
	hildon_app_menu_append(HILDON_APP_MENU(user_window->menu),
				GTK_BUTTON(user_window->menu_upload_remote));
	hildon_app_menu_append(HILDON_APP_MENU(user_window->menu),
				GTK_BUTTON(user_window->menu_rename_remote));
	hildon_app_menu_append(HILDON_APP_MENU(user_window->menu),
				GTK_BUTTON(user_window->menu_show_remote));
	hildon_app_menu_append(HILDON_APP_MENU(user_window->menu),
				GTK_BUTTON(user_window->menu_delete_remote));
	hildon_app_menu_append(HILDON_APP_MENU(user_window->menu),
				GTK_BUTTON(user_window->menu_about));

	/* Show menu. */
	hildon_window_set_app_menu(HILDON_WINDOW(user_window->window),
					HILDON_APP_MENU(user_window->menu));
	gtk_widget_show_all(user_window->menu);

	/* Connect signals. */
	g_signal_connect(G_OBJECT(user_window->menu_new_remote),
			 "clicked",
			 G_CALLBACK(irreco_window_user_new_remote),
			 user_window);
	g_signal_connect(G_OBJECT(user_window->menu_download_remote),
			 "clicked",
			 G_CALLBACK(irreco_window_user_main_menu_download),
			 user_window);
	g_signal_connect(G_OBJECT(user_window->menu_edit_remote),
			 "clicked",
			 G_CALLBACK(irreco_window_user_edit_remote),
			 user_window);
	g_signal_connect(G_OBJECT(user_window->menu_rename_remote),
			 "clicked",
			 G_CALLBACK(irreco_window_user_main_menu_rename),
			 user_window);
	g_signal_connect(G_OBJECT(user_window->menu_upload_remote),
			 "clicked",
			 G_CALLBACK(irreco_window_user_main_menu_upload),
			 user_window);
	g_signal_connect(G_OBJECT(user_window->menu_delete_remote),
			 "clicked",
			 G_CALLBACK(irreco_window_user_delete_remote),
			 user_window);
	g_signal_connect(G_OBJECT(user_window->menu_show_remote),
			 "clicked",
			 G_CALLBACK(irreco_window_user_show_remote),
			 user_window);
	g_signal_connect(G_OBJECT(user_window->menu_about),
			 "clicked",
			 G_CALLBACK(irreco_window_user_menu_about),
			 user_window);
	IRRECO_RETURN
}

/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Main menu callback functions.                                              */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Main menu callback functions
 * @{
 */

/**
 * Create dialog window and ask the user for remote name. If user gives the
 * remote name and clicks create, a new IrrecoButtonLayout and edit user interface
 * will be created.
 */
void irreco_window_user_new_remote(GtkMenuItem * menuitem, IrrecoWindowUser * user_window)
{
	IrrecoData *irreco_data = user_window->irreco_data;
	GtkWindow *parent = irreco_window_get_gtk_window(user_window->window);
	IRRECO_ENTER
	if (irreco_window_user_create_new_remote(irreco_data, parent)) {
		irreco_window_manager_show(irreco_data->window_manager,
					   IRRECO_WINDOW_EDIT);
	}

	IRRECO_RETURN
}

void irreco_window_user_main_menu_download(GtkMenuItem * menuitem,
					   IrrecoWindowUser * user_window)
{
	IRRECO_ENTER

	irreco_show_remote_download_dlg(user_window->irreco_data,
					irreco_window_get_gtk_window(
					user_window->window));

	IRRECO_RETURN
}

void irreco_window_user_edit_remote(GtkMenuItem * menuitem, IrrecoWindowUser * user_window)
{
	IrrecoData *irreco_data = user_window->irreco_data;
	IRRECO_ENTER

	if (irreco_string_table_is_empty(irreco_data->irreco_layout_array)) {
		irreco_info_dlg(irreco_window_get_gtk_window(user_window->window),
			        _(IRRECO_NO_REMOTE_HELP));
	} else {
		irreco_window_manager_set_layout(user_window->manager,
						 user_window->irreco_layout);
		irreco_window_manager_show(user_window->manager,
					   IRRECO_WINDOW_EDIT);
	}

	IRRECO_RETURN
}

void irreco_window_user_main_menu_rename(GtkMenuItem * menuitem,
					 IrrecoWindowUser * user_window)
{
	IrrecoInputDlg *input;
	IRRECO_ENTER

	IRRECO_PRINTF("%p\n", (void *) user_window);

	/* Create input dialog. */
	input = IRRECO_INPUT_DLG(irreco_input_dlg_new(
				     irreco_window_get_gtk_window(
				     user_window->window), _("Rename remote")));
	irreco_input_dlg_set_label(input, _("Name"));
	irreco_input_dlg_set_entry(input, irreco_button_layout_get_name(
				   user_window->irreco_layout));

	/* Loop until cancel or successfull rename. */
	do {
		if (irreco_show_input_dlg(input) == FALSE) break;
		if (irreco_string_table_change_key(
			user_window->irreco_data->irreco_layout_array,
			irreco_button_layout_get_name(
			user_window->irreco_layout),
			irreco_input_dlg_get_entry(input)) == TRUE) break;
		irreco_error_dlg(GTK_WINDOW(input),
				 _(IRRECO_LAYOUT_NAME_COLLISION));
	} while (TRUE);

	irreco_config_save_layouts(user_window->irreco_data);
/*	irreco_window_user_create_show_remote_menu(user_window);*/
	gtk_widget_destroy(GTK_WIDGET(input));
	IRRECO_RETURN
}

void irreco_window_user_main_menu_upload(GtkMenuItem * menuitem,
					 IrrecoWindowUser * user_window)
{
	IRRECO_ENTER

	irreco_show_remote_upload_dlg(user_window->irreco_data,
				      irreco_window_get_gtk_window(
				      user_window->window));

	IRRECO_RETURN
}

void irreco_window_user_delete_remote(GtkMenuItem * menuitem,
				      IrrecoWindowUser * user_window)
{
	guint index;
	IrrecoButtonLayout *irreco_layout = NULL;
	IrrecoData * irreco_data = user_window->irreco_data;
	IRRECO_ENTER

	/* Do we have anything to destroy? */
	if (user_window->irreco_layout == NULL) {
		irreco_info_dlg(irreco_window_get_gtk_window(
				user_window->window),
			        _(IRRECO_NO_REMOTE_HELP));

	/* If we can get the index of the current layout, and user agrees,
	   then destroy the layout. */
	} else if(
	    irreco_string_table_get_index(irreco_data->irreco_layout_array,
	    				  user_window->irreco_layout, &index) &&
	    irreco_yes_no_dlg(irreco_window_get_gtk_window(user_window->window),
			      _("Delete remote?"))) {

		IrrecoButtonLayout * layout = user_window->irreco_layout;
		IRRECO_PRINTF("Destroying layout: \"%s\" from index %u\n",
			      irreco_button_layout_get_name(layout), index);

		irreco_window_manager_set_layout(user_window->manager, NULL);
		irreco_string_table_remove_by_data(
			irreco_data->irreco_layout_array, layout);
		irreco_config_save_layouts(irreco_data);

		if (index > 0) index--;
		IRRECO_PRINTF("Displaying layout from index %u\n", index);
		irreco_string_table_index(irreco_data->irreco_layout_array,
					  index, NULL,
					  (gpointer *) &irreco_layout);
		irreco_window_manager_set_layout(user_window->manager,
						 irreco_layout);
	}

	IRRECO_RETURN
}

/**
 * Pop up a info message if the user has not created any remotes.
 */

void irreco_window_user_show_remote(GtkButton *button,
				    IrrecoWindowUser *user_window)
{
	IRRECO_ENTER

	if (irreco_string_table_is_empty(
		user_window->irreco_data->irreco_layout_array)) {
		irreco_info_dlg(irreco_window_get_gtk_window(
				user_window->window),
			        _(IRRECO_NO_REMOTE_HELP));
	} else {
		irreco_show_select_remote_dlg(user_window);
	}

	IRRECO_RETURN
}

void irreco_window_user_menu_about(GtkMenuItem * menuitem,
				   IrrecoWindowUser * user_window)
{
	GError *error = NULL;
	GdkPixbuf *icon = NULL;
	GString *license = NULL;
	const gchar *authors[] = { "Arto Karppinen <arto.karppinen@iki.fi>\n"
				"Joni Kokko <t5kojo01@students.oamk.fi>\n"
				"Harri Vattulainen <t5vaha01@students.oamk.fi>\n"
				"Sami Mäki <kasmra@xob.kapsi.fi>\n"
				"Sampo Savola <samposav@paju.oulu.fi>\n"
				"Sami Parttimaa <t5pasa02@students.oamk.fi>\n"
				"Pekka Gehör <pegu6@msn.com>\n",
				    NULL };
	IRRECO_ENTER

	license = g_string_new(NULL);
	g_string_append(license,
		"This program is free software; you can redistribute it and/or "
		"modify it under the terms of the GNU General Public License "
		"as published by the Free Software Foundation; either version 2 "
		"of the License, or (at your option) any later version. "
		"\n");
	g_string_append(license,
		"This program is distributed in the hope that it will be useful, "
		"but WITHOUT ANY WARRANTY; without even the implied warranty of "
		"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the "
		"GNU General Public License for more details. "
		"\n");
	g_string_append(license,
		"You should have received a copy of the GNU General Public "
		"License along with this program; if not, write to the Free "
		"Software Foundation, Inc., 51 Franklin Street, Fifth Floor, "
		"Boston, MA  02110-1301, USA.");

	IRRECO_PRINTF("Loading icon: \"%s\".\n", IRRECO_ICON);
	icon = gdk_pixbuf_new_from_file(IRRECO_ICON, &error);
	if (error != NULL) g_error_free(error);

	gtk_show_about_dialog(
		irreco_window_manager_get_gtk_window(user_window->manager),
		"authors",		authors,
		"license",		license->str,
		"logo",			icon,
		"version",		VERSION,
		"website",		"http://irreco.garage.maemo.org",
		"wrap-license",		TRUE,
		NULL);
	g_string_free(license, TRUE);
	IRRECO_RETURN
}

gboolean irreco_window_user_key_press(GtkWidget * widget, GdkEventKey * event,
				      IrrecoWindowUser * user_window)
{
	IrrecoCmdChain *chain;
	IRRECO_ENTER

	if (user_window->irreco_layout != NULL) {
		if (event->keyval == IRRECO_HARDKEY_MENU ||
		    event->keyval == IRRECO_HARDKEY_HOME ) {
			gchar *hardkey = irreco_hardkey_to_str(event->keyval);
			IRRECO_PRINTF("Ignoring keyval \"%s\"\n", hardkey);
			g_free(hardkey);
		} else {
			chain = irreco_hardkey_map_get_cmd_chain(
				user_window->irreco_layout->hardkey_map,
				event->keyval);
			if (chain != NULL) {
				irreco_window_user_execute_start(
					user_window, chain, NULL);
			}
		}
	}
	IRRECO_RETURN_BOOL(FALSE);
}

gboolean irreco_window_user_window_state_event(GtkWidget *widget,
					       GdkEventWindowState *event,
					       IrrecoWindowUser *user_window)
{
	IRRECO_ENTER

	/* Syncronize menu option state with window fullscreen state. */
	if (event->changed_mask & GDK_WINDOW_STATE_FULLSCREEN) {
		g_signal_handler_block(user_window->menu_fullscreen,
			user_window->menu_fullscreen_handler);

		if (event->new_window_state & GDK_WINDOW_STATE_FULLSCREEN) {
			gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(
				user_window->menu_fullscreen), TRUE);
		} else {
			gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(
				user_window->menu_fullscreen), FALSE);
		}

		g_signal_handler_unblock(user_window->menu_fullscreen,
			user_window->menu_fullscreen_handler);
	}

	IRRECO_RETURN_BOOL(FALSE);
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Functions                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */

/**
 * Create dialog window and ask the user for remote name. If user gives the
 * remote name and clicks create, a new IrrecoButtonLayout and edit user interface
 * will be created.
 */
gboolean irreco_window_user_create_new_remote(IrrecoData *irreco_data,
					      GtkWindow *parent)
{
	gboolean loop = TRUE;
	IrrecoInputDlg *input_dlg;
	gboolean remote_created = FALSE;
	IRRECO_ENTER

	/* Create input dialog. */
	input_dlg = IRRECO_INPUT_DLG(irreco_input_dlg_new(parent,
							  _("New remote")));
	irreco_input_dlg_set_label(input_dlg, _("Name"));

	/* Loop until we get a name, or user cancels. */
	while (loop == TRUE && irreco_show_input_dlg(input_dlg)) {
		const gchar *name = irreco_input_dlg_get_entry(input_dlg);

		/* Does it exist already? */
		if (irreco_string_table_exists(irreco_data->irreco_layout_array,
					       name)) {
			irreco_error_dlg(GTK_WINDOW(input_dlg),
				         _(IRRECO_LAYOUT_NAME_COLLISION));

		/* Create new layout. */
		} else {
			IrrecoButtonLayout *layout;
			IrrecoCmdChain *chain;
			IrrecoCmd *command;

			/* Create button layout. */
			layout = irreco_button_layout_create(
				NULL, irreco_data->cmd_chain_manager);
			irreco_button_layout_set_name(layout, name);
			irreco_string_table_add(
				irreco_data->irreco_layout_array,
				irreco_button_layout_get_name(layout),
				layout);
			irreco_string_table_sort_abc(
				irreco_data->irreco_layout_array);

			/* Attach fullscreen command to fullscreen button. */
			irreco_hardkey_map_assosiate_chain(
				layout->hardkey_map, IRRECO_HARDKEY_FULLSCREEN);
			chain = irreco_hardkey_map_get_cmd_chain(
				layout->hardkey_map, IRRECO_HARDKEY_FULLSCREEN);
			command = irreco_cmd_create();
			irreco_cmd_set_builtin(
				command, IRRECO_COMMAND_FULLSCREEN_TOGGLE);
			irreco_cmd_chain_append(chain, command);

			/* Attach next remote command to plus button. */
			irreco_hardkey_map_assosiate_chain(
				layout->hardkey_map, IRRECO_HARDKEY_PLUS);
			chain = irreco_hardkey_map_get_cmd_chain(
				layout->hardkey_map, IRRECO_HARDKEY_PLUS);
			command = irreco_cmd_create();
			irreco_cmd_set_builtin(
				command, IRRECO_COMMAND_NEXT_REMOTE);
			irreco_cmd_chain_append(chain, command);

			/* Attach previous remote command to minus button. */
			irreco_hardkey_map_assosiate_chain(
				layout->hardkey_map, IRRECO_HARDKEY_MINUS);
			chain = irreco_hardkey_map_get_cmd_chain(
				layout->hardkey_map, IRRECO_HARDKEY_MINUS);
			command = irreco_cmd_create();
			irreco_cmd_set_builtin(
				command, IRRECO_COMMAND_PREVIOUS_REMOTE);
			irreco_cmd_chain_append(chain, command);

			/* Set layout. */
			irreco_window_manager_set_layout(
				irreco_data->window_manager, layout);
			loop = FALSE;
			remote_created = TRUE;
		}
	}

	gtk_widget_destroy(GTK_WIDGET(input_dlg));
	IRRECO_RETURN_BOOL(remote_created);
}

/** @} */
/** @} */

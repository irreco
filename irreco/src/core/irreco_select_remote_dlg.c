/*
 * irreco - Ir Remote Control
 * Copyright (C) 2008 Harri Vattulainen
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_select_remote_dlg.h"
#include "irreco_config.h"
#include <hildon/hildon.h>

/**
 * @addtogroup IrrecoSelectRemoteDlg
 * @ingroup Irreco
 *
 * @todo PURPOCE OF CLASS.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoSelectRemoteDlg.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
void remote_selected(HildonTouchSelector * selector, gint column, gpointer user_data);
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Functions                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */

void irreco_show_select_remote_dlg(IrrecoWindowUser *user_window)
{
	SelectRemoteGeneral *self = NULL;
	self = g_new0(SelectRemoteGeneral, 1);
	gint active = 0;

	IRRECO_ENTER

	/* FIXME */
	/* Setting parent window ? */

	/* Set irreco data */
	self->irreco_data = user_window->irreco_data;

	/*  Dialog creation */
	self->remote_dialog = gtk_dialog_new();
	gtk_window_set_title(GTK_WINDOW(self->remote_dialog),"Change remote");
	gtk_widget_set_size_request(self->remote_dialog, 800, 380);

	/* Touch selector */
	self->remote_selector = NULL;
	self->remote_selector = hildon_touch_selector_new_text();
	hildon_touch_selector_set_column_selection_mode (HILDON_TOUCH_SELECTOR (self->remote_selector),
			HILDON_TOUCH_SELECTOR_SELECTION_MODE_SINGLE);
	/* Populate touch selector */
	IRRECO_STRING_TABLE_FOREACH(self->irreco_data->irreco_layout_array,
					key,
					IrrecoButtonLayout *,
					layout)
		g_print("Append name: %s\n", layout->name->str);
		hildon_touch_selector_append_text (HILDON_TOUCH_SELECTOR(self->remote_selector),
						   layout->name->str);
		/* Find current layout */
		if(g_strcmp0(layout->name->str, self->irreco_data->window_manager->current_layout->name->str) == 0) {
			hildon_touch_selector_set_active(HILDON_TOUCH_SELECTOR(self->remote_selector),
					0, active);
		}
		active++;
	IRRECO_STRING_TABLE_FOREACH_END
	hildon_touch_selector_center_on_selected(HILDON_TOUCH_SELECTOR(self->remote_selector));
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG(self->remote_dialog)->vbox),self->remote_selector);

	gtk_widget_show_all(self->remote_dialog);

	g_signal_connect (G_OBJECT (self->remote_selector), "changed",
			  G_CALLBACK(remote_selected), self);

	IRRECO_RETURN
}

/** @} */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Events and Callbacks                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Events and Callbacks
 * @{
 */

void remote_selected(HildonTouchSelector * selector, gint column, gpointer user_data)
{
	SelectRemoteGeneral *self = (SelectRemoteGeneral *)user_data;
	gchar *selection = NULL;
	selection = hildon_touch_selector_get_current_text (selector);
	g_print("Selected layout: %s\n", selection);

	IRRECO_STRING_TABLE_FOREACH(self->irreco_data->irreco_layout_array,
				    key,
				    IrrecoButtonLayout *,
				    layout)
		g_print("Layout name: %s\n", layout->name->str);
		if(g_strcmp0(layout->name->str, selection) == 0) {
			irreco_window_manager_set_layout(self->irreco_data->window_manager, layout);
		}
	IRRECO_STRING_TABLE_FOREACH_END
	g_free(selection);
	gtk_widget_destroy(self->remote_dialog);
}

/** @} */
/** @} */


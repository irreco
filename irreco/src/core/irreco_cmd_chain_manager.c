/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_cmd_chain_manager.h"
#include "irreco_config.h"

/**
 * @addtogroup IrrecoCmdChainManager
 * @ingroup Irreco
 *
 * A class for storing IrrecoCmdChains assosiated with buttons and hardkeys.
 *
 * - Stores an array of IrrecoCmdChains
 * - Can save command chains to file.
 * - Can read command chains from file.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoCmdChainManager.
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

typedef struct _IrrecoChainFindData IrrecoChainFindData;
struct _IrrecoChainFindData {
	gpointer value;
	gpointer key;
};

static gboolean irreco_cmd_chain_manager_free_foreach(gpointer key,
						      gpointer value,
						      gpointer user_data);
static gboolean irreco_cmd_chain_manager_find_chain_foreach(gpointer key,
							    gpointer value,
							    gpointer user_data);
static void irreco_cmd_chain_manager_to_config_foreach(gpointer key,
						       gpointer value,
						       gpointer user_data);



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

IrrecoCmdChainManager *irreco_cmd_chain_manager_new()
{
	IrrecoCmdChainManager *self;
	IRRECO_ENTER

	self = g_slice_new0(IrrecoCmdChainManager);
	self->table = g_hash_table_new(NULL, NULL);
	IRRECO_RETURN_PTR(self);
}

void irreco_cmd_chain_manager_free(IrrecoCmdChainManager *self)
{
	IRRECO_ENTER
	if (g_hash_table_size(self->table)) {
		IRRECO_DEBUG("Hashtable is not empty. Something does not "
			     "properly free IrrecoCmdChains.\n");
	}

	g_hash_table_foreach_remove(self->table,
				    irreco_cmd_chain_manager_free_foreach,
				    NULL);
	g_hash_table_destroy(self->table);
	g_slice_free(IrrecoCmdChainManager, self);
	IRRECO_RETURN
}
static gboolean irreco_cmd_chain_manager_free_foreach(gpointer key,
						      gpointer value,
						      gpointer user_data)
{
	IRRECO_PRINTF("Destroying command chain with id \"%i\".\n",
		      (IrrecoCmdChainId) key);
	irreco_cmd_chain_destroy((IrrecoCmdChain *) value);
	return TRUE;
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Private Functions
 * @{
 */

static IrrecoCmdChainId
irreco_cmd_chain_manager_find_empty_index(IrrecoCmdChainManager *self)
{
	IrrecoCmdChainId i = 1;
	IRRECO_ENTER

	while (g_hash_table_lookup(self->table, (gconstpointer) i)) i++;
	IRRECO_RETURN_INT(i);
}



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Public Functions                                                           */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */

/**
 * Create a new IrrecoCmdChain and assign an id for it.
 */
IrrecoCmdChainId irreco_cmd_chain_manager_new_chain(IrrecoCmdChainManager *self)
{
	IrrecoCmdChainId id;
	IrrecoCmdChain *chain;
	IRRECO_ENTER

	id = irreco_cmd_chain_manager_find_empty_index(self);
	chain = irreco_cmd_chain_create();
	irreco_cmd_chain_set_id(chain, id);
	g_hash_table_insert(self->table, (gpointer) id, chain);
	IRRECO_RETURN_INT(id);
}

/**
 * Create a new command chain and assosiate it with the given id, if it does
 * not already exists.
 *
 * @return TRUE if created, FALSE if already exists.
 */
gboolean irreco_cmd_chain_manager_new_chain_with_id(IrrecoCmdChainManager *self,
						    IrrecoCmdChainId id)
{
	IrrecoCmdChain *chain;
	IRRECO_ENTER

	if (g_hash_table_lookup(self->table, (gpointer) id)) {
		IRRECO_DEBUG("Comman chain with id \"%i\" "
			     "already exists.\n", id);
		IRRECO_RETURN_BOOL(FALSE);
	}

	chain = irreco_cmd_chain_create();
	irreco_cmd_chain_set_id(chain, id);
	g_hash_table_insert(self->table, (gpointer) id, chain);
	IRRECO_RETURN_BOOL(TRUE);
}

void irreco_cmd_chain_manager_free_chain(IrrecoCmdChainManager *self,
					 IrrecoCmdChainId id)
{
	IRRECO_ENTER
	g_hash_table_remove(self->table, (gpointer) id);
	IRRECO_RETURN
}

/**
 * Get chain.
 *
 * Returns: IrrecoCmdChain pointer or NULL.
 */
IrrecoCmdChain *irreco_cmd_chain_manager_get_chain(IrrecoCmdChainManager *self,
						   IrrecoCmdChainId id)
{
	IRRECO_ENTER
	IRRECO_RETURN_PTR(g_hash_table_lookup(self->table, (gpointer) id));
}

/**
 * Find IrrecoCmdChainId that corresponds to IrrecoCmdChain pointer.
 */
IrrecoCmdChainId irreco_cmd_chain_manager_find_chain(IrrecoCmdChainManager *self,
						     IrrecoCmdChain *chain)
{
	IrrecoChainFindData find_data;
	IRRECO_ENTER

	if (g_hash_table_find(self->table,
			      irreco_cmd_chain_manager_find_chain_foreach,
			      &find_data)) {
		IRRECO_RETURN_INT((IrrecoCmdChainId) find_data.key);
	}
	IRRECO_RETURN_INT(0);
}
static gboolean irreco_cmd_chain_manager_find_chain_foreach(gpointer key,
							    gpointer value,
							    gpointer user_data)
{
	IrrecoChainFindData *find_data = (IrrecoChainFindData *) user_data;
	if (find_data->value != value) return FALSE;
	find_data->key = key;
	return TRUE;
}

/**
 * Save command chains to disk.
 */
gboolean irreco_cmd_chain_manager_to_config(IrrecoCmdChainManager *self)
{
	GKeyFile *keyfile;
	gboolean success;
	IRRECO_ENTER

	keyfile = g_key_file_new();
	g_hash_table_foreach(self->table,
			     irreco_cmd_chain_manager_to_config_foreach,
			     keyfile);

	success = irreco_gkeyfile_write_to_config_file(
		keyfile, "irreco", irreco_config_cmd_chain_file);

	g_key_file_free(keyfile);
	IRRECO_RETURN_BOOL(success);
}
static void irreco_cmd_chain_manager_to_config_foreach(gpointer key,
						       gpointer value,
						       gpointer user_data)
{
	GKeyFile         *keyfile = (GKeyFile *) user_data;
	IrrecoCmdChain   *chain   = (IrrecoCmdChain *) value;
	IrrecoCmdChainId  id      = (IrrecoCmdChainId) key;
	IRRECO_ENTER

	irreco_cmd_chain_set_id(chain, id);
	irreco_cmd_chain_to_config(chain, keyfile);
	IRRECO_RETURN
}

/**
 * Read command chains from disk.
 */
gboolean irreco_cmd_chain_manager_from_config(IrrecoDirForeachData *dir_data,
		IrrecoStringTable *list)
{
	IrrecoData *irreco_data = (IrrecoData *)dir_data->user_data_1;
	IrrecoCmdChainManager *self = irreco_data->cmd_chain_manager;
	guint i;
	IrrecoKeyFile *keyfile = NULL;
	gchar *config_dir = NULL;
	gchar *config_file = NULL;
	gchar** groups = NULL;
	gsize group_count = 0;
	gboolean clash;
	IRRECO_ENTER

	/* Read config file into IrrecoKeyFile. */
	/* g_sprintf(config_dir, "%s", dir_data->directory); */
	config_dir = irreco_get_config_dir("irreco");
	config_file = irreco_get_config_file("irreco", dir_data->filename);

	keyfile = irreco_keyfile_create(config_dir, config_file, NULL);
	if (config_dir == NULL || config_file == NULL || keyfile == NULL) {
		g_free(config_dir);
		g_free(config_file);
		irreco_keyfile_destroy(keyfile);
		IRRECO_RETURN_BOOL(FALSE);
	}

	/* Get list of groups from. */
	IRRECO_PRINTF("Reading layout configuration file \"%s\"\n",
		      config_file);
	groups = g_key_file_get_groups(keyfile->keyfile, &group_count);

	/* Read layouts. */
	for (i = 0; i < group_count; i++) {
		const gchar      *group    = groups[i];
		IrrecoCmdChain   *chain    = NULL;
		IrrecoCmdChain   *id_chain = NULL;
		IrrecoCmdChainId chain_id;

		clash = FALSE;

		/* Read only command chain groups. */
		if (!g_str_has_prefix(group, "chain-") ||
		    g_strrstr(group, ":") != NULL) continue;

		/* Read command chain from keyfile. */
		irreco_keyfile_set_group(keyfile, group);
		chain = irreco_cmd_chain_new_from_config(keyfile,
				dir_data->user_data_1);

		chain_id = irreco_cmd_chain_get_id(chain);

		IRRECO_STRING_TABLE_FOREACH(list, key, gchar *, listdata)
			if(chain_id == atoi(key)) {
				chain_id = atoi(listdata);
				IRRECO_DEBUG("key, listdata: %s, %s\n",
						key, listdata);

				irreco_cmd_chain_manager_new_chain_with_id(
						self, chain_id);
				id_chain = irreco_cmd_chain_manager_get_chain(
						self,
						chain_id);
				irreco_cmd_chain_copy(chain, id_chain);
				irreco_cmd_chain_free(chain);

				clash = TRUE;
			}
		IRRECO_STRING_TABLE_FOREACH_END

		/**
		 * Copy chain into ID-mapped chain.
		 * @todo Rework this so that there is no need to copy chains.
		 */
		if(!clash) {
			irreco_cmd_chain_manager_new_chain_with_id(
				self, irreco_cmd_chain_get_id(chain));
			id_chain = irreco_cmd_chain_manager_get_chain(
				self, irreco_cmd_chain_get_id(chain));
			irreco_cmd_chain_copy(chain, id_chain);
			irreco_cmd_chain_free(chain);
		}
	}

	g_strfreev(groups);
	irreco_keyfile_destroy(keyfile);
	g_free(config_dir);
	g_free(config_file);
	IRRECO_RETURN_BOOL(TRUE);
}

/** @} */

/** @} */


/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


/**
 * @addtogroup IrrecoListboxText
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoListboxText.
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_LISTBOX_TEXT_H_TYPEDEF__
#define __IRRECO_LISTBOX_TEXT_H_TYPEDEF__

typedef struct _IrrecoListboxText	IrrecoListboxText;
typedef struct _IrrecoListboxTextClass 	IrrecoListboxTextClass;

#define IRRECO_TYPE_LISTBOX_TEXT irreco_listbox_text_get_type()

#define IRRECO_LISTBOX_TEXT(obj) \
	(G_TYPE_CHECK_INSTANCE_CAST ((obj), \
	IRRECO_TYPE_LISTBOX_TEXT, IrrecoListboxText))

#define IRRECO_LISTBOX_TEXT_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_CAST ((klass), \
	IRRECO_TYPE_LISTBOX_TEXT, IrrecoListboxTextClass))

#define IRRECO_IS_LISTBOX_TEXT(obj) \
	(G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
	IRRECO_TYPE_LISTBOX_TEXT))

#define IRRECO_IS_LISTBOX_TEXT_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_TYPE ((klass), \
	IRRECO_TYPE_LISTBOX_TEXT))

#define IRRECO_LISTBOX_TEXT_GET_CLASS(obj) \
	(G_TYPE_INSTANCE_GET_CLASS ((obj), \
	IRRECO_TYPE_LISTBOX_TEXT, IrrecoListboxTextClass))

#endif /* __IRRECO_LISTBOX_TEXT_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_LISTBOX_TEXT_H__
#define __IRRECO_LISTBOX_TEXT_H__
#include "irreco.h"
#include "irreco_listbox.h"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

struct _IrrecoListboxText {
	IrrecoListbox parent;
};

struct _IrrecoListboxTextClass {
	IrrecoListboxClass parent_class;
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
GType		irreco_listbox_text_get_type		(void);
GtkWidget*	irreco_listbox_text_new			(void);
GtkWidget*	irreco_listbox_text_new_with_autosize	(gint min_width,
							 gint max_width,
							 gint min_height,
							 gint max_height);
void irreco_listbox_text_append(IrrecoListboxText *self,
				const gchar *label,
				gpointer user_data);




#endif /* __IRRECO_LISTBOX_TEXT_H__ */

/** @} */


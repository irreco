/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_cmd_chain.h"
#include <hildon/hildon-banner.h>

/**
 * @addtogroup IrrecoCmdChain
 * @ingroup Irreco
 *
 * IrrecoCmdChain contains a list of IrrecoCmds, which can then all be
 * executed one after another by calling irreco_cmd_chain_execute().
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoCmdChain.
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

typedef struct _IrrecoCmdChainExecute IrrecoCmdChainExecute;
struct _IrrecoCmdChainExecute {

	IrrecoData     *irreco_data;
	GFunc           call_when_done;
	gpointer        user_data;

	GString        *banner_message;
	GtkWidget      *banner;

	/* Execution time will be timed in relation of time_sync. */
	GTimeVal        time_sync;
	glong		time_pos;

	IrrecoCmdChain *self;
	IrrecoCmd      *command;
	guint           pos;
	guint           len;
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

static gboolean irreco_cmd_chain_execute_next_2(IrrecoCmdChainExecute *execute);



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

IrrecoCmdChain *irreco_cmd_chain_new()
{
	IrrecoCmdChain *self;
	IRRECO_ENTER

	self = g_slice_new0(IrrecoCmdChain);
	self->execution_rate = IRRECO_SECONDS_TO_USEC(0.3);
	self->show_progress = TRUE;
	IRRECO_RETURN_PTR(self);
}

IrrecoCmdChain *irreco_cmd_chain_new_from_config(IrrecoKeyFile *keyfile,
						 IrrecoData    *irreco_data)
{
	IrrecoCmdChain *self;
	IRRECO_ENTER

	self = irreco_cmd_chain_new();
	irreco_cmd_chain_from_config(self, keyfile, irreco_data);
	IRRECO_RETURN_PTR(self);
}

void irreco_cmd_chain_free(IrrecoCmdChain *self)
{
	IRRECO_ENTER
	if (self == NULL) IRRECO_RETURN
	irreco_cmd_chain_remove_all(self);
	g_slice_free(IrrecoCmdChain, self);
	IRRECO_RETURN
}

/**
 * @deprecated
 * @todo
 */
IrrecoCmdChain *irreco_cmd_chain_create()
{
	IRRECO_ENTER
	IRRECO_RETURN_PTR(irreco_cmd_chain_new());
}

/**
 * @deprecated
 * @todo
 */
void irreco_cmd_chain_destroy(IrrecoCmdChain *self)
{
	IRRECO_ENTER
	irreco_cmd_chain_free(self);
	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Private Functions
 * @{
 */

/**
 * Command execution phaze one, update banner.
 */
static gboolean irreco_cmd_chain_execute_next_1(IrrecoCmdChainExecute *execute)
{
	gdouble fraction;
	IRRECO_ENTER

	IRRECO_PRINTF("Command %i / %u\n", execute->pos + 1, execute->len);
	execute->command = irreco_cmd_chain_get(execute->self, execute->pos);

	g_string_printf(execute->banner_message, "%s%s", _("Executing: "),
			irreco_cmd_get_long_name(execute->command));
	IRRECO_PRINTF("%s\n", execute->banner_message->str);

	/* Show progressbar if enabled. */
	if (execute->self->show_progress) {

		/* Setup banner. */
		if (execute->banner == NULL) {
			GtkWidget *owner;
			owner = GTK_WIDGET(irreco_window_manager_get_gtk_window(
				execute->irreco_data->window_manager));
			execute->banner	= hildon_banner_show_progress(
				owner, NULL, execute->banner_message->str);
		} else {
			hildon_banner_set_text(HILDON_BANNER(execute->banner),
					       execute->banner_message->str);
		}

		/* Set progress. */
		fraction = (float) 1 / (float) execute->len *
			   (float)(execute->pos + 1);
		IRRECO_DEBUG("Proggressbar fraction %f\n", fraction);
		hildon_banner_set_fraction(HILDON_BANNER(execute->banner),
					   fraction);
	}
	g_idle_add(G_SOURCEFUNC(irreco_cmd_chain_execute_next_2), execute);
	IRRECO_RETURN_BOOL(FALSE);
}

/**
 * Command execution phaze two, execute command.
 */
static gboolean irreco_cmd_chain_execute_next_2(IrrecoCmdChainExecute *execute)
{
	gboolean result;
	IRRECO_ENTER

	/* Wait command delay is simply added to the time_pos. */
	if (execute->command->type == IRRECO_COMMAND_WAIT) {

		/* Wait command overrides the chain specific execution rate.*/
		execute->time_pos = execute->time_pos
				    + execute->command->wait.delay;
		execute->pos = execute->pos + 1;
		result = TRUE;
	} else {
		result = irreco_cmd_execute(execute->command,
					    execute->irreco_data);
		execute->time_pos = execute->time_pos
				    + execute->self->execution_rate;
		execute->pos = execute->pos + 1;
	}

	/* Is something remaining to be executed? */
	if (result == TRUE && execute->pos < execute->len) {

		GTimeVal time_current, time_next;
		glong time_diff;

		g_get_current_time(&time_current);
		time_next.tv_sec = execute->time_sync.tv_sec;
		time_next.tv_usec = execute->time_sync.tv_usec;
		g_time_val_add(&time_next, execute->time_pos);

		/* How much time to next execution? */
		time_diff = irreco_time_diff(&time_current, &time_next);
		IRRECO_PRINTF("Executing next command after \"%li\" usec.\n",
			      time_diff);

		/* Schedule next call. */
		if (time_diff > 0) {
			g_timeout_add(time_diff / 1000,
				      G_SOURCEFUNC(
				      irreco_cmd_chain_execute_next_1),
				      execute);
		} else {
			g_idle_add(G_SOURCEFUNC(
				   irreco_cmd_chain_execute_next_1),
				   execute);
		}

	/* End of chain. */
	} else {
		if (execute->banner) gtk_widget_destroy(execute->banner);
		g_string_free(execute->banner_message, TRUE);
		IRRECO_DEBUG("Calling call_when_done()\n");
		execute->call_when_done(execute->self, execute->user_data);
		g_slice_free(IrrecoCmdChainExecute, execute);
	}

	IRRECO_RETURN_BOOL(FALSE);
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Public Functions                                                           */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */

/**
 * Set command chain id.
 */
void irreco_cmd_chain_set_id(IrrecoCmdChain   *self,
			     IrrecoCmdChainId  id)
{
	IRRECO_ENTER
	self->id = id;
	IRRECO_RETURN
}

/**
 * Get command chain id.
 */
IrrecoCmdChainId irreco_cmd_chain_get_id(IrrecoCmdChain *self)
{
	IRRECO_ENTER
	IRRECO_RETURN_INT(self->id);
}

/**
 * Save command chain into GKeyFile.
 */
void irreco_cmd_chain_to_config(IrrecoCmdChain   *self,
				GKeyFile         *keyfile)
{
	gint     command_id = 0;
	GString *group      = NULL;
	IRRECO_ENTER

	/* Save command chain settings to keyfile. */
	group = g_string_new(NULL);
	g_string_printf(group, "chain-%i", self->id);
	g_key_file_set_integer(keyfile, group->str, "chain-id", self->id);
	irreco_gkeyfile_set_glong(keyfile, group->str, "exec-rate",
				  self->execution_rate);
	g_key_file_set_boolean(keyfile, group->str, "show-progress",
			       self->show_progress);

	/* Save commands to keyfile. */
	IRRECO_CMD_CHAIN_FOREACH(self, command)
		g_string_printf(group, "chain-%i:command-%i", self->id, ++command_id);
		g_key_file_set_integer(keyfile, group->str, "chain-id", self->id);
		irreco_cmd_to_keyfile(command, keyfile, group->str);
	IRRECO_CMD_CHAIN_FOREACH_END

	g_string_free(group, TRUE);
	IRRECO_RETURN
}

/**
 * Read command chain from IrrecoKeyFile.
 */
gboolean irreco_cmd_chain_from_config(IrrecoCmdChain *self,
				      IrrecoKeyFile  *keyfile,
				      IrrecoData     *irreco_data)
{
	guint        i           = 0;
	gsize        group_count = 0;
	gchar      **groups      = NULL;
	GString     *prefix      = NULL;
	const gchar *group       = NULL;
	IRRECO_ENTER

	/* Read command chain settings from keyfile. */
	group = irreco_keyfile_get_group(keyfile);
	IRRECO_PRINTF("Reading command chain from group \"%s\"\n", group);

	if (!irreco_keyfile_get_int(keyfile, "chain-id", &self->id) ||
	    !irreco_keyfile_get_glong(keyfile, "exec-rate",
		    		      &self->execution_rate)) {
		IRRECO_RETURN_BOOL(FALSE);
	}

	/* Optional keys. */
	irreco_keyfile_get_bool(keyfile, "show-progress",
				&self->show_progress);

	/* Read commands. */
	groups = g_key_file_get_groups(keyfile->keyfile, &group_count);
	prefix = g_string_new(NULL);
	g_string_printf(prefix, "chain-%i", self->id);

	for (i = 0; i < group_count; i++) {
		IrrecoCmdChainId  id      = 0;
		const gchar      *group   = groups[i];
		IrrecoCmd        *command = NULL;

		/* Read only command that belong to this chain. */
		if (!g_str_has_prefix(group, prefix->str) ||
		    !irreco_keyfile_get_int(keyfile, "chain-id", &id) ||
		    self->id != id) {
			continue;
		}

		IRRECO_PRINTF("Reading command from group \"%s\"\n", group);
		irreco_keyfile_set_group(keyfile, group);
		command = irreco_cmd_from_keyfile(irreco_data, keyfile);
		if (command == NULL) continue;

		IRRECO_PRINTF("Appending command to chain \"%i\".\n", self->id);
		irreco_cmd_chain_append(self, command);
	}

	g_strfreev(groups);
	g_string_free(prefix, TRUE);
	IRRECO_RETURN_BOOL(TRUE);
}

/**
 * Remove all commands from command chain.
 */
void irreco_cmd_chain_remove_all(IrrecoCmdChain *self)
{
	IRRECO_ENTER

	g_assert(self != NULL);

	while (self->command_list != NULL) {
		irreco_cmd_destroy(self->command_list->data);
		self->command_list = g_list_remove(
			self->command_list,
			self->command_list->data);
	}
	IRRECO_RETURN
}

/**
 * Set interval between the start of execution of commands inside command chain.
 */
void irreco_cmd_chain_set_execution_rate(IrrecoCmdChain *self,
					 glong execution_rate)
{
	IRRECO_ENTER
	g_assert(self != NULL);
	IRRECO_PRINTF("Setting execution rate to \"%li\".\n", execution_rate);
	self->execution_rate = execution_rate;
	IRRECO_RETURN
}

/**
 * Append a command to command chain.
 */
void irreco_cmd_chain_append(IrrecoCmdChain *self,
			     IrrecoCmd * irreco_cmd)
{
	IRRECO_ENTER

	g_assert(self != NULL);

	self->command_list = g_list_append(
		g_list_last(self->command_list), irreco_cmd);
	IRRECO_RETURN
}

/**
 * Create a copy of command, and append the command to command chain.
 */
void irreco_cmd_chain_append_copy(IrrecoCmdChain *self,
				  IrrecoCmd * irreco_cmd)
{
	IRRECO_ENTER

	g_assert(self != NULL);

	self->command_list = g_list_append(
		g_list_last(self->command_list),
		irreco_cmd_dublicate(irreco_cmd));
	IRRECO_RETURN
}

/**
 * Remove command by index.
 */
void irreco_cmd_chain_remove(IrrecoCmdChain *self, guint index)
{
	IrrecoCmd *irreco_cmd;
	IRRECO_ENTER

	g_assert(self != NULL);

	IRRECO_DEBUG("Removing command from index \"%u\".\n", index);
	irreco_cmd = (IrrecoCmd *) g_list_nth_data(g_list_first(
		self->command_list), index);
	irreco_cmd_destroy(irreco_cmd);
	self->command_list = g_list_remove(
		g_list_first(self->command_list), irreco_cmd);

	IRRECO_RETURN
}

/**
 * Change order of commands inside command chain.
 */
void irreco_cmd_chain_move(IrrecoCmdChain *self,
			   guint from_index, guint to_index)
{
	gpointer data;
	GList* from_link;
	gint length;
	IRRECO_ENTER

	length = irreco_cmd_chain_length(self);

	if (from_index < 0) {
		from_index = 0;
	} else if (from_index > length) {
		IRRECO_RETURN
	}

	if (to_index > length) {
		to_index = length;
	} else if (to_index < 0) {
		IRRECO_RETURN
	}

	from_link = g_list_nth(g_list_first(
		self->command_list), from_index);
	data = from_link->data;
	self->command_list = g_list_delete_link(
		self->command_list, from_link);
	self->command_list = g_list_insert(g_list_first(
		self->command_list), data, to_index);

	IRRECO_RETURN
}

/**
 * Make a copy of a command chain.
 *
 * @param from  Make a copy of this IrrecoCmdChain, or NULL.
 */
void irreco_cmd_chain_copy(IrrecoCmdChain * from,
			   IrrecoCmdChain * to)
{
	IRRECO_ENTER
	irreco_cmd_chain_remove_all(to);
	to->execution_rate = from->execution_rate;
	to->show_progress = from->show_progress;

	if (from == NULL) IRRECO_RETURN
	IRRECO_CMD_CHAIN_FOREACH(from, command)
		irreco_cmd_chain_append_copy(to, command);
	IRRECO_CMD_CHAIN_FOREACH_END

	IRRECO_RETURN
}

/**
 * Get number of command in command chain.
 */
guint irreco_cmd_chain_length(IrrecoCmdChain *self)
{
	IRRECO_ENTER
	if (self->command_list == NULL) IRRECO_RETURN_UINT(0);
	IRRECO_RETURN_UINT(g_list_length(g_list_first(
			   self->command_list)));
}

/**
 * Get Nth IrrecoCmd from command chain.
 */
IrrecoCmd *irreco_cmd_chain_get(IrrecoCmdChain *self,
				guint from_index)
{
	IRRECO_ENTER
	if (self->command_list == NULL) IRRECO_RETURN_PTR(NULL);
	IRRECO_RETURN_PTR(g_list_nth_data(g_list_first(
			  self->command_list),
			  from_index));
}

/**
 * Print list of command inside command chain.
 */
void irreco_cmd_chain_print(IrrecoCmdChain *self)
{
	gint i = 0;
	guint len;
	IRRECO_ENTER

	if (self == NULL) {
		IRRECO_ERROR("IrrecoCmdChain pointer is NULL.\n");
		IRRECO_RETURN
	}

	len = irreco_cmd_chain_length(self);
	IRRECO_PRINTF("Printing command chain. Lenght: %u\n", len);
	IRRECO_CMD_CHAIN_FOREACH(self, command)
		IRRECO_PRINTF("Command %i / %u\n", ++i, len);
		irreco_cmd_print(command);
	IRRECO_CMD_CHAIN_FOREACH_END

	IRRECO_RETURN
}

/**
 * Set if progressbar banner is shown when executing command chains.
 */
void irreco_cmd_chain_set_show_progress(IrrecoCmdChain *self,
					gboolean        show_progress)
{
	IRRECO_ENTER
	self->show_progress = show_progress;
	if (self->show_progress) {
		IRRECO_PRINTF("Progressbar is enabled.\n");
	} else {
		IRRECO_PRINTF("Progressbar is disabled.\n");
	}
	IRRECO_RETURN
}

/**
 * Get if progressbar banner is shown when executing command chains.
 */
gboolean irreco_cmd_chain_get_show_progress(IrrecoCmdChain *self)
{
	IRRECO_ENTER
	IRRECO_RETURN_INT(self->show_progress);
}

/**
 * Start executing the command chain.
 *
 * If the command chain is longer than one command, then delayed execution is
 * started. In that case, call_when_done will be called when the command chain
 * has finished executing.
 *
 * Data pointer will be the command chain, and user_data what user has given.
 *
 * @return TRUE if delayed execution started, FALSE otherwise.
 */
gboolean irreco_cmd_chain_execute(IrrecoCmdChain *self,
				  IrrecoData *irreco_data,
				  GFunc call_when_done,
				  gpointer user_data)
{
	guint len;
	IRRECO_ENTER

	if (self == NULL) {
		IRRECO_RETURN_BOOL(FALSE);
	}
	/* Empty list. */
	if (self->command_list == NULL) {
		IRRECO_RETURN_BOOL(FALSE);

	/* Only one comamnd. */
	} else if ((len = irreco_cmd_chain_length(self)) == 1) {
		irreco_cmd_execute(irreco_cmd_chain_get(self, 0),
				   irreco_data);
		IRRECO_RETURN_BOOL(FALSE);

	/* Multiple commands. */
	} else {
		IrrecoCmdChainExecute *execute;

		execute = g_slice_new0(IrrecoCmdChainExecute);
		execute->banner_message = g_string_new("");
		execute->irreco_data = irreco_data;
		execute->self        = self;
		execute->pos         = 0;
		execute->len         = len;

		execute->call_when_done	= call_when_done;
		execute->user_data = user_data;

		g_get_current_time(&execute->time_sync);
		irreco_cmd_chain_execute_next_1(execute);
	}
	IRRECO_RETURN_BOOL(TRUE);
}

/** @} */












/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_button_dlg.h"
#include "irreco_button.h"
#include "irreco_style_browser_dlg.h"
#include "irreco_cmd_chain_editor.h"

/**
 * @addtogroup IrrecoButtonDlg
 * @ingroup Irreco
 *
 * @todo PURPOCE OF CLASS.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoButtonDlg.
 */

enum {
	IRRECO_STYLE_DLG_SHOW = 1,
	IRRECO_RESPONSE_CLEAR
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
void irreco_button_dlg_create_window(IrrecoButtonDlg * button_dlg,
				     IrrecoWindow * parent,
				     const gchar * title);
void irreco_button_dlg_scrolled_size_request(GtkWidget * widget,
					     GtkRequisition * requisition,
					     gpointer user_data);
void irreco_button_dlg_size_request(GtkWidget *widget,
				    GtkRequisition *requisition,
				    gpointer user_data);
void irreco_button_dlg_style_click(IrrecoButton * irreco_button,
				   GtkWidget * dialog);



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Button dialog structure management.                                        */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Button dialog structure management
 *
 * @{
 */

/*
 * IrrecoButtonDialog
 *
 * Allows user to change the style, command and other properties of a button.
 */
IrrecoButtonDlg* irreco_button_dlg_create(IrrecoData * irreco_data)
{
	IrrecoButtonDlg* button_dlg;
	IRRECO_ENTER

	button_dlg = g_slice_new0(IrrecoButtonDlg);
	button_dlg->irreco_data = irreco_data;
	button_dlg->accepted_cmd_chain = irreco_cmd_chain_create();
	button_dlg->accepted_theme_name = g_string_new("");
	IRRECO_RETURN_PTR(button_dlg);
}

void irreco_button_dlg_clear(IrrecoButtonDlg * button_dlg)
{
	IRRECO_ENTER

	button_dlg->new_style = NULL;
	g_free(button_dlg->accepted_title);
	button_dlg->accepted_title = NULL;
	irreco_cmd_chain_remove_all(button_dlg->accepted_cmd_chain);
	button_dlg->accepted_style = NULL;

	IRRECO_RETURN
}

void irreco_button_dlg_destroy(IrrecoButtonDlg * button_dlg)
{
	IRRECO_ENTER
	irreco_button_dlg_clear(button_dlg);
	irreco_cmd_chain_destroy(button_dlg->accepted_cmd_chain);
	g_slice_free(IrrecoButtonDlg, button_dlg);

	IRRECO_RETURN
}

void irreco_button_dlg_print_data(const gchar * title,
				  IrrecoCmdChain * cmd_chain,
				  IrrecoThemeButton * style)
{
	IRRECO_ENTER
	IRRECO_PRINTF("Title: \"%s\"\n", title);

	if (style == NULL) {
		IRRECO_PRINTF("Style is NULL\n");
	} else {
		IRRECO_PRINTF("Title: \"%s\"\n", style->name->str);
	}

	if (cmd_chain == NULL) {
		IRRECO_PRINTF("Command chain is NULL\n");
	} else {
		IRRECO_PRINTF("Command chain:\n");
		irreco_cmd_chain_print(cmd_chain);
	}

	IRRECO_RETURN
}

void irreco_button_dlg_set(IrrecoButtonDlg * button_dlg,
			   const gchar * title,
			   IrrecoCmdChain * cmd_chain,
			   IrrecoThemeButton * style)
{
	IRRECO_ENTER
	IRRECO_DEBUG("Setting button dialog:\n");
	irreco_button_dlg_print_data(title, cmd_chain, style);

	irreco_button_dlg_clear(button_dlg);
	button_dlg->accepted_title = g_strdup(title);
	if (cmd_chain != NULL) {
		irreco_cmd_chain_copy(cmd_chain,
				      button_dlg->accepted_cmd_chain);
	}
	button_dlg->accepted_style = style;
	IRRECO_RETURN
}

/*
 * Copy information from IrrecoButton into IrrecoButtonDlg.
 */
void irreco_button_dlg_data_from_button(IrrecoButtonDlg * button_dlg,
					IrrecoButton * irreco_button)
{
	IRRECO_ENTER
	irreco_button_dlg_set(button_dlg, irreco_button->title,
			      irreco_button_get_cmd_chain(irreco_button),
			      irreco_button->style);

	IRRECO_RETURN
}

/*
 * Copy information from IrrecoButtonDlg to IrrecoButton.
 */
void irreco_button_dlg_data_to_button(IrrecoButtonDlg * button_dlg,
				      IrrecoButton * irreco_button)
{
	IRRECO_ENTER
	irreco_button_set_title(irreco_button, button_dlg->accepted_title);
	irreco_button_set_cmd_chain(irreco_button,
				    button_dlg->accepted_cmd_chain);
	irreco_button_set_style(irreco_button, button_dlg->accepted_style);
	IRRECO_RETURN
}

void irreco_button_dlg_print(IrrecoButtonDlg * button_dlg)
{
	IrrecoCmdChain *new_cmd_chain = NULL;
	IRRECO_ENTER

	IRRECO_DEBUG("Accepted data:\n");
	irreco_button_dlg_print_data(button_dlg->accepted_title,
				     button_dlg->accepted_cmd_chain,
				     button_dlg->accepted_style);
	IRRECO_DEBUG("New data:\n");

	IRRECO_DEBUG_LINE
	IRRECO_DEBUG("%p\n", (void*) button_dlg->editor);
	if (button_dlg->editor) {
		IRRECO_DEBUG_LINE
		new_cmd_chain = irreco_cmd_chain_editor_get_chain(
			IRRECO_CMD_CHAIN_EDITOR(button_dlg->editor));
	}
	IRRECO_DEBUG_LINE

	irreco_button_dlg_print_data(
		gtk_entry_get_text(GTK_ENTRY(button_dlg->title_entry)),
		new_cmd_chain, button_dlg->new_style);

	IRRECO_RETURN
}

void irreco_button_dlg_update_dialog(IrrecoButtonDlg * button_dlg)
{
	IrrecoTheme *accepted_theme = NULL;
	const gchar *key;
	IRRECO_ENTER

	irreco_cmd_chain_editor_set_chain(IRRECO_CMD_CHAIN_EDITOR(
					  button_dlg->editor),
					  button_dlg->accepted_cmd_chain);

	if (button_dlg->accepted_title != NULL) {
		gtk_entry_set_text(GTK_ENTRY(button_dlg->title_entry),
				   button_dlg->accepted_title);
	} else {
		gtk_entry_set_text(GTK_ENTRY(button_dlg->title_entry), "");
	}

	IRRECO_DEBUG("ACCEPTED THEME NAME %s\n",
		     button_dlg->accepted_theme_name->str);

	IRRECO_DEBUG("ACCEPTED BUTTON INDEX %d\n",
		     button_dlg->accepted_button_index);
	IRRECO_LINE
	if (button_dlg->accepted_theme_name->len == 0) {
		IRRECO_RETURN
	}

	irreco_string_table_get(button_dlg->irreco_data->theme_manager->themes,
				button_dlg->accepted_theme_name->str,
				(gpointer *) &accepted_theme);

	if (accepted_theme != NULL) {
		irreco_string_table_index(accepted_theme->buttons,
					button_dlg->accepted_button_index,
					&key, (gpointer *)
					&button_dlg->accepted_style);
		gtk_entry_set_text(GTK_ENTRY(button_dlg->title_entry),
				   button_dlg->accepted_style->name->str);
	} else {
		button_dlg->accepted_style = NULL;
	}


	irreco_button_set_style(button_dlg->style_button,
				button_dlg->accepted_style);

	irreco_button_create_widget(button_dlg->style_button);

	button_dlg->new_style = button_dlg->accepted_style;

	IRRECO_RETURN
}

/*
 * Show button dialog.
 *
 * Returns TRUE if user clicks OK.
 * Returns FALSE otherwise.
 */
gboolean irreco_button_dlg_run(IrrecoButtonDlg * button_dlg,
			       IrrecoWindow * parent,
			       const gchar * title)
{
	IrrecoData *irreco_data = button_dlg->irreco_data;
	gint rvalue = -1;
	IRRECO_ENTER

	/* Prepare dialog. */
	irreco_button_dlg_create_window(button_dlg, parent, title);
	irreco_button_dlg_update_dialog(button_dlg);

	/* Show dialog. */
	gtk_widget_show_all(button_dlg->dialog);

	if (button_dlg->accepted_style != NULL); {
		irreco_button_set_style(button_dlg->style_button,
				button_dlg->accepted_style);
		button_dlg->new_style = button_dlg->accepted_style;
	}

	irreco_button_create_widget(button_dlg->style_button);


	while (rvalue == -1) {
		switch (gtk_dialog_run(GTK_DIALOG(button_dlg->dialog))) {
		case GTK_RESPONSE_DELETE_EVENT:
			rvalue = FALSE;
			break;

		case GTK_RESPONSE_ACCEPT: {
			IrrecoTheme *theme = NULL;

			g_free(button_dlg->accepted_title);

			button_dlg->accepted_title =
				g_strdup(gtk_entry_get_text(GTK_ENTRY(
				button_dlg->title_entry)));

			button_dlg->accepted_style = button_dlg->new_style;

			irreco_cmd_chain_editor_apply_changes(
				IRRECO_CMD_CHAIN_EDITOR(button_dlg->editor));

			if (button_dlg->new_style == NULL) {
				rvalue = TRUE;
				break;
			}

			g_string_printf(button_dlg->accepted_theme_name, "%s",
					button_dlg->new_style->theme_name->str);

			irreco_string_table_get(
					irreco_data->theme_manager->themes,
					button_dlg->accepted_theme_name->str,
					(gpointer *) &theme);

			if (theme != NULL) {

			irreco_string_table_get_index(theme->buttons,
					button_dlg->new_style,
					&button_dlg->accepted_button_index);


				if(irreco_string_table_lenght(theme->buttons) ==
				++button_dlg->accepted_button_index) {
					button_dlg->accepted_button_index = 0;
				}
			}

			rvalue = TRUE;

			break;
		}
		case IRRECO_STYLE_DLG_SHOW:
			if (irreco_show_style_browser_dlg(irreco_data,
						GTK_WINDOW(button_dlg->dialog),
						&button_dlg->new_style)) {
				irreco_button_set_style(
						button_dlg->style_button,
						button_dlg->new_style);

				irreco_button_create_widget(
						button_dlg->style_button);
			}
			/* Large buttons may blow up the dialog a bit,
			   so resize the dialog as small as possible. */
			gtk_window_resize(GTK_WINDOW(button_dlg->dialog), 1, 1);

			break;

		case IRRECO_RESPONSE_CLEAR:
			gtk_entry_set_text(GTK_ENTRY(
					   button_dlg->title_entry), "");

			irreco_cmd_chain_editor_clear_chain(
				IRRECO_CMD_CHAIN_EDITOR(button_dlg->editor));

			break;
		}
	}

	gtk_widget_destroy(button_dlg->dialog);
	button_dlg->dialog = NULL;
	irreco_button_layout_destroy(button_dlg->layout);
	button_dlg->layout = NULL;
	button_dlg->title_entry = NULL;
	button_dlg->style_button = NULL;
	button_dlg->editor = NULL;
	IRRECO_RETURN_INT(rvalue);
}

/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Button dialog building.                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Button dialog building
 * @{
 */

void irreco_button_dlg_create_window(IrrecoButtonDlg * button_dlg,
				     IrrecoWindow * parent,
				     const gchar * title)
{
	GtkWidget *hbox;
	GtkWidget *vbox_left;
	GtkWidget *vbox_right;

	GtkWidget *style_frame;
	GtkWidget *style_scrolled;
	GtkWidget *style_fixed;

	GtkWidget *editor_frame;
	IRRECO_ENTER

	/* Create objects. */
	button_dlg->dialog = gtk_dialog_new_with_buttons(
		title, irreco_window_get_gtk_window(parent),
		GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT |
		GTK_DIALOG_NO_SEPARATOR,
		GTK_STOCK_CLEAR, IRRECO_RESPONSE_CLEAR,
		GTK_STOCK_OK, GTK_RESPONSE_ACCEPT,
		NULL);
	hbox = gtk_hbox_new(FALSE, 10);
	vbox_left = gtk_vbox_new(FALSE, 0);
	vbox_right = gtk_vbox_new(FALSE, 0);

	button_dlg->title_entry = gtk_entry_new();
	style_frame = gtk_frame_new(NULL);
	style_scrolled = gtk_scrolled_window_new(NULL, NULL);
	style_fixed = gtk_fixed_new();
	button_dlg->layout = irreco_button_layout_create(style_fixed, NULL);
	button_dlg->style_button = irreco_button_create(button_dlg->layout,
							0, 0, _("Change"),
							NULL, NULL);

	/* Build dialog. */
	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(button_dlg->dialog)->vbox),
			  irreco_gtk_align(hbox, 0, 0, 1, 1, 8, 8, 8, 8));
	gtk_container_add(GTK_CONTAINER(hbox), vbox_left);
	gtk_container_add(GTK_CONTAINER(hbox), vbox_right);

	/* Connect signals. */
	g_signal_connect(G_OBJECT(button_dlg->dialog), "size-request",
			 G_CALLBACK(irreco_button_dlg_size_request),
			 NULL);
	g_signal_connect(G_OBJECT(style_scrolled), "size-request",
			 G_CALLBACK(irreco_button_dlg_scrolled_size_request),
			 NULL);

	/* Build left side of dialog. */
	gtk_box_pack_start(GTK_BOX(vbox_left),
			   irreco_gtk_label(_("Name"), 0.5, 1, 0, 0, 0, 0),
			   0, 0, 0);
	gtk_box_pack_start(GTK_BOX(vbox_left),
			   button_dlg->title_entry,
			   0, 0, 0);
	gtk_box_pack_start(GTK_BOX(vbox_left),
			   irreco_gtk_label(_("Style"), 0.5, 1, 10, 0, 0, 0),
			   0, 0, 0);
	gtk_box_pack_start(GTK_BOX(vbox_left),
			   style_frame,
			   1, 1, 1);
	gtk_container_add(GTK_CONTAINER(style_frame), style_scrolled);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(style_scrolled),
				       GTK_POLICY_AUTOMATIC,
				       GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(
					      style_scrolled),
		     			      irreco_gtk_align(style_fixed,
					      0.5, 0.5, 0, 0, 8, 8, 8, 8));

	/* Build right side of dialog. */
	button_dlg->editor = irreco_cmd_chain_editor_new(
		button_dlg->irreco_data, GTK_WINDOW(button_dlg->dialog));
	irreco_cmd_chain_editor_set_chain(IRRECO_CMD_CHAIN_EDITOR(
		button_dlg->editor), button_dlg->accepted_cmd_chain);
	editor_frame = gtk_frame_new(NULL);

	gtk_box_pack_start(GTK_BOX(vbox_right), irreco_gtk_label(
			   _("Command Chain"), 0.5, 1, 0, 0, 0, 0),
			   0, 0, 0);
	gtk_container_add(GTK_CONTAINER(vbox_right), editor_frame);
	gtk_container_add(GTK_CONTAINER(editor_frame), button_dlg->editor);

	/* Connect callbacks. */
	irreco_button_layout_set_press_callback(button_dlg->layout,
						irreco_button_down);
	irreco_button_layout_set_release_callback(button_dlg->layout,
					         irreco_button_dlg_style_click);
	irreco_button_layout_set_callback_data(button_dlg->layout,
					       button_dlg->dialog);

	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Signal callbacks.                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Signal callbacks
 * @{
 */

/*
 * Adjust size GtkRequisition in a way that will hide the scrollbar if possible.
 */
void irreco_button_dlg_scrolled_size_request(GtkWidget * widget,
					     GtkRequisition * requisition,
					     gpointer user_data)
{
	GtkAdjustment *adjustment;
	IRRECO_ENTER

	adjustment = gtk_scrolled_window_get_vadjustment(
			GTK_SCROLLED_WINDOW(widget));
	requisition->height = adjustment->upper;
	adjustment = gtk_scrolled_window_get_hadjustment(
			GTK_SCROLLED_WINDOW(widget));
	requisition->width = adjustment->upper;
	/*
	IRRECO_DEBUG("value %f\n", adjustment->value);
	IRRECO_DEBUG("lower %f\n", adjustment->lower);
	IRRECO_DEBUG("upper %f\n", adjustment->upper);
	IRRECO_DEBUG("step_increment %f\n", adjustment->step_increment);
	IRRECO_DEBUG("page_increment %f\n", adjustment->page_increment);
	IRRECO_DEBUG("page_size %f\n", adjustment->page_size);
	*/

	IRRECO_RETURN
}

void irreco_button_dlg_size_request(GtkWidget *widget,
				    GtkRequisition *requisition,
				    gpointer user_data)
{
	IRRECO_ENTER
	requisition->height = 300;
	IRRECO_RETURN
}

void irreco_button_dlg_style_click(IrrecoButton * irreco_button,
				   GtkWidget * dialog)
{
	IRRECO_ENTER
	gtk_dialog_response(GTK_DIALOG(dialog), IRRECO_STYLE_DLG_SHOW);
	irreco_button_up(irreco_button);
	IRRECO_RETURN
}

/** @} */
/** @} */


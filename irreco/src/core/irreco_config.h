/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *		       Harri Vattulainen (t5vaha01@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoConfig
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoConfig.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_CONFIG_H_TYPEDEF__
#define __IRRECO_CONFIG_H_TYPEDEF__

#endif /* __IRRECO_CONFIG_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_CONFIG_H__
#define __IRRECO_CONFIG_H__
#include "irreco.h"
#include "irreco_data.h"
extern gchar* irreco_config_layout_file;
extern gchar* irreco_config_active_layout_file;
extern gchar* irreco_config_backend_file;
extern gchar* irreco_config_cmd_chain_file;


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
gboolean irreco_config_read_layouts_from_files(IrrecoData *irreco_data);
void irreco_config_read_layouts(IrrecoDirForeachData *dir_data);
gboolean irreco_config_save_layouts(IrrecoData * irreco_data);
gboolean irreco_config_save_active_layout(IrrecoButtonLayout * irreco_layout);
IrrecoButtonLayout *irreco_config_read_active_layout(IrrecoData * irreco_data);
gboolean irreco_config_save_backends(IrrecoBackendManager * backend_manager);
gboolean irreco_config_read_backends(IrrecoBackendManager * backend_manager);
gboolean irreco_layout_get_themes(IrrecoData *data,
				  gchar *layoutname,
				  IrrecoStringTable **list);
gboolean irreco_layout_get_backends(IrrecoData *data,
				    gchar *layoutname,
				    IrrecoStringTable **list);

#endif /* __IRRECO_CONFIG_H__ */

/** @} */

/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007,2008  Arto Karppinen (arto.karppinen@iki.fi),
 * Joni Kokko (t5kojo01@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoWebdbDlg
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoWebdbDlg.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_WEBDB_DLG_H_TYPEDEF__
#define __IRRECO_WEBDB_DLG_H_TYPEDEF__

#define IRRECO_TYPE_WEBDB_DLG irreco_webdb_dlg_get_type()
#define IRRECO_WEBDB_DLG(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), IRRECO_TYPE_WEBDB_DLG, IrrecoWebdbDlg))
#define IRRECO_WEBDB_DLG_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), IRRECO_TYPE_WEBDB_DLG, IrrecoWebdbDlgClass))
#define IRRECO_IS_WEBDB_DLG(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), IRRECO_TYPE_WEBDB_DLG))
#define IRRECO_IS_WEBDB_DLG_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), IRRECO_TYPE_WEBDB_DLG))
#define IRRECO_WEBDB_DLG_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), IRRECO_TYPE_WEBDB_DLG, IrrecoWebdbDlgClass))

typedef struct _IrrecoWebdbDlg IrrecoWebdbDlg;
typedef struct _IrrecoWebdbDlgClass IrrecoWebdbDlgClass;

#endif /* __IRRECO_WEBDB_DLG_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_WEBDB_DLG_H__
#define __IRRECO_WEBDB_DLG_H__
#include "irreco.h"
#include "irreco_dlg.h"
#include "irreco_data.h"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

struct _IrrecoWebdbDlg {
	IrrecoDlg 		 parent;
	IrrecoData 		*irreco_data;
	GtkTreeStore		*tree_store;
	GtkWidget		*tree_view;
	GtkWidget		*banner;
	GtkWidget		*hbox;
	IrrecoWebdbConf		*config;
	GtkWidget		*config_info;
	GtkWidget		*config_user;
	GtkWidget		*config_backend;
	GtkWidget		*config_category;
	GtkWidget		*config_manuf;
	GtkWidget		*config_model;
	GtkWidget		*config_uploaded;
	GtkWidget		*config_download_count;
	GtkWidget		*scrollbar;

	gint			 loader_func_id;
	gint			 loader_state;
	gint			 loader_pos;
	/** The loaded items are childern of this iter.*/
	GtkTreeIter		*loader_parent_iter;
};

struct _IrrecoWebdbDlgClass {
	IrrecoDlgClass parent_class;
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
GType irreco_webdb_dlg_get_type(void);
GtkWidget *irreco_webdb_dlg_new(IrrecoData *irreco_data,
				GtkWindow *parent);
void irreco_webdb_dlg_set_irreco_data(IrrecoWebdbDlg *self,
				      IrrecoData *irreco_data);
void irreco_show_webdb_dlg(IrrecoData *irreco_data, GtkWindow *parent);


#endif /* __IRRECO_WEBDB_DLG_H__ */

/** @} */

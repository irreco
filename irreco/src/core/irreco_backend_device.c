/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_backend_device.h"

/**
 * @addtogroup IrrecoBackendDevice
 * @ingroup Irreco
 *
 * Used to storage the information and data provided about the device by
 * an instance of some backend.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoBackendDevice.
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

IrrecoBackendDevice *
irreco_backend_device_create(const gchar *name,
			     IrrecoBackendInstance * backend_instance,
			     gpointer *contex)
{
	IrrecoBackendDevice *backend_device;
	IRRECO_ENTER

	g_assert(name != NULL);
	g_assert(backend_instance != NULL);

	backend_device = g_slice_new0(IrrecoBackendDevice);
	backend_device->backend_instance = backend_instance;
	backend_device->name = g_strdup(name);
	backend_device->contex = contex;
	backend_device->command_list = irreco_string_table_new(NULL, NULL);
	IRRECO_RETURN_PTR(backend_device);
}

void irreco_backend_device_destroy(IrrecoBackendDevice *backend_device)
{
	IRRECO_ENTER
	if (backend_device == NULL) IRRECO_RETURN
	irreco_string_table_free(backend_device->command_list);
	g_free(backend_device->name);
	backend_device->name = NULL;
	g_slice_free(IrrecoBackendDevice, backend_device);
	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Backend API                                                                */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Backend API
 * @{
 */

/**
 * Can this device be exported?
 *
 * @sa irreco_backend_instance_api_export
 */
gboolean irreco_backend_device_is_editable(IrrecoBackendDevice * device)
{
	gboolean rvalue;
	IRRECO_ENTER

	g_assert(device != NULL);

	/* Does the backend support editable devices at all? */
	if ( ! irreco_backend_instance_api_edit(device->backend_instance)) {
		IRRECO_RETURN_BOOL(FALSE);
	}

	/* Is this device editable? */
	IRRECO_BACKEND_ENTER(device->backend_instance, "is_device_editable()");
	rvalue = device->backend_instance->lib->api->is_device_editable(
		device->backend_instance->contex, device->name, device->contex);
	IRRECO_BACKEND_RETURN("is_device_editable()");
	IRRECO_RETURN_BOOL(rvalue);
}

/**
 * Can this device be exported?
 *
 * @sa irreco_backend_instance_api_export
 */
gboolean irreco_backend_device_is_exportable(IrrecoBackendDevice * device)
{
	IRRECO_ENTER
	g_assert(device != NULL);
	IRRECO_RETURN_BOOL(
		irreco_backend_instance_api_export(
			device->backend_instance))
}

void irreco_backend_device_edit(IrrecoBackendDevice * device,
				GtkWindow * parent)
{
	IrrecoBackendStatus status;
	IRRECO_ENTER

	g_assert(device != NULL);
	g_assert(parent != NULL);

	IRRECO_BACKEND_ENTER(device->backend_instance, "edit_device()");
	status = device->backend_instance->lib->api->edit_device(
		device->backend_instance->contex, device->name,
		device->contex, parent);
	IRRECO_BACKEND_RETURN("edit_device()");
	irreco_backend_instance_check_status(device->backend_instance, status);

	IRRECO_RETURN
}

void irreco_backend_device_delete(IrrecoBackendDevice * device,
				  GtkWindow * parent)
{
	IrrecoBackendStatus status;
	IRRECO_ENTER

	g_assert(device != NULL);
	g_assert(parent != NULL);

	IRRECO_BACKEND_ENTER(device->backend_instance, "delete_device()");
	status = device->backend_instance->lib->api->delete_device(
		device->backend_instance->contex, device->name,
		device->contex, parent);
	IRRECO_BACKEND_RETURN("delete_device()");
	irreco_backend_instance_check_status(device->backend_instance, status);

	IRRECO_RETURN
}

/** @} */
/** @} */



/*
 * irreco - Ir Remote Control
 * Copyright (C) 2008  Harri Vattulainen (t5vaha01@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoWebdbRegisterDlg
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoWebdbRegisterDlg.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef _IRRECO_WEBDB_REGISTER_DLG
#define _IRRECO_WEBDB_REGISTER_DLG

#define IRRECO_TYPE_WEBDB_REGISTER_DLG irreco_webdb_register_dlg_get_type()

#define IRRECO_WEBDB_REGISTER_DLG(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), IRRECO_TYPE_WEBDB_REGISTER_DLG, \
 IrrecoWebdbRegisterDlg))

#define IRRECO_WEBDB_REGISTER_DLG_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), IRRECO_TYPE_WEBDB_REGISTER_DLG, \
 IrrecoWebdbRegisterDlgClass))

#define IRRECO_IS_WEBDB_REGISTER_DLG(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), IRRECO_TYPE_WEBDB_REGISTER_DLG))

#define IRRECO_IS_WEBDB_REGISTER_DLG_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), IRRECO_TYPE_WEBDB_REGISTER_DLG))

#define IRRECO_WEBDB_REGISTER_DLG_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), IRRECO_TYPE_WEBDB_REGISTER_DLG, \
IrrecoWebdbRegisterDlgClass))

typedef struct _IrrecoWebdbRegisterDlg IrrecoWebdbRegisterDlg;
typedef struct _IrrecoWebdbRegisterDlgClass IrrecoWebdbRegisterDlgClass;

#endif /* _IRRECO_WEBDB_REGISTER_DLG */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __HEADER_NAME_H__
#define __HEADER_NAME_H__
#include "irreco.h"
#include "irreco_dlg.h"
#include "irreco_data.h"


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

struct _IrrecoWebdbRegisterDlg {
	IrrecoDlg 	parent;
	IrrecoData 	*irreco_data;

	const gchar	*name;
	const gchar	*email;
	const gchar	*passwd;
	const gchar	*repasswd;
};

struct _IrrecoWebdbRegisterDlgClass {
	IrrecoDlgClass parent_class;
};


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

GType irreco_webdb_register_dlg_get_type (void);
/*
IrrecoWebdbRegisterDlg* irreco_webdb_register_dlg_new (void);
*/
/*
void irreco_show_webdb_register_dlg(IrrecoData *irreco_data, GtkWindow *parent);
*/
void irreco_show_webdb_register_dlg(IrrecoData *irreco_data, GtkWindow *parent);

#endif /* __HEADER_NAME_H__ */

/** @} */

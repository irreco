/*
 * irreco - Ir Remote Control
 * Copyright (C) 2008 Joni Kokko (t5kojo01@students.oamk.fi)
 *                    Sami Mäki (kasmra@xob.kapsi.fi)
 *		      Pekka Gehör (pegu6@msn.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_theme_manager.h"

 /**
 * @addtogroup IrrecoThemeManager
 * @ingroup Irreco
 *
 * Contains information of themes.
 *
 * @{
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

void irreco_theme_manager_read_file_foreach(IrrecoDirForeachData * dir_data);

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

IrrecoThemeManager *irreco_theme_manager_new(IrrecoData * irreco_data)
{
	IrrecoThemeManager *self;
	IRRECO_ENTER

	self = g_slice_new0(IrrecoThemeManager);

	self->irreco_data = irreco_data;

	self->themes =  irreco_string_table_new(
				(GDestroyNotify)irreco_theme_free, NULL);

	irreco_theme_manager_read_themes_from_dir(self, IRRECO_THEME_DIR);
	irreco_theme_manager_read_themes_from_dir(self, "/home/user/MyDocs/irreco");
/*	irreco_theme_manager_read_themes_from_dir(self, "/media/mmc2/irreco");*/

	IRRECO_RETURN_PTR(self);
}

void irreco_theme_manager_free(IrrecoThemeManager *self)
{
	IRRECO_ENTER

	g_assert(self != NULL);

	irreco_string_table_free(self->themes);
	self->themes = NULL;

	g_slice_free(IrrecoThemeManager, self);
	self = NULL;

	IRRECO_RETURN
}

/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Private Functions
 * @{
 */

void irreco_theme_manager_read_file_foreach(IrrecoDirForeachData * dir_data)
{
	IrrecoTheme        *theme = NULL;
	IrrecoTheme        *old_theme = NULL;
	IrrecoThemeManager *self  = (IrrecoThemeManager*) dir_data->user_data_1;
	IRRECO_ENTER

	if (irreco_is_dir(dir_data->filepath)) {
		theme = irreco_theme_new_from_dir(dir_data->filepath);

		if (irreco_string_table_exists(self->themes, theme->name->str)) {
			IRRECO_ERROR("Error: Theme %s has already been read. "
				"You cannot have two themes with the same name.\n",
				theme->name->str);

			irreco_string_table_get(self->themes, theme->name->str,
					       (gpointer*) &old_theme);
			irreco_theme_read(old_theme, dir_data->filepath);
			irreco_theme_free(theme);

		} else {
			irreco_string_table_add(self->themes, theme->name->str, theme);
		}
	} else {
		IRRECO_DEBUG("DIR: %s \n", dir_data->filepath);
	}

	IRRECO_RETURN
}

/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Public Functions                                                           */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */

void irreco_theme_manager_print(IrrecoThemeManager *self)
{
	IRRECO_ENTER

	#if 1
	irreco_string_table_print(self->themes);
	#endif

	#if 0
	IRRECO_STRING_TABLE_FOREACH_DATA(self->themes, void *, theme)
		IRRECO_PRINTF("Theme: %s \n", theme->name->str);
		irreco_theme_print(theme);
	IRRECO_STRING_TABLE_FOREACH_END
	#endif

	IRRECO_RETURN
}

IrrecoStringTable *irreco_theme_manager_get_themes(IrrecoThemeManager *self)
{
	IRRECO_ENTER

	IRRECO_RETURN_PTR(self->themes);
}

IrrecoStringTable *irreco_theme_manager_get_buttons(IrrecoThemeManager *self,
						   const gchar *theme_name)
{
	IrrecoStringTable	*buttons	= NULL;
	IrrecoTheme		*theme		= NULL;
	IRRECO_ENTER

	IRRECO_DEBUG("theme_name: %s\n", theme_name);
	if (irreco_string_table_get(self->themes, theme_name,
				    (gpointer*) &theme)) {
		buttons = theme->buttons;
	}

	IRRECO_RETURN_PTR(buttons);
}

IrrecoStringTable *irreco_theme_manager_get_backgrounds(IrrecoThemeManager *self,
						       const gchar *theme_name)
{
	IrrecoStringTable	*backgrounds	= NULL;
	IrrecoTheme		*theme		= NULL;
	IRRECO_ENTER

	if (irreco_string_table_get(self->themes, theme_name,
				    (gpointer*)&theme)) {
		backgrounds = theme->backgrounds;
	}

	IRRECO_RETURN_PTR(backgrounds);
}

void irreco_theme_manager_read_themes_from_dir(IrrecoThemeManager *self,
					       const gchar *dir)
{
	IrrecoDirForeachData themes;
	IRRECO_ENTER

	IRRECO_DEBUG("THEME_DIR = %s\n", dir);
	themes.directory = dir;

	themes.filesuffix = "";
	themes.user_data_1 = self;

	irreco_dir_foreach(&themes, irreco_theme_manager_read_file_foreach);

	irreco_string_table_sort_abc(self->themes);
	IRRECO_RETURN
}

gboolean irreco_theme_manager_get_button_style(IrrecoThemeManager *self,
					       const gchar *style_name,
					       IrrecoThemeButton **style)
{
	IrrecoThemeButton *button = NULL;
	gboolean rvalue = FALSE;
	IRRECO_ENTER
	IRRECO_STRING_TABLE_FOREACH_DATA(self->themes, void *, theme) {
		IrrecoStringTable* buttons = irreco_theme_get_buttons(theme);

		if (irreco_string_table_get(buttons, style_name,
					    (gpointer *) &button)) {
			*style = button;
			rvalue = TRUE;
		}
	}
	IRRECO_STRING_TABLE_FOREACH_END
	IRRECO_RETURN_BOOL(rvalue);
}

gboolean irreco_theme_manager_does_deb_exist(IrrecoThemeManager *self)
{
	gboolean rvalue = FALSE;
	IRRECO_ENTER
	IRRECO_STRING_TABLE_FOREACH_DATA(self->themes, IrrecoTheme *, theme) {
		if (g_utf8_collate(theme->source->str, "deb") == 0 ||
		    g_utf8_collate(theme->source->str, "DEB") == 0) {
			rvalue = TRUE;
		}
	}
	IRRECO_STRING_TABLE_FOREACH_END
	IRRECO_RETURN_BOOL(rvalue);
}

gboolean irreco_theme_manager_does_web_exist(IrrecoThemeManager *self)
{
	gboolean rvalue = FALSE;
	IRRECO_ENTER
	IRRECO_STRING_TABLE_FOREACH_DATA(self->themes, IrrecoTheme *, theme) {
		if (g_utf8_collate(theme->source->str, "web") == 0 ||
		    g_utf8_collate(theme->source->str, "WEB") == 0) {
			rvalue = TRUE;
		}
	}
	IRRECO_STRING_TABLE_FOREACH_END
	IRRECO_RETURN_BOOL(rvalue);
}

gboolean irreco_theme_manager_does_user_exist(IrrecoThemeManager *self)
{
	gboolean rvalue = FALSE;
	IRRECO_ENTER
	IRRECO_STRING_TABLE_FOREACH_DATA(self->themes, IrrecoTheme *, theme) {
		if (g_utf8_collate(theme->source->str, "user") == 0 ||
		    g_utf8_collate(theme->source->str, "USER") == 0) {
			rvalue = TRUE;
		}
	}
	IRRECO_STRING_TABLE_FOREACH_END
	IRRECO_RETURN_BOOL(rvalue);
}

gboolean irreco_theme_manager_remove_theme(IrrecoThemeManager *self,
                                           const gchar *theme_name)
{
        IrrecoTheme *rmtheme;
        gchar *rm_cmd;
        gboolean rvalue = FALSE;
	IrrecoStringTable *table;

        IRRECO_ENTER

        if (irreco_string_table_get(self->themes, theme_name,
				    (gpointer *) &rmtheme)) {
		/* Remove styles from layouts */
		table = self->irreco_data->irreco_layout_array;
		IRRECO_STRING_TABLE_FOREACH(table, key,
					    IrrecoButtonLayout *, layout)
			IRRECO_PTR_ARRAY_FORWARDS(layout->button_array,
						  IrrecoButton *, button)
				if (button->style != NULL &&
				    g_str_equal(button->style->theme_name->str,
						theme_name)) {
					button->type = IRRECO_BTYPE_GTK_BUTTON;
					irreco_button_set_style(button, NULL);
					irreco_button_create_widget(button);
				}

			IRRECO_PTR_ARRAY_FORWARDS_END
		IRRECO_STRING_TABLE_FOREACH_END

		/* Save layouts */
                irreco_config_save_layouts(self->irreco_data);

                /* Build remove command */
                rm_cmd = g_strconcat("rm -r ", rmtheme->path->str, NULL);

                /* Remove theme directory and its contents from device */
                system(rm_cmd);

	        g_free(rm_cmd);

                /* And from the string table */
                irreco_string_table_remove(self->themes, theme_name);

                rvalue = TRUE;
        }

        IRRECO_RETURN_BOOL(rvalue);
}

void irreco_theme_manager_update_theme_manager(IrrecoThemeManager *self)
{
        IRRECO_ENTER

	irreco_theme_manager_read_themes_from_dir(self, IRRECO_THEME_DIR);
	irreco_theme_manager_read_themes_from_dir(self, "/home/user/MyDocs/irreco");
/*	irreco_theme_manager_read_themes_from_dir(self, "/media/mmc2/irreco");*/

	/* Check if some theme is deleted */
	IRRECO_STRING_TABLE_FOREACH(self->themes, key, IrrecoTheme *,
				    theme)
		if(!irreco_is_dir(theme->path->str)) {
			irreco_string_table_remove(self->themes, key);
		}
	IRRECO_STRING_TABLE_FOREACH_END

        IRRECO_RETURN
}

/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Events and Callbacks                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/** @} */



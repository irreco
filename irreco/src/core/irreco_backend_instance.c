/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_backend_instance.h"
#include "irreco_config.h"
#include "irreco_backend_err_dlg.h"

/**
 * @addtogroup IrrecoBackendInstance
 * @ingroup Irreco
 *
 * Since it is possible that there are, for exmaple, several LIRC servers the
 * user wants to use, we should make sure that the user can use several LIRC
 * servers.
 *
 * Because of this every backend should be programmed so, that it is possible to
 * create several instances of them by calling irreco_backend_create() several
 * times. Ofcourse, if there are several instances of the same backend around,
 * then those instances must also be properly handled inside Irreco, which is
 * exactly what this file should do.
 *
 * Backend instance will call create() from the backend lib, and
 * allocate structures for the instance.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoBackendInstance.
 */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

IrrecoBackendInstance *
irreco_backend_instance_create(IrrecoBackendLib * backend_lib,
			       IrrecoData *irreco_data)
{
	IrrecoBackendInstance *self;
	IRRECO_ENTER

	g_assert(backend_lib != NULL);

	self 				= g_slice_new0(IrrecoBackendInstance);
	self->irreco_data		= irreco_data;
	self->name			= g_string_new(NULL);
	self->description		= g_string_new(NULL);
	self->name_and_description	= g_string_new(NULL);
	self->lib			= backend_lib;
	self->irreco_cmd_dependencies	= g_ptr_array_new();

	IRRECO_BACKEND_ENTER(self, "create()");
	self->contex = self->lib->api->create();
	IRRECO_BACKEND_RETURN("create()");

	if (self->contex == NULL) {
		g_slice_free(IrrecoBackendInstance, self);
		IRRECO_RETURN_PTR(NULL);
	}

	self->device_list = irreco_string_table_new(
		G_DESTROYNOTIFY(irreco_backend_device_destroy), NULL);

	IRRECO_RETURN_PTR(self);
}

void irreco_backend_instance_destroy(IrrecoBackendInstance *self)
{
	IRRECO_ENTER
	irreco_backend_instance_destroy_full(self, FALSE);
	IRRECO_RETURN
}

/**
 * Handle the destruction of the instance and device list.
 */
void irreco_backend_instance_destroy_full(IrrecoBackendInstance *self,
					  gboolean permanently)
{
	IRRECO_ENTER

	if (self == NULL) IRRECO_RETURN

	/* Remove all IrrecoCmd dependencies. */
	IRRECO_PTR_ARRAY_BACKWARDS(self->irreco_cmd_dependencies,
				   IrrecoCmd *, command)
		irreco_cmd_set_builtin(command, IRRECO_COMMAND_NONE);
	IRRECO_PTR_ARRAY_FORWARDS_END
	g_ptr_array_free(self->irreco_cmd_dependencies, TRUE);

	/* Notify library of destruction. */
	IRRECO_BACKEND_ENTER(self, "destroy()");
	self->lib->api->destroy(self->contex,
					    permanently);
	IRRECO_BACKEND_RETURN("destroy()");

	/* Cleanup lib assosiated stuff. */
	irreco_string_table_free(self->device_list);
	g_free(self->config);
	self->config = NULL;

	/* Free name and description. */
	g_string_free(self->name, TRUE);
	self->name = NULL;
	g_string_free(self->description, TRUE);
	self->description = NULL;
	g_string_free(self->name_and_description, TRUE);
	self->name_and_description = NULL;

	g_slice_free(IrrecoBackendInstance, self);
	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Public Functions                                                           */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */

/**
 * Does this instance support editable devices API?
 *
 * @return TRUE if supported, FALSE otherwise.
 */
gboolean irreco_backend_instance_api_edit(IrrecoBackendInstance *self)
{
	if (self->lib->api->flags & IRRECO_BACKEND_EDITABLE_DEVICES) {
		IRRECO_RETURN_BOOL(TRUE);
	}
	IRRECO_RETURN_BOOL(FALSE);
}

/**
 * Does this instance support command export API?
 *
 * @return TRUE if supported, FALSE otherwise.
 */
gboolean irreco_backend_instance_api_export(IrrecoBackendInstance *self)
{
	if (self->lib->api->flags & IRRECO_BACKEND_CONFIGURATION_EXPORT) {
		IRRECO_RETURN_BOOL(TRUE);
	}
	IRRECO_RETURN_BOOL(FALSE);
}

void irreco_backend_instance_add_cmd_dependency(IrrecoBackendInstance *self,
						IrrecoCmd * command)
{
	IRRECO_ENTER
	g_ptr_array_add(self->irreco_cmd_dependencies, command);

	IRRECO_RETURN
}

void
irreco_backend_instance_remove_cmd_dependency(IrrecoBackendInstance *self,
					      IrrecoCmd * command)
{
	IRRECO_ENTER
	g_ptr_array_remove_fast(self->irreco_cmd_dependencies,
				command);

	IRRECO_RETURN
}

gboolean irreco_backend_instance_check_status(IrrecoBackendInstance *self,
					      IrrecoBackendStatus status)
{
	IRRECO_ENTER

	if (status != IRRECO_BACKEND_OK) {
		GString *string;
		const gchar *error_msg;
		const gchar *name;

		IRRECO_BACKEND_ENTER(self, "get_error_msg()");
		error_msg = self->lib->api->get_error_msg(
			self->contex, status);
		IRRECO_BACKEND_RETURN("get_error_msg()");

		/* Print description if it is available. */
		if (irreco_str_isempty(self->name_and_description->str)) {
			name = self->name_and_description->str;
		} else {
			name = self->name->str;
		}

		string = g_string_new(NULL);
		g_string_printf(string, "%i", status);
		irreco_show_backend_err_dlg(
			irreco_window_manager_get_gtk_window(
				self->irreco_data->window_manager),
				self->lib->name, name, string->str, error_msg);
		g_string_free(string, TRUE);

		IRRECO_RETURN_BOOL(FALSE);
	}
	IRRECO_RETURN_BOOL(TRUE);
}

/**
 * DO NOT CALL DIRECTLY, use irreco_backend_manager_set_instance_name()
 *
 * @sa irreco_backend_manager_set_instance_name
 */
void irreco_backend_instance_set_name(IrrecoBackendInstance *self,
				      gchar * name)
{
	IRRECO_ENTER;

	g_string_assign(self->name, name);
	IRRECO_RETURN
}

/**
 * DO NOT CALL DIRECTLY, use irreco_backend_manager_set_instance_config()
 *
 * @sa irreco_backend_manager_set_instance_config
 */
void irreco_backend_instance_set_config(IrrecoBackendInstance *self,
					gchar * config)
{
	IRRECO_ENTER;
	g_free(self->config);
	self->config = g_strdup(config);

	IRRECO_RETURN
}

/**
 * Get name of the instance.
 */
const gchar *
irreco_backend_instance_get_name(IrrecoBackendInstance *self)
{
	IRRECO_ENTER

	if (irreco_str_isempty(self->name->str)) {
		IRRECO_RETURN_PTR(NULL);
	} else {
		IRRECO_RETURN_CONST_STR(self->name->str);
	}
}

/**
 * Get description of the instance.
 */
const gchar *
irreco_backend_instance_get_description(IrrecoBackendInstance *self)
{
	IRRECO_ENTER

	if (self->lib->api->get_description == NULL) {
		g_string_assign(self->description, "");

	} else {
		gchar *description = NULL;

		IRRECO_BACKEND_ENTER(self, "get_description()");
		description = self->lib->api->get_description(self->contex);
		IRRECO_BACKEND_RETURN("get_description()");

		g_string_assign(self->description, description);
		g_free(description);
	}

	if (irreco_str_isempty(self->description->str)) {
		IRRECO_RETURN_PTR(NULL);
	} else {
		IRRECO_RETURN_CONST_STR(self->description->str);
	}
}

/**
 * Get name and description of the instance in one string.
 */
const gchar *
irreco_backend_instance_get_name_and_description(IrrecoBackendInstance *self)
{
	IRRECO_ENTER

	if (irreco_str_isempty(self->name->str)) {
		g_string_assign(self->name_and_description, "");
	} else if (irreco_backend_instance_get_description(self)) {
		g_string_printf(self->name_and_description,
				"%s - %s", self->name->str,
				self->description->str);
	} else {
		g_string_assign(self->name_and_description, self->name->str);
	}

	if (irreco_str_isempty(self->name->str)) {
		IRRECO_RETURN_PTR(NULL);
	} else {
		IRRECO_RETURN_CONST_STR(self->name_and_description->str);
	}
}

/**
 * Call irreco_backend_read_from_conf() inside the backend.
 */
gboolean irreco_backend_instance_read_from_conf(IrrecoBackendInstance *self)
{
	gchar *filepath;
	IrrecoBackendStatus status;
	IRRECO_ENTER

	if (self->config == NULL) {
		IRRECO_ERROR("Config file is not set. Cannot read config "
			     "of instance \"%s\"\n", self->name->str);
	}

	filepath = irreco_get_config_file("irreco", self->config);
	IRRECO_BACKEND_ENTER(self, "from_conf()");
	status = self->lib->api->from_conf(
		self->contex, filepath);
	IRRECO_BACKEND_RETURN("from_conf()");
	g_free(filepath);
	IRRECO_RETURN_BOOL(irreco_backend_instance_check_status(
		self, status));
}

/**
 * Call irreco_backend_save_to_conf() inside the backend.
 */
gboolean irreco_backend_instance_save_to_conf(IrrecoBackendInstance *self)
{
	gchar *filepath;
	IrrecoBackendStatus status;
	IRRECO_ENTER

	if (self->config == NULL) {
		IRRECO_ERROR("Config file is not set. Cannot save config "
			     "of instance \"%s\"\n", self->name->str);
	}

	filepath = irreco_get_config_file("irreco", self->config);
	IRRECO_BACKEND_ENTER(self, "to_conf()");
	status  = self->lib->api->to_conf(
		self->contex, filepath);
	IRRECO_BACKEND_RETURN("to_conf()");
	g_free(filepath);
	IRRECO_RETURN_BOOL(irreco_backend_instance_check_status(self, status));
}

/**
 * Send command.
 *
 * This function takes device and command name as strings for a purpose.
 * Because the list of supported commands and devices for some instance
 * of a backend depend on the configuration of the backend, and because
 * you cant assume that the configuration remains the same, you cannot
 * assume that the structures allocated by irreco to store those commands
 * will exist in the future.
 *
 * So the proper way to do this is to take a copy of device name and command
 * name, and when were sending command, to check if the command still exists.
 */
gboolean irreco_backend_instance_send_command(IrrecoBackendInstance *self,
					      const gchar *device_name,
					      const gchar *command_name)
{
	IrrecoBackendStatus status;
	IrrecoBackendDevice *device;
	gpointer command_contex;
	IRRECO_ENTER

	IRRECO_PRINTF("Sending command \"%s\" to device \"%s\".\n",
		      command_name, device_name);

	/* Find device. */
	if (!irreco_string_table_get(self->device_list, device_name,
				     (gpointer *) &device)) {
		IRRECO_ERROR("Cant send command \"%s\" to device \"%s\" "
			     "because no such device exists.\n",
			     command_name, device_name);
		IRRECO_RETURN_BOOL(FALSE);
	}

	/* Find command contex. */
	if (!irreco_string_table_get(device->command_list, command_name,
				     (gpointer *) &command_contex)) {
		IRRECO_ERROR("Cant send command \"%s\" to device \"%s\" "
			     "because device does not support that command.\n",
			     command_name, device_name);
		IRRECO_RETURN_BOOL(FALSE);
	}

	/* Send command*/
	IRRECO_BACKEND_ENTER(self, "send_command()");
	status = self->lib->api->send_command(
		self->contex,
		device_name,
		device->contex,
		command_name,
		command_contex);
	IRRECO_BACKEND_RETURN("send_command()");
	IRRECO_RETURN_BOOL(irreco_backend_instance_check_status(
		self, status));
}

void irreco_backend_instance_configure(IrrecoBackendInstance *self,
				       GtkWindow *parent)
{
	IrrecoBackendStatus status;
	IRRECO_ENTER

	IRRECO_BACKEND_ENTER(self, "configure()");
	status = self->lib->api->configure(
		self->contex, parent);
	IRRECO_BACKEND_RETURN("configure()");
	irreco_backend_instance_check_status(self, status);

	IRRECO_RETURN
}

void irreco_backend_instance_create_device(IrrecoBackendInstance *self,
					   GtkWindow *parent)
{
	IrrecoBackendStatus status;
	IRRECO_ENTER

	IRRECO_BACKEND_ENTER(self, "create_device()");
	status = self->lib->api->create_device(
		self->contex, parent);
	IRRECO_BACKEND_RETURN("create_device()");
	irreco_backend_instance_check_status(self, status);

	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Device / Command management                                                */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/**
 * @name Command management
 * @{
 */

/*
 * Used by callbacks
 */
IrrecoBackendInstance	*current_backend_instance = NULL;
IrrecoBackendDevice	*current_device = NULL;

void irreco_backend_get_device_callback(const gchar *name,
					gpointer device_contex)
{
	IrrecoBackendDevice *backend_device;
	IRRECO_ENTER

	if (current_backend_instance == NULL) {
		IRRECO_ERROR("Variable current_backend_instance is not set.\n");
		IRRECO_RETURN
	}
	if (name == NULL) {
		IRRECO_ERROR("Device name is NULL. "
			     "Device name must be given\n");
		IRRECO_RETURN
	}

	if (!irreco_string_table_exists(current_backend_instance->device_list,
					name)) {
		backend_device = irreco_backend_device_create(
			name, current_backend_instance, device_contex);
		irreco_string_table_add(current_backend_instance->device_list,
					backend_device->name, backend_device);
		IRRECO_PRINTF("Device \"%s\" added.\n", backend_device->name);
	}

	IRRECO_RETURN
}

void irreco_backend_get_command_callback(const gchar *name,
					 gpointer command_contex)
{
	IRRECO_ENTER
	if (current_device == NULL) {
		IRRECO_ERROR("Variable current_device is not set.\n");
		IRRECO_RETURN
	}
	if (name == NULL) {
		IRRECO_ERROR("Command name is NULL. "
			     "Command name must be given\n");
		IRRECO_RETURN
	}

	if (!irreco_string_table_exists(current_device->command_list, name)) {
		irreco_string_table_add(current_device->command_list,
					name, command_contex);
		IRRECO_PRINTF("Command \"%s\" added.\n", name);
	}

	IRRECO_RETURN
}

/**
 * This function will first get a list of devices from the backend instance,
 * and after that get a list of commands for every device. After this,
 * Irreco should have a complete list of commands the backend instance provides.
 */
void irreco_backend_instance_get_devcmd_list(IrrecoBackendInstance *self)
{
	IrrecoBackendStatus status;
	IRRECO_ENTER

	/* Destroy old data. */
	irreco_string_table_remove_all(self->device_list);

	/* Fetch a list of devices. */
	current_backend_instance = self;
	IRRECO_BACKEND_ENTER(self, "get_devices()");
	status = self->lib->api->get_devices(
		self->contex, irreco_backend_get_device_callback);
	IRRECO_BACKEND_RETURN("get_devices()");
	irreco_backend_instance_check_status(self, status);
	current_backend_instance = NULL;

	/* Fetch all commands from all devices. */
	IRRECO_BACKEND_INSTANCE_FOREACH(self, backend_device)
		IRRECO_PRINTF("Getting commands of device \"%s\".\n",
			      backend_device->name);

		current_device = backend_device;

		IRRECO_BACKEND_ENTER(self, "get_commands()");
		status = self->lib->api->get_commands(
			self->contex,
			backend_device->name,
			backend_device->contex,
			irreco_backend_get_command_callback);
		IRRECO_BACKEND_RETURN("get_commands()");
		irreco_backend_instance_check_status(self, status);

		current_device = NULL;
		irreco_string_table_sort_abc(backend_device->command_list);
	IRRECO_BACKEND_INSTANCE_FOREACH_END
	irreco_string_table_sort_abc(self->device_list);

	IRRECO_RETURN
}

gboolean irreco_backend_instance_export_conf(IrrecoBackendInstance *self,
				const char * device_name,
				IrrecoBackendFileContainer **file_container)
{
	IrrecoBackendStatus status;
	IRRECO_ENTER

	IRRECO_BACKEND_ENTER(self, "export_conf()");
	status = self->lib->api->export_conf(self->contex, device_name,
					     file_container);
	IRRECO_BACKEND_RETURN("export_conf()");

	IRRECO_RETURN_BOOL(irreco_backend_instance_check_status(self, status));
}

gboolean irreco_backend_instance_import_conf(IrrecoBackendInstance *self,
				IrrecoBackendFileContainer *file_container)
{
	IrrecoBackendStatus status;
	IRRECO_ENTER

	IRRECO_BACKEND_ENTER(self, "import_conf()");
	status = self->lib->api->import_conf(self->contex, file_container);
	IRRECO_BACKEND_RETURN("import_conf()");

	IRRECO_RETURN_BOOL(irreco_backend_instance_check_status(self, status));
}

gboolean irreco_backend_instance_check_conf(IrrecoBackendInstance *self,
				IrrecoBackendFileContainer *file_container)
{
	IrrecoBackendStatus status;
	gboolean file_exist = TRUE;
	IRRECO_ENTER

	IRRECO_BACKEND_ENTER(self, "check_conf()");
	status = self->lib->api->check_conf(self->contex, file_container,
					    &file_exist);
	IRRECO_BACKEND_RETURN("check_conf()");

	irreco_backend_instance_check_status(self, status);

	IRRECO_RETURN_BOOL(file_exist);
}

/** @} */
/** @} */


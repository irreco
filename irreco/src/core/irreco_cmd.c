/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_cmd.h"
#include "irreco_config.h"

/**
 * @addtogroup IrrecoCmd
 * @ingroup Irreco
 *
 * Stores a command which is called when a button is pressed.
 * Serves also as an abstraction between built in commands and backend commands.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoCmd.
 */


void irreco_cmd_clean(IrrecoCmd * irreco_cmd);
gboolean irreco_cmd_get_current_layout_pos(IrrecoData * irreco_data,
					       guint * pos, guint * len);
void irreco_cmd_set_current_layout_pos(IrrecoData * irreco_data,
					   guint pos);



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */


IrrecoCmd *irreco_cmd_create()
{
	IrrecoCmd *irreco_cmd;
	IRRECO_ENTER

	irreco_cmd = g_slice_new0(IrrecoCmd);
	IRRECO_RETURN_PTR(irreco_cmd);
}

void irreco_cmd_destroy(IrrecoCmd * irreco_cmd)
{
	IRRECO_ENTER
	irreco_cmd_clean(irreco_cmd);
	g_slice_free(IrrecoCmd, irreco_cmd);
	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Public Functions                                                           */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */

void irreco_cmd_clean(IrrecoCmd * irreco_cmd)
{
	IRRECO_ENTER
	switch (irreco_cmd->type) {
		case IRRECO_COMMAND_NONE:
		case IRRECO_COMMAND_NEXT_REMOTE:
		case IRRECO_COMMAND_PREVIOUS_REMOTE:
		case IRRECO_COMMAND_FULLSCREEN_TOGGLE:
			break;

		case IRRECO_COMMAND_BACKEND:
			irreco_backend_instance_remove_cmd_dependency(
				irreco_cmd->backend.instance, irreco_cmd);
			irreco_cmd->backend.instance = NULL;
			g_free(irreco_cmd->backend.device_name);
			irreco_cmd->backend.device_name = NULL;
			g_free(irreco_cmd->backend.command_name);
			irreco_cmd->backend.command_name = NULL;
			g_string_free(irreco_cmd->backend.title, TRUE);
			irreco_cmd->backend.title = NULL;
			break;

		case IRRECO_COMMAND_SHOW_LAYOUT:
			g_free(irreco_cmd->layout.name);
			irreco_cmd->layout.name = NULL;
			g_string_free(irreco_cmd->layout.title, TRUE);
			irreco_cmd->backend.title = NULL;
			break;

		case IRRECO_COMMAND_WAIT:
			g_string_free(irreco_cmd->wait.title, TRUE);
			irreco_cmd->wait.title = NULL;
			break;
	}
	irreco_cmd->type = IRRECO_COMMAND_NONE;
	IRRECO_RETURN
}

void irreco_cmd_set_builtin(IrrecoCmd * irreco_cmd,
			    IrrecoCmdType type)
{
	IRRECO_ENTER
	irreco_cmd_clean(irreco_cmd);

	switch (type) {
		case IRRECO_COMMAND_NONE:
		case IRRECO_COMMAND_NEXT_REMOTE:
		case IRRECO_COMMAND_PREVIOUS_REMOTE:
		case IRRECO_COMMAND_FULLSCREEN_TOGGLE:
			irreco_cmd->type = type;
			break;

		default:
			IRRECO_ERROR("Type id \"%i\" is not reserved for"
				     "built-in commands.\n", type);
			break;
	}

	IRRECO_RETURN
}

void irreco_cmd_set_backend(IrrecoCmd * irreco_cmd,
			       IrrecoBackendInstance * instance,
			       const gchar * device_name,
			       const gchar * command_name)
{
	IRRECO_ENTER
	irreco_cmd_clean(irreco_cmd);
	irreco_cmd->type = IRRECO_COMMAND_BACKEND;
	irreco_cmd->backend.instance = instance;
	irreco_cmd->backend.device_name = g_strdup(device_name);
	irreco_cmd->backend.command_name = g_strdup(command_name);
	irreco_cmd->backend.title = g_string_new("");
	irreco_backend_instance_add_cmd_dependency(instance, irreco_cmd);
	IRRECO_RETURN
}

void irreco_cmd_set_layout(IrrecoCmd * irreco_cmd,
			       const gchar * name)
{
	IRRECO_ENTER
	irreco_cmd_clean(irreco_cmd);
	irreco_cmd->type = IRRECO_COMMAND_SHOW_LAYOUT;
	irreco_cmd->layout.name = g_strdup(name);
	irreco_cmd->layout.title = g_string_new("");
	IRRECO_RETURN
}

void irreco_cmd_set_wait(IrrecoCmd * irreco_cmd, gulong delay)
{
	IRRECO_ENTER
	irreco_cmd_clean(irreco_cmd);
	irreco_cmd->type = IRRECO_COMMAND_WAIT;
	irreco_cmd->wait.delay = delay;
	irreco_cmd->wait.title = g_string_new("");
	IRRECO_RETURN
}


/**
 * Copy data from one IrrecoCmd object to another.
 */
void irreco_cmd_copy(IrrecoCmd * from, IrrecoCmd * to)
{
	IRRECO_ENTER
	switch (from->type) {
		case IRRECO_COMMAND_NONE:
		case IRRECO_COMMAND_NEXT_REMOTE:
		case IRRECO_COMMAND_PREVIOUS_REMOTE:
		case IRRECO_COMMAND_FULLSCREEN_TOGGLE:
			irreco_cmd_set_builtin(to, from->type);
			break;

		case IRRECO_COMMAND_BACKEND:
			irreco_cmd_set_backend(to,
				from->backend.instance,
				from->backend.device_name,
				from->backend.command_name);
			break;

		case IRRECO_COMMAND_SHOW_LAYOUT:
			irreco_cmd_set_layout(to, from->layout.name);
			break;

		case IRRECO_COMMAND_WAIT:
			irreco_cmd_set_wait(to, from->wait.delay);
			break;
	}

	IRRECO_RETURN
}

/**
 * Create a new IrrecoCmd based on an old IrrecoCmd.
 */
IrrecoCmd *irreco_cmd_dublicate(IrrecoCmd * old)
{
	IrrecoCmd *new;
	IRRECO_ENTER

	new = irreco_cmd_create();
	irreco_cmd_copy(old, new);
	IRRECO_RETURN_PTR(new);
}

/**
 * Get descriptive title for the command.
 */
const gchar *irreco_cmd_get_long_name(IrrecoCmd * irreco_cmd)
{
	IRRECO_ENTER
	switch (irreco_cmd->type) {
	case IRRECO_COMMAND_NONE:
		IRRECO_RETURN_STR(_(IRRECO_COMMAND_NONE_TITLE));
	case IRRECO_COMMAND_NEXT_REMOTE:
		IRRECO_RETURN_STR(_(IRRECO_COMMAND_NEXT_REMOTE_TITLE));
	case IRRECO_COMMAND_PREVIOUS_REMOTE:
		IRRECO_RETURN_STR(_(IRRECO_COMMAND_PREVIOUS_REMOTE_TITLE));
	case IRRECO_COMMAND_FULLSCREEN_TOGGLE:
		IRRECO_RETURN_STR(_(IRRECO_COMMAND_FULLSCREEN_TOGGLE_TITLE));

	case IRRECO_COMMAND_BACKEND:
		if (irreco_str_isempty(irreco_cmd->backend.title->str)) {
			g_string_printf(irreco_cmd->backend.title, "%s: %s",
					irreco_cmd->backend.device_name,
					irreco_cmd->backend.command_name);
		}
		IRRECO_RETURN_STR(irreco_cmd->backend.title->str);

	case IRRECO_COMMAND_SHOW_LAYOUT:
		if (irreco_str_isempty(irreco_cmd->layout.title->str)) {
			g_string_printf(irreco_cmd->layout.title, "%s%s",
				_(IRRECO_COMMAND_SHOW_LAYOUT_TITLE_PREFIX),
				irreco_cmd->layout.name);
		}
		IRRECO_RETURN_STR(irreco_cmd->layout.title->str);

	case IRRECO_COMMAND_WAIT:
		if (irreco_str_isempty(irreco_cmd->wait.title->str)) {
			g_string_printf(irreco_cmd->wait.title,
					IRRECO_COMMAND_WAIT_TITLE_FORMAT,
					(float)(irreco_cmd->wait.delay) /
					(float)IRRECO_SECOND_IN_USEC);
		}
		IRRECO_RETURN_STR(irreco_cmd->wait.title->str);

	default:
		IRRECO_ERROR("Could not get title for command. "
			     "Which should not be possible!\n");
		IRRECO_RETURN_PTR(NULL);
	}
}

/**
 * Get short name of the command.
 */
const gchar *irreco_cmd_get_short_name(IrrecoCmd * irreco_cmd)
{
	IRRECO_ENTER
	switch (irreco_cmd->type) {
	case IRRECO_COMMAND_NONE:
		IRRECO_RETURN_STR(_(IRRECO_COMMAND_NONE_TITLE));
	case IRRECO_COMMAND_NEXT_REMOTE:
		IRRECO_RETURN_STR(_(IRRECO_COMMAND_NEXT_REMOTE_TITLE));
	case IRRECO_COMMAND_PREVIOUS_REMOTE:
		IRRECO_RETURN_STR(_(IRRECO_COMMAND_PREVIOUS_REMOTE_TITLE));
	case IRRECO_COMMAND_FULLSCREEN_TOGGLE:
		IRRECO_RETURN_STR(_(IRRECO_COMMAND_FULLSCREEN_TOGGLE_NAME));
	case IRRECO_COMMAND_BACKEND:
		IRRECO_RETURN_STR(irreco_cmd->backend.command_name);
	case IRRECO_COMMAND_SHOW_LAYOUT:
		IRRECO_RETURN_STR(irreco_cmd->layout.name);
	case IRRECO_COMMAND_WAIT:
		g_string_printf(irreco_cmd->wait.title,
				"Wait %.1fs",
				(float)(irreco_cmd->wait.delay) /
				(float)IRRECO_SECOND_IN_USEC);
		IRRECO_RETURN_STR(irreco_cmd->wait.title->str);

	default:
		IRRECO_ERROR("Could not get title for command. "
			     "Which should not be possible!\n");
		IRRECO_RETURN_PTR(NULL);
	}
}


void irreco_cmd_print(IrrecoCmd * irreco_cmd)
{
	IRRECO_ENTER
	switch (irreco_cmd->type) {
		case IRRECO_COMMAND_NONE:
			IRRECO_PRINTF("Type: IRRECO_COMMAND_NONE\n");
			break;

		case IRRECO_COMMAND_NEXT_REMOTE:
			IRRECO_PRINTF("Type: IRRECO_COMMAND_NEXT_REMOTE\n");
			break;

		case IRRECO_COMMAND_PREVIOUS_REMOTE:
			IRRECO_PRINTF("Type: IRRECO_COMMAND_PREVIOUS_REMOTE\n");
			break;

		case IRRECO_COMMAND_FULLSCREEN_TOGGLE:
			IRRECO_PRINTF("Type: IRRECO_COMMAND_FULLSCREEN_TOGGLE\n");
			break;

		case IRRECO_COMMAND_BACKEND: {
			const gchar *name = irreco_backend_instance_get_name(
				irreco_cmd->backend.instance);

			IRRECO_PRINTF("Type: IRRECO_COMMAND_BACKEND\n");
			IRRECO_PRINTF("Int/dev/cmd: \"%s\" \"%s\" \"%s\"\n",
				      name,
				      irreco_cmd->backend.device_name,
				      irreco_cmd->backend.command_name);
			} break;

		case IRRECO_COMMAND_SHOW_LAYOUT:
			IRRECO_PRINTF("Type: IRRECO_COMMAND_SHOW_LAYOUT\n");
			IRRECO_PRINTF("Layout: \"%s\"\n",
				      irreco_cmd->layout.name);
			break;

		case IRRECO_COMMAND_WAIT:
			IRRECO_PRINTF("Type: IRRECO_COMMAND_WAIT\n");
			IRRECO_PRINTF("Delay: \"%lu\"\n",
				      irreco_cmd->wait.delay);
			break;

	}

	IRRECO_RETURN
}

gboolean irreco_cmd_execute(IrrecoCmd * irreco_cmd, IrrecoData * irreco_data)
{
	guint len, pos;
	IrrecoButtonLayout *layout;
	IRRECO_ENTER

	IRRECO_PRINTF("Execuging command.\n");
	irreco_cmd_print(irreco_cmd);

	switch (irreco_cmd->type) {
		case IRRECO_COMMAND_NONE: break;

		/* Display next layout in UserUi. */
		case IRRECO_COMMAND_NEXT_REMOTE:
			if (!irreco_cmd_get_current_layout_pos(irreco_data,
				&pos, &len) || len < 2) break;
			if (++pos >= len) pos = 0;
			irreco_cmd_set_current_layout_pos(irreco_data, pos);
			IRRECO_RETURN_BOOL(FALSE);

		/* Display previous layout in UserUi. */
		case IRRECO_COMMAND_PREVIOUS_REMOTE:
			if (!irreco_cmd_get_current_layout_pos(irreco_data,
				&pos, &len) || len < 2) break;
			if (pos > 0) {
				pos--;
			} else {
				pos = irreco_string_table_lenght(
					irreco_data->irreco_layout_array) - 1;

			}
			irreco_cmd_set_current_layout_pos(irreco_data, pos);
			IRRECO_RETURN_BOOL(FALSE);

		case IRRECO_COMMAND_FULLSCREEN_TOGGLE:
			if (irreco_data->window_manager->user_window != NULL) {
				irreco_window_toggle_fullscreen(
				      irreco_data->window_manager->user_window->window);
			}
			IRRECO_RETURN_BOOL(TRUE);

		case IRRECO_COMMAND_BACKEND:
			IRRECO_RETURN_BOOL(irreco_backend_instance_send_command(
				irreco_cmd->backend.instance,
				irreco_cmd->backend.device_name,
				irreco_cmd->backend.command_name));

		case IRRECO_COMMAND_SHOW_LAYOUT:
			if (irreco_string_table_get(
					irreco_data->irreco_layout_array,
					irreco_cmd->layout.name,
					(gpointer *) &layout)) {
				irreco_window_manager_set_layout(
					irreco_data->window_manager, layout);
			}
			IRRECO_RETURN_BOOL(FALSE);

		case IRRECO_COMMAND_WAIT:
			IRRECO_DEBUG_LINE
			IRRECO_ERROR("Wait command should be handled in "
				     "command chain execution function.\n");
			IRRECO_RETURN_BOOL(TRUE);
	}

	IRRECO_ERROR("Could not execute command. Unknown type id \"%i\".\n",
		     irreco_cmd->type);
	IRRECO_RETURN_BOOL(FALSE);
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Layout position finding and setting.                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Layout position finding and setting
 * @{
 */


/**
 * Get the position of the current layout inside the
 * irreco_data->irreco_layout_array, and the lenght of the array.
 */
gboolean irreco_cmd_get_current_layout_pos(IrrecoData * irreco_data,
					   guint * pos, guint * len)
{
	IrrecoButtonLayout *layout;
	IrrecoStringTable *table = irreco_data->irreco_layout_array;
	IRRECO_ENTER

	layout = irreco_data->window_manager->current_layout;
	if (layout == NULL) IRRECO_RETURN_BOOL(FALSE);

	if (!irreco_string_table_get_index(table, layout, pos)) {
		IRRECO_RETURN_BOOL(FALSE);
	}

	*len = irreco_string_table_lenght(table);
	IRRECO_RETURN_BOOL(TRUE);
}

/**
 * Set active layout of User ui by using a position index of the layout in
 * irreco_data->irreco_layout_array.
 */
void irreco_cmd_set_current_layout_pos(IrrecoData * irreco_data,
				       guint pos)
{
	IrrecoButtonLayout *layout;
	IrrecoStringTable *table = irreco_data->irreco_layout_array;
	IRRECO_ENTER

	if (irreco_string_table_index(table, pos, NULL, (gpointer *) &layout)){
		irreco_window_manager_set_layout(irreco_data->window_manager,
						 layout);
	} else {
		irreco_window_manager_set_layout(irreco_data->window_manager,
						 NULL);
	}
	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* IrrecoCmdType                                                              */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */


const gchar *irreco_cmd_type_to_str(IrrecoCmdType type)
{
	IRRECO_ENTER
	switch (type) {
	case IRRECO_COMMAND_NONE:
		IRRECO_RETURN_STR(IRRECO_COMMAND_NONE_STRING);

	case IRRECO_COMMAND_NEXT_REMOTE:
		IRRECO_RETURN_STR(IRRECO_COMMAND_NEXT_REMOTE_STRING);

	case IRRECO_COMMAND_PREVIOUS_REMOTE:
		IRRECO_RETURN_STR(IRRECO_COMMAND_PREVIOUS_REMOTE_STRING);

	case IRRECO_COMMAND_FULLSCREEN_TOGGLE:
		IRRECO_RETURN_STR(IRRECO_COMMAND_FULLSCREEN_TOGGLE_STRING);

	case IRRECO_COMMAND_BACKEND:
		IRRECO_RETURN_STR(IRRECO_COMMAND_SHOW_BACKEND_STRING);

	case IRRECO_COMMAND_SHOW_LAYOUT:
		IRRECO_RETURN_STR(IRRECO_COMMAND_SHOW_LAYOUT_STRING);

	case IRRECO_COMMAND_WAIT:
		IRRECO_RETURN_STR(IRRECO_COMMAND_WAIT_STRING);
	}
	IRRECO_RETURN_PTR(NULL);
}

IrrecoCmdType irreco_cmd_str_to_type(const gchar * type)
{
	IRRECO_ENTER
	if (strcmp(type, IRRECO_COMMAND_NONE_STRING) == 0) {
		IRRECO_RETURN_INT(IRRECO_COMMAND_NONE);

	} else if (strcmp(type, IRRECO_COMMAND_NEXT_REMOTE_STRING) == 0) {
		IRRECO_RETURN_INT(IRRECO_COMMAND_NEXT_REMOTE);

	} else if (strcmp(type, IRRECO_COMMAND_PREVIOUS_REMOTE_STRING) == 0) {
		IRRECO_RETURN_INT(IRRECO_COMMAND_PREVIOUS_REMOTE);

	} else if (strcmp(type, IRRECO_COMMAND_FULLSCREEN_TOGGLE_STRING) == 0) {
		IRRECO_RETURN_INT(IRRECO_COMMAND_FULLSCREEN_TOGGLE);

	} else if (strcmp(type, IRRECO_COMMAND_SHOW_BACKEND_STRING) == 0) {
		IRRECO_RETURN_INT(IRRECO_COMMAND_BACKEND);

	} else if (strcmp(type, IRRECO_COMMAND_SHOW_LAYOUT_STRING) == 0) {
		IRRECO_RETURN_INT(IRRECO_COMMAND_SHOW_LAYOUT);

	} else if (strcmp(type, IRRECO_COMMAND_WAIT_STRING) == 0) {
		IRRECO_RETURN_INT(IRRECO_COMMAND_WAIT);

	} else {
		IRRECO_RETURN_INT(-1);
	}
}

void irreco_cmd_to_keyfile(IrrecoCmd   *command,
			   GKeyFile    *keyfile,
			   const gchar *group)
{
	IRRECO_ENTER

	irreco_gkeyfile_set_string(keyfile, group, "type",
				 irreco_cmd_type_to_str(command->type));

	if (command->type == IRRECO_COMMAND_BACKEND) {
		const gchar *name = irreco_backend_instance_get_name(
			command->backend.instance);

		irreco_gkeyfile_set_string(keyfile, group, "type",
			       IRRECO_COMMAND_SHOW_BACKEND_STRING);
		irreco_gkeyfile_set_string(keyfile, group, "instance-name",
					   name);
		irreco_gkeyfile_set_string(keyfile, group, "device-name",
					   command->backend.device_name);
		irreco_gkeyfile_set_string(keyfile, group, "command-name",
					   command->backend.command_name);

	} else if (command->type == IRRECO_COMMAND_SHOW_LAYOUT) {
		irreco_gkeyfile_set_string(keyfile, group, "type",
				IRRECO_COMMAND_SHOW_LAYOUT_STRING);
		irreco_gkeyfile_set_string(keyfile, group, "show-layout",
					 command->layout.name);

	} else if (command->type == IRRECO_COMMAND_WAIT) {
		g_key_file_set_integer(keyfile, group, "delay",
				       command->wait.delay);
	}

	IRRECO_RETURN
}

static IrrecoCmdType irreco_cmd_type_from_keyfile(IrrecoKeyFile * keyfile)
{
	IrrecoCmdType type;
	gchar *type_name = NULL;
	IRRECO_ENTER

	if (irreco_keyfile_get_str(keyfile, "type", &type_name) == FALSE) {
		IRRECO_RETURN_INT(-1);
	}

	if ((type = irreco_cmd_str_to_type(type_name)) == -1) {
		IRRECO_ERROR("Unknown command type \"%s\"\n", type_name);
	}

	g_free(type_name);
	IRRECO_RETURN_INT(type);
}

IrrecoCmd* irreco_cmd_from_keyfile(IrrecoData * irreco_data,
				   IrrecoKeyFile * keyfile)
{
	IrrecoCmdType type;
	IrrecoCmd *command;
	IRRECO_ENTER

	/* Get type ID. */
	type = irreco_cmd_type_from_keyfile(keyfile);
	if (type == -1) IRRECO_RETURN_PTR(NULL);

	/* Create IrrecoCmd. */
	switch (type) {
	case IRRECO_COMMAND_NONE:
	case IRRECO_COMMAND_NEXT_REMOTE:
	case IRRECO_COMMAND_PREVIOUS_REMOTE:
	case IRRECO_COMMAND_FULLSCREEN_TOGGLE:
		command = irreco_cmd_create();
		irreco_cmd_set_builtin(command, type);
		IRRECO_RETURN_PTR(command);
		break;

	case IRRECO_COMMAND_BACKEND: {
		gchar *instance_name = NULL;
		gchar *device_name = NULL;
		gchar *command_name = NULL;
		IrrecoBackendInstance * backend_instance;

		if (!irreco_keyfile_get_str(keyfile, "instance-name",
					     &instance_name) ||
		    !irreco_keyfile_get_str(keyfile, "device-name",
		    			     &device_name) ||
		    !irreco_keyfile_get_str(keyfile, "command-name",
		    			     &command_name)) {
			g_free(instance_name);
			g_free(device_name);
			g_free(command_name);
			IRRECO_RETURN_PTR(NULL);
		}

		if (!irreco_backend_manager_find_instance(
			irreco_data->irreco_backend_manager,
			instance_name,
			&backend_instance)) {
			IRRECO_ERROR("Could not find instance with name "
				     "\"%s\"\n", instance_name);
			IRRECO_RETURN_PTR(NULL);
		}
		command = irreco_cmd_create();
		irreco_cmd_set_backend(command,
				       backend_instance,
				       device_name,
				       command_name);

		g_free(instance_name);
		g_free(device_name);
		g_free(command_name);
		IRRECO_RETURN_PTR(command);
		} break;

	case IRRECO_COMMAND_SHOW_LAYOUT: {
		gchar *show_layout;
		if (!irreco_keyfile_get_str(keyfile, "show-layout",
					     &show_layout)) {
			IRRECO_RETURN_PTR(NULL);
		}

		command = irreco_cmd_create();
		irreco_cmd_set_layout(command, show_layout);
		g_free(show_layout);
		IRRECO_RETURN_PTR(command);
		} break;

	case IRRECO_COMMAND_WAIT: {
		guint delay;
		if (!irreco_keyfile_get_uint(keyfile, "delay", &delay)) {
			IRRECO_RETURN_PTR(NULL);
		}

		command = irreco_cmd_create();
		irreco_cmd_set_wait(command, delay);
		IRRECO_RETURN_PTR(command);
		} break;
	}

	IRRECO_DEBUG("Unknown command type id \"%i\".\n", type);
	IRRECO_RETURN_PTR(NULL);
}
 /** @} */
 /** @} */


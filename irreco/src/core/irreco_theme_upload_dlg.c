/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *		 2008  Joni Kokko (t5kojo01@students.oamk.fi)
 *		 2008  Harri Vattulainen (t5vaha01@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_theme_upload_dlg.h"
#include "irreco_button_browser_dlg.h"
#include <hildon/hildon-banner.h>


/**
 * @addtogroup IrrecoThemeUploadDlg
 * @ingroup Irreco
 *
 * Allow user to set theme settings and upload it.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoThemeUploadDlg.
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

#define IRRECO_THEME_UPLOAD_PREVIEW_WIDHT	(IRRECO_SCREEN_WIDTH / 6)
#define IRRECO_THEME_UPLOAD_PREVIEW_HEIGHT	(IRRECO_SCREEN_HEIGHT / 6)

/** Loader states. */
enum
{
	LOADER_STATE_INIT,
	LOADER_STATE_THEME,
	LOADER_STATE_BUTTONS,
	LOADER_STATE_BACKGROUNDS,
	LOADER_STATE_END
};

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
static gboolean _texts_ok(IrrecoThemeUploadDlg *self);
static gboolean _send_theme(IrrecoThemeUploadDlg *self);
static void _update_preview(IrrecoThemeUploadDlg *self);
static void _run_button_browser_dlg(GtkWidget      *widget,
				    GdkEventButton *event,
				    IrrecoThemeUploadDlg *self);
static gboolean _get_image_data_and_hash(gchar* img_path,
					 guchar **img_data,
					 gchar **img_sha,
					 gint *img_data_len,
					 gsize buffer_size,
					 gchar **error_msg);
static void _hide_banner(IrrecoThemeUploadDlg *self);
static void _set_banner(IrrecoThemeUploadDlg *self,
			const gchar *text,
			gdouble fraction);
static void _loader_start(IrrecoThemeUploadDlg *self);
static void _loader_stop(IrrecoThemeUploadDlg *self);

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

G_DEFINE_TYPE(IrrecoThemeUploadDlg, irreco_theme_upload_dlg,
	      IRRECO_TYPE_INTERNAL_DLG)

static void irreco_theme_upload_dlg_constructed(GObject *object)
{
	IrrecoThemeUploadDlg *self;

	GtkWidget *hbox_lr;
	GtkWidget *vbox_preview;
	GtkWidget *label_name;
	GtkWidget *label_comments;
	GtkWidget *label_preview;
	GtkWidget *scrolled_comments;
	GtkWidget *scrolled_window;
	GtkWidget *table;
	GtkWidget *frame_comments;
	GtkWidget *frame_preview;

	IRRECO_ENTER

	G_OBJECT_CLASS(irreco_theme_upload_dlg_parent_class)->constructed(object);
	self = IRRECO_THEME_UPLOAD_DLG(object);

	self->comments = NULL;

	/* Construct dialog. */
	gtk_window_set_title(GTK_WINDOW(self), _("Upload theme"));
	gtk_window_set_modal(GTK_WINDOW(self), TRUE);
	gtk_window_set_destroy_with_parent(GTK_WINDOW(self), TRUE);
	gtk_dialog_set_has_separator(GTK_DIALOG(self), FALSE);
	gtk_dialog_add_buttons(GTK_DIALOG(self),
			       _("Upload"), GTK_RESPONSE_OK,
			       NULL);

	/* Create widgets. */
	hbox_lr = gtk_hbox_new(FALSE, 10);
	vbox_preview = gtk_vbox_new(FALSE, 0);
	label_name = gtk_label_new("Theme name:");
	label_comments = gtk_label_new("Comments:   ");
	label_preview = gtk_label_new("Select\npreview image:");
	self->entry_name = gtk_entry_new();
	gtk_editable_set_editable(GTK_EDITABLE(self->entry_name), FALSE);
	self->textview_comments = gtk_text_view_new();
	frame_comments = gtk_frame_new(NULL);

	/* preview */
	self->preview_image = gtk_image_new();
	gtk_image_set_from_file(GTK_IMAGE(self->preview_image), NULL);
	frame_preview = gtk_frame_new(NULL);
	self->preview_event_box = gtk_event_box_new();
	gtk_container_add(GTK_CONTAINER(frame_preview), self->preview_image);
	gtk_container_add(GTK_CONTAINER(self->preview_event_box), frame_preview);
	gtk_widget_set_size_request(frame_preview, 200,200);


	gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(self->textview_comments),
				    GTK_WRAP_WORD_CHAR);
	gtk_widget_set_size_request(self->textview_comments, 400, 150);
	scrolled_comments = gtk_scrolled_window_new(NULL, NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolled_comments),
				       GTK_POLICY_NEVER,
				       GTK_POLICY_AUTOMATIC);
	table = gtk_table_new(4, 2, FALSE);

	/* Window scrolling */
	scrolled_window = gtk_scrolled_window_new(NULL, NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolled_window),
				       GTK_POLICY_NEVER,
				       GTK_POLICY_AUTOMATIC);

	/* Build dialog. */
	/*gtk_box_pack_start_defaults(GTK_BOX(GTK_DIALOG(self)->vbox),
				    hbox_lr);*/
	/* TODO hbox_lr to scrolled and scrolled to self->vbox */
	gtk_box_pack_start_defaults(GTK_BOX(GTK_DIALOG(self)->vbox),
				    scrolled_window);
	gtk_scrolled_window_add_with_viewport(
					GTK_SCROLLED_WINDOW(scrolled_window),
					hbox_lr);
	/*gtk_widget_set_size_request(GTK_WIDGET(hbox_lr), 600, 470);*/
	gtk_window_set_default_size(GTK_WINDOW(self), 600, 340);

	gtk_box_pack_start_defaults(GTK_BOX(hbox_lr), table);
	gtk_box_pack_end_defaults(GTK_BOX(hbox_lr), vbox_preview);
	gtk_box_pack_start_defaults(GTK_BOX(vbox_preview), label_preview);
	gtk_box_pack_end_defaults(GTK_BOX(vbox_preview),
				  self->preview_event_box);
	gtk_container_add(GTK_CONTAINER(scrolled_comments),
			  self->textview_comments);
	gtk_container_add(GTK_CONTAINER (frame_comments),
			  scrolled_comments);
	gtk_table_attach_defaults(GTK_TABLE(table),
				  label_name, 0, 1, 0, 1);
	gtk_table_attach_defaults(GTK_TABLE(table),
				  self->entry_name, 1, 2, 0, 1);
	gtk_table_attach_defaults(GTK_TABLE(table),
				  label_comments, 0, 1, 1, 2);
	/*gtk_table_attach_defaults(GTK_TABLE(table),
				  frame_comments, 0, 2, 2, 3);*/
	gtk_table_attach(GTK_TABLE(table),
				  frame_comments,
				  0, 2, 2, 3,
				  GTK_EXPAND, GTK_EXPAND,
				  5, 0);
	gtk_container_set_border_width (GTK_CONTAINER (self), 5);

	/* Connect preview eventbox and start dialog from there */
	g_signal_connect(G_OBJECT(self->preview_event_box),
			 "button-release-event",
			 G_CALLBACK(_run_button_browser_dlg),
			 IRRECO_THEME_UPLOAD_DLG(self));

	gtk_widget_show_all(GTK_WIDGET(self));
	IRRECO_RETURN
}

static void irreco_theme_upload_dlg_init(IrrecoThemeUploadDlg *self)
{
	IRRECO_ENTER
	IRRECO_RETURN
}

static void irreco_theme_upload_dlg_finalize(GObject *object)
{
	IrrecoThemeUploadDlg *self;
	IRRECO_ENTER

	self = IRRECO_THEME_UPLOAD_DLG(object);

	G_OBJECT_CLASS(irreco_theme_upload_dlg_parent_class)->finalize(object);
	IRRECO_RETURN
}

static void irreco_theme_upload_dlg_class_init(IrrecoThemeUploadDlgClass *klass)
{
	GObjectClass* object_class = G_OBJECT_CLASS (klass);
	/* IrrecoDlgClass* parent_class = IRRECO_DLG_CLASS (klass); */

	object_class->finalize = irreco_theme_upload_dlg_finalize;
	object_class->constructed = irreco_theme_upload_dlg_constructed;
}

GtkWidget *irreco_theme_upload_dlg_new(IrrecoData *irreco_data,
				     GtkWindow *parent_window)
{
	IrrecoThemeUploadDlg *self;
	IRRECO_ENTER

	self = g_object_new(IRRECO_TYPE_THEME_UPLOAD_DLG,
			    "irreco-data", irreco_data,
			    NULL);
	irreco_dlg_set_parent(IRRECO_DLG(self), parent_window);
	IRRECO_RETURN_PTR(self);
}

/** @} */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/**
 * @name Private Functions
 * @{
 */

/**
 * Show hildon progressbar banner.
 *
 * This function will create a new banner if one has not been created yet,
 * if banner already exists, it's properties will be changed.
 *
 * @param text		Text to show.
 * @param fraction	Value of progress.
 */
static void _set_banner(IrrecoThemeUploadDlg *self,
						const gchar *text,
						gdouble fraction)
{
	IRRECO_ENTER

	if (self->banner == NULL) {
		self->banner = hildon_banner_show_progress(
			GTK_WIDGET(self), NULL, "");
	}

	hildon_banner_set_text(HILDON_BANNER(self->banner), text);
	hildon_banner_set_fraction(HILDON_BANNER(self->banner), fraction);

	IRRECO_RETURN
}

/**
 * Destroy banner, if it exists.
 */
static void _hide_banner(IrrecoThemeUploadDlg *self)
{
	IRRECO_ENTER

	if (self->banner) {
		gtk_widget_destroy(self->banner);
		self->banner = NULL;
	}

	IRRECO_RETURN
}

/**
 * Start a loader state machine if one is not running already.
 */
static void _loader_start(IrrecoThemeUploadDlg *self)
{
	IRRECO_ENTER

	if (self->loader_func_id == 0) {
		self->loader_func_id = g_idle_add((GSourceFunc)_send_theme, self);
	}

	IRRECO_RETURN
}

/**
 * Stop and cleanup loader if a loader is running.
 */
static void _loader_stop(IrrecoThemeUploadDlg *self)
{
	IRRECO_ENTER
	_hide_banner(self);
	if (self->loader_func_id != 0) {
		g_source_remove(self->loader_func_id);
		self->loader_func_id = 0;
		self->loader_state = 0;
	}
	IRRECO_RETURN
}

/**
 * Check textfields, return TRUE if everythings fine
 */
static gboolean _texts_ok(IrrecoThemeUploadDlg *self)
{
	gboolean rvalue = TRUE;
	GtkTextIter startiter;
	GtkTextIter enditer;

	IRRECO_ENTER

	if (self->comments != NULL) g_free(self->comments);

	gtk_text_buffer_get_start_iter(GTK_TEXT_BUFFER(self->buffer_comments),
								&startiter);
	gtk_text_buffer_get_end_iter(GTK_TEXT_BUFFER(self->buffer_comments),
								&enditer);
	self->comments = gtk_text_buffer_get_text(
					GTK_TEXT_BUFFER(self->buffer_comments),
					&startiter,
					&enditer,
					FALSE);
	/* a Superb hack */
	if(strlen(self->comments) == 0) {
		self->comments = " ";
		IRRECO_DEBUG("No comment given, "
			     "using space to prevent DB failing\n");
	}

	IRRECO_DEBUG("theme comments: %s\n", self->comments);

	IRRECO_RETURN_BOOL(rvalue);
}

/**
 * Upload theme confs and images, return TRUE if everything went fine
 */
static gboolean _send_theme(IrrecoThemeUploadDlg *self)
{
	IrrecoWebdbTheme *theme;

	IRRECO_ENTER

	switch (self->loader_state) {

	case LOADER_STATE_INIT:

		IRRECO_DEBUG("Loader state init\n");

		self->uploading_errors = FALSE;

		self->cache = self->irreco_data->webdb_cache;

		self->buffer_size =
			irreco_webdb_cache_get_max_image_size(self->cache);
		self->banner_max = irreco_string_table_lenght(
						self->theme->buttons);
		self->banner_max += irreco_string_table_lenght(
						self->theme->backgrounds);
		self->strtablesize = irreco_string_table_lenght(
						self->theme->buttons);
		self->strtableindex = 0;
		_set_banner(self, _("Uploading theme..."), 0);
		self->loader_state = LOADER_STATE_THEME;

		IRRECO_RETURN_BOOL(TRUE);

	case LOADER_STATE_THEME: {

		gchar			*parsedpath;

		IRRECO_DEBUG("Loader state theme\n");

		/* Parse folder out from theme path */
		parsedpath = g_utf8_strrchr(self->theme->path->str,
					    -1,
					    G_DIR_SEPARATOR);
		parsedpath = g_utf8_find_next_char(parsedpath, NULL);

		/* Create new theme */
		self->themeindex = irreco_webdb_cache_create_theme(
					self->cache,
					/*self->theme_name,*/
					self->theme->name->str,
					self->comments,
					self->theme->preview_button_name->str,
					parsedpath,
					self->user,
					self->password);

		/* Theme creation failed for unknown reasons */
		if(self->themeindex == 0) {
			irreco_error_dlg(GTK_WINDOW(self),
					 "Theme creation failed");
			self->uploading_errors = TRUE;
			_loader_stop(self);
			self->uploading_started = FALSE;
			IRRECO_RETURN_BOOL(FALSE);
		}

	/* Theme creation prevented for mismatching username & password pair */
		if(self->themeindex == -1) {
			irreco_error_dlg(GTK_WINDOW(self),
					 "Username and password mismatch");
			self->uploading_errors = TRUE;
			_loader_stop(self);
			self->uploading_started = FALSE;
			IRRECO_RETURN_BOOL(FALSE);
		}

		/* Theme creation prevented for "wrong" user */
		if(self->themeindex == -2) {
			irreco_error_dlg(GTK_WINDOW(self),
					 "Theme name already taken");
			self->uploading_errors = TRUE;
			_loader_stop(self);
			self->uploading_started = FALSE;
			IRRECO_RETURN_BOOL(FALSE);
		}

		self->loader_state = LOADER_STATE_BUTTONS;

		IRRECO_RETURN_BOOL(TRUE);
	}

	case LOADER_STATE_BUTTONS: {

		guchar *img_up_data = g_malloc0(self->buffer_size);
		guchar *img_down_data = g_malloc0(self->buffer_size);
		gchar *img_up_sha; /* SHAs */
		gchar *img_down_sha;
		gchar *img_up_name; /* filenames */
		gchar *img_down_name;
		gint img_up_len; /* img data lengths */
		gint img_down_len;
		GString *img_folder = g_string_new(NULL); /* folder of button */
		gint button_index;
		gchar *error_msg = NULL;
		const gchar *key;
		IrrecoThemeButton *button = NULL;
		gfloat banner;

		IRRECO_DEBUG("Loader state buttons\n");

		irreco_string_table_index(self->theme->buttons,
					  self->strtableindex, &key,
       					  (gpointer *) &button);

		if(button == NULL) {
			IRRECO_DEBUG("No buttons\n");
			goto cleanup_button;
		}

		banner = (gfloat) self->strtableindex/(gfloat) self->banner_max;

		self->strtableindex++;

		_set_banner(self,
			    "Uploading buttons...",
			    banner);

		IRRECO_DEBUG("Current button: %s\n", button->name->str);

		/* button up sizecheck */
		if(irreco_file_length(button->image_up->str) >=
				      self->buffer_size) {
			img_up_data = NULL;
			img_up_sha = NULL;
			img_up_name = NULL;
			irreco_error_dlg(GTK_WINDOW(self),
					 g_strdup_printf(
						"Button %s image too large\n"
						"Max allowed size: %d KiB",
						button->name->str,
						self->buffer_size/1024));
			self->uploading_errors = TRUE;
			goto cleanup_button;
		}

		/* button up */
		if(_get_image_data_and_hash(button->image_up->str,
					    &img_up_data,
					    &img_up_sha,
					    &img_up_len,
					    self->buffer_size,
					    &error_msg)) {

			/* Parse folder out from theme img */
			img_up_name = g_utf8_strrchr(button->image_up->str,
						    -1,
						    G_DIR_SEPARATOR);
			img_up_name = g_utf8_find_next_char(img_up_name, NULL);
		} else {
			img_up_data = NULL;
			img_up_sha = NULL;
			img_up_name = NULL;
			irreco_error_dlg(GTK_WINDOW(self), error_msg);
			self->uploading_errors = TRUE;
			goto cleanup_button;
		}

		/* button down sizecheck */
		if(irreco_file_length(button->image_down->str) >=
				      self->buffer_size) {
			img_down_data = NULL;
			img_down_sha = NULL;
			img_down_name = NULL;
			irreco_error_dlg(GTK_WINDOW(self),
					 g_strdup_printf(
						"Button %s image too large\n"
						"Max allowed size: %d KiB",
						button->name->str,
						self->buffer_size/1024));
			self->uploading_errors = TRUE;
			goto cleanup_button;
		}

		/* button down */
		if(_get_image_data_and_hash(button->image_down->str,
					    &img_down_data,
					    &img_down_sha,
					    &img_down_len,
					    self->buffer_size,
					    &error_msg)) {
			/* Parse folder out from theme img */
			img_down_name = g_utf8_strrchr(button->image_down->str,
						    -1,
						    G_DIR_SEPARATOR);
			img_down_name = g_utf8_find_next_char(img_down_name,
							      NULL);
		} else {
			img_down_data = NULL;
			img_down_sha = NULL;
			img_down_name = NULL;
			IRRECO_DEBUG("No optional down button\n");
		}

		/* Copy whole path+button name */
		g_string_printf(img_folder, "%s", button->image_down->str);
		/* Remove everything past and including rightmost folder char */
		img_folder = g_string_erase(img_folder,
					    strlen(button->image_down->str) -
					    strlen(g_utf8_strrchr(
							button->image_down->str,
							-1,
							G_DIR_SEPARATOR)),
					    -1);
		/* Remove everything prior rightmost folder character */
		g_string_printf(img_folder, "%s", g_utf8_strrchr(img_folder->str,
						-1,
						G_DIR_SEPARATOR));
		/* Remove starting folder character */
		img_folder = g_string_erase(img_folder, 0, 1);

		/* Upload button */
		button_index = irreco_webdb_cache_add_button_to_theme(
						self->cache,
						button->name->str,
						button->allow_text,
						button->text_format_up->str,
						button->text_format_down->str,
						button->text_padding,
						button->text_h_align,
						button->text_v_align,
						img_up_sha,
						img_up_name,
						img_up_data,
						img_up_len,
						img_down_sha,
						img_down_name,
						img_down_data,
						img_down_len,
						img_folder->str,
						self->themeindex,
						self->user,
						self->password);

		if(button_index == 0) {
			irreco_error_dlg(GTK_WINDOW(self),
					 g_strdup_printf(
						"Button %s uploading failed",
						button->name->str));
			self->uploading_errors = TRUE;
		}

		cleanup_button:
		/* Free resources */
		g_free(img_up_data);
		img_up_data = NULL;
		g_free(img_down_data);
		img_down_data = NULL;
		g_string_free(img_folder, TRUE);
		img_folder = NULL;

		if(self->strtablesize == self->strtableindex ||
		   self->strtablesize == 0) {
			self->strtablesize = irreco_string_table_lenght(
						      self->theme->backgrounds);
			self->strtableindex = 0;
			self->loader_state = LOADER_STATE_BACKGROUNDS;
			IRRECO_DEBUG("Next loader state backgrounds\n");
		}

		IRRECO_RETURN_BOOL(TRUE);
	}

	case LOADER_STATE_BACKGROUNDS: {
		GString	*img_folder = g_string_new(NULL);
		guchar	*img_data = g_malloc0(self->buffer_size);
		gchar	*img_sha;
		gchar	*img_name;
		gint	img_len;
		gint	bg_index;
		gchar	*error_msg;
		const gchar	*key;
		IrrecoThemeBg *bg = NULL;
		gfloat banner;

		irreco_string_table_index(self->theme->backgrounds,
					  self->strtableindex, &key,
       					  (gpointer *) &bg);

		if(bg == NULL) {
			IRRECO_DEBUG("No backgrounds\n");
			goto cleanup_bg;
		}

		banner = (gfloat) (self->banner_max -
				   self->strtablesize + self->strtableindex) /
			 (gfloat) self->banner_max;

		self->strtableindex++;

		_set_banner(self, "Uploading backgrounds...", banner);

		IRRECO_DEBUG("Current bg: %s\n", bg->image_name->str);

		/* backgrounds image sizecheck */
		if(irreco_file_length(bg->image_path->str) >=
				      self->buffer_size) {
			img_data = NULL;
			img_sha = NULL;
			img_name = NULL;
			irreco_error_dlg(GTK_WINDOW(self),
					 g_strdup_printf(
						"Background %s image too large\n"
						"Max allowed size: %d KiB",
						bg->image_name->str,
						self->buffer_size/1024));
			self->uploading_errors = TRUE;
			goto cleanup_bg;
		}
		if(_get_image_data_and_hash(bg->image_path->str,
					    &img_data,
					    &img_sha,
					    &img_len,
					    self->buffer_size,
					    &error_msg)) {

			/* Parse folder out from bg img path */
			img_name = g_utf8_strrchr(bg->image_path->str,
						    -1,
						    G_DIR_SEPARATOR);
			img_name = g_utf8_find_next_char(img_name, NULL);
		} else {
			img_data = NULL;
			img_sha = NULL;
			img_name = NULL;
			irreco_error_dlg(GTK_WINDOW(self), error_msg);
			self->uploading_errors = TRUE;
			goto cleanup_bg;
		}

		/* Copy whole path+bg name */
		g_string_printf(img_folder, "%s", bg->image_path->str);
		/* Remove everything past and including rightmost folder char */
		img_folder = g_string_erase(img_folder,
					    strlen(bg->image_path->str) -
					    strlen(g_utf8_strrchr(
							bg->image_path->str,
							-1,
							G_DIR_SEPARATOR)),
					    -1);
		/* Remove everything prior rightmost folder character */
		g_string_printf(img_folder, "%s", g_utf8_strrchr(img_folder->str,
						-1,
						G_DIR_SEPARATOR));
		/* Remove starting folder character */
		img_folder = g_string_erase(img_folder, 0, 1);

		if(img_len >= self->buffer_size) {
			irreco_error_dlg(GTK_WINDOW(self),
					 g_strdup_printf(
						"Bg %s image too large",
						bg->image_name->str));
			self->uploading_errors = TRUE;
			goto cleanup_bg;
		}

		bg_index = irreco_webdb_cache_add_bg_to_theme(
							self->cache,
							bg->image_name->str,
							img_sha,
							img_name,
							img_data,
							img_len,
							img_folder->str,
							self->themeindex,
							self->user,
							self->password);
		if(bg_index == 0) {
			irreco_error_dlg(GTK_WINDOW(self),
					 g_strdup_printf(
						"Bg %s uploading failed",
						bg->image_name->str));
			self->uploading_errors = TRUE;
		}

		cleanup_bg:
		g_free(img_data);
		img_data = NULL;
		g_string_free(img_folder, TRUE);
		img_folder = NULL;

		if(self->strtablesize == self->strtableindex ||
		   self->strtablesize == 0) {
			self->loader_state = LOADER_STATE_END;
			IRRECO_DEBUG("Next loader state end\n");
		}

		IRRECO_RETURN_BOOL(TRUE);
	}

	case LOADER_STATE_END:

		_hide_banner(self);

		if(!self->uploading_errors) {
			if(!irreco_webdb_cache_set_theme_downloadable(
							self->cache,
							self->themeindex,
							TRUE,
							self->user,
							self->password)) {
				irreco_error_dlg(GTK_WINDOW(self),
					 "Failed to make theme downloadable.\n"
					 "Theme won't show at database.");
				self->uploading_errors = TRUE;
			} else {
				irreco_info_dlg(GTK_WINDOW(self),
					"Theme uploaded successfully");
			}

			/* Get latest theme version to just uploaded theme */
			if(!irreco_webdb_cache_get_theme_by_id(self->cache,
							self->themeindex,
							&theme)) {
				/*irreco_error_dlg(GTK_WINDOW(self),
					 "Failed to get theme version");
				self->uploading_errors = TRUE;*/
				IRRECO_DEBUG("Failed to get theme version\n");
				IRRECO_DEBUG("Not fatal bug, continuing\n");

			} else {
				g_string_printf(self->theme->version,
						"%s",
						theme->uploaded->str);
				irreco_theme_update_keyfile(self->theme);
			}

		} else {
			irreco_error_dlg(GTK_WINDOW(self),
					"Some error(s) occured during upload.\n"
					"Please try again.");
			_loader_stop(self);
			self->uploading_started = FALSE;
			IRRECO_RETURN_BOOL(FALSE);
		}
		self->uploading_started = FALSE;
		gtk_dialog_response(GTK_DIALOG(self), GTK_RESPONSE_CLOSE);
		IRRECO_RETURN_BOOL(FALSE);
	}

	IRRECO_DEBUG("Oh no, shouldn't ever be here\n");
	IRRECO_RETURN_BOOL(FALSE);
}

/**
 * Retrieves image data and hash of given file
 *
 * @param img_path Path to file containing binary data
 * @param img_data Set to contain read binary data
 * @param img_sha Set to contain sha1 of data
 * @param img_data_len Set to contain length of data
 * @param buffer_size Max length of data to read
 * @param error_msg Set to contain error message, if error occurs
 * @return True upon success, false otherwise
 */
static gboolean _get_image_data_and_hash(gchar *img_path,
					 guchar **img_data,
					 gchar **img_sha,
					 gint *img_data_len,
					 gsize buffer_size,
					 gchar **error_msg)
{
	gboolean rvalue = TRUE;

	IRRECO_ENTER

	if(irreco_file_exists(img_path)) {
		if(irreco_read_binary_file(img_path,
					    *img_data,
					    buffer_size,
					    img_data_len)) {
			*img_sha = g_compute_checksum_for_data(
								G_CHECKSUM_SHA1,
								*img_data,
								*img_data_len);
		} else {
			IRRECO_DEBUG("File read failed\n");
			*error_msg = "File read failed\n";
			rvalue = FALSE;
		}
	} else {
		IRRECO_DEBUG("No such file\n");
		*error_msg = "No such file\n";
		rvalue = FALSE;
	}

	IRRECO_RETURN_BOOL(rvalue)

}

/**
 * Update shown preview image
 */
static void _update_preview(IrrecoThemeUploadDlg *self)
{
	IRRECO_ENTER

	/* if theme contains preview image, use it, else get first button */
	if(self->preview_name) {
		IRRECO_DEBUG("Preview set in dlg, using it\n");
	} else if(strlen(self->theme->preview_button_name->str) != 0) {
		gchar *strtblkey;
		IRRECO_DEBUG("Preview set to theme, using it\n");
		strtblkey = g_strdup_printf("%s/%s", self->theme->name->str,
				self->theme->preview_button_name->str);
		/*IRRECO_DEBUG("key: %s\n", strtblkey);*/
		if(irreco_string_table_get(self->theme->buttons,
					   strtblkey,
					   (gpointer *) &self->preview_button)) {
			self->preview_name = self->preview_button->image_up->str;
		} else {
			IRRECO_DEBUG("Theme preview set wrong\n");
		}
	} else {
		const gchar *key;
		IRRECO_DEBUG("No preview set, using first button of theme\n");
		irreco_string_table_index(self->theme->buttons,
					  0, &key,
       					  (gpointer *) &self->preview_button);
		if(self->preview_button) {
			self->preview_name = self->preview_button->image_up->str;
		}
	}

	gtk_image_set_from_file(GTK_IMAGE(self->preview_image),
				  self->preview_name);

	gtk_widget_show_all(GTK_WIDGET(self));

	IRRECO_RETURN
}

/** @} */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Public Functions                                                           */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * Show dialog, and ask user for input.
 * @param irreco_data Irreco data
 * @param irreco_theme IrrecoTheme to be uploaded
 */
gboolean irreco_theme_upload_dlg_run(GtkWindow *parent_window,
				     IrrecoData *irreco_data,
				     IrrecoTheme *irreco_theme,
				     gchar *user,
				     gchar *password)
{
	IrrecoThemeUploadDlg *self;
	gint		response;
	gboolean	loop = TRUE;
	gboolean	rvalue = FALSE;

	IRRECO_ENTER

	if(irreco_theme == NULL) { IRRECO_RETURN_BOOL(FALSE); }

	self = (IrrecoThemeUploadDlg*)irreco_theme_upload_dlg_new(irreco_data,
								  parent_window);

	self->parent_window = GTK_WINDOW(self);
	self->irreco_data = irreco_data;
	self->theme = irreco_theme;
	self->preview_name = NULL;
	self->uploading_started = FALSE;
	self->loader_func_id = 0;
	self->user = user;
	self->password = password;

	/* set things to dialog, cause theme & data wasn't set @ _constructed */
	gtk_entry_set_text(GTK_ENTRY(self->entry_name), self->theme->name->str);
	self->buffer_comments = gtk_text_view_get_buffer(
					GTK_TEXT_VIEW(self->textview_comments));
	gtk_text_buffer_set_text(GTK_TEXT_BUFFER(self->buffer_comments),
				 self->theme->comment->str,
				 -1);
	_update_preview(self);

	do {
		response = gtk_dialog_run(GTK_DIALOG(self));
		switch (response) {
		case GTK_RESPONSE_OK:
			IRRECO_DEBUG("GTK_RESPONSE_OK\n");

			if(self->uploading_started) {
				IRRECO_DEBUG("Already uploading theme\n");
				break;
			}

			/* Check themename length and get texts */
			if(!_texts_ok(self)) {
				IRRECO_DEBUG("Input failure\n");
				break;
			}

			/* Show login/reg dialog */
			if(self->user == NULL || self->password == NULL)
			if(!irreco_show_login_dlg(irreco_data,
						  parent_window,
						  &self->user,
						  &self->password)) {
				IRRECO_DEBUG("Failed login\n");
				break;
			}

			/* set new info to theme */
			irreco_theme_set_author(self->theme, self->user);
			irreco_theme_set_comment(self->theme, self->comments);
			if(!self->preview_button) {
				irreco_error_dlg(GTK_WINDOW(self),
					"Please select preview image first");
				break;
			}
			irreco_theme_set_preview_button(self->theme,
					self->preview_button->name->str);
			self->uploading_started = TRUE;
			self->loader_state = LOADER_STATE_INIT;
			IRRECO_DEBUG("Start loader\n");
			_loader_start(self);

			break;

		case GTK_RESPONSE_DELETE_EVENT:
			IRRECO_DEBUG("GTK_RESPONSE_DELETE_EVENT\n");
			_loader_stop(self);
			if(self->uploading_started) {
				irreco_info_dlg(GTK_WINDOW(self),
						"Theme uploading aborted");
			}
			loop = FALSE;
			break;

		case GTK_RESPONSE_CLOSE:
			IRRECO_DEBUG("GTK_RESPONSE_CLOSE\n");
			_loader_stop(self);
			loop = FALSE;
			rvalue = TRUE;
			break;

		default:
			IRRECO_DEBUG("Something went horribly wrong\n");
			break;
		}

	} while (loop);

	gtk_widget_destroy(GTK_WIDGET(self));

	IRRECO_RETURN_BOOL(rvalue);
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Events and Callbacks                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Events and Callbacks
 * @{
 */

/**
 * Run button browser where one button can be selected
 * Stores selected buttons name and buttonstructure
 */
static void _run_button_browser_dlg(GtkWidget *widget,
				    GdkEventButton *event,
				    IrrecoThemeUploadDlg *self)
{
	IrrecoThemeButton *preview_theme_button = NULL;

	IRRECO_ENTER

	preview_theme_button = irreco_button_browser_dlg_run(
			GTK_WINDOW(self),
			self->irreco_data,
			self->theme);

	if(preview_theme_button) {
		IRRECO_DEBUG("Set preview button to theme and self->\n");
		self->preview_name = preview_theme_button->image_up->str;
		self->preview_button = preview_theme_button;
		_update_preview(self);
	}

	IRRECO_RETURN
}

/** @} */



/** @} */

/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_cmd_chain_setup_dlg.h"

/**
 * @addtogroup IrrecoCmdChainSetupDlg
 * @ingroup Irreco
 *
 * Dialog for setting some properties of command chains.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoCmdChainSetupDlg.
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
void irreco_cmd_chain_setup_dlg_responce_signal(IrrecoCmdChainSetupDlg *self,
						gint response);
void irreco_cmd_chain_setup_dlg_progress_toggled(GtkToggleButton *togglebutton,
                                                 IrrecoCmdChainSetupDlg *self);



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

G_DEFINE_TYPE(IrrecoCmdChainSetupDlg, irreco_cmd_chain_setup_dlg,
	      IRRECO_TYPE_DLG)

/*
static void irreco_cmd_chain_setup_dlg_finalize(GObject *object)
{
	G_OBJECT_CLASS(irreco_cmd_chain_setup_dlg_parent_class)
		->finalize(object);
}

static void
irreco_cmd_chain_setup_dlg_class_init(IrrecoCmdChainSetupDlgClass *klass)
{
	GObjectClass *object_class;
	IRRECO_ENTER

	object_class = G_OBJECT_CLASS(klass);
	object_class->finalize = irreco_cmd_chain_setup_dlg_finalize;
	IRRECO_RETURN
}
*/

static void
irreco_cmd_chain_setup_dlg_class_init(IrrecoCmdChainSetupDlgClass *klass)
{
	IRRECO_ENTER
	IRRECO_RETURN
}

static void irreco_cmd_chain_setup_dlg_init(IrrecoCmdChainSetupDlg *self)
{
	GtkWidget *vbox;
	GtkWidget *hbox;
	/*
	GtkWidget *label_repeat;
	GtkWidget *radio_repeat;
	GtkWidget *radio_norepeat;
	*/
	IRRECO_ENTER

	/* Construct dialog. */
	gtk_window_set_title(GTK_WINDOW(self), _("Command chain properties"));
	gtk_window_set_modal(GTK_WINDOW(self), TRUE);
	gtk_window_set_destroy_with_parent(GTK_WINDOW(self), TRUE);
	gtk_dialog_set_has_separator(GTK_DIALOG(self), FALSE);
	gtk_dialog_add_buttons(GTK_DIALOG(self),
			       GTK_STOCK_OK, GTK_RESPONSE_OK,
			       NULL);

	/* Create widgets. */
	vbox = gtk_vbox_new(0, 8);
	/*
	radio_repeat = gtk_radio_button_new_with_label(NULL, _("Yes"));
	radio_norepeat = gtk_radio_button_new_with_label_from_widget(
		GTK_RADIO_BUTTON(radio_repeat), _("No"));
	label_repeat = gtk_label_new(_("Should the command chain be "
				       "automatically repeated if user "
				       "keeps the button presseed."));
	gtk_label_set_line_wrap(GTK_LABEL(label_repeat), TRUE);
	gtk_misc_set_alignment(GTK_MISC(label_repeat), 0, 0.5);
	*/
	self->combo_box = gtk_combo_box_new_text();

	/* Add rate selection setting. */
	gtk_box_pack_start(GTK_BOX(vbox),
			   irreco_gtk_label_bold(_("Command execution rate"),
			   0, 0.5, 0, 0, 0, 0), 0, 0, 0);
	hbox = gtk_hbox_new(0, 8);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, 0, 0, 0);
	gtk_box_pack_start(GTK_BOX(hbox),
			   irreco_gtk_label(_("Execute one command every"),
			   0, 0.5, 0, 0, 16, 0), 0, 0, 0);
	gtk_box_pack_start(GTK_BOX(hbox), self->combo_box, 0, 0, 0);
	gtk_box_pack_start(GTK_BOX(hbox),
			   irreco_gtk_label(_("seconds."),
			   0, 0.5, 0, 0, 5, 0), 0, 0, 0);

	/* Add options. */
	gtk_box_pack_start(GTK_BOX(vbox),
			   irreco_gtk_label_bold(_("Options"),
			   0, 0.5, 8, 0, 0, 0), 0, 0, 0);
	self->progbar_check = gtk_check_button_new_with_label(
		_("Show progressbar while executing several commands."));
	gtk_box_pack_start(GTK_BOX(vbox),
			   irreco_gtk_pad(self->progbar_check, 0, 0, 16, 0),
			   0, 0, 0);


	/* Add repeat setting. *
	gtk_box_pack_start(GTK_BOX(vbox),
			   irreco_gtk_label_bold(_("Command chain repeating"),
			   0, 0.5, 16, 0, 0, 0), 0, 0, 0);
	gtk_box_pack_start(GTK_BOX(vbox),
			   irreco_gtk_pad(label_repeat, 0, 0, 16, 0),
			   1, 1, 1);
	hbox = gtk_hbox_new(0, 8);
	gtk_box_pack_start(GTK_BOX(vbox),
			   irreco_gtk_pad(hbox, 0, 0, 16, 0),
			   0, 0, 0);
	gtk_box_pack_start(GTK_BOX(hbox), radio_repeat, 0, 0, 0);
	gtk_box_pack_start(GTK_BOX(hbox), radio_norepeat, 0, 0, 0);
	*/

	/* Setup dialgo. */
	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(self)->vbox),
			  irreco_gtk_pad(vbox, 8, 8, 8, 8));
	gtk_widget_show_all(GTK_WIDGET(self));

	/* Fill combobox. */
	gtk_combo_box_append_text(GTK_COMBO_BOX(self->combo_box), "0.0");
	gtk_combo_box_append_text(GTK_COMBO_BOX(self->combo_box), "0.1");
	gtk_combo_box_append_text(GTK_COMBO_BOX(self->combo_box), "0.2");
	gtk_combo_box_append_text(GTK_COMBO_BOX(self->combo_box), "0.3");
	gtk_combo_box_append_text(GTK_COMBO_BOX(self->combo_box), "0.4");
	gtk_combo_box_append_text(GTK_COMBO_BOX(self->combo_box), "0.6");
	gtk_combo_box_append_text(GTK_COMBO_BOX(self->combo_box), "0.8");
	gtk_combo_box_append_text(GTK_COMBO_BOX(self->combo_box), "1.0");
	gtk_combo_box_append_text(GTK_COMBO_BOX(self->combo_box), "1.2");
	gtk_combo_box_append_text(GTK_COMBO_BOX(self->combo_box), "1.4");
	gtk_combo_box_append_text(GTK_COMBO_BOX(self->combo_box), "1.6");
	gtk_combo_box_append_text(GTK_COMBO_BOX(self->combo_box), "1.8");
	gtk_combo_box_append_text(GTK_COMBO_BOX(self->combo_box), "2.0");
	gtk_combo_box_append_text(GTK_COMBO_BOX(self->combo_box), "2.5");
	gtk_combo_box_append_text(GTK_COMBO_BOX(self->combo_box), "3.0");
	gtk_combo_box_append_text(GTK_COMBO_BOX(self->combo_box), "5.0");
	gtk_combo_box_append_text(GTK_COMBO_BOX(self->combo_box), "7.0");
	gtk_combo_box_append_text(GTK_COMBO_BOX(self->combo_box), "10.0");

	/* Connect signals. */
	g_signal_connect(G_OBJECT(self), "response",
			 G_CALLBACK(irreco_cmd_chain_setup_dlg_responce_signal),
			 NULL);
	g_signal_connect(G_OBJECT(self->progbar_check), "toggled",
			G_CALLBACK(irreco_cmd_chain_setup_dlg_progress_toggled),
			 self);

	IRRECO_RETURN
}

GtkWidget* irreco_cmd_chain_setup_dlg_new(GtkWindow *parent,
					  IrrecoCmdChain *cmd_chain)
{
	IrrecoCmdChainSetupDlg *self;
	IRRECO_ENTER

	self = g_object_new(IRRECO_TYPE_CMD_CHAIN_SETUP_DLG, NULL);
	irreco_dlg_set_parent(IRRECO_DLG(self), parent);
	irreco_cmd_chain_setup_dlg_set_chain(self, cmd_chain);
	IRRECO_RETURN_PTR(self);
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Private Functions
 * @{
 */

/* Add functions here. */

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Public Functions                                                           */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */

void irreco_cmd_chain_setup_dlg_set_chain(IrrecoCmdChainSetupDlg *self,
					  IrrecoCmdChain *cmd_chain)
{
	gint i;
	GString* rate;
	GString* combo;
	IRRECO_ENTER

	self->cmd_chain = cmd_chain;

	combo = g_string_new(NULL);
	rate = g_string_new(NULL);
	g_string_printf(rate, "%.1f", ((gfloat) self->cmd_chain->execution_rate)
			/ IRRECO_SECOND_IN_USEC);

	/** @todo
	 * This is a terrible way to set the selected item.
	 * Replace text combo box with proper model / view structure.
	 */
	for (i = 0; i < 17; i++) {
		gtk_combo_box_set_active(GTK_COMBO_BOX(self->combo_box), i);
		irreco_gstring_set_and_free(combo,
					    gtk_combo_box_get_active_text(
					    GTK_COMBO_BOX(self->combo_box)));
		if (strcmp(combo->str, rate->str) == 0) {
			i = -1;
			break;
		}
	}

	g_string_free(combo, FALSE);
	g_string_free(rate, FALSE);
	if (i > 0) gtk_combo_box_set_active(GTK_COMBO_BOX(self->combo_box), 0);

	/* Set progress checkbox. */
	gtk_toggle_button_set_active(
		GTK_TOGGLE_BUTTON(self->progbar_check),
		irreco_cmd_chain_get_show_progress(self->cmd_chain));

	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Events and Callbacks                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Events and Callbacks
 * @{
 */

void irreco_cmd_chain_setup_dlg_responce_signal(IrrecoCmdChainSetupDlg *self,
						gint response)
{
	IRRECO_ENTER
	if (response == GTK_RESPONSE_OK) {
		gchar *text;
		gfloat rate_f;
		glong rate_l;

		IRRECO_PRINTF("GTK_RESPONSE_OK\n");

		text = gtk_combo_box_get_active_text(
			GTK_COMBO_BOX(self->combo_box));
		sscanf(text, "%f", &rate_f);
		g_free(text);

		rate_l = IRRECO_SECONDS_TO_USEC(rate_f);
		irreco_cmd_chain_set_execution_rate(self->cmd_chain, rate_l);
	}
	IRRECO_RETURN
}

void irreco_cmd_chain_setup_dlg_progress_toggled(GtkToggleButton *togglebutton,
                                                 IrrecoCmdChainSetupDlg *self)
{
	IRRECO_ENTER
	irreco_cmd_chain_set_show_progress(
		self->cmd_chain, gtk_toggle_button_get_active(togglebutton));
	IRRECO_RETURN
}

/** @} */

/** @} */

































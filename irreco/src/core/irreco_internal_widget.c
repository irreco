/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007,2008  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_internal_widget.h"

/**
 * @addtogroup IrrecoInternalWidget
 * @ingroup Irreco
 *
 * A widget class which has IrrecoData construction property.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoInternalWidget.
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

enum
{
	PROP_0,
	PROP_IRRECO_DATA
};

typedef struct _IrrecoInternalWidgetPrivate IrrecoInternalWidgetPrivate;
struct _IrrecoInternalWidgetPrivate {
	IrrecoData *irreco_data;
};

#define _GET_PRIVATE(_self) \
	G_TYPE_INSTANCE_GET_PRIVATE( \
		IRRECO_INTERNAL_WIDGET(_self), \
		IRRECO_TYPE_INTERNAL_WIDGET, \
		IrrecoInternalWidgetPrivate);



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

G_DEFINE_TYPE(IrrecoInternalWidget, irreco_internal_widget, GTK_TYPE_VBOX)

static void
irreco_internal_widget_init (IrrecoInternalWidget *object)
{
	/* TODO: Add initialization code here */
}

static void
irreco_internal_widget_finalize (GObject *object)
{
	/* TODO: Add deinitalization code here */

	G_OBJECT_CLASS (irreco_internal_widget_parent_class)->finalize (object);
}

static void
irreco_internal_widget_set_property (GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec)
{
	IrrecoInternalWidgetPrivate *priv = NULL;
	g_return_if_fail (IRRECO_IS_INTERNAL_WIDGET (object));

	switch (prop_id)
	{
	case PROP_IRRECO_DATA:
		priv = _GET_PRIVATE(object);
		priv->irreco_data = (IrrecoData*) g_value_get_pointer(value);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static void
irreco_internal_widget_get_property (GObject *object, guint prop_id, GValue *value, GParamSpec *pspec)
{
	IrrecoInternalWidgetPrivate *priv = NULL;
	g_return_if_fail (IRRECO_IS_INTERNAL_WIDGET (object));

	switch (prop_id)
	{
	case PROP_IRRECO_DATA:
		priv = _GET_PRIVATE(object);
		g_value_set_pointer(value, priv->irreco_data);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static void
irreco_internal_widget_class_init (IrrecoInternalWidgetClass *klass)
{
	GObjectClass* object_class = G_OBJECT_CLASS (klass);
	/*GtkHBoxClass* parent_class = GTK_HBOX_CLASS (klass);*/

	object_class->finalize = irreco_internal_widget_finalize;
	object_class->set_property = irreco_internal_widget_set_property;
	object_class->get_property = irreco_internal_widget_get_property;

	g_type_class_add_private(klass, sizeof (IrrecoInternalWidgetPrivate));

	g_object_class_install_property(
		object_class,
	        PROP_IRRECO_DATA,
	        g_param_spec_pointer(
			"irreco-data",
	                "irreco-data",
	                "IrrecoData",
	                G_PARAM_READABLE |
			G_PARAM_WRITABLE |
			G_PARAM_CONSTRUCT_ONLY));
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Public Functions                                                           */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */

IrrecoData *irreco_internal_widget_get_irreco_data(IrrecoInternalWidget *self)
{
	IrrecoInternalWidgetPrivate *priv = NULL;
	IRRECO_ENTER

	priv = _GET_PRIVATE(self);
	IRRECO_RETURN_PTR(priv->irreco_data);
}

/** @} */

/** @} */


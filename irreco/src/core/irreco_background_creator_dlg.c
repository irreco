/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * irreco - Ir Remote Control
 * Copyright (C) 2008 Pekka Gehör (pegu6@msn.com)
 *
 * irreco_background_creator_dlg.c is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * irreco_background_creator_dlg.c is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "irreco_background_creator_dlg.h"
#include "irreco_theme_creator_dlg.h"
#include <hildon/hildon-banner.h>
#include <hildon/hildon-color-button.h>
#include <hildon/hildon-file-chooser-dialog.h>

/**
 * @addtogroup IrrecoBackgroundCreatorDlg
 * @ingroup Irreco
 *
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoBackgroundCreatorDlg.
 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

#define IRRECO_BACKGROUND_PREVIEW_WIDHT	(IRRECO_SCREEN_WIDTH/2.5)
#define IRRECO_BACKGROUND_PREVIEW_HEIGHT (IRRECO_SCREEN_HEIGHT/2.5)

/** Loader states. */
enum
{
	LOADER_STATE_INIT,
	LOADER_STATE_THEME,
	LOADER_STATE_BUTTONS,
	LOADER_STATE_BACKGROUNDS,
	LOADER_STATE_END
};

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
static void _signal_image_clicked(GtkButton *button,
				  IrrecoBackgroundCreatorDlg *self);
static gboolean _draw_preview_image(IrrecoBackgroundCreatorDlg *self,
				    const gchar *image);
void
_set_bg_details(IrrecoBackgroundCreatorDlg *self, IrrecoTheme *irreco_theme,
		IrrecoThemeBg *bg);

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

G_DEFINE_TYPE (IrrecoBackgroundCreatorDlg, irreco_background_creator_dlg,
	       IRRECO_TYPE_INTERNAL_DLG)

static void irreco_background_creator_dlg_constructed(GObject *object)
{

	IrrecoBackgroundCreatorDlg	*self;
	GtkWidget			*table;
	GtkWidget			*frame;
	GtkWidget			*label_name;
	GtkWidget			*label_select;
	GtkWidget			*button_select;
	GtkWidget			*vbox_preview;
	IRRECO_ENTER

	G_OBJECT_CLASS(
	irreco_background_creator_dlg_parent_class)->constructed(object);

	self = IRRECO_BACKGROUND_CREATOR_DLG(object);

	/* Construct dialog. */
	gtk_window_set_title(GTK_WINDOW(self), _("Create a Background"));
	gtk_window_set_modal(GTK_WINDOW(self), TRUE);
	gtk_window_set_destroy_with_parent(GTK_WINDOW(self), TRUE);
	gtk_dialog_set_has_separator(GTK_DIALOG(self), FALSE);

	/*Buttons*/
	self->cancel_button = gtk_dialog_add_button(GTK_DIALOG(self),
						   _("Cancel"),
						   GTK_RESPONSE_CANCEL);
	self->add_button = gtk_dialog_add_button(GTK_DIALOG(self),
						_("Add"), GTK_RESPONSE_OK);
	gtk_widget_set_sensitive(self->add_button, FALSE);

	/* Create widgets. */

	table = gtk_table_new(8, 9, TRUE);
	label_name = gtk_label_new(_("Name:"));
	label_select = gtk_label_new(_("Background:"));
	self->entry_name = gtk_entry_new();
	frame = gtk_frame_new(NULL);
	vbox_preview = gtk_vbox_new(FALSE, 8);
	self->label_size = gtk_label_new("");
	self->preview_image = gtk_image_new();
	button_select = gtk_button_new_with_label(_("Select"));

	gtk_box_pack_start_defaults(GTK_BOX(GTK_DIALOG(self)->vbox),
				    table);

	/* equal to the text of the left-side */
	gtk_misc_set_alignment(GTK_MISC(label_name), 0, 0.5);
	gtk_misc_set_alignment(GTK_MISC(label_select), 0, 0.5);

	/* Set widgets on the table */

	gtk_table_set_row_spacings(GTK_TABLE(table), 6);
	gtk_table_set_col_spacings(GTK_TABLE(table), 6);

	gtk_table_attach_defaults(GTK_TABLE(table), label_name, 0, 3, 0, 1);
	gtk_table_attach_defaults(GTK_TABLE(table), label_select, 0, 3, 1, 2);
	gtk_table_attach_defaults(GTK_TABLE(table),
				  self->entry_name, 3, 9, 0, 1);
	gtk_table_attach_defaults(GTK_TABLE(table), button_select, 3, 9, 1, 2);
	gtk_table_attach_defaults(GTK_TABLE(table), frame, 0, 9, 2, 8);

	gtk_container_add(GTK_CONTAINER(frame), vbox_preview);
	gtk_container_add(GTK_CONTAINER(vbox_preview), self->preview_image);
	gtk_container_add(GTK_CONTAINER(vbox_preview), self->label_size);

	/* Button signals. */
	g_signal_connect(G_OBJECT(button_select), "clicked",
			 G_CALLBACK(_signal_image_clicked),
			 self);

	gtk_widget_show_all(GTK_WIDGET(self));

	IRRECO_RETURN
}

static void
irreco_background_creator_dlg_init (IrrecoBackgroundCreatorDlg *self)
{
	IRRECO_ENTER
	self->filename = g_string_new(NULL);
	IRRECO_RETURN
}

static void
irreco_background_creator_dlg_finalize(GObject *object)
{
	/* TODO: Add deinitalization code here */
	IrrecoBackgroundCreatorDlg *self;
	IRRECO_ENTER

	self = IRRECO_BACKGROUND_CREATOR_DLG(object);
	g_string_free(self->filename, TRUE);
	self->filename = NULL;

	G_OBJECT_CLASS(irreco_background_creator_dlg_parent_class)->finalize(object);
	IRRECO_RETURN

}

static void
irreco_background_creator_dlg_class_init(IrrecoBackgroundCreatorDlgClass *klass)
{
	GObjectClass* object_class = G_OBJECT_CLASS (klass);
	object_class->finalize = irreco_background_creator_dlg_finalize;
	object_class->constructed = irreco_background_creator_dlg_constructed;
}

GtkWidget
*irreco_background_creator_dlg_new(IrrecoData *irreco_data, GtkWindow *parent)
{
	IrrecoBackgroundCreatorDlg *self;

	IRRECO_ENTER

	self = g_object_new(IRRECO_TYPE_BACKGROUND_CREATOR_DLG,
			    "irreco-data", irreco_data, NULL);
	irreco_dlg_set_parent(IRRECO_DLG(self), parent);

	IRRECO_RETURN_PTR(self);
}

/**
 * @deprecated
 * @todo Replace calls to irreco_background_creator_dlg_create() with
 *       irreco_background_creator_dlg_new().
 */
IrrecoBackgroundCreatorDlg
*irreco_background_creator_dlg_create(IrrecoData *irreco_data,
				      GtkWindow *parent_window)
{
	IRRECO_ENTER
	IRRECO_RETURN_PTR(irreco_background_creator_dlg_new(
			  irreco_data, parent_window));
}

/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/**
 * @name Private Functions
 * @{
 */
/**
 * Draw preview with current image.
 */
static gboolean
_draw_preview_image(IrrecoBackgroundCreatorDlg *self, const gchar *image)
{
	GError 		*error = NULL;
	GdkPixbuf 	*pixbuf = NULL;
	GdkPixbuf 	*pixbuf_size = NULL;
	GString		*size = g_string_new(NULL);
	gint		widht;
	gint		height;
	IRRECO_ENTER
	g_assert(self != NULL);

	/* Attempt to load the image. */
	if (image != NULL) {
		pixbuf = gdk_pixbuf_new_from_file_at_scale(image,
						IRRECO_BACKGROUND_PREVIEW_WIDHT,
      						IRRECO_BACKGROUND_PREVIEW_HEIGHT,
	    					TRUE, &error);
		/* set sensitive if image are selected */
		gtk_widget_set_sensitive(self->add_button, TRUE);

		if (irreco_gerror_check_print(&error)) {
			IRRECO_RETURN_BOOL(FALSE);
		}
	}
	gtk_widget_realize(GTK_WIDGET(self->preview_image));

	/* Show image real size */
	pixbuf_size =  gdk_pixbuf_new_from_file(image, &error);

	widht = gdk_pixbuf_get_width(pixbuf_size);
	height = gdk_pixbuf_get_height(pixbuf_size);
	g_string_printf(size, "%sx%s", g_strdup_printf("%d", widht),
			 g_strdup_printf("%d", height));

	gtk_label_set_text(GTK_LABEL(self->label_size), size->str);
	if (pixbuf_size != NULL) g_object_unref(G_OBJECT(pixbuf_size));

	gtk_image_set_from_pixbuf(GTK_IMAGE(self->preview_image),
				  GDK_PIXBUF(pixbuf));
	/* Set image path */
	g_string_printf(self->filename, "%s", image);

	if (pixbuf != NULL) g_object_unref(G_OBJECT(pixbuf));
	g_string_free(size, TRUE);
	IRRECO_RETURN_BOOL(TRUE);
}

static void _select_image(GtkButton *button, IrrecoBackgroundCreatorDlg *self)
{

	GtkWidget *file_dlg = NULL;
	gchar *image_dir = NULL;
	IRRECO_ENTER

	/* Create image select dialog. */

	file_dlg = hildon_file_chooser_dialog_new(GTK_WINDOW(self),
						  GTK_FILE_CHOOSER_ACTION_OPEN);
	gtk_window_set_title(GTK_WINDOW(file_dlg),_("Select background image"));
	gtk_file_chooser_set_local_only(GTK_FILE_CHOOSER(file_dlg), TRUE);
	image_dir = g_build_path("/", getenv("HOME"), "MyDocs/.images/", NULL);
	gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(file_dlg),
					    image_dir);
	g_free(image_dir);

	/* Loop until user cancels or we get a valid image. */
	gtk_widget_show_all(GTK_WIDGET(file_dlg));
	while (gtk_dialog_run(GTK_DIALOG(file_dlg)) == GTK_RESPONSE_OK) {
		gchar *filename = gtk_file_chooser_get_filename(
						    GTK_FILE_CHOOSER(file_dlg));

		/* Attempt to display the image. */
		if (_draw_preview_image(self, filename)) {
			irreco_gstring_set_and_free(self->filename, filename);
			filename = NULL;
			break;

		/* Cleanup */
		} else {
			gchar *basename = g_path_get_basename(filename);
			irreco_error_dlg_printf(GTK_WINDOW(file_dlg),
						_("Cannot open image \"%s\""),
						basename);
			IRRECO_PRINTF("Image invalid.\n");

			g_free(basename);
			g_free(filename);
		}
	}

	gtk_widget_destroy(file_dlg);
	IRRECO_RETURN
}
gboolean
irreco_background_creator_dlg_check_details(IrrecoBackgroundCreatorDlg *self)
{
	gboolean	rvalue = TRUE;
	IRRECO_ENTER

	/*check that it is not the same name button*/
	if (irreco_string_table_exists (self->theme->backgrounds,
	    gtk_entry_get_text(GTK_ENTRY(self->entry_name)))) {
		irreco_error_dlg(GTK_WINDOW(self),
				 "Background has already existed");
		rvalue = FALSE;
	}
	if (g_utf8_strlen(gtk_entry_get_text(GTK_ENTRY(self->entry_name)), 1) ==0) {
		irreco_error_dlg(GTK_WINDOW(self),
				 "Set Background name");
		rvalue = FALSE;
	}
	IRRECO_RETURN_BOOL(rvalue);
}
/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Functions                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Public Functions                                                           */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
gboolean irreco_background_creator_dlg_run(IrrecoData *irreco_data,
					   IrrecoTheme *irreco_theme,
					   GtkWindow *parent_window,
					   IrrecoThemeBg *bg)
{
	IrrecoBackgroundCreatorDlg	*self;
	gint				response;
	gboolean			loop = TRUE;
	gboolean			rvalue = FALSE;
	gboolean			edit = FALSE;
	IRRECO_ENTER

	self = (IrrecoBackgroundCreatorDlg*)irreco_background_creator_dlg_create(
		irreco_data, parent_window);
	self->irreco_data = irreco_data;
	self->theme = irreco_theme;
	IRRECO_DEBUG("Pointer: %p \n", (void*) self->theme);
	IRRECO_DEBUG("Pointer: %p \n", (void*) irreco_theme);

	irreco_theme_bg_print(bg);

	if (g_utf8_strlen(bg->image_name->str, 1) >0) {

		/* Sets the bg details */
		_set_bg_details(self, irreco_theme, bg);
		edit = TRUE;
	}

	do {
		response = gtk_dialog_run(GTK_DIALOG(self));
		switch (response) {
		case GTK_RESPONSE_OK:

			if (edit) {
				irreco_theme_bg_set(bg,
					    gtk_entry_get_text(GTK_ENTRY(
					    self->entry_name)),
					    self->filename->str);
				irreco_theme_bg_print(bg);
				loop = FALSE;
				rvalue = TRUE;

			} else {
				/* Check button name */
				if (irreco_background_creator_dlg_check_details(
				    self)) {
					irreco_theme_bg_set(bg,
							gtk_entry_get_text(
							GTK_ENTRY(
							self->entry_name)),
							self->filename->str);
					irreco_theme_bg_print(bg);
					loop = FALSE;
					rvalue = TRUE;
				} else {
					rvalue = FALSE;
					loop = TRUE;
				}

			}
			break;
		case GTK_RESPONSE_CANCEL:
			IRRECO_DEBUG("GTK_RESPONSE_CANCEL\n");
			rvalue = FALSE;
			loop = FALSE;
			break;

		default:
			IRRECO_DEBUG("default\n");
			break;
		}

	} while (loop);

	gtk_widget_destroy(GTK_WIDGET(self));

	IRRECO_RETURN_BOOL(rvalue);
}

void
_set_bg_details(IrrecoBackgroundCreatorDlg *self, IrrecoTheme *irreco_theme,
		IrrecoThemeBg *bg)
{
	IRRECO_ENTER

	gtk_entry_set_text(GTK_ENTRY(self->entry_name),
			   bg->image_name->str);

	_draw_preview_image(self, bg->image_path->str);

	/* Set button label */
	gtk_button_set_label(GTK_BUTTON(self->add_button), "Save");
	gtk_window_set_title(GTK_WINDOW(self), _("Edit a Background"));
	gtk_widget_set_sensitive(self->entry_name, FALSE);

	IRRECO_RETURN
}

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Events and Callbacks                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Events and Callbacks
 * @{
 */

static void
_signal_image_clicked(GtkButton *button, IrrecoBackgroundCreatorDlg *self)
{
	IRRECO_ENTER
	_select_image(button, self);
	IRRECO_RETURN
}

/** @} */

/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007,2008  Arto Karppinen (arto.karppinen@iki.fi),
 * Joni Kokko (t5kojo01@students.oamk.fi),
 * Harri Vattulainen (t5vaha01@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_webdb_dlg.h"
#include <hildon/hildon-banner.h>
#include "irreco_webdb_register_dlg.h"
#include "irreco_select_instance_dlg.h"
#include "irreco_config.h"
#include <hildon/hildon-gtk.h>
#include <hildon/hildon-pannable-area.h>


/**
 * @addtogroup IrrecoWebdbDlg
 * @ingroup Irreco
 *
 * @todo PURPOCE OF CLASS.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoWebdbDlg.
 */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
static void irreco_webdb_dlg_clean_details(IrrecoWebdbDlg *self);
static gboolean irreco_webdb_dlg_loader_categ(IrrecoWebdbDlg *self);
static gboolean irreco_webdb_dlg_loader_manuf(IrrecoWebdbDlg *self);
static gboolean irreco_webdb_dlg_loader_model(IrrecoWebdbDlg *self);
static gboolean irreco_webdb_dlg_loader_config(IrrecoWebdbDlg *self);
static gboolean irreco_webdb_dlg_display_config_detail(IrrecoWebdbDlg *self,
						       GtkTreeIter * iter);
static gboolean irreco_webdb_dlg_load_file(IrrecoWebdbDlg *self);

static gboolean irreco_show_webdb_dlg_map_event(IrrecoWebdbDlg *self,
						GdkEvent  *event,
						gpointer   user_data);
static void irreco_show_webdb_dlg_destroy_event(IrrecoWebdbDlg *self,
						gpointer user_data);
static void irreco_show_webdb_dlg_row_activated_event(GtkTreeView *tree_view,
						      GtkTreePath *path,
						      GtkTreeViewColumn *column,
						      IrrecoWebdbDlg *self);
static void irreco_show_webdb_dlg_row_expanded_event(GtkTreeView *tree_view,
						     GtkTreeIter *iter,
						     GtkTreePath *path,
						     IrrecoWebdbDlg *self);
static void irreco_show_webdb_dlg_row_collapsed_event(GtkTreeView *tree_view,
						     GtkTreeIter *iter,
						     GtkTreePath *path,
						     IrrecoWebdbDlg *self);
static void irreco_show_webdb_dlg_row_selected_event(GtkTreeSelection *sel,
						     IrrecoWebdbDlg *self);




/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/** Button responce codes. */
enum {
	IRRECO_DEVICE_REFRESH
};

/** GtkTreeStore colums. */
enum
{
	TEXT_COL,
	FLAG_COL,
	DATA_COL,
	N_COLUMNS
};

/** Loader states. */
enum
{
	LOADER_STATE_INIT,
	LOADER_STATE_LOOP,
	LOADER_STATE_END,
	LOADER_STATE_CLEANUP
};

/** Row flags. */
enum
{
	ROW_CHILDREN_LOADED	= 1 << 1,
	ROW_TYPE_CATEGORY	= 1 << 2,
	ROW_TYPE_MANUFACTURER	= 1 << 3,
	ROW_TYPE_MODEL		= 1 << 4,
 	ROW_TYPE_CONFIG		= 1 << 5,
	ROW_TYPE_LOADING	= 1 << 6
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

G_DEFINE_TYPE(IrrecoWebdbDlg, irreco_webdb_dlg, IRRECO_TYPE_DLG)

static void irreco_webdb_dlg_finalize(GObject *object)
{
	if (G_OBJECT_CLASS(irreco_webdb_dlg_parent_class)->finalize)
		G_OBJECT_CLASS(irreco_webdb_dlg_parent_class)->finalize(object);
}

static void irreco_webdb_dlg_class_init(IrrecoWebdbDlgClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS(klass);
	object_class->finalize = irreco_webdb_dlg_finalize;
}

static void irreco_webdb_dlg_init(IrrecoWebdbDlg *self)
{
	GtkTreeViewColumn *column;
	GtkCellRenderer *renderer;
	GtkTreeSelection *select;
	GtkWidget *label_user;
	GtkWidget *label_backend;
	GtkWidget *label_category;
	GtkWidget *label_manuf;
	GtkWidget *label_model;
	GtkWidget *label_file_uploaded;
	GtkWidget *label_file_download_count;
	GtkWidget *config_info_alignment;
	GtkWidget *config_info_frame;
	GtkWidget *tree_view_frame;
	GtkWidget *tree_view_hbox;
	PangoFontDescription *initial_font;
/*	GtkWidget *pannablearea;*/

	IRRECO_ENTER

	/* Construct dialog. */
	gtk_window_set_title(GTK_WINDOW(self),
			     _("Download device from IrrecoDB"));
	gtk_window_set_modal(GTK_WINDOW(self), TRUE);
	gtk_window_set_destroy_with_parent(GTK_WINDOW(self), TRUE);
	gtk_dialog_set_has_separator(GTK_DIALOG(self), FALSE);
	gtk_dialog_add_buttons(GTK_DIALOG(self),
			       GTK_STOCK_REFRESH, IRRECO_DEVICE_REFRESH,
			       GTK_STOCK_OK, GTK_RESPONSE_OK,
			       NULL);
	/* Create hbox */
	self->hbox = g_object_new(GTK_TYPE_HBOX, NULL);
	gtk_box_set_spacing(GTK_BOX(self->hbox), 8);
	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(self)->vbox),
			  irreco_gtk_align(GTK_WIDGET(self->hbox),
					   0, 0, 1, 1, 8, 8, 8, 8));
	/* Create tree_view_hbox */
	tree_view_hbox = g_object_new(GTK_TYPE_HBOX, NULL);

	/* Create Treeview */
	self->tree_store = gtk_tree_store_new(
		N_COLUMNS, G_TYPE_STRING, G_TYPE_INT, G_TYPE_POINTER);
/*	self->tree_view = GTK_TREE_VIEW(gtk_tree_view_new_with_model(
		GTK_TREE_MODEL(self->tree_store)));*/
	self->tree_view = hildon_gtk_tree_view_new_with_model(
					HILDON_UI_MODE_NORMAL,
					GTK_TREE_MODEL(self->tree_store));

	renderer = gtk_cell_renderer_text_new();
	column = gtk_tree_view_column_new_with_attributes(
		NULL, renderer, "text", TEXT_COL, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(self->tree_view), column);

	/* Treeview to pannablearea */
	/*TODO*/
	gtk_box_pack_start(GTK_BOX(tree_view_hbox), GTK_WIDGET(self->tree_view),
			   TRUE, TRUE, 0);

	/* Create scrollbar for Treeview */
	/*self->scrollbar = gtk_vscrollbar_new(gtk_tree_view_get_vadjustment(
					     GTK_TREE_VIEW(self->tree_view)));
	gtk_box_pack_start(GTK_BOX(tree_view_hbox), GTK_WIDGET(self->scrollbar),
			   FALSE, TRUE, 0);TODO rm*/

	/* Create Frame for Treeview */
	tree_view_frame = gtk_frame_new("");
	gtk_frame_set_label_widget(GTK_FRAME(tree_view_frame),
		irreco_gtk_label_bold("Devices", 0, 0, 0, 0, 0, 0));

/*	pannablearea = hildon_pannable_area_new();
	hildon_pannable_area_add_with_viewport(HILDON_PANNABLE_AREA(pannablearea), tree_view_hbox);*/

	gtk_container_add(GTK_CONTAINER(tree_view_frame),
			  GTK_WIDGET(tree_view_hbox));

	gtk_box_pack_start(GTK_BOX(self->hbox), GTK_WIDGET(tree_view_frame),
			   TRUE, TRUE, 0);

	/* Create config_info */
	self->config_info = gtk_table_new(7, 2, FALSE);
	gtk_table_set_row_spacings(GTK_TABLE(self->config_info), 4);

	label_user = gtk_label_new("Creator: ");
	gtk_misc_set_alignment(GTK_MISC(label_user), 0, 0.5);
	gtk_table_attach_defaults(GTK_TABLE(self->config_info),
				  label_user, 0, 1, 0, 1);

	label_backend = gtk_label_new("Backend: ");
	gtk_misc_set_alignment(GTK_MISC(label_backend), 0, 0.5);
	gtk_table_attach_defaults(GTK_TABLE(self->config_info),
				  label_backend, 0, 1, 1, 2);

	label_category = gtk_label_new("Category: ");
	gtk_misc_set_alignment(GTK_MISC(label_category), 0, 0.5);
	gtk_table_attach_defaults(GTK_TABLE(self->config_info),
				  label_category, 0, 1, 2, 3);

	label_manuf = gtk_label_new("Manufacturer: ");
	gtk_misc_set_alignment(GTK_MISC(label_manuf), 0, 0.5);
	gtk_table_attach_defaults(GTK_TABLE(self->config_info),
				  label_manuf, 0, 1, 3, 4);

	label_model = gtk_label_new("Model: ");
	gtk_misc_set_alignment(GTK_MISC(label_model), 0, 0.5);
	gtk_table_attach_defaults(GTK_TABLE(self->config_info),
				  label_model, 0, 1, 4, 5);

	label_file_uploaded = gtk_label_new("Created: ");
	gtk_misc_set_alignment(GTK_MISC(label_file_uploaded), 0, 0.5);
	gtk_table_attach_defaults(GTK_TABLE(self->config_info),
				  label_file_uploaded, 0, 1, 5, 6);

	label_file_download_count = gtk_label_new("Downloaded: ");
	gtk_misc_set_alignment(GTK_MISC(label_file_download_count), 0, 0.5);
	gtk_table_attach_defaults(GTK_TABLE(self->config_info),
				  label_file_download_count, 0, 1, 6, 7);

	self->config_user = gtk_label_new("-");
	gtk_misc_set_alignment(GTK_MISC(self->config_user), 0, 0.5);
	gtk_table_attach(GTK_TABLE(self->config_info), self->config_user,
			 1, 2, 0, 1, GTK_FILL, GTK_FILL, 0, 0);

	self->config_backend = gtk_label_new("-");
	gtk_misc_set_alignment(GTK_MISC(self->config_backend), 0, 0.5);
	gtk_table_attach(GTK_TABLE(self->config_info), self->config_backend,
			 1, 2, 1, 2, GTK_FILL, GTK_FILL, 0, 0);

	self->config_category = gtk_label_new("-");
	gtk_misc_set_alignment(GTK_MISC(self->config_category), 0, 0.5);
	gtk_table_attach(GTK_TABLE(self->config_info), self->config_category,
			 1, 2, 2, 3, GTK_FILL, GTK_FILL, 0, 0);

	self->config_manuf = gtk_label_new("-");
	gtk_misc_set_alignment(GTK_MISC(self->config_manuf), 0, 0.5);
	gtk_table_attach(GTK_TABLE(self->config_info), self->config_manuf,
			 1, 2, 3, 4, GTK_FILL, GTK_FILL, 0, 0);

	self->config_model = gtk_label_new("-");
	gtk_misc_set_alignment(GTK_MISC(self->config_model), 0, 0.5);
	gtk_table_attach(GTK_TABLE(self->config_info), self->config_model,
			 1, 2, 4, 5, GTK_FILL, GTK_FILL, 0, 0);

	self->config_uploaded = gtk_label_new("-");
	gtk_misc_set_alignment(GTK_MISC(self->config_uploaded), 0, 0.5);
	gtk_table_attach(GTK_TABLE(self->config_info),
			 self->config_uploaded,
			 1, 2, 5, 6, GTK_FILL, GTK_FILL, 0, 0);

	self->config_download_count = gtk_label_new("-");
	gtk_misc_set_alignment(GTK_MISC(self->config_download_count), 0, 0.5);
	gtk_table_attach(GTK_TABLE(self->config_info),
			 self->config_download_count,
			 1, 2, 6, 7, GTK_FILL, GTK_FILL, 0, 0);

	/* Resize text */
	initial_font = pango_font_description_from_string ("Sans 9");
	gtk_widget_modify_font (label_user, initial_font);
	gtk_widget_modify_font (label_backend, initial_font);
	gtk_widget_modify_font (label_category, initial_font);
	gtk_widget_modify_font (label_manuf, initial_font);
	gtk_widget_modify_font (label_model, initial_font);
	gtk_widget_modify_font (label_file_uploaded, initial_font);
	gtk_widget_modify_font (label_file_download_count, initial_font);
	gtk_widget_modify_font (self->config_user, initial_font);
	gtk_widget_modify_font (self->config_backend, initial_font);
	gtk_widget_modify_font (self->config_category, initial_font);
	gtk_widget_modify_font (self->config_manuf, initial_font);
	gtk_widget_modify_font (self->config_model, initial_font);
	gtk_widget_modify_font (self->config_uploaded, initial_font);
	gtk_widget_modify_font (self->config_download_count, initial_font);


	/* Create frame for config_info */
	config_info_frame = gtk_frame_new("");
	gtk_frame_set_label_widget(GTK_FRAME(config_info_frame),
		irreco_gtk_label_bold("Details", 0, 0, 0, 0, 0, 0));

	gtk_box_pack_start(GTK_BOX(self->hbox),
			   GTK_WIDGET(config_info_frame),
				      FALSE, TRUE, 0);

	config_info_alignment = gtk_alignment_new(0, 0, 0, 0);
	gtk_alignment_set_padding(GTK_ALIGNMENT(config_info_alignment),
				  0, 8, 8, 8);
	gtk_container_add(GTK_CONTAINER(config_info_frame),
			  GTK_WIDGET(config_info_alignment));

	gtk_container_add(GTK_CONTAINER(config_info_alignment),
			  GTK_WIDGET(self->config_info));

	/* Setup the selection handler for TREE	*/
	select = gtk_tree_view_get_selection(GTK_TREE_VIEW(self->tree_view));
	gtk_tree_selection_set_mode(select, GTK_SELECTION_SINGLE);

	/* Signal handlers. */
	g_signal_connect(G_OBJECT(self), "map-event",
			 G_CALLBACK(irreco_show_webdb_dlg_map_event), NULL);
	g_signal_connect(G_OBJECT(self), "destroy",
			 G_CALLBACK(irreco_show_webdb_dlg_destroy_event), NULL);
	g_signal_connect(G_OBJECT(self->tree_view), "row-activated",
			 G_CALLBACK(irreco_show_webdb_dlg_row_activated_event),
			 self);
	g_signal_connect(G_OBJECT(self->tree_view), "row-expanded",
			 G_CALLBACK(irreco_show_webdb_dlg_row_expanded_event),
			 self);
	g_signal_connect(G_OBJECT(self->tree_view), "row-collapsed",
			 G_CALLBACK(irreco_show_webdb_dlg_row_collapsed_event),
			 self);
	g_signal_connect(G_OBJECT (select), "changed",
			 G_CALLBACK (irreco_show_webdb_dlg_row_selected_event),
			 self);


	gtk_tree_view_set_enable_tree_lines(GTK_TREE_VIEW(self->tree_view), TRUE);
	g_object_set (G_OBJECT (self->tree_view), "show-expanders", TRUE, NULL);
	g_object_set (G_OBJECT (self->tree_view), "level-indentation", 0, NULL);
	gtk_tree_view_set_rubber_banding(GTK_TREE_VIEW(self->tree_view), FALSE);

	gtk_widget_set_size_request(GTK_WIDGET(self), -1, 335);
	gtk_widget_show_all(GTK_WIDGET(self));
	IRRECO_RETURN
}

GtkWidget *irreco_webdb_dlg_new(IrrecoData *irreco_data,
				GtkWindow *parent)
{
	IrrecoWebdbDlg *self;
	IRRECO_ENTER

	self = g_object_new(IRRECO_TYPE_WEBDB_DLG, NULL);
	irreco_dlg_set_parent(IRRECO_DLG(self), parent);
	irreco_webdb_dlg_set_irreco_data(self, irreco_data);
	IRRECO_RETURN_PTR(self);
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Private Functions
 * @{
 */

/**
 * Clean detail list
 *
 */
static void irreco_webdb_dlg_clean_details(IrrecoWebdbDlg *self)
{
	IRRECO_ENTER

	self->config = NULL;
	gtk_label_set_text(
		GTK_LABEL(self->config_user), "-");
	gtk_label_set_text(
		GTK_LABEL(self->config_backend), "-");
	gtk_label_set_text(
		GTK_LABEL(self->config_category), "-");
	gtk_label_set_text(
		GTK_LABEL(self->config_manuf), "-");
	gtk_label_set_text(
		GTK_LABEL(self->config_model), "-");
	gtk_label_set_text(
		GTK_LABEL(self->config_uploaded), "-");
	gtk_label_set_text(
		GTK_LABEL(self->config_download_count), "-");

	IRRECO_RETURN
}

/**
 * Have the childern of this item been loaded.
 *
 * @return TRUE if children have been loade, FALSE otherwise.
 */
static gboolean irreco_webdb_dlg_row_is_loaded(IrrecoWebdbDlg *self,
					       GtkTreeIter *iter)
{
	gint i;
	IRRECO_ENTER

	gtk_tree_model_get(GTK_TREE_MODEL(self->tree_store),
			   iter, FLAG_COL, &i, -1);
	if (i & ROW_CHILDREN_LOADED) IRRECO_RETURN_BOOL(TRUE);
	IRRECO_RETURN_BOOL(FALSE);
}

/**
 * Enable / Disable ROW_CHILDREN_LOADED flag from a row.
 *
 * @param value If set, ROW_CHILDREN_LOADED will be enabled.
 */
static void irreco_webdb_dlg_row_set_loaded(IrrecoWebdbDlg *self,
					    GtkTreeIter *iter,
					    gboolean value)
{
	gint i;
	IRRECO_ENTER

	gtk_tree_model_get(GTK_TREE_MODEL(self->tree_store),
			   iter, FLAG_COL, &i, -1);
	if (value) {
		i = i | ROW_CHILDREN_LOADED;
	} else {
		i = i & ~ROW_CHILDREN_LOADED;
	}
	gtk_tree_store_set(self->tree_store, iter, FLAG_COL, i, -1);

	IRRECO_RETURN
}

/**
 * Get type of row.
 */
static gint irreco_webdb_dlg_row_get_type(IrrecoWebdbDlg *self,
					  GtkTreeIter *iter)
{
	gint i;
	IRRECO_ENTER

	gtk_tree_model_get(GTK_TREE_MODEL(self->tree_store),
			   iter, FLAG_COL, &i, -1);
	if (i & ROW_TYPE_CATEGORY)
		IRRECO_RETURN_ENUM(ROW_TYPE_CATEGORY);
	if (i & ROW_TYPE_MANUFACTURER)
		IRRECO_RETURN_ENUM(ROW_TYPE_MANUFACTURER);
	if (i & ROW_TYPE_MODEL)
		IRRECO_RETURN_ENUM(ROW_TYPE_MODEL);
	if (i & ROW_TYPE_CONFIG)
		IRRECO_RETURN_ENUM(ROW_TYPE_CONFIG);
	if (i & ROW_TYPE_LOADING)
		IRRECO_RETURN_ENUM(ROW_TYPE_LOADING);
	IRRECO_RETURN_INT(0);
}

/**
 * Show hildon progressbar banner.
 *
 * This function will create a new banner if one has not been created yet,
 * if banner already exists, it's properties will be changed.
 *
 * @param text		Text to show.
 * @param fraction	Value of progress.
 */
static void irreco_webdb_dlg_set_banner(IrrecoWebdbDlg *self,
					const gchar *text,
					gdouble fraction)
{
	IRRECO_ENTER
	if (self->banner == NULL) {
		self->banner = hildon_banner_show_progress(
			GTK_WIDGET(self), NULL, "");
	}

	hildon_banner_set_text(HILDON_BANNER(self->banner), text);
	hildon_banner_set_fraction(HILDON_BANNER(self->banner), fraction);
	IRRECO_RETURN
}

/**
 * Destroy banner, if it exists.
 */
static void irreco_webdb_dlg_hide_banner(IrrecoWebdbDlg *self)
{
	IRRECO_ENTER
	if (self->banner) {
		gtk_widget_destroy(self->banner);
		self->banner = NULL;
	}
	IRRECO_RETURN
}

/**
 * Start a loader state machine if one is not running already.
 */
static void irreco_webdb_dlg_loader_start(IrrecoWebdbDlg *self,
					  GSourceFunc function,
					  GtkTreeIter *parent_iter)
{
	IRRECO_ENTER

	if (self->loader_func_id == 0) {
		if (parent_iter) {
			self->loader_parent_iter = gtk_tree_iter_copy(
				parent_iter);
		}

		if (function) {
			self->loader_func_id = g_idle_add(function, self);
		} else {
			IRRECO_ERROR("Loader function pointer not given.\n");
		}
	}

	IRRECO_RETURN
}

/**
 * Stop and cleanup loader if a loader is running.
 */
static void irreco_webdb_dlg_loader_stop(IrrecoWebdbDlg *self)
{
	IRRECO_ENTER
	if (self->loader_func_id != 0) {
		g_source_remove(self->loader_func_id);
		self->loader_func_id = 0;
		self->loader_state = 0;
		if (self->loader_parent_iter) {
			gtk_tree_iter_free(self->loader_parent_iter);
		}
		self->loader_parent_iter = NULL;
	}
	IRRECO_RETURN
}

/**
 * Web Database Category loader.
 *
 * This loader will request a list of categories from the Web Database and
 * update the TreeView accordingly. Every category row created by this loader
 * will have row type ROW_TYPE_CATEGORY.
 */
static gboolean irreco_webdb_dlg_loader_categ(IrrecoWebdbDlg *self)
{
	IRRECO_ENTER

	switch (self->loader_state) {
	case LOADER_STATE_INIT:
		irreco_webdb_dlg_set_banner(self, _("Loading ..."), 0);
		self->loader_state = LOADER_STATE_LOOP;
		irreco_webdb_dlg_clean_details(self);
		IRRECO_RETURN_BOOL(TRUE);

	case LOADER_STATE_LOOP: {
		GtkTreeIter iter;
		GtkTreeIter iter_loading;
		IrrecoStringTable *categories = NULL;
		IrrecoWebdbCache *webdb_cache = NULL;

		webdb_cache = irreco_data_get_webdb_cache(self->irreco_data,
							  FALSE);
		if(irreco_webdb_cache_get_categories(webdb_cache, &categories)){
			IRRECO_STRING_TABLE_FOREACH_KEY(categories, key)

				/* Append categogy. */
				gtk_tree_store_append(self->tree_store,
						      &iter, NULL);
				gtk_tree_store_set(self->tree_store,
						   &iter, TEXT_COL, key,
						   FLAG_COL, ROW_TYPE_CATEGORY,
						   DATA_COL, NULL, -1);

				/* Add loading item into category. */
				gtk_tree_store_append(self->tree_store,
						      &iter_loading, &iter);
				gtk_tree_store_set(self->tree_store,
						   &iter_loading,
						   TEXT_COL, _("Loading ..."),
						   FLAG_COL, ROW_TYPE_LOADING,
						   DATA_COL, NULL, -1);
			IRRECO_STRING_TABLE_FOREACH_END
		} else {
			irreco_error_dlg(GTK_WINDOW(self),
					 irreco_webdb_cache_get_error(
					 webdb_cache));
		}

		irreco_webdb_dlg_set_banner(self, _("Loading ..."), 1);
		self->loader_state = LOADER_STATE_END;
		IRRECO_RETURN_BOOL(TRUE);
		}

	case LOADER_STATE_END:
		irreco_webdb_dlg_hide_banner(self);
		irreco_webdb_dlg_loader_stop(self);
	}

	IRRECO_RETURN_BOOL(FALSE);
}

/**
 * Web Database Manufacturer loader.
 *
 * @todo
 */
static gboolean irreco_webdb_dlg_loader_manuf(IrrecoWebdbDlg *self)
{
	IRRECO_ENTER

	switch (self->loader_state) {
	case LOADER_STATE_INIT:
		irreco_webdb_dlg_set_banner(self, _("Loading ..."), 0);
		self->loader_state = LOADER_STATE_LOOP;
		IRRECO_RETURN_BOOL(TRUE);

	case LOADER_STATE_LOOP: {
		GtkTreeIter iter;
		GtkTreeIter iter_loading;
		IrrecoStringTable *manufacturers = NULL;
		IrrecoWebdbCache *webdb_cache = NULL;
		gchar *category;

		gtk_tree_model_get(GTK_TREE_MODEL(self->tree_store),
				   self->loader_parent_iter,
				   TEXT_COL, &category, -1);

		webdb_cache = irreco_data_get_webdb_cache(self->irreco_data,
							  FALSE);

		if(irreco_webdb_cache_get_manufacturers(webdb_cache,
							category,
		   					&manufacturers)){

			IRRECO_STRING_TABLE_FOREACH_KEY(manufacturers, key)

				/* Add manufacturer item into category. */
				gtk_tree_store_append(self->tree_store,
				       &iter, self->loader_parent_iter);
				gtk_tree_store_set(self->tree_store,
						&iter,
						TEXT_COL, key,
						FLAG_COL, ROW_TYPE_MANUFACTURER,
						DATA_COL, NULL, -1);

				/* Add loading item into manufacturer. */
				gtk_tree_store_append(self->tree_store,
						      &iter_loading, &iter);
				gtk_tree_store_set(self->tree_store,
						   &iter_loading,
						   TEXT_COL, _("Loading ..."),
						   FLAG_COL, ROW_TYPE_LOADING,
						   DATA_COL, NULL, -1);

			IRRECO_STRING_TABLE_FOREACH_END
		} else {
			irreco_error_dlg(GTK_WINDOW(self),
					 irreco_webdb_cache_get_error(
					 webdb_cache));
		}

		g_free (category);

		/* Delete loading item from category. */
		gtk_tree_model_iter_nth_child(GTK_TREE_MODEL(self->tree_store),
					      &iter,self->loader_parent_iter,0);
		if(irreco_webdb_dlg_row_get_type(self,
						&iter) == ROW_TYPE_LOADING) {
			gtk_tree_store_remove(self->tree_store,&iter);
		}
		irreco_webdb_dlg_set_banner(self, _("Loading ..."), 1);
		self->loader_state = LOADER_STATE_END;
		IRRECO_RETURN_BOOL(TRUE);
		}

	case LOADER_STATE_END:
		irreco_webdb_dlg_row_set_loaded(self, self->loader_parent_iter,
						TRUE);
		irreco_webdb_dlg_hide_banner(self);
		irreco_webdb_dlg_loader_stop(self);
	}

	IRRECO_RETURN_BOOL(FALSE);
}

/**
 * Web Database Model loader.
 *
 * @todo
 */
static gboolean irreco_webdb_dlg_loader_model(IrrecoWebdbDlg *self)
{
	IRRECO_ENTER

	switch (self->loader_state) {
	case LOADER_STATE_INIT:
		irreco_webdb_dlg_set_banner(self, _("Loading ..."), 0);
		self->loader_state = LOADER_STATE_LOOP;
		IRRECO_RETURN_BOOL(TRUE);

	case LOADER_STATE_LOOP: {
		GtkTreeIter iter;
		GtkTreeIter iter_loading;
		GtkTreeIter category_iter;
		IrrecoStringTable *models = NULL;
		IrrecoWebdbCache *webdb_cache = NULL;
		gchar *category;
		gchar *manufacturer;

		/*Get category and manufacturer*/

		gtk_tree_model_get(GTK_TREE_MODEL(self->tree_store),
				   self->loader_parent_iter,
				   TEXT_COL, &manufacturer, -1);

		gtk_tree_model_iter_parent(GTK_TREE_MODEL(self->tree_store),
					   &category_iter,
					   self->loader_parent_iter);

		gtk_tree_model_get(GTK_TREE_MODEL(self->tree_store),
				   &category_iter,
				   TEXT_COL, &category, -1);

		webdb_cache = irreco_data_get_webdb_cache(self->irreco_data,
							  FALSE);

		if(irreco_webdb_cache_get_models(webdb_cache, category,
						 manufacturer, &models)){

			IRRECO_STRING_TABLE_FOREACH_KEY(models, key)

				/* Add model item into manufacturer. */
				gtk_tree_store_append(self->tree_store,
				       &iter, self->loader_parent_iter);
				gtk_tree_store_set(self->tree_store,
						&iter,
						TEXT_COL, key,
						FLAG_COL, ROW_TYPE_MODEL,
						DATA_COL, NULL, -1);

				/* Add loading item into model. */
				gtk_tree_store_append(self->tree_store,
						      &iter_loading, &iter);
				gtk_tree_store_set(self->tree_store,
						   &iter_loading,
						   TEXT_COL, _("Loading ..."),
						   FLAG_COL, ROW_TYPE_LOADING,
						   DATA_COL, NULL, -1);

			IRRECO_STRING_TABLE_FOREACH_END
		} else {
			irreco_error_dlg(GTK_WINDOW(self),
					 irreco_webdb_cache_get_error(
					 webdb_cache));
		}
		g_free (category);
		g_free (manufacturer);

		/* Delete loading item from manufacturer. */
		gtk_tree_model_iter_nth_child(GTK_TREE_MODEL(self->tree_store),
					      &iter, self->loader_parent_iter,
	   				      0);
		if(irreco_webdb_dlg_row_get_type(self,
						&iter) == ROW_TYPE_LOADING) {
			gtk_tree_store_remove(self->tree_store, &iter);
		}

		irreco_webdb_dlg_set_banner(self, _("Loading ..."), 1);
		self->loader_state = LOADER_STATE_END;

		IRRECO_RETURN_BOOL(TRUE);
	}
	case LOADER_STATE_END:
		irreco_webdb_dlg_row_set_loaded(self, self->loader_parent_iter,
						TRUE);
		irreco_webdb_dlg_hide_banner(self);
		irreco_webdb_dlg_loader_stop(self);
	}

	IRRECO_RETURN_BOOL(FALSE);
}

/**
 * Web Database Config loader.
 *
 * @todo
 */
static gboolean irreco_webdb_dlg_loader_config(IrrecoWebdbDlg *self)
{
	IRRECO_ENTER

	switch (self->loader_state) {
	case LOADER_STATE_INIT:
		irreco_webdb_dlg_set_banner(self, _("Loading ..."), 0);
		self->loader_state = LOADER_STATE_LOOP;
		self->loader_pos = 0;
		IRRECO_RETURN_BOOL(TRUE);

	case LOADER_STATE_LOOP: {
		GtkTreeIter		iter;
		GtkTreeIter		manufacturer_iter;
		GtkTreeIter		category_iter;
		IrrecoWebdbConf		*config 	= NULL;
		IrrecoStringTable	*configs	= NULL;
		IrrecoWebdbCache	*webdb_cache	= NULL;
		const gchar		*config_id	= NULL;
		gpointer		id_data;
		gchar			*category;
		gchar			*manufacturer;
		gchar			*model;
		gint			config_count;
		gfloat			banner;

		/*Get manufacturer_iter and category_iter */

		gtk_tree_model_iter_parent(GTK_TREE_MODEL(self->tree_store),
					   &manufacturer_iter,
					   self->loader_parent_iter);

		gtk_tree_model_iter_parent(GTK_TREE_MODEL(self->tree_store),
					   &category_iter,
					   &manufacturer_iter);

		/*Get category, manufacturer and model*/

		gtk_tree_model_get(GTK_TREE_MODEL(self->tree_store),
				   &category_iter,
				   TEXT_COL, &category, -1);

		gtk_tree_model_get(GTK_TREE_MODEL(self->tree_store),
				   &manufacturer_iter,
				   TEXT_COL, &manufacturer, -1);

		gtk_tree_model_get(GTK_TREE_MODEL(self->tree_store),
				   self->loader_parent_iter,
				   TEXT_COL, &model, -1);


		webdb_cache = irreco_data_get_webdb_cache(self->irreco_data,
							  FALSE);

		if(irreco_webdb_cache_get_configs(webdb_cache,
			category, manufacturer, model, &configs)){
			gint id;
			irreco_string_table_index(configs, self->loader_pos,
						  &config_id, &id_data);
			id = atoi(config_id);
			if(irreco_webdb_cache_get_configuration(webdb_cache,
								id, &config)){

				/* Add config item into model. */
				gtk_tree_store_append(self->tree_store,
					&iter, self->loader_parent_iter);
				gtk_tree_store_set(self->tree_store,
						&iter,
						TEXT_COL, config->user->str,
						FLAG_COL, ROW_TYPE_CONFIG,
						DATA_COL, config_id, -1);
			} else {
			irreco_error_dlg(GTK_WINDOW(self),
					 irreco_webdb_cache_get_error(
					 webdb_cache));
			}

		} else {
			irreco_error_dlg(GTK_WINDOW(self),
					 irreco_webdb_cache_get_error(
					 webdb_cache));
		}
		g_free (category);
		g_free (manufacturer);
		g_free (model);


		/* Delete loading item from model. */
		gtk_tree_model_iter_nth_child(GTK_TREE_MODEL(self->tree_store),
					      &iter, self->loader_parent_iter,
	   				      0);
		if(irreco_webdb_dlg_row_get_type(self,
						&iter) == ROW_TYPE_LOADING) {
			gtk_tree_store_remove(self->tree_store, &iter);
		}

		config_count = irreco_string_table_lenght(configs);

		banner = ((float) self->loader_pos + 1) / (float) config_count;
		irreco_webdb_dlg_set_banner(self, _("Loading ..."), banner);

		if (self->loader_pos + 1 >= config_count) {
			self->loader_state = LOADER_STATE_END;
			IRRECO_RETURN_BOOL(TRUE);
		} else {
			self->loader_pos++;
			IRRECO_RETURN_BOOL(TRUE);
		}


		IRRECO_RETURN_BOOL(TRUE);
	}
	case LOADER_STATE_END:
		irreco_webdb_dlg_row_set_loaded(self, self->loader_parent_iter,
						TRUE);
		irreco_webdb_dlg_hide_banner(self);
		irreco_webdb_dlg_loader_stop(self);
	}

	IRRECO_RETURN_BOOL(FALSE);
}

/**
 * Web Database Configuration details loader.
 *
 * @todo
 */
static gboolean irreco_webdb_dlg_display_config_detail(IrrecoWebdbDlg *self,
						       GtkTreeIter * iter)
{
	IrrecoWebdbCache	*webdb_cache	= NULL;
	const gchar		*config_id	= NULL;
	GString			*download_count = g_string_new("");
	gint 			config_id_int;
	IRRECO_ENTER

	gtk_tree_model_get(GTK_TREE_MODEL(self->tree_store),
				iter, DATA_COL, &config_id, -1);

	config_id_int = atoi(config_id);

	webdb_cache = irreco_data_get_webdb_cache(self->irreco_data,
							FALSE);

	irreco_webdb_cache_get_configuration(webdb_cache,
						config_id_int, &self->config);

	gtk_label_set_text(GTK_LABEL(self->config_user),
			   self->config->user->str);
	gtk_label_set_text(GTK_LABEL(self->config_backend),
			   self->config->backend->str);
	gtk_label_set_text(GTK_LABEL(self->config_category),
			   self->config->category->str);
	gtk_label_set_text(GTK_LABEL(self->config_manuf),
			   self->config->manufacturer->str);
	gtk_label_set_text(GTK_LABEL(self->config_model),
			   self->config->model->str);
	gtk_label_set_text(GTK_LABEL(self->config_uploaded),
			   self->config->file_uploaded->str);

	g_string_printf(download_count, "%s times",
			self->config->file_download_count->str);
	gtk_label_set_text(GTK_LABEL(self->config_download_count),
			   download_count->str);
	g_string_free(download_count, TRUE);

	IRRECO_RETURN_BOOL(TRUE);
}


/**
 * Web Database File loader.
 *
 * @todo
 */
static gboolean irreco_webdb_dlg_load_file(IrrecoWebdbDlg *self)
{
	IrrecoWebdbCache		*webdb_cache	= NULL;
	IrrecoBackendInstance		*inst		= NULL;
	GString				*error_msg	= g_string_new("");
	IrrecoBackendManager		*manager;
	IrrecoBackendFileContainer	*file_container;
	GString				*file_data;
	gchar				*sha1		= NULL;
	gboolean			success		= TRUE;
	gboolean			new_instance	= FALSE;
	gint				instances;
	IRRECO_ENTER

	if(self->config == NULL) {
		g_string_printf(error_msg, "Select configuration first");
		goto end;
	}

	/* Get file_data */
	webdb_cache = irreco_data_get_webdb_cache(self->irreco_data, FALSE);
	if (!irreco_webdb_cache_get_file(webdb_cache,
					self->config->file_hash->str,
					self->config->file_name->str,
					&file_data)) {
		g_string_printf(error_msg, irreco_webdb_cache_get_error(
				webdb_cache));
		goto end;
	}

	sha1 = g_compute_checksum_for_string(G_CHECKSUM_SHA1,
					       file_data->str, -1);

	if (g_utf8_collate(self->config->file_hash->str, sha1) != 0) {
		g_string_printf(error_msg, "sha1 checksum failed.");
		goto end;

	}

	/* Search backend */
	manager = self->irreco_data->irreco_backend_manager;
	IRRECO_BACKEND_MANAGER_FOREACH_LIB(manager, lib)
		if (g_utf8_collate(lib->name, self->config->backend->str) != 0){
			continue;
		}
		if (!(lib->api->flags & IRRECO_BACKEND_EDITABLE_DEVICES)) {
			g_string_printf(error_msg,
			"\"%s\" backend is not editable...",
			self->config->backend->str);
			goto end;
		}
		else if (!(lib->api->flags &
			 IRRECO_BACKEND_CONFIGURATION_EXPORT)) {
			g_string_printf(error_msg,
			"\"%s\" backend doesn't support "
			"exporting its configuration...",
			self->config->backend->str);
			goto end;
		}
		/* Check if many instances */
		instances = 0;
		IRRECO_BACKEND_MANAGER_FOREACH_INSTANCE(manager, instance)
			irreco_backend_instance_get_description(instance);
			if (g_utf8_collate(instance->lib->name,
			    self->config->backend->str) == 0) {
				instances++;

				/*if there is only one instance, then use it */
				inst = instance;
			}
		IRRECO_STRING_TABLE_FOREACH_END
		IRRECO_DEBUG("INSTACES: %d\n",instances);

		/* Create new instance if it comes to the crunch */
		if (instances == 0 || lib->api->flags &
		    IRRECO_BACKEND_NEW_INST_ON_CONF_IMPORT) {
			inst = irreco_backend_manager_create_instance(
			manager, lib, NULL, NULL);
			new_instance = TRUE;
			break;
		}

		/* Select instace */
		if (instances > 1) {
			if (!irreco_show_select_instance_dlg(
			    self->irreco_data, GTK_WINDOW(self),
			    self->config->backend->str, &inst)) {
				g_string_printf(error_msg,
				"Operation aborted by user...");
				goto end;
			}
		}
		goto instance_ready;
	IRRECO_STRING_TABLE_FOREACH_END

	if (inst == NULL) {
		g_string_printf(error_msg, "\"%s\" backend is not installed...",
				self->config->backend->str);
		goto end;
	}

	irreco_backend_instance_configure(inst, GTK_WINDOW(self));
	irreco_backend_instance_save_to_conf(inst);
	irreco_config_save_backends(manager);

	instance_ready:
	file_container = irreco_backend_file_container_new();
	irreco_backend_file_container_set(file_container,
					  inst->lib->name,
					  self->config->category->str,
					  self->config->manufacturer->str,
					  self->config->model->str,
					  self->config->file_name->str,
					  file_data->str);

	if (irreco_backend_instance_check_conf(inst, file_container)) {
		GString *question = g_string_new("");
		g_string_printf(question, "\"%s\" backend already contains\n"
				"device \"%s\".\n"
				"Do you want to overwrite?",
				inst->lib->name,self->config->model->str);
		success = irreco_yes_no_dlg(GTK_WINDOW(self), question->str);
		g_string_free(question, TRUE);

		if(success == FALSE) {
			goto end;
		}
	}

	/* Send file_data for backend */
	if(irreco_backend_instance_import_conf(inst, file_container)) {
		irreco_info_dlg(GTK_WINDOW(self),
				"Configuration downloaded successfully!");
		irreco_backend_manager_get_devcmd_lists(
			       self->irreco_data->irreco_backend_manager);
	} else {
		g_string_printf(error_msg, "Backend error");

		if (new_instance) {
			irreco_backend_manager_destroy_instance(manager, inst);
		}
		goto end;
	}

	end:
	if(error_msg->len > 0) {
		irreco_error_dlg(GTK_WINDOW(self), error_msg->str);
		success = FALSE;
	}
	g_string_free(error_msg, TRUE);
	if (sha1 != NULL) g_free(sha1);
	IRRECO_RETURN_BOOL(success);
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Functions                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */

void irreco_webdb_dlg_set_irreco_data(IrrecoWebdbDlg *self,
				      IrrecoData *irreco_data)
{
	IRRECO_ENTER
	self->irreco_data = irreco_data;
	IRRECO_RETURN
}

void irreco_show_webdb_dlg(IrrecoData *irreco_data, GtkWindow *parent)
{
	IrrecoWebdbDlg	*self;
	gint		response;
	gboolean	loop = TRUE;
	IRRECO_ENTER

	self = IRRECO_WEBDB_DLG(irreco_webdb_dlg_new(irreco_data, parent));

	do {
		response = gtk_dialog_run(GTK_DIALOG(self));
		switch (response) {
		case IRRECO_DEVICE_REFRESH:
			IRRECO_DEBUG("IRRECO_DEVICE_REFRESH\n");
			gtk_tree_store_clear(self->tree_store);
			irreco_data_get_webdb_cache(self->irreco_data, TRUE);
			irreco_webdb_dlg_loader_start(self,
				G_SOURCEFUNC(irreco_webdb_dlg_loader_categ),
				NULL);
			break;

		case GTK_RESPONSE_DELETE_EVENT:
			IRRECO_DEBUG("GTK_RESPONSE_DELETE_EVENT\n");
			loop = FALSE;
			break;

		case GTK_RESPONSE_OK:
			IRRECO_DEBUG("GTK_RESPONSE_OK\n");
			loop = !irreco_webdb_dlg_load_file(self);
			break;

		}
	} while (loop);

	gtk_widget_destroy(GTK_WIDGET(self));
	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Events and Callbacks                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Events and Callbacks
 * @{
 */

static gboolean irreco_show_webdb_dlg_map_event(IrrecoWebdbDlg *self,
						GdkEvent *event,
						gpointer user_data)
{
	IRRECO_ENTER
	irreco_webdb_dlg_loader_start(
		self, G_SOURCEFUNC(irreco_webdb_dlg_loader_categ), NULL);
	IRRECO_RETURN_BOOL(FALSE);
}

static void irreco_show_webdb_dlg_destroy_event(IrrecoWebdbDlg *self,
						gpointer user_data)
{
	IRRECO_ENTER
	irreco_webdb_dlg_loader_stop(self);
	IRRECO_RETURN
}

static void irreco_show_webdb_dlg_row_activated_event(GtkTreeView *tree_view,
						      GtkTreePath *path,
						      GtkTreeViewColumn *column,
						      IrrecoWebdbDlg *self)
{
	IRRECO_ENTER

	if (gtk_tree_view_row_expanded(tree_view, path)) {
		gtk_tree_view_expand_row(tree_view, path, FALSE);
	} else {
		gtk_tree_view_collapse_row(tree_view, path);
	}
	IRRECO_RETURN
}

static void irreco_show_webdb_dlg_row_expanded_event(GtkTreeView *tree_view,
						     GtkTreeIter *iter,
						     GtkTreePath *path,
						     IrrecoWebdbDlg *self)
{
	IRRECO_ENTER

	irreco_webdb_dlg_clean_details(self);

	if (self->loader_func_id != 0) {
		gtk_tree_view_collapse_row(tree_view, path);
	}

	if (!irreco_webdb_dlg_row_is_loaded(self, iter)) {
		switch (irreco_webdb_dlg_row_get_type(self, iter)) {
		case ROW_TYPE_CATEGORY:
			irreco_webdb_dlg_loader_start(self,
				G_SOURCEFUNC(irreco_webdb_dlg_loader_manuf),
				iter);
			break;

		case ROW_TYPE_MANUFACTURER:
			irreco_webdb_dlg_loader_start(self,
				G_SOURCEFUNC(irreco_webdb_dlg_loader_model),
				iter);
			break;

		case ROW_TYPE_MODEL:
			irreco_webdb_dlg_loader_start(self,
				G_SOURCEFUNC(irreco_webdb_dlg_loader_config),
				iter);
			break;
		}
	}

	IRRECO_RETURN
}

static void irreco_show_webdb_dlg_row_collapsed_event(GtkTreeView *tree_view,
						     GtkTreeIter *iter,
						     GtkTreePath *path,
						     IrrecoWebdbDlg *self)
{
	IRRECO_ENTER

	irreco_webdb_dlg_clean_details(self);

	IRRECO_RETURN
}

static void irreco_show_webdb_dlg_row_selected_event(GtkTreeSelection *sel,
						     IrrecoWebdbDlg *self)
{
	GtkTreeIter iter;
	GtkTreeModel *model;

	IRRECO_ENTER

	if (gtk_tree_selection_get_selected (sel, &model, &iter))
	{
		switch (irreco_webdb_dlg_row_get_type(self, &iter)) {

		case ROW_TYPE_CONFIG:
			IRRECO_DEBUG("ROW_TYPE_CONFIG\n");
			irreco_webdb_dlg_display_config_detail(self, &iter);
			break;

		default:
			irreco_webdb_dlg_clean_details(self);
			break;
		}
	}

	IRRECO_RETURN
}

/** @} */
/** @} */


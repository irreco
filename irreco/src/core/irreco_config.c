/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi),
 *			Harri Vattulainen (t5vaha01@students.oamk.fi)
 *			Sami Mäki (kasmra@xob.kapsi.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_config.h"
#include "irreco_button.h"
#include "irreco_theme_button.h"

/**
 * @addtogroup IrrecoConfig
 * @ingroup Irreco
 *
 * Configuration saving and reading.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoConfig.
 */


#define IRRECO_CONFIG_GROUP_LEN	20
#define ENABLE_DEBUGGING 0

gchar* irreco_config_layout_file	= "layouts.conf";
gchar* irreco_config_active_layout_file	= "active-layout.conf";
gchar* irreco_config_backend_file	= "backends.conf";
gchar* irreco_config_cmd_chain_file	= "command-chains.conf";



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

void irreco_config_read_button_from_keyfile(IrrecoData * irreco_data,
					    IrrecoKeyFile * keyfile,
					    IrrecoStringTable *list);
void irreco_config_read_hardkey_from_keyfile(IrrecoData * irreco_data,
					     IrrecoKeyFile * keyfile,
					     IrrecoStringTable *list);
void irreco_config_read_layout_from_keyfile(IrrecoData * irreco_data,
					    IrrecoKeyFile * keyfile,
					    const gchar *filename);
void irreco_config_save_layout_to_keyfile(GKeyFile * keyfile,
					  IrrecoButtonLayout * layout,
					  gint * command_index,
					  gint * hardkey_id);
void irreco_config_save_hardkey_to_file(IrrecoHardkeyMap *hardkey_map,
					guint             keyval,
					IrrecoCmdChainId  chain_id,
					IrrecoCmdChain   *chain,
					gpointer          user_data);
void irreco_config_save_hardkey_to_keyfile(IrrecoHardkeyMap *hardkey_map,
					   guint             keyval,
					   IrrecoCmdChainId  chain_id,
					   IrrecoCmdChain   *chain,
					   gpointer          user_data);
void irreco_config_save_button_to_keyfile(GKeyFile * keyfile,
					  IrrecoButton * button,
					  gint button_index,
					  gint * command_index);
void irreco_config_save_cmd_chain_to_keyfile(GKeyFile * keyfile,
					     IrrecoButton * button,
					     gint * command_index);
void irreco_config_read_backend_from_keyfile(
					 IrrecoBackendManager * backend_manager,
					 IrrecoKeyFile * keyfile);
void irreco_config_read_layouts(IrrecoDirForeachData *dir_data);
static void irreco_config_cmd_chain_save(gpointer key,
					 gpointer value,
					 gpointer user_data);


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Layout reading.                                                            */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Layout reading
 * @{
 */

gboolean irreco_config_read_layouts_from_files(IrrecoData *irreco_data)
{
	IrrecoDirForeachData dir_data;

	IRRECO_ENTER

	dir_data.directory = irreco_get_config_dir("irreco");
	dir_data.filesuffix = ".conf";
	dir_data.user_data_1 = irreco_data;

	irreco_dir_foreach(&dir_data,
			   irreco_config_read_layouts);

	IRRECO_RETURN_BOOL(TRUE);
}

void irreco_config_read_layouts(IrrecoDirForeachData *dir_data)
{
	guint i;
	IrrecoKeyFile *keyfile = NULL;
	gchar *config_dir = NULL;
	gchar *config_file = NULL;
	gchar** groups = NULL;
	gsize group_count = 0;
	IrrecoData *irreco_data = dir_data->user_data_1;
	IrrecoStringTable *list = irreco_string_table_new(NULL, NULL);

	IRRECO_ENTER

	if(!g_str_has_prefix(dir_data->filename, "layout") &&
	   !g_str_has_prefix(dir_data->filename, "command-chains") /* ||
	   strcmp(dir_data->filename, "layouts.conf") == 0*/ ) {
		IRRECO_DEBUG("Not layout*.conf file\n");
		IRRECO_RETURN
	}

	config_dir = irreco_get_config_dir("irreco");
	/* g_sprintf(config_dir, "%s", dir_data->directory); */
	config_file = irreco_get_config_file("irreco", dir_data->filename);

	g_print("config_dir: %s\n", config_dir);
	g_print("config_file: %s\n", config_file);
	keyfile = irreco_keyfile_create(config_dir, config_file, NULL);
	if (config_dir == NULL || config_file == NULL || keyfile == NULL) {
		g_free(config_dir);
		g_free(config_file);
		irreco_keyfile_destroy(keyfile);
		IRRECO_RETURN
	}

	IRRECO_PRINTF("Reading layout configuration file \"%s\"\n",
		      config_file);
	groups = g_key_file_get_groups(keyfile->keyfile, &group_count);

	/* Read layout. */
	for (i = 0; i < group_count; i++) {
		gchar *group = groups[i];
		if (g_str_has_prefix(group, "layout")) {
			IRRECO_PRINTF("Reading layout group \"%s\"\n", group);
			irreco_keyfile_set_group(keyfile, group);
			irreco_config_read_layout_from_keyfile(irreco_data,
							       keyfile,
							dir_data->filename);
		}
	}

	/* Read Buttons. */
	for (i = 0; i < group_count; i++) {
		gchar *group = groups[i];
		if (g_str_has_prefix(group, "button")) {
			IRRECO_PRINTF("Reading button group \"%s\"\n", group);
			irreco_keyfile_set_group(keyfile, group);
			irreco_config_read_button_from_keyfile(irreco_data,
							       keyfile,
							       list);
		}
	}

	/* Read Hardkeys. */
	for (i = 0; i < group_count; i++) {
		gchar *group = groups[i];
		if (g_str_has_prefix(group, "hardkey")) {
			IRRECO_PRINTF("Reading hardkey group \"%s\"\n", group);
			irreco_keyfile_set_group(keyfile, group);
			irreco_config_read_hardkey_from_keyfile(irreco_data,
							        keyfile,
								list);
		}
	}

	/* Read command chains. */
	irreco_cmd_chain_manager_from_config(dir_data, list);

	g_free(config_dir);
	g_free(config_file);
	g_strfreev(groups);
	irreco_keyfile_destroy(keyfile);

#if ENABLE_DEBUGGING
	if(irreco_string_table_lenght(list) > 0)
	{
		IRRECO_STRING_TABLE_FOREACH(list, key, gchar *, listdata)
			g_print("old & new id: %s, %s\n", key, listdata);
		IRRECO_STRING_TABLE_FOREACH_END
	} else {
		g_print("No remapped commandchain ids\n");
	}
	IRRECO_PAUSE
#endif

	irreco_string_table_free(list);

	IRRECO_RETURN
}

void irreco_config_read_layout_from_keyfile(IrrecoData * irreco_data,
					    IrrecoKeyFile * keyfile,
					    const gchar *filename)
{
	gchar *name = NULL;
	IrrecoButtonLayout *layout = NULL;
	GdkColor color;
	gint type;
	gchar *image = NULL;
	IRRECO_ENTER

	/* Required fields. */
	if (!irreco_keyfile_get_str(keyfile, "name", &name)) IRRECO_RETURN
	layout = irreco_button_layout_create(NULL,
					     irreco_data->cmd_chain_manager);
	irreco_button_layout_set_name(layout, name);
	g_free(name);
	name = NULL;

	/* Don't save old type filename, so it get renamed */
	if(!strcmp(filename, "layouts.conf") == 0) {
		irreco_button_layout_set_filename(layout, filename);
	}

	if (irreco_keyfile_get_int(keyfile, "bg-type", &type)) {
		switch (type) {
		case IRRECO_BACKGROUND_DEFAULT:
		case IRRECO_BACKGROUND_COLOR:
		case IRRECO_BACKGROUND_IMAGE:
			irreco_button_layout_set_bg_type(layout, type);
			break;

		default:
			IRRECO_ERROR("Unknown background type \"%i\".\n",
				     type);
			break;
		}
	}

	if (irreco_keyfile_get_str(keyfile, "bg-image", &image)) {
		irreco_button_layout_set_bg_image(layout, image);
		g_free(image);
		image = NULL;
	}

	color.pixel = 0;
	if (irreco_keyfile_get_uint16(keyfile, "bg-color-red", &color.red) &&
	    irreco_keyfile_get_uint16(keyfile, "bg-color-green", &color.green)&&
	    irreco_keyfile_get_uint16(keyfile, "bg-color-blue", &color.blue)) {
		irreco_button_layout_set_bg_color(layout, &color);
	}

	if (!irreco_string_table_add(irreco_data->irreco_layout_array,
				    irreco_button_layout_get_name(layout),
				    layout)) {
		IRRECO_ERROR("Layout \"%s\" already exists.",
			     irreco_button_layout_get_name(layout));
		irreco_button_layout_destroy(layout);
	}

	IRRECO_RETURN
}

typedef struct _IrrecoConfigHardkeyId IrrecoConfigHardkeyId;
struct _IrrecoConfigHardkeyId {
	IrrecoStringTable *ids_in_use;
	gboolean command_clash;
	gint command_clash2;
	IrrecoCmdChainId chain_id;
};

void irreco_config_foreach_hardkey_id(gpointer key,
					gpointer value,
					gpointer user_data)
{
	IrrecoConfigHardkeyId *hardkeyid;
	gchar *id_as_gchar = g_malloc0(10);

	IRRECO_ENTER

	hardkeyid = (IrrecoConfigHardkeyId *) user_data;

	if(hardkeyid->chain_id == (gint) value) {
		hardkeyid->command_clash = TRUE;
		hardkeyid->command_clash2 = 1;
		g_print("clash asetettu true\n");
	}

	g_sprintf(id_as_gchar, "%d\n", (gint) value);
	irreco_string_table_add(hardkeyid->ids_in_use,
				id_as_gchar,
				NULL);

	g_free(id_as_gchar);

	IRRECO_RETURN
}

void irreco_config_read_button_from_keyfile(IrrecoData * irreco_data,
					    IrrecoKeyFile * keyfile,
					    IrrecoStringTable *list)
{
	gchar *layout_name = NULL;
	gchar *style_name = NULL;
	gchar *title = NULL;
	gchar *command = NULL;
	IrrecoCmdChainId chain_id;
	gint x;
	gint y;

	IrrecoButton *button = NULL;
	IrrecoButtonLayout *layout = NULL;
	IrrecoThemeButton *style = NULL;

	IrrecoConfigHardkeyId hardkeyid;

	IRRECO_ENTER

	hardkeyid.command_clash2 = 0;

	/* Required fields. */
	if (!irreco_keyfile_get_int(keyfile, "x", &x) ||
	    !irreco_keyfile_get_int(keyfile, "y", &y) ||
	    !irreco_keyfile_get_str(keyfile, "layout", &layout_name)) {
		IRRECO_RETURN
	}
	IRRECO_PRINTF("Position: x%i y%i\n", x, y);

	/* Find layout. */
	if (!irreco_string_table_get(irreco_data->irreco_layout_array,
				    layout_name, (gpointer *) &layout)) {
		IRRECO_ERROR("Button uses unknown layout \"%s\"\n",
			     layout_name);
		g_free(layout_name);
		IRRECO_RETURN
	}
	g_free(layout_name);
	IRRECO_PRINTF("Style: \"%s\"\n",
		      irreco_button_layout_get_name(layout));

	/* Find style. */
	irreco_keyfile_get_str(keyfile, "style", &style_name);
	if (style_name != NULL) {
		if (irreco_theme_manager_get_button_style(
						irreco_data->theme_manager,
						style_name, &style)) {
			IRRECO_PRINTF("Style: \"%s\"\n", style->name->str);
		} else {
			IRRECO_ERROR("Button uses unknown style \"%s\"\n",
				     style_name);
		}

		g_free(style_name);
	}

	/* Get title and command. */
	irreco_keyfile_get_str(keyfile, "title", &title);
	IRRECO_PRINTF("title: \"%s\"\n", title);

	button = irreco_button_create(layout, x, y, title, style,
				      irreco_data->cmd_chain_manager);

	if (irreco_keyfile_get_int(keyfile, "command-chain-id", &chain_id)) {
		gboolean command_clash = FALSE;
		IrrecoStringTable *ids_in_use = irreco_string_table_new(NULL,
									NULL);

		IRRECO_STRING_TABLE_FOREACH(irreco_data->irreco_layout_array, key,
					IrrecoButtonLayout *, layout)
		IRRECO_PTR_ARRAY_FORWARDS(layout->button_array,
						IrrecoButton *,
						irreco_button)
			gchar *id_as_gchar = g_malloc0(10);
			g_sprintf(id_as_gchar, "%d\n", irreco_button->cmd_chain_id);
			if(chain_id == irreco_button->cmd_chain_id) {
				g_print("cid: %d\n", irreco_button->cmd_chain_id);
				command_clash = TRUE;
			}
			irreco_string_table_add(ids_in_use,
						id_as_gchar,
						NULL);
			g_free(id_as_gchar);
		IRRECO_PTR_ARRAY_FORWARDS_END /* End of button loop */

		/* Handle hardkey commandchain ids */
			hardkeyid.ids_in_use = ids_in_use;
			hardkeyid.command_clash = command_clash;
			hardkeyid.chain_id = chain_id;
		g_hash_table_foreach(layout->hardkey_map->table,
				     irreco_config_foreach_hardkey_id,
				     &hardkeyid);

		IRRECO_STRING_TABLE_FOREACH_END /* End of layout loop */

#if ENABLE_DEBUGGING
		g_print("button clashes %d, %d\n", command_clash, hardkeyid.command_clash2);
#endif

		if(command_clash || hardkeyid.command_clash2 == 1) { /* commandchain id in use */
			const gchar *key;
			gint tmp;
			gchar *id_as_gchar = g_malloc0(10);
			gchar *new_id_as_gchar = g_malloc0(10);

			g_sprintf(id_as_gchar, "%d", chain_id);
			irreco_string_table_sort_123(ids_in_use);
			irreco_string_table_index(ids_in_use,
					irreco_string_table_lenght(ids_in_use)-1,
					&key,
					NULL);
			tmp = atoi(key)+1;
			g_sprintf(new_id_as_gchar, "%d", tmp);
			irreco_button_set_cmd_chain_id(button, atoi(key)+1);
			irreco_string_table_add(list, id_as_gchar, new_id_as_gchar);
			IRRECO_DEBUG("Clashing button ID\n");
			IRRECO_DEBUG("Trying to use ID: %s\n", id_as_gchar);
			IRRECO_DEBUG("New assosiated ID %s\n", new_id_as_gchar);
			g_free(id_as_gchar);
		} else { /* commandchain id not in use */
			irreco_button_set_cmd_chain_id(button, chain_id);
			IRRECO_DEBUG("Uniq button ID: %d\n", chain_id);
		}
	}
#if ENABLE_DEBUGGING
	IRRECO_PAUSE
#endif
	g_free(title);
	g_free(command);
	IRRECO_RETURN
}

void irreco_config_read_hardkey_from_keyfile(IrrecoData * irreco_data,
					     IrrecoKeyFile * keyfile,
					     IrrecoStringTable *list)
{
	IrrecoButtonLayout *layout = NULL;
	gchar *layout_name = NULL;
	IrrecoCmdChainId id;
	guint keyval;
	gchar *hardkey_str = NULL;

	gboolean command_clash = FALSE;
	IrrecoConfigHardkeyId hardkeyid;
	IrrecoStringTable *ids_in_use = irreco_string_table_new(NULL, NULL);

	IRRECO_ENTER

	hardkeyid.command_clash2 = 0;

	/* Required fields. */
	if (!irreco_keyfile_get_str(keyfile, "layout", &layout_name) ||
	    !irreco_keyfile_get_int(keyfile, "chain-id", &id) ||
	    !irreco_keyfile_get_uint(keyfile, "key-value", &keyval)) {
		g_free(layout_name);
		IRRECO_RETURN
	}

	/* Find layout. */
	if (!irreco_string_table_get(irreco_data->irreco_layout_array,
				     layout_name, (gpointer *) &layout)) {
		IRRECO_ERROR("Hardkey uses unknown layout \"%s\"\n",
			     layout_name);
		g_free(layout_name);
		IRRECO_RETURN
	}
	g_free(layout_name);

	/* Prevent use of clashing commandchain ids */
	IRRECO_STRING_TABLE_FOREACH(irreco_data->irreco_layout_array, key,
				IrrecoButtonLayout *, layout)
	IRRECO_PTR_ARRAY_FORWARDS(layout->button_array,
					IrrecoButton *,
					irreco_button)
		gchar *id_as_gchar = g_malloc0(10);
		g_sprintf(id_as_gchar, "%d\n", irreco_button->cmd_chain_id);
		if(id == irreco_button->cmd_chain_id) {
			g_print("cid: %d\n", irreco_button->cmd_chain_id);
			command_clash = TRUE;
		}
		irreco_string_table_add(ids_in_use,
					id_as_gchar,
					NULL);
		g_free(id_as_gchar);
	IRRECO_PTR_ARRAY_FORWARDS_END /* End of button loop */

	/* Handle hardkey commandchain ids */
		hardkeyid.ids_in_use = ids_in_use;
		hardkeyid.command_clash = command_clash;
		hardkeyid.chain_id = id;
	g_hash_table_foreach(layout->hardkey_map->table,
				irreco_config_foreach_hardkey_id,
				&hardkeyid);

	IRRECO_STRING_TABLE_FOREACH_END /* End of layout loop */

#if ENABLE_DEBUGGING
	g_print("hardkey clashes %d, %d\n", command_clash, hardkeyid.command_clash2);
#endif

	if(command_clash || hardkeyid.command_clash2 == 1) { /* commandchain id in use */
		const gchar *key;
		gint tmp;
		gchar *id_as_gchar = g_malloc0(10);
		gchar *new_id_as_gchar = g_malloc0(10);

		g_sprintf(id_as_gchar, "%d", id);
		irreco_string_table_sort_123(ids_in_use);
		irreco_string_table_index(ids_in_use,
				irreco_string_table_lenght(ids_in_use)-1,
				&key,
				NULL);
		tmp = atoi(key)+1;
		id = tmp;
		g_sprintf(new_id_as_gchar, "%d", tmp);
		irreco_string_table_add(list, id_as_gchar, new_id_as_gchar);
		IRRECO_DEBUG("Clashing hk ID\n");
		IRRECO_DEBUG("Trying to use ID: %s\n", id_as_gchar);
		IRRECO_DEBUG("New assosiated ID %s\n", new_id_as_gchar);

		g_free(id_as_gchar);
	} else { /* commandchain id not in use */
		IRRECO_DEBUG("Uniq hk ID: %d\n", id);
	}

#if ENABLE_DEBUGGING
	IRRECO_PAUSE
#endif


	hardkey_str = irreco_hardkey_to_str(keyval);
	IRRECO_PRINTF("Associating layout \"%s\" hardkey \"%s\" with "
		      "command chain \"%i\".\n",
		      irreco_button_layout_get_name(layout),
		      hardkey_str, id);
	g_free(hardkey_str);

	irreco_hardkey_map_assosiate_chain_with_id(layout->hardkey_map,
						   keyval, id);
	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Layout saving.                                                             */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Layout saving
 * @{
 */

typedef struct _IrrecoConfigCmdAndButton IrrecoConfigCmdAndButton;
struct _IrrecoConfigCmdAndButton {
	IrrecoButton *button;
	GKeyFile *keyfile;
};
typedef struct _IrrecoConfigSaveHardkeyData IrrecoConfigSaveHardkeyData;
struct _IrrecoConfigSaveHardkeyData {
	GKeyFile *keyfile;
	gint hardkey_index;
	IrrecoButtonLayout *layout;
	gint *hardkey_id;
};

/**
 * Save layout.
 *
 * Since GKeyFile does not allow two groups with the same name,
 * layout_index and button_index variables are used to make sure that each
 * button and layout group has unique name. The index is ignored when reading
 * the layout config.
 */
gboolean irreco_config_save_layouts(IrrecoData * irreco_data)
{
	gint command_index = 1;
	gint hardkey_id = 1;
	GKeyFile *keyfile;
	IrrecoStringTable *table;
	gboolean success;
	GList *list = NULL;
	IrrecoConfigCmdAndButton kf_and_btn;
	gchar *old_cmd_chain_conf_file;

	IRRECO_ENTER

	table = irreco_data->irreco_layout_array;
	IRRECO_STRING_TABLE_FOREACH(table, key, IrrecoButtonLayout *, layout)
		gchar *filename;
		keyfile = g_key_file_new();
		irreco_config_save_layout_to_keyfile(keyfile,
						     layout,
						     &command_index,
						     &hardkey_id);

		filename = irreco_create_uniq_layout_filename(
							layout->filename->str);
		g_string_assign(layout->filename, filename);

		list = g_list_prepend(list, g_strdup(filename));

		IRRECO_DEBUG("Layout %s in file: %s\n",
			     layout->name->str,
			     layout->filename->str);

		IRRECO_PTR_ARRAY_FORWARDS(layout->button_array,
					IrrecoButton *,
					irreco_button)
			kf_and_btn.button = irreco_button;
			kf_and_btn.keyfile = keyfile;
			g_hash_table_foreach(
					irreco_data->cmd_chain_manager->table,
					irreco_config_cmd_chain_save,
					&kf_and_btn);
		IRRECO_PTR_ARRAY_BACKWARDS_END

		success = irreco_gkeyfile_write_to_config_file(keyfile, "irreco",
							       filename);
		g_key_file_free(keyfile);
		g_free(filename);
	IRRECO_STRING_TABLE_FOREACH_END

	/* Remove unused layouts afterwards */
	irreco_remove_layouts_exept_glist(list);
	g_list_free(list);

	old_cmd_chain_conf_file = irreco_get_config_file("irreco",
			"command-chains.conf");

	/* Remove old command-chains.conf */
	if(irreco_file_exists(old_cmd_chain_conf_file)) {
		gchar *rm_cmd;
                rm_cmd = g_strconcat("rm ", old_cmd_chain_conf_file, NULL);
		system(rm_cmd);
		g_free(rm_cmd);
	}

	g_free(old_cmd_chain_conf_file);

	IRRECO_RETURN_BOOL(success);
}

/* Saves hardkey command to keyfile */
void irreco_config_save_hardkey_to_keyfile(IrrecoHardkeyMap *hardkey_map,
					   guint             keyval,
					   IrrecoCmdChainId  chain_id,
					   IrrecoCmdChain   *chain,
					   gpointer          user_data)
{
	IrrecoConfigSaveHardkeyData *hardkey_data;

	IRRECO_ENTER

	hardkey_data = (IrrecoConfigSaveHardkeyData*) user_data;

	/* Save command chain for matching hard key */
	if(chain_id == *hardkey_data->hardkey_id) {
		irreco_cmd_chain_to_config(chain, hardkey_data->keyfile);
	}

	IRRECO_RETURN
}

static void irreco_config_cmd_chain_save(gpointer key,
					 gpointer value,
					 gpointer user_data)
{
	IrrecoConfigCmdAndButton *iccab_struct =
			    (IrrecoConfigCmdAndButton *) user_data;
	GKeyFile *keyfile	  = iccab_struct->keyfile;
	IrrecoButton *button	  = iccab_struct->button;
	IrrecoCmdChain   *chain   = (IrrecoCmdChain *) value;
	IrrecoCmdChainId  id      = (IrrecoCmdChainId) key;
	IRRECO_ENTER

	if(button->cmd_chain_id == id) {
		irreco_cmd_chain_to_config(chain, keyfile);
	}

	IRRECO_RETURN
}

void irreco_config_save_layout_to_keyfile(GKeyFile * keyfile,
					  IrrecoButtonLayout * layout,
					  gint * command_index,
					  gint * hardkey_id)
{
	GdkColor *color;
	gint button_index = 1;
	gchar group[IRRECO_CONFIG_GROUP_LEN];
	IrrecoConfigSaveHardkeyData hardkey_data;
	IRRECO_ENTER

	/* Save layout. */
	g_assert(irreco_button_layout_get_name(layout) != NULL);
	g_snprintf(group, IRRECO_CONFIG_GROUP_LEN, "layout");
	irreco_gkeyfile_set_string(keyfile, group, "name",
				 irreco_button_layout_get_name(layout));
	g_key_file_set_integer(keyfile, group, "bg-type",
			       irreco_button_layout_get_bg_type(layout));
	irreco_gkeyfile_set_string(keyfile, group, "bg-image",
				 irreco_button_layout_get_bg_image(layout));
	color = irreco_button_layout_get_bg_color(layout);
	g_key_file_set_integer(keyfile, group, "bg-color-red", color->red);
	g_key_file_set_integer(keyfile, group, "bg-color-green", color->green);
	g_key_file_set_integer(keyfile, group, "bg-color-blue", color->blue);

	/* Save buttons. */
	irreco_button_layout_reindex(layout);
	IRRECO_PTR_ARRAY_FORWARDS(layout->button_array, IrrecoButton *, button)
		irreco_config_save_button_to_keyfile(keyfile,
						     button,
						     button_index,
						     command_index);
		button_index = button_index + 1;
	IRRECO_PTR_ARRAY_FORWARDS_END

	/* Save Hardkeys. */
	hardkey_data.keyfile = keyfile;
	hardkey_data.hardkey_index = 1;
	hardkey_data.layout = layout;
	hardkey_data.hardkey_id = hardkey_id;
	irreco_hardkey_map_chain_foreach(layout->hardkey_map,
					 irreco_config_save_hardkey_to_file,
					 &hardkey_data);
	IRRECO_RETURN
}

void irreco_config_save_hardkey_to_file(IrrecoHardkeyMap *hardkey_map,
					guint             keyval,
					IrrecoCmdChainId  chain_id,
					IrrecoCmdChain   *chain,
					gpointer          user_data)
{
	gchar group[IRRECO_CONFIG_GROUP_LEN];
	IrrecoConfigSaveHardkeyData *hardkey_data;

	IRRECO_ENTER

	hardkey_data = (IrrecoConfigSaveHardkeyData*) user_data;

	g_snprintf(group, IRRECO_CONFIG_GROUP_LEN, "hardkey-%i",
		   hardkey_data->hardkey_index);
	irreco_gkeyfile_set_string(hardkey_data->keyfile, group, "layout",
		irreco_button_layout_get_name(hardkey_data->layout));
	g_key_file_set_integer(hardkey_data->keyfile, group,
			       "chain-id", chain_id);
	irreco_gkeyfile_set_guint(hardkey_data->keyfile, group,
				  "key-value", keyval);

	*hardkey_data->hardkey_id = chain_id;

	irreco_hardkey_map_chain_foreach(hardkey_data->layout->hardkey_map,
					 irreco_config_save_hardkey_to_keyfile,
					 hardkey_data);

	hardkey_data->hardkey_index = hardkey_data->hardkey_index + 1;

	IRRECO_RETURN
}

void irreco_config_save_button_to_keyfile(GKeyFile * keyfile,
					  IrrecoButton * button,
					  gint button_index,
					  gint * command_index)
{
	gchar group[IRRECO_CONFIG_GROUP_LEN];
	IRRECO_ENTER

	g_snprintf(group, IRRECO_CONFIG_GROUP_LEN, "button-%i", button_index);
	irreco_gkeyfile_set_string(keyfile, group, "layout",
		irreco_button_layout_get_name(button->irreco_layout));
	irreco_gkeyfile_set_string(keyfile, group, "title", button->title);
	g_key_file_set_integer(keyfile, group, "index", button->index);
	g_key_file_set_integer(keyfile, group, "command-chain-id",
			       button->cmd_chain_id);
	g_key_file_set_integer(keyfile, group, "x", button->x);
	g_key_file_set_integer(keyfile, group, "y", button->y);

	/* It is valid for a button to have NULL style. */
	if (button->style != NULL) {
		irreco_gkeyfile_set_string(keyfile, group, "style",
				      button->style->style_name->str);
	}

	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Save / Read active layout.                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Save / Read active layout
 * @{
 */

gboolean irreco_config_save_active_layout(IrrecoButtonLayout * irreco_layout)
{
	GKeyFile *keyfile;
	gboolean success;
	IRRECO_ENTER

	keyfile = g_key_file_new();
	if (irreco_layout != NULL) {
		irreco_gkeyfile_set_string(keyfile, "active-layout", "name",
			irreco_button_layout_get_name(irreco_layout));
	}
	success = irreco_gkeyfile_write_to_config_file(keyfile, "irreco",
					      irreco_config_active_layout_file);
	g_key_file_free(keyfile);
	IRRECO_RETURN_BOOL(success);
}

IrrecoButtonLayout *irreco_config_read_active_layout(IrrecoData * irreco_data)
{
	IrrecoKeyFile *keyfile = NULL;
	gchar *config_dir = NULL;
	gchar *config_file = NULL;
	gchar *name = NULL;
	IrrecoButtonLayout *irreco_layout = NULL;
	IRRECO_ENTER

	/* Attempt to read layout name, and find layout with the same name. */
	config_dir = irreco_get_config_dir("irreco");
	config_file = irreco_get_config_file("irreco",
					     irreco_config_active_layout_file);
	keyfile = irreco_keyfile_create(config_dir, config_file, NULL);
	if (config_dir != NULL && config_file != NULL && keyfile != NULL
	    && irreco_keyfile_set_group(keyfile, "active-layout")
	    && irreco_keyfile_get_str(keyfile, "name", &name)) {

		IRRECO_DEBUG("Read active layout name \"%s\" from \"%s\".\n",
			  name, irreco_config_active_layout_file);

		if (irreco_string_table_get(irreco_data->irreco_layout_array,
					    name, (gpointer *) &irreco_layout)){
			IRRECO_DEBUG("Found matching layout.\n");
		} else {
			IRRECO_DEBUG("No matching layout found.\n");
		}
	}

	irreco_keyfile_destroy(keyfile);
	g_free(name);
	g_free(config_dir);
	g_free(config_file);

	/* If we could not find correct active layout,
	   attempt to get some layout.*/
	if (irreco_layout == NULL) {
		irreco_string_table_index(irreco_data->irreco_layout_array, 0,
					  NULL, (gpointer *) &irreco_layout);
	}
	IRRECO_RETURN_PTR(irreco_layout);
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Save / Read backend configuration.                                         */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Save / Read backend configuration
 * @{
 */

gboolean irreco_config_save_backends(IrrecoBackendManager * backend_manager)
{
	gint i = 0;
	GKeyFile *keyfile;
	gboolean success;
	gchar group[IRRECO_CONFIG_GROUP_LEN];
	IRRECO_ENTER

	keyfile = g_key_file_new();
	IRRECO_BACKEND_MANAGER_FOREACH_INSTANCE(backend_manager, instance)
		g_snprintf(group, IRRECO_CONFIG_GROUP_LEN, "instance-%i", ++i);
		irreco_gkeyfile_set_string(keyfile, group, "library",
					 instance->lib->filename);
		irreco_gkeyfile_set_string(keyfile, group, "config",
					 instance->config);
		irreco_gkeyfile_set_string(keyfile, group, "name",
			irreco_backend_instance_get_name(instance));
	IRRECO_BACKEND_MANAGER_FOREACH_END

	success = irreco_gkeyfile_write_to_config_file(keyfile, "irreco",
						    irreco_config_backend_file);
	g_key_file_free(keyfile);
	IRRECO_RETURN_BOOL(success);
}

gboolean irreco_config_read_backends(IrrecoBackendManager * manager)
{
	guint i;
	IrrecoKeyFile *keyfile = NULL;
	gchar *config_dir = NULL;
	gchar *config_file = NULL;
	gchar** groups = NULL;
	gsize group_count = 0;
	IRRECO_ENTER

	config_dir = irreco_get_config_dir("irreco");
	config_file = irreco_get_config_file("irreco",
					     irreco_config_backend_file);
	keyfile = irreco_keyfile_create(config_dir, config_file, NULL);
	if (config_dir == NULL || config_file == NULL || keyfile == NULL) {
		g_free(config_dir);
		g_free(config_file);
		irreco_keyfile_destroy(keyfile);
		IRRECO_RETURN_BOOL(FALSE);
	}

	IRRECO_PRINTF("Reading backend configuration file \"%s\"\n",
		      config_file);
	groups = g_key_file_get_groups(keyfile->keyfile, &group_count);

	/* Read instances. */
	for (i = 0; i < group_count; i++) {
		gchar *group = groups[i];
		if (g_str_has_prefix(group, "instance")) {
			IRRECO_PRINTF("Reading instance group \"%s\"\n", group);
			irreco_keyfile_set_group(keyfile, group);
			irreco_config_read_backend_from_keyfile(manager,
								keyfile);
		}
	}

	g_strfreev(groups);
	irreco_keyfile_destroy(keyfile);
	g_free(config_dir);
	g_free(config_file);
	IRRECO_RETURN_BOOL(TRUE);
}

void irreco_config_read_backend_from_keyfile(IrrecoBackendManager * manager,
					     IrrecoKeyFile * keyfile)
{
	gchar *library = NULL;
	gchar *config = NULL;
	gchar *name = NULL;
	IrrecoBackendLib *lib;
	IRRECO_ENTER

	/* Read variables. */
	if (!irreco_keyfile_get_str(keyfile, "library", &library) ||
	    !irreco_keyfile_get_str(keyfile, "config", &config) ||
	    !irreco_keyfile_get_str(keyfile, "name", &name)) {
		g_free(library);
		g_free(config);
		g_free(name);
		IRRECO_RETURN
	}

	/* Find library. */
	if (!irreco_backend_manager_find_lib(manager, library, &lib)) {
		IRRECO_ERROR("Cannot find backend library \"%s\"\n", library);
		g_free(library);
		g_free(config);
		g_free(name);
		IRRECO_RETURN
	}
	g_free(library);

	/* Create instance. */
	irreco_backend_manager_create_instance(manager, lib, name, config);
	g_free(name);
	g_free(config);

	IRRECO_RETURN
}


/** @} */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Misc layout functions						      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Misc layout functions
 * @{
 */

/**
 * Get list of themes used by layout
 *
 * @param data IrrecoData
 * @param layoutname name of layout
 * @param list StringTable that'll contain theme names (key) and versions (data)
 * @returns TRUE in case of success, FALSE otherwise
 */
gboolean irreco_layout_get_themes(IrrecoData *data,
				  gchar *layoutname,
				  IrrecoStringTable **list)
{
	IrrecoStringTable	*themelist = NULL;
	gchar			*bgpath = NULL;

	IRRECO_ENTER

	/* List of theme names used by layout, stored as keys */
	*list = irreco_string_table_new(NULL, NULL);
	/* List of all themes */
	themelist = irreco_theme_manager_get_themes(data->theme_manager);

	if(irreco_string_table_lenght(data->irreco_layout_array) == 0) {
		IRRECO_RETURN_BOOL(FALSE);
	}

	/* Loop through layout buttons and store used themes to list */
	IRRECO_STRING_TABLE_FOREACH(data->irreco_layout_array,
				    key,
				    IrrecoButtonLayout *,
				    layout)
		/* Skip layouts with wrong name */
		if(g_str_equal(layoutname, layout->name->str)) {
			if(layout->background_type == 2) {
				bgpath = g_strdup(layout->background_image->str);
			}
			IRRECO_PTR_ARRAY_FORWARDS(layout->button_array,
						IrrecoButton *,
						irreco_button)
				/* Block buttons without theme */
				if(irreco_button->style != NULL) {
				gboolean match = FALSE;
				IRRECO_STRING_TABLE_FOREACH_KEY(*list, themename)
					if(g_str_equal(
					irreco_button->style->theme_name->str,
					themename)) {
						match = TRUE;
					}
				IRRECO_STRING_TABLE_FOREACH_END
				if(!match) {
					irreco_string_table_add(*list,
					irreco_button->style->theme_name->str,
								NULL);
				}
				}
			IRRECO_PTR_ARRAY_BACKWARDS_END
		}
	IRRECO_STRING_TABLE_FOREACH_END

	/* Loop through themes and get versions of wanted themes */
	IRRECO_STRING_TABLE_FOREACH(themelist, key, IrrecoTheme*, theme)
		IRRECO_STRING_TABLE_FOREACH(theme->backgrounds,
					    bgkey,
					    IrrecoThemeBg*,
					    bgtheme)
			if(bgpath != NULL)
			if(g_str_equal(bgtheme->image_path->str, bgpath)) {
				gboolean match = FALSE;
				/* Matching theme found for bg */
				IRRECO_STRING_TABLE_FOREACH_KEY(*list, themename)
					if(g_str_equal(theme->name->str,
						       themename)) {
						match = TRUE;
					}
				IRRECO_STRING_TABLE_FOREACH_END
				if(!match) {
					irreco_string_table_add(*list,
								theme->name->str,
								NULL);
				}
			}
		IRRECO_STRING_TABLE_FOREACH_END
		IRRECO_STRING_TABLE_FOREACH_KEY(*list, themename)
				if(g_str_equal(themename, theme->name->str)) {
				irreco_string_table_change_data(*list, themename, theme->version->str);
				}
		IRRECO_STRING_TABLE_FOREACH_END
	IRRECO_STRING_TABLE_FOREACH_END

	g_free(bgpath);

	IRRECO_RETURN_BOOL(TRUE);
}

typedef struct _IrrecoConfigHardkeyInstance IrrecoConfigHardkeyInstance;
struct _IrrecoConfigHardkeyInstance {
	IrrecoStringTable	*list;
	IrrecoData		*data;
};

/* Private func for irreco_layout_get_backends that handles hardkeys */
static void irreco_layout_get_backends_of_hardkeys(gpointer key,
						   gpointer value,
						   gpointer user_data)
{
	IrrecoConfigHardkeyInstance *data;
	IrrecoCmdChain* cmd_chain;

	IRRECO_ENTER

	data = (IrrecoConfigHardkeyInstance *) user_data;

	/* Get commandchain */
	cmd_chain = irreco_cmd_chain_manager_get_chain(
			data->data->cmd_chain_manager,
			(gint)value);
	/* Loop through commandchain */
	IRRECO_CMD_CHAIN_FOREACH(cmd_chain, cmd)
		gboolean match = FALSE;
		if(cmd->type == IRRECO_COMMAND_BACKEND) {
			/* Check list doesn't already contain backend name */
			IRRECO_STRING_TABLE_FOREACH_KEY(data->list, device_name)
				if(g_str_equal(cmd->backend.device_name,
						device_name)) {
					match = TRUE;
				}
			IRRECO_STRING_TABLE_FOREACH_END
			/* Add backend instance name to list */
			if(!match) {
			irreco_string_table_add(data->list,
						cmd->backend.device_name,
						cmd->backend.instance);
			}
		}
	IRRECO_CMD_CHAIN_FOREACH_END

	IRRECO_RETURN
}

/**
 * Get list of backends used by layout
 *
 * @param data IrrecoData
 * @param layoutname gchar name of layout
 * @param list StringTable that'll contain backend names as key
 * @returns TRUE in case of success, FALSE otherwise
 * TODO When changing to less insane indenting style, fix indentation
 */
gboolean irreco_layout_get_backends(IrrecoData *data,
				    gchar *layoutname,
				    IrrecoStringTable **list)
{
	IrrecoCmdChain* cmd_chain;
	IrrecoConfigHardkeyInstance user_data;

	IRRECO_ENTER

	*list = irreco_string_table_new(NULL, NULL);

	user_data.list = *list;
	user_data.data = data;

	/* Loop through layouts */
	IRRECO_STRING_TABLE_FOREACH(data->irreco_layout_array, key,
				    IrrecoButtonLayout *, layout)
	if(g_str_equal(layout->name->str, layoutname)) {
	/* Loop through buttons */
	IRRECO_PTR_ARRAY_FORWARDS(layout->button_array,
					IrrecoButton *,
					irreco_button)
	/* Get commandchain */
	cmd_chain = irreco_cmd_chain_manager_get_chain(
			data->cmd_chain_manager,
			irreco_button->cmd_chain_id);
	/* Loop through commandchain */
	IRRECO_CMD_CHAIN_FOREACH(cmd_chain, cmd)
		gboolean match = FALSE;
		if(cmd->type == IRRECO_COMMAND_BACKEND) {
			IRRECO_DEBUG("%s\n",cmd->backend.device_name);
			/* Check list doesn't already contain backend name */
			IRRECO_STRING_TABLE_FOREACH_KEY(*list, device_name)
				if(g_str_equal(cmd->backend.device_name,
						device_name)) {
					match = TRUE;
				}
			IRRECO_STRING_TABLE_FOREACH_END
			/* Add backend instance name to list */
			if(!match) {
			irreco_string_table_add(*list,
						cmd->backend.device_name,
						cmd->backend.instance);
			}
		}
	IRRECO_CMD_CHAIN_FOREACH_END /* End of commandchain loop */
	IRRECO_PTR_ARRAY_FORWARDS_END /* End of button loop */

	/* Get cmd_chain_id's from hardkeys */
	g_hash_table_foreach(layout->hardkey_map->table,
			     irreco_layout_get_backends_of_hardkeys,
			     &user_data);

	}
	IRRECO_STRING_TABLE_FOREACH_END /* End of layout loop */

	IRRECO_RETURN_BOOL(TRUE);
}

/** @} */
/** @} */


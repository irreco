/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoCmdChainSetupDlg
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoCmdChainSetupDlg.
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_CMD_CHAIN_SETUP_DLG_H_TYPEDEF__
#define __IRRECO_CMD_CHAIN_SETUP_DLG_H_TYPEDEF__

#define IRRECO_TYPE_CMD_CHAIN_SETUP_DLG irreco_cmd_chain_setup_dlg_get_type()

#define IRRECO_CMD_CHAIN_SETUP_DLG(obj) \
	(G_TYPE_CHECK_INSTANCE_CAST ((obj), \
	IRRECO_TYPE_CMD_CHAIN_SETUP_DLG, IrrecoCmdChainSetupDlg))

#define IRRECO_CMD_CHAIN_SETUP_DLG_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_CAST ((klass), \
	IRRECO_TYPE_CMD_CHAIN_SETUP_DLG, IrrecoCmdChainSetupDlgClass))

#define IRRECO_IS_CMD_CHAIN_SETUP_DLG(obj) \
	(G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
	IRRECO_TYPE_CMD_CHAIN_SETUP_DLG))

#define IRRECO_IS_CMD_CHAIN_SETUP_DLG_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_TYPE ((klass), \
	IRRECO_TYPE_CMD_CHAIN_SETUP_DLG))

#define IRRECO_CMD_CHAIN_SETUP_DLG_GET_CLASS(obj) \
	(G_TYPE_INSTANCE_GET_CLASS ((obj), \
	IRRECO_TYPE_CMD_CHAIN_SETUP_DLG, IrrecoCmdChainSetupDlgClass))

typedef struct _IrrecoCmdChainSetupDlg IrrecoCmdChainSetupDlg ;
typedef struct _IrrecoCmdChainSetupDlgClass IrrecoCmdChainSetupDlgClass;

#endif /* __IRRECO_CMD_CHAIN_SETUP_DLG_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_CMD_CHAIN_SETUP_DLG_H__
#define __IRRECO_CMD_CHAIN_SETUP_DLG_H__
#include "irreco.h"
#include "irreco_dlg.h"
#include "irreco_cmd_chain.h"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

struct _IrrecoCmdChainSetupDlg{
	GtkDialog 	 parent;
	GtkWidget	*combo_box;
	IrrecoCmdChain	*cmd_chain;
	GtkWidget       *progbar_check;
};

struct _IrrecoCmdChainSetupDlgClass{
	IrrecoDlgClass parent_class;
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
GType 		irreco_cmd_chain_setup_dlg_get_type(void);
GtkWidget*	irreco_cmd_chain_setup_dlg_new(	GtkWindow *parent,
						IrrecoCmdChain *cmd_chain);
void 		irreco_cmd_chain_setup_dlg_set_chain(
						IrrecoCmdChainSetupDlg *self,
						IrrecoCmdChain *cmd_chain);


#endif /* __IRRECO_CMD_CHAIN_SETUP_DLG_H__ */

/** @} */


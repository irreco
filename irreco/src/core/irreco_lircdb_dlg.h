/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007,2008  Arto Karppinen (arto.karppinen@iki.fi),
 * Joni Kokko (t5kojo01@students.oamk.fi)
 * Sami Mäki (kasmra@xob.kapsi.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoLircdbDlg
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoLircdbDlg.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_LIRCDB_DLG_H_TYPEDEF__
#define __IRRECO_LIRCDB_DLG_H_TYPEDEF__

#define IRRECO_TYPE_LIRCDB_DLG irreco_lircdb_dlg_get_type()
#define IRRECO_LIRCDB_DLG(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), IRRECO_TYPE_LIRCDB_DLG, IrrecoLircdbDlg))
#define IRRECO_LIRCDB_DLG_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), IRRECO_TYPE_LIRCDB_DLG, IrrecoLircdbDlgClass))
#define IRRECO_IS_LIRCDB_DLG(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), IRRECO_TYPE_LIRCDB_DLG))
#define IRRECO_IS_LIRCDB_DLG_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), IRRECO_TYPE_LIRCDB_DLG))
#define IRRECO_LIRCDB_DLG_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), IRRECO_TYPE_LIRCDB_DLG, IrrecoLircdbDlgClass))

typedef struct _IrrecoLircdbDlg IrrecoLircdbDlg;
typedef struct _IrrecoLircdbDlgClass IrrecoLircdbDlgClass;

#endif /* __IRRECO_LIRCDB_DLG_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_LIRCDB_DLG_H__
#define __IRRECO_LIRCDB_DLG_H__
#include "irreco.h"
#include "irreco_dlg.h"
#include "irreco_data.h"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

struct _IrrecoLircdbDlg {
	IrrecoDlg 		 parent;
	IrrecoData 		*irreco_data;
	GtkTreeStore		*tree_store;
	GtkTreeView		*tree_view;
	GtkWidget		*banner;
	GtkWidget		*hbox;
	GtkWidget		*download_button;
	IrrecoWebdbConf		*config;

	gint			 loader_func_id;
	gint			 loader_state;
	gint			 loader_pos;

	/** The loaded items are childern of this iter.*/
	GtkTreeIter		*loader_parent_iter;
	gchar			*dir;
	gchar			*manufacturer;
	gchar			*model;
	gchar			*file_path;
};

struct _IrrecoLircdbDlgClass {
	IrrecoDlgClass parent_class;
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
GType irreco_lircdb_dlg_get_type(void);
GtkWidget *irreco_lircdb_dlg_new(IrrecoData *irreco_data,
				GtkWindow *parent);
void irreco_lircdb_dlg_set_irreco_data(IrrecoLircdbDlg *self,
				      IrrecoData *irreco_data);
void irreco_show_lircdb_dlg(IrrecoData *irreco_data, GtkWindow *parent);


#endif /* __IRRECO_LIRCDB_DLG_H__ */

/** @} */

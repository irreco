/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * irreco - Ir Remote Control
 * Copyright (C) 2008 Pekka Gehör (pegu6@msn.com)
 *
 * irreco_theme_creator_dlg.c is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * irreco_theme_creator_dlg.c is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "irreco_theme_creator_dlg.h"
#include "irreco_button_browser_dlg.h"
#include "irreco_theme_creator_backgrounds.h"
#include "irreco_theme_creator_buttons.h"
#include "irreco_background_creator_dlg.h"
#include "irreco_button_creator_dlg.h"
#include <hildon/hildon-banner.h>
#include <hildon/hildon-color-button.h>
#include <hildon/hildon-file-chooser-dialog.h>
#include "irreco_theme_upload_dlg.h"


/**
 * @addtogroup IrrecoThemeCreatorDlg
 * @ingroup Irreco
 *
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoThemeCreatorDlg.
 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

#define IRRECO_BUTTON_PREVIEW_WIDHT		(IRRECO_SCREEN_WIDTH / 6)
#define IRRECO_BUTTON_PREVIEW_HEIGHT	(IRRECO_SCREEN_HEIGHT / 6)

enum
{
	DATA_COL,
	TEXT_COL,
	PIXBUF_COL,
	N_COLUMNS
};
/** Notebook table */
enum
{
	ABOUT,
	BUTTONS,
	BACKGROUNDS,
	N_TABLES
};
/** Loader states. */
enum
{
	LOADER_STATE_INIT,
	LOADER_STATE_THEME,
	LOADER_STATE_BUTTONS,
	LOADER_STATE_BACKGROUNDS,
	LOADER_STATE_END
};

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

static void _loader_start(IrrecoThemeCreatorDlg *self, GSourceFunc function);
static void _loader_stop(IrrecoThemeCreatorDlg *self);
static void irreco_theme_creator_dlg_preview_image(GtkWidget *widget,
						   GdkEventButton *event,
	 					   IrrecoThemeCreatorDlg *self);

static void
irreco_theme_creator_dlg_notebook_changed(GtkNotebook *notebook,
					  GtkNotebookPage *page,
       					  guint page_num,
	      				  IrrecoThemeCreatorDlg *self);

static gboolean
irreco_theme_creator_dlg_display_theme_detail(IrrecoThemeCreatorDlg *self,
					      gint page);
static void irreco_theme_creator_dlg_new_bg_button(GtkButton *button,
					      	IrrecoThemeCreatorDlg *self);
static void irreco_theme_creator_dlg_edit_bg_button(GtkButton *button,
					      	IrrecoThemeCreatorDlg *self);
static void _set_preview(IrrecoThemeCreatorDlg *self);

static void irreco_theme_creator_dlg_delete_bg_button(GtkButton *button,
					      	IrrecoThemeCreatorDlg *self);

void _set_theme_details(IrrecoThemeCreatorDlg *self, IrrecoTheme *irreco_theme);
void _create_bg_and_button_widgets(IrrecoThemeCreatorDlg *self);
void _set_edited_theme_details(IrrecoThemeCreatorDlg *self);
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

G_DEFINE_TYPE(IrrecoThemeCreatorDlg, irreco_theme_creator_dlg,
			  IRRECO_TYPE_INTERNAL_DLG)

static void irreco_theme_creator_dlg_constructed(GObject *object)
{
	/* TODO: Add initialization code here */
	IrrecoThemeCreatorDlg *self;
	/*About widgets*/
	GtkWidget	*scrolled_table;
	GtkWidget	*table_about;
	GtkWidget	*label_author;
	GtkWidget	*label_name;
	GtkWidget	*frame_comments;
	GtkWidget	*scrolled_comments;

	GtkWidget	*preview_button_frame;



	IRRECO_ENTER

	G_OBJECT_CLASS(irreco_theme_creator_dlg_parent_class)->constructed(object);
	self = IRRECO_THEME_CREATOR_DLG(object);
	/* Construct dialog. */
	gtk_window_set_title(GTK_WINDOW(self), _("Create a Theme"));
	gtk_window_set_modal(GTK_WINDOW(self), TRUE);
	gtk_window_set_destroy_with_parent(GTK_WINDOW(self), TRUE);
	gtk_dialog_set_has_separator(GTK_DIALOG(self), FALSE);

	/*Buttons*/
	self->save_button = gtk_dialog_add_button (GTK_DIALOG(self),
                                       _("Save"),
                                       GTK_RESPONSE_OK);
	self->add_button = gtk_dialog_add_button (GTK_DIALOG(self),
                                       _("Add"), GTK_RESPONSE_NONE);
	self->edit_button = gtk_dialog_add_button (GTK_DIALOG(self),
                                     _("Edit"),
                                       GTK_RESPONSE_NONE);
	self->delete_button = gtk_dialog_add_button (GTK_DIALOG(self),
                                       _("Delete"),
                                       GTK_RESPONSE_NONE);

	/* Create widgets. */
	self->notebook = gtk_notebook_new();

	/* ABOUT*/

	scrolled_table = gtk_scrolled_window_new(NULL, NULL);
	table_about = gtk_table_new(7, 9, FALSE);
	label_author = gtk_label_new(_("Author:"));
	label_name = gtk_label_new(_("Name:  "));
	self->entry_author =  gtk_entry_new();
	self->entry_name = gtk_entry_new();
	frame_comments = gtk_frame_new("");
	scrolled_comments = gtk_scrolled_window_new(NULL, NULL);
	self->textview_comments =  gtk_text_view_new();
	preview_button_frame = gtk_frame_new("");
	self->preview_event_box = gtk_event_box_new();
	self->preview_image = gtk_image_new();
	gtk_image_set_from_file(GTK_IMAGE(self->preview_image), NULL);
	self->hbox_backgrounds = gtk_hbox_new(FALSE, 2);
	self->hbox_buttons = gtk_hbox_new(FALSE, 2);

	/* equal to the text of the left-side */
	gtk_misc_set_alignment(GTK_MISC(label_name), 0, 0.5);
	gtk_misc_set_alignment(GTK_MISC(label_author), 0, 0.5);


	/* Set frame text bold */
	gtk_frame_set_label_widget(GTK_FRAME(frame_comments),
				   irreco_gtk_label_bold(
				   "Comments", 0, 0, 0, 0, 0, 0));
	gtk_frame_set_label_widget(GTK_FRAME(preview_button_frame),
				   irreco_gtk_label_bold(
				   "Preview button", 0, 0, 0, 0, 0, 0));

	/* Set table on the scrolled */
	/*gtk_container_add(GTK_CONTAINER(scrolled_table),
			  table_about);*/

	gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(
					      scrolled_table),
	   				      GTK_WIDGET(self->notebook));

	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolled_table),
				       GTK_POLICY_NEVER,
	   			       GTK_POLICY_AUTOMATIC);

	/* Create Notebook tabs. */

	gtk_notebook_append_page(GTK_NOTEBOOK(self->notebook),
				 irreco_gtk_align(GTK_WIDGET(table_about),
				 0, 0, 1, 1, 8, 8, 8, 8),
     				 gtk_label_new("About"));
	gtk_notebook_append_page(GTK_NOTEBOOK(self->notebook),
				 irreco_gtk_align(GTK_WIDGET(self->hbox_buttons),
				 0, 0, 1, 1, 8, 8, 8, 8),
     				 gtk_label_new("Buttons"));
	gtk_notebook_append_page(GTK_NOTEBOOK(self->notebook),
				 irreco_gtk_align(GTK_WIDGET(
				 self->hbox_backgrounds),
     				 0, 0, 1, 1, 8, 8, 8, 8),
	  			 gtk_label_new("Backgrounds"));

	/*gtk_box_pack_start_defaults(GTK_BOX(GTK_DIALOG(self)->vbox),
				    scrolled_table);*/

	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(self)->vbox),
			   scrolled_table,
			   TRUE,
      			   TRUE,
	    		   0);

	/* Set widgets on the table_about */

	gtk_table_set_row_spacings(GTK_TABLE(table_about), 6);
	gtk_table_set_col_spacings(GTK_TABLE(table_about), 6);

	gtk_table_attach_defaults(GTK_TABLE(table_about),
				  label_author, 0, 2, 0, 1);
	gtk_table_attach_defaults(GTK_TABLE(table_about),
				  label_name, 0, 2, 1, 2);
	gtk_table_attach_defaults(GTK_TABLE(table_about),
				  self->entry_author, 2, 9, 0, 1);
	gtk_table_attach_defaults(GTK_TABLE(table_about),
				  self->entry_name, 2, 9, 1, 2);
	/*gtk_table_attach_defaults(GTK_TABLE(table_about),
				  frame_comments, 0, 6, 2, 7);
	gtk_table_attach_defaults(GTK_TABLE(table_about),
				  preview_button_frame, 6, 9, 2, 7);*/
	gtk_table_attach(GTK_TABLE(table_about), frame_comments, 0, 6, 2, 7,
			 GTK_FILL | GTK_EXPAND, GTK_FILL | GTK_EXPAND, 0, 0);
	gtk_table_attach(GTK_TABLE(table_about), preview_button_frame, 6, 9, 2, 7,
			 GTK_FILL | GTK_EXPAND, GTK_FILL | GTK_EXPAND, 0, 0);

	/*ABOUT*/
	/* set preview image*/
	gtk_container_add(GTK_CONTAINER(preview_button_frame),
			  self->preview_event_box);
	gtk_container_add(GTK_CONTAINER(self->preview_event_box),
			  self->preview_image);

	/* TEXTVIEW */
	/* set max size of frame */
	gtk_widget_set(frame_comments,
                        "width-request",
			370,
   			NULL);
	gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(self->textview_comments),
				    GTK_WRAP_WORD_CHAR);

	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolled_comments),
				       GTK_POLICY_NEVER,
	   			       GTK_POLICY_AUTOMATIC);

	gtk_container_add(GTK_CONTAINER(frame_comments), scrolled_comments);
	gtk_container_add(GTK_CONTAINER(scrolled_comments),
			  self->textview_comments);

	/* Signal handlers. */

	g_signal_connect(G_OBJECT(self->notebook), "switch-page",
			 G_CALLBACK(irreco_theme_creator_dlg_notebook_changed),
			 self);

	g_signal_connect(G_OBJECT(self->preview_event_box),
			 "button-release-event",
    			 G_CALLBACK(irreco_theme_creator_dlg_preview_image),
			 self);

	g_signal_connect(G_OBJECT(self->add_button), "clicked",
			 G_CALLBACK(irreco_theme_creator_dlg_new_bg_button),
			 self);
	g_signal_connect(G_OBJECT(self->edit_button), "clicked",
			 G_CALLBACK(irreco_theme_creator_dlg_edit_bg_button),
			 self);
	g_signal_connect(G_OBJECT(self->delete_button), "clicked",
			 G_CALLBACK(irreco_theme_creator_dlg_delete_bg_button),
			 self);
	gtk_window_set_default_size(GTK_WINDOW(self), 680, 355);
	/*gtk_widget_set_size_request(GTK_WIDGET(self), 696, 396);*/
	gtk_widget_show_all(GTK_WIDGET(self));
	IRRECO_RETURN
}

static void irreco_theme_creator_dlg_init(IrrecoThemeCreatorDlg *self)
{
	IRRECO_ENTER
	IRRECO_RETURN
}

static void irreco_theme_creator_dlg_finalize(GObject *object)
{
	IrrecoThemeCreatorDlg *self;
	IRRECO_ENTER

	self = IRRECO_THEME_CREATOR_DLG(object);

	G_OBJECT_CLASS(irreco_theme_creator_dlg_parent_class)->finalize(object);
	IRRECO_RETURN
}

static void
irreco_theme_creator_dlg_class_init(IrrecoThemeCreatorDlgClass *klass)
{
	GObjectClass* object_class = G_OBJECT_CLASS (klass);

	object_class->finalize = irreco_theme_creator_dlg_finalize;
	object_class->constructed = irreco_theme_creator_dlg_constructed;

}

GtkWidget
*irreco_theme_creator_dlg_new(IrrecoData *irreco_data, GtkWindow *parent)
{
	IrrecoThemeCreatorDlg *self;

	IRRECO_ENTER
	self = g_object_new(IRRECO_TYPE_THEME_CREATOR_DLG,
			    "irreco-data", irreco_data,
			    NULL);
	/*self = g_object_new(IRRECO_TYPE_THEME_CREATOR_DLG, NULL);*/
	irreco_dlg_set_parent(IRRECO_DLG(self), parent);

	IRRECO_RETURN_PTR(self);
}

/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/**
 * @name Private Functions
 * @{
 */

/**
 * Start a loader state machine if one is not running already.
 */
static void _loader_start(IrrecoThemeCreatorDlg *self, GSourceFunc function)
{
	IRRECO_ENTER

	if (self->loader_func_id == 0) {
		self->loader_func_id = g_idle_add((GSourceFunc)function, self);
	}

	IRRECO_RETURN
}

/**
 * Stop and cleanup loader if a loader is running.
 */
static void _loader_stop(IrrecoThemeCreatorDlg *self)
{
	IRRECO_ENTER
	/*_hide_banner(self);*/
	if (self->loader_func_id != 0) {
		g_source_remove(self->loader_func_id);
		self->loader_func_id = 0;
		self->loader_state = 0;
	}
	IRRECO_RETURN
}

static gboolean
irreco_theme_creator_dlg_display_theme_detail(IrrecoThemeCreatorDlg *self,
					      gint page)
{

	IRRECO_ENTER
	IRRECO_DEBUG("Page_nro %d\n", page);
	IRRECO_RETURN_BOOL(TRUE);
}


/**
 * Sets the theme details
 */

void _set_theme_details(IrrecoThemeCreatorDlg *self, IrrecoTheme *irreco_theme)
{

	IRRECO_ENTER

	gtk_entry_set_text(GTK_ENTRY(self->entry_name),
			   self->theme->name->str);
	gtk_entry_set_text(GTK_ENTRY(self->entry_author),
			   self->theme->author->str);

	self->buffer_comments = gtk_text_view_get_buffer(GTK_TEXT_VIEW(
							 self->textview_comments));
	gtk_text_buffer_set_text(GTK_TEXT_BUFFER(self->buffer_comments),
				 self->theme->comment->str,
      				 -1);
	self->backgrounds =  irreco_theme_creator_backgrounds_new(
							GTK_WINDOW(self),
							self->irreco_data,
       							self->theme);
	self->buttons = irreco_theme_creator_buttons_new(GTK_WINDOW(self),
							 self->irreco_data,
							 self->theme);
	/* Set window title */
	gtk_window_set_title(GTK_WINDOW(self), _("Edit Theme"));


	_set_preview(self);

	gtk_container_add(GTK_CONTAINER(self->hbox_buttons), self->buttons);
	gtk_container_add(GTK_CONTAINER(self->hbox_backgrounds),
			  self->backgrounds);


	IRRECO_RETURN
}

void _create_bg_and_button_widgets(IrrecoThemeCreatorDlg *self)
{
	IRRECO_ENTER

	self->backgrounds =  irreco_theme_creator_backgrounds_new(
							GTK_WINDOW(self),
							self->irreco_data,
       							self->theme);
	self->buttons = irreco_theme_creator_buttons_new(GTK_WINDOW(self),
							 self->irreco_data,
							 self->theme);

	gtk_container_add(GTK_CONTAINER(self->hbox_buttons), self->buttons);
	gtk_container_add(GTK_CONTAINER(self->hbox_backgrounds),
			  self->backgrounds);
	IRRECO_RETURN

}
/**
 * Update shown preview image
 */
static void _set_preview(IrrecoThemeCreatorDlg *self)
{
	IRRECO_ENTER

	/* if theme contains preview image, use it, else get first button */
	if(self->preview_name) {
		IRRECO_DEBUG("Preview set in dlg, using it\n");
	} else if(strlen(self->theme->preview_button_name->str) != 0) {
		gchar *strtblkey;
		IRRECO_DEBUG("Preview set to theme, using it\n");
		strtblkey = g_strdup_printf("%s/%s", self->theme->name->str,
					 self->theme->preview_button_name->str);
		IRRECO_DEBUG("STRTBKEY: %s\n", strtblkey);
		if(irreco_string_table_get(self->theme->buttons, strtblkey,
		   (gpointer *) &self->preview_button)) {
			self->preview_name = self->preview_button->image_up->str;
		} else {
			IRRECO_DEBUG("Theme preview set wrong\n");
		}
	} else {
		const gchar *key;
		IRRECO_DEBUG("No preview set, using first button of theme\n");
		irreco_string_table_index(self->theme->buttons, 0, &key,
					  (gpointer *) &self->preview_button);
		if(self->preview_button) {
			self->preview_name = self->preview_button->image_up->str;
		}
	}
	gtk_image_set_from_file(GTK_IMAGE(self->preview_image),
				self->preview_name);
	IRRECO_RETURN
}

void _set_edited_theme_details(IrrecoThemeCreatorDlg *self)
{
	GtkTextIter startiter;
	GtkTextIter enditer;
	const gchar *name = NULL;
	const gchar *author = NULL;
	gchar *comment = NULL;

	IRRECO_ENTER

	name = gtk_entry_get_text(GTK_ENTRY(self->entry_name));
	author = gtk_entry_get_text(GTK_ENTRY(self->entry_author));


	self->buffer_comments = gtk_text_view_get_buffer(GTK_TEXT_VIEW(
						self->textview_comments));

	gtk_text_buffer_get_start_iter(GTK_TEXT_BUFFER(self->buffer_comments),
				       &startiter);
	gtk_text_buffer_get_end_iter(GTK_TEXT_BUFFER(self->buffer_comments),
				     &enditer);
	comment = gtk_text_buffer_get_text(GTK_TEXT_BUFFER(
					   self->buffer_comments),
					   &startiter,
					   &enditer,
					   FALSE);
	/* Check whether preview button set */
	if (self->preview_name == NULL) {
		_set_preview(self);
	}
	irreco_theme_set(self->theme,
			 name,
    			NULL,
		      	"user",
			author,
		      	comment,
			self->preview_name,
			NULL);


	IRRECO_RETURN
}

gboolean
irreco_theme_creator_dlg_check_details(IrrecoThemeCreatorDlg *self)
{

	gboolean	rvalue = TRUE;
	IRRECO_ENTER
	/* check that there is at least one button */
	if (!irreco_string_table_lenght(self->theme->buttons)) {
		irreco_error_dlg(GTK_WINDOW(self),
				 "must be at least one button");
		rvalue = FALSE;
	} else if (strlen(gtk_entry_get_text(GTK_ENTRY(self->entry_name))) == 0) {
		irreco_error_dlg(GTK_WINDOW(self),
				 "Name is missing");
		rvalue = FALSE;
	} else if (strlen(gtk_entry_get_text(GTK_ENTRY(self->entry_author))) == 0) {
		irreco_error_dlg(GTK_WINDOW(self),
				 "Author is missing");
		rvalue = FALSE;
	}

	IRRECO_RETURN_BOOL(rvalue);
}
/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Functions                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Public Functions                                                           */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
gboolean
irreco_theme_creator_dlg_run(GtkWindow *parent_window, IrrecoData *irreco_data,
			     IrrecoTheme *irreco_theme)
{
	IrrecoThemeCreatorDlg *self;
	gint		response;
	gboolean	loop = TRUE;
	gboolean	rvalue = FALSE;
	IRRECO_ENTER

	self = (IrrecoThemeCreatorDlg*)irreco_theme_creator_dlg_new(irreco_data,
		parent_window);
	self->parent_window = GTK_WINDOW(self);
	self->loader_func_id = 0;
	self->irreco_data = irreco_data;
	self->preview_name = NULL;
	irreco_theme_print(irreco_theme);
	self->theme = irreco_theme;
	IRRECO_DEBUG("Pointer: %p \n", (void*) self->theme);
	IRRECO_DEBUG("Pointer: %p \n", (void*) irreco_theme);

	/* Check whether the theme of a blank*/
	if (irreco_string_table_lenght(irreco_theme->buttons) != 0) {
		/* Sets the theme details */
		_set_theme_details(self, irreco_theme);
	} else {
		/* create blank bg:s and buttons widgets */
		_create_bg_and_button_widgets(self);
	}

	do {
		response = gtk_dialog_run(GTK_DIALOG(self));
		switch (response) {
		case GTK_RESPONSE_OK:

			self->loader_state = LOADER_STATE_INIT;
			_loader_start(self, NULL);
			/* Check theme details */
			if (irreco_theme_creator_dlg_check_details(self)) {
				/* Call set edited_theme_details functio */
				_set_edited_theme_details(self);

				/* Call ThemeSaveDlg */
				if (irreco_theme_save_dlg_run(self->irreco_data,
				    irreco_theme, GTK_WINDOW(self))) {
					rvalue = TRUE;
					loop = FALSE;
				} else {
					rvalue = FALSE;
					loop = TRUE;
				}
			} else {
				rvalue = FALSE;
				loop = TRUE;
			}
			break;

		case GTK_RESPONSE_DELETE_EVENT:
			IRRECO_DEBUG("GTK_RESPONSE_DELETE_EVENT\n");
			_loader_stop(self);

			loop = FALSE;
			break;

		default:
			IRRECO_DEBUG("default\n");
			break;
		}

	} while (loop);

	gtk_widget_destroy(GTK_WIDGET(self));

	IRRECO_RETURN_BOOL(rvalue);
}

static void
irreco_theme_creator_dlg_notebook_changed(GtkNotebook *notebook,
					  GtkNotebookPage *page,
       					  guint page_num,
	      				  IrrecoThemeCreatorDlg *self)
{

	IRRECO_ENTER

 	switch (page_num){

	case ABOUT:
/*		gtk_widget_show(self->cancel_button);*/
		gtk_widget_show(self->save_button);
		gtk_widget_hide(self->add_button);
		gtk_widget_hide(self->edit_button);
		gtk_widget_hide(self->delete_button);

		irreco_theme_creator_dlg_display_theme_detail(self, page_num);

		break;
	case BUTTONS:
/*		gtk_widget_hide(self->cancel_button);*/
		gtk_widget_hide(self->save_button);
		gtk_widget_show(self->add_button);
		gtk_widget_show(self->edit_button);
		gtk_widget_show(self->delete_button);

		irreco_theme_creator_dlg_display_theme_detail(self, page_num);
		gtk_widget_show_all(GTK_WIDGET(self->buttons));

		break;
	case BACKGROUNDS:
/*		gtk_widget_hide(self->cancel_button);*/
		gtk_widget_hide(self->save_button);
		gtk_widget_show(self->add_button);
		gtk_widget_show(self->edit_button);
		gtk_widget_show(self->delete_button);

		irreco_theme_creator_dlg_display_theme_detail(self, page_num);
		gtk_widget_show_all(GTK_WIDGET(self->backgrounds));

		break;

	default:
		IRRECO_DEBUG("default\n");
		break;
		}
	IRRECO_RETURN
}

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Events and Callbacks                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Events and Callbacks
 * @{
 */

static void irreco_theme_creator_dlg_preview_image(GtkWidget *widget,
						   GdkEventButton *event,
	 					   IrrecoThemeCreatorDlg *self)
{

	IrrecoThemeButton *preview_theme_button = NULL;

	IRRECO_ENTER
	if (self->theme != NULL) {
		preview_theme_button = irreco_button_browser_dlg_run(
							GTK_WINDOW(self),
							self->irreco_data,
	    						self->theme);

		if(preview_theme_button) {
			IRRECO_DEBUG("Set preview button to theme and self->\n");

			self->preview_name = preview_theme_button->image_up->str;
			self->preview_button = preview_theme_button;
			_set_preview(self);
		}
	}
	IRRECO_RETURN
}

/** @} */

/*
*Create new background or button
*/

static void irreco_theme_creator_dlg_new_bg_button(GtkButton *button,
					      	IrrecoThemeCreatorDlg *self)
{

	IRRECO_ENTER
	if (gtk_notebook_get_current_page(GTK_NOTEBOOK(
	    self->notebook))==BACKGROUNDS) {

		IrrecoThemeBg *new_bg = NULL;
		new_bg = irreco_theme_bg_new();

		if (irreco_background_creator_dlg_run(self->irreco_data,
						  self->theme,
						  GTK_WINDOW(self), new_bg)) {
			irreco_string_table_add(self->theme->backgrounds,
				 	 new_bg->image_name->str,
				 	 new_bg);
			irreco_theme_creator_backgrounds_refresh(
					       IRRECO_THEME_CREATOR_BACKGROUNDS(
						self->backgrounds));
		} else {
			/*irreco_theme_bg_free(new_bg);*/
		}
	} else {
		IrrecoThemeButton *new_button = NULL;
		new_button = irreco_theme_button_new(NULL);

		if (irreco_button_creator_dlg_run(self->irreco_data, self->theme,
						  GTK_WINDOW(self), new_button)) {
			irreco_string_table_add(self->theme->buttons,
				 		new_button->name->str,
				 		new_button);
			irreco_theme_creator_buttons_refresh(
				IRRECO_THEME_CREATOR_BUTTONS(self->buttons));
		} else {
			/*irreco_theme_button_free(new_button);*/
		}
	}
	IRRECO_RETURN
}

/**
 *Create edit background or button
 */

static void irreco_theme_creator_dlg_edit_bg_button(GtkButton *button,
					      	IrrecoThemeCreatorDlg *self)
{
	IRRECO_ENTER
	/* Check which page */
	/* Edit bg */
	if (gtk_notebook_get_current_page(GTK_NOTEBOOK(
	    self->notebook)) == BACKGROUNDS) {

		IrrecoThemeBg *bg = NULL;
		IrrecoThemeBg *new_bg = NULL;
		IrrecoThemeBg *old_bg = NULL;

		bg = irreco_theme_creator_backgrounds_get_selected_bg(
				IRRECO_THEME_CREATOR_BACKGROUNDS(self->backgrounds));
		/* check whether the background image of the selected */
		if (bg) {
			new_bg = irreco_theme_bg_copy(bg);

			IRRECO_DEBUG("Pointer: %p \n", (void*) self->theme);

			if (irreco_background_creator_dlg_run(self->irreco_data,
							      self->theme,
       							      GTK_WINDOW(self),
							      new_bg)) {
				old_bg = irreco_theme_bg_copy(bg);
				irreco_theme_bg_set(bg, new_bg->image_name->str,
						    new_bg->image_path->str);

				irreco_theme_creator_backgrounds_refresh(
						IRRECO_THEME_CREATOR_BACKGROUNDS(
						self->backgrounds));
			}
			irreco_theme_bg_free(new_bg);

		} else
		{
			 IRRECO_DEBUG("the background is not selected\n");
		}
	/* Edit Button */
	} else {
		IrrecoThemeButton *button = NULL;
		IrrecoThemeButton *new_button = NULL;
		IrrecoThemeButton *old_button = NULL;

		button = irreco_theme_creator_buttons_get_selected_button(
				IRRECO_THEME_CREATOR_BUTTONS(self->buttons));
		/* check whether the button image of the selected */
		if (button) {
			new_button = irreco_theme_button_copy(button);

			if (irreco_button_creator_dlg_run(self->irreco_data,
							  self->theme,
							  GTK_WINDOW(self),
							  new_button)) {
				old_button = irreco_theme_button_copy(button);
				irreco_theme_button_print(new_button);
				irreco_theme_button_set_from_button(button,
								    new_button);

				irreco_theme_button_print(old_button);
				irreco_theme_button_print(button);

				irreco_theme_creator_buttons_refresh(
					IRRECO_THEME_CREATOR_BUTTONS(
					self->buttons));

			}

			irreco_theme_button_free(new_button);
		} else {
			 IRRECO_DEBUG("the button is not selected\n");
		}
	}
	IRRECO_RETURN
}

static void irreco_theme_creator_dlg_delete_bg_button(GtkButton *button,
					      	IrrecoThemeCreatorDlg *self)
{
	IRRECO_ENTER

	if (gtk_notebook_get_current_page(GTK_NOTEBOOK(
	    self->notebook)) == BACKGROUNDS) {
		IrrecoThemeBg	*bg;
		bg = irreco_theme_creator_backgrounds_get_selected_bg(
				IRRECO_THEME_CREATOR_BACKGROUNDS(self->backgrounds));

		if (bg !=NULL) {
			if (irreco_theme_creator_backgrounds_remove_selected(
			IRRECO_THEME_CREATOR_BACKGROUNDS(self->backgrounds))) {
				irreco_string_table_remove((self->theme)->backgrounds,
							bg->image_name->str);
			}
		}
	 } else {
		IrrecoThemeButton *button;
		button = irreco_theme_creator_buttons_get_selected_button(
				IRRECO_THEME_CREATOR_BUTTONS(self->buttons));
		if (button !=NULL) {
			if (irreco_theme_creator_buttons_remove_selected(
				IRRECO_THEME_CREATOR_BUTTONS(self->buttons))) {
				irreco_string_table_remove((self->theme)->buttons,
							button->style_name->str);
			}
		}
		self->preview_name = NULL;
		g_string_printf(self->theme->preview_button_name, "%s", "");

	}

	IRRECO_RETURN
}

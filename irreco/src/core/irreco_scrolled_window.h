/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoScrolledWindow
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoScrolledWindow.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_SCROLLED_WINDOW_H_TYPEDEF__
#define __IRRECO_SCROLLED_WINDOW_H_TYPEDEF__

typedef struct _IrrecoScrolledWindow IrrecoScrolledWindow;
typedef struct _IrrecoScrolledWindowClass IrrecoScrolledWindowClass;

#define IRRECO_TYPE_SCROLLED_WINDOW irreco_scrolled_window_get_type()

#define IRRECO_SCROLLED_WINDOW(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  IRRECO_TYPE_SCROLLED_WINDOW, IrrecoScrolledWindow))

#define IRRECO_SCROLLED_WINDOW_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  IRRECO_TYPE_SCROLLED_WINDOW, IrrecoScrolledWindowClass))

#define IRRECO_IS_SCROLLED_WINDOW(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  IRRECO_TYPE_SCROLLED_WINDOW))

#define IRRECO_IS_SCROLLED_WINDOW_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  IRRECO_TYPE_SCROLLED_WINDOW))

#define IRRECO_SCROLLED_WINDOW_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  IRRECO_TYPE_SCROLLED_WINDOW, IrrecoScrolledWindowClass))

#endif /* __IRRECO_SCROLLED_WINDOW_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_SCROLLED_WINDOW_H__
#define __IRRECO_SCROLLED_WINDOW_H__
#include "irreco.h"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
struct _IrrecoScrolledWindow {
	GtkScrolledWindow parent;
	GtkWidget *alignment;
	GtkWidget *child;

	gint	width_min;
	gint	width_max;
	gint	height_min;
	gint	height_max;
};

struct _IrrecoScrolledWindowClass {
	GtkScrolledWindowClass parent_class;
};


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
GType		irreco_scrolled_window_get_type (void);
GtkWidget*	irreco_scrolled_window_new (void);
GtkWidget*	irreco_scrolled_window_new_with_autoresize(gint width_min,
							   gint width_max,
							   gint height_min,
							   gint height_max);
void	irreco_scrolled_window_set_autoresize(IrrecoScrolledWindow *self,
					      gint width_min,
					      gint width_max,
					      gint height_min,
					      gint height_max);
void	irreco_scrolled_window_add(IrrecoScrolledWindow *self,
				   GtkWidget *widget);
#endif /* __IRRECO_SCROLLED_WINDOW_H__ */

/** @} */

/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoBackendSelectDlg
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoBackendSelectDlg.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_BACKEND_SELECT_DLG_TYPEDEF__
#define __IRRECO_BACKEND_SELECT_DLG_TYPEDEF__
#define IRRECO_TYPE_BACKEND_SELECT_DLG irreco_backend_select_dlg_get_type()
#define IRRECO_BACKEND_SELECT_DLG(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  IRRECO_TYPE_BACKEND_SELECT_DLG, IrrecoBackendSelectDlg))
#define IRRECO_BACKEND_SELECT_DLG_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  IRRECO_TYPE_BACKEND_SELECT_DLG, IrrecoBackendSelectDlgClass))
#define IRRECO_IS_BACKEND_SELECT_DLG(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  IRRECO_TYPE_BACKEND_SELECT_DLG))
#define IRRECO_IS_BACKEND_SELECT_DLG_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  IRRECO_TYPE_BACKEND_SELECT_DLG))
#define IRRECO_BACKEND_SELECT_DLG_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  IRRECO_TYPE_BACKEND_SELECT_DLG, IrrecoBackendSelectDlgClass))

typedef struct _IrrecoBackendSelectDlg IrrecoBackendSelectDlg;
typedef struct _IrrecoBackendSelectDlgClass IrrecoBackendSelectDlgClass;

#endif /* __IRRECO_BACKEND_SELECT_DLG_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_BACKEND_SELECT_DLG__
#define __IRRECO_BACKEND_SELECT_DLG__
#include "irreco.h"
#include "irreco_dlg.h"
#include "irreco_data.h"
#include "irreco_listbox_text.h"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

struct _IrrecoBackendSelectDlg {
	IrrecoDlg parent;

	IrrecoData 	*irreco_data;
	GtkWidget	*listbox;
};

struct _IrrecoBackendSelectDlgClass {
	IrrecoDlgClass parent_class;
};


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
GType		irreco_backend_select_dlg_get_type(void);
GtkWidget *	irreco_backend_select_dlg_new(IrrecoData *irreco_data,
					      GtkWindow *parent);
gboolean	irreco_show_backend_select_dlg(IrrecoData *irreco_data,
					       GtkWindow *parent,
					       IrrecoBackendLib ** backend_lib);
void	irreco_backend_select_dlg_set_irreco_data(IrrecoBackendSelectDlg *self,
						  IrrecoData *irreco_data);

#endif /* __IRRECO_BACKEND_SELECT_DLG__ */

/** @} */

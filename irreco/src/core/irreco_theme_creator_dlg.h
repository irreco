/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * irreco - Ir Remote Control
 * Copyright (C) 2008 Pekka Gehör (pegu6@msn.com)
 *
 * irreco_theme_creator_dlg.c is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * irreco_theme_creator_dlg.c is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @addtogroup IrrecoThemeCreatorDlg
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoThemeCreatorDlg.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_THEME_CREATOR_DLG_H_TYPEDEF__
#define __IRRECO_THEME_CREATOR_DLG_H_TYPEDEF__

#define IRRECO_TYPE_THEME_CREATOR_DLG             (irreco_theme_creator_dlg_get_type ())
#define IRRECO_THEME_CREATOR_DLG(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), IRRECO_TYPE_THEME_CREATOR_DLG, IrrecoThemeCreatorDlg))
#define IRRECO_THEME_CREATOR_DLG_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), IRRECO_TYPE_THEME_CREATOR_DLG, IrrecoThemeCreatorDlgClass))
#define IRRECO_IS_THEME_CREATOR_DLG(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), IRRECO_TYPE_THEME_CREATOR_DLG))
#define IRRECO_IS_THEME_CREATOR_DLG_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), IRRECO_TYPE_THEME_CREATOR_DLG))
#define IRRECO_THEME_CREATOR_DLG_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), IRRECO_TYPE_THEME_CREATOR_DLG, IrrecoThemeCreatorDlgClass))

typedef struct _IrrecoThemeCreatorDlgClass IrrecoThemeCreatorDlgClass;
typedef struct _IrrecoThemeCreatorDlg IrrecoThemeCreatorDlg;

#endif /* __IRRECO_THEME_CREATOR_DLG_H_TYPEDEF__ */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_THEME_CREATOR_DLG_H__
#define __IRRECO_THEME_CREATOR_DLG_H__
#include "irreco.h"
#include "irreco_internal_dlg.h"
#include "irreco_button.h"
#include "irreco_bg_browser_widget.h"
#include "irreco_button_browser_widget.h"
#include "irreco_background_creator_dlg.h"
#include "irreco_button_creator_dlg.h"
#include "irreco_theme_creator_backgrounds.h"
#include "irreco_theme_save_dlg.h"
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/



struct _IrrecoThemeCreatorDlgClass
{
	IrrecoInternalDlgClass parent_class;
};


struct _IrrecoThemeCreatorDlg
{

	IrrecoInternalDlg	parent_instance;
	GtkWindow		*parent_window;
	IrrecoTheme		*theme;
	IrrecoData		*irreco_data;
	GtkWidget		*notebook;

	/*Buttons*/
	GtkWidget		*save_button;
	GtkWidget		*add_button;
	GtkWidget		*edit_button;
	GtkWidget		*delete_button;

	GString			*filename;

	IrrecoThemeButton	*preview_button;

	GtkWidget		*theme_bg;

	/*ABOUT*/
	GtkWidget		*entry_author;
	GtkWidget		*entry_name;
	GtkWidget		*textview_comments;
	GtkWidget		*preview_image;
	GtkTextBuffer		*buffer_comments;
	gchar			*preview_name;
	GtkWidget		*preview_event_box;

	/* BUTTONS */
	GtkWidget		*buttons;
	GtkWidget		*hbox_buttons;
	/* BACKGROUNDS */
	GtkWidget		*backgrounds;
	GtkWidget		*hbox_backgrounds;

	gint			loader_state;
	gint			loader_func_id;


};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

GType irreco_theme_creator_dlg_get_type (void);
gboolean irreco_theme_creator_dlg_run(GtkWindow *parent_window,
				      IrrecoData *irreco_data,
	  			      IrrecoTheme *irreco_theme);

#endif /* __IRRECO_THEME_CREATOR_DLG_H__ */

/** @} */

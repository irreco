/*
 * irreco - Ir Remote Control
 * Copyright (C) 2008 Joni Kokko (t5kojo01@students.oamk.fi)
 *		      Pekka Gehör (pegu6@msn.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_theme_bg.h"

/**
 * @addtogroup IrrecoThemeBg
 * @ingroup Irreco
 *
 * Contains information of background.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoThemeBg.
 */

#define IRRECO_BACKGROUNDS_PREVIEW_WIDHT	(IRRECO_SCREEN_WIDTH)
#define IRRECO_BACKGROUNDS_PREVIEW_HEIGHT	(IRRECO_SCREEN_HEIGHT)
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

IrrecoThemeBg *irreco_theme_bg_new()
{
	IrrecoThemeBg *self;
	IRRECO_ENTER

	self = g_slice_new0(IrrecoThemeBg);

	self->image_name = g_string_new("");
	self->image_path = g_string_new("");

	IRRECO_RETURN_PTR(self);
}

void irreco_theme_bg_free(IrrecoThemeBg *self)
{
	IRRECO_ENTER

	g_assert(self != NULL);

	g_string_free(self->image_name, TRUE);
	self->image_name = NULL;

	g_string_free(self->image_path, TRUE);
	self->image_path = NULL;

	g_slice_free(IrrecoThemeBg, self);
	self = NULL;

	IRRECO_RETURN
}

/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Public Functions                                                           */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */

void irreco_theme_bg_set(IrrecoThemeBg *self,
			 const char *image_name,
			 const char *image_path)
{
	IRRECO_ENTER

	g_string_printf(self->image_name, "%s", image_name);
	g_string_printf(self->image_path, "%s", image_path);

	IRRECO_RETURN
}

void irreco_theme_bg_print(IrrecoThemeBg *self)
{
	IRRECO_ENTER

	IRRECO_DEBUG("--------------------------------------------\n");
	IRRECO_DEBUG("Background-image: %s\n", self->image_name->str);
	IRRECO_DEBUG("Background-image_path: %s\n", self->image_path->str);
	IRRECO_DEBUG("--------------------------------------------\n");

	IRRECO_RETURN
}

IrrecoThemeBg *irreco_theme_bg_copy(IrrecoThemeBg *self)
{
	IrrecoThemeBg *new = NULL;
	IRRECO_ENTER

	new = irreco_theme_bg_new();
	irreco_theme_bg_set(new, self->image_name->str, self->image_path->str);
	IRRECO_RETURN_PTR(new);
}

/**
 * IrrecoThemeBg new from dir
 */

IrrecoThemeBg *irreco_theme_bg_new_from_dir(const gchar *dir)
{
	IrrecoThemeBg *self = NULL;

	IRRECO_ENTER

	self = irreco_theme_bg_new();
	irreco_theme_bg_read(self, dir);
	IRRECO_RETURN_PTR(self);
}

void irreco_theme_bg_read(IrrecoThemeBg *self, const gchar *dir)
{

	IrrecoKeyFile	*keyfile;
	char		*name	= NULL;
	char		*image	= NULL;
	GString	*conf		= NULL;
	IRRECO_ENTER

	conf = g_string_new(dir);
	g_string_append_printf(conf, "/bg.conf");
	keyfile = irreco_keyfile_create(dir,
					conf->str,
					"theme-bg");
	if (keyfile == NULL) goto end;
	/* Required fields. */
	if (!irreco_keyfile_get_str(keyfile, "name", &name) ||
	    !irreco_keyfile_get_path(keyfile, "image", &image)) {
		IRRECO_PRINTF("Could not read style from group \"%s\"\n",
			      keyfile->group);
		goto end;
	}

	irreco_theme_bg_set(self, name, image);
	irreco_theme_bg_print(self);

	end:
	if(keyfile != NULL) irreco_keyfile_destroy(keyfile);
	if(name != NULL) g_free(name);
	if(image != NULL) g_free(image);
	IRRECO_RETURN

}

/**
 * Save bg to theme folder
 */
gboolean irreco_theme_bg_save(IrrecoThemeBg *self, const gchar *path)
{
	gboolean		rvalue = FALSE;
	GString			*keyfile_path;
	GString			*image_path;
	GString			*folder;
	gchar			*folder_name;
	GKeyFile		*keyfile;
	gchar			*cp_cmd;
	gchar			*type;
	GString			*image_name;
	IRRECO_ENTER

	keyfile = g_key_file_new();
	image_name = g_string_new(NULL);
	image_path = g_string_new(path);
	keyfile_path = g_string_new(path);
	folder = g_string_new(self->image_name->str);

	g_string_ascii_down(folder);
	folder_name = g_strdup(folder->str);
	g_strdelimit(folder_name, " ", '_');
	g_strdelimit(folder_name, "-|> <.", 'a');
	g_string_printf(folder, "%s", folder_name);
	/* Create Folder */
	g_string_append_printf(image_path, "/%s", folder_name);
	g_string_append_printf(keyfile_path, "/%s", folder_name);

	IRRECO_DEBUG("mkdir %s\n",image_path->str);
	g_mkdir(image_path->str, 0777);

	/* get file type */
	type = g_strrstr(self->image_path->str, ".");
	g_string_printf(image_name, "%s%s", "image", type);
	g_string_append_printf(image_path, "/%s%s", "image", type);

	/* Copy image to bg folder */
        cp_cmd = g_strconcat("cp -f '", self->image_path->str, "' '",
			     image_path->str,"'", NULL);
        system(cp_cmd);

	/* Create keyfile and save it to folder*/
	irreco_gkeyfile_set_string(keyfile, "theme-bg" , "name",
				   self->image_name->str);
	irreco_gkeyfile_set_string(keyfile, "theme-bg", "image", image_name->str);
	g_string_append_printf(keyfile_path, "/bg.conf");
	irreco_write_keyfile(keyfile, keyfile_path->str);

	/* No error occured. */
	rvalue = TRUE;

	g_free(cp_cmd);
	/*g_free(type);*/
	g_string_free(image_name, TRUE);
	g_key_file_free(keyfile);
	g_string_free(keyfile_path, TRUE);
	g_string_free(image_path, TRUE);

	IRRECO_RETURN_BOOL(rvalue);
}

/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Events and Callbacks                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/** @} */

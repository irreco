/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007,2008  Sami Mäki (kasmra@xob.kapsi.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoWebdbUploadDlg
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoWebdbUploadDlg.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_WEBDB_UPLOAD_DLG_H_TYPEDEF
#define __IRRECO_WEBDB_UPLOAD_DLG_H_TYPEDEF

#define IRRECO_TYPE_WEBDB_UPLOAD_DLG irreco_webdb_upload_dlg_get_type()
#define IRRECO_WEBDB_UPLOAD_DLG(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), IRRECO_TYPE_WEBDB_UPLOAD_DLG, \
  IrrecoWebdbUploadDlg))
#define IRRECO_WEBDB_UPLOAD_DLG_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), IRRECO_TYPE_WEBDB_UPLOAD_DLG, \
  IrrecoWebdbUploadDlgClass))
#define IRRECO_IS_WEBDB_UPLOAD_DLG(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), IRRECO_TYPE_WEBDB_UPLOAD_DLG))
#define IRRECO_IS_WEBDB_UPLOAD_DLG_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), IRRECO_TYPE_WEBDB_UPLOAD_DLG))
#define IRRECO_WEBDB_UPLOAD_DLG_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), IRRECO_TYPE_WEBDB_UPLOAD_DLG, \
  IrrecoWebdbUploadDlgClass))

typedef struct _IrrecoWebdbUploadDlg IrrecoWebdbUploadDlg;
typedef struct _IrrecoWebdbUploadDlgClass IrrecoWebdbUploadDlgClass;

#endif /* _IRRECO_WEBDB_UPLOAD_DLG_H_TYPEDEF */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_WEBDB_UPLOAD_DLG_H__
#define __IRRECO_WEBDB_UPLOAD_DLG_H__
#include "irreco.h"
#include "irreco_dlg.h"
#include "irreco_data.h"
#include "irreco_login_dlg.h"
#include "irreco_device_dlg.h"

/* Include the prototypes for GConf client functions. */
#include <gconf/gconf-client.h>


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

struct _IrrecoWebdbUploadDlg {
	IrrecoDlg 	parent;
	IrrecoData 	*irreco_data;

	const gchar	*backend;
	const gchar	*category;
	const gchar	*file_hash;
	const gchar	*file_name;
	const gchar	*manufacturer;
	const gchar	*model;
	gchar		*user;
	gchar		*password;
	const gchar	*data;

	GtkWidget	*categories;
	GtkWidget	*manufacturers;
	GtkWidget	*models;
	GtkWidget	*device;
};

struct _IrrecoWebdbUploadDlgClass {
	IrrecoDlgClass parent_class;
};


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

GType irreco_webdb_upload_dlg_get_type (void);
IrrecoWebdbUploadDlg *irreco_webdb_upload_dlg_new (IrrecoData *irreco_data,
	GtkWindow *parent);
gboolean irreco_show_webdb_upload_dlg(IrrecoData *irreco_data, GtkWindow *parent,
				  IrrecoBackendFileContainer *file_container,
				  const gchar *device_description,
				  gchar *user,
				  gchar *password);
gboolean irreco_webdb_upload_dlg_add_configuration_cache(
	IrrecoWebdbUploadDlg *self);
gboolean irreco_webdb_upload_dlg_add_configuration(IrrecoWebdbUploadDlg *self);
gboolean irreco_webdb_upload_dlg_populate_category_cbentry(
	IrrecoWebdbUploadDlg *self);
gboolean irreco_webdb_upload_dlg_populate_manufacturer_cbentry(
		IrrecoWebdbUploadDlg *self);

#endif /* __IRRECO_WEBDB_UPLOAD_DLG_H__ */

/** @} */

/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoHardkeyDlg
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoHardkeyDlg.
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_HARDKEY_DLG_H_TYPEDEF__
#define __IRRECO_HARDKEY_DLG_H_TYPEDEF__

typedef struct _IrrecoHardkeyDlg IrrecoHardkeyDlg;
typedef struct _IrrecoHardkeyDlgClass IrrecoHardkeyDlgClass;

#define IRRECO_TYPE_HARDKEY_DLG irreco_hardkey_dlg_get_type()

#define IRRECO_HARDKEY_DLG(obj) \
	(G_TYPE_CHECK_INSTANCE_CAST ((obj), \
	IRRECO_TYPE_HARDKEY_DLG, IrrecoHardkeyDlg))

#define IRRECO_HARDKEY_DLG_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_CAST ((klass), \
	IRRECO_TYPE_HARDKEY_DLG, IrrecoHardkeyDlgClass))

#define IRRECO_IS_HARDKEY_DLG(obj) \
	(G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
	IRRECO_TYPE_HARDKEY_DLG))

#define IRRECO_IS_HARDKEY_DLG_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_TYPE ((klass), \
	IRRECO_TYPE_HARDKEY_DLG))

#define IRRECO_HARDKEY_DLG_GET_CLASS(obj) \
	(G_TYPE_INSTANCE_GET_CLASS ((obj), \
	IRRECO_TYPE_HARDKEY_DLG, IrrecoHardkeyDlgClass))

#endif /* __IRRECO_HARDKEY_DLG_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_HARDKEY_DLG_H__
#define __IRRECO_HARDKEY_DLG_H__
#include "irreco.h"
#include "irreco_internal_dlg.h"
#include "irreco_data.h"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

struct _IrrecoHardkeyDlg {
	IrrecoInternalDlg parent;

	GtkWidget	*keylist;
	GtkWidget	*editor;

	IrrecoHardkeyMap *old_hardkey_map;

	/* The modified copies command chains are stored here until
	   the user clicks OK. If user click CANCEL, these will be
	   simply destroyed. */
	IrrecoHardkeyMap *new_hardkey_map;
};

struct _IrrecoHardkeyDlgClass {
	IrrecoInternalDlgClass parent_class;
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
GtkWidget*	irreco_hardkey_dlg_new(		GtkWindow        *parent,
						IrrecoData       *irreco_data,
						IrrecoHardkeyMap *hardkey_map);
void		irreco_hardkey_dlg_set_hardkey_map(
						IrrecoHardkeyDlg *self,
						IrrecoHardkeyMap *hardkey_map);
void		irreco_hardkey_dlg_apply_changes(
						IrrecoHardkeyDlg *self);



#endif /* __IRRECO_HARDKEY_DLG_H__ */

/** @} */


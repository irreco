/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_hardkey_dlg.h"
#include "irreco_listbox_image.h"
#include "irreco_cmd_chain_editor.h"

/**
 * @addtogroup IrrecoHardkeyDlg
 * @ingroup Irreco
 *
 * Allows user attach commands to hardkeys.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoHardkeyDlg.
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

enum
{
	PROP_0,
	PROP_OLD_HARDKEY_MAP
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

static void
irreco_hardkey_dlg_button_sel_changed(GtkTreeSelection *treeselection,
				      IrrecoHardkeyDlg *self);
static void irreco_hardkey_dlg_responce_signal(IrrecoHardkeyDlg *self,
					       gint response);



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

G_DEFINE_TYPE(IrrecoHardkeyDlg, irreco_hardkey_dlg, IRRECO_TYPE_INTERNAL_DLG)

static void irreco_hardkey_dlg_constructed(GObject *object)
{
	IrrecoHardkeyDlg *self;
	IrrecoData *irreco_data;
	GtkWidget *keylist_frame;
	GtkWidget *editor_frame;
	GtkWidget *hbox;
	IRRECO_ENTER

	G_OBJECT_CLASS(irreco_hardkey_dlg_parent_class)->constructed(object);
	self = IRRECO_HARDKEY_DLG(object);
	irreco_data = irreco_internal_dlg_get_irreco_data(
		IRRECO_INTERNAL_DLG(self));

	/* Construct dialog. */
	gtk_window_set_title(GTK_WINDOW(self), _("Hardware keys"));
	gtk_window_set_modal(GTK_WINDOW(self), TRUE);
	gtk_window_set_destroy_with_parent(GTK_WINDOW(self), TRUE);
	gtk_dialog_set_has_separator(GTK_DIALOG(self), FALSE);
	gtk_dialog_add_buttons(GTK_DIALOG(self),
/*				_("Cancel"), GTK_RESPONSE_NO,*/
			       GTK_STOCK_OK, GTK_RESPONSE_OK,
			       NULL);

	/* Create new Hardkey map. */
	self->new_hardkey_map = irreco_hardkey_map_new(
		irreco_data->cmd_chain_manager);

	/* Check that hardkey maps are set. */
	g_assert(self->new_hardkey_map);
	g_assert(self->old_hardkey_map);

	/* Create widgets. */
	hbox = gtk_hbox_new(0, 8);
	keylist_frame = gtk_frame_new(NULL);
	gtk_frame_set_label_widget(GTK_FRAME(keylist_frame),
				   irreco_gtk_label_bold(_("Key"),
				   0, 0, 0, 0, 0, 0));
	editor_frame = gtk_frame_new(NULL);
	gtk_frame_set_label_widget(GTK_FRAME(editor_frame),
				   irreco_gtk_label_bold(_("Command chain"),
				   0, 0, 0, 0, 0, 0));
	self->keylist = irreco_listbox_image_new_with_autosize(0, 300, 100, 220);
	self->editor = irreco_cmd_chain_editor_new(
		irreco_data, GTK_WINDOW(self));
	irreco_cmd_chain_editor_set_edit_mode(
		IRRECO_CMD_CHAIN_EDITOR(self->editor), IRRECO_DIRECT_EDITING);
	irreco_cmd_chain_editor_set_autosize(
		IRRECO_CMD_CHAIN_EDITOR(self->editor), 0, 350, 100, 220);

	/* Build dialog. */
	gtk_container_add(GTK_CONTAINER(keylist_frame),
			  irreco_gtk_pad(self->keylist, 4, 1, 1, 1));
	gtk_container_add(GTK_CONTAINER(editor_frame),
			  irreco_gtk_pad(self->editor, 4, 1, 1, 1));
	gtk_container_add(GTK_CONTAINER(hbox), keylist_frame);
	gtk_container_add(GTK_CONTAINER(hbox), editor_frame);
	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(self)->vbox),
			  irreco_gtk_pad(hbox, 8, 8, 8, 8));

	/* Fill listbox. */
	/*irreco_listbox_image_append(IRRECO_LISTBOX_IMAGE(self->keylist),
			      "Up",     (void *) IRRECO_HARDKEY_UP,
			      IRRECO_ICON_HARDKEY_DIR "/up.png");
	irreco_listbox_image_append(IRRECO_LISTBOX_IMAGE(self->keylist),
			      "Down",   (void *) IRRECO_HARDKEY_DOWN,
			      IRRECO_ICON_HARDKEY_DIR "/down.png");
	irreco_listbox_image_append(IRRECO_LISTBOX_IMAGE(self->keylist),
			      "Left",   (void *) IRRECO_HARDKEY_LEFT,
			      IRRECO_ICON_HARDKEY_DIR "/left.png");
	irreco_listbox_image_append(IRRECO_LISTBOX_IMAGE(self->keylist),
			      "Right",  (void *) IRRECO_HARDKEY_RIGHT,
			      IRRECO_ICON_HARDKEY_DIR "/right.png");
	irreco_listbox_image_append(IRRECO_LISTBOX_IMAGE(self->keylist),
			      "Select", (void *) IRRECO_HARDKEY_SELECT,
			      IRRECO_ICON_HARDKEY_DIR "/select.png");
	irreco_listbox_image_append(IRRECO_LISTBOX_IMAGE(self->keylist),
			      "Back",   (void *) IRRECO_HARDKEY_BACK,
			      IRRECO_ICON_HARDKEY_DIR "/back.png");
	irreco_listbox_image_append(IRRECO_LISTBOX_IMAGE(self->keylist),
			      "Menu",   (void *) IRRECO_HARDKEY_MENU,
			      IRRECO_ICON_HARDKEY_DIR "/menu.png");
	irreco_listbox_image_append(IRRECO_LISTBOX_IMAGE(self->keylist),
			      "Home",   (void *) IRRECO_HARDKEY_HOME,
			      IRRECO_ICON_HARDKEY_DIR "/home.png");
	
	irreco_listbox_image_append(IRRECO_LISTBOX_IMAGE(self->keylist),
			      "Fullscreen", (void *) IRRECO_HARDKEY_FULLSCREEN,
			      IRRECO_ICON_HARDKEY_DIR "/fullscreen.png");*/
	irreco_listbox_image_append(IRRECO_LISTBOX_IMAGE(self->keylist),
			      "Plus",   (void *) IRRECO_HARDKEY_PLUS,
			      IRRECO_ICON_HARDKEY_DIR "/plus.png");
	irreco_listbox_image_append(IRRECO_LISTBOX_IMAGE(self->keylist),
			      "Minus",  (void *) IRRECO_HARDKEY_MINUS,
			      IRRECO_ICON_HARDKEY_DIR "/minus.png");

	/* Signal handlers. */
	g_signal_connect(G_OBJECT(
			 IRRECO_LISTBOX(self->keylist)->tree_selection),
			 "changed",
			 G_CALLBACK(irreco_hardkey_dlg_button_sel_changed),
			 self);
	g_signal_connect(G_OBJECT(self), "response",
			 G_CALLBACK(irreco_hardkey_dlg_responce_signal),
			 NULL);

	/* This should trigger the signal handler and set fill the editor. */
	irreco_listbox_set_selection(IRRECO_LISTBOX(self->keylist), 0);

	gtk_widget_show_all(GTK_WIDGET(self));
	IRRECO_RETURN
}

static void irreco_hardkey_dlg_finalize(GObject *object)
{
	IrrecoHardkeyDlg *self = NULL;
	IRRECO_ENTER

	G_OBJECT_CLASS(irreco_hardkey_dlg_parent_class)->finalize (object);
	self = IRRECO_HARDKEY_DLG(object);
	irreco_hardkey_map_free(self->new_hardkey_map);
	IRRECO_RETURN
}

static void
irreco_hardkey_dlg_set_property(GObject *object, guint prop_id,
				const GValue *value, GParamSpec *pspec)
{
	IrrecoHardkeyDlg *self = NULL;
	g_return_if_fail (IRRECO_IS_HARDKEY_DLG (object));

	switch (prop_id)
	{
	case PROP_OLD_HARDKEY_MAP:
		self = IRRECO_HARDKEY_DLG(object);
		self->old_hardkey_map =
			(IrrecoHardkeyMap*) g_value_get_pointer(value);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static void irreco_hardkey_dlg_class_init(IrrecoHardkeyDlgClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS(klass);

	object_class->finalize = irreco_hardkey_dlg_finalize;
	object_class->constructed = irreco_hardkey_dlg_constructed;
	object_class->set_property = irreco_hardkey_dlg_set_property;

	g_object_class_install_property(
		object_class,
		PROP_OLD_HARDKEY_MAP,
		g_param_spec_pointer(
			"old-hardkey-map",
			"old-hardkey-map",
			"Old Hardkey Map",
			G_PARAM_CONSTRUCT_ONLY |
			G_PARAM_WRITABLE));
}

static void irreco_hardkey_dlg_init(IrrecoHardkeyDlg *self)
{
}

GtkWidget* irreco_hardkey_dlg_new(GtkWindow *parent,
				  IrrecoData *irreco_data,
				  IrrecoHardkeyMap *hardkey_map)
{
	IrrecoHardkeyDlg *self;
	IRRECO_ENTER

	self = g_object_new(IRRECO_TYPE_HARDKEY_DLG,
			    "irreco-data", irreco_data,
			    "old-hardkey-map", hardkey_map,
			    NULL);

	irreco_dlg_set_parent(IRRECO_DLG(self), parent);
	IRRECO_RETURN_PTR(self);
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Private Functions
 * @{
 */

static void irreco_hardkey_dlg_apply_changes_foreach(IrrecoHardkeyMap *map,
						     guint keyval,
						     IrrecoCmdChainId chain_id,
					 	     IrrecoCmdChain *new_chain,
						     gpointer user_data)
{
	IrrecoCmdChain *old_chain = NULL;
	IrrecoHardkeyDlg *self = IRRECO_HARDKEY_DLG(user_data);
	IRRECO_ENTER

	irreco_hardkey_map_assosiate_chain(
		self->old_hardkey_map, keyval);
	old_chain = irreco_hardkey_map_get_cmd_chain(
		self->old_hardkey_map, keyval);
	irreco_cmd_chain_copy(new_chain, old_chain);
	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Public Functions                                                           */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */

void irreco_hardkey_dlg_set_hardkey_map(IrrecoHardkeyDlg *self,
					IrrecoHardkeyMap *hardkey_map)
{
	IRRECO_ENTER
	self->old_hardkey_map = hardkey_map;
	IRRECO_RETURN

}

void irreco_hardkey_dlg_apply_changes(IrrecoHardkeyDlg *self)
{
	IRRECO_ENTER
	irreco_hardkey_map_chain_foreach(self->new_hardkey_map,
				       irreco_hardkey_dlg_apply_changes_foreach,
				       self);
	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Events and Callbacks                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Events and Callbacks
 * @{
 */

static void
irreco_hardkey_dlg_button_sel_changed(GtkTreeSelection *treeselection,
				      IrrecoHardkeyDlg *self)
{
	guint keyval;
	gchar *hardkey_str;
	gint sel_index;
	gpointer sel_user_data;
	IrrecoCmdChain *old_chain = NULL;
	IrrecoCmdChain *new_chain = NULL;
	IRRECO_ENTER

	irreco_listbox_get_selection(IRRECO_LISTBOX(self->keylist),
				     &sel_index, NULL, &sel_user_data);

	if (sel_index == -1) IRRECO_RETURN;

	g_assert(self->new_hardkey_map);
	g_assert(self->old_hardkey_map);

	keyval = (guint) sel_user_data;
	hardkey_str = irreco_hardkey_to_str(keyval);
	IRRECO_PRINTF("Editing hardkey \"%s\".\n", hardkey_str);
	g_free(hardkey_str);

	if (irreco_hardkey_map_cmd_chain_exists(self->new_hardkey_map, keyval)
						== FALSE) {
		IRRECO_PRINTF("Creating a copy of command chain.\n");
		irreco_hardkey_map_assosiate_chain(
			self->new_hardkey_map, keyval);
		IRRECO_DEBUG("Creating new chain.\n");
		new_chain = irreco_hardkey_map_get_cmd_chain(
			self->new_hardkey_map, keyval);
		IRRECO_DEBUG("Getting old chain.\n");
		old_chain = irreco_hardkey_map_get_cmd_chain(
			self->old_hardkey_map, keyval);
		if (old_chain != NULL && new_chain != NULL) {
			irreco_cmd_chain_copy(old_chain, new_chain);
		}
		irreco_cmd_chain_editor_set_chain(
			IRRECO_CMD_CHAIN_EDITOR(
			self->editor),
			new_chain);

	} else {
		IRRECO_PRINTF("Using old copy of command chain.\n");
		new_chain = irreco_hardkey_map_get_cmd_chain(
			self->new_hardkey_map, keyval);
		irreco_cmd_chain_editor_set_chain(
			IRRECO_CMD_CHAIN_EDITOR(self->editor), new_chain);
	}

	IRRECO_RETURN
}

static void irreco_hardkey_dlg_responce_signal(IrrecoHardkeyDlg *self,
					       gint response)
{
	IRRECO_ENTER
	if (response == GTK_RESPONSE_OK) {
		irreco_hardkey_dlg_apply_changes(self);
	}
	IRRECO_RETURN
}

/** @} */

/** @} */

















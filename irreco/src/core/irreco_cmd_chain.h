/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoCmdChain
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoCmdChain.
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_CMD_CHAIN_H_TYPEDEF__
#define __IRRECO_CMD_CHAIN_H_TYPEDEF__

typedef struct _IrrecoCmdChain IrrecoCmdChain;
typedef signed int IrrecoCmdChainId;

#endif /* __IRRECO_CMD_CHAIN_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_CMD_CHAIN_H__
#define __IRRECO_CMD_CHAIN_H__
#include "irreco.h"
#include "irreco_cmd.h"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
struct _IrrecoCmdChain {
	GList            *command_list;
	glong             execution_rate;
	IrrecoCmdChainId  id;
	gboolean	  show_progress;
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#define IRRECO_CMD_CHAIN_FOREACH(_cmd_chain, _cmd_var_name)	       	       \
	{ GList *_list;							       \
	IrrecoCmd *_cmd_var_name;					       \
	if ((_list = g_list_first(_cmd_chain->command_list)) != NULL) do {     \
		_cmd_var_name = (IrrecoCmd *) _list->data; {
#define IRRECO_CMD_CHAIN_FOREACH_END					       \
	}} while ((_list = g_list_next(_list)) != NULL); }



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
IrrecoCmdChain *irreco_cmd_chain_new();
IrrecoCmdChain *irreco_cmd_chain_new_from_config(IrrecoKeyFile *keyfile,
						 IrrecoData    *irreco_data);
void irreco_cmd_chain_free(IrrecoCmdChain *self);
IrrecoCmdChain *irreco_cmd_chain_create();
void irreco_cmd_chain_remove_all(IrrecoCmdChain *self);
void irreco_cmd_chain_destroy(IrrecoCmdChain *self);
void irreco_cmd_chain_set_id(IrrecoCmdChain   *self,
			     IrrecoCmdChainId  id);
IrrecoCmdChainId irreco_cmd_chain_get_id(IrrecoCmdChain *self);
void irreco_cmd_chain_to_config(IrrecoCmdChain   *self,
				GKeyFile         *keyfile);
gboolean irreco_cmd_chain_from_config(IrrecoCmdChain *self,
				      IrrecoKeyFile  *keyfile,
				      IrrecoData     *irreco_data);
void irreco_cmd_chain_set_execution_rate(IrrecoCmdChain *self,
					 glong execution_rate);
void irreco_cmd_chain_append(IrrecoCmdChain *self,
			     IrrecoCmd * irreco_cmd);
void irreco_cmd_chain_append_copy(IrrecoCmdChain *self,
				  IrrecoCmd * irreco_cmd);
void irreco_cmd_chain_remove(IrrecoCmdChain *self, guint index);
void irreco_cmd_chain_move(IrrecoCmdChain *self,
			   guint from_index, guint to_index);
void irreco_cmd_chain_copy(IrrecoCmdChain * from,
			   IrrecoCmdChain * to);
guint irreco_cmd_chain_length(IrrecoCmdChain *self);
IrrecoCmd *irreco_cmd_chain_get(IrrecoCmdChain *self,
				guint from_index);
void irreco_cmd_chain_print(IrrecoCmdChain *self);
void irreco_cmd_chain_set_show_progress(IrrecoCmdChain *self,
					gboolean        show_progress);
gboolean irreco_cmd_chain_get_show_progress(IrrecoCmdChain *self);
gboolean irreco_cmd_chain_execute(IrrecoCmdChain *self,
				  IrrecoData * irreco_data,
				  GFunc call_when_done,
				  gpointer user_data);

#endif /* __IRRECO_CMD_CHAIN_H__ */

/** @} */


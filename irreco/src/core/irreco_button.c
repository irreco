/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_button.h"

/**
 * @addtogroup IrrecoButton
 * @ingroup Irreco
 *
 * Glues together three different types of buttons, and abstracts the
 * differences.
 *
 * The main component of the IRRECO user interface. The main point is to
 * support a button which is made out of two images. When the button is up
 * the up-image is shown. When the button is down, the down-image is
 * shown.
 *
 * The button images are provided by button style. If buttonstyle is NULL
 * we show a GTK-button. The GTK-button is mainly a fallback option.
 *
 * All buttons are part of a layout array. The layout array contains pointers
 * to callback functions which are called when the button recieves press,
 * release or motion events. You should catch button events trough these
 * callbacks.
 *
 * Relationships:
 *     	   button_layout    ButtonLayout 1
 *         button           ButtonLayout 1, Button 1
 *         button           ButtonLayout 1, Button 2
 *         button           ButtonLayout 1, Button 3
 *         button           ButtonLayout 1, Button 4
 *     	   button_layout    ButtonLayout 2
 *         button           ButtonLayout 2, Button 1
 *         button           ButtonLayout 2, Button 2
 *         button           ButtonLayout 2, Button 3
 *         button           ButtonLayout 2, Button 4
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoButton.
 */


gboolean irreco_button_flash_handler(IrrecoButton * irreco_button);
void irreco_button_connect_signals(IrrecoButton * irreco_button);
void irreco_button_disconnect_signals(IrrecoButton * irreco_button);
void irreco_button_set_motion_events(GtkWidget * gtk_widget);
void irreco_button_validate_position(IrrecoButton * irreco_button, gint * x, gint * y);
void irreco_button_generic_connect_signals(IrrecoButton * irreco_button);
void irreco_button_generic_disconnect_signals(IrrecoButton * irreco_button);
void irreco_button_generic_move(IrrecoButton * irreco_button, gint * x, gint * y);
gboolean irreco_button_generic_motion_notify_event(GtkWidget * widget,
					    GdkEventMotion * event,
					    IrrecoButton * irreco_button);
gboolean irreco_button_generic_press_event(GtkWidget * widget,
					GdkEventButton * event,
					IrrecoButton * irreco_button);
gboolean irreco_button_generic_release_event(GtkWidget * widget,
					  GdkEventButton * event,
					  IrrecoButton * irreco_button);
void irreco_button_generic_destroy_event(GtkObject *object, GtkObject ** widget);
void irreco_button_generic_size_request_event(GtkWidget * widget,
					   GtkRequisition *requisition,
					   IrrecoButton * irreco_button);
void irreco_button_gtk_create(IrrecoButton * irreco_button);
void irreco_button_gtk_connect_signals(IrrecoButton * irreco_button);
void irreco_button_gtk_disconnect_signals(IrrecoButton * irreco_button);
void irreco_button_gtk_down(IrrecoButton * irreco_button);
void irreco_button_gtk_up(IrrecoButton * irreco_button);
void irreco_button_gtk_toggled_event(GtkToggleButton *togglebutton,
				  IrrecoButton * irreco_button);
void irreco_button_img_create(IrrecoButton * irreco_button);
void irreco_button_img_set_text_format(IrrecoButton * irreco_button, gchar * format);
void irreco_button_double_img_connect_signals(IrrecoButton * irreco_button);
void irreco_button_double_img_disconnect_signals(IrrecoButton * irreco_button);
void irreco_button_img_move(IrrecoButton * irreco_button, gint * x, gint * y);
void irreco_button_double_img_down(IrrecoButton * irreco_button);
void irreco_button_double_img_up(IrrecoButton * irreco_button);



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Irreco Button.                                                                */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Irreco Button
 * @{
 */

/*
 * Create new IrrecoButton.
 */

IrrecoButton *irreco_button_create(IrrecoButtonLayout * irreco_layout,
				   gdouble x,
				   gdouble y,
				   const gchar * title,
				   IrrecoThemeButton * style,
				   IrrecoCmdChainManager * manager)
{
	IrrecoButton *irreco_button;
	IRRECO_ENTER

	/* Create new IrrecoButton */
	irreco_button = g_slice_new0(IrrecoButton);
	g_ptr_array_add(irreco_layout->button_array, irreco_button);
	irreco_button->index = irreco_layout->button_array->len - 1;

	/* Copy data to structure */
	irreco_button->irreco_layout = irreco_layout;
	irreco_button->style = style;
	irreco_button->x = x;
	irreco_button->y = y;
	irreco_button->title = g_strdup(title);
	irreco_button->cmd_chain_manager = manager;
	irreco_button_validate_position(irreco_button,
					&irreco_button->x,
					&irreco_button->y);
	IRRECO_RETURN_PTR(irreco_button);
}

/*
 * Destroy widget and remove it from the button_array.
 */
void irreco_button_destroy(IrrecoButton * irreco_button)
{
	IRRECO_ENTER
	irreco_button_destroy_widget(irreco_button);
	g_free(irreco_button->title);
	irreco_button->title = NULL;
	if (irreco_button->cmd_chain_manager && irreco_button->cmd_chain_id) {
		irreco_cmd_chain_manager_free_chain(
			irreco_button->cmd_chain_manager,
			irreco_button->cmd_chain_id);
	}
	g_ptr_array_remove(irreco_button->irreco_layout->button_array,
			   irreco_button);
	g_slice_free(IrrecoButton, irreco_button);
	IRRECO_RETURN
}

void irreco_button_set_title(IrrecoButton * irreco_button, const gchar * title)
{
	IRRECO_ENTER
	irreco_button_destroy_widget(irreco_button);
	g_free(irreco_button->title);
	irreco_button->title = g_strdup(title);
	IRRECO_RETURN
}

void irreco_button_set_cmd_chain_id(IrrecoButton * irreco_button,
				    IrrecoCmdChainId cmd_chain_id)
{
	IRRECO_ENTER
	IRRECO_DEBUG("Setting command chain id to \"%i\".\n",
		     cmd_chain_id);

	irreco_cmd_chain_manager_free_chain(irreco_button->cmd_chain_manager,
					    irreco_button->cmd_chain_id);
	if (irreco_cmd_chain_manager_get_chain(irreco_button->cmd_chain_manager,
					       cmd_chain_id) == NULL) {
		irreco_cmd_chain_manager_new_chain_with_id(
			irreco_button->cmd_chain_manager, cmd_chain_id);
	}
	irreco_button->cmd_chain_id = cmd_chain_id;
	IRRECO_RETURN
}

/*
 * Copy data to the internal command chain.
 */
void irreco_button_set_cmd_chain(IrrecoButton * irreco_button,
				 IrrecoCmdChain * cmd_chain)
{
	IRRECO_ENTER
	irreco_cmd_chain_copy(cmd_chain,
		irreco_button_get_cmd_chain(irreco_button));
	IRRECO_RETURN
}

IrrecoCmdChain *irreco_button_get_cmd_chain(IrrecoButton * irreco_button)
{
	IRRECO_ENTER

	if (irreco_button->cmd_chain_id == 0) {
		irreco_button->cmd_chain_id =
			irreco_cmd_chain_manager_new_chain(
				irreco_button->cmd_chain_manager);
		IRRECO_DEBUG("New command chain id is \"%i\".\n",
			     irreco_button->cmd_chain_id);
	} else {
		IRRECO_DEBUG("Command chain id is \"%i\".\n",
			     irreco_button->cmd_chain_id);
	}

	IRRECO_RETURN_PTR(irreco_cmd_chain_manager_get_chain(
		irreco_button->cmd_chain_manager, irreco_button->cmd_chain_id));
}

void irreco_button_set_style(IrrecoButton * irreco_button,
			     IrrecoThemeButton * style)
{
	IRRECO_ENTER
	irreco_button_destroy_widget(irreco_button);
	irreco_button->style = style;
	IRRECO_RETURN
}

/*
 * Create a new GtkWidget which will match the data in the IrrecoButton
 * structure.
 */
void irreco_button_create_widget(IrrecoButton * irreco_button)
{
	IRRECO_ENTER

	g_assert(irreco_button);
	g_assert(irreco_button->irreco_layout);

	if (irreco_button->irreco_layout->container == NULL) {
		IRRECO_ERROR("Cannot create button %i. Container not set.\n",
			     irreco_button->index);
		IRRECO_RETURN
	}
	if (irreco_button->type != IRRECO_BTYPE_NONE) {
		irreco_button_destroy_widget(irreco_button);
	}

	/* Detect type. */
	if (irreco_button->style == NULL) {
		irreco_button->type = IRRECO_BTYPE_GTK_BUTTON;
		IRRECO_DEBUG("Button %i type is GTK_BUTTON\n",
			     irreco_button->index);

	} else if (irreco_button->style->image_up != NULL &&
		   irreco_button->style->image_down == NULL) {
		irreco_button->type = IRRECO_BTYPE_SINGLE_IMAGE;
		IRRECO_DEBUG("Button %i type is SINGLE_IMAGE. Style is %s.\n",
			     irreco_button->index,
			     irreco_button->style->name->str);

	} else if (irreco_button->style->image_up != NULL &&
		   irreco_button->style->image_down != NULL) {
		irreco_button->type = IRRECO_BTYPE_DOUBLE_IMAGE;
		IRRECO_DEBUG("Button %i type is DOUBLE_IMAGE, Style is %s.\n",
			     irreco_button->index,
			     irreco_button->style->name->str);

	} else {
		IRRECO_ERROR("Failed to create new button for IrrecoButton\n");
		IRRECO_ERROR("Error: Cannot detect type of button.\n");
		IRRECO_RETURN
	}

	/* Create button widget. */
	switch (irreco_button->type) {
		case IRRECO_BTYPE_NONE: IRRECO_RETURN
		case IRRECO_BTYPE_GTK_BUTTON:
			irreco_button_gtk_create(irreco_button);
			break;

		case IRRECO_BTYPE_SINGLE_IMAGE:
		case IRRECO_BTYPE_DOUBLE_IMAGE:
			irreco_button_img_create(irreco_button);
			break;
	}

	/* Set state. */
	if (irreco_button->state == IRRECO_BSTATE_DOWN) {
		irreco_button_down(irreco_button);
	} else {
		irreco_button_up(irreco_button);
	}

	IRRECO_RETURN
}

/*
 * Destroy the button.
 */
void irreco_button_destroy_widget(IrrecoButton* irreco_button)
{
	IRRECO_ENTER
	irreco_button_disconnect_signals(irreco_button);
	if (irreco_button->widget != NULL) {
		gtk_widget_destroy(irreco_button->widget);
		irreco_button->widget = NULL;
	}
	if (irreco_button->widget_down != NULL) {
		gtk_widget_destroy(irreco_button->widget_down);
		irreco_button->widget_down = NULL;
	}
	if (irreco_button->widget_label != NULL) {
		gtk_widget_destroy(irreco_button->widget_label);
		irreco_button->widget_label = NULL;
	}

	IRRECO_RETURN
}

/*
 * Press the button down.
 */
void irreco_button_down(IrrecoButton * irreco_button)
{
	IRRECO_ENTER
	switch (irreco_button->type) {
		case IRRECO_BTYPE_NONE:
			IRRECO_RETURN

		case IRRECO_BTYPE_GTK_BUTTON:
			irreco_button_gtk_down(irreco_button);
			break;

		case IRRECO_BTYPE_DOUBLE_IMAGE:
			irreco_button_double_img_down(irreco_button);
		case IRRECO_BTYPE_SINGLE_IMAGE:
			irreco_button_img_set_text_format(irreco_button,
				irreco_button->style->text_format_down->str);
			break;
	}

	IRRECO_RETURN
}

/*
 * Pop the button up.
 */
void irreco_button_up(IrrecoButton * irreco_button)
{
	IRRECO_ENTER
	switch (irreco_button->type) {
		case IRRECO_BTYPE_NONE:
			IRRECO_RETURN

		case IRRECO_BTYPE_GTK_BUTTON:
			irreco_button_gtk_up(irreco_button);
			break;

		case IRRECO_BTYPE_DOUBLE_IMAGE:
			irreco_button_double_img_up(irreco_button);
		case IRRECO_BTYPE_SINGLE_IMAGE:
			irreco_button_img_set_text_format(irreco_button,
				irreco_button->style->text_format_up->str);
			break;
	}

	IRRECO_RETURN
}

/*
 * Move button inside the layout size.
 *
 * x, y arguments should contain the reguested position of the button should be
 * placed. When this function return, x, y will contain the location where the
 * button was _actually_ placed. You cant move a button outside the layout size.
 */
void irreco_button_move(IrrecoButton * irreco_button, gint * x, gint * y)
{
	IRRECO_ENTER
	switch (irreco_button->type) {
		case IRRECO_BTYPE_NONE: IRRECO_RETURN
		case IRRECO_BTYPE_GTK_BUTTON:
			irreco_button_generic_move(irreco_button, x, y);
			break;

		case IRRECO_BTYPE_DOUBLE_IMAGE:
		case IRRECO_BTYPE_SINGLE_IMAGE:
			irreco_button_img_move(irreco_button, x, y);
			break;
	}

	IRRECO_RETURN
}

/*
 * Pop the button up.
 *
 * You cant change the order the GtkFixed displays widgets, so we destroy
 * the old widget and create new one on the top.
 */
void irreco_button_to_front(IrrecoButton * irreco_button)
{
	IRRECO_ENTER
	g_ptr_array_remove(irreco_button->irreco_layout->button_array,
			   irreco_button);
	g_ptr_array_add(irreco_button->irreco_layout->button_array,
			irreco_button);
	if (irreco_button->type != IRRECO_BTYPE_NONE) {
		irreco_button_destroy_widget(irreco_button);
		irreco_button_create_widget(irreco_button);
	}
	irreco_button_layout_reindex(irreco_button->irreco_layout);
	IRRECO_RETURN
}

/*
 * Push the button down and pop it up soon after.
 *
 * The main point of this function is to simulate button click effect where the
 * use has time to see the button go down, and then pop up.
 */
void irreco_button_flash(IrrecoButton * irreco_button)
{
	IRRECO_ENTER
	switch (irreco_button->type) {
		case IRRECO_BTYPE_NONE:
		case IRRECO_BTYPE_SINGLE_IMAGE:
			IRRECO_RETURN

		case IRRECO_BTYPE_DOUBLE_IMAGE:
		case IRRECO_BTYPE_GTK_BUTTON:
			irreco_button_down(irreco_button);
			irreco_button->state = IRRECO_BSTATE_FLASH;
			g_idle_add(G_SOURCEFUNC(irreco_button_flash_handler),
				   irreco_button);
			break;
	}


	IRRECO_RETURN
}
gboolean irreco_button_flash_handler(IrrecoButton * irreco_button)
{
	IRRECO_ENTER
	if (irreco_button->state == IRRECO_BSTATE_FLASH) {
		irreco_button_up(irreco_button);
	}
	IRRECO_RETURN_BOOL(FALSE);
}

void irreco_button_connect_signals(IrrecoButton * irreco_button)
{
	IRRECO_ENTER
	switch (irreco_button->type) {
		case IRRECO_BTYPE_NONE:
			IRRECO_RETURN

		case IRRECO_BTYPE_GTK_BUTTON:
			irreco_button_gtk_connect_signals(irreco_button);
			break;

		case IRRECO_BTYPE_DOUBLE_IMAGE:
			irreco_button_double_img_connect_signals(irreco_button);
		case IRRECO_BTYPE_SINGLE_IMAGE:
			irreco_button_generic_connect_signals(irreco_button);
			break;
	}

	IRRECO_RETURN
}

/*
 * Disconnect whatever signal handlers the button may have.
 */
void irreco_button_disconnect_signals(IrrecoButton * irreco_button)
{
	IRRECO_ENTER
	switch (irreco_button->type) {
		case IRRECO_BTYPE_NONE:
			IRRECO_RETURN

		case IRRECO_BTYPE_GTK_BUTTON:
			irreco_button_gtk_disconnect_signals(irreco_button);
			break;

		case IRRECO_BTYPE_DOUBLE_IMAGE:
			irreco_button_double_img_disconnect_signals(
			irreco_button);
		case IRRECO_BTYPE_SINGLE_IMAGE:
			irreco_button_generic_disconnect_signals(irreco_button);
			break;
	}

	IRRECO_RETURN
}

/*
 * Setup widget events so that we get motion events.
 */
void irreco_button_set_motion_events(GtkWidget * gtk_widget)
{
	gint events;
	IRRECO_ENTER
	events = (gtk_widget_get_events(GTK_WIDGET(gtk_widget))
		/*| GDK_BUTTON_PRESS_MASK */
		/*| GDK_BUTTON_RELEASE_MASK */
		| GDK_POINTER_MOTION_MASK)
		& ~GDK_POINTER_MOTION_HINT_MASK;
	gtk_widget_set_events(GTK_WIDGET(gtk_widget), events);

	IRRECO_RETURN
}

/*
 * Check if the given x, y coordiantes place the button completly inside
 * layout size. If they dont, change the coordinates so that they do.
 *
 * As a special case, if layout size is 0x0, all coordinates are valid.
 * The point of this is, that you can first create the layout, and set the
 * layout size later on when you know the size of the window.
 */
void irreco_button_validate_position(IrrecoButton * irreco_button,
				     gint * x, gint * y)
{
	IRRECO_ENTER

	if (irreco_button->irreco_layout->width > 0) {
		if (*x + irreco_button->width > irreco_button->irreco_layout->width) {
			*x = irreco_button->irreco_layout->width - irreco_button->width;
		}
		if (*x < 0) *x = 0;
	}

	if (irreco_button->irreco_layout->height > 0) {
		if (*y + irreco_button->height > irreco_button->irreco_layout->height) {
			*y = irreco_button->irreco_layout->height - irreco_button->height;
		}
		if (*y < 0) *y = 0;
	}

	IRRECO_RETURN
}

/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Generic button functions.                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Generic button functions
 * @{
 */

void irreco_button_generic_connect_signals(IrrecoButton * irreco_button)
{
	IRRECO_ENTER
	irreco_button->motion_handler_id = g_signal_connect(
		G_OBJECT(irreco_button->widget), "motion-notify-event",
		G_CALLBACK(irreco_button_generic_motion_notify_event),
			   irreco_button);

	irreco_button->press_handler_id = g_signal_connect(
		G_OBJECT(irreco_button->widget), "button-press-event",
		G_CALLBACK(irreco_button_generic_press_event), irreco_button);

	irreco_button->release_handler_id = g_signal_connect(
		G_OBJECT(irreco_button->widget), "button-release-event",
		G_CALLBACK(irreco_button_generic_release_event), irreco_button);

	irreco_button->size_request_handler_id = g_signal_connect(
		G_OBJECT(irreco_button->widget), "size-request",
		G_CALLBACK(irreco_button_generic_size_request_event),
			   irreco_button);

	irreco_button->destroy_handler_id = g_signal_connect(
		G_OBJECT(irreco_button->widget), "destroy",
		G_CALLBACK(irreco_button_generic_destroy_event),
		&irreco_button->widget);

	IRRECO_RETURN
}

void irreco_button_generic_disconnect_signals(IrrecoButton * irreco_button)
{
	IRRECO_ENTER
	if (irreco_button->widget == NULL) IRRECO_RETURN
	g_signal_handler_disconnect(G_OBJECT(irreco_button->widget),
				    irreco_button->motion_handler_id);
	g_signal_handler_disconnect(G_OBJECT(irreco_button->widget),
				    irreco_button->press_handler_id);
	g_signal_handler_disconnect(G_OBJECT(irreco_button->widget),
				    irreco_button->release_handler_id);
	g_signal_handler_disconnect(G_OBJECT(irreco_button->widget),
				    irreco_button->destroy_handler_id);

	IRRECO_RETURN
}

void
irreco_button_generic_move(IrrecoButton * irreco_button, gint * x, gint * y)
{
	IRRECO_ENTER
	irreco_button_validate_position(irreco_button, x, y);
	irreco_button->x = *x;
	irreco_button->y = *y;
	irreco_button_layout_container_move(irreco_button->irreco_layout,
					    irreco_button->widget, *x, *y);

	IRRECO_RETURN
}

gboolean irreco_button_generic_motion_notify_event(GtkWidget * widget,
						   GdkEventMotion * event,
						   IrrecoButton * irreco_button)
{
	IRRECO_ENTER
	if (irreco_button->irreco_layout->button_motion_callback != NULL) {
		irreco_button->irreco_layout->button_motion_callback(irreco_button,
			irreco_button->irreco_layout->callback_user_data);
	}
	IRRECO_RETURN_BOOL(FALSE);
}

/**
 * Combined press and release functionality to improve usability
 * Now action is performed on klick rather than on button release
 */
gboolean irreco_button_generic_press_event(GtkWidget * widget,
					   GdkEventButton * event,
					   IrrecoButton * irreco_button)
{
	IRRECO_ENTER
	if (irreco_button->irreco_layout->button_press_callback != NULL) {
		irreco_button->irreco_layout->button_press_callback(irreco_button,
			irreco_button->irreco_layout->callback_user_data);
	}

	/*if (irreco_button->irreco_layout->button_release_callback != NULL) {
		irreco_button->irreco_layout->button_release_callback(irreco_button,
				irreco_button->irreco_layout->callback_user_data);
	}*/
	IRRECO_RETURN_BOOL(FALSE);
}

gboolean irreco_button_generic_release_event(GtkWidget * widget,
					     GdkEventButton * event,
					     IrrecoButton * irreco_button)
{
	IRRECO_ENTER
	if (irreco_button->irreco_layout->button_release_callback != NULL) {
		irreco_button->irreco_layout->button_release_callback(irreco_button,
			irreco_button->irreco_layout->callback_user_data);
	}
	IRRECO_RETURN_BOOL(FALSE);
}

/*
 * Gtk will call this when the main window is desroyed.
 *
 * This can be used to catch the destruction of the widget, so that we
 * can set the pointer to NULL.
 */
void irreco_button_generic_destroy_event(GtkObject *object, GtkObject ** widget)
{
	IRRECO_ENTER
	if (*widget == object) {
		*widget = NULL;
	}
	IRRECO_RETURN
}

/*
 * Get Widget size.
 *
 * We need to hide some image widgets during draw, so that we can change
 * the image, but that also means that hidden widget will have size
 * requisition of 0x0. So we must store the size of widget.
 */
void irreco_button_generic_size_request_event(GtkWidget * widget,
					      GtkRequisition *requisition,
					      IrrecoButton * irreco_button)
{
	IRRECO_ENTER
	if (irreco_button->state == IRRECO_BSTATE_UP) {

		/* Store widget size, we no longer need the event hander. */
		irreco_button->width = requisition->width;
		irreco_button->height = requisition->height;
		g_signal_handler_disconnect(G_OBJECT(irreco_button->widget),
					    irreco_button->size_request_handler_id);

		/* If the button contains a label widget, then resize the
		   label to match the widget size.*/
		if (irreco_button->widget_label != NULL) {
			gint padding = irreco_button->style->text_padding * 2;
			gtk_widget_set_size_request(
				GTK_WIDGET(irreco_button->widget_label),
				irreco_button->width - padding,
				irreco_button->height - padding);
		}
	}

	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Gtk button functions.                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Gtk button functions
 * @{
 */

void irreco_button_gtk_create(IrrecoButton * irreco_button)
{
	IrrecoButtonLayout *irreco_layout = irreco_button->irreco_layout;
	IRRECO_ENTER

	/* Make sure that we dont leak old widgets. */
	irreco_button_destroy_widget(irreco_button);

	/* Create and setup button. GtkButton with empty title looks
	   pretty bad, so we use spaces instead of empty title. */
	if (irreco_str_isempty(irreco_button->title)) {
		irreco_button->widget = gtk_toggle_button_new_with_label("   ");
	} else {
		irreco_button->widget = gtk_toggle_button_new_with_label(
			irreco_button->title);
	}

	g_signal_connect(G_OBJECT(irreco_button->widget), "destroy",
			 G_CALLBACK(gtk_widget_destroyed),
			 &irreco_button->widget);

	irreco_button_set_motion_events(irreco_button->widget);
	irreco_button_layout_container_put(irreco_layout, irreco_button->widget,
		      			   irreco_button->x, irreco_button->y);
	irreco_button_connect_signals(irreco_button);
	gtk_widget_show(irreco_button->widget);

	IRRECO_RETURN
}

void irreco_button_gtk_connect_signals(IrrecoButton * irreco_button)
{
	IRRECO_ENTER
	irreco_button_generic_connect_signals(irreco_button);
	irreco_button->extra_handler_id = g_signal_connect(
		G_OBJECT(irreco_button->widget), "toggled",
		G_CALLBACK(irreco_button_gtk_toggled_event), irreco_button);

	IRRECO_RETURN
}

void irreco_button_gtk_disconnect_signals(IrrecoButton * irreco_button)
{
	IRRECO_ENTER
	if (irreco_button->widget == NULL) IRRECO_RETURN
	irreco_button_generic_disconnect_signals(irreco_button);
	g_signal_handler_disconnect(G_OBJECT(irreco_button->widget),
				    irreco_button->extra_handler_id);

	IRRECO_RETURN
}

void irreco_button_gtk_down(IrrecoButton * irreco_button)
{
	GtkToggleButton* button = GTK_TOGGLE_BUTTON(irreco_button->widget);
	IRRECO_ENTER

	irreco_button->state = IRRECO_BSTATE_DOWN;
	if (button != NULL) {
		gtk_toggle_button_set_active(button, TRUE);
	}

	IRRECO_RETURN
}

void irreco_button_gtk_up(IrrecoButton * irreco_button)
{
	GtkToggleButton* button = GTK_TOGGLE_BUTTON(irreco_button->widget);
	IRRECO_ENTER

	irreco_button->state = IRRECO_BSTATE_UP;
	if (button != NULL) {
		gtk_toggle_button_set_active(button, FALSE);
	}

	IRRECO_RETURN
}

/*
 * Because GtkToggleButton responds to some user actions, we need some extra
 * logic to make sure that GtkToggleButton stays always in the state we want.
 */
void irreco_button_gtk_toggled_event(GtkToggleButton *togglebutton,
				     IrrecoButton * irreco_button)
{
	IRRECO_ENTER
	switch (irreco_button->state) {
		case IRRECO_BSTATE_UP:
			irreco_button_gtk_up(irreco_button);
			break;

		case IRRECO_BSTATE_FLASH:
		case IRRECO_BSTATE_DOWN:
			irreco_button_gtk_down(irreco_button);
			break;
	}

	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Image button functions.                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Image button functions
 * @{
 */

void irreco_button_img_create(IrrecoButton * irreco_button)
{
	GtkWidget* image;
	GtkWidget* event_box;
	IrrecoButtonLayout *irreco_layout = irreco_button->irreco_layout;
	gint padding;
	IRRECO_ENTER

	/* Make sure that we dont leak old widgets. */
	irreco_button_destroy_widget(irreco_button);

	/* Create up image widget. */
	image = gtk_image_new_from_file(irreco_button->style->image_up->str);
	event_box = gtk_event_box_new();
	gtk_event_box_set_visible_window(GTK_EVENT_BOX(event_box), FALSE);
	gtk_container_add(GTK_CONTAINER(event_box), image);
	irreco_button_set_motion_events(event_box);

	/* Store pointers, and set cleanup*/
	irreco_button->widget = event_box;
	g_signal_connect(G_OBJECT(irreco_button->widget), "destroy",
			 G_CALLBACK(gtk_widget_destroyed),
			 &irreco_button->widget);
	irreco_button->widget_up = image;
	g_signal_connect(G_OBJECT(irreco_button->widget_up), "destroy",
			 G_CALLBACK(gtk_widget_destroyed),
			 &irreco_button->widget_up);

	irreco_button_layout_container_put(irreco_layout, irreco_button->widget,
					   irreco_button->x, irreco_button->y);

	/* Create down image widget. */
	if (irreco_button->type == IRRECO_BTYPE_DOUBLE_IMAGE) {
		irreco_button->widget_down = gtk_image_new_from_file(
			irreco_button->style->image_down->str);
		g_signal_connect(G_OBJECT(irreco_button->widget_down), "destroy",
			 G_CALLBACK(gtk_widget_destroyed),
			 &irreco_button->widget_down);

		irreco_button_layout_container_put(irreco_layout,
						   irreco_button->widget_down,
						   irreco_button->x,
						   irreco_button->y);
		gtk_widget_hide(irreco_button->widget_down);
	}

	/* Create label. */
	padding = irreco_button->style->text_padding;
	irreco_button->widget_label = gtk_label_new(NULL);
	g_signal_connect(G_OBJECT(irreco_button->widget_label), "destroy",
			 G_CALLBACK(gtk_widget_destroyed),
			 &irreco_button->widget_label);

	gtk_misc_set_alignment(GTK_MISC(irreco_button->widget_label),
			       irreco_button->style->text_h_align,
			       irreco_button->style->text_v_align);

	irreco_button_layout_container_put(irreco_layout,
					   irreco_button->widget_label,
					   irreco_button->x + padding,
					   irreco_button->y + padding);

	irreco_button_img_set_text_format(irreco_button,
					  irreco_button->style->text_format_up->str);

	irreco_button_connect_signals(irreco_button);
	gtk_widget_show_all(irreco_button->widget);
	gtk_widget_show_all(irreco_button->widget_label);

	IRRECO_RETURN
}

void irreco_button_img_set_text_format(IrrecoButton * irreco_button,
				       gchar * format)
{
	IRRECO_ENTER

	if (irreco_button->style->allow_text == FALSE) IRRECO_RETURN;

	if (format != NULL) {
		gchar* markup;
		markup = g_markup_printf_escaped(format, irreco_button->title);
		gtk_label_set_markup(GTK_LABEL(irreco_button->widget_label),
				     markup);
		g_free(markup);
	} else {
		gtk_label_set_text(GTK_LABEL(irreco_button->widget_label),
				   irreco_button->title);
	}

	IRRECO_RETURN
}

void irreco_button_double_img_connect_signals(IrrecoButton * irreco_button)
{
	IRRECO_ENTER
	irreco_button->extra_handler_id = g_signal_connect(
		G_OBJECT(irreco_button->widget_down), "destroy",
		G_CALLBACK(irreco_button_generic_destroy_event),
		&irreco_button->widget_down);

	IRRECO_RETURN
}

void irreco_button_double_img_disconnect_signals(IrrecoButton * irreco_button)
{
	IRRECO_ENTER
	if (irreco_button->widget_down == NULL) IRRECO_RETURN
	g_signal_handler_disconnect(G_OBJECT(irreco_button->widget_down),
				    irreco_button->extra_handler_id);

	IRRECO_RETURN
}

void irreco_button_img_move(IrrecoButton * irreco_button, gint * x, gint * y)
{
	gint padding = irreco_button->style->text_padding;
	IRRECO_ENTER

	irreco_button_generic_move(irreco_button, x, y);
	irreco_button_layout_container_move(irreco_button->irreco_layout,
					    irreco_button->widget_label,
					    *x + padding, *y + padding);
	if (irreco_button->type == IRRECO_BTYPE_DOUBLE_IMAGE) {
		irreco_button_layout_container_move(irreco_button->irreco_layout,
						    irreco_button->widget_down,
						    *x, *y);
	}

	IRRECO_RETURN
}

void irreco_button_double_img_down(IrrecoButton * irreco_button)
{
	IRRECO_ENTER
	irreco_button->state = IRRECO_BSTATE_DOWN;
	if (irreco_button->widget_up != NULL) {
		gtk_widget_hide(irreco_button->widget_up);
	}
	if (irreco_button->widget_down != NULL) {
		gtk_widget_show(irreco_button->widget_down);
	}

	IRRECO_RETURN
}

void irreco_button_double_img_up(IrrecoButton * irreco_button)
{
	IRRECO_ENTER
	irreco_button->state = IRRECO_BSTATE_UP;
	if (irreco_button->widget_up != NULL) {
		gtk_widget_show(irreco_button->widget_up);
	}
	if (irreco_button->widget_down != NULL) {
		gtk_widget_hide(irreco_button->widget_down);
	}

	IRRECO_RETURN
}

/** @} */
/** @} */

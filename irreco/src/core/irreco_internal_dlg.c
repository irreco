/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007,2008  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_internal_dlg.h"

/**
 * @addtogroup IrrecoInternalDlg
 * @ingroup Irreco
 *
 * A dialog class which has IrrecoData construction property.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoInternalDlg.
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

enum
{
	PROP_0,
	PROP_IRRECO_DATA
};

typedef struct _IrrecoInternalDlgPrivate IrrecoInternalDlgPrivate;
struct _IrrecoInternalDlgPrivate {
	IrrecoData *irreco_data;
};

#define _GET_PRIVATE(_self) \
	G_TYPE_INSTANCE_GET_PRIVATE( \
		IRRECO_INTERNAL_DLG(_self), \
		IRRECO_TYPE_INTERNAL_DLG, \
		IrrecoInternalDlgPrivate);



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

G_DEFINE_TYPE(IrrecoInternalDlg, irreco_internal_dlg, IRRECO_TYPE_DLG)

static void
irreco_internal_dlg_init(IrrecoInternalDlg *object)
{
	/* TODO: Add initialization code here */
}

static void
irreco_internal_dlg_finalize(GObject *object)
{
	/* TODO: Add deinitalization code here */

	G_OBJECT_CLASS(irreco_internal_dlg_parent_class)->finalize (object);
}

static void
irreco_internal_dlg_set_property(GObject *object, guint prop_id,
				 const GValue *value, GParamSpec *pspec)
{
	IrrecoInternalDlgPrivate *priv = NULL;
	g_return_if_fail (IRRECO_IS_INTERNAL_DLG (object));

	switch (prop_id)
	{
	case PROP_IRRECO_DATA:
		priv = _GET_PRIVATE(object);
		priv->irreco_data = (IrrecoData*) g_value_get_pointer(value);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static void
irreco_internal_dlg_get_property(GObject *object, guint prop_id,
				 GValue *value, GParamSpec *pspec)
{
	IrrecoInternalDlgPrivate *priv = NULL;
	g_return_if_fail (IRRECO_IS_INTERNAL_DLG (object));

	switch (prop_id)
	{
	case PROP_IRRECO_DATA:
		priv = _GET_PRIVATE(object);
		g_value_set_pointer(value, priv->irreco_data);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static void
irreco_internal_dlg_class_init(IrrecoInternalDlgClass *klass)
{
	GObjectClass* object_class = G_OBJECT_CLASS (klass);
	/*IrrecoDlgClass* parent_class = IRRECO_DLG_CLASS (klass);*/

	object_class->finalize = irreco_internal_dlg_finalize;
	object_class->set_property = irreco_internal_dlg_set_property;
	object_class->get_property = irreco_internal_dlg_get_property;

	g_type_class_add_private(klass, sizeof (IrrecoInternalDlgPrivate));

	g_object_class_install_property(
		object_class,
		PROP_IRRECO_DATA,
		g_param_spec_pointer(
			"irreco-data",
			"irreco-data",
			"IrrecoData",
			G_PARAM_CONSTRUCT_ONLY |
			G_PARAM_WRITABLE |
			G_PARAM_READABLE));
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Public Functions                                                           */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */

IrrecoData *irreco_internal_dlg_get_irreco_data(IrrecoInternalDlg *self)
{
	IrrecoInternalDlgPrivate *priv = NULL;
	IRRECO_ENTER

	priv = _GET_PRIVATE(self);
	IRRECO_RETURN_PTR(priv->irreco_data);
}

/** @} */

/** @} */


/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007,2008  Sami Mäki (kasmra@xob.kapsi.fi)
 * Joni Kokko (t5kojo01@students.oamk.fi)
 * Harri Vattulainen (t5vaha01@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_webdb_upload_dlg.h"
#include "irreco_select_instance_dlg.h"

/* Include the prototypes for GConf client functions. */
#include <gconf/gconf-client.h>

/**
 * @addtogroup IrrecoWebdbUploadDlg
 * @ingroup Irreco
 *
 * This file builds IrrecoWebdbUploadDlg, which is used for uploading your own
 * device configurations into the Irreco Web Database.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoWebdbUploadDlg.
 */

#define APP_NAME "irreco"
#define GC_ROOT  "/apps/Maemo/" APP_NAME "/"

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

enum {
IRRECO_UPLOAD_OK
};

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

G_DEFINE_TYPE (IrrecoWebdbUploadDlg, irreco_webdb_upload_dlg, IRRECO_TYPE_DLG)

static void irreco_webdb_upload_dlg_dispose (GObject *object)
{
	if (G_OBJECT_CLASS (irreco_webdb_upload_dlg_parent_class)->dispose)
		G_OBJECT_CLASS (
		irreco_webdb_upload_dlg_parent_class)->dispose (object);
}

static void irreco_webdb_upload_dlg_finalize (GObject *object)
{
	if (G_OBJECT_CLASS (irreco_webdb_upload_dlg_parent_class)->finalize)
		G_OBJECT_CLASS (
		irreco_webdb_upload_dlg_parent_class)->finalize (object);
}

static void
irreco_webdb_upload_dlg_class_init (IrrecoWebdbUploadDlgClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->dispose = irreco_webdb_upload_dlg_dispose;
	object_class->finalize = irreco_webdb_upload_dlg_finalize;
}

static void irreco_webdb_upload_dlg_init (IrrecoWebdbUploadDlg *self)
{
	GtkWidget *table;
	GtkWidget *label_category;
	GtkWidget *label_manufacturer;
	GtkWidget *label_model;
	GtkWidget *label_device;
	GtkWidget *align;
	IRRECO_ENTER

	/* Build the dialog */
	gtk_window_set_title(GTK_WINDOW(self), _("Upload device to IrrecoDB"));
	gtk_window_set_modal(GTK_WINDOW(self), TRUE);
	gtk_window_set_destroy_with_parent(GTK_WINDOW(self), TRUE);
	gtk_dialog_set_has_separator(GTK_DIALOG(self), FALSE);
	gtk_dialog_add_buttons(GTK_DIALOG(self),
			       GTK_STOCK_OK, IRRECO_UPLOAD_OK,
			       NULL);

	table = gtk_table_new(5, 2, FALSE);

	label_device = gtk_label_new("Device: ");
	label_category = gtk_label_new("Category: ");
	label_manufacturer = gtk_label_new("Manufacturer: ");
	label_model = gtk_label_new("Model: ");
	gtk_table_attach_defaults(GTK_TABLE(table), label_device, 0, 1, 0, 1);
	gtk_table_attach_defaults(GTK_TABLE(table), label_category, 0, 1, 1, 2);
	gtk_table_attach_defaults(GTK_TABLE(table), label_manufacturer,
				  0, 1, 2, 3);
	gtk_table_attach_defaults(GTK_TABLE(table), label_model, 0, 1, 3, 4);
	gtk_misc_set_alignment(GTK_MISC(label_device), 0, 0.5);
	gtk_misc_set_alignment(GTK_MISC(label_category), 0, 0.5);
	gtk_misc_set_alignment(GTK_MISC(label_manufacturer), 0, 0.5);
	gtk_misc_set_alignment(GTK_MISC(label_model), 0, 0.5);

	self->device = gtk_label_new(NULL);
	self->categories = gtk_combo_box_entry_new_text();
	self->manufacturers = gtk_combo_box_entry_new_text();
	self->models = gtk_entry_new();

	gtk_table_attach(GTK_TABLE(table), self->device, 1, 2, 0, 1,
			 GTK_FILL, GTK_FILL, 0, 2);
	gtk_table_attach(GTK_TABLE(table), self->categories, 1, 2, 1, 2,
			 GTK_FILL, GTK_FILL, 0, 2);
	gtk_table_attach(GTK_TABLE(table), self->manufacturers, 1, 2, 2, 3,
	   		 GTK_FILL, GTK_FILL, 0, 2);
	gtk_table_attach(GTK_TABLE(table), self->models, 1, 2, 3, 4,
			 GTK_FILL, GTK_FILL, 0, 0);

	align = gtk_alignment_new(0.5, 0.5, 1, 1);
	gtk_alignment_set_padding(GTK_ALIGNMENT(align), 12, 12, 12, 12);
	gtk_container_add(GTK_CONTAINER(align), GTK_WIDGET(table));
	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(self)->vbox),
		GTK_WIDGET(align));

        gtk_widget_set_size_request(GTK_WIDGET(self), 455, -1);

	gtk_widget_show_all(GTK_WIDGET(self));

	IRRECO_RETURN
}

gboolean irreco_webdb_upload_dlg_add_configuration(IrrecoWebdbUploadDlg *self)
{
/* 	gchar *pwhash; */
	IRRECO_ENTER

	/* Check some things */
	self->category = gtk_combo_box_get_active_text(GTK_COMBO_BOX(
				self->categories));
	self->manufacturer = gtk_combo_box_get_active_text(
			GTK_COMBO_BOX( self->manufacturers));

	if(strlen(self->category) == 0) {
		irreco_error_dlg(GTK_WINDOW(self),
				"Empty category field.");
		IRRECO_RETURN_BOOL(FALSE);
	}

	if(strlen(self->manufacturer) == 0) {
		irreco_error_dlg(GTK_WINDOW(self),
				"Empty manufacturer field.");
		IRRECO_RETURN_BOOL(FALSE);
	}

	if(strlen(self->model) == 0) {
		irreco_error_dlg(GTK_WINDOW(self),
				"Empty model field.");
		IRRECO_RETURN_BOOL(FALSE);
	}

	/* Add configuration to webdb */
	if(irreco_webdb_upload_dlg_add_configuration_cache(self)) {
		IRRECO_RETURN_BOOL(TRUE);
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
}

gboolean irreco_webdb_upload_dlg_add_configuration_cache(
		IrrecoWebdbUploadDlg *self)
{
	IrrecoWebdbCache *webdb_cache = NULL;
	IRRECO_ENTER

	webdb_cache = irreco_data_get_webdb_cache(self->irreco_data, FALSE);

 	if(irreco_webdb_cache_upload_configuration(webdb_cache,
				self->backend,
				self->category, self->file_hash,
				self->file_name, self->manufacturer,
				self->model, self->password, self->user,
				self->data)) {
		IRRECO_RETURN_BOOL(TRUE);
	} else {
		irreco_error_dlg(GTK_WINDOW(self),
				irreco_webdb_cache_get_error(webdb_cache));
		IRRECO_RETURN_BOOL(FALSE);
	}
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Private Functions
 * @{
 */

IrrecoWebdbUploadDlg *irreco_webdb_upload_dlg_new (IrrecoData *irreco_data,
	GtkWindow *parent)
{
	IrrecoWebdbUploadDlg *self;
	IRRECO_ENTER

	self = g_object_new(IRRECO_TYPE_WEBDB_UPLOAD_DLG, NULL);
	irreco_dlg_set_parent(IRRECO_DLG(self), parent);
	self->irreco_data = irreco_data;

	IRRECO_RETURN_PTR(self);
}

gboolean irreco_webdb_upload_dlg_populate_category_cbentry(
		IrrecoWebdbUploadDlg *self)
{
	IrrecoStringTable *categories = NULL;
	IrrecoWebdbCache *webdb_cache = NULL;
	IRRECO_ENTER

	webdb_cache = irreco_data_get_webdb_cache(self->irreco_data, FALSE);

	/* Get categories */
	if(irreco_webdb_cache_get_all_categories(webdb_cache, &categories)) {
		IRRECO_STRING_TABLE_FOREACH_KEY(categories, key)

		gtk_combo_box_append_text(GTK_COMBO_BOX(self->categories),
				key);

		IRRECO_STRING_TABLE_FOREACH_END
		irreco_string_table_free(categories);

	} else {
		irreco_error_dlg(GTK_WINDOW(self), irreco_webdb_cache_get_error(
				 webdb_cache));
		IRRECO_RETURN_BOOL(FALSE);
	}

	IRRECO_RETURN_BOOL(TRUE);
}

gboolean irreco_webdb_upload_dlg_populate_manufacturer_cbentry(
		IrrecoWebdbUploadDlg *self)
{
	IrrecoStringTable *manufacturers = NULL;
	IrrecoWebdbCache *webdb_cache = NULL;
	IRRECO_ENTER

	webdb_cache = irreco_data_get_webdb_cache(self->irreco_data,
			FALSE);

	/* Get manufacturers */
	if(irreco_webdb_cache_get_all_manufacturers(webdb_cache,
				&manufacturers)) {

		IRRECO_STRING_TABLE_FOREACH_KEY(manufacturers, key)

			gtk_combo_box_append_text(GTK_COMBO_BOX(
						self->manufacturers),
					key);

		IRRECO_STRING_TABLE_FOREACH_END
		irreco_string_table_free(manufacturers);
	} else {
		irreco_error_dlg(GTK_WINDOW(self),
				irreco_webdb_cache_get_error(
					webdb_cache));
	}

	IRRECO_RETURN_BOOL(TRUE);
}

/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Functions                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */

gboolean irreco_show_webdb_upload_dlg(IrrecoData *irreco_data, GtkWindow *parent,
				  IrrecoBackendFileContainer *file_container,
				  const gchar *device_name,
				  gchar *user,
				  gchar *password)
{
	IrrecoWebdbUploadDlg *self;
	gint response;
	gboolean loop = TRUE;
	gboolean ret_value = FALSE;

	IRRECO_ENTER

	/* Create upload dialog */
	self = IRRECO_WEBDB_UPLOAD_DLG(
			irreco_webdb_upload_dlg_new(irreco_data, parent));

	self->user = user;
	self->password = password;

	/* Populate some ComboBoxEntries */
	if(irreco_webdb_upload_dlg_populate_category_cbentry(self)) {
		gtk_combo_box_set_active(GTK_COMBO_BOX(self->categories), 0);
		irreco_webdb_upload_dlg_populate_manufacturer_cbentry(self);
		gtk_combo_box_set_active(GTK_COMBO_BOX(self->manufacturers), 0);
	} else {
		gtk_widget_destroy(GTK_WIDGET(self));
		IRRECO_RETURN_BOOL(ret_value);
	}

	if(file_container->category->len > 0) {
		gtk_combo_box_append_text(GTK_COMBO_BOX(self->categories),
					  file_container->category->str);
	}

	if(file_container->manufacturer->len > 0) {
		gtk_combo_box_append_text(GTK_COMBO_BOX(self->manufacturers),
					  file_container->manufacturer->str);
	}

	/* Get model from TextEntry
	   Get backend, filename and filehash from file_container */
	gtk_label_set_label(GTK_LABEL(self->device), device_name);
	gtk_entry_set_text(GTK_ENTRY(self->models), file_container->model->str);
	self->model = gtk_entry_get_text(GTK_ENTRY(self->models));
	self->backend = file_container->backend->str;
	self->file_hash = file_container->hash->str;
	self->file_name = file_container->name->str;
	self->data = file_container->data->str;

	do {
		/* Show upload dlg */
		response = gtk_dialog_run(GTK_DIALOG(self));

		switch(response) {
			case GTK_RESPONSE_DELETE_EVENT:
				loop = FALSE;
				break;
			case IRRECO_UPLOAD_OK:
				if(self->user == NULL || self->password == NULL)
				if(!irreco_show_login_dlg(irreco_data,
							  parent,
							  &self->user,
							  &self->password)) {
					IRRECO_DEBUG("Failed login\n");
					break;
				}
				if(irreco_webdb_upload_dlg_add_configuration
						(self)) {
					irreco_info_dlg(GTK_WINDOW(self),
							"Upload successful");
					ret_value = TRUE;
				} else {
					break;
				}

				loop = FALSE;
				break;
			default:
				break;
		}
	} while(loop);

	gtk_widget_destroy(GTK_WIDGET(self));
	IRRECO_RETURN_BOOL(ret_value);
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Events and Callbacks                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/** @} */

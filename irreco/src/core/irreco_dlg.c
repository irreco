/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_dlg.h"

/**
 * @addtogroup IrrecoDlg
 * @ingroup Irreco
 *
 * Base class for Irreco dialogs.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoDlg.
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

#define IRRECO_DLG_CHILD_DATA_KEY "IrrecoChildDialog"

static void irreco_dlg_data_key_cleanup(IrrecoDlg *self, GtkWindow *parent);



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

G_DEFINE_TYPE(IrrecoDlg, irreco_dlg, GTK_TYPE_DIALOG)

static void irreco_dlg_finalize(GObject *object)
{
	G_OBJECT_CLASS(irreco_dlg_parent_class)->finalize(object);
}

static void irreco_dlg_class_init(IrrecoDlgClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS(klass);
	object_class->finalize = irreco_dlg_finalize;
}

static void irreco_dlg_init(IrrecoDlg *self)
{
	IRRECO_ENTER
	IRRECO_RETURN
}

GtkWidget *irreco_dlg_new(void)
{
	return g_object_new(IRRECO_TYPE_DLG, NULL);
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/





/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Public Functions                                                           */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */

/**
 * Find the current topmost window, and set it as the parent window of this
 * dialog.
 *
 * Normal Gtk windowing system stores only the parent window of dialogs. It
 * does not store which children a parent window has. Because of that, it
 * is not possible to iterate towards the topmost window from the parent windows
 * until you reach the topmost window. This is a problem because Irreco uses
 * a lot of dialogs, and dialogs may be created on somewhat random order.
 * Especially windows created by backends can cause problems if order of window
 * parentage is not properly handled.
 *
 * irreco_dlg_set_parent() works around this problem by using
 * g_object_set_data() function to store a pointer to the current child window
 * inside the parent window. This in effect creates a linked list of GtkWindows
 * which can then be iterated until the current topmost window is found.
 *
 * @param parent
 *	Any GtkWindow which has IrrecoChildDialog data key set or
 *	Irreco main window.
 */
void irreco_dlg_set_parent(IrrecoDlg *self, GtkWindow *parent)
{
	gint depth = 0;
	gpointer data;
	IRRECO_ENTER

	if (gtk_window_get_transient_for(GTK_WINDOW(self))) {
		IRRECO_PRINTF("Dialog is already tracient for something.\n");
		IRRECO_RETURN
	}

	/* Iterate trough child windows, until we find the one that is
	   topmost on screen. */
	do {
		data = g_object_get_data(G_OBJECT(parent),
					 IRRECO_DLG_CHILD_DATA_KEY);
		if (data != NULL && GTK_IS_WINDOW(data)) {
			parent = GTK_WINDOW(data);
		} else {
			break;
		}
		depth++;
	} while (TRUE);

	IRRECO_PRINTF("Number of parent dialogs on screen: %i\n", depth);

	/* Set self as a child window. */
	gtk_window_set_transient_for(GTK_WINDOW(self), parent);
	g_object_set_data(G_OBJECT(parent), IRRECO_DLG_CHILD_DATA_KEY, self);

	/* Set up cleanup event. */
	g_signal_connect(G_OBJECT(self), "destroy",
			 G_CALLBACK(irreco_dlg_data_key_cleanup), parent);
	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Events and Callbacks                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Events and Callbacks
 * @{
 */

static void irreco_dlg_data_key_cleanup(IrrecoDlg *self, GtkWindow *parent)
{
	gpointer data;
	IRRECO_ENTER

	if (G_IS_OBJECT(parent) == FALSE) IRRECO_RETURN;

	/* Does parent have a pointer which points to this instace? */
	data = g_object_get_data(G_OBJECT(parent), IRRECO_DLG_CHILD_DATA_KEY);
	if (data == NULL || data != self) IRRECO_RETURN;

	/* Remote the pointer. */
	g_object_steal_data(G_OBJECT(parent), IRRECO_DLG_CHILD_DATA_KEY);
	IRRECO_RETURN
}

/** @} */

/** @} */

































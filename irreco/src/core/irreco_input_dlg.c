/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_input_dlg.h"

/**
 * @addtogroup IrrecoInputDlg
 * @ingroup Irreco
 *
 * A simple dialog where the user can type input.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoInputDlg.
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

G_DEFINE_TYPE(IrrecoInputDlg, irreco_input_dlg, IRRECO_TYPE_DLG)

static void irreco_input_dlg_finalize(GObject *object)
{
	G_OBJECT_CLASS(irreco_input_dlg_parent_class)->finalize(object);
}

static void irreco_input_dlg_class_init(IrrecoInputDlgClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS(klass);
	object_class->finalize = irreco_input_dlg_finalize;
}

static void irreco_input_dlg_init(IrrecoInputDlg *self)
{
	GtkWidget *padding;
	IRRECO_ENTER

	/* Construct dialog. */
	gtk_window_set_title(GTK_WINDOW(self), _("Input dialog"));
	gtk_window_set_modal(GTK_WINDOW(self), TRUE);
	gtk_window_set_destroy_with_parent(GTK_WINDOW(self), TRUE);
	gtk_dialog_set_has_separator(GTK_DIALOG(self), FALSE);
	gtk_dialog_add_buttons(GTK_DIALOG(self),
/*				_("Cancel"), GTK_RESPONSE_NO,*/
			       GTK_STOCK_OK, GTK_RESPONSE_OK,
			       NULL);

	/* Add widgets. */
	self->hbox = gtk_hbox_new(FALSE, 0);
	self->label = gtk_label_new(NULL);
	self->entry = gtk_entry_new();
	gtk_container_add(GTK_CONTAINER(self->hbox), self->label);
	gtk_container_add(GTK_CONTAINER(self->hbox), self->entry);
	padding = irreco_gtk_pad(self->hbox, 8, 8, 8, 8);
	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(self)->vbox), padding);
	IRRECO_RETURN
}

GtkWidget *irreco_input_dlg_new(GtkWindow *parent, const gchar *window_title)
{
	IrrecoInputDlg *self;
	IRRECO_ENTER

	self = g_object_new(IRRECO_TYPE_INPUT_DLG, NULL);
	irreco_dlg_set_parent(IRRECO_DLG(self), parent);
	gtk_window_set_title(GTK_WINDOW(self), window_title);
	IRRECO_RETURN_PTR(self);
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Public Functions                                                           */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */

void irreco_input_dlg_set_ok_button_text(IrrecoInputDlg *self,
					 const gchar *text)
{
	GtkWidget *button;
	IRRECO_ENTER

	button = irreco_gtk_dialog_get_button(GTK_WIDGET(self), 1);
	if (text == NULL) {
		gtk_button_set_label(GTK_BUTTON(button), GTK_STOCK_OK);
	} else {
		gtk_button_set_label(GTK_BUTTON(button), text);
	}
	IRRECO_RETURN
}

void irreco_input_dlg_set_label(IrrecoInputDlg *self, const gchar *text)
{
	IRRECO_ENTER
	if (text == NULL) {
		gtk_box_set_spacing(GTK_BOX(self->hbox), 0);
		gtk_label_set_text(GTK_LABEL(self->label), NULL);
	} else {
		gtk_box_set_spacing(GTK_BOX(self->hbox), 8);
		gtk_label_set_text(GTK_LABEL(self->label), text);
	}
	IRRECO_RETURN
}

void irreco_input_dlg_set_entry(IrrecoInputDlg *self, const gchar *text)
{
	IRRECO_ENTER
	gtk_entry_set_text(GTK_ENTRY(self->entry), text);
	IRRECO_RETURN
}

const gchar *irreco_input_dlg_get_entry(IrrecoInputDlg *self)
{
	IRRECO_ENTER
	IRRECO_RETURN_CONST_STR(gtk_entry_get_text(GTK_ENTRY(self->entry)))
}

gboolean irreco_show_input_dlg(IrrecoInputDlg *self)
{
	const gchar *input;
	IRRECO_ENTER

	gtk_widget_show_all(GTK_WIDGET(self));
	while (gtk_dialog_run(GTK_DIALOG(self)) == GTK_RESPONSE_OK)
	{
		input = irreco_input_dlg_get_entry(IRRECO_INPUT_DLG(self));
		if (g_utf8_strlen(input, -1) < 1) {
			irreco_error_dlg(GTK_WINDOW(self),
				         _("Empty input is not valid. "
					 "Type something, or press cancel."));
		} else {
			IRRECO_RETURN_BOOL(TRUE);
		}
	}
	IRRECO_RETURN_BOOL(FALSE);
}

/** @} */

/** @} */



















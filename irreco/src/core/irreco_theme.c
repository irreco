/*
 * irreco - Ir Remote Control
 * Copyright (C) 2008 Joni Kokko (t5kojo01@students.oamk.fi)
 *		      Pekka Gehör (pegu6@msn.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_theme.h"

/**
 * @addtogroup IrrecoTheme
 * @ingroup Irreco
 *
 * Contains information of theme.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoTheme.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
void irreco_theme_read_button_keyfile_foreach(IrrecoDirForeachData * dir_data);
void irreco_theme_read_bg_keyfile_foreach(IrrecoDirForeachData * dir_data);
void irreco_theme_read(IrrecoTheme *self, const gchar *dir);

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

/**
 * Create new theme
 */
IrrecoTheme *irreco_theme_new()
{
	IrrecoTheme *self;
	IRRECO_ENTER

	self = g_slice_new0(IrrecoTheme);

	self->name = g_string_new(NULL);
	self->path = g_string_new(NULL);
	self->source = g_string_new(NULL);
	self->author = g_string_new(NULL);
	self->comment = g_string_new(NULL);
	self->preview_button_name = g_string_new(NULL);
	self->version = g_string_new(NULL);
	self->backgrounds =  irreco_string_table_new(
				(GDestroyNotify)irreco_theme_bg_free, NULL);
	self->buttons =  irreco_string_table_new(
				(GDestroyNotify)irreco_theme_button_free, NULL);

	IRRECO_RETURN_PTR(self);
}

void irreco_theme_free(IrrecoTheme *self)
{
	IRRECO_ENTER

	g_assert(self != NULL);

	g_string_free(self->name, TRUE);
	self->name = NULL;

	g_string_free(self->path, TRUE);
	self->path = NULL;

	g_string_free(self->source, TRUE);
	self->source = NULL;

	g_string_free(self->author, TRUE);
	self->author = NULL;

	g_string_free(self->comment, TRUE);
	self->comment = NULL;

	g_string_free(self->preview_button_name, TRUE);
	self->preview_button_name = NULL;

	irreco_string_table_free(self->backgrounds);
	self->backgrounds = NULL;

	irreco_string_table_free(self->buttons);
	self->buttons = NULL;

	g_slice_free(IrrecoTheme, self);
	self = NULL;

	IRRECO_RETURN
}

/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Private Functions
 * @{
 */


/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Public Functions                                                           */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */
void irreco_theme_update_keyfile(IrrecoTheme *self)
{
	GString *keyfile_path = g_string_new(self->path->str);
	GKeyFile *keyfile = g_key_file_new();
	IRRECO_ENTER

	g_string_append(keyfile_path, "/theme.conf");

	irreco_gkeyfile_set_string(keyfile, "theme" , "name",
				   self->name->str);

	if (self->source->len > 0) {
		irreco_gkeyfile_set_string(keyfile, "theme" , "source",
					   self->source->str);
	}

	if (self->author->len > 0) {
		irreco_gkeyfile_set_string(keyfile, "theme", "author",
					   self->author->str);
	}

	if (self->preview_button_name->len > 0) {
		irreco_gkeyfile_set_string(keyfile, "theme", "preview-button",
					   self->preview_button_name->str);
	}

	if (self->version->len > 0) {
		irreco_gkeyfile_set_string(keyfile, "theme", "version",
					   self->version->str);
	}

	if (self->comment->len > 0) {
		irreco_gkeyfile_set_string(keyfile, "theme", "comment",
					   self->comment->str);
	}

	irreco_write_keyfile(keyfile, keyfile_path->str);

	g_key_file_free(keyfile);
	g_string_free(keyfile_path, TRUE);
	IRRECO_RETURN
}

void irreco_theme_print(IrrecoTheme *self)
{
	IRRECO_ENTER

	IRRECO_DEBUG("Themename: %s \n", self->name->str);
	IRRECO_DEBUG("Folder: %s \n", self->path->str);
	IRRECO_DEBUG("Source: %s \n", self->source->str);
	IRRECO_DEBUG("Author: %s \n", self->author->str);
	IRRECO_DEBUG("Comment: %s \n", self->comment->str);
	IRRECO_DEBUG("Previewbutton: %s \n", self->preview_button_name->str);
	IRRECO_DEBUG("Version: %s \n", self->version->str);
	irreco_string_table_print(self->backgrounds);
	irreco_string_table_print(self->buttons);

	IRRECO_RETURN
}

void irreco_theme_read_button_keyfile_foreach(IrrecoDirForeachData * dir_data)
{
	IrrecoTheme		*self = (IrrecoTheme*) dir_data->user_data_1;
	IrrecoThemeButton	*button = NULL;
	IrrecoThemeButton	*old_button = NULL;
	IRRECO_ENTER

	button = irreco_theme_button_new_from_dir(dir_data->directory,
						  self->name->str);

	if (irreco_string_table_exists(self->buttons, button->name->str)) {
		IRRECO_ERROR("Error: Button %s has already been read. "
			     "You cannot have two buttons with the same name.\n",
			     button->name->str);
		irreco_string_table_get(self->buttons, button->name->str,
					(gpointer*) &old_button);
		irreco_theme_button_read(old_button, dir_data->filepath);
		irreco_theme_button_free(button);
	} else {
		irreco_string_table_add(self->buttons,
					button->style_name->str, button);
	}

	IRRECO_RETURN
}

void irreco_theme_read_bg_keyfile_foreach(IrrecoDirForeachData * dir_data)
{
	IrrecoTheme	*self = (IrrecoTheme*) dir_data->user_data_1;
	IrrecoThemeBg	*bg = NULL;
	IrrecoThemeBg	*old_bg = NULL;
	IRRECO_ENTER

	bg = irreco_theme_bg_new_from_dir(dir_data->directory);

	if (irreco_string_table_exists(self->backgrounds, bg->image_name->str)) {
		IRRECO_ERROR("Error: Background %s has already been read. "
			     "You cannot have two backgrounds with the same name.\n",
			     bg->image_name->str);
		irreco_string_table_get(self->backgrounds, bg->image_name->str,
					(gpointer*) &old_bg);
		irreco_theme_bg_read(old_bg, dir_data->filepath);
		irreco_theme_bg_free(bg);
	} else {
		irreco_string_table_add(self->backgrounds,
					bg->image_name->str, bg);
	}
	IRRECO_RETURN
}

IrrecoStringTable* irreco_theme_get_buttons(IrrecoTheme *self)
{
	IRRECO_ENTER
	IRRECO_RETURN_PTR(self->buttons);
}

IrrecoThemeButton *irreco_theme_get_button(IrrecoTheme *self,
					   const char *button_name)
{
	IrrecoThemeButton *button = NULL;
	IRRECO_ENTER
	IRRECO_STRING_TABLE_FOREACH_DATA(self->buttons, IrrecoThemeButton *,
					 pointer)
		if (g_utf8_collate(pointer->name->str, button_name) == 0) {
			button = pointer;
		}
	IRRECO_STRING_TABLE_FOREACH_END
	IRRECO_RETURN_PTR(button);
}

IrrecoStringTable* irreco_theme_get_backgrounds(IrrecoTheme *self)
{
	IRRECO_ENTER
	IRRECO_RETURN_PTR(self->backgrounds);
}

IrrecoThemeBg *irreco_theme_get_background(IrrecoTheme *self,
					   const char *bg_name)
{
	IrrecoThemeBg *bg = NULL;
	IRRECO_ENTER
	IRRECO_STRING_TABLE_FOREACH_DATA(self->backgrounds, IrrecoThemeBg *,
					 pointer)
		if (g_utf8_collate(pointer->image_name->str, bg_name) == 0) {
			bg = pointer;
		}
	IRRECO_STRING_TABLE_FOREACH_END
	IRRECO_RETURN_PTR(bg);
}

void irreco_theme_set_author(IrrecoTheme *self, const char * author)
{
	IRRECO_ENTER
	if (author != NULL) {
		g_string_printf(self->author, "%s", author);

		irreco_theme_update_keyfile(self);
	}
	IRRECO_RETURN
}

void irreco_theme_set_comment(IrrecoTheme *self, const char * comment)
{
	IRRECO_ENTER
	if (comment != NULL) {
		g_string_printf(self->comment, "%s", comment);

		irreco_theme_update_keyfile(self);
	}
	IRRECO_RETURN
}

void irreco_theme_set_preview_button(IrrecoTheme *self,
				     const char * button_name)
{
	IRRECO_ENTER
	if (button_name != NULL) {
		g_string_printf(self->preview_button_name, "%s", button_name);

		irreco_theme_update_keyfile(self);
	}
	IRRECO_RETURN
}


#if 0
/* This function will work after IrrecoButtonStyle destruction*/

void irreco_theme_set_name(IrrecoTheme *self, IrrecoData *irreco_data,
			   const char * name)
{
	IRRECO_ENTER
	if (name != NULL) {
		GString	*style_name = g_string_new("");

		/*
		 *TODO Move this part to IrrecoThemeManager and call this
		 *funtion from ThemeManager*/
		{
		irreco_string_table_change_key(
					irreco_data->theme_manager->themes,
					self->name->str, name);
		}
		g_string_printf(self->name, "%s", name);

		IRRECO_STRING_TABLE_FOREACH_DATA(self->buttons,
						 IrrecoThemeButton *, button) {
			g_string_printf(button->style_name,"%s/%s",
					self->name->str,
					button->name->str);
			IRRECO_PRINTF("style: %s\n",button->style_name->str);
			IRRECO_PAUSE
		}
		IRRECO_STRING_TABLE_FOREACH_END

		irreco_theme_update_keyfile(self);
		irreco_config_save_layouts(irreco_data);
		g_string_free(style_name, TRUE);
	}
	IRRECO_RETURN
}
#endif

void irreco_theme_set(IrrecoTheme *self, const char *name, const char *path,
		      const char *source, const char *author,
		      const char *comment, const char *preview_button_name,
		      const char *version)
{
	IRRECO_ENTER

	if (name != NULL) {
		g_string_printf(self->name, "%s", name);
	} else {
		g_string_erase(self->name, 0, -1);
	}

	if (path != NULL) {
		g_string_printf(self->path, "%s", path);
	} else {
		g_string_erase(self->path, 0, -1);
	}

	if (source != NULL) {
		g_string_printf(self->source, "%s", source);
	} else {
		g_string_erase(self->source, 0, -1);
	}

	if (author != NULL) {
		g_string_printf(self->author, "%s", author);
	} else {
		g_string_erase(self->author, 0, -1);
	}

	if (comment != NULL) {
		g_string_printf(self->comment, "%s", comment);
	} else {
		g_string_erase(self->comment, 0, -1);
	}

	if (preview_button_name != NULL) {
		g_string_printf(self->preview_button_name, "%s",
				preview_button_name);
	} else {
		g_string_erase(self->preview_button_name, 0, -1);
	}

	if (version != NULL) {
		g_string_printf(self->version, "%s", version);
	} else {
		g_string_erase(self->version, 0, -1);
	}

	irreco_theme_update_keyfile(self);

	/* Update buttons */
	{
		IrrecoDirForeachData button_styles;
		GString * directory = g_string_new("");

		g_string_printf(directory, "%s/buttons/", path);
		IRRECO_DEBUG("Directory = %s\n", directory->str);
		button_styles.directory = directory->str;

		button_styles.filesuffix = "button.conf";
		button_styles.user_data_1 = self;

		irreco_dir_foreach_subdirectories(&button_styles,
				irreco_theme_read_button_keyfile_foreach);

		g_string_free(directory, TRUE);
		directory = NULL;

		irreco_string_table_sort_abc(self->buttons);
	}

	/* Update backgrounds */
	{
		IrrecoDirForeachData bg_styles;
		GString * directory = g_string_new("");

		g_string_printf(directory, "%s/bg/", path);
		IRRECO_DEBUG("Directory = %s\n", directory->str);
		bg_styles.directory = directory->str;

		bg_styles.filesuffix = "bg.conf";
		bg_styles.user_data_1 = self;

		irreco_dir_foreach_subdirectories(&bg_styles,
				irreco_theme_read_bg_keyfile_foreach);

		g_string_free(directory, TRUE);
		directory = NULL;

		irreco_string_table_sort_abc(self->backgrounds);
	}

	IRRECO_RETURN
}

void irreco_theme_check(IrrecoTheme *self)
{
	IRRECO_ENTER

	/* Check if some background is deleted */
	IRRECO_STRING_TABLE_FOREACH(self->backgrounds, key, IrrecoThemeBg *,
				    theme_bg)
		if(!irreco_is_file(theme_bg->image_path->str)) {
			irreco_string_table_remove(self->backgrounds, key);
		}
	IRRECO_STRING_TABLE_FOREACH_END

	/* Check if some button is deleted */
	IRRECO_STRING_TABLE_FOREACH(self->buttons, key, IrrecoThemeButton *,
				    theme_button)
		if(!irreco_is_file(theme_button->image_up->str)) {
			irreco_string_table_remove(self->buttons, key);
		}
	IRRECO_STRING_TABLE_FOREACH_END

	IRRECO_RETURN
}

IrrecoTheme *irreco_theme_copy(IrrecoTheme *self)
{
	IrrecoTheme *new = NULL;

	IRRECO_ENTER

	new = irreco_theme_new();

	irreco_theme_set(new, self->name->str, self->path->str,
		      self->source->str, self->author->str,
		      self->comment->str, self->preview_button_name->str,
		      self->version->str);


	IRRECO_STRING_TABLE_FOREACH(self->backgrounds, key, IrrecoThemeBg *,
				    theme_bg)
		irreco_string_table_add(new->backgrounds, key,
					irreco_theme_bg_copy(theme_bg));

	IRRECO_STRING_TABLE_FOREACH_END


	IRRECO_STRING_TABLE_FOREACH(self->buttons, key, IrrecoThemeButton *,
				    theme_button)
		irreco_string_table_add(new->buttons, key,
					irreco_theme_button_copy(theme_button));

	IRRECO_STRING_TABLE_FOREACH_END


	IRRECO_RETURN_PTR(new);
}

/**
 * IrrecoTheme new from dir
 */

IrrecoTheme *irreco_theme_new_from_dir(const gchar *dir)
{
	IrrecoTheme *self = NULL;

	IRRECO_ENTER
	self = irreco_theme_new();
	irreco_theme_read(self, dir);
	IRRECO_RETURN_PTR(self);
}

void irreco_theme_read(IrrecoTheme *self, const gchar *dir)
{
	IrrecoKeyFile		*keyfile = NULL;
	char *name		= NULL;
	char *source 		= NULL;
	char *author		= NULL;
	char *comment		= NULL;
	char *preview_button	= NULL;
	char *version		= NULL;
	GString	*conf		= NULL;
	IRRECO_ENTER

	conf = g_string_new(dir);
	g_string_append_printf(conf, "/theme.conf");
	keyfile = irreco_keyfile_create(dir,
					conf->str,
					"theme");
	if (keyfile == NULL) goto end;

	/* Required fields. */
	if (!irreco_keyfile_get_str(keyfile, "name", &name)) {
		goto end;
	}

	/* Optional fields. */
	irreco_keyfile_get_str(keyfile, "source", &source);
	irreco_keyfile_get_str(keyfile, "author", &author);
	irreco_keyfile_get_str(keyfile, "comment", &comment);
	irreco_keyfile_get_str(keyfile, "preview-button", &preview_button);
	irreco_keyfile_get_str(keyfile, "version", &version);

	/* call irreco_theme_set() */
	irreco_theme_set(self, name, dir, source,
			 author, comment, preview_button, version);

	end:
	g_string_free(conf, TRUE);
	if (keyfile != NULL) irreco_keyfile_destroy(keyfile);
	if (name != NULL) g_free(name);
	if (source != NULL) g_free(source);
	if (author != NULL) g_free(author);
	if (comment != NULL) g_free(comment);
	if (preview_button != NULL) g_free(preview_button);
	if (version != NULL) g_free(version);
	IRRECO_RETURN

}

gboolean irreco_theme_save(IrrecoTheme *self,
			   const gchar *theme_path)
{
	gboolean		rvalue = FALSE;
	IrrecoTheme		*theme;
	GString			*path;
	IrrecoStringTable	*bg_list = NULL;
	IrrecoStringTable	*button_list = NULL;
	IRRECO_ENTER

	/*Create new theme*/
	theme = irreco_theme_new();
	irreco_theme_set(theme,
				self->name->str,
				theme_path,
    				self->source->str,
				self->author->str,
				self->comment->str,
				self->preview_button_name->str,
				NULL);

	irreco_theme_update_keyfile(theme);


	/* Get buttons and backgrounds */
	/* Get backrounds */
	bg_list = irreco_theme_get_backgrounds(self);

	path = g_string_new("");
	g_string_printf(path, "%s/bg", theme_path);
	g_mkdir(path->str, 0777);

	IRRECO_STRING_TABLE_FOREACH_DATA(bg_list, IrrecoThemeBg *, background)

			irreco_theme_bg_print(background);

			irreco_theme_bg_save(background, path->str);


	IRRECO_STRING_TABLE_FOREACH_END

	/* Get buttons */
	button_list = irreco_theme_get_buttons(self);

	g_string_printf(path, "%s/buttons", theme_path);
	g_mkdir(path->str, 0777);

	IRRECO_STRING_TABLE_FOREACH_DATA(button_list, IrrecoThemeButton *, button)

			if (g_str_equal(self->preview_button_name->str,
			    button->image_up->str) && !rvalue) {
				irreco_theme_set_preview_button(theme,
							button->name->str);
				rvalue = TRUE;

			}
			irreco_theme_button_save(button, path->str);
			irreco_theme_button_print(button);

	IRRECO_STRING_TABLE_FOREACH_END



	g_string_free(path, TRUE);
	irreco_theme_free(theme);

	IRRECO_RETURN_BOOL(rvalue);

}

/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Events and Callbacks                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/** @} */

/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoCmdChainEditor
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoCmdChainEditor.
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __HEADER_NAME_H_TYPEDEF__
#define __HEADER_NAME_H_TYPEDEF__

typedef struct _IrrecoCmdChainEditor IrrecoCmdChainEditor;
typedef struct _IrrecoCmdChainEditorClass IrrecoCmdChainEditorClass;

#define IRRECO_TYPE_CMD_CHAIN_EDITOR irreco_cmd_chain_editor_get_type()

#define IRRECO_CMD_CHAIN_EDITOR(obj) \
	(G_TYPE_CHECK_INSTANCE_CAST ((obj), \
	IRRECO_TYPE_CMD_CHAIN_EDITOR, IrrecoCmdChainEditor))

#define IRRECO_CMD_CHAIN_EDITOR_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_CAST ((klass), \
	IRRECO_TYPE_CMD_CHAIN_EDITOR, IrrecoCmdChainEditorClass))

#define IRRECO_IS_CMD_CHAIN_EDITOR(obj) \
	(G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
	IRRECO_TYPE_CMD_CHAIN_EDITOR))

#define IRRECO_IS_CMD_CHAIN_EDITOR_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_TYPE ((klass), \
	IRRECO_TYPE_CMD_CHAIN_EDITOR))

#define IRRECO_CMD_CHAIN_EDITOR_GET_CLASS(obj) \
	(G_TYPE_INSTANCE_GET_CLASS ((obj), \
	IRRECO_TYPE_CMD_CHAIN_EDITOR, IrrecoCmdChainEditorClass))


typedef enum {
	/* Changes will take effect only if
	   irreco_cmd_chain_editor_apply_changes() is called. */
	IRRECO_INDIRECT_EDITING = 0,

	/* Changes affect the command chain immediately. */
	IRRECO_DIRECT_EDITING

} IrrecoEditMode;

#endif /* __HEADER_NAME_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __HEADER_NAME_H__
#define __HEADER_NAME_H__
#include "irreco.h"
#include "irreco_internal_widget.h"
#include "irreco_listbox_text.h"
#include "irreco_cmd_chain.h"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
struct _IrrecoCmdChainEditor{
	IrrecoInternalWidget parent;

	GtkWindow 	*parent_window;

	GtkWidget	*listbox;
	GtkWidget	*hbox;
	GtkWidget	*add;
	GtkWidget	*remove;
	GtkWidget	*up;
	GtkWidget	*down;
	GtkWidget	*properties;

	IrrecoEditMode	 edit_mode;
	IrrecoCmdChain	*old_cmd_chain;
	IrrecoCmdChain	*new_cmd_chain;
};

struct _IrrecoCmdChainEditorClass{
	IrrecoInternalWidgetClass parent_class;
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
GType		irreco_cmd_chain_editor_get_type(void);
GtkWidget* irreco_cmd_chain_editor_new		(IrrecoData *irreco_data,
				       		 GtkWindow *parent);
void irreco_cmd_chain_editor_set_parent_window(IrrecoCmdChainEditor *self,
					       GtkWindow *parent);
void	irreco_cmd_chain_editor_set_autosize	(IrrecoCmdChainEditor *self,
						 gint min_width,
						 gint max_width,
						 gint min_height,
						 gint max_height);
void	irreco_cmd_chain_editor_set_edit_mode	(IrrecoCmdChainEditor *self,
						 IrrecoEditMode	edit_mode);
void	irreco_cmd_chain_editor_set_chain	(IrrecoCmdChainEditor *self,
						 IrrecoCmdChain *cmd_chain);
IrrecoCmdChain *irreco_cmd_chain_editor_get_chain(IrrecoCmdChainEditor *self);
void	irreco_cmd_chain_editor_apply_changes	(IrrecoCmdChainEditor *self);
void	irreco_cmd_chain_editor_reset_changes	(IrrecoCmdChainEditor *self);
void	irreco_cmd_chain_editor_clear_chain	(IrrecoCmdChainEditor *self);


#endif /* __HEADER_NAME_H__ */

/** @} */


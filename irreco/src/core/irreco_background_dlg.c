/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *		 2008  Joni Kokko (t5kojo01@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_background_dlg.h"
#include <hildon/hildon-color-button.h>
#include <hildon/hildon-file-chooser-dialog.h>


/**
 * @addtogroup IrrecoBackgroundDlg
 * @ingroup Irreco
 *
 * Allow user to setup layout background.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoBackgroundDlg.
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

#define IRRECO_BACKGROUND_PREVIEW_WIDHT		(IRRECO_SCREEN_WIDTH / 6)
#define IRRECO_BACKGROUND_PREVIEW_HEIGHT	(IRRECO_SCREEN_HEIGHT / 6)



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

static gboolean _draw_preview_custom(IrrecoBackgroundDlg *self,
				     const gchar *image,
				     const GdkColor *bg_color);
static void _signal_color_clicked(GtkColorButton *widget,
				  IrrecoBackgroundDlg *self);
static void _signal_type_toggled(GtkToggleButton *togglebutton,
				 IrrecoBackgroundDlg *self);
static void _signal_image_clicked(gpointer *ignore,
				  IrrecoBackgroundDlg *self);
static void _signal_preview_clicked(gpointer *ignore1,
				    gpointer *ignore2,
				    IrrecoBackgroundDlg *self);
static void _signal_theme_image_selection_changed(GtkTreeSelection *selection,
						  IrrecoBackgroundDlg *self);



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

G_DEFINE_TYPE(IrrecoBackgroundDlg, irreco_background_dlg,
	      IRRECO_TYPE_INTERNAL_DLG)

static void irreco_background_dlg_constructed(GObject *object)
{
	IrrecoBackgroundDlg *self;

	GtkWidget *vbox;
	GtkWidget *hbox;
	GtkWidget *vbox_left;
	GtkWidget *vbox_right;
	GtkTable *table_right;

	GtkWidget *label_left;
	GtkWidget *label_right;
	GtkWidget *label_color;
	GtkWidget *label_image;

	GtkWidget *radio_default_a;
	GtkWidget *radio_color_a;
	GtkWidget *radio_image_a;

	GtkWidget *button_image;
	GtkWidget *preview_frame;
	GtkWidget *preview_event_box;
	IRRECO_ENTER

	G_OBJECT_CLASS(irreco_background_dlg_parent_class)->constructed(object);
	self = IRRECO_BACKGROUND_DLG(object);

	/* Construct dialog. */
	gtk_window_set_title(GTK_WINDOW(self), _("Background"));
	gtk_window_set_modal(GTK_WINDOW(self), TRUE);
	gtk_window_set_destroy_with_parent(GTK_WINDOW(self), TRUE);
	gtk_dialog_set_has_separator(GTK_DIALOG(self), FALSE);
	gtk_dialog_add_buttons(GTK_DIALOG(self),
			       GTK_STOCK_OK, GTK_RESPONSE_OK,
			       NULL);

	/* Create widgets. */
	vbox = gtk_vbox_new(0, 0);
	hbox = gtk_hbox_new(0, 12);
	vbox_left = gtk_vbox_new(0, 0);
	vbox_right = gtk_vbox_new(0, 0);
	self->notebook = gtk_notebook_new();
	label_left = irreco_gtk_label_bold(_("Background"), 0, 0, 0, 6, 0, 0);
	label_right = irreco_gtk_label_bold(_("Select"), 0, 0, 0, 6, 0, 0);
	label_color = irreco_gtk_label(_("Color"), 0, 0.5, 0, 0, 12, 0);
	label_image = irreco_gtk_label(_("Image"), 0, 0.5, 0, 0, 12, 0);

	/* Create Background Browser tab. */
	self->theme_bg_browser = irreco_bg_browser_widget_new(
		irreco_internal_dlg_get_irreco_data(IRRECO_INTERNAL_DLG(self)));
	gtk_notebook_append_page(GTK_NOTEBOOK(self->notebook),
				 irreco_gtk_align(
					 GTK_WIDGET(self->theme_bg_browser),
					 0, 0, 1, 1, 8, 8, 8, 8),
				 gtk_label_new("Theme"));

	/* Create custom tab. */
	gtk_notebook_append_page(GTK_NOTEBOOK(self->notebook),
				 vbox, gtk_label_new("Custom"));
	gtk_box_pack_start_defaults(GTK_BOX(GTK_DIALOG(self)->vbox),
				    self->notebook);

	self->radio_default =
		gtk_radio_button_new_with_label(
			NULL, _("Default image"));
	self->radio_color =
		gtk_radio_button_new_with_label_from_widget(
			GTK_RADIO_BUTTON(self->radio_default),
			_("Color only"));
	self->radio_image =
		gtk_radio_button_new_with_label_from_widget(
			GTK_RADIO_BUTTON(self->radio_color),
			_("Color and Image"));
	radio_default_a = irreco_gtk_pad(self->radio_default,
					 0, 0, 12, 0);
	radio_color_a = irreco_gtk_pad(self->radio_color,
				       0, 0, 12, 0);
	radio_image_a = irreco_gtk_pad(self->radio_image,
				       0, 0, 12, 0);

	self->color_button = hildon_color_button_new();
	button_image = gtk_button_new_with_label(_("Select"));

	preview_frame = gtk_frame_new(NULL);
	preview_event_box = gtk_event_box_new();
	self->preview = gtk_drawing_area_new();
	gtk_drawing_area_size(GTK_DRAWING_AREA(self->preview),
			      IRRECO_BACKGROUND_PREVIEW_WIDHT,
			      IRRECO_BACKGROUND_PREVIEW_HEIGHT);

	/* Build dialog. */
	table_right = GTK_TABLE(gtk_table_new(2, 2, FALSE));
	gtk_table_set_row_spacings(table_right, 6);
	gtk_table_set_col_spacings(table_right, 12);
	gtk_table_attach_defaults(table_right, label_color, 0, 1, 0, 1);
	gtk_table_attach_defaults(table_right, label_image, 0, 1, 1, 2);
	gtk_table_attach_defaults(table_right, self->color_button,
				  1, 2, 0, 1);
	gtk_table_attach_defaults(table_right, button_image, 1, 2, 1, 2);

	gtk_box_pack_start_defaults(GTK_BOX(vbox),
				    irreco_gtk_pad(hbox, 12, 0, 12, 12));
	gtk_box_pack_start_defaults(GTK_BOX(hbox), vbox_left);
	gtk_box_pack_start_defaults(GTK_BOX(hbox), vbox_right);
	gtk_box_pack_start(GTK_BOX(vbox_left), label_left, 0, 0, 0);
	gtk_box_pack_start(GTK_BOX(vbox_left), radio_default_a, 0, 0, 0);
	gtk_box_pack_start(GTK_BOX(vbox_left), radio_color_a, 0, 0, 0);
	gtk_box_pack_start(GTK_BOX(vbox_left), radio_image_a, 0, 0, 0);
	gtk_box_pack_start(GTK_BOX(vbox_right), label_right, 0, 0, 0);
	gtk_box_pack_start(GTK_BOX(vbox_right), GTK_WIDGET(table_right),
			   0, 0, 0);

	gtk_container_add(GTK_CONTAINER(preview_event_box), preview_frame);
	gtk_container_add(GTK_CONTAINER(preview_frame),self->preview);
	gtk_box_pack_start_defaults(GTK_BOX(vbox), irreco_gtk_align(
				    preview_event_box,
				    0.5, 0.5, 0, 0, 12, 0, 0, 0));

	/* Radio button signals. */
	g_signal_connect(G_OBJECT(self->radio_default), "toggled",
			 G_CALLBACK(_signal_type_toggled),
			 self);
	g_object_set_data(G_OBJECT(self->radio_default),
			  "IrrecoButtonLayoutBgType",
			  (gpointer) IRRECO_BACKGROUND_DEFAULT);
	g_signal_connect(G_OBJECT(self->radio_color), "toggled",
			 G_CALLBACK(_signal_type_toggled),
			 self);
	g_object_set_data(G_OBJECT(self->radio_color),
			  "IrrecoButtonLayoutBgType",
			  (gpointer) IRRECO_BACKGROUND_COLOR);
	g_signal_connect(G_OBJECT(self->radio_image), "toggled",
			 G_CALLBACK(_signal_type_toggled),
			 self);
	g_object_set_data(G_OBJECT(self->radio_image),
			  "IrrecoButtonLayoutBgType",
			  (gpointer) IRRECO_BACKGROUND_IMAGE);

	/* Button signals. */
	g_signal_connect(G_OBJECT(button_image), "clicked",
			 G_CALLBACK(_signal_image_clicked),
			 self);
	g_signal_connect(G_OBJECT(self->color_button), "clicked",
			 G_CALLBACK(_signal_color_clicked),
			 self);
	g_signal_connect(G_OBJECT(preview_event_box), "button-release-event",
			 G_CALLBACK(_signal_preview_clicked),
			 self);

	/* Signal handler for theme_bg_browser*/
	g_signal_connect(G_OBJECT(IRRECO_LISTBOX(
		self->theme_bg_browser->images)->tree_selection),
		"changed",
		G_CALLBACK(_signal_theme_image_selection_changed),
		self);

	gtk_widget_show_all(GTK_WIDGET(self));
	IRRECO_RETURN
}

static void irreco_background_dlg_init(IrrecoBackgroundDlg *self)
{
	IRRECO_ENTER
	self->filename = g_string_new(NULL);
	IRRECO_RETURN
}

static void irreco_background_dlg_finalize(GObject *object)
{
	IrrecoBackgroundDlg *self;
	IRRECO_ENTER

	self = IRRECO_BACKGROUND_DLG(object);
	g_string_free(self->filename, TRUE);
	self->filename = NULL;

	G_OBJECT_CLASS(irreco_background_dlg_parent_class)->finalize(object);
	IRRECO_RETURN
}

static void irreco_background_dlg_class_init(IrrecoBackgroundDlgClass *klass)
{
	GObjectClass* object_class = G_OBJECT_CLASS (klass);
	/* IrrecoDlgClass* parent_class = IRRECO_DLG_CLASS (klass); */

	object_class->finalize = irreco_background_dlg_finalize;
	object_class->constructed = irreco_background_dlg_constructed;
}

GtkWidget *irreco_background_dlg_new(IrrecoData *irreco_data,
				     GtkWindow *parent_window)
{
	IrrecoBackgroundDlg *self;
	IRRECO_ENTER

	self = g_object_new(IRRECO_TYPE_BACKGROUND_DLG,
			    "irreco-data", irreco_data,
			    NULL);
	irreco_dlg_set_parent(IRRECO_DLG(self), parent_window);
	IRRECO_RETURN_PTR(self);
}

/**
 * @deprecated
 * @todo Replace calls to irreco_background_dlg_create() with
 *       irreco_background_dlg_new().
 */
IrrecoBackgroundDlg *irreco_background_dlg_create(IrrecoData *irreco_data,
						  GtkWindow *parent_window)
{
	IRRECO_ENTER
	IRRECO_RETURN_PTR(irreco_background_dlg_new(
		irreco_data, parent_window));
}

/**
 * @deprecated
 * @todo Replace calls to irreco_background_dlg_destroy() with
 *       gtk_widget_destroy().
 */
void irreco_background_dlg_destroy(IrrecoBackgroundDlg * self)
{
	IRRECO_ENTER
	gtk_widget_destroy(GTK_WIDGET(self));
	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Private Functions
 * @{
 */

static void _sync_radio(IrrecoBackgroundDlg *self)
{
	IRRECO_ENTER
	switch (self->type) {
	case IRRECO_BACKGROUND_DEFAULT:
		gtk_widget_grab_focus(self->radio_default);
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(
					     self->radio_default),
					     TRUE);
		break;

	case IRRECO_BACKGROUND_COLOR:
		gtk_widget_grab_focus(self->radio_color);
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(
					     self->radio_color),
					     TRUE);
		break;

	case IRRECO_BACKGROUND_IMAGE:
		gtk_widget_grab_focus(self->radio_image);
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(
					     self->radio_image),
					     TRUE);
		break;
	}
	IRRECO_RETURN
}

/**
 * Draw preview with current image.
 */
static gboolean _draw_preview(IrrecoBackgroundDlg *self)
{
	const gchar *image = NULL;
	const GdkColor *color = NULL;
	IRRECO_ENTER

	switch (self->type) {
	case IRRECO_BACKGROUND_DEFAULT:
		image = irreco_button_layout_default_image;
		color = &irreco_button_layout_default_color;
		break;

	case IRRECO_BACKGROUND_IMAGE:
		image = self->filename->str;
		if (irreco_str_isempty(image)) image = NULL;

	case IRRECO_BACKGROUND_COLOR:
		g_object_get(G_OBJECT(self->color_button),
			     "color", &color, NULL);
		break;
	}

	IRRECO_RETURN_BOOL(_draw_preview_custom(
		self, image, color));
}

/**
 * Draw preview with custom image.
 */
static gboolean _draw_preview_custom(IrrecoBackgroundDlg *self,
				     const gchar *image,
				     const GdkColor *bg_color)
{
	GError *error = NULL;
	GdkPixbuf *pixbuf = NULL;
	GdkPixmap *pixmap = NULL;
	GdkGC *bg_gc = NULL;
	IRRECO_ENTER

	g_assert(self != NULL);

	/* Attempt to load the image. */
	if (image != NULL) {
		pixbuf = gdk_pixbuf_new_from_file_at_scale(image,
			IRRECO_BACKGROUND_PREVIEW_WIDHT,
			IRRECO_BACKGROUND_PREVIEW_HEIGHT,
			FALSE, &error);
		if (irreco_gerror_check_print(&error)) {
			IRRECO_RETURN_BOOL(FALSE);
		}
	}

	gtk_widget_realize(GTK_WIDGET(self->preview));
	pixmap = gdk_pixmap_new(GDK_DRAWABLE(
				self->preview->window),
				IRRECO_BACKGROUND_PREVIEW_WIDHT,
				IRRECO_BACKGROUND_PREVIEW_HEIGHT,
				-1);

	/* Fill background with solid color. */
	bg_gc = gdk_gc_new(GDK_DRAWABLE(self->preview->window));
	gdk_gc_set_rgb_fg_color(bg_gc, bg_color);
	gdk_gc_set_rgb_bg_color(bg_gc, bg_color);
	gdk_draw_rectangle(GDK_DRAWABLE(pixmap), bg_gc, TRUE, 0, 0,
			   IRRECO_BACKGROUND_PREVIEW_WIDHT,
			   IRRECO_BACKGROUND_PREVIEW_HEIGHT);
	g_object_unref(G_OBJECT(bg_gc));

	/* Draw images to pixmap. */
	if (pixbuf != NULL) {
		gdk_draw_pixbuf(GDK_DRAWABLE(pixmap), NULL, pixbuf, 0, 0, 0, 0,
				IRRECO_BACKGROUND_PREVIEW_WIDHT,
				IRRECO_BACKGROUND_PREVIEW_HEIGHT,
				GDK_RGB_DITHER_NORMAL, 0, 0);
	}

	/* Set background image, and queque redraw, so the image is shown.*/
	gdk_window_set_back_pixmap(self->preview->window,
				   pixmap, FALSE);
	gtk_widget_queue_draw_area(self->preview, 0, 0,
				self->preview->allocation.width,
				self->preview->allocation.height);

	if (pixbuf != NULL) g_object_unref(G_OBJECT(pixbuf));
	if (pixmap != NULL) g_object_unref(G_OBJECT(pixmap));
	IRRECO_RETURN_BOOL(TRUE);
}

static void _select_image(IrrecoBackgroundDlg *self)
{
	GdkColor *color = NULL;
	GtkWidget *file_dlg = NULL;
	gchar *image_dir = NULL;
	IRRECO_ENTER

	/* Create image select dialog. */
	file_dlg = hildon_file_chooser_dialog_new(GTK_WINDOW(self),
						  GTK_FILE_CHOOSER_ACTION_OPEN);
	gtk_window_set_title(GTK_WINDOW(file_dlg),_("Select background image"));
	gtk_file_chooser_set_local_only(GTK_FILE_CHOOSER(file_dlg), TRUE);
	image_dir = g_build_path("/", getenv("HOME"), "MyDocs/.images/", NULL);
	gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(file_dlg),
					    image_dir);
	g_free(image_dir);

	/* Loop until user cancels or we get a valid image. */
	gtk_widget_show_all(GTK_WIDGET(file_dlg));
	while (gtk_dialog_run(GTK_DIALOG(file_dlg)) == GTK_RESPONSE_OK) {
		gchar *filename = gtk_file_chooser_get_filename(
			GTK_FILE_CHOOSER(file_dlg));
		IRRECO_PRINTF("Attempting to display background "
			      "image: \"%s\".\n", filename);

		/* Attempt to display the image. */
		g_object_get(G_OBJECT(self->color_button),
			     "color", &color, NULL);
		if (_draw_preview_custom(
				self, filename, color)) {
			IRRECO_PRINTF("Image OK.\n");
			irreco_gstring_set_and_free(
				self->filename, filename);
			filename = NULL;
			self->theme_bg_browser->current_image = NULL;

			/* Set type as image. */
			if (self->type != IRRECO_BACKGROUND_IMAGE) {
				gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(
					self->radio_image), TRUE);
			}
			break;

		/* Cleanup */
		} else {
			gchar *basename = g_path_get_basename(filename);
			irreco_error_dlg_printf(GTK_WINDOW(file_dlg),
						_("Cannot open image \"%s\""),
						basename);
			IRRECO_PRINTF("Image invalid.\n");
			g_free(basename);
			g_free(filename);
		}
	}

	gtk_widget_grab_focus(self->radio_image);
	gtk_widget_destroy(file_dlg);
	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Public Functions                                                           */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */

/**
 * Show dialog, and ask user for input.
 */
gboolean irreco_background_dlg_run(IrrecoBackgroundDlg *self,
				   IrrecoButtonLayout *irreco_layout)
{
	IRRECO_ENTER

	/* Setup dialog. */
	self->type = irreco_button_layout_get_bg_type(irreco_layout);
	irreco_gstring_set(self->filename,
			   irreco_button_layout_get_bg_image(irreco_layout));
	hildon_color_button_set_color(
		HILDON_COLOR_BUTTON(self->color_button),
		irreco_button_layout_get_bg_color(irreco_layout));

	/* Prepare ui. */
	_sync_radio(self);
	_draw_preview(self);

	if (gtk_dialog_run(GTK_DIALOG(self)) == GTK_RESPONSE_OK) {
		GdkColor *color;
		g_object_get(G_OBJECT(self->color_button),
			     "color", &color, NULL);
		irreco_button_layout_set_bg_type(irreco_layout,
						 self->type);
		irreco_button_layout_set_bg_color(irreco_layout, color);
		irreco_button_layout_set_bg_image(
			irreco_layout, self->filename->str);

		IRRECO_PRINTF("%i\n",
			irreco_button_layout_get_bg_type(irreco_layout));

		IRRECO_RETURN_BOOL(TRUE);
	}
	IRRECO_RETURN_BOOL(FALSE);
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Events and Callbacks                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Events and Callbacks
 * @{
 */

static void _signal_color_clicked(GtkColorButton *widget,
				  IrrecoBackgroundDlg *self)
{
	IRRECO_ENTER

	/* Make sure the background type is color or image. */
	if (self->type == IRRECO_BACKGROUND_DEFAULT) {
		/* This should trigger a redraw ... */
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(
					     self->radio_color),
					     TRUE);
	} else {
		_draw_preview(self);
	}
	IRRECO_RETURN
}

static void _signal_type_toggled(GtkToggleButton *togglebutton,
				 IrrecoBackgroundDlg *self)
{
	IrrecoButtonLayoutBgType type;
	IRRECO_ENTER

	if (gtk_toggle_button_get_active(togglebutton) == TRUE) {
		type = (IrrecoButtonLayoutBgType) g_object_get_data(
			G_OBJECT(togglebutton), "IrrecoButtonLayoutBgType");
		self->type = type;
		IRRECO_PRINTF("Type: %i\n", self->type);
	}

	_draw_preview(self);
	IRRECO_RETURN
}

static void _signal_image_clicked(gpointer *ignore,
				  IrrecoBackgroundDlg *self)
{
	IRRECO_ENTER
	_select_image(self);
	IRRECO_RETURN
}

static void _signal_preview_clicked(gpointer *ignore1,
				    gpointer *ignore2,
				    IrrecoBackgroundDlg *self)
{
	IRRECO_ENTER
	_select_image(self);
	IRRECO_RETURN
}

static void _signal_theme_image_selection_changed(GtkTreeSelection *selection,
						  IrrecoBackgroundDlg *self)
{
	IrrecoThemeBg *bg = self->theme_bg_browser->current_image;
	IRRECO_ENTER

	if (bg != NULL) {
		g_string_printf (self->filename, "%s", bg->image_path->str);

		/* Set type as image. */
		if (self->type != IRRECO_BACKGROUND_IMAGE) {
			gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(
				self->radio_image), TRUE);
		}
	} else {
		g_string_printf (self->filename, "%s", "");
	}

	_draw_preview(self);

	IRRECO_RETURN
}

/** @} */

/** @} */



/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_listbox.h"

/**
 * @addtogroup IrrecoListbox
 * @ingroup Irreco
 *
 * A base class for Irreco listbox classes.
 *
 * The basic structure is to have a GtkTreview widget inside a
 * GtkScrolledWindow, and provide convenience frappers for so you dont have
 * to deal with GtkTreview directly.
 *
 * That is, IrrecoListbox classes provide a simple listboxes, without all
 * that tree nonsense.
 *
 * Subclasses must set these variables, or this class will not work:
 *      IRRECO_LISTBOX(self)->list_store
 *      IRRECO_LISTBOX(self)->tree_view
 *      IRRECO_LISTBOX(self)->tree_selection
 *      IRRECO_LISTBOX(self)->data_col_id
 *      IRRECO_LISTBOX(self)->text_col_id
 *
 * @{ @file
 * Source file of @ref IrrecoListbox.
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes.                                                                */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#if 0
static void irreco_listbox_scrolled_size_request(GtkWidget *widget,
						 GtkRequisition *requisition,
						 IrrecoListbox *self);
#endif


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

G_DEFINE_TYPE(IrrecoListbox, irreco_listbox, GTK_TYPE_HBOX)

static void irreco_listbox_class_init(IrrecoListboxClass *klass)
{
}

static void irreco_listbox_init(IrrecoListbox *self)
{
	IRRECO_ENTER

	self->select_new_rows = TRUE;
	gtk_box_set_spacing(GTK_BOX(self), 1);
#if 0
	/* Connect signals. */
	g_signal_connect(G_OBJECT(self), "size-request",
			 G_CALLBACK(irreco_listbox_scrolled_size_request),
			 self);
#endif
	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Public Functions                                                           */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */

/**
 * Set if new rows added to the listbox will automatically be selected.
 */
void irreco_listbox_set_select_new_rows(IrrecoListbox *self,
					gboolean select_new_rows)
{
	IRRECO_ENTER
	self->select_new_rows = select_new_rows;
	IRRECO_RETURN
}

void irreco_listbox_set_autosize(IrrecoListbox *self,
				 gint min_width,
				 gint max_width,
				 gint min_height,
				 gint max_height)
{
	IRRECO_ENTER
	if ((self->min_width  = min_width)  < 0) self->min_width  = 0;
	if ((self->max_width  = max_width)  < 0) self->max_width  = 0;
	if ((self->min_height = min_height) < 0) self->min_height = 0;
	if ((self->max_height = max_height) < 0) self->max_height = 0;
	IRRECO_RETURN
}

void irreco_listbox_clear(IrrecoListbox *self)
{
	IRRECO_ENTER
	gtk_list_store_clear(self->list_store);
	IRRECO_RETURN
}

/**
 * Get GtkTreeIter for some index in the listbox.
 */
gboolean irreco_listbox_get_iter(IrrecoListbox *self,
				 gint index, GtkTreeIter * iter)
{
	gboolean rvalue;
	GtkTreeModel *model;
	GtkTreePath *path;
	IRRECO_ENTER

	IRRECO_DEBUG("Index \"%i\".\n", index);
	model = gtk_tree_view_get_model(GTK_TREE_VIEW(
					self->tree_view));
	path = gtk_tree_path_new_from_indices(index, -1);
	rvalue = gtk_tree_model_get_iter(model, iter, path);
	gtk_tree_path_free(path);
	IRRECO_RETURN_INT(rvalue);
}

/**
 * Select a row.
 */
gboolean irreco_listbox_set_selection(IrrecoListbox *self, gint index)
{
	GtkTreeIter iter;
	IRRECO_ENTER

	IRRECO_DEBUG("Selecting row \"%i\".\n", index);
	if (irreco_listbox_get_iter(self, index, &iter)) {
		gtk_tree_selection_select_iter(self->tree_selection, &iter);
		IRRECO_RETURN_BOOL(TRUE);
	}
	IRRECO_RETURN_BOOL(FALSE);
}

/**
 * Get currect selection of listbox.
 *
 * @par Example
 * @code
 * gint sel_index = -1;
 * gchar *sel_label = NULL;
 * gpointer sel_user_data = NULL;
 * irreco_listbox_get_selection(IRRECO_LISTBOX(listbox),
 *                              &sel_index, &sel_label, &sel_user_data);
 * @endcode
 */
gboolean irreco_listbox_get_selection(IrrecoListbox  *self,
				      gint           *index,
				      gchar         **label,
				      gpointer       *user_data)
{
	gboolean          rvalue	= FALSE;
	GtkTreeIter       iter;
        GtkTreeModel     *model         = NULL;
	GtkTreePath      *path          = NULL;
	gint             *path_indices  = NULL;
	GtkTreeSelection *selection     = NULL;
	gint              sel_index     = -1;
	gchar            *sel_label     = NULL;
	gpointer          sel_user_data = NULL;
	IRRECO_ENTER

	/* Get currect selection, if set. */
	selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(self->tree_view));
	if (gtk_tree_selection_get_selected(self->tree_selection,
					    &model, &iter)) {
		path = gtk_tree_model_get_path(model, &iter);
		path_indices = gtk_tree_path_get_indices(path);
		sel_index = path_indices[0];
		gtk_tree_path_free(path);
		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter,
				   self->text_col_id, &sel_label,
                   		   self->data_col_id, &sel_user_data,
				   -1);
		rvalue = TRUE;
	}

	IRRECO_DEBUG("Selection \"%i\" \"%s\" \"%p\"\n",
		     sel_index, sel_label, sel_user_data);

	/* Store values to pointers. */
	if (index) *index = sel_index;
	if (user_data) *user_data = sel_user_data;
	if (label) {
		*label = sel_label;
	} else {
		g_free(label);
	}

	IRRECO_RETURN_BOOL(rvalue);
}

gint irreco_listbox_get_selection_index(IrrecoListbox *self)
{
	gint sel_index = -1;
	IRRECO_ENTER;

	irreco_listbox_get_selection(self, &sel_index, NULL, NULL);
	IRRECO_RETURN_INT(sel_index);
}

gchar *irreco_listbox_get_selection_label(IrrecoListbox *self)
{
	gchar *sel_label = NULL;
	IRRECO_ENTER;

	irreco_listbox_get_selection(self, NULL, &sel_label, NULL);
	IRRECO_RETURN_STR(sel_label);
}

gpointer irreco_listbox_get_selection_data(IrrecoListbox *self)
{
	gpointer sel_user_data = NULL;
	IRRECO_ENTER;

	irreco_listbox_get_selection(self, NULL, NULL, &sel_user_data);
	IRRECO_RETURN_PTR(sel_user_data);
}

/**
 * Move the currently selected row upwards.
 */
gboolean irreco_listbox_move_selected_up(IrrecoListbox *self)
{
	gint sel_index;
	IRRECO_ENTER

	sel_index = irreco_listbox_get_selection_index(self);
	if (sel_index == -1) IRRECO_RETURN_BOOL(FALSE);
	IRRECO_RETURN_BOOL(irreco_listbox_move(
		self, sel_index, sel_index - 1));
}

/**
 * Move the currently selected row downwards.
 */
gboolean irreco_listbox_move_selected_down(IrrecoListbox *self)
{
	gint sel_index;
	IRRECO_ENTER

	sel_index = irreco_listbox_get_selection_index(self);
	if (sel_index == -1) IRRECO_RETURN_BOOL(FALSE);
	IRRECO_RETURN_BOOL(irreco_listbox_move(
		self, sel_index, sel_index + 1));
}

gboolean irreco_listbox_move(IrrecoListbox *self,
			     gint from_index,
			     gint to_index)
{
	GtkTreeIter from_iter;
	GtkTreeIter to_iter;
	IRRECO_ENTER

	/* Check indices. We assume that below zero means first,
	   and above list lenght means last. */
	if (from_index < 0) {
		from_index = 0;
	} else if (from_index > self->list_store->length) {
		IRRECO_RETURN_BOOL(FALSE);
	}

	if (to_index > self->list_store->length) {
		to_index = self->list_store->length;
	} else if (to_index < 0) {
		IRRECO_RETURN_BOOL(FALSE);
	}

	IRRECO_DEBUG("Moving item from index \"%i\" to index \"%i\".\n",
		     from_index, to_index);

	if (irreco_listbox_get_iter(self, from_index, &from_iter)
	    && irreco_listbox_get_iter(self, to_index, &to_iter)) {
		if (from_index > to_index) {
			IRRECO_DEBUG("Moving up.\n");
			gtk_list_store_move_before(self->list_store,
						   &from_iter, &to_iter);
		} else {
			IRRECO_DEBUG("Moving down.\n");
			gtk_list_store_move_after(self->list_store,
						  &from_iter, &to_iter);
		}

		/* Set selection to the moved row. */
		if (gtk_tree_selection_iter_is_selected(
			self->tree_selection, &from_iter)) {
		} else {
			gtk_tree_selection_select_iter(self->tree_selection,
						       &from_iter);
		}

		IRRECO_RETURN_BOOL(TRUE);
	}

	IRRECO_RETURN_BOOL(FALSE);
}

gboolean irreco_listbox_remove_selected(IrrecoListbox *self)
{
	gint index;
	GtkTreeIter iter;
	gint sel_index;
	IRRECO_ENTER

	sel_index = irreco_listbox_get_selection_index(self);
	if ((index = sel_index) == -1
	    || irreco_listbox_get_iter(self, index, &iter) == FALSE) {
		IRRECO_RETURN_BOOL(FALSE);
	}

	/* Attemt to select another row before we destroy the current one.
	   This helps to prevent double selection changed events being sent,
	   when the old row is destroyed (and thus unselected) and selecting
	   another row afterwards. */
	if(!irreco_listbox_set_selection(self, index - 1)) {
		irreco_listbox_set_selection(self, index + 1);
	}

	gtk_list_store_remove(self->list_store, &iter);
	IRRECO_RETURN_BOOL(TRUE);
}

gboolean irreco_listbox_remove(IrrecoListbox *self, gint index)
{
	GtkTreeIter iter;
	IRRECO_ENTER

	if (irreco_listbox_get_iter(self, index, &iter) == FALSE) {
		IRRECO_RETURN_BOOL(FALSE);
	}
	gtk_list_store_remove(self->list_store, &iter);
	IRRECO_RETURN_BOOL(TRUE);
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Events and Callbacks                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Events and Callbacks
 * @{
 */
#if 0
static void irreco_listbox_scrolled_size_request(GtkWidget *widget,
						 GtkRequisition *requisition,
						 IrrecoListbox *self)
{
	IRRECO_ENTER

	if (requisition->width < self->min_width) {
		requisition->width = self->min_width;
	} else if (requisition->width > self->max_width) {
		requisition->width = self->max_width;
	}

	if (requisition->height < self->min_height) {
		requisition->height = self->min_height;
	} else if (requisition->height > self->max_height) {
		requisition->height = self->max_height;
	}

	IRRECO_RETURN
}
#endif
/** @} */

/** @} */













/*
 * irreco - Ir Remote Control
 * Copyright (C) 2008 Joni Kokko (t5kojo01@students.oamk.fi)
 *		      Pekka Gehör (pegu6@msn.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_theme_button.h"

/**
 * @addtogroup IrrecoThemeButton
 * @ingroup Irreco
 *
 * Contains information of button.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoThemeButton.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

IrrecoThemeButton *irreco_theme_button_new(const char *theme_name)
{
	IrrecoThemeButton *self;
	IRRECO_ENTER

	self = g_slice_new0(IrrecoThemeButton);

	self->theme_name = g_string_new(theme_name);
	self->style_name = g_string_new("");
	self->name = g_string_new("");
	self->image_up = g_string_new("");
	self->image_down = g_string_new("");
	self->text_format_up = g_string_new("");
	self->text_format_down = g_string_new("");
	self->text_padding = 5;

	/* Default to center alignment. */
	self->text_h_align = 0.5;
	self->text_v_align = 0.5;

	IRRECO_RETURN_PTR(self);
}

void irreco_theme_button_free(IrrecoThemeButton *self)
{
	IRRECO_ENTER

	g_assert(self != NULL);

	g_string_free(self->theme_name, TRUE);
	self->theme_name = NULL;

	g_string_free(self->style_name, TRUE);
	self->style_name = NULL;

	g_string_free(self->name, TRUE);
	self->name = NULL;

	g_string_free(self->image_up, TRUE);
	self->image_up = NULL;

	g_string_free(self->image_down, TRUE);
	self->image_down = NULL;

	g_string_free(self->text_format_up, TRUE);
	self->text_format_up = NULL;

	g_string_free(self->text_format_down, TRUE);
	self->text_format_down = NULL;

	g_slice_free(IrrecoThemeButton, self);
	self = NULL;

	IRRECO_RETURN
}

/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Public Functions                                                           */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */

void irreco_theme_button_set(IrrecoThemeButton *self,
			     const char *style_name,
			     const char *name,
			     gboolean	 allow_text,
			     const char *image_up,
			     const char *image_down,
			     const char *text_format_up,
			     const char *text_format_down,
			     gint	 text_padding,
			     gfloat	 text_h_align,
			     gfloat	 text_v_align)
{
	IRRECO_ENTER

	if (style_name != NULL) {
		g_string_printf(self->style_name, "%s", style_name);
	}
	if (name != NULL) {
		g_string_printf(self->name, "%s", name);
	}
	self->allow_text = allow_text;
	if (image_up != NULL) {
		g_string_printf(self->image_up, "%s", image_up);
	}
	if (image_down != NULL) {
		g_string_printf(self->image_down, "%s", image_down);
	} else {
		g_string_printf(self->image_down, "%s", image_up);
	}
	if (text_format_up != NULL) {
		g_string_printf(self->text_format_up, "%s", text_format_up);
	}
	if (text_format_down != NULL) {
		g_string_printf(self->text_format_down, "%s", text_format_down);
	}

	self->text_padding = text_padding;

	/* Set Horisontal aligment. */
	if (self->text_h_align < 0) {
		self->text_h_align = 0;
	} else if (self->text_h_align > 1) {
		self->text_h_align = 1;
	} else {
		self->text_h_align = text_h_align;
	}

	/* Set vertical alignment. */
	if (self->text_v_align < 0) {
		self->text_v_align = 0;
	} else if (self->text_v_align > 1) {
		self->text_v_align = 1;
	} else {
		self->text_v_align = text_v_align;
	}

	IRRECO_RETURN
}
void irreco_theme_button_set_from_button(IrrecoThemeButton *self,
					 IrrecoThemeButton *button)
{
	IRRECO_ENTER
	irreco_theme_button_set(self,
				button->style_name->str,
				button->name->str,
				button->allow_text,
				button->image_up->str,
				button->image_down->str,
				button->text_format_up->str,
				button->text_format_down->str,
				button->text_padding,
				button->text_h_align,
				button->text_v_align);
	IRRECO_RETURN
}
void irreco_theme_button_print(IrrecoThemeButton *self)
{
	IRRECO_ENTER

	IRRECO_DEBUG("--------------------------------------------\n");
	IRRECO_DEBUG("theme_name: %s\n", self->theme_name->str);
	IRRECO_DEBUG("style_name: %s\n", self->style_name->str);
	IRRECO_DEBUG("button_name: %s\n", self->name->str);
	IRRECO_DEBUG("allow_text: %d\n", self->allow_text);
	IRRECO_DEBUG("image_up: %s\n",self->image_up->str);
	IRRECO_DEBUG("image_down: %s\n", self->image_down->str);
	IRRECO_DEBUG("text_format_up: %s\n", self->text_format_up->str);
	IRRECO_DEBUG("text_format_down: %s\n", self->text_format_down->str);
  	IRRECO_DEBUG("text_padding: %d\n", self->text_padding);
	IRRECO_DEBUG("--------------------------------------------\n");

	IRRECO_RETURN
}

IrrecoThemeButton *irreco_theme_button_copy(IrrecoThemeButton *self)
{
	IrrecoThemeButton *new = NULL;
	IRRECO_ENTER

	new = irreco_theme_button_new(self->theme_name->str);
	irreco_theme_button_set(new, self->style_name->str, self->name->str,
				self->allow_text, self->image_up->str,
    				self->image_down->str, self->text_format_up->str,
				self->text_format_down->str, self->text_padding,
			        self->text_h_align, self->text_v_align);

	IRRECO_RETURN_PTR(new);
}

/**
 * IrrecoThemeBg new from dir
 */

IrrecoThemeButton *
irreco_theme_button_new_from_dir(const gchar *dir, const gchar *theme_name)
{
	IrrecoThemeButton *self = NULL;

	IRRECO_ENTER

	self = irreco_theme_button_new(theme_name);
	irreco_theme_button_read(self, dir);
	IRRECO_RETURN_PTR(self);
}

void irreco_theme_button_read(IrrecoThemeButton *self, const gchar *dir)
{
	IrrecoKeyFile	*keyfile;
	GString		*conf = NULL;
	gchar		*name = NULL;
	gchar		*up = NULL;
	gchar		*down = NULL;
	gboolean	allow_text;
	gchar		*text_format_up = NULL;
	gchar		*text_format_down = NULL;
	gint		text_padding = 5;
	gfloat		text_h_align = 0.5;
	gfloat		text_v_align = 0.5;
	GString		*style_name = g_string_new(self->theme_name->str);
	IRRECO_ENTER

	conf = g_string_new(dir);
	g_string_append_printf(conf, "/button.conf");
	keyfile = irreco_keyfile_create(dir,
					conf->str,
					"theme-button");
	if (keyfile == NULL) goto end;

	/* Required fields. */
	if (!irreco_keyfile_get_str(keyfile, "name", &name) ||
	    !irreco_keyfile_get_path(keyfile, "up", &up)) {
		IRRECO_PRINTF("Could not read style from group \"%s\"\n",
			      keyfile->group);
		goto end;
	}

	/* Optional fields. */
	irreco_keyfile_get_path(keyfile, "down", &down);
	irreco_keyfile_get_bool(keyfile, "allow-text", &allow_text);
	irreco_keyfile_get_str(keyfile, "text-format-up", &text_format_up);
	irreco_keyfile_get_str(keyfile, "text-format-down", &text_format_down);
	irreco_keyfile_get_int(keyfile, "text-padding", &text_padding);
	irreco_keyfile_get_float(keyfile, "text-h-align", &text_h_align);
	irreco_keyfile_get_float(keyfile, "text-v-align", &text_v_align);

	g_string_append_printf(style_name,"/%s", name);

	irreco_theme_button_set(self, style_name->str,
					name, allow_text,
					up, down,
					text_format_up, text_format_down,
					text_padding,
					text_h_align,
					text_v_align);

	irreco_theme_button_print(self);

	end:
	if(keyfile != NULL) irreco_keyfile_destroy(keyfile);
	if(name != NULL) g_free(name);
	if(name != NULL) g_free(up);
	if(name != NULL) g_free(down);
	if(name != NULL) g_free(text_format_up);
	if(name != NULL) g_free(text_format_down);
	g_string_free(style_name, TRUE);

	IRRECO_RETURN
}

/**
 * Save button to theme folder
 */

gboolean
irreco_theme_button_save(IrrecoThemeButton *self, const gchar *path)
{
	gboolean		rvalue = FALSE;
	GString			*keyfile_path;
	GString			*unpressed_image_path;
	GString			*pressed_image_path;
	GString			*folder;
	gchar			*folder_name;
	GKeyFile		*keyfile;
	gchar			*cp_cmd_unpressed;
	gchar			*cp_cmd_pressed;
	gchar			*type_unpressed;
	gchar			*type_pressed;
	GString			*unpressed_image_name;
	GString			*pressed_image_name;
	IRRECO_ENTER

	keyfile = g_key_file_new();
	unpressed_image_name = g_string_new(NULL);
	pressed_image_name = g_string_new(NULL);
	unpressed_image_path = g_string_new(path);
	pressed_image_path = g_string_new(path);
	keyfile_path = g_string_new(path);
	folder = g_string_new(self->name->str);
	g_string_ascii_down(folder);
	folder_name = g_strdup(folder->str);
	g_strdelimit(folder_name, " ", '_');
	g_strdelimit(folder_name, ",-|> <.", 'a');

	g_string_printf(folder, "%s", folder_name);

	/* get file type */
	type_unpressed = g_strrstr(self->image_up->str, ".");
	type_pressed = g_strrstr(self->image_down->str, ".");

	g_string_printf(unpressed_image_name, "%s%s", "unpressed",
			type_unpressed);
	g_string_printf(pressed_image_name, "%s%s", "pressed", type_pressed);

	/* Create Folder */
	g_string_append_printf(unpressed_image_path, "/%s", folder_name);
	g_string_append_printf(pressed_image_path, "/%s", folder_name);

	g_string_append_printf(keyfile_path, "/%s", folder_name);

	g_mkdir(unpressed_image_path->str, 0777);

	g_string_append_printf(unpressed_image_path, "/%s%s", "unpressed",
			       type_unpressed);
	g_string_append_printf(pressed_image_path, "/%s%s", "pressed",
			       type_pressed);

	/* Copy images to button folder */

        cp_cmd_unpressed = g_strconcat("cp -f '", self->image_up->str, "' '",
				      unpressed_image_path->str, "'", NULL);
	cp_cmd_pressed = g_strconcat("cp -f '", self->image_down->str, "' '",
				     pressed_image_path->str, "'", NULL);
        system(cp_cmd_unpressed);
	system(cp_cmd_pressed);
	g_free(cp_cmd_unpressed);
	g_free(cp_cmd_pressed);

	/* Create keyfile and save it to folder*/

	irreco_gkeyfile_set_string(keyfile, "theme-button",
				   "up", unpressed_image_name->str);

	irreco_gkeyfile_set_string(keyfile, "theme-button",
				   "down", pressed_image_name->str);

	irreco_gkeyfile_set_string(keyfile, "theme-button" , "name",
				   self->name->str);

	if (self->allow_text) {
		irreco_gkeyfile_set_string(keyfile, "theme-button",
					   "allow-text", "true");
	} else {
		irreco_gkeyfile_set_string(keyfile, "theme-button",
					   "allow-text", "false");
	}

	if (self->text_format_up != NULL && strlen(self->text_format_up->str) > 0) {
		irreco_gkeyfile_set_string(keyfile, "theme-button",
					"text-format-up", self->text_format_up->str);
	}

	if (self->text_format_down != NULL && strlen(self->text_format_down->str) > 0) {
		irreco_gkeyfile_set_string(keyfile, "theme-button",
					"text-format-down", self->text_format_down->str);
	}

	irreco_gkeyfile_set_glong(keyfile, "theme-button",
				   "text-padding", (glong) self->text_padding);

	irreco_gkeyfile_set_gfloat(keyfile,
				   "theme-button",
				   "text-h-align",
				   (gfloat) self->text_h_align);

	irreco_gkeyfile_set_gfloat(keyfile,
				   "theme-button",
				   "text-v-align",
				   (gfloat) self->text_v_align);

	g_string_append_printf(keyfile_path, "/button.conf");
	irreco_write_keyfile(keyfile, keyfile_path->str);

	rvalue = TRUE;
	g_key_file_free(keyfile);
	g_string_free(unpressed_image_name, TRUE);
	g_string_free(pressed_image_name, TRUE);
	g_string_free(keyfile_path, TRUE);
	g_string_free(unpressed_image_path, TRUE);
	g_string_free(pressed_image_path, TRUE);
	IRRECO_RETURN_BOOL(rvalue);
}

/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Events and Callbacks                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/** @} */

/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoData
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoData.
 */




/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_DATA_H_TYPEDEF__
#define __IRRECO_DATA_H_TYPEDEF__
typedef struct _IrrecoData IrrecoData;
#endif /* __IRRECO_DATA_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_DATA_H__
#define __IRRECO_DATA_H__
#include "irreco.h"
#include "irreco_hardkey_map.h"
#include <irreco_string_table.h>
#include "irreco_button_dlg.h"
#include "irreco_backend_manager.h"
#include "irreco_cmd_chain_manager.h"
#include "irreco_window_manager.h"
#include <irreco_webdb.h>

#include "irreco_theme_manager.h"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/*
 * Main IRRECO data structure.
 */
struct _IrrecoData {

	/* Ui resoirrecoes  */
	HildonProgram *program;

	/* Contains all layouts. */
	IrrecoStringTable *irreco_layout_array;

	/* Backend manager. */
	IrrecoBackendManager *irreco_backend_manager;

	/* Window manager. */
	IrrecoWindowManager *window_manager;

	/* We use this dialog to create new buttons. */
	IrrecoButtonDlg *new_button_dlg;

	/* Keeps a list of IrrecoCmdChains assosiated with
	   Hardkeys and buttons. */
	IrrecoCmdChainManager *cmd_chain_manager;

	IrrecoWebdbCache *webdb_cache;

	/* Contains all themes */
	IrrecoThemeManager *theme_manager;
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
IrrecoData *irreco_data_new();
void irreco_data_free(IrrecoData * irreco_data);
IrrecoWebdbCache *irreco_data_get_webdb_cache(IrrecoData *self,
					      gboolean reset);


#endif /* __IRRECO_DATA_H__ */

/** @} */


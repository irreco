/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * irreco - Ir Remote Control
 * Copyright (C) 2008 Pekka Gehör (pegu6@msn.com)
 *
 * irreco_button_creator_dlg.c is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * irreco_button_creator_dlg.c is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @addtogroup IrrecoButtonCreatorDlg
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoButtonCreatorDlg.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef _IRRECO_BUTTON_CREATOR_DLG_H_TYPEDEF_
#define _IRRECO_BUTTON_CREATOR_DLG_H_TYPEDEF_

#define IRRECO_TYPE_BUTTON_CREATOR_DLG             (irreco_button_creator_dlg_get_type ())
#define IRRECO_BUTTON_CREATOR_DLG(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), IRRECO_TYPE_BUTTON_CREATOR_DLG, IrrecoButtonCreatorDlg))
#define IRRECO_BUTTON_CREATOR_DLG_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), IRRECO_TYPE_BUTTON_CREATOR_DLG, IrrecoButtonCreatorDlgClass))
#define IRRECO_IS_BUTTON_CREATOR_DLG(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), IRRECO_TYPE_BUTTON_CREATOR_DLG))
#define IRRECO_IS_BUTTON_CREATOR_DLG_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), IRRECO_TYPE_BUTTON_CREATOR_DLG))
#define IRRECO_BUTTON_CREATOR_DLG_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), IRRECO_TYPE_BUTTON_CREATOR_DLG, IrrecoButtonCreatorDlgClass))

typedef struct _IrrecoButtonCreatorDlgClass IrrecoButtonCreatorDlgClass;
typedef struct _IrrecoButtonCreatorDlg IrrecoButtonCreatorDlg;

#endif /* __IRRECO_BUTTON_CREATOR_DLG_H_TYPEDEF__ */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_BUTTON_CREATOR_DLG_H__
#define __IRRECO_BUTTON_CREATOR_DLG_H__
#include "irreco.h"
#include "irreco_internal_dlg.h"
#include "irreco_button.h"
#include "irreco_bg_browser_widget.h"
#include "irreco_button_browser_widget.h"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/





struct _IrrecoButtonCreatorDlgClass
{
	IrrecoInternalDlgClass parent_class;
};

struct _IrrecoButtonCreatorDlg
{

	IrrecoInternalDlg 		parent_instance;
	IrrecoTheme			*theme;
	IrrecoData			*irreco_data;
	GtkWindow			*parent_window;

	GtkWidget			*notebook;
	GtkWidget			*combobox_name;
	GtkWidget			*select_button;
	GtkWidget			*preview_image_unpressed;
	GtkWidget			*preview_image_pressed;
	GtkWidget			*event_box_pressed;
	GtkWidget			*add_button_unpressed;
	GtkWidget			*add_button_pressed;

	GtkWidget			*label_unpressed_size;
	GtkWidget			*label_pressed_size;

	/* Dialog buttons */
	GtkWidget			*cancel_button;
	GtkWidget			*add_button;
	/* Settings */
	GString				*unpressed_path;
	GString				*pressed_path;
	const gchar			*unpressed_format;
	const gchar			*pressed_format;


	GtkWidget			*allow_text;
	GtkWidget			*text_format_up;
	GtkWidget			*text_format_down;
	GtkWidget			*text_padding;
	GtkWidget			*text_h_align;
	GtkWidget			*text_v_align;

	GtkWidget			*unbutton_size;
	GtkWidget			*button_size;


	GtkWidget			*label_sample_text_format_up;
	GtkWidget			*label_sample_text_format_down;

	IrrecoBgBrowserWidget		*theme_bg_browser;
	GString				*filename;

	gint				loader_state;
	gint				loader_func_id;


};


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

GType irreco_button_creator_dlg_get_type (void);
gboolean irreco_button_creator_dlg_run(IrrecoData *irreco_data,
					   IrrecoTheme * irreco_theme,
					   GtkWindow *parent_window,
					   IrrecoThemeButton *bg);

#endif /* _IRRECO_BUTTON_CREATOR_DLG_H_ */

/** @} */

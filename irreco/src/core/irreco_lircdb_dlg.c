/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007,2008  Arto Karppinen (arto.karppinen@iki.fi),
 * Joni Kokko (t5kojo01@students.oamk.fi),
 * Harri Vattulainen (t5vaha01@students.oamk.fi)
 * Sami Mäki (kasmra@xob.kapsi.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_lircdb_dlg.h"
#include <hildon/hildon-banner.h>
#include "irreco_webdb_register_dlg.h"
#include "irreco_select_instance_dlg.h"
#include "irreco_config.h"
#include <hildon/hildon-gtk.h>
#include <hildon/hildon-pannable-area.h>


/**
 * @addtogroup IrrecoLircdbDlg
 * @ingroup Irreco
 *
 * @todo PURPOSE OF CLASS.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoLircdbDlg.
 */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
static void irreco_lircdb_dlg_clean_details(IrrecoLircdbDlg *self);
static gboolean irreco_lircdb_dlg_loader_dirs(IrrecoLircdbDlg *self);
static gboolean irreco_lircdb_dlg_loader_manuf(IrrecoLircdbDlg *self);
static gboolean irreco_lircdb_dlg_loader_models(IrrecoLircdbDlg *self);
static gboolean irreco_lircdb_dlg_ready_to_dl(IrrecoLircdbDlg *self,
						       GtkTreeIter * iter);
static gboolean irreco_lircdb_dlg_load_file(IrrecoLircdbDlg *self);

static gboolean irreco_show_lircdb_dlg_map_event(IrrecoLircdbDlg *self,
						GdkEvent  *event,
						gpointer   user_data);
static void irreco_show_lircdb_dlg_destroy_event(IrrecoLircdbDlg *self,
						gpointer user_data);
static void irreco_show_lircdb_dlg_row_activated_event(GtkTreeView *tree_view,
						      GtkTreePath *path,
						      GtkTreeViewColumn *column,
						      IrrecoLircdbDlg *self);
static void irreco_show_lircdb_dlg_row_expanded_event(GtkTreeView *tree_view,
						     GtkTreeIter *iter,
						     GtkTreePath *path,
						     IrrecoLircdbDlg *self);
static void irreco_show_lircdb_dlg_row_collapsed_event(GtkTreeView *tree_view,
						     GtkTreeIter *iter,
						     GtkTreePath *path,
						     IrrecoLircdbDlg *self);
static void irreco_show_lircdb_dlg_row_selected_event(GtkTreeSelection *sel,
						     IrrecoLircdbDlg *self);




/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/** Button response codes. */
enum {
	IRRECO_DEVICE_REFRESH
};

/** GtkTreeStore colums. */
enum
{
	TEXT_COL,
	FLAG_COL,
	DATA_COL,
	N_COLUMNS
};

/** Loader states. */
enum
{
	LOADER_STATE_INIT,
	LOADER_STATE_LOOP,
	LOADER_STATE_END,
	LOADER_STATE_CLEANUP
};

/** Row flags. */
enum
{
	ROW_CHILDREN_LOADED	= 1 << 1,
	ROW_TYPE_DIR		= 1 << 2,
	ROW_TYPE_MANUFACTURER	= 1 << 3,
	ROW_TYPE_MODEL		= 1 << 4,
	ROW_TYPE_LOADING	= 1 << 5
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

G_DEFINE_TYPE(IrrecoLircdbDlg, irreco_lircdb_dlg, IRRECO_TYPE_DLG)

static void irreco_lircdb_dlg_finalize(GObject *object)
{
	if (G_OBJECT_CLASS(irreco_lircdb_dlg_parent_class)->finalize)
		G_OBJECT_CLASS(irreco_lircdb_dlg_parent_class)->finalize(object);
}

static void irreco_lircdb_dlg_class_init(IrrecoLircdbDlgClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS(klass);
	object_class->finalize = irreco_lircdb_dlg_finalize;
}

static void irreco_lircdb_dlg_init(IrrecoLircdbDlg *self)
{
	GtkTreeViewColumn *column;
	GtkCellRenderer *renderer;
	GtkTreeSelection *select;
	GtkWidget *tree_view_frame;
	GtkWidget *tree_view_pannable;
	GtkWidget *tree_box;

	IRRECO_ENTER

	self->download_button = gtk_button_new_with_label("Download");

	/* Construct dialog. */
	gtk_window_set_title(GTK_WINDOW(self),
			     _("Download device from LircDB"));
	gtk_window_set_modal(GTK_WINDOW(self), TRUE);
	gtk_window_set_destroy_with_parent(GTK_WINDOW(self), TRUE);
	gtk_dialog_set_has_separator(GTK_DIALOG(self), FALSE);

	self->download_button = gtk_dialog_add_button(GTK_DIALOG(self),
				_("Download"), GTK_RESPONSE_OK);
	/* Create hbox */
	self->hbox = g_object_new(GTK_TYPE_HBOX, NULL);
	gtk_box_set_spacing(GTK_BOX(self->hbox), 8);
	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(self)->vbox),
			  irreco_gtk_align(GTK_WIDGET(self->hbox),
					   0, 0, 1, 1, 8, 8, 8, 8));
	/* Create tree_view_hbox */
	tree_box = gtk_hbox_new(FALSE, 2);
	tree_view_pannable = hildon_pannable_area_new();

	/* Create Treeview */
	self->tree_store = gtk_tree_store_new(
		N_COLUMNS, G_TYPE_STRING, G_TYPE_INT, G_TYPE_POINTER);
	self->tree_view = GTK_TREE_VIEW(hildon_gtk_tree_view_new_with_model(1,
		GTK_TREE_MODEL(self->tree_store)));

	renderer = gtk_cell_renderer_text_new();
	column = gtk_tree_view_column_new_with_attributes(
		NULL, renderer, "text", TEXT_COL, NULL);
	gtk_tree_view_append_column(self->tree_view, column);

	gtk_box_pack_start(GTK_BOX(tree_box), GTK_WIDGET(self->tree_view),
					   TRUE, TRUE, 0);

	hildon_pannable_area_add_with_viewport(HILDON_PANNABLE_AREA(tree_view_pannable),
					       GTK_WIDGET(tree_box));

	/* Create Frame for Treeview */
	tree_view_frame = gtk_frame_new("");
	gtk_frame_set_label_widget(GTK_FRAME(tree_view_frame),
		irreco_gtk_label_bold("Devices", 0, 0, 0, 0, 0, 0));

	gtk_container_add(GTK_CONTAINER(tree_view_frame),
			  GTK_WIDGET(tree_view_pannable));

	gtk_box_pack_start(GTK_BOX(self->hbox), GTK_WIDGET(tree_view_frame),
			   TRUE, TRUE, 0);

	/* Setup the selection handler for TREE	*/
	select = gtk_tree_view_get_selection(self->tree_view);
	gtk_tree_selection_set_mode(select, GTK_SELECTION_SINGLE);

	/* Signal handlers. */
	g_signal_connect(G_OBJECT(self), "map-event",
			 G_CALLBACK(irreco_show_lircdb_dlg_map_event), NULL);
	g_signal_connect(G_OBJECT(self), "destroy",
			 G_CALLBACK(irreco_show_lircdb_dlg_destroy_event), NULL);
	g_signal_connect(G_OBJECT(self->tree_view), "row-activated",
			 G_CALLBACK(irreco_show_lircdb_dlg_row_activated_event),
			 self);
	g_signal_connect(G_OBJECT(self->tree_view), "row-expanded",
			 G_CALLBACK(irreco_show_lircdb_dlg_row_expanded_event),
			 self);
	g_signal_connect(G_OBJECT(self->tree_view), "row-collapsed",
			 G_CALLBACK(irreco_show_lircdb_dlg_row_collapsed_event),
			 self);
	g_signal_connect(G_OBJECT (select), "changed",
			 G_CALLBACK (irreco_show_lircdb_dlg_row_selected_event),
			 self);


	gtk_tree_view_set_enable_tree_lines(self->tree_view, TRUE);
	g_object_set (G_OBJECT (self->tree_view), "show-expanders", TRUE, NULL);
	/*g_object_set (G_OBJECT (self->tree_view), "level-indentation", 0, NULL);
	gtk_tree_view_set_rubber_banding(self->tree_view, FALSE);*/

	gtk_widget_set_size_request(GTK_WIDGET(self), -1, 335);
	gtk_widget_show_all(GTK_WIDGET(self));
	gtk_widget_hide(self->download_button);
	IRRECO_RETURN
}

GtkWidget *irreco_lircdb_dlg_new(IrrecoData *irreco_data,
				GtkWindow *parent)
{
	IrrecoLircdbDlg *self;
	IRRECO_ENTER

	self = g_object_new(IRRECO_TYPE_LIRCDB_DLG, NULL);
	irreco_dlg_set_parent(IRRECO_DLG(self), parent);
	irreco_lircdb_dlg_set_irreco_data(self, irreco_data);
	IRRECO_RETURN_PTR(self);
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Private Functions
 * @{
 */

/**
 * Clean detail list
 *
 */
static void irreco_lircdb_dlg_clean_details(IrrecoLircdbDlg *self)
{
	IRRECO_ENTER

	self->config = NULL;

	gtk_widget_hide(self->download_button);

	IRRECO_RETURN
}

/**
 * Have the childern of this item been loaded.
 *
 * @return TRUE if children have been loade, FALSE otherwise.
 */
static gboolean irreco_lircdb_dlg_row_is_loaded(IrrecoLircdbDlg *self,
					       GtkTreeIter *iter)
{
	gint i;
	IRRECO_ENTER

	gtk_tree_model_get(GTK_TREE_MODEL(self->tree_store),
			   iter, FLAG_COL, &i, -1);
	if (i & ROW_CHILDREN_LOADED) IRRECO_RETURN_BOOL(TRUE);
	IRRECO_RETURN_BOOL(FALSE);
}

/**
 * Enable / Disable ROW_CHILDREN_LOADED flag from a row.
 *
 * @param value If set, ROW_CHILDREN_LOADED will be enabled.
 */
static void irreco_lircdb_dlg_row_set_loaded(IrrecoLircdbDlg *self,
					    GtkTreeIter *iter,
					    gboolean value)
{
	gint i;
	IRRECO_ENTER

	gtk_tree_model_get(GTK_TREE_MODEL(self->tree_store),
			   iter, FLAG_COL, &i, -1);
	if (value) {
		i = i | ROW_CHILDREN_LOADED;
	} else {
		i = i & ~ROW_CHILDREN_LOADED;
	}
	gtk_tree_store_set(self->tree_store, iter, FLAG_COL, i, -1);

	IRRECO_RETURN
}

/**
 * Get type of row.
 */
static gint irreco_lircdb_dlg_row_get_type(IrrecoLircdbDlg *self,
					  GtkTreeIter *iter)
{
	gint i;
	IRRECO_ENTER

	gtk_tree_model_get(GTK_TREE_MODEL(self->tree_store),
			   iter, FLAG_COL, &i, -1);
	if (i & ROW_TYPE_DIR)
		IRRECO_RETURN_ENUM(ROW_TYPE_DIR);
	if (i & ROW_TYPE_MANUFACTURER)
		IRRECO_RETURN_ENUM(ROW_TYPE_MANUFACTURER);
	if (i & ROW_TYPE_MODEL)
		IRRECO_RETURN_ENUM(ROW_TYPE_MODEL);
	if (i & ROW_TYPE_LOADING)
		IRRECO_RETURN_ENUM(ROW_TYPE_LOADING);
	IRRECO_RETURN_INT(0);
}

/**
 * Show hildon progressbar banner.
 *
 * This function will create a new banner if one has not been created yet,
 * if banner already exists, it's properties will be changed.
 *
 * @param text		Text to show.
 * @param fraction	Value of progress.
 */
static void irreco_lircdb_dlg_set_banner(IrrecoLircdbDlg *self,
					const gchar *text,
					gdouble fraction)
{
	IRRECO_ENTER
	if (self->banner == NULL) {
		self->banner = hildon_banner_show_progress(
			GTK_WIDGET(self), NULL, "");
	}

	hildon_banner_set_text(HILDON_BANNER(self->banner), text);
	hildon_banner_set_fraction(HILDON_BANNER(self->banner), fraction);
	IRRECO_RETURN
}

/**
 * Destroy banner, if it exists.
 */
static void irreco_lircdb_dlg_hide_banner(IrrecoLircdbDlg *self)
{
	IRRECO_ENTER
	if (self->banner) {
		gtk_widget_destroy(self->banner);
		self->banner = NULL;
	}
	IRRECO_RETURN
}

/**
 * Start a loader state machine if one is not running already.
 */
static void irreco_lircdb_dlg_loader_start(IrrecoLircdbDlg *self,
					  GSourceFunc function,
					  GtkTreeIter *parent_iter)
{
	IRRECO_ENTER

	if (self->loader_func_id == 0) {
		if (parent_iter) {
			self->loader_parent_iter = gtk_tree_iter_copy(
				parent_iter);
		}

		if (function) {
			self->loader_func_id = g_idle_add(function, self);
		} else {
			IRRECO_ERROR("Loader function pointer not given.\n");
		}
	}

	IRRECO_RETURN
}

/**
 * Stop and cleanup loader if a loader is running.
 */
static void irreco_lircdb_dlg_loader_stop(IrrecoLircdbDlg *self)
{
	IRRECO_ENTER
	if (self->loader_func_id != 0) {
		g_source_remove(self->loader_func_id);
		self->loader_func_id = 0;
		self->loader_state = 0;
		if (self->loader_parent_iter) {
			gtk_tree_iter_free(self->loader_parent_iter);
		}
		self->loader_parent_iter = NULL;
	}
	IRRECO_RETURN
}

/**
 * Load Lircdb directories.
 *
 * This loader will request a list of directories from the Lircdb server and
 * update the TreeView accordingly. Every directory row created by this loader
 * will have row type ROW_TYPE_DIR.
 */
static gboolean irreco_lircdb_dlg_loader_dirs(IrrecoLircdbDlg *self)
{
	IRRECO_ENTER

	switch (self->loader_state) {
	case LOADER_STATE_INIT:
		irreco_lircdb_dlg_set_banner(self, _("Loading ..."), 0);
		self->loader_state = LOADER_STATE_LOOP;
		irreco_lircdb_dlg_clean_details(self);
		IRRECO_RETURN_BOOL(TRUE);

	case LOADER_STATE_LOOP: {
		GtkTreeIter iter;
		GtkTreeIter iter_loading;
		IrrecoStringTable *dirs = NULL;
		IrrecoWebdbCache *webdb_cache = NULL;

		gtk_widget_hide(self->download_button);

		webdb_cache = irreco_data_get_webdb_cache(self->irreco_data,
							  FALSE);
		if(irreco_webdb_cache_get_lirc_dirs(webdb_cache, &dirs)){
			IRRECO_STRING_TABLE_FOREACH_KEY(dirs, key)

				/* Append directory */
				gtk_tree_store_append(self->tree_store,
						      &iter, NULL);
				gtk_tree_store_set(self->tree_store,
						   &iter, TEXT_COL, key,
						   FLAG_COL, ROW_TYPE_DIR,
						   DATA_COL, NULL, -1);

				/* Add loading item into directory */
				gtk_tree_store_append(self->tree_store,
						      &iter_loading, &iter);
				gtk_tree_store_set(self->tree_store,
						   &iter_loading,
						   TEXT_COL, _("Loading ..."),
						   FLAG_COL, ROW_TYPE_LOADING,
						   DATA_COL, NULL, -1);
			IRRECO_STRING_TABLE_FOREACH_END
		} else {
			irreco_error_dlg(GTK_WINDOW(self),
					 irreco_webdb_cache_get_error(
					 webdb_cache));
		}

		irreco_lircdb_dlg_set_banner(self, _("Loading ..."), 1);
		self->loader_state = LOADER_STATE_END;
		IRRECO_RETURN_BOOL(TRUE);
		}

	case LOADER_STATE_END:
		irreco_lircdb_dlg_hide_banner(self);
		irreco_lircdb_dlg_loader_stop(self);
	}

	IRRECO_RETURN_BOOL(FALSE);
}

/**
 * Load Lircdb manufacturers
 *
 * @todo
 */
static gboolean irreco_lircdb_dlg_loader_manuf(IrrecoLircdbDlg *self)
{
	IRRECO_ENTER

	switch (self->loader_state) {
	case LOADER_STATE_INIT:
		irreco_lircdb_dlg_set_banner(self, _("Loading ..."), 0);
		self->loader_state = LOADER_STATE_LOOP;
		IRRECO_RETURN_BOOL(TRUE);

	case LOADER_STATE_LOOP: {
		GtkTreeIter iter;
		GtkTreeIter iter_loading;
		IrrecoStringTable *manufacturers = NULL;
		IrrecoWebdbCache *webdb_cache = NULL;

		gtk_tree_model_get(GTK_TREE_MODEL(self->tree_store),
				   self->loader_parent_iter,
				   TEXT_COL, &self->dir, -1);

		webdb_cache = irreco_data_get_webdb_cache(self->irreco_data,
							  FALSE);

		if(irreco_webdb_cache_get_lirc_manufacturers(webdb_cache,
							self->dir,
		   					&manufacturers)){

			IRRECO_STRING_TABLE_FOREACH_KEY(manufacturers, key)

				/* Add manufacturer item into directory. */
				gtk_tree_store_append(self->tree_store,
				       &iter, self->loader_parent_iter);
				gtk_tree_store_set(self->tree_store,
						&iter,
						TEXT_COL, key,
						FLAG_COL, ROW_TYPE_MANUFACTURER,
						DATA_COL, NULL, -1);

				/* Add loading item into manufacturer. */
				gtk_tree_store_append(self->tree_store,
						      &iter_loading, &iter);
				gtk_tree_store_set(self->tree_store,
						   &iter_loading,
						   TEXT_COL, _("Loading ..."),
						   FLAG_COL, ROW_TYPE_LOADING,
						   DATA_COL, NULL, -1);

			IRRECO_STRING_TABLE_FOREACH_END
		} else {
			irreco_error_dlg(GTK_WINDOW(self),
					 irreco_webdb_cache_get_error(
					 webdb_cache));
		}

		/* Delete loading item from directory. */
		gtk_tree_model_iter_nth_child(GTK_TREE_MODEL(self->tree_store),
					      &iter,self->loader_parent_iter,0);
		if(irreco_lircdb_dlg_row_get_type(self,
						&iter) == ROW_TYPE_LOADING) {
			gtk_tree_store_remove(self->tree_store,&iter);
		}
		irreco_lircdb_dlg_set_banner(self, _("Loading ..."), 1);
		self->loader_state = LOADER_STATE_END;
		IRRECO_RETURN_BOOL(TRUE);
		}

	case LOADER_STATE_END:
		irreco_lircdb_dlg_row_set_loaded(self, self->loader_parent_iter,
						TRUE);
		irreco_lircdb_dlg_hide_banner(self);
		irreco_lircdb_dlg_loader_stop(self);
	}

	IRRECO_RETURN_BOOL(FALSE);
}

/**
 * Load Lircdb models
 *
 * @todo
 */
static gboolean irreco_lircdb_dlg_loader_models(IrrecoLircdbDlg *self)
{
	IRRECO_ENTER

	switch (self->loader_state) {
	case LOADER_STATE_INIT:
		irreco_lircdb_dlg_set_banner(self, _("Loading ..."), 0);
		self->loader_state = LOADER_STATE_LOOP;
		IRRECO_RETURN_BOOL(TRUE);

	case LOADER_STATE_LOOP: {
		GtkTreeIter iter;
		GtkTreeIter dir_iter;
		IrrecoStringTable *models = NULL;
		IrrecoWebdbCache *webdb_cache = NULL;

		/*Get directory and manufacturer*/

		gtk_tree_model_get(GTK_TREE_MODEL(self->tree_store),
				   self->loader_parent_iter,
				   TEXT_COL, &self->manufacturer, -1);

		gtk_tree_model_iter_parent(GTK_TREE_MODEL(self->tree_store),
					   &dir_iter,
					   self->loader_parent_iter);

		gtk_tree_model_get(GTK_TREE_MODEL(self->tree_store),
				   &dir_iter,
				   TEXT_COL, &self->dir, -1);

		webdb_cache = irreco_data_get_webdb_cache(self->irreco_data,
							  FALSE);

		self->file_path = g_strdup_printf("%s/%s/", self->dir,
					self->manufacturer);

		if(irreco_webdb_cache_get_lirc_models(webdb_cache, self->file_path,
						 &models)){

			IRRECO_STRING_TABLE_FOREACH_KEY(models, key)

				/* Add model item into manufacturer. */
				gtk_tree_store_append(self->tree_store,
				       &iter, self->loader_parent_iter);
				gtk_tree_store_set(self->tree_store,
						&iter,
						TEXT_COL, key,
						FLAG_COL, ROW_TYPE_MODEL,
						DATA_COL, NULL, -1);

			IRRECO_STRING_TABLE_FOREACH_END
		} else {
			irreco_error_dlg(GTK_WINDOW(self),
					 irreco_webdb_cache_get_error(
					 webdb_cache));
		}

		/* Delete loading item from manufacturer. */
		gtk_tree_model_iter_nth_child(GTK_TREE_MODEL(self->tree_store),
					      &iter, self->loader_parent_iter,
	   				      0);
		if(irreco_lircdb_dlg_row_get_type(self,
						&iter) == ROW_TYPE_LOADING) {
			gtk_tree_store_remove(self->tree_store, &iter);
		}

		irreco_lircdb_dlg_set_banner(self, _("Loading ..."), 1);
		self->loader_state = LOADER_STATE_END;

		IRRECO_RETURN_BOOL(TRUE);
	}
	case LOADER_STATE_END:
		irreco_lircdb_dlg_row_set_loaded(self, self->loader_parent_iter,
						TRUE);
		irreco_lircdb_dlg_hide_banner(self);
		irreco_lircdb_dlg_loader_stop(self);
	}

	IRRECO_RETURN_BOOL(FALSE);
}

/**
 * Get filepath and show D/L button.
 *
 * @todo
 */
static gboolean irreco_lircdb_dlg_ready_to_dl(IrrecoLircdbDlg *self,
						       GtkTreeIter * iter)
{
	IRRECO_ENTER

	gtk_tree_model_get(GTK_TREE_MODEL(self->tree_store),
				iter, TEXT_COL, &self->model, -1);

	self->file_path = g_strdup_printf("%s/%s/%s", self->dir,
				self->manufacturer, self->model);

	gtk_widget_show(self->download_button);

	IRRECO_RETURN_BOOL(TRUE);
}


/**
 * Load config file from Lircdb.
 *
 * @todo
 */
static gboolean irreco_lircdb_dlg_load_file(IrrecoLircdbDlg *self)
{
	IrrecoWebdbCache		*webdb_cache	= NULL;
	IrrecoBackendInstance		*inst		= NULL;
	GString				*error_msg	= g_string_new("");
	IrrecoBackendManager		*manager;
	IrrecoBackendFileContainer	*file_container;
	IrrecoStringTable		*file = NULL;
	gboolean			success		= TRUE;
	gboolean			new_instance	= FALSE;
	gint				instances;
	gchar				*backend = "Internal Lirc";
	gchar			*file_contents = NULL;
	IRRECO_ENTER

	webdb_cache = irreco_data_get_webdb_cache(self->irreco_data,
						  FALSE);

	if(irreco_webdb_cache_get_lirc_file(webdb_cache, self->file_path, &file)) {
		IRRECO_STRING_TABLE_FOREACH_KEY(file, key)
			if(file_contents == NULL) {
				file_contents = g_strdup_printf("%s", key);
			} else {
				file_contents = g_strdup_printf(
						"%s%s", file_contents, key);
			}
		IRRECO_STRING_TABLE_FOREACH_END
	} else {
		goto end;
	}

	webdb_cache = NULL;

	/* Search backend */
	manager = self->irreco_data->irreco_backend_manager;
	IRRECO_BACKEND_MANAGER_FOREACH_LIB(manager, lib)
		if (g_utf8_collate(lib->name, backend) != 0){
			continue;
		}
		if (!(lib->api->flags & IRRECO_BACKEND_EDITABLE_DEVICES)) {
			g_string_printf(error_msg,
			"\"%s\" backend is not editable...",
			backend);
			goto end;
		}
		else if (!(lib->api->flags &
			 IRRECO_BACKEND_CONFIGURATION_EXPORT)) {
			g_string_printf(error_msg,
			"\"%s\" backend doesn't support "
			"exporting its configuration...",
			backend);
			goto end;
		}
		/* Check if many instances */
		instances = 0;
		IRRECO_BACKEND_MANAGER_FOREACH_INSTANCE(manager, instance)
			irreco_backend_instance_get_description(instance);
			if (g_utf8_collate(instance->lib->name,
				    backend) == 0) {
				instances++;

				/*if there is only one instance, then use it */
				inst = instance;
			}
		IRRECO_STRING_TABLE_FOREACH_END
		IRRECO_DEBUG("INSTANCES: %d\n",instances);

		/* Create new instance if it comes to the crunch */
		if (instances == 0 || lib->api->flags &
		    IRRECO_BACKEND_NEW_INST_ON_CONF_IMPORT) {
			inst = irreco_backend_manager_create_instance(
			manager, lib, NULL, NULL);
			new_instance = TRUE;
			break;
		}

		/* Select instace */
		if (instances > 1) {
			if (!irreco_show_select_instance_dlg(
			    self->irreco_data, GTK_WINDOW(self),
			    backend, &inst)) {
				g_string_printf(error_msg,
				"Operation aborted by user...");
				goto end;
			}
		}
		goto instance_ready;
	IRRECO_STRING_TABLE_FOREACH_END

	if (inst == NULL) {
		g_string_printf(error_msg, "\"%s\" backend is not installed...",
				backend);
		goto end;
	}

	irreco_backend_instance_configure(inst, GTK_WINDOW(self));
	irreco_backend_instance_save_to_conf(inst);
	irreco_config_save_backends(manager);

	instance_ready:
	g_print("instance_ready: \n");

	file_container = irreco_backend_file_container_new();
	irreco_backend_file_container_set(file_container,
					  inst->lib->name,
					  backend,
					  self->manufacturer,
					  self->model,
					  self->model,
					  file_contents);

	if (irreco_backend_instance_check_conf(inst, file_container)) {
		GString *question = g_string_new("");
		g_string_printf(question, "\"%s\" backend already contains\n"
				"device \"%s\".\n"
				"Do you want to overwrite?",
				inst->lib->name,self->model);
		success = irreco_yes_no_dlg(GTK_WINDOW(self), question->str);
		g_string_free(question, TRUE);

		if(success == FALSE) {
			goto end;
		}
	}

	/* Send file_data for backend */
	if(irreco_backend_instance_import_conf(inst, file_container)) {
		irreco_info_dlg(GTK_WINDOW(self),
				"Configuration downloaded successfully!");
		irreco_backend_manager_get_devcmd_lists(
			       self->irreco_data->irreco_backend_manager);
	} else {
		g_string_printf(error_msg, "Backend error");

		if (new_instance) {
			irreco_backend_manager_destroy_instance(manager, inst);
		}
		goto end;
	}

	end:
	if(error_msg->len > 0) {
		irreco_error_dlg(GTK_WINDOW(self), error_msg->str);
		success = FALSE;
	}

	g_free(file_contents);
	g_string_free(error_msg, TRUE);
	IRRECO_RETURN_BOOL(success);
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Functions                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */

void irreco_lircdb_dlg_set_irreco_data(IrrecoLircdbDlg *self,
				      IrrecoData *irreco_data)
{
	IRRECO_ENTER
	self->irreco_data = irreco_data;
	IRRECO_RETURN
}

void irreco_show_lircdb_dlg(IrrecoData *irreco_data, GtkWindow *parent)
{
	IrrecoLircdbDlg	*self;
	gint		response;
	gboolean	loop = TRUE;
	IRRECO_ENTER

	self = IRRECO_LIRCDB_DLG(irreco_lircdb_dlg_new(irreco_data, parent));

	do {
		response = gtk_dialog_run(GTK_DIALOG(self));
		switch (response) {
		case IRRECO_DEVICE_REFRESH:
			IRRECO_DEBUG("IRRECO_DEVICE_REFRESH\n");
			gtk_tree_store_clear(self->tree_store);
			irreco_data_get_webdb_cache(self->irreco_data, TRUE);
			irreco_lircdb_dlg_loader_start(self,
				G_SOURCEFUNC(irreco_lircdb_dlg_loader_dirs),
				NULL);
			break;

		case GTK_RESPONSE_DELETE_EVENT:
			IRRECO_DEBUG("GTK_RESPONSE_DELETE_EVENT\n");
			loop = FALSE;
			break;

		case GTK_RESPONSE_OK:
			IRRECO_DEBUG("GTK_RESPONSE_OK\n");
			loop = !irreco_lircdb_dlg_load_file(self);
			break;

		}
	} while (loop);

	gtk_widget_destroy(GTK_WIDGET(self));
	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Events and Callbacks                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Events and Callbacks
 * @{
 */

static gboolean irreco_show_lircdb_dlg_map_event(IrrecoLircdbDlg *self,
						GdkEvent *event,
						gpointer user_data)
{
	IRRECO_ENTER
	irreco_lircdb_dlg_loader_start(
		self, G_SOURCEFUNC(irreco_lircdb_dlg_loader_dirs), NULL);
	IRRECO_RETURN_BOOL(FALSE);
}

static void irreco_show_lircdb_dlg_destroy_event(IrrecoLircdbDlg *self,
						gpointer user_data)
{
	IRRECO_ENTER
	irreco_lircdb_dlg_loader_stop(self);
	IRRECO_RETURN
}

static void irreco_show_lircdb_dlg_row_activated_event(GtkTreeView *tree_view,
						      GtkTreePath *path,
						      GtkTreeViewColumn *column,
						      IrrecoLircdbDlg *self)
{
	IRRECO_ENTER

	if (gtk_tree_view_row_expanded(tree_view, path)) {
		g_print("was expanded\n");
		gtk_tree_view_expand_row(tree_view, path, FALSE);
		g_print("tried expand row\n");
	} else {
		g_print("was not expanded\n");
		gtk_tree_view_collapse_row(tree_view, path);
		g_print("tried collapsing row\n");
	}
	IRRECO_RETURN
}

static void irreco_show_lircdb_dlg_row_expanded_event(GtkTreeView *tree_view,
						     GtkTreeIter *iter,
						     GtkTreePath *path,
						     IrrecoLircdbDlg *self)
{
	IRRECO_ENTER

	irreco_lircdb_dlg_clean_details(self);

	if (self->loader_func_id != 0) {
		gtk_tree_view_collapse_row(tree_view, path);
	}

	if (!irreco_lircdb_dlg_row_is_loaded(self, iter)) {
		switch (irreco_lircdb_dlg_row_get_type(self, iter)) {
		case ROW_TYPE_DIR:
			irreco_lircdb_dlg_loader_start(self,
				G_SOURCEFUNC(irreco_lircdb_dlg_loader_manuf),
				iter);
			gtk_widget_hide(self->download_button);
			break;

		case ROW_TYPE_MANUFACTURER:
			irreco_lircdb_dlg_loader_start(self,
				G_SOURCEFUNC(irreco_lircdb_dlg_loader_models),
				iter);
			gtk_widget_hide(self->download_button);
			break;
		}
	}

	IRRECO_RETURN
}

static void irreco_show_lircdb_dlg_row_collapsed_event(GtkTreeView *tree_view,
						     GtkTreeIter *iter,
						     GtkTreePath *path,
						     IrrecoLircdbDlg *self)
{
	IRRECO_ENTER

	irreco_lircdb_dlg_clean_details(self);

	IRRECO_RETURN
}

static void irreco_show_lircdb_dlg_row_selected_event(GtkTreeSelection *sel,
						     IrrecoLircdbDlg *self)
{
	GtkTreeIter iter;
	GtkTreeModel *model;

	IRRECO_ENTER

	if (gtk_tree_selection_get_selected (sel, &model, &iter))
	{
		switch (irreco_lircdb_dlg_row_get_type(self, &iter)) {

		case ROW_TYPE_MODEL:
			IRRECO_DEBUG("ROW_TYPE_MODEL\n");
			irreco_lircdb_dlg_ready_to_dl(self, &iter);
			break;

		default:
			irreco_lircdb_dlg_clean_details(self);
			break;
		}
	}

	IRRECO_RETURN
}

/** @} */
/** @} */


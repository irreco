/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * irreco - Ir Remote Control
 * Copyright (C) 2008 Pekka Gehör (pegu6@msn.com)
 *
 * irreco_theme_creator_backgrounds.c is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * irreco_theme_creator_backgrounds.c is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @addtogroup IrrecoThemeCreatorBackgrounds
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoThemeCreatorBackgrounds.
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_THEME_CREATOR_BACKGROUNDS_H_TYPEDEF__
#define __IRRECO_THEME_CREATOR_BACKGROUNDS_H_TYPEDEF__

typedef struct _IrrecoThemeCreatorBackgroundsClass IrrecoThemeCreatorBackgroundsClass;
typedef struct _IrrecoThemeCreatorBackgrounds IrrecoThemeCreatorBackgrounds;

#define IRRECO_TYPE_THEME_CREATOR_BACKGROUNDS             (irreco_theme_creator_backgrounds_get_type ())
#define IRRECO_THEME_CREATOR_BACKGROUNDS(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), IRRECO_TYPE_THEME_CREATOR_BACKGROUNDS, IrrecoThemeCreatorBackgrounds))
#define IRRECO_THEME_CREATOR_BACKGROUNDS_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), IRRECO_TYPE_THEME_CREATOR_BACKGROUNDS, IrrecoThemeCreatorBackgroundsClass))
#define IRRECO_IS_THEME_CREATOR_BACKGROUNDS(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), IRRECO_TYPE_THEME_CREATOR_BACKGROUNDS))
#define IRRECO_IS_THEME_CREATOR_BACKGROUNDS_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), IRRECO_TYPE_THEME_CREATOR_BACKGROUNDS))
#define IRRECO_THEME_CREATOR_BACKGROUNDS_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), IRRECO_TYPE_THEME_CREATOR_BACKGROUNDS, IrrecoThemeCreatorBackgroundsClass))

#endif /* __IRRECO_THEME_CREATOR_BACKGROUNDS_H_TYPEDEF__ */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef _IRRECO_THEME_CREATOR_BACKGROUNDS_H_TYPEDEF_
#define _IRRECO_THEME_CREATOR_BACKGROUNDS_H_TYPEDEF_
#include "irreco.h"
#include "irreco_internal_widget.h"
#include "irreco_listbox.h"
#include "irreco_theme.h"
#include "irreco_button.h"
#include "irreco_listbox_image.h"
#include "irreco_bg_browser_widget.h"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/


struct _IrrecoThemeCreatorBackgrounds
{
	IrrecoInternalWidget		parent;
	GtkWindow			*parent_window;
	IrrecoTheme			*theme;
	IrrecoData			*irreco_data;

	GtkCellRenderer			*renderer_backgrounds;
	GtkTreeViewColumn		*column_backgrounds;
	GtkListStore			*store_backgrounds;
	GtkWidget			*view_backgrounds;
	GtkWidget			*scroll_backgrounds;
	GtkTreeSelection		*tree_selection;

	gint				loader_index;
	gint				loader_state;
	gint				loader_func_id;
	GtkTreeIter			*loader_iter;

	IrrecoThemeBg			*current_bg;

	gint 				sel_index;
};

struct _IrrecoThemeCreatorBackgroundsClass
{
	IrrecoInternalWidgetClass parent_class;
};

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
GType irreco_theme_creator_backgrounds_get_type (void);
GtkWidget* irreco_theme_creator_backgrounds_new(GtkWindow *parent,
						IrrecoData *irreco_data,
      						IrrecoTheme * irreco_theme);
void irreco_theme_creator_backgrounds_set_parent_window(
					IrrecoThemeCreatorBackgrounds *self,
     					GtkWindow *parent);
IrrecoThemeBg
*irreco_theme_creator_backgrounds_get_selected_bg(IrrecoThemeCreatorBackgrounds
						  *self);
gboolean
irreco_theme_creator_backgrounds_remove_selected(IrrecoThemeCreatorBackgrounds
						 *self);
void
irreco_theme_creator_backgrounds_refresh(
					 IrrecoThemeCreatorBackgrounds *self);

#endif /* __IRRECO_THEME_CREATOR_BACKGROUNDS_H__ */

/** @} */

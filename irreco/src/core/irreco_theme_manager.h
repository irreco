/*
 * irreco - Ir Remote Control
 * Copyright (C) 2008 Joni Kokko (t5kojo01@students.oamk.fi)
 *                    Sami Mäki (kasmra@xob.kapsi.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_THEME_MANAGER_H_TYPEDEF__
#define __IRRECO_THEME_MANAGER_H_TYPEDEF__
typedef struct _IrrecoThemeManager IrrecoThemeManager;
#endif /* __IRRECO_THEME_MANAGER_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_THEME_MANAGER_H__
#define __IRRECO_THEME_MANAGER_H__
#include "irreco.h"
#include "irreco_theme.h"
#include "irreco_button.h"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @addtogroup IrrecoThemeManager
 *
 * @{
 */

struct _IrrecoThemeManager
{
	IrrecoStringTable *themes;
	IrrecoData *irreco_data;
};

/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

IrrecoThemeManager *irreco_theme_manager_new(IrrecoData * irreco_data);
void irreco_theme_manager_free(IrrecoThemeManager *self);
void irreco_theme_manager_print(IrrecoThemeManager *self);
IrrecoStringTable *irreco_theme_manager_get_themes(IrrecoThemeManager *self);
IrrecoStringTable *irreco_theme_manager_get_buttons(IrrecoThemeManager *self,
						    const gchar *theme_name);
IrrecoStringTable *irreco_theme_manager_get_backgrounds(
						IrrecoThemeManager *self,
						const gchar *theme_name);
void irreco_theme_manager_read_themes_from_dir(IrrecoThemeManager *self,
					       const gchar *dir);
gboolean irreco_theme_manager_get_button_style(IrrecoThemeManager *self,
					       const gchar *style_name,
					       IrrecoThemeButton **style);
gboolean irreco_theme_manager_does_deb_exist(IrrecoThemeManager *self);
gboolean irreco_theme_manager_does_web_exist(IrrecoThemeManager *self);
gboolean irreco_theme_manager_does_user_exist(IrrecoThemeManager *self);
gboolean irreco_theme_manager_remove_theme(IrrecoThemeManager *self,
                                           const gchar *theme_name);
void irreco_theme_manager_update_theme_manager(IrrecoThemeManager *self);

#endif /* __IRRECO_THEME_MANAGER_H__ */



/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_cmd_dlg.h"
#include "irreco_backend_dlg.h"
#include "irreco_backend_instance.h"
#include <hildon/hildon-pannable-area.h>

/**
 * @addtogroup IrrecoCmdDlg
 * @ingroup Irreco
 *
 * Show a dialog where the user can select one command from a list of all
 * possible commands
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoCmdDlg.
 */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Types and definitions.                                                     */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/


#define IRRECO_SELECT_CMD_DLG_BUTTON_ROW_WIDTH		520
#define IRRECO_SELECT_CMD_DLG_BUTTON_PAD		5
#define IRRECO_SELECT_CMD_DLG_EXPAND_BUTTONS		FALSE

enum
{
	IRRECO_RESPONCE_EDIT = 1,
	IRRECO_RESPONCE_REFRESH,
	IRRECO_RESPONCE_COMMAND_SET
};

typedef struct _IrrecoButtonAppend IrrecoButtonAppend;
struct _IrrecoButtonAppend {
	GtkWidget *vbox;
	GtkWidget *hbox;
	uint width;
	uint count;
};

typedef struct _IrrecoSelectCmdDlg IrrecoSelectCmdDlg;
struct _IrrecoSelectCmdDlg {
	IrrecoData	*irreco_data;
	IrrecoCmd	*command;
	GtkWidget	*dialog;
	GtkWidget	*scrolled;
	GtkWidget	*vbox;
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes.                                                                */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/


void irreco_cmd_dlg_create_command_list(IrrecoSelectCmdDlg * cmd_dlg);
void irreco_cmd_dlg_append_device(IrrecoSelectCmdDlg * cmd_dlg,
				  IrrecoBackendDevice * device);
void irreco_cmd_dlg_append_special(IrrecoSelectCmdDlg * cmd_dlg);
void irreco_cmd_dlg_append_wait_button(IrrecoSelectCmdDlg * cmd_dlg,
				       IrrecoButtonAppend * button_append,
				       uint delay);
GtkWidget *irreco_cmd_dlg_append_hbox(GtkWidget *vbox,
				      guint padding_between,
				      guint padding_top,
				      guint padding_bottom,
				      guint padding_left,
				      guint padding_right,
				      gfloat x_expand_scale,
				      gfloat y_expand_scale);
GtkWidget *irreco_cmd_dlg_append_title_hbox(GtkWidget *vbox);
GtkWidget *irreco_cmd_dlg_append_command_hbox(GtkWidget *vbox);
void irreco_cmd_dlg_append_button_init(IrrecoButtonAppend * data,
				       GtkWidget *vbox);
GtkWidget *irreco_cmd_dlg_append_button(IrrecoButtonAppend * data,
					const gchar *tite);



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Run.                                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Run
 * @{
 */


gboolean irreco_cmd_dlg_run(IrrecoData *irreco_data, IrrecoCmd * command,
			    GtkWindow *parent)
{
	IrrecoSelectCmdDlg cmd_dlg;
	gint rvalue = -1;
	IRRECO_ENTER

	memset(&cmd_dlg, '\0', sizeof(cmd_dlg));
	cmd_dlg.irreco_data = irreco_data;
	cmd_dlg.command = command;

	/* Create objects. */
	cmd_dlg.dialog = gtk_dialog_new_with_buttons(
		_("Select command"), parent,
		/*irreco_window_manager_get_gtk_window(
		irreco_data->window_manager),*/
		GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT /* |
		GTK_DIALOG_NO_SEPARATOR*/,
		_("Refresh"), IRRECO_RESPONCE_REFRESH,
		/*_("Device controllers"), IRRECO_RESPONCE_EDIT,*/
		NULL);
	cmd_dlg.scrolled = hildon_pannable_area_new();

	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(cmd_dlg.dialog)->vbox),
			  cmd_dlg.scrolled);
	irreco_cmd_dlg_create_command_list(&cmd_dlg);

	/* Min width, max height. */
	gtk_window_resize(GTK_WINDOW(cmd_dlg.dialog), 1, 300);
	gtk_widget_show_all(cmd_dlg.dialog);

	while (rvalue == -1) {
		switch (gtk_dialog_run(GTK_DIALOG(cmd_dlg.dialog))) {
		case GTK_RESPONSE_DELETE_EVENT:
			rvalue = FALSE;
			break;

		case IRRECO_RESPONCE_COMMAND_SET:
			rvalue = TRUE;
			break;

		case IRRECO_RESPONCE_EDIT:
			irreco_show_backend_dlg(irreco_data,
					       GTK_WINDOW(cmd_dlg.dialog));
			irreco_cmd_dlg_create_command_list(&cmd_dlg);
			break;

		case IRRECO_RESPONCE_REFRESH:
			irreco_backend_manager_get_devcmd_lists(
				irreco_data->irreco_backend_manager);
			irreco_cmd_dlg_create_command_list(&cmd_dlg);
			break;
		}
	}

	gtk_widget_destroy(cmd_dlg.dialog);
	IRRECO_RETURN_INT(rvalue);
}

void irreco_cmd_dlg_create_command_list(IrrecoSelectCmdDlg * cmd_dlg)
{
	const gchar *name = NULL;
	IRRECO_ENTER

	if (cmd_dlg->vbox != NULL) {
		gtk_widget_destroy(cmd_dlg->vbox);
	}
	cmd_dlg->vbox = gtk_vbox_new(FALSE, 0);
	hildon_pannable_area_add_with_viewport(HILDON_PANNABLE_AREA(cmd_dlg->scrolled),
					       GTK_WIDGET(cmd_dlg->vbox));


	/* Iterate trough instance, device and command arrays. */
	IRRECO_DEBUG("Generating command list\n");
	IRRECO_BACKEND_MANAGER_FOREACH_INSTANCE(
	    cmd_dlg->irreco_data->irreco_backend_manager, instance)

		name = irreco_backend_instance_get_name(instance);
		IRRECO_DEBUG("Instance: \"%s\"\n", name);

		IRRECO_BACKEND_INSTANCE_FOREACH(instance, device)
			IRRECO_DEBUG("Device: \"%s\"\n", device->name);
			irreco_cmd_dlg_append_device(cmd_dlg, device);
		IRRECO_BACKEND_INSTANCE_FOREACH_END

	IRRECO_STRING_TABLE_FOREACH_END

	IRRECO_DEBUG("Special commands.\n");
	irreco_cmd_dlg_append_special(cmd_dlg);
	gtk_widget_show_all(cmd_dlg->dialog);

	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Signal handlers.                                                           */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Signal handlers
 * @{
 */


void irreco_cmd_dlg_set_built_in(GtkButton * button,
				 IrrecoSelectCmdDlg * cmd_dlg)
{
	gpointer type;
	IRRECO_ENTER

	type = g_object_get_data(G_OBJECT(button), "IrrecoCmdType");
	irreco_cmd_set_builtin(cmd_dlg->command, (IrrecoCmdType) type);
	gtk_dialog_response(GTK_DIALOG(cmd_dlg->dialog),
			    IRRECO_RESPONCE_COMMAND_SET);

	IRRECO_RETURN
}

void irreco_cmd_dlg_set_layout(GtkButton * button,
			       IrrecoSelectCmdDlg * cmd_dlg)
{
	gpointer name;
	IRRECO_ENTER

	name = g_object_get_data(G_OBJECT(button), "IrrecoCmdLayout");
	irreco_cmd_set_layout(cmd_dlg->command, (const gchar *) name);
	gtk_dialog_response(GTK_DIALOG(cmd_dlg->dialog),
			    IRRECO_RESPONCE_COMMAND_SET);

	IRRECO_RETURN
}

void irreco_cmd_dlg_set_backend(GtkButton * button,
			       IrrecoSelectCmdDlg * cmd_dlg)
{
	IrrecoBackendDevice *device;
	const gchar *command;
	IRRECO_ENTER

	device = (IrrecoBackendDevice *) g_object_get_data(
		G_OBJECT(button), "IrrecoCmdBackendDevice");
	command = (const gchar *) g_object_get_data(
		G_OBJECT(button), "IrrecoCmdBackendCommand");
	irreco_cmd_set_backend(cmd_dlg->command,
				  device->backend_instance,
				  device->name,
				  command);
	gtk_dialog_response(GTK_DIALOG(cmd_dlg->dialog),
			    IRRECO_RESPONCE_COMMAND_SET);

	IRRECO_RETURN
}

void irreco_cmd_dlg_set_wait(GtkButton * button, IrrecoSelectCmdDlg * cmd_dlg)
{
	gpointer delay;
	IRRECO_ENTER

	delay = g_object_get_data(G_OBJECT(button), "IrrecoCmdWaitDelay");
	irreco_cmd_set_wait(cmd_dlg->command, (gulong) delay);
	gtk_dialog_response(GTK_DIALOG(cmd_dlg->dialog),
			    IRRECO_RESPONCE_COMMAND_SET);

	IRRECO_RETURN
}


/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Dialog construction.                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Dialog construction
 * @{
 */


void irreco_cmd_dlg_append_commands(IrrecoSelectCmdDlg * cmd_dlg,
				    IrrecoBackendDevice * device)
{
	IrrecoButtonAppend button_append;
	GtkWidget *button;
	IRRECO_ENTER

	irreco_cmd_dlg_append_button_init(&button_append, cmd_dlg->vbox);
	IRRECO_BACKEND_DEVICE_FOREACH(device, command)
		button = irreco_cmd_dlg_append_button(&button_append,
							     command);
		g_object_set_data(G_OBJECT(button),
				  "IrrecoCmdBackendDevice",
				  (gpointer) device);
		g_object_set_data(G_OBJECT(button),
				  "IrrecoCmdBackendCommand",
				  (gpointer) command);
		g_signal_connect(G_OBJECT(button), "clicked",
				 G_CALLBACK(irreco_cmd_dlg_set_backend),
				 cmd_dlg);
	IRRECO_BACKEND_DEVICE_FOREACH_END
	IRRECO_RETURN
}

void irreco_cmd_dlg_append_device(IrrecoSelectCmdDlg * cmd_dlg,
				  IrrecoBackendDevice * device)
{
	GtkWidget *hbox;
	GtkWidget *arrow;
	GtkWidget *arrow_align;
	GtkWidget *device_label;
	GtkWidget *instance_label;
	IRRECO_ENTER

	hbox = irreco_cmd_dlg_append_title_hbox(cmd_dlg->vbox);
	arrow = gtk_arrow_new(GTK_ARROW_RIGHT, GTK_SHADOW_NONE);
	arrow_align = gtk_alignment_new(0.5, 0.5, 1, 1);
	device_label = gtk_label_new(device->name);
	instance_label = gtk_label_new(
		irreco_backend_instance_get_name(device->backend_instance));

	/*
	instance_label = gtk_label_new(
		irreco_backend_instance_get_name_and_description(
			device->backend_instance));
	*/

	/* Arrow: 25px width, auto height, 5px padding left and right.*/
	gtk_widget_set_size_request(arrow, 25, 0);
	gtk_misc_set_alignment(GTK_MISC(arrow), 0.5, 0.5);
	gtk_alignment_set_padding(GTK_ALIGNMENT(arrow_align), 0, 0, 8, 8);

	gtk_misc_set_alignment(GTK_MISC(device_label), 0, 0.5);
	gtk_misc_set_alignment(GTK_MISC(instance_label), 0, 0.5);

	gtk_container_add(GTK_CONTAINER(arrow_align), arrow);
	gtk_box_pack_start(GTK_BOX(hbox), instance_label, 0, 0, 0);
	gtk_box_pack_start(GTK_BOX(hbox), arrow_align, 0, 0, 0);
	gtk_box_pack_start(GTK_BOX(hbox), device_label, 0, 0, 0);

	if (irreco_backend_device_is_empty(device)) {
		GtkWidget *label = gtk_label_new(_("This device does not "
						 "have any commands."));
		hbox = irreco_cmd_dlg_append_command_hbox(cmd_dlg->vbox);
		gtk_box_pack_start_defaults(GTK_BOX(hbox), label);
	} else {
		/*GtkWidget *command_vbox = gtk_vbox_new(FALSE, 10);*/
		irreco_cmd_dlg_append_commands(cmd_dlg, device);
	}

	IRRECO_RETURN
}



void irreco_cmd_dlg_append_special(IrrecoSelectCmdDlg * cmd_dlg)
{
	GtkWidget *hbox;
	GtkWidget *label;
	GtkWidget *button;
	IrrecoButtonAppend button_append;
	GString *string;
	IRRECO_ENTER

	hbox = irreco_cmd_dlg_append_title_hbox(cmd_dlg->vbox);
	label = gtk_label_new(_("Special commands"));
	gtk_box_pack_start(GTK_BOX(hbox), label, 0, 0, 0);

	/* Create next remote button. */
	irreco_cmd_dlg_append_button_init(&button_append, cmd_dlg->vbox);
	button = irreco_cmd_dlg_append_button(
		&button_append, _(IRRECO_COMMAND_NEXT_REMOTE_TITLE));
	g_object_set_data(G_OBJECT(button), "IrrecoCmdType",
			  (gpointer) IRRECO_COMMAND_NEXT_REMOTE);
	g_signal_connect(G_OBJECT(button), "clicked",
			 G_CALLBACK(irreco_cmd_dlg_set_built_in),
			 cmd_dlg);

	/* Create previous remote button. */
	button = irreco_cmd_dlg_append_button(
		&button_append, _(IRRECO_COMMAND_PREVIOUS_REMOTE_TITLE));
	g_object_set_data(G_OBJECT(button), "IrrecoCmdType",
			  (gpointer) IRRECO_COMMAND_PREVIOUS_REMOTE);
	g_signal_connect(G_OBJECT(button), "clicked",
			 G_CALLBACK(irreco_cmd_dlg_set_built_in),
			 cmd_dlg);

	/* Fullscreen toggle button. */
/*	button = irreco_cmd_dlg_append_button(
		&button_append, _(IRRECO_COMMAND_FULLSCREEN_TOGGLE_TITLE));
	g_object_set_data(G_OBJECT(button), "IrrecoCmdType",
			  (gpointer) IRRECO_COMMAND_FULLSCREEN_TOGGLE);
	g_signal_connect(G_OBJECT(button), "clicked",
			 G_CALLBACK(irreco_cmd_dlg_set_built_in),
			 cmd_dlg);*/

	/* Create show remote buttons. */
	string = g_string_new("");
	IRRECO_STRING_TABLE_FOREACH_KEY(
	    cmd_dlg->irreco_data->irreco_layout_array, layout_name)
		g_string_append(g_string_set_size(string, 0),
				_(IRRECO_COMMAND_SHOW_LAYOUT_TITLE_PREFIX));
		g_string_append(string, layout_name);
		button = irreco_cmd_dlg_append_button(&button_append,
							     string->str);
		g_object_set_data(G_OBJECT(button), "IrrecoCmdLayout",
				  (gpointer) layout_name);
		g_signal_connect(G_OBJECT(button), "clicked",
				 G_CALLBACK(irreco_cmd_dlg_set_layout),
				 cmd_dlg);
	IRRECO_STRING_TABLE_FOREACH_END
	g_string_free(string, TRUE);

	/* Append wait buttons. */
	irreco_cmd_dlg_append_wait_button(cmd_dlg, &button_append,
					  IRRECO_SECONDS_TO_USEC(0.3));
	irreco_cmd_dlg_append_wait_button(cmd_dlg, &button_append,
					  IRRECO_SECONDS_TO_USEC(0.6));
	irreco_cmd_dlg_append_wait_button(cmd_dlg, &button_append,
					  IRRECO_SECONDS_TO_USEC(1));
	irreco_cmd_dlg_append_wait_button(cmd_dlg, &button_append,
					  IRRECO_SECONDS_TO_USEC(1.5));
	irreco_cmd_dlg_append_wait_button(cmd_dlg, &button_append,
  					  IRRECO_SECONDS_TO_USEC(2));
	irreco_cmd_dlg_append_wait_button(cmd_dlg, &button_append,
  					  IRRECO_SECONDS_TO_USEC(3));
	irreco_cmd_dlg_append_wait_button(cmd_dlg, &button_append,
  					  IRRECO_SECONDS_TO_USEC(5));
	irreco_cmd_dlg_append_wait_button(cmd_dlg, &button_append,
  					  IRRECO_SECONDS_TO_USEC(7));
	irreco_cmd_dlg_append_wait_button(cmd_dlg, &button_append,
  					  IRRECO_SECONDS_TO_USEC(10));
	IRRECO_RETURN
}

void irreco_cmd_dlg_append_wait_button(IrrecoSelectCmdDlg * cmd_dlg,
				       IrrecoButtonAppend * button_append,
				       uint delay)
{
	GtkWidget *button;
	IrrecoCmd *command;
	IRRECO_ENTER

	/* Set title. */
	command = irreco_cmd_create();
	irreco_cmd_set_wait(command, delay);
	button = irreco_cmd_dlg_append_button(
		button_append, irreco_cmd_get_short_name(command));
	irreco_cmd_destroy(command);

	/* Set title.
	string = g_string_new(NULL);
	g_string_printf(string, _(IRRECO_COMMAND_WAIT_TITLE_FORMAT),
			(float)(delay) / 1000);

	g_string_free(string, TRUE);
	*/

	/* Set callback. */
	g_object_set_data(G_OBJECT(button), "IrrecoCmdWaitDelay",
			  (gpointer) delay);
	g_signal_connect(G_OBJECT(button), "clicked",
			 G_CALLBACK(irreco_cmd_dlg_set_wait),
			 cmd_dlg);
	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* GtkHBox utility functions.                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name GtkHBox utility functions
 * @{
 */


GtkWidget *irreco_cmd_dlg_append_hbox(GtkWidget * vbox,
				      guint padding_between,
				      guint padding_top,
				      guint padding_bottom,
				      guint padding_left,
				      guint padding_right,
				      gfloat x_expand_scale,
				      gfloat y_expand_scale)
{
	GtkWidget *hbox;
	GtkWidget *align;
	IRRECO_ENTER

	hbox = gtk_hbox_new(FALSE, padding_between);
	align = irreco_gtk_align(hbox, 0, 0, x_expand_scale, y_expand_scale,
				 padding_top, padding_bottom, padding_left,
				 padding_right);
	gtk_box_pack_start(GTK_BOX(vbox), align, 0, 0, 0);
	IRRECO_RETURN_PTR(hbox);
}

void irreco_cmd_dlg_append_separator(GtkWidget * vbox,
				     guint padding_top,
				     guint padding_bottom)
{
	GtkWidget *align;
	IRRECO_ENTER

	align = irreco_gtk_align(gtk_hseparator_new(), 0, 0, 1, 1,
				 padding_top, padding_bottom, 20, 0);
	gtk_box_pack_start(GTK_BOX(vbox), align, 0, 0, 0);
	IRRECO_RETURN
}

GtkWidget *irreco_cmd_dlg_append_title_hbox(GtkWidget * vbox)
{
	GtkWidget* hbox;
	IRRECO_ENTER

	hbox = irreco_cmd_dlg_append_hbox(vbox,
		0,	/*padding_between	*/
		15,	/*padding_top		*/
		0,	/*padding_bottom	*/
		25,	/*padding_left		*/
		20,	/*padding_right		*/
		0,	/* x_expand_scale	*/
		0	/* y_expand_scale	*/
		);
	irreco_cmd_dlg_append_separator(vbox, 2, 6);
	IRRECO_RETURN_PTR(hbox);
}

GtkWidget *irreco_cmd_dlg_append_command_hbox(GtkWidget * vbox)
{
	return irreco_cmd_dlg_append_hbox(vbox,
		IRRECO_SELECT_CMD_DLG_BUTTON_PAD, 	/* padding_between */
		0,					/* padding_top     */
		IRRECO_SELECT_CMD_DLG_BUTTON_PAD,	/* padding_bottom  */
		25,					/* padding_left    */
		20,					/* padding_right   */
		IRRECO_SELECT_CMD_DLG_EXPAND_BUTTONS,	/* x_expand_scale  */
		0					/* y_expand_scale  */
		);
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Button adding to button rows.                                              */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Button adding to button rows
 * @{
 */


/*
 * Initialize IrrecoButtonAppend structure.
 *
 * There is no need to clean IrrecoButtonAppend structure because all
 * created widgets will be attached to the vbox.
 */
void irreco_cmd_dlg_append_button_init(IrrecoButtonAppend * data,
				       GtkWidget *vbox)
{
	IRRECO_ENTER
	data->vbox = vbox;
	data->hbox = irreco_cmd_dlg_append_command_hbox(vbox);
	data->width = 0;
	data->count = 0;
	IRRECO_RETURN
}

/*
 * Add a button to button row. Creates a new button row if current is full.
 *
 * Returns: a new GtkButton.
 */
GtkWidget *irreco_cmd_dlg_append_button(IrrecoButtonAppend * data,
					const gchar * title)
{
	GtkWidget *button;
	GtkRequisition requisition;
	IRRECO_ENTER

	/* Because you cant get widget data->width without adding it to a
	   container, this code does thing a bit backwards. */
	button = gtk_button_new_with_label(title);
	gtk_box_pack_start_defaults(GTK_BOX(data->hbox), button);
	gtk_widget_size_request(GTK_WIDGET(button), &requisition);
	data->width += requisition.width + IRRECO_SELECT_CMD_DLG_BUTTON_PAD;

	/* If we have too much buttons on one row, destroy button,
	   and create another button on a new row. */
	if (data->width > IRRECO_SELECT_CMD_DLG_BUTTON_ROW_WIDTH
	    && data->count > 0) {
		gtk_widget_destroy(button);
		data->count = 0;
		data->width = requisition.width +
			      IRRECO_SELECT_CMD_DLG_BUTTON_PAD;
		data->hbox = irreco_cmd_dlg_append_command_hbox(data->vbox);
		button = gtk_button_new_with_label(title);
		gtk_box_pack_start_defaults(GTK_BOX(data->hbox), button);
	}

	data->count += 1;
	IRRECO_RETURN_PTR(button);
}

/** @} */
/** @} */



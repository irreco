/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoInputDlg
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoInputDlg.
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_INPUT_DLG_H_TYPEDEF__
#define __IRRECO_INPUT_DLG_H_TYPEDEF__

#define IRRECO_TYPE_INPUT_DLG irreco_input_dlg_get_type()
#define IRRECO_INPUT_DLG(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  IRRECO_TYPE_INPUT_DLG, IrrecoInputDlg))
#define IRRECO_INPUT_DLG_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  IRRECO_TYPE_INPUT_DLG, IrrecoInputDlgClass))
#define IRRECO_IS_INPUT_DLG(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  IRRECO_TYPE_INPUT_DLG))
#define IRRECO_IS_INPUT_DLG_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  IRRECO_TYPE_INPUT_DLG))
#define IRRECO_INPUT_DLG_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  IRRECO_TYPE_INPUT_DLG, IrrecoInputDlgClass))

typedef struct _IrrecoInputDlg IrrecoInputDlg;
typedef struct _IrrecoInputDlgClass IrrecoInputDlgClass;

#endif /* __IRRECO_INPUT_DLG_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_INPUT_DLG_H__
#define __IRRECO_INPUT_DLG_H__
#include "irreco.h"
#include "irreco_dlg.h"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
struct _IrrecoInputDlg {
	IrrecoDlg parent;
	GtkWidget *hbox;
	GtkWidget *label;
	GtkWidget *entry;
};

struct _IrrecoInputDlgClass {
	IrrecoDlgClass parent_class;
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
GType 		irreco_input_dlg_get_type(void);
GtkWidget *	irreco_input_dlg_new(GtkWindow *parent,
				     const gchar *window_title);
void 		irreco_input_dlg_set_ok_button_text(IrrecoInputDlg *self,
						    const gchar *text);
void 		irreco_input_dlg_set_label(IrrecoInputDlg *self,
					   const gchar *text);
void 		irreco_input_dlg_set_entry(IrrecoInputDlg *self,
					   const gchar *text);
const gchar *	irreco_input_dlg_get_entry(IrrecoInputDlg *self);
gboolean 	irreco_show_input_dlg(IrrecoInputDlg *self);


#endif /* __IRRECO_INPUT_DLG_H__ */

/** @} */


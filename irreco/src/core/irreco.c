/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco.h"
#include "irreco_data.h"
#include "irreco_config.h"
#include "irreco_window_user.h"
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <libosso.h>
#include <irreco_webdb.h>
#include <irreco_retry_loop.h>

/**
 * @mainpage
 *
 * @par About Irreco
 * Irreco is a remote control application for Nokia Internet Tablets. With
 * Irreco and the help of external IR transceiver you can control IR compatible
 * devices like televisions, DVD players and AV Receivers. Irreco also has a
 * remote editor which you can use to place buttons to the screen and build the
 * kind of remote you want to use.
 *
 * @par Irreco elsewhere
 * @li Homepage: http://irreco.garage.maemo.org/
 * @li Garage project: https://garage.maemo.org/projects/irreco/
 * @li Mailing list: https://garage.maemo.org/mailman/listinfo/irreco-everything
 * @li Internet Tablet Talk Thread:
 *     http://www.internettablettalk.com/forums/showthread.php?t=15919
 * @li Maemo Downloads page: http://maemo.org/downloads/product/OS2008/irreco/
 *
 * @par Documentation
 * @li Irreco Class documentation can be found in
 *     @htmlonly<a class="el" href="modules.html">Modules</a>@endhtmlonly page.
 *
 * @par How to implement irreco backend
 * @li First take a look at @ref IrrecoBackendApi.
 * @li Then make a copy of @ref PageIrrecoBackendTemplate.
 * @li And write some code :)
 */

/**
 * @addtogroup Irreco Internal Irreco Classes
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
void irreco_parse_args(int * argc, char **argv[]);
void irreco_process_args(IrrecoData *irreco_data);
void irreco_link_bg_image_dir();



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Main.                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/*
 * Popup help message if user doesnt have any remotes.
 */
gboolean irreco_startup_help_check(IrrecoData *irreco_data)
{
	IRRECO_ENTER

	if (irreco_string_table_is_empty(irreco_data->irreco_layout_array)) {

		irreco_info_dlg(irreco_window_manager_get_gtk_window(
				irreco_data->window_manager),
				_(IRRECO_STARTUP_NO_REMOTE));

		if (!irreco_show_remote_download_dlg(irreco_data,
		    irreco_window_manager_get_gtk_window(
		    irreco_data->window_manager))) {
			irreco_info_dlg(irreco_window_manager_get_gtk_window(
					irreco_data->window_manager),
					_(IRRECO_NO_REMOTE_HELP));
		}
	}
	IRRECO_RETURN_BOOL(FALSE);
}

int main(int argc, char *argv[])
{
	IrrecoData *irreco_data;
   	osso_context_t* osso_context = NULL;
	IRRECO_ENTER

	/* Setup gettext. */
	setlocale(LC_ALL, "");
	IRRECO_DEBUG("bindtextdomain: \"%s\"\n",
		     bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR));
	IRRECO_DEBUG("bind_textdomain_codeset: \"%s\"\n",
		     bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8"));
	IRRECO_DEBUG("textdomain: \"%s\"\n", textdomain(GETTEXT_PACKAGE));

	/* Create program main data structure & parse arguments. */
	irreco_data = irreco_data_new();
	irreco_parse_args(&argc, &argv);
	IRRECO_DEBUG("LOCALEDIR: %s\n", LOCALEDIR);
	IRRECO_DEBUG("GETTEXT_PACKAGE: %s\n", GETTEXT_PACKAGE);

	irreco_process_args(irreco_data);

	/* Init Web Database*/
	irreco_webdb_init();

	/* Initialize the GTK. */
	g_thread_init(NULL);
	/*gdk_threads_init();*/
	gtk_init(&argc, &argv);

	/* Initialize libosso and maemo dbus thingy. */
	osso_context = osso_initialize(PACKAGE_DBUS_NAME, PACKAGE_VERSION,
				       TRUE, NULL);
  	if (osso_context == NULL) {
    		IRRECO_PRINTF("Failed to init LibOSSO\n");
    		return EXIT_FAILURE;
  	}

	/* Init UI. */
	g_set_application_name(IRRECO_APP_NAME_SHORT);
	irreco_window_manager_show(irreco_data->window_manager,
				   IRRECO_WINDOW_USER);

	/* Load plugins. */
	{
	IrrecoBackendManager* manager = irreco_data->irreco_backend_manager;
	irreco_backend_manager_load_lib_dir(manager, IRRECO_BACKEND_DIR);
	irreco_config_read_backends(manager);
	irreco_backend_manager_print(manager);
	irreco_backend_manager_read_from_confs(manager);
	irreco_backend_manager_get_devcmd_lists(manager);
	}

	/* Read layouts. */
	irreco_data->theme_manager = irreco_theme_manager_new(irreco_data);
	irreco_config_read_layouts_from_files(irreco_data);

	/* Start Lirc daemon */
	system("sudo /etc/init.d/lirc start");
	system("sudo /etc/init.d/lirc reload");

	/* Create link to background image dir. */
	irreco_link_bg_image_dir();

	/* Begin the main application. */
	irreco_window_manager_set_layout(irreco_data->window_manager,
					 irreco_config_read_active_layout(
					 irreco_data));
	g_idle_add(G_SOURCEFUNC(irreco_startup_help_check), irreco_data);
	gtk_main();

	/* Save active layout. */
	if (irreco_data->window_manager->current_layout != NULL) {
		irreco_config_save_active_layout(
			irreco_data->window_manager->current_layout);
	}

	/* Exit */
	system("sudo /etc/init.d/lirc stop");
	irreco_data_free(irreco_data);
	osso_deinitialize(osso_context);
	irreco_webdb_finalize();
	IRRECO_RETURN_INT(0);
}



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Command line argument parsing.                                             */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

static gchar** style_dir_array = NULL;
static gchar** style_file_array = NULL;
static GOptionEntry irreco_option_entries[] = {
	{ "button-style-dir", 'B', 0, G_OPTION_ARG_FILENAME_ARRAY,
	  &style_dir_array, "Read button styles files from directory", "DIR" },
	/*{ "button-style-file", 'b', 0, G_OPTION_ARG_FILENAME_ARRAY,
	  &style_file_array, "Read button style from file.", "FILE" },*/
	{ "debug", 'd', 0, G_OPTION_ARG_INT, &IRRECO_DEBUG_LEVEL_VAR,
	  "Debugging ouput level", "N" },
	{ NULL }
};

void irreco_parse_args(int * argc, char **argv[])
{
	gchar* help_title;
	GError *error = NULL;
	GOptionContext *context;
	IRRECO_ENTER

	/* Format help title. */
	help_title = g_strjoin("", "- ", IRRECO_APP_NAME_LONG,
			       " v", VERSION, NULL);
	context = g_option_context_new(help_title);
	g_free(help_title);

	/* Parse arguments. */
	g_option_context_add_main_entries(context, irreco_option_entries,
					  GETTEXT_PACKAGE);
	g_option_context_add_group(context, gtk_get_option_group(TRUE));
	g_option_context_parse(context, argc, argv, &error);
	if (irreco_gerror_check_print(&error)) {
		exit(1);
	}
	g_option_context_free(context);
	IRRECO_RETURN
}

void irreco_process_args(IrrecoData *irreco_data)
{
	guint array_pos;
	gboolean file_error = FALSE;
	IRRECO_ENTER

	/* Check file filetypes. */
	if (style_dir_array != NULL) {
		for (array_pos = g_strv_length(style_dir_array); array_pos--;) {
			if (!irreco_is_dir(style_dir_array[array_pos])) {
				IRRECO_ERROR("\"%s\" is not a directory.\n",
					     style_dir_array[array_pos]);
				file_error = TRUE;
			}
		}
	}
	if (style_file_array != NULL) {
		for (array_pos = g_strv_length(style_file_array); array_pos--;) {
			if (!irreco_is_file(style_file_array[array_pos])) {
				IRRECO_ERROR("\"%s\" is not a file.\n",
					     style_file_array[array_pos]);
				file_error = TRUE;
			}
		}
	}
	if (file_error) {
		exit(1);
	}

	/* Read button configurations. */
	if (style_dir_array != NULL) {
		for (array_pos = g_strv_length(style_dir_array); array_pos--;) {
			IRRECO_PRINTF("Reading button styles from \"%s\".\n",
				      style_dir_array[array_pos]);
			/*irreco_config_read_themes_from_dir(
				irreco_data, style_dir_array[array_pos]);*/
			irreco_theme_manager_read_themes_from_dir(
						irreco_data->theme_manager,
						style_dir_array[array_pos]);
		}
		g_strfreev(style_dir_array);
		style_dir_array = NULL;
	}

	IRRECO_RETURN
}

/*
 * Default Irreco background images are installed to somewhere under /usr/lib.
 * That location is not normally accessible with the Maemo file selection
 * dialog. So in order to work around that, we create a symlink into
 * $HOME/MyDocs/.images/ which points to that directory.
 */
void irreco_link_bg_image_dir()
{
	const gchar *oldpwd;
	const gchar link_name[] = "Irreco default backgrounds";
	IRRECO_ENTER

	oldpwd = getenv("PWD");
	if (chdir(getenv("HOME")) != 0 || chdir("MyDocs/.images/") != 0) {
		IRRECO_ERROR("Could not chdir into "
			     "\"$HOME/MyDocs/.images/\".\n");
		IRRECO_RETURN
	}

	if (irreco_file_exists(link_name)) {
		IRRECO_PRINTF("File \"%s\" already exists.\n", link_name);
	} else if(symlink(IRRECO_BG_IMAGE_DIR, link_name) == 0) {
		IRRECO_PRINTF("Created link from \"%s\" as \"%s\".\n",
			      link_name, IRRECO_BG_IMAGE_DIR);
	} else {
		IRRECO_ERROR("Could not make a link from \"%s\" to \"%s\".\n",
			     link_name, IRRECO_BG_IMAGE_DIR);
	}

	chdir(oldpwd);
	IRRECO_RETURN
}


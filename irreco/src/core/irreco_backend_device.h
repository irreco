/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoBackendDevice
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoBackendDevice.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_BACKEND_DEVICE_H_TYPEDEF__
#define __IRRECO_BACKEND_DEVICE_H_TYPEDEF__
typedef struct _IrrecoBackendDevice IrrecoBackendDevice;
#endif /* __IRRECO_BACKEND_DEVICE_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_BACKEND_DEVICE_H__
#define __IRRECO_BACKEND_DEVICE_H__
#include "irreco.h"
#include "irreco_backend_instance.h"
#include <irreco_string_table.h>



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
struct _IrrecoBackendDevice {
	IrrecoBackendInstance	*backend_instance;
	gpointer		contex;
	gchar			*name;
	IrrecoStringTable	*command_list;
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/*
 * Check if command_list is empty.
 *
 * Returns: TRUE if empty, FALSE otherwise.
 */
#define irreco_backend_device_is_empty(_device) \
	irreco_string_table_is_empty(_device->command_list)

/*
 * Iterate through the list of commands the device supports.
 */
#define IRRECO_BACKEND_DEVICE_FOREACH(_device, _command_var_name)	       \
	IRRECO_STRING_TABLE_FOREACH_KEY(_device->command_list,		       \
					_command_var_name)
#define IRRECO_BACKEND_DEVICE_FOREACH_END IRRECO_STRING_TABLE_FOREACH_END



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
IrrecoBackendDevice *
irreco_backend_device_create(const gchar *name,
			     IrrecoBackendInstance * backend_instance,
			     gpointer *contex);
void irreco_backend_device_destroy(IrrecoBackendDevice *backend_device);
gboolean irreco_backend_device_is_editable(IrrecoBackendDevice * device);
gboolean irreco_backend_device_is_exportable(IrrecoBackendDevice * device);
void irreco_backend_device_edit(IrrecoBackendDevice *backend_device,
				GtkWindow * parent);
void irreco_backend_device_delete(IrrecoBackendDevice *backend_device,
				  GtkWindow * parent);


#endif /* __IRRECO_BACKEND_DEVICE_H__ */

/** @} */

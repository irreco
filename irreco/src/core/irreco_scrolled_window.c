/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_scrolled_window.h"

/**
 * @addtogroup IrrecoScrolledWindow
 * @ingroup Irreco
 *
 * A GtkScrolledWindow with automatic resising to fit client widgets.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoScrolledWindow.
 */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
static void irreco_scrolled_window_size_request(GtkWidget *widget,
						GtkRequisition *requisition,
						IrrecoScrolledWindow *self);



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

G_DEFINE_TYPE(IrrecoScrolledWindow, irreco_scrolled_window, GTK_TYPE_SCROLLED_WINDOW)

static void irreco_scrolled_window_finalize(GObject *object)
{
	G_OBJECT_CLASS(irreco_scrolled_window_parent_class)->finalize (object);
}

static void irreco_scrolled_window_class_init(IrrecoScrolledWindowClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS(klass);
	object_class->finalize = irreco_scrolled_window_finalize;
}

static void irreco_scrolled_window_init(IrrecoScrolledWindow *self)
{
	IRRECO_ENTER

	/* Setup scrolled window. */
	gtk_scrolled_window_set_hadjustment(GTK_SCROLLED_WINDOW(self), NULL);
	gtk_scrolled_window_set_vadjustment(GTK_SCROLLED_WINDOW(self), NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(self),
				       GTK_POLICY_AUTOMATIC,
				       GTK_POLICY_AUTOMATIC);

	/* Create container for child. We use this container to make sure that
	   possible extra space inside the scrolled_window is not allocated to
	   the child widget. */
	self->alignment = gtk_alignment_new(0, 0, 0, 0);
	gtk_alignment_set_padding(GTK_ALIGNMENT(self->alignment), 0, 0, 0, 0);
	gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(self),
					      self->alignment);

	/* Connect signals. */
	g_signal_connect(G_OBJECT(self), "size-request",
			 G_CALLBACK(irreco_scrolled_window_size_request),
			 self);

	IRRECO_RETURN
}

GtkWidget* irreco_scrolled_window_new(void)
{
	IRRECO_ENTER
	IRRECO_RETURN_PTR(g_object_new(IRRECO_TYPE_SCROLLED_WINDOW, NULL));
}

GtkWidget* irreco_scrolled_window_new_with_autoresize(gint width_min,
						      gint width_max,
						      gint height_min,
						      gint height_max)
{
	IrrecoScrolledWindow *self;
	IRRECO_ENTER

	self = g_object_new(IRRECO_TYPE_SCROLLED_WINDOW, NULL);
	irreco_scrolled_window_set_autoresize(self, width_min, width_max,
					      height_min, height_max);
	IRRECO_RETURN_PTR(self);
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Functions                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */

void irreco_scrolled_window_set_autoresize(IrrecoScrolledWindow *self,
					   gint width_min,
					   gint width_max,
					   gint height_min,
					   gint height_max)
{
	IRRECO_ENTER
	if ((self->width_min  = width_min)  < 0) self->width_min  = 0;
	if ((self->width_max  = width_max)  < 0) self->width_max  = 0;
	if ((self->height_min = height_min) < 0) self->height_min = 0;
	if ((self->height_max = height_max) < 0) self->height_max = 0;
	IRRECO_RETURN
}

void irreco_scrolled_window_add(IrrecoScrolledWindow *self,
				GtkWidget *widget)
{
	IRRECO_ENTER
	self->child = widget;
	gtk_container_add(GTK_CONTAINER(self->alignment), widget);
	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Events and Callbacks                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Events and Callbacks
 * @{
 */

static void irreco_scrolled_window_size_request(GtkWidget *widget,
						GtkRequisition *requisition,
						IrrecoScrolledWindow *self)
{
	GtkRequisition child_req;
	gint scrollbar_space = 30;
	/*GtkAdjustment *adj;*/
	IRRECO_ENTER

	if (self->child == NULL) IRRECO_RETURN;

	gtk_widget_size_request(self->child, &child_req);
	IRRECO_DEBUG("Child size requisition: w%i h%i\n",
		     child_req.width,
		     child_req.height);

	/* Height */
	if (self->height_min > child_req.height) {
		requisition->height = self->height_min;
	} else {
		requisition->height = child_req.height;
	}
	if (self->height_max > 0
	    && requisition->height > self->height_max) {
		requisition->height = self->height_max;
	}

	/* Width */
	if (self->width_min > child_req.width) {
		requisition->width = self->width_min + scrollbar_space;
	} else {
		requisition->width = child_req.width + scrollbar_space;
	}
	if (self->width_max > 0
	    && requisition->width > self->width_max) {
		requisition->width = self->width_max;
	}

	IRRECO_DEBUG("Size requisition: w%i h%i\n",
		     requisition->width,
		     requisition->height);

	IRRECO_RETURN
}

/** @} */
/** @} */




/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoWindowUser
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoWindowUser.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_WINDOW_USER_H_TYPEDEF__
#define __IRRECO_WINDOW_USER_H_TYPEDEF__
typedef struct _IrrecoWindowUser IrrecoWindowUser;
#endif /* __IRRECO_WINDOW_USER_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_WINDOW_USER_H__
#define __IRRECO_WINDOW_USER_H__
#include "irreco.h"
#include "irreco_data.h"
#include "irreco_button.h"
#include "irreco_window.h"
#include "irreco_window_manager.h"
#include "irreco_remote_download_dlg.h"
#include "irreco_remote_upload_dlg.h"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/*
 * Remote user interface data.
 */
struct _IrrecoWindowUser {

	IrrecoWindow *window;
	IrrecoData *irreco_data;
	IrrecoWindowManager *manager;

	/* Buttonlayout being displayed. */
	IrrecoButtonLayout *irreco_layout;

	gboolean executing_cmd_chain;
	IrrecoButton *execution_button;

	GtkWidget *menu;
	GtkWidget *menu_new_remote;
	GtkWidget *menu_download_remote;
	GtkWidget *menu_edit_remote;
	GtkWidget *menu_rename_remote;
	GtkWidget *menu_upload_remote;
	GtkWidget *menu_delete_remote;
	GtkWidget *menu_show_remote;
	GtkWidget *menu_fullscreen;
	GtkWidget *menu_about;

	gulong menu_fullscreen_handler;
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
IrrecoWindowUser *irreco_window_user_create(IrrecoWindowManager * manager);
void irreco_window_user_destroy(IrrecoWindowUser * user_window);
void irreco_window_user_set_layout(IrrecoWindowUser * user_window,
				   IrrecoButtonLayout * layout);
gboolean irreco_window_user_create_new_remote(IrrecoData *irreco_data,
					      GtkWindow *parent);


#endif /* __IRRECO_WINDOW_USER_H__ */

/** @} */

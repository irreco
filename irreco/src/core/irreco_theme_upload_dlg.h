/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi),
 * Harri Vattulainen (t5vaha01@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoThemeUploadDlg
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoThemeUploadDlg.
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_THEME_UPLOAD_DLG_H_TYPEDEF__
#define __IRRECO_THEME_UPLOAD_DLG_H_TYPEDEF__

#define IRRECO_TYPE_THEME_UPLOAD_DLG             (irreco_theme_upload_dlg_get_type ())
#define IRRECO_THEME_UPLOAD_DLG(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), IRRECO_TYPE_THEME_UPLOAD_DLG, IrrecoThemeUploadDlg))
#define IRRECO_THEME_UPLOAD_DLG_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), IRRECO_TYPE_THEME_UPLOAD_DLG, IrrecoThemeUploadDlgClass))
#define IRRECO_IS_THEME_UPLOAD_DLG(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), IRRECO_TYPE_THEME_UPLOAD_DLG))
#define IRRECO_IS_THEME_UPLOAD_DLG_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), IRRECO_TYPE_THEME_UPLOAD_DLG))
#define IRRECO_THEME_UPLOAD_DLG_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), IRRECO_TYPE_THEME_UPLOAD_DLG, IrrecoThemeUploadDlgClass))

typedef struct _IrrecoThemeUploadDlgClass IrrecoThemeUploadDlgClass;
typedef struct _IrrecoThemeUploadDlg IrrecoThemeUploadDlg;

#endif /* __IRRECO_THEME_UPLOAD_DLG_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_THEME_UPLOAD_DLG_H__
#define __IRRECO_THEME_UPLOAD_DLG_H__
#include "irreco.h"
#include "irreco_internal_dlg.h"
#include "irreco_button.h"
#include "irreco_bg_browser_widget.h"
#include "irreco_button_browser_widget.h"
#include "irreco_login_dlg.h"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

struct _IrrecoThemeUploadDlgClass
{
	IrrecoInternalDlgClass parent_class;
};

struct _IrrecoThemeUploadDlg
{
	IrrecoInternalDlg parent_instance;

	GtkWindow		*parent_window;
	IrrecoData		*irreco_data;
	const gchar		*theme_name;
	gchar			*comments;
	gchar			*user;
	gchar			*password;
	GtkWidget		*preview;
	IrrecoTheme		*theme;
	GtkWidget 		*entry_name;
	GtkWidget		*textview_comments;
	GtkTextBuffer		*buffer_comments;
	IrrecoThemeButton	*preview_button;
	GtkWidget		*preview_image;
	GtkWidget		*preview_event_box;
	gchar			*preview_name;
	gboolean		uploading_errors;
	GtkWidget		*banner;
	gint			banner_max; /* num. of buttons and bg's */
	gint			banner_index; /* num. of img curr. uploading */
	gint			loader_state;
	gint			loader_func_id;
	gboolean		uploading_started; /* is theme being uploaded */
	gint			themeindex;
	gsize			buffer_size;
	IrrecoWebdbCache	*cache;
	gint			strtablesize;
	gint			strtableindex;
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

GType irreco_theme_upload_dlg_get_type (void) G_GNUC_CONST;
void irreco_theme_upload_dlg_destroy(IrrecoThemeUploadDlg * theme_upload_dlg);
gboolean irreco_theme_upload_dlg_run(GtkWindow *parent_window,
				     IrrecoData *irreco_data,
				     IrrecoTheme *irreco_theme,
				     gchar *user,
				     gchar *password);


#endif /* __IRRECO_THEME_UPLOAD_DLG_H__ */

/** @} */


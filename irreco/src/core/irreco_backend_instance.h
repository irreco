/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoBackendInstance
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoBackendInstance.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_BACKEND_INSTANCE_H_TYPEDEF__
#define __IRRECO_BACKEND_INSTANCE_H_TYPEDEF__
typedef struct _IrrecoBackendInstance IrrecoBackendInstance;
#endif /* __IRRECO_BACKEND_INSTANCE_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_BACKEND_INSTANCE_H__
#define __IRRECO_BACKEND_INSTANCE_H__
#include "irreco.h"
#include "irreco_cmd.h"
#include "irreco_backend_lib.h"
#include "irreco_backend_device.h"
#include <irreco_string_table.h>
#include "../api/irreco_backend_api.h"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
struct _IrrecoBackendInstance {
	IrrecoData		*irreco_data;

	GString			*name;
	GString 		*description;
	GString			*name_and_description;

	gchar			*config;
	IrrecoBackendLib	*lib;
	gpointer		 contex;
	IrrecoStringTable	*device_list;

	/* List of IrrecoCmd objects that depend on this instace. */
	GPtrArray 		*irreco_cmd_dependencies;
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

#define IRRECO_BACKEND_ENTER(_instance, _function) \
	IRRECO_OUTPUT(2, ">> ") \
		irreco_debug_print("Calling \"%s\" inside \"%s\"\n", \
			 _function, _instance->lib->name)
#define IRRECO_BACKEND_RETURN(_function) \
	IRRECO_OUTPUT(2, "<< ") \
		irreco_debug_print("Returned from \"%s\"\n", _function)

/*
 * Iterate trough device list of the instance.
 */
#define IRRECO_BACKEND_INSTANCE_FOREACH(_instance, _device_var_name)	       \
	IRRECO_STRING_TABLE_FOREACH_DATA(_instance->device_list,	       \
					 IrrecoBackendDevice*, 	       	       \
					 _device_var_name)
#define IRRECO_BACKEND_INSTANCE_FOREACH_END IRRECO_STRING_TABLE_FOREACH_END



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
IrrecoBackendInstance *
irreco_backend_instance_create(IrrecoBackendLib * backend_lib,
			       IrrecoData *irreco_data);
void irreco_backend_instance_destroy(IrrecoBackendInstance *self);
void irreco_backend_instance_destroy_full(IrrecoBackendInstance *self,
					  gboolean permanently);
gboolean irreco_backend_instance_api_edit(IrrecoBackendInstance *self);
gboolean irreco_backend_instance_api_export(IrrecoBackendInstance *self);
void irreco_backend_instance_add_cmd_dependency(IrrecoBackendInstance *self,
						IrrecoCmd * command);
void irreco_backend_instance_remove_cmd_dependency(IrrecoBackendInstance *self,
						   IrrecoCmd * command);
gboolean irreco_backend_instance_check_status(IrrecoBackendInstance *self,
					      IrrecoBackendStatus status);
void irreco_backend_instance_set_name(IrrecoBackendInstance *self,
				      gchar * name);
void irreco_backend_instance_set_config(IrrecoBackendInstance *self,
					gchar * config);
const gchar *
irreco_backend_instance_get_name(IrrecoBackendInstance *self);
const gchar *
irreco_backend_instance_get_description(IrrecoBackendInstance *self);
const gchar *
irreco_backend_instance_get_name_and_description(IrrecoBackendInstance *self);
gboolean irreco_backend_instance_read_from_conf(IrrecoBackendInstance *self);
gboolean irreco_backend_instance_save_to_conf(IrrecoBackendInstance *self);
gboolean irreco_backend_instance_send_command(IrrecoBackendInstance *self,
					      const gchar *device_name,
					      const gchar *command_name);
void irreco_backend_instance_configure(IrrecoBackendInstance *self,
				       GtkWindow *parent);
void irreco_backend_instance_create_device(IrrecoBackendInstance *self,
					   GtkWindow *parent);
void irreco_backend_instance_get_devcmd_list(IrrecoBackendInstance *self);

gboolean irreco_backend_instance_export_conf(IrrecoBackendInstance *self,
				const char * device_name,
				IrrecoBackendFileContainer **file_container);

gboolean irreco_backend_instance_import_conf(IrrecoBackendInstance *self,
				IrrecoBackendFileContainer *file_container);

gboolean irreco_backend_instance_check_conf(IrrecoBackendInstance *self,
				IrrecoBackendFileContainer *file_container);

#endif /* __IRRECO_BACKEND_INSTANCE_H__ */

/** @} */

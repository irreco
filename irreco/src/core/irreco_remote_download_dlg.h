/*
 * irreco - Ir Remote Control
 * Copyright (C) 2008  Joni Kokko (t5kojo01@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoRemoteDownloadDlg
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoRemoteDownloadDlg.
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_REMOTE_DOWNLOAD_DLG_H_TYPEDEF__
#define __IRRECO_REMOTE_DOWNLOAD_DLG_H_TYPEDEF__

#define IRRECO_TYPE_REMOTE_DOWNLOAD_DLG irreco_remote_download_dlg_get_type()

#define IRRECO_REMOTE_DOWNLOAD_DLG(obj) \
	(G_TYPE_CHECK_INSTANCE_CAST ((obj), \
	IRRECO_TYPE_REMOTE_DOWNLOAD_DLG, IrrecoRemoteDownloadDlg))

#define IRRECO_REMOTE_DOWNLOAD_DLG_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_CAST ((klass), \
	IRRECO_TYPE_REMOTE_DOWNLOAD_DLG, IrrecoRemoteDownloadDlgClass))

#define IRRECO_IS_REMOTE_DOWNLOAD_DLG(obj) \
	(G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
	IRRECO_TYPE_REMOTE_DOWNLOAD_DLG))

#define IRRECO_IS_REMOTE_DOWNLOAD_DLG_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_TYPE ((klass), IRRECO_TYPE_REMOTE_DOWNLOAD_DLG))

#define IRRECO_REMOTE_DOWNLOAD_DLG_GET_CLASS(obj) \
	(G_TYPE_INSTANCE_GET_CLASS ((obj), \
	IRRECO_TYPE_REMOTE_DOWNLOAD_DLG, IrrecoRemoteDownloadDlgClass))

typedef struct _IrrecoRemoteDownloadDlgClass IrrecoRemoteDownloadDlgClass;
typedef struct _IrrecoRemoteDownloadDlg IrrecoRemoteDownloadDlg;
#endif /* __IRRECO_REMOTE_DOWNLOAD_DLG_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_REMOTE_DOWNLOAD_DLG_H__
#define __IRRECO_REMOTE_DOWNLOAD_DLG_H__
#include "irreco.h"
#include "irreco_data.h"
#include "irreco_dlg.h"
#include "irreco_webdb_remote.h"
#include <glib-object.h>



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

struct _IrrecoRemoteDownloadDlg {
	IrrecoDlg	parent;
	IrrecoData	*irreco_data;
	GtkWidget	*banner;
	gint		loader_func_id;
	gint		loader_state;
	gint		loader_pos;
	GtkTreeIter	*loader_parent_iter;
	IrrecoWebdbRemote *remote;

	/* Remotes */
	GtkTreeStore	*tree_store;
	GtkTreeView	*tree_view;

	/* Details */
	GtkWidget	*details;
	GtkWidget	*help_text;
	GtkWidget	*details_model;
	GtkWidget	*details_creator;
	GtkWidget	*details_backends;
	GtkWidget	*details_themes;
	GtkWidget	*details_downloaded;
	GtkWidget	*details_comment;
};

struct _IrrecoRemoteDownloadDlgClass {
	IrrecoDlgClass parent_class;
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

GType irreco_remote_download_dlg_get_type (void);

IrrecoRemoteDownloadDlg* irreco_remote_download_dlg_new(IrrecoData *irreco_data,
							GtkWindow *parent);

gboolean irreco_show_remote_download_dlg(IrrecoData *irreco_data,
					 GtkWindow *parent);


#endif /* __IRRECO_REMOTE_DOWNLOAD_DLG_H__ */

/** @} */

/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_listbox_text.h"
#include <hildon/hildon-gtk.h>

/**
 * @addtogroup IrrecoListboxText
 * @ingroup Irreco
 *
 * A single column, scrollable listbox with text cels.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoListboxText.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes.                                                                */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/*static void irreco_listbox_text_tree_size_request(GtkWidget *widget,
						   GtkRequisition *requisition,
						   IrrecoListboxText *self);*/

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

enum
{
	DATA_COL,
	TEXT_COL,
	N_COLUMNS
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

G_DEFINE_TYPE(IrrecoListboxText, irreco_listbox_text, IRRECO_TYPE_LISTBOX)

static void irreco_listbox_text_class_init(IrrecoListboxTextClass *klass)
{
}

static void irreco_listbox_text_init(IrrecoListboxText *self)
{
	IrrecoListbox *parent;
	GtkTreeViewColumn *column;
	GtkCellRenderer *renderer;
	GtkTable *table;
	IRRECO_ENTER

	parent = IRRECO_LISTBOX(self);
	parent->text_col_id = TEXT_COL;
	parent->data_col_id = DATA_COL;

	/* Create GtkTreeStore and GtkTreeView */
	parent->list_store = gtk_list_store_new(
		N_COLUMNS, G_TYPE_POINTER, G_TYPE_STRING);
/*	parent->tree_view = gtk_tree_view_new_with_model(
		GTK_TREE_MODEL(parent->list_store));*/
	parent->tree_view = GTK_WIDGET(hildon_gtk_tree_view_new_with_model(
					1,
					GTK_TREE_MODEL(parent->list_store)));

	g_object_unref(G_OBJECT(parent->list_store));

	/* Setup column. */
	renderer = gtk_cell_renderer_text_new();
	column = gtk_tree_view_column_new_with_attributes(
		NULL, renderer, "text", TEXT_COL, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(parent->tree_view),
				    column);

	/* Set selection callback. */
	parent->tree_selection = gtk_tree_view_get_selection(
		GTK_TREE_VIEW(parent->tree_view));
	gtk_tree_selection_set_mode(parent->tree_selection,
				    GTK_SELECTION_SINGLE);

	/* Add Widgets to GtkTable. */
	table = GTK_TABLE(gtk_table_new(2, 2, FALSE));

	gtk_table_attach_defaults(table, parent->tree_view, 0 ,1, 0, 1);

/*	parent->vscrollbar = gtk_vscrollbar_new(gtk_tree_view_get_vadjustment(
					GTK_TREE_VIEW(parent->tree_view)));
	gtk_table_attach(table, parent->vscrollbar, 1 ,2, 0, 1,
					GTK_SHRINK, GTK_FILL, 0, 0);

	parent->hscrollbar = gtk_hscrollbar_new(gtk_tree_view_get_hadjustment(
					GTK_TREE_VIEW(parent->tree_view)));
	gtk_table_attach(table, parent->hscrollbar, 0 ,1, 1, 2,
					GTK_FILL, GTK_SHRINK, 0, 0);*/

	gtk_box_pack_start(GTK_BOX(self), GTK_WIDGET(table), TRUE, TRUE, 0);

	/* Connect signals. */
	/*g_signal_connect(G_OBJECT(IRRECO_LISTBOX(self)->tree_view),
			 "size-request",
			 G_CALLBACK(irreco_listbox_text_tree_size_request),
			 self);*/

	IRRECO_RETURN
}

GtkWidget *irreco_listbox_text_new()
{
	IrrecoListboxText *self;
	IRRECO_ENTER

	self = IRRECO_LISTBOX_TEXT(g_object_new(IRRECO_TYPE_LISTBOX_TEXT, NULL));
	IRRECO_RETURN_PTR(GTK_WIDGET(self));
}

/**
 * Create widgets and initialize IrrecoListboxText structure.
 *
 * The is no need to call destructor for IrrecoListboxText as long as you attach
 * the returned GtkWidget to somewhere in the GTK widget tree. Gtk will then
 * handle the destruction of the widgets when they are no longer needed. This
 * will work fine as long as IrrecoListboxText structure is still around when the
 * widget is destroyed.
 *
 * @param min_width
 *	Minimum width Requisition for the widget.
 * @param min_height
 *	Minimum height Requisition for the widget.
 */
GtkWidget *irreco_listbox_text_new_with_autosize(gint min_width,
						 gint max_width,
						 gint min_height,
						 gint max_height)
{
	IrrecoListboxText *self;
	IRRECO_ENTER

	self = IRRECO_LISTBOX_TEXT(g_object_new(IRRECO_TYPE_LISTBOX_TEXT, NULL));
	irreco_listbox_set_autosize(IRRECO_LISTBOX(self), min_width, max_width,
				    min_height, max_height);
	IRRECO_RETURN_PTR(GTK_WIDGET(self));
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Public Functions                                                           */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */

/**
 * Append text to the listbox.
 */
void irreco_listbox_text_append(IrrecoListboxText *self,
				const gchar *label,
				gpointer user_data)
{
	IrrecoListbox *parent;
	GtkTreeIter iter;
	IRRECO_ENTER

	parent = IRRECO_LISTBOX(self);
	gtk_list_store_append(parent->list_store, &iter);
	gtk_list_store_set(parent->list_store, &iter,
			   TEXT_COL, label, DATA_COL, user_data, -1);

	gtk_tree_view_columns_autosize(GTK_TREE_VIEW(parent->tree_view));

	if (parent->select_new_rows == TRUE) {
		gtk_tree_selection_select_iter(parent->tree_selection, &iter);
	}

	IRRECO_RETURN
}

/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Events and Callbacks                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Events and Callbacks
 * @{
 */

/*static void irreco_listbox_text_tree_size_request(GtkWidget *widget,
						  GtkRequisition *requisition,
						  IrrecoListboxText *self)
{
	IrrecoListbox *parent;
	gboolean show_hscrollbar = TRUE;
	IRRECO_ENTER

	parent = IRRECO_LISTBOX(self);

	if (requisition->width <= parent->max_width - 22) {
		show_hscrollbar = FALSE;
	}

	if (requisition->width < parent->min_width - 22) {
		requisition->width = parent->min_width - 22;
	} else if (requisition->width > parent->max_width - 22) {
		requisition->width = parent->max_width - 22;
	}

	if (requisition->height < parent->min_height - 22) {
		requisition->height = parent->min_height - 22;
	} else if (requisition->height > parent->max_height &&
		   show_hscrollbar == FALSE) {
		requisition->height = parent->max_height;
	} else if (requisition->height > parent->max_height - 22) {
		requisition->height = parent->max_height - 22;
	}

	if (show_hscrollbar == TRUE) {
		gtk_widget_show(parent->hscrollbar);
	} else {
		gtk_widget_hide(parent->hscrollbar);
	}

	IRRECO_RETURN
}*/

/** @} */

/** @} */


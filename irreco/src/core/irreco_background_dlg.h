/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi),
 * Harri Vattulainen (t5vaha01@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoBackgroundDlg
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoBackgroundDlg.
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_BACKGROUND_DLG_H_TYPEDEF__
#define __IRRECO_BACKGROUND_DLG_H_TYPEDEF__

#define IRRECO_TYPE_BACKGROUND_DLG             (irreco_background_dlg_get_type ())
#define IRRECO_BACKGROUND_DLG(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), IRRECO_TYPE_BACKGROUND_DLG, IrrecoBackgroundDlg))
#define IRRECO_BACKGROUND_DLG_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), IRRECO_TYPE_BACKGROUND_DLG, IrrecoBackgroundDlgClass))
#define IRRECO_IS_BACKGROUND_DLG(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), IRRECO_TYPE_BACKGROUND_DLG))
#define IRRECO_IS_BACKGROUND_DLG_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), IRRECO_TYPE_BACKGROUND_DLG))
#define IRRECO_BACKGROUND_DLG_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), IRRECO_TYPE_BACKGROUND_DLG, IrrecoBackgroundDlgClass))

typedef struct _IrrecoBackgroundDlgClass IrrecoBackgroundDlgClass;
typedef struct _IrrecoBackgroundDlg IrrecoBackgroundDlg;

#endif /* __IRRECO_BACKGROUND_DLG_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_BACKGROUND_DLG_H__
#define __IRRECO_BACKGROUND_DLG_H__
#include "irreco.h"
#include "irreco_internal_dlg.h"
#include "irreco_button.h"
#include "irreco_bg_browser_widget.h"
#include "irreco_button_browser_widget.h"


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

struct _IrrecoBackgroundDlgClass
{
	IrrecoInternalDlgClass parent_class;
};

struct _IrrecoBackgroundDlg
{
	IrrecoInternalDlg parent_instance;

	GtkWidget 			*preview;
	GtkWidget 			*color_button;
	GtkWidget			*radio_default;
	GtkWidget			*radio_color;
	GtkWidget			*radio_image;
	GtkWidget			*notebook;
	IrrecoBgBrowserWidget		*theme_bg_browser;
	IrrecoButtonLayoutBgType	 type;
	GString				*filename;
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

GType irreco_background_dlg_get_type (void) G_GNUC_CONST;
IrrecoBackgroundDlg *irreco_background_dlg_create(IrrecoData *irreco_data,
						  GtkWindow *parent_window);
void irreco_background_dlg_destroy(IrrecoBackgroundDlg * background_dlg);
gboolean irreco_background_dlg_run(IrrecoBackgroundDlg * background_dlg,
				   IrrecoButtonLayout * irreco_layout);


#endif /* __IRRECO_BACKGROUND_DLG_H__ */

/** @} */


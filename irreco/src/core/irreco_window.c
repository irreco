/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_window.h"

/**
 * @addtogroup IrrecoWindow
 * @ingroup Irreco
 *
 * Base class for other Irreco windows.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoWindow.
 */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
static gboolean irreco_window_delete_event(GtkWidget *widget,
					   GdkEvent *event,
					   gpointer *data);
static gboolean irreco_window_state_event(GtkWidget *widget,
					  GdkEventWindowState *event,
					  IrrecoWindow *self);



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

G_DEFINE_TYPE(IrrecoWindow, irreco_window, HILDON_TYPE_WINDOW)

static void irreco_window_finalize(GObject *object)
{
	G_OBJECT_CLASS (irreco_window_parent_class)->finalize (object);
}

static void irreco_window_class_init(IrrecoWindowClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	object_class->finalize = irreco_window_finalize;
}

static void irreco_window_init(IrrecoWindow *self)
{
	IRRECO_ENTER

	/*
	 * Create eventbox
	 *
	 * There is a little problem with the HildonWindow and the skin borders
	 * around it, for some reason the hildon window includes the borders of
	 * the windows _inside_ the window. This means that mouse events dont
	 * properly align to the left top corner of the window, and that the
	 * size of the window also contains the size of the borders. By creaging
	 * an eventbox, which has it's own GdkWindow, we can get the real usable
	 * area.
	 */
	self->event_box = gtk_event_box_new();
	gtk_event_box_set_visible_window(GTK_EVENT_BOX(self->event_box),
					 TRUE);
	gtk_container_add(GTK_CONTAINER(self), GTK_WIDGET(self->event_box));

	self->layout = gtk_layout_new(NULL, NULL);
	gtk_container_add(GTK_CONTAINER(self->event_box),
			  self->layout);

	/* Setup size & scrollbars. */
	gtk_layout_set_size(GTK_LAYOUT(self->layout),
			    IRRECO_LAYOUT_WIDTH,
			    IRRECO_LAYOUT_HEIGHT);

	/* Signals. */
	g_signal_connect(G_OBJECT(self), "delete-event",
			 G_CALLBACK(irreco_window_delete_event), NULL);
	g_signal_connect(G_OBJECT(self), "window-state-event",
			 G_CALLBACK(irreco_window_state_event), self);
	gtk_widget_show_all(GTK_WIDGET(self));
	IRRECO_RETURN
}

GtkWidget* irreco_window_new()
{
	IrrecoWindow* self;
	IRRECO_ENTER

	self = g_object_new(IRRECO_TYPE_WINDOW, NULL);
	hildon_program_add_window(HILDON_PROGRAM(hildon_program_get_instance()),
				  HILDON_WINDOW(self));
	IRRECO_RETURN_PTR(self);
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Private Functions
 * @{
 */

/** @} */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Functions                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */

/**
 * A pixbuf loading frapper around irreco_window_set_background_buf().
 */
gboolean irreco_window_set_background_image(IrrecoWindow *self,
					    const GdkColor * color,
					    const gchar * image1,
					    const gchar * image2)
{
	GError *error = NULL;
	GdkPixbuf *pixbuf1 = NULL;
	GdkPixbuf *pixbuf2 = NULL;
	IRRECO_ENTER

	g_assert(self != NULL);

	if (image1 != NULL) {
		pixbuf1 = gdk_pixbuf_new_from_file_at_scale(
			image1, IRRECO_SCREEN_WIDTH, IRRECO_SCREEN_HEIGHT,
			FALSE, &error);
		if (irreco_gerror_check_print(&error)) {
			IRRECO_RETURN_BOOL(FALSE);
		}
	}

	if (image2 != NULL) {
		pixbuf2 = gdk_pixbuf_new_from_file_at_scale(
			image1, IRRECO_SCREEN_WIDTH, IRRECO_SCREEN_HEIGHT,
			FALSE, &error);
		if (irreco_gerror_check_print(&error)) {
			g_object_unref(G_OBJECT(pixbuf1));
			IRRECO_RETURN_BOOL(FALSE);
		}
	}

	irreco_window_set_background_buf(self, color, pixbuf1, pixbuf2);
	if (pixbuf1 != NULL) g_object_unref(G_OBJECT(pixbuf1));
	if (pixbuf2 != NULL) g_object_unref(G_OBJECT(pixbuf2));
	IRRECO_RETURN_BOOL(TRUE);
}

/**
 * Set background image of scrolled window.
 *
 * This functions will:
 * @li Fill the background with the background color.
 * @li Draw the first image on top of the background color.
 * @li Draw the second image on top of the first image.
 *
 * @param pixbuf1	First image pixbuf, or NULL.
 * @param pixbuf2	Second image pixbuf, or NULL.
 */
void irreco_window_set_background_buf(IrrecoWindow *self,
				      const GdkColor * color,
				      GdkPixbuf * pixbuf1,
				      GdkPixbuf * pixbuf2)
{
	gint width = IRRECO_SCREEN_WIDTH;
	gint height = IRRECO_SCREEN_HEIGHT;
	GdkPixmap *pixmap = NULL;
	GdkGC *bg_gc = NULL;
	IRRECO_ENTER

	/* Create and fill pixmap with background color. */
	pixmap = gdk_pixmap_new(GDK_DRAWABLE(GTK_LAYOUT(
				self->layout)->bin_window),
				width, height, -1);

	/* Fill background with solid color. */
	bg_gc = gdk_gc_new(GDK_DRAWABLE(GTK_LAYOUT(
			   self->layout)->bin_window));
	gdk_gc_set_rgb_fg_color(bg_gc, color);
	gdk_gc_set_rgb_bg_color(bg_gc, color);
	gdk_draw_rectangle(GDK_DRAWABLE(pixmap),
			   bg_gc, TRUE, 0, 0, width, height);
	g_object_unref(G_OBJECT(bg_gc));

	/* Draw images to pixmap. */
	if (pixbuf1 != NULL) {
		width = gdk_pixbuf_get_width(pixbuf1);
		height = gdk_pixbuf_get_height(pixbuf1);
		gdk_draw_pixbuf(GDK_DRAWABLE(pixmap), NULL, pixbuf1, 0, 0, 0, 0,
				width, height, GDK_RGB_DITHER_NORMAL, 0, 0);
	}
	if (pixbuf2 != NULL) {
		width = gdk_pixbuf_get_width(pixbuf2);
		height = gdk_pixbuf_get_height(pixbuf2);
		gdk_draw_pixbuf(GDK_DRAWABLE(pixmap), NULL, pixbuf2,
				0, 0, 0, 0, width, height,
				GDK_RGB_DITHER_NORMAL, 0, 0);
	}

	/* Set background image, and queque redraw, so the image is shown.*/
	gdk_window_set_back_pixmap(GTK_LAYOUT(self->layout)->bin_window,
				   pixmap, FALSE);
	gtk_widget_queue_draw_area(GTK_WIDGET(self->layout), 0, 0,
				GTK_WIDGET(self->layout)->allocation.width,
				GTK_WIDGET(self->layout)->allocation.height);
	g_object_unref(G_OBJECT(pixmap));
	IRRECO_RETURN
}

/**
 * How much of the left-top corner is hidden due to scrolling.
 */
void irreco_window_get_scroll_offset(IrrecoWindow *self,
				     gdouble *x, gdouble *y)
{
	IRRECO_ENTER

	*x = 0;
	*y = 0;

	IRRECO_RETURN
}

/**
 * Adjust scrollbars so that the given point is visible.
 */
void irreco_window_scroll_visible(IrrecoWindow *self, gdouble x, gdouble y)
{
	IRRECO_ENTER

	IRRECO_RETURN
}

gboolean irreco_window_is_fullscreen(IrrecoWindow *self)
{
	GdkWindow *window;
	IRRECO_ENTER

	window = GTK_WIDGET(self)->window;
	IRRECO_RETURN_BOOL(gdk_window_get_state(window)
			   & GDK_WINDOW_STATE_FULLSCREEN);
}

void irreco_window_toggle_fullscreen(IrrecoWindow *self)
{
	IRRECO_ENTER

	IRRECO_RETURN
}

void irreco_window_scrollbar_hide(IrrecoWindow *self)
{
	IRRECO_ENTER

	IRRECO_RETURN
}

void irreco_window_scrollbar_show(IrrecoWindow *self)
{
	IRRECO_ENTER

	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Events                                                                     */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Events
 * @{
 */

static gboolean irreco_window_delete_event(GtkWidget *widget,
					   GdkEvent *event,
					   gpointer *data)
{
	gboolean  rvalue = TRUE;
	GString  *msg    = NULL;
	IRRECO_ENTER

	msg = g_string_new(NULL);
	g_string_printf(msg, _("Exit %s?"), IRRECO_APP_NAME_CONBINED);

	gtk_main_quit();
	rvalue = FALSE;

	g_string_free(msg, TRUE);
	IRRECO_RETURN_BOOL(rvalue);
}

static gboolean irreco_window_state_event(GtkWidget *widget,
					  GdkEventWindowState *event,
					  IrrecoWindow *self)
{
	IRRECO_ENTER

	/* Hide scrollbars in fullscreen mode. */
	if (event->changed_mask & GDK_WINDOW_STATE_FULLSCREEN) {
		if (event->new_window_state & GDK_WINDOW_STATE_FULLSCREEN) {
			irreco_window_scrollbar_hide(self);
		} else {
			irreco_window_scrollbar_show(self);
		}
	}

	IRRECO_RETURN_BOOL(FALSE);
}

/** @} */
/** @} */

/*
 * irreco - Ir Remote Control
 * Copyright (C) 2008  Joni Kokko (t5kojo01@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_remote_upload_dlg.h"
#include "irreco_login_dlg.h"
#include "irreco_theme_upload_dlg.h"
#include "irreco_webdb_upload_dlg.h"
#include "irreco_webdb_cache.h"

/** Loader states. */
enum
{
	LOADER_STATE_INIT,
	LOADER_STATE_CONFIGURATIONS,
	LOADER_STATE_THEMES,
	LOADER_STATE_END
};


/**
 * @addtogroup IrrecoRemoteUploadDlg
 * @ingroup Irreco
 *
 * @todo PURPOCE OF CLASS.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoRemoteUploadDlg.
 */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

void irreco_remote_upload_dlg_comment_size_request(GtkWidget *widget,
						   GtkRequisition *requisition,
						   IrrecoRemoteUploadDlg *self);

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

G_DEFINE_TYPE(IrrecoRemoteUploadDlg, irreco_remote_upload_dlg, IRRECO_TYPE_DLG)

static void irreco_remote_upload_dlg_dispose(GObject *object)
{
	G_OBJECT_CLASS(irreco_remote_upload_dlg_parent_class)->dispose(object);
}

static void irreco_remote_upload_dlg_finalize(GObject *object)
{
	G_OBJECT_CLASS(irreco_remote_upload_dlg_parent_class)->finalize(object);
}

static void irreco_remote_upload_dlg_class_init(
					IrrecoRemoteUploadDlgClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);


	object_class->dispose = irreco_remote_upload_dlg_dispose;
	object_class->finalize = irreco_remote_upload_dlg_finalize;
}

static void irreco_remote_upload_dlg_init(IrrecoRemoteUploadDlg *self)
{
	GtkWidget *table;
	GtkWidget *label_category;
	GtkWidget *label_manufacturer;
	GtkWidget *label_model;
	GtkWidget *label_comment;
	GtkWidget *align;
	GtkWidget *comment_frame;
	IRRECO_ENTER

	self->remote_uploaded = FALSE;

	/* Build the dialog */
	gtk_window_set_title(GTK_WINDOW(self), _("Upload Remote"));
	gtk_window_set_modal(GTK_WINDOW(self), TRUE);
	gtk_window_set_destroy_with_parent(GTK_WINDOW(self), TRUE);
	gtk_dialog_set_has_separator(GTK_DIALOG(self), FALSE);
	gtk_dialog_add_buttons(GTK_DIALOG(self),
			       GTK_STOCK_OK, GTK_RESPONSE_OK,
			       NULL);

	/* labels */

	label_category = gtk_label_new("Category: ");
	label_manufacturer = gtk_label_new("Manufacturer: ");
	label_model = gtk_label_new("Model: ");
	label_comment = gtk_label_new("Comment: ");

	gtk_misc_set_alignment(GTK_MISC(label_category), 0, 0.5);
	gtk_misc_set_alignment(GTK_MISC(label_manufacturer), 0, 0.5);
	gtk_misc_set_alignment(GTK_MISC(label_model), 0, 0.5);
	gtk_misc_set_alignment(GTK_MISC(label_comment), 0, 0.5);

	/* entries */

	self->category = gtk_combo_box_entry_new_text();
	self->manufacturer = gtk_combo_box_entry_new_text();
	self->model = gtk_entry_new();

	/* comment textview */

        self->comment = gtk_text_view_new();
	gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(self->comment),
				    GTK_WRAP_WORD_CHAR);

	comment_frame = gtk_frame_new(NULL);
	gtk_container_add(GTK_CONTAINER(comment_frame), self->comment);

	/* table */

	table = gtk_table_new(5, 2, FALSE);

	gtk_table_attach(GTK_TABLE(table), label_category, 0, 1, 0, 1,
			 GTK_FILL, GTK_FILL, 0, 2);
	gtk_table_attach(GTK_TABLE(table), label_manufacturer, 0, 1, 1, 2,
			 GTK_FILL, GTK_FILL, 0, 2);
	gtk_table_attach(GTK_TABLE(table), label_model, 0, 1, 2, 3,
			 GTK_FILL, GTK_FILL, 0, 2);
	gtk_table_attach(GTK_TABLE(table), label_comment, 0, 1, 3, 4,
			 GTK_FILL, GTK_FILL, 0, 2);
	gtk_table_attach(GTK_TABLE(table), self->category, 1, 2, 0, 1,
			 GTK_FILL, GTK_FILL, 0, 2);
	gtk_table_attach(GTK_TABLE(table), self->manufacturer, 1, 2, 1, 2,
	   		 GTK_FILL, GTK_FILL, 0, 2);
	gtk_table_attach(GTK_TABLE(table), self->model, 1, 2, 2, 3,
			 GTK_FILL, GTK_FILL, 0, 0);
	gtk_table_attach(GTK_TABLE(table), comment_frame, 0, 2, 4, 5,
			 GTK_FILL | GTK_EXPAND, GTK_FILL | GTK_EXPAND, 0, 0);

	/* scroll */

	self->scroll = gtk_scrolled_window_new(NULL, NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(self->scroll),
				       GTK_POLICY_NEVER,
				       GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(self->scroll),
					      table);

	/* alignment */

	align = gtk_alignment_new(0.5, 0.5, 1, 1);
	gtk_alignment_set_padding(GTK_ALIGNMENT(align), 12, 12, 12, 12);

	gtk_container_add(GTK_CONTAINER(align), self->scroll);
	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(self)->vbox),
			  GTK_WIDGET(align));

	/* Signals */
	g_signal_connect(G_OBJECT(self->comment), "size-request", G_CALLBACK(
			 irreco_remote_upload_dlg_comment_size_request), self);

	gtk_widget_set_size_request(GTK_WIDGET(self), -1, 280);

	gtk_widget_show_all(GTK_WIDGET(self));

	IRRECO_RETURN
}

IrrecoRemoteUploadDlg* irreco_remote_upload_dlg_new(IrrecoData *irreco_data,
						    GtkWindow *parent)
{
	IrrecoRemoteUploadDlg *self;
	IRRECO_ENTER

	self = g_object_new(IRRECO_TYPE_REMOTE_UPLOAD_DLG, NULL);
	irreco_dlg_set_parent(IRRECO_DLG(self), parent);
	self->irreco_data = irreco_data;
	IRRECO_RETURN_PTR(self);
}


/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Private Functions
 * @{
 */

gboolean irreco_remote_upload_dlg_populate_category(IrrecoRemoteUploadDlg *self)
{
	IrrecoStringTable *categories = NULL;
	IrrecoWebdbCache *webdb_cache;
	IRRECO_ENTER

	webdb_cache = irreco_data_get_webdb_cache(self->irreco_data, FALSE);

	/* Get categories */
	if(irreco_webdb_cache_get_all_categories(webdb_cache, &categories)) {
		IRRECO_STRING_TABLE_FOREACH_KEY(categories, key)

			gtk_combo_box_append_text(
					GTK_COMBO_BOX(self->category), key);

		IRRECO_STRING_TABLE_FOREACH_END
	} else {
		irreco_error_dlg(GTK_WINDOW(self),
				 irreco_webdb_cache_get_error(webdb_cache));
		IRRECO_RETURN_BOOL(FALSE);
	}

	if (categories != NULL) irreco_string_table_free(categories);

	IRRECO_RETURN_BOOL(TRUE);
}

gboolean irreco_remote_upload_dlg_populate_manufacturer(
						IrrecoRemoteUploadDlg *self)
{
	IrrecoStringTable *manufacturers = NULL;
	IrrecoWebdbCache *webdb_cache;
	IRRECO_ENTER

	webdb_cache = irreco_data_get_webdb_cache(self->irreco_data, FALSE);

	/* Get manufacturers */
	if(irreco_webdb_cache_get_all_manufacturers(webdb_cache,
						    &manufacturers)) {
		IRRECO_STRING_TABLE_FOREACH_KEY(manufacturers, key)

			gtk_combo_box_append_text(
					GTK_COMBO_BOX(self->manufacturer), key);

		IRRECO_STRING_TABLE_FOREACH_END
	} else {
		irreco_error_dlg(GTK_WINDOW(self),
				irreco_webdb_cache_get_error(webdb_cache));
		IRRECO_RETURN_BOOL(FALSE);
	}

	if (manufacturers != NULL) irreco_string_table_free(manufacturers);
	IRRECO_RETURN_BOOL(TRUE);
}

gboolean irreco_remote_upload_dlg_send(IrrecoRemoteUploadDlg *self)
{
	IrrecoWebdbCache *webdb_cache;
	IrrecoButtonLayout *layout;
	gboolean rvalue = FALSE;
	IRRECO_ENTER
	webdb_cache = irreco_data_get_webdb_cache(self->irreco_data, FALSE);
	layout = self->irreco_data->window_manager->current_layout;

	switch (self->loader_state) {

	case LOADER_STATE_INIT: {
		gchar *category = NULL;
		gchar *manufacturer = NULL;
		const gchar *model;
		gchar *comment = NULL;
		GtkTextIter start;
		GtkTextIter end;
		GtkTextBuffer *buffer;
		GString *path = g_string_new(irreco_get_config_dir("irreco"));
		gchar *file_data = NULL;
		gint file_length;
		IRRECO_DEBUG("LOADER_STATE_INIT");

		category = gtk_combo_box_get_active_text(
					GTK_COMBO_BOX(self->category));

		manufacturer = gtk_combo_box_get_active_text(
					GTK_COMBO_BOX(self->manufacturer));

		model = gtk_entry_get_text(GTK_ENTRY(self->model));

		/* Set new name */
		if (irreco_string_table_exists(
		    self->irreco_data->irreco_layout_array, model) &&
		    !g_str_equal(layout->name->str, model)) {
			irreco_error_dlg(GTK_WINDOW(self),
				 _(IRRECO_LAYOUT_NAME_COLLISION));
			goto end_init;
		}

		if (!irreco_string_table_change_key(
		    self->irreco_data->irreco_layout_array, layout->name->str,
		    model)) {
			irreco_error_dlg(GTK_WINDOW(self),
					 "Incompatible model");
			goto end_init;
		}

		irreco_config_save_layouts(self->irreco_data);

		/* comment */

		buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(self->comment));
		gtk_text_buffer_get_start_iter(GTK_TEXT_BUFFER(buffer), &start);
		gtk_text_buffer_get_end_iter(GTK_TEXT_BUFFER(buffer), &end);
		comment = gtk_text_buffer_get_text(GTK_TEXT_BUFFER(buffer),
						&start, &end, FALSE);

		/* data */

		g_string_append_printf(path,"/%s",layout->filename->str);
		file_length = irreco_file_length(path->str);
		file_data = g_malloc0(file_length);

		/* check errors */

		if (!irreco_read_text_file(path->str, file_data, file_length)) {
			irreco_error_dlg(GTK_WINDOW(self),
					 "Can't open layout file.");
			goto end_init;
		}

		if (strlen(category) == 0) {
			irreco_error_dlg(GTK_WINDOW(self),
					 "Empty category field.");
			goto end_init;
		}

		if (strlen(manufacturer) == 0) {
			irreco_error_dlg(GTK_WINDOW(self),
					 "Empty manufacturer field.");
			goto end_init;
		}

		if (strlen(model) == 0) {
			irreco_error_dlg(GTK_WINDOW(self),
					 "Empty model field.");
			goto end_init;
		}

		if (strlen(comment) == 0) {
			irreco_error_dlg(GTK_WINDOW(self),
					 "Empty comment field.");
			goto end_init;
		}

		/* Show login dialog */

		if (!irreco_show_login_dlg(self->irreco_data, GTK_WINDOW(self),
					&self->user_name, &self->password)) {
			IRRECO_DEBUG("Failed login\n");
			goto end_init;
		}

		/* create remote to db */

		self->remote_id = irreco_webdb_cache_create_new_remote(
				webdb_cache, comment, category,
				manufacturer, model, layout->filename->str,
				file_data, self->user_name, self->password);


		if (self->remote_id != 0) {
			rvalue = TRUE;
		}
		end_init:
		if (category != NULL) g_free(category);
		if (manufacturer != NULL) g_free(manufacturer);
		if (comment != NULL) g_free(comment);
		if (file_data != NULL) g_free(file_data);
		g_string_free(path, FALSE);

		if (rvalue == TRUE) {
			self->loader_state = LOADER_STATE_CONFIGURATIONS;
		} else {
			self->loader_state = LOADER_STATE_END;
		}

		IRRECO_RETURN_BOOL(TRUE);
	}
	case LOADER_STATE_CONFIGURATIONS: {
		IrrecoStringTable *devices = NULL;
		IRRECO_DEBUG("LOADER_STATE_CONFIGURATIONS\n");

		if (!irreco_layout_get_backends(self->irreco_data,
						layout->name->str,
						&devices)) {
			goto end_configurations;
		}
		IRRECO_STRING_TABLE_FOREACH(devices, name,
					    IrrecoBackendInstance *, instance) {
			IrrecoBackendFileContainer *file = NULL;
			gint id;
			if (!irreco_backend_instance_export_conf(instance, name,
								 &file)) {
				self->loader_state = LOADER_STATE_END;
				IRRECO_DEBUG("exporting error\n");
				goto end_configurations;
			}
			id = irreco_webdb_cache_get_config_id(webdb_cache,
							      file->hash->str,
							      file->name->str);

			if (id == 0) {
				if (!irreco_show_webdb_upload_dlg(
				    self->irreco_data, GTK_WINDOW(self),
				    file, name, self->user_name,
				    self->password)) {
					self->loader_state = LOADER_STATE_END;
					IRRECO_DEBUG("Uploading aborted\n");
					goto end_configurations;
				}

				id = irreco_webdb_cache_get_config_id(
							webdb_cache,
							file->hash->str,
							file->name->str);

			}

			/* Append configuration to remote */

			if (id != 0) {
				irreco_webdb_cache_add_configuration_to_remote(
					webdb_cache, self->remote_id, id,
					self->user_name, self->password);
			}

			if (file != NULL) {
				irreco_backend_file_container_free(file);
				file = NULL;
			}
		}
		IRRECO_STRING_TABLE_FOREACH_END

		self->loader_state = LOADER_STATE_THEMES;

		end_configurations:
		if (devices != NULL) irreco_string_table_free(devices);
		IRRECO_RETURN_BOOL(TRUE);
	}
	case LOADER_STATE_THEMES: {
		IrrecoStringTable *themes = NULL;
		IRRECO_DEBUG("LOADER_STATE_THEMES\n");

		if (!irreco_layout_get_themes(self->irreco_data,
					      layout->name->str, &themes)) {
			goto end_themes;
		}

		IRRECO_STRING_TABLE_FOREACH(themes, name, gchar *, date) {
		gint id;
		id = irreco_webdb_cache_get_theme_id_by_name_and_date(
			webdb_cache, name, date);

		if (id == 0) {

			/* Get theme */

			IrrecoTheme *theme;
			irreco_string_table_get(
			self->irreco_data->theme_manager->themes,
			name, (gpointer*) &theme);

			/* Upload theme to database */

			if (!irreco_theme_upload_dlg_run(GTK_WINDOW(self),
							 self->irreco_data,
							 theme,
							 self->user_name,
							 self->password)) {
				goto end_themes;
			}

			id = irreco_webdb_cache_get_theme_id_by_name_and_date(
				webdb_cache, theme->name->str,
				theme->version->str);
		}

		/* Append theme to remote */

		if (id != 0) {
			irreco_webdb_cache_add_theme_to_remote(webdb_cache,
							       self->remote_id,
							       id,
							       self->user_name,
							       self->password);
		}
		}
		IRRECO_STRING_TABLE_FOREACH_END

		/* Set remote downloadable to TRUE */

		irreco_webdb_cache_set_remote_downloadable(webdb_cache,
							   self->remote_id,
							   TRUE,
							   self->user_name,
							   self->password);

		irreco_info_dlg(GTK_WINDOW(self),
					"Remote uploaded successfully!");
		self->remote_uploaded = TRUE;

		/* Clean cache */
		if (webdb_cache->remote_id_hash != NULL) {
			g_hash_table_remove_all(webdb_cache->remote_id_hash);
		}
		if (webdb_cache->remote_categories != NULL) {
			irreco_string_table_free(
						webdb_cache->remote_categories);
			webdb_cache->remote_categories = NULL;
		}

		end_themes:
		if (themes != NULL) irreco_string_table_free(themes);

		self->loader_state = LOADER_STATE_END;
		IRRECO_RETURN_BOOL(TRUE);
	}
	case LOADER_STATE_END:
		IRRECO_DEBUG("LOADER_STATE_END\n");
		g_source_remove(self->loader_func_id);

		if (self->remote_uploaded == TRUE) {
			gtk_dialog_response(GTK_DIALOG(self),
					    GTK_RESPONSE_DELETE_EVENT);
		}
	}

	IRRECO_RETURN_BOOL(FALSE);
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Public Functions                                                           */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */

void irreco_show_remote_upload_dlg(IrrecoData *irreco_data, GtkWindow *parent)
{
	IrrecoRemoteUploadDlg	*self;
	gboolean		loop = TRUE;
	IrrecoButtonLayout *layout =
				irreco_data->window_manager->current_layout;
	IrrecoStringTable *themes = NULL;
	gboolean deb_theme_in_layout = FALSE;
	IrrecoStringTable *devices = NULL;
	GString* instance_number = g_string_new("");
	IRRECO_ENTER

	self = irreco_remote_upload_dlg_new(irreco_data, parent);

	irreco_remote_upload_dlg_populate_category(self);
	irreco_remote_upload_dlg_populate_manufacturer(self);
	gtk_entry_set_text(GTK_ENTRY(self->model),
			   irreco_button_layout_get_name(layout));

	irreco_layout_get_themes(irreco_data, layout->name->str, &themes);
	IRRECO_STRING_TABLE_FOREACH_KEY(themes, theme_name)
		IrrecoTheme *theme;
		irreco_string_table_get(irreco_data->theme_manager->themes,
					theme_name, (gpointer *) &theme);
		if (g_str_equal(theme->source->str, "deb")) {
			deb_theme_in_layout = TRUE;
		}
	IRRECO_STRING_TABLE_FOREACH_END

	if (deb_theme_in_layout) {
		irreco_error_dlg(GTK_WINDOW(self),
			"You can't upload remotes with Build In themes "
			"please check buttons and backgrounds.");
		goto end;
	}

	irreco_layout_get_backends(irreco_data, layout->name->str, &devices);
	IRRECO_STRING_TABLE_FOREACH(devices, name,
				    IrrecoBackendInstance *, instance)
		if (!(instance->lib->api->flags &
		    IRRECO_BACKEND_CONFIGURATION_EXPORT)) {
			GString *error_msg = g_string_new("");
			g_string_printf(error_msg,
			"\"%s\" backend doesn't support "
			"exporting its configuration...",
			instance->name->str);
			irreco_error_dlg(GTK_WINDOW(self), error_msg->str);
			g_string_free(error_msg, FALSE);
			goto end;
		}

		g_string_printf(instance_number, "%s", instance->name->str);
		g_string_erase(instance_number, 0, instance->name->len - 1);
		if (!g_str_equal(instance_number->str, "1")) {
			GString *error_msg = g_string_new("");
			g_string_printf(error_msg,
			"Remote contains commands from\n"
			"\"%s\"\nYou can't upload remotes where\n"
			"instance number differs from \"1\"",
			instance->name->str);
			irreco_error_dlg(GTK_WINDOW(self), error_msg->str);
			g_string_free(error_msg, FALSE);
			goto end;
		}
	IRRECO_STRING_TABLE_FOREACH_END

	do {
		gint response = gtk_dialog_run(GTK_DIALOG(self));
		switch (response) {
		case GTK_RESPONSE_DELETE_EVENT:
			IRRECO_DEBUG("GTK_RESPONSE_DELETE_EVENT\n");
			loop = FALSE;
			break;

		case GTK_RESPONSE_OK:
			IRRECO_DEBUG("GTK_RESPONSE_OK\n");
			self->loader_state = LOADER_STATE_INIT;

			self->loader_func_id = g_idle_add((GSourceFunc)
					irreco_remote_upload_dlg_send, self);
			break;
		}
	} while (loop);

	end:
	if (themes != NULL) irreco_string_table_free(themes);
	if (devices != NULL) irreco_string_table_free(devices);
	g_string_free(instance_number, FALSE);
	gtk_widget_destroy(GTK_WIDGET(self));
	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Events and Callbacks                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Events and Callbacks
 * @{
 */

void irreco_remote_upload_dlg_comment_size_request(GtkWidget *widget,
						   GtkRequisition *requisition,
						   IrrecoRemoteUploadDlg *self)
{
	GtkAdjustment* vadjustment = gtk_scrolled_window_get_vadjustment(
					GTK_SCROLLED_WINDOW(self->scroll));
	IRRECO_ENTER

	if (requisition->height > self->cursor_position) {
		vadjustment->value += 27;
	} else 	if (requisition->height < self->cursor_position) {
		vadjustment->value -= 27;
	}
	self->cursor_position = requisition->height;

	IRRECO_RETURN
}

/** @} */

/** @} */



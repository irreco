/*
 * irreco - Ir Remote Control
 * Copyright (C) 2008 Joni Kokko (t5kojo01@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

 /**
 * @addtogroup IrrecoTheme
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoTheme.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_THEME_H_TYPEDEF__
#define __IRRECO_THEME_H_TYPEDEF__
typedef struct _IrrecoTheme IrrecoTheme;
#endif /* __IRRECO_THEME_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_THEME_H__
#define __IRRECO_THEME_H__
#include "irreco.h"
#include "irreco_theme_bg.h"
#include "irreco_theme_button.h"
#include "irreco_config.h"
#include <irreco_string_table.h>




/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

struct _IrrecoTheme
{
	GString	*name;
	GString	*path;
	GString	*source;
	GString	*author;
	GString	*comment;
	GString	*preview_button_name;
	GString *version;
	IrrecoStringTable *backgrounds;
	IrrecoStringTable *buttons;
};

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

IrrecoTheme *irreco_theme_new();
void irreco_theme_free(IrrecoTheme *self);
void irreco_theme_print(IrrecoTheme *self);
IrrecoStringTable* irreco_theme_get_buttons(IrrecoTheme *self);
IrrecoThemeButton *irreco_theme_get_button(IrrecoTheme *self,
					   const char *button_name);
IrrecoStringTable* irreco_theme_get_backgrounds(IrrecoTheme *self);
IrrecoThemeBg *irreco_theme_get_background(IrrecoTheme *self,
					   const char *bg_name);
void irreco_theme_set_author(IrrecoTheme *self, const char * author);
void irreco_theme_set_comment(IrrecoTheme *self, const char * comment);
void irreco_theme_set_preview_button(IrrecoTheme *self,
				     const char * button_name);
void irreco_theme_update_keyfile(IrrecoTheme *self);
/*void irreco_theme_set_name(IrrecoTheme *self, IrrecoData *irreco_data,
			   const char * name);*/
void irreco_theme_set(IrrecoTheme *self, const char *name, const char *path,
		      const char *source, const char *author,
		      const char *comment, const char *preview_button_name,
		      const char *version);
void irreco_theme_check(IrrecoTheme *self);
IrrecoTheme *irreco_theme_copy(IrrecoTheme *self);
IrrecoTheme *theme_new_from_dir();
void irreco_theme_read(IrrecoTheme *self, const gchar *dir);
IrrecoTheme *irreco_theme_new_from_dir(const gchar *dir);
gboolean irreco_theme_save(IrrecoTheme *self,
			   const gchar *theme_path);
#endif /* __IRRECO_THEME_H__ */

/** @} */

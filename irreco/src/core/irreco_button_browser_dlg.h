/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi),
 * Harri Vattulainen (t5vaha01@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoButtonBrowserDlg
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoButtonBrowserDlg.
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_BUTTON_BROWSER_DLG_H_TYPEDEF__
#define __IRRECO_BUTTON_BROWSER_DLG_H_TYPEDEF__

#define IRRECO_TYPE_BUTTON_BROWSER_DLG             (irreco_button_browser_dlg_get_type ())
#define IRRECO_BUTTON_BROWSER_DLG(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), IRRECO_TYPE_BUTTON_BROWSER_DLG, IrrecoButtonBrowserDlg))
#define IRRECO_BUTTON_BROWSER_DLG_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), IRRECO_TYPE_BUTTON_BROWSER_DLG, IrrecoButtonBrowserDlgClass))
#define IRRECO_IS_BUTTON_BROWSER_DLG(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), IRRECO_TYPE_BUTTON_BROWSER_DLG))
#define IRRECO_IS_BUTTON_BROWSER_DLG_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), IRRECO_TYPE_BUTTON_BROWSER_DLG))
#define IRRECO_BUTTON_BROWSER_DLG_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), IRRECO_TYPE_BUTTON_BROWSER_DLG, IrrecoButtonBrowserDlgClass))

typedef struct _IrrecoButtonBrowserDlgClass IrrecoButtonBrowserDlgClass;
typedef struct _IrrecoButtonBrowserDlg IrrecoButtonBrowserDlg;

#endif /* __IRRECO_BUTTON_BROWSER_DLG_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_BUTTON_BROWSER_DLG_H__
#define __IRRECO_BUTTON_BROWSER_DLG_H__
#include "irreco.h"
#include "irreco_internal_dlg.h"
#include "irreco_button.h"
#include "irreco_listbox_image.h"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

struct _IrrecoButtonBrowserDlgClass
{
	IrrecoInternalDlgClass parent_class;
};

struct _IrrecoButtonBrowserDlg
{
	IrrecoInternalDlg parent_instance;

	IrrecoData		*irreco_data;
	IrrecoTheme		*theme; /* teema */
	IrrecoThemeButton	*current_image; /* valittu buttoni tai NULL */
	IrrecoListboxImage	*images;
	GtkWidget		*banner;
	gint			 loader_index; /* index of button */
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

GType irreco_button_browser_dlg_get_type (void) G_GNUC_CONST;
void irreco_button_browser_dlg_destroy(IrrecoButtonBrowserDlg * theme_upload_dlg);
IrrecoThemeButton *irreco_button_browser_dlg_run(GtkWindow *parent_window,
				    IrrecoData *irreco_data,
				    IrrecoTheme *irreco_theme);


#endif /* __IRRECO_BUTTON_BROWSER_DLG_H__ */

/** @} */


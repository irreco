/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoListbox
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoListbox.
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_LISTBOX_H_TYPEDEF__
#define __IRRECO_LISTBOX_H_TYPEDEF__

typedef struct _IrrecoListbox		IrrecoListbox;
typedef struct _IrrecoListboxClass 	IrrecoListboxClass;

#define IRRECO_TYPE_LISTBOX irreco_listbox_get_type()

#define IRRECO_LISTBOX(obj) \
	(G_TYPE_CHECK_INSTANCE_CAST ((obj), \
	IRRECO_TYPE_LISTBOX, IrrecoListbox))

#define IRRECO_LISTBOX_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_CAST ((klass), \
	IRRECO_TYPE_LISTBOX, IrrecoListboxClass))

#define IRRECO_IS_LISTBOX(obj) \
	(G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
	IRRECO_TYPE_LISTBOX))

#define IRRECO_IS_LISTBOX_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_TYPE ((klass), \
	IRRECO_TYPE_LISTBOX))

#define IRRECO_LISTBOX_GET_CLASS(obj) \
	(G_TYPE_INSTANCE_GET_CLASS ((obj), \
	IRRECO_TYPE_LISTBOX, IrrecoListboxClass))

#endif /* __IRRECO_LISTBOX_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_LISTBOX_H__
#define __IRRECO_LISTBOX_H__
#include "irreco.h"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

struct _IrrecoListbox {
	GtkScrolledWindow parent;

	GtkWidget		*tree_view;
	GtkListStore		*list_store;
	GtkTreeSelection	*tree_selection;

	GtkWidget		*vscrollbar;
	GtkWidget		*hscrollbar;

	gint	data_col_id;
	gint	text_col_id;

	gint	min_width;
	gint	max_width;
	gint	min_height;
	gint	max_height;

	gboolean select_new_rows;
};

struct _IrrecoListboxClass {
	GtkScrolledWindowClass parent_class;
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
GType		irreco_listbox_get_type	(void);
void		irreco_listbox_set_select_new_rows(IrrecoListbox *self,
						   gboolean select_new_rows);
void		irreco_listbox_set_autosize	(IrrecoListbox *self,
						 gint min_width,
						 gint max_width,
						 gint min_height,
						 gint max_height);
void		irreco_listbox_clear	(IrrecoListbox *self);
gboolean 	irreco_listbox_get_iter	(IrrecoListbox * irreco_listbox,
				 	 gint index,
					 GtkTreeIter * iter);
gboolean	irreco_listbox_set_selection		(IrrecoListbox *self,
							 gint index);
gboolean	irreco_listbox_get_selection	(IrrecoListbox  *self,
				     		 gint           *index,
						 gchar         **label,
						 gpointer       *user_data);
gint		irreco_listbox_get_selection_index	(IrrecoListbox *self);
gchar*		irreco_listbox_get_selection_label	(IrrecoListbox *self);
gpointer	irreco_listbox_get_selection_data	(IrrecoListbox *self);
gboolean	irreco_listbox_move_selected_up		(IrrecoListbox *self);
gboolean	irreco_listbox_move_selected_down	(IrrecoListbox *self);
gboolean	irreco_listbox_move	(IrrecoListbox *self,
					 gint from_index,
					 gint to_index);
gboolean	irreco_listbox_remove_selected	(IrrecoListbox *self);
gboolean	irreco_listbox_remove		(IrrecoListbox *self,
						 gint index);




#endif /* __IRRECO_LISTBOX_H__ */

/** @} */


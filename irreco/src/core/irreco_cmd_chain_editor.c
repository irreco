/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_cmd_chain_editor.h"
#include "irreco_cmd_chain_setup_dlg.h"
#include "irreco_cmd_dlg.h"

/**
 * @addtogroup IrrecoCmdChainEditor
 * @ingroup Irreco
 *
 * User interface for editing IrrecoCmdChains.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoCmdChainEditor.
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes.                                                                */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
static void irreco_cmd_chain_editor_set_sensitivity(IrrecoCmdChainEditor *self);
static void irreco_cmd_chain_editor_clear(IrrecoCmdChainEditor *self);
void irreco_cmd_chain_editor_add_clicked(GtkButton *button,
					 IrrecoCmdChainEditor *self);
void irreco_cmd_chain_editor_remove_clicked(GtkButton *button,
					    IrrecoCmdChainEditor *self);
void irreco_cmd_chain_editor_up_clicked(GtkButton *button,
					IrrecoCmdChainEditor *self);
void irreco_cmd_chain_editor_down_clicked(GtkButton *button,
					  IrrecoCmdChainEditor *self);
void irreco_cmd_chain_editor_properties_clicked(GtkButton *button,
						IrrecoCmdChainEditor *self);
void irreco_cmd_chain_editor_selection_changed(GtkTreeSelection * selection,
					       IrrecoCmdChainEditor *self);



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

G_DEFINE_TYPE(IrrecoCmdChainEditor, irreco_cmd_chain_editor,
	      IRRECO_TYPE_INTERNAL_WIDGET)

static void irreco_cmd_chain_editor_finalize(GObject *object)
{
	IRRECO_ENTER
	G_OBJECT_CLASS(irreco_cmd_chain_editor_parent_class)->finalize(object);
	irreco_cmd_chain_editor_clear(IRRECO_CMD_CHAIN_EDITOR(object));
	IRRECO_RETURN
}

static void irreco_cmd_chain_editor_class_init(IrrecoCmdChainEditorClass *klass)
{
	GObjectClass *object_class;
	IRRECO_ENTER

	object_class = G_OBJECT_CLASS (klass);
	object_class->finalize = irreco_cmd_chain_editor_finalize;
	IRRECO_RETURN
}

static void irreco_cmd_chain_editor_init(IrrecoCmdChainEditor *self)
{
	GdkPixbuf *icon;
	GtkWidget *image;
	IRRECO_ENTER

	gtk_box_set_spacing(GTK_BOX(self), 1);

	self->edit_mode = IRRECO_INDIRECT_EDITING;
	self->old_cmd_chain = NULL;
	self->new_cmd_chain = NULL;

	self->listbox = irreco_listbox_text_new();
	self->hbox = gtk_hbox_new(0, 0);

	gtk_box_pack_start(GTK_BOX(self), self->listbox, 1, 1, 1);
	gtk_box_pack_start(GTK_BOX(self), self->hbox, 0, 0, 0);

	self->add 	 = gtk_button_new_from_stock(GTK_STOCK_ADD);
	self->remove	 = gtk_button_new_from_stock(GTK_STOCK_REMOVE);
	self->up	 = gtk_button_new_from_stock(GTK_STOCK_GO_UP);
	self->down	 = gtk_button_new_from_stock(GTK_STOCK_GO_DOWN);
	self->properties = gtk_button_new();

	/* Set properties icon. We use just the icon to save space. */
	icon = gtk_widget_render_icon(self->properties, GTK_STOCK_PROPERTIES,
				      GTK_ICON_SIZE_BUTTON, NULL);
	image = gtk_image_new_from_pixbuf(icon);
	gtk_button_set_image(GTK_BUTTON(self->properties), image);

	gtk_box_pack_start_defaults(GTK_BOX(self->hbox), self->add);
	gtk_box_pack_start_defaults(GTK_BOX(self->hbox), self->remove);
	gtk_box_pack_start_defaults(GTK_BOX(self->hbox), self->up);
	gtk_box_pack_start_defaults(GTK_BOX(self->hbox), self->down);
	gtk_box_pack_start_defaults(GTK_BOX(self->hbox), self->properties);

	g_signal_connect(G_OBJECT(self->add), "clicked",
			 G_CALLBACK(irreco_cmd_chain_editor_add_clicked),
			 self);
	g_signal_connect(G_OBJECT(self->remove), "clicked",
			 G_CALLBACK(irreco_cmd_chain_editor_remove_clicked),
			 self);
	g_signal_connect(G_OBJECT(self->up), "clicked",
			 G_CALLBACK(irreco_cmd_chain_editor_up_clicked),
			 self);
	g_signal_connect(G_OBJECT(self->down), "clicked",
			 G_CALLBACK(irreco_cmd_chain_editor_down_clicked),
			 self);
	g_signal_connect(G_OBJECT(self->properties), "clicked",
			 G_CALLBACK(irreco_cmd_chain_editor_properties_clicked),
			 self);
	g_signal_connect(G_OBJECT(IRRECO_LISTBOX(
			 self->listbox)->tree_selection),
			 "changed",
			 G_CALLBACK(irreco_cmd_chain_editor_selection_changed),
			 self);

	irreco_cmd_chain_editor_set_sensitivity(self);

	IRRECO_RETURN
}

GtkWidget* irreco_cmd_chain_editor_new(IrrecoData *irreco_data,
				       GtkWindow *parent)
{
	IrrecoCmdChainEditor *self;
	IRRECO_ENTER

	self = g_object_new(IRRECO_TYPE_CMD_CHAIN_EDITOR,
			    "irreco-data", irreco_data,
			    NULL);
	irreco_cmd_chain_editor_set_parent_window(self, parent);
	IRRECO_RETURN_PTR(GTK_WIDGET(self));
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Private Functions
 * @{
 */

static void irreco_cmd_chain_editor_clear(IrrecoCmdChainEditor *self)
{
	IRRECO_ENTER
	irreco_cmd_chain_destroy(self->new_cmd_chain);
	self->old_cmd_chain = NULL;
	self->new_cmd_chain = NULL;
	IRRECO_RETURN
}

/**
 * Get the command chain which we should be editing.
 */
static IrrecoCmdChain *
irreco_cmd_chain_editor_get_edit_chain(IrrecoCmdChainEditor *self)
{
	IRRECO_ENTER
	switch (self->edit_mode) {
	case IRRECO_INDIRECT_EDITING:
		g_assert(self->new_cmd_chain);
		IRRECO_RETURN_PTR(self->new_cmd_chain);

	case IRRECO_DIRECT_EDITING:
		g_assert(self->old_cmd_chain);
		IRRECO_RETURN_PTR(self->old_cmd_chain);
	}

	/* Invalid enum value. */
	g_assert_not_reached();
}

static void irreco_cmd_chain_editor_populate_listbox(IrrecoCmdChainEditor *self)
{
	IrrecoCmdChain *edit_chain;
	IRRECO_ENTER

	edit_chain = irreco_cmd_chain_editor_get_edit_chain(self);
	irreco_listbox_clear(IRRECO_LISTBOX(self->listbox));
	IRRECO_CMD_CHAIN_FOREACH(edit_chain, command)
		irreco_listbox_text_append(IRRECO_LISTBOX_TEXT(self->listbox),
					   irreco_cmd_get_long_name(command),
					   command);
	IRRECO_CMD_CHAIN_FOREACH_END
	irreco_listbox_set_selection(IRRECO_LISTBOX(self->listbox), 0);
	IRRECO_RETURN
}

static void irreco_cmd_chain_editor_set_sensitivity(IrrecoCmdChainEditor *self)
{
	gint sel_index;
	IRRECO_ENTER

	sel_index = irreco_listbox_get_selection_index(
		IRRECO_LISTBOX(self->listbox));

	if (sel_index == -1) {
		gtk_widget_set_sensitive(self->remove, FALSE);
		gtk_widget_set_sensitive(self->up, FALSE);
		gtk_widget_set_sensitive(self->down, FALSE);
	} else if (IRRECO_LISTBOX(self->listbox)->list_store->length == 1) {
		gtk_widget_set_sensitive(self->remove, TRUE);
		gtk_widget_set_sensitive(self->up, FALSE);
		gtk_widget_set_sensitive(self->down, FALSE);
	} else if (sel_index == 0) {
		gtk_widget_set_sensitive(self->remove, TRUE);
		gtk_widget_set_sensitive(self->up, FALSE);
		gtk_widget_set_sensitive(self->down, TRUE);
	} else if (sel_index ==
		  (IRRECO_LISTBOX(self->listbox)->list_store->length - 1)) {
		gtk_widget_set_sensitive(self->remove, TRUE);
		gtk_widget_set_sensitive(self->up, TRUE);
		gtk_widget_set_sensitive(self->down, FALSE);
	} else {
		gtk_widget_set_sensitive(self->remove, TRUE);
		gtk_widget_set_sensitive(self->up, TRUE);
		gtk_widget_set_sensitive(self->down, TRUE);
	}
	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Public Functions                                                           */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */

/**
 * Set parent window for popup dialogs.
 */
void irreco_cmd_chain_editor_set_parent_window(IrrecoCmdChainEditor *self,
					       GtkWindow *parent)
{
	IRRECO_ENTER
	self->parent_window = parent;
	IRRECO_RETURN
}

void irreco_cmd_chain_editor_set_edit_mode(IrrecoCmdChainEditor *self,
					   IrrecoEditMode edit_mode)
{
	IRRECO_ENTER
	switch (edit_mode) {
	case IRRECO_INDIRECT_EDITING:
	case IRRECO_DIRECT_EDITING:
		self->edit_mode = edit_mode;
		break;

	default:
		IRRECO_ERROR("Invalid edit mode \"%i\".\n", edit_mode);
		break;
	}
	IRRECO_RETURN
}

void irreco_cmd_chain_editor_set_chain(IrrecoCmdChainEditor *self,
				       IrrecoCmdChain *cmd_chain)
{
	IRRECO_ENTER

	g_assert(cmd_chain != NULL);
	irreco_cmd_chain_editor_clear(self);

	if (self->edit_mode == IRRECO_INDIRECT_EDITING) {
		self->old_cmd_chain = cmd_chain;
		self->new_cmd_chain = irreco_cmd_chain_create();
		irreco_cmd_chain_copy(cmd_chain, self->new_cmd_chain);
	} else {
		self->old_cmd_chain = cmd_chain;
	}

	irreco_cmd_chain_editor_populate_listbox(self);
	IRRECO_RETURN
}

IrrecoCmdChain *irreco_cmd_chain_editor_get_chain(IrrecoCmdChainEditor *self)
{
	IRRECO_ENTER
	IRRECO_RETURN_PTR(self->old_cmd_chain);
}

void irreco_cmd_chain_editor_set_autosize(IrrecoCmdChainEditor *self,
					  gint min_width,
					  gint max_width,
					  gint min_height,
					  gint max_height)
{
	gint width, height;
	IRRECO_ENTER
	gtk_widget_get_size_request(self->hbox, &width, &height);
	if (max_width < width) max_width = width;
	irreco_listbox_set_autosize(IRRECO_LISTBOX(self->listbox),
				    min_width, max_width,
				    min_height, max_height - height);
	IRRECO_RETURN
}

void irreco_cmd_chain_editor_apply_changes(IrrecoCmdChainEditor *self)
{
	IRRECO_ENTER

	g_assert(self->edit_mode == IRRECO_INDIRECT_EDITING);

	if (self->old_cmd_chain != NULL && self->new_cmd_chain != NULL) {
		IRRECO_DEBUG("Saving command chain changes.\n");
		irreco_cmd_chain_print(self->new_cmd_chain);
		irreco_cmd_chain_copy(self->new_cmd_chain, self->old_cmd_chain);
	}
	IRRECO_RETURN
}

void irreco_cmd_chain_editor_reset_changes(IrrecoCmdChainEditor *self)
{
	IRRECO_ENTER

	g_assert(self->edit_mode == IRRECO_INDIRECT_EDITING);

	if (self->old_cmd_chain != NULL && self->new_cmd_chain != NULL) {
		irreco_cmd_chain_copy(self->old_cmd_chain, self->new_cmd_chain);
	}
	IRRECO_RETURN
}

void irreco_cmd_chain_editor_clear_chain(IrrecoCmdChainEditor *self)
{
	IRRECO_ENTER
	irreco_cmd_chain_remove_all(self->new_cmd_chain);
	irreco_cmd_chain_editor_populate_listbox(self);
	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Signals, Events and Callbacks                                              */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Events and Callbacks
 * @{
 */

void irreco_cmd_chain_editor_add_clicked(GtkButton *button,
					 IrrecoCmdChainEditor *self)
{
	IrrecoCmdChain *edit_chain;
	IrrecoCmd *command;
	IRRECO_ENTER

	command = irreco_cmd_create();
	edit_chain = irreco_cmd_chain_editor_get_edit_chain(self);

	if (irreco_cmd_dlg_run(irreco_internal_widget_get_irreco_data(
		IRRECO_INTERNAL_WIDGET(self)), command, self->parent_window)) {

		irreco_cmd_chain_append(edit_chain, command);
		irreco_listbox_text_append(IRRECO_LISTBOX_TEXT(self->listbox),
					   irreco_cmd_get_long_name(command),
					   command);
		gtk_widget_grab_focus(IRRECO_LISTBOX(self->listbox)->tree_view);
	} else {
		irreco_cmd_destroy(command);
	}

	irreco_cmd_chain_editor_set_sensitivity(self);
	IRRECO_RETURN
}

void irreco_cmd_chain_editor_remove_clicked(GtkButton *button,
					    IrrecoCmdChainEditor *self)
{
	gint sel_index;
	IrrecoCmdChain *edit_chain;
	IRRECO_ENTER

	sel_index = irreco_listbox_get_selection_index(
		IRRECO_LISTBOX(self->listbox));
	edit_chain = irreco_cmd_chain_editor_get_edit_chain(self);
	irreco_cmd_chain_remove(edit_chain, sel_index);
	irreco_listbox_remove_selected(IRRECO_LISTBOX(self->listbox));
	irreco_cmd_chain_editor_set_sensitivity(self);
	gtk_widget_grab_focus(IRRECO_LISTBOX(self->listbox)->tree_view);
	IRRECO_RETURN
}

void irreco_cmd_chain_editor_up_clicked(GtkButton *button,
					IrrecoCmdChainEditor *self)
{
	gint sel_index;
	IrrecoCmdChain *edit_chain;
	IRRECO_ENTER

	sel_index = irreco_listbox_get_selection_index(
		IRRECO_LISTBOX(self->listbox));
	edit_chain = irreco_cmd_chain_editor_get_edit_chain(self);
	irreco_cmd_chain_move(edit_chain, sel_index, sel_index - 1);
	irreco_listbox_move_selected_up(IRRECO_LISTBOX(self->listbox));
	irreco_cmd_chain_editor_set_sensitivity(self);
	gtk_widget_grab_focus(IRRECO_LISTBOX(self->listbox)->tree_view);
	IRRECO_RETURN
}

void irreco_cmd_chain_editor_down_clicked(GtkButton *button,
					  IrrecoCmdChainEditor *self)
{
	gint sel_index;
	IrrecoCmdChain *edit_chain;
	IRRECO_ENTER

	sel_index = irreco_listbox_get_selection_index(
		IRRECO_LISTBOX(self->listbox));
	edit_chain = irreco_cmd_chain_editor_get_edit_chain(self);
	irreco_cmd_chain_move(edit_chain, sel_index, sel_index + 1);
	irreco_listbox_move_selected_down(IRRECO_LISTBOX(self->listbox));
	irreco_cmd_chain_editor_set_sensitivity(self);
	gtk_widget_grab_focus(IRRECO_LISTBOX(self->listbox)->tree_view);
	IRRECO_RETURN
}

void irreco_cmd_chain_editor_properties_clicked(GtkButton *button,
						IrrecoCmdChainEditor *self)
{
	GtkWidget *dlg;
	IrrecoCmdChain *edit_chain;
	IRRECO_ENTER

	edit_chain = irreco_cmd_chain_editor_get_edit_chain(self);
	dlg = irreco_cmd_chain_setup_dlg_new(self->parent_window, edit_chain);
	gtk_dialog_run(GTK_DIALOG(dlg));
	gtk_widget_destroy(dlg);
	IRRECO_RETURN
}

void irreco_cmd_chain_editor_selection_changed(GtkTreeSelection *selection,
					       IrrecoCmdChainEditor *self)
{
	IRRECO_ENTER
	irreco_cmd_chain_editor_set_sensitivity(self);
	IRRECO_RETURN
}

/** @} */

/** @} */

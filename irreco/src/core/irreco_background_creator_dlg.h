/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * irreco - Ir Remote Control
 * Copyright (C) 2008 Pekka Gehör (pegu6@msn.com)
 *
 * irreco_background_creator_dlg.c is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * irreco_background_creator_dlg.c is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __IRRECO_BACKGROUND_CREATOR_DLG_H_TYPEDEF__
#define __IRRECO_BACKGROUND_CREATOR_DLG_H_TYPEDEF__

#define IRRECO_TYPE_BACKGROUND_CREATOR_DLG             (irreco_background_creator_dlg_get_type ())
#define IRRECO_BACKGROUND_CREATOR_DLG(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), IRRECO_TYPE_BACKGROUND_CREATOR_DLG, IrrecoBackgroundCreatorDlg))
#define IRRECO_BACKGROUND_CREATOR_DLG_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), IRRECO_TYPE_BACKGROUND_CREATOR_DLG, IrrecoBackgroundCreatorDlgClass))
#define IRRECO_IS_BACKGROUND_CREATOR_DLG(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), IRRECO_TYPE_BACKGROUND_CREATOR_DLG))
#define IRRECO_IS_BACKGROUND_CREATOR_DLG_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), IRRECO_TYPE_BACKGROUND_CREATOR_DLG))
#define IRRECO_BACKGROUND_CREATOR_DLG_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), IRRECO_TYPE_BACKGROUND_CREATOR_DLG, IrrecoBackgroundCreatorDlgClass))

typedef struct _IrrecoBackgroundCreatorDlgClass IrrecoBackgroundCreatorDlgClass;
typedef struct _IrrecoBackgroundCreatorDlg IrrecoBackgroundCreatorDlg;

#endif /* __IRRECO_BACKGROUND_CREATOR_DLG_H_TYPEDEF__ */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_BACKGROUND_CREATOR_DLG_H__
#define __IRRECO_BACKGROUND_CREATOR_DLG_H__
#include "irreco.h"
#include "irreco_internal_dlg.h"
#include "irreco_bg_browser_widget.h"
#include "irreco_theme_creator_dlg.h"
#include "irreco_button.h"
#include "irreco_button_browser_widget.h"
#include "irreco_theme_creator_backgrounds.h"

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

struct _IrrecoBackgroundCreatorDlgClass
{
	IrrecoInternalDlgClass parent_class;
};

struct _IrrecoBackgroundCreatorDlg
{
	IrrecoInternalDlg	parent_instance;
	IrrecoTheme		*theme;
	IrrecoData		*irreco_data;
	GtkWindow		*parent_window;

	GtkWidget		*entry_name;
	GtkWidget		*select_button;
	GtkWidget		*preview_image;
	GtkWidget		*label_size;
	/* Buttons */
	GtkWidget		*cancel_button;
	GtkWidget		*add_button;

	GtkWidget 		*preview;
	IrrecoBgBrowserWidget	*theme_bg_browser;
	GString			*filename;

	gint			loader_state;
	gint			loader_func_id;

};

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

GType irreco_background_creator_dlg_get_type (void) G_GNUC_CONST;
gboolean irreco_background_creator_dlg_run(IrrecoData *irreco_data,
					   IrrecoTheme *irreco_theme,
					   GtkWindow *parent_window,
					   IrrecoThemeBg *bg);

#endif /* __IRRECO_BACKGROUND_CREATOR_DLG_H__ */
/** @} */

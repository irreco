/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *	      &  2008  Joni Kokko     (t5kojo01@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoDeviceDlg
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoDeviceDlg.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */


#ifndef __IRRECO_DEVICE_DLG_H_TYPEDEF__
#define __IRRECO_DEVICE_DLG_H_TYPEDEF__

#define IRRECO_TYPE_DEVICE_DLG irreco_device_dlg_get_type()

#define IRRECO_DEVICE_DLG(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  IRRECO_TYPE_DEVICE_DLG, IrrecoDeviceDlg))

#define IRRECO_DEVICE_DLG_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  IRRECO_TYPE_DEVICE_DLG, IrrecoDeviceDlgClass))

#define IRRECO_IS_DEVICE_DLG(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  IRRECO_TYPE_DEVICE_DLG))

#define IRRECO_IS_DEVICE_DLG_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  IRRECO_TYPE_DEVICE_DLG))

#define IRRECO_DEVICE_DLG_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  IRRECO_TYPE_DEVICE_DLG, IrrecoDeviceDlgClass))

typedef struct _IrrecoDeviceDlg IrrecoDeviceDlg;
typedef struct _IrrecoDeviceDlgClass IrrecoDeviceDlgClass;

#endif /* __IRRECO_DEVICE_DLG_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_DEVICE_DLG_H__
#define __IRRECO_DEVICE_DLG_H__
#include "irreco.h"
#include "irreco_dlg.h"
#include "irreco_data.h"
#include "irreco_listbox_text.h"

#include <glib-object.h>



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
struct _IrrecoDeviceDlg {
	IrrecoDlg parent;
	GtkWidget * vbox;
	IrrecoData * irreco_data;
	IrrecoListbox *listbox;
};

struct _IrrecoDeviceDlgClass {
	IrrecoDlgClass parent_class;
};

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
void irreco_show_device_dlg(IrrecoData *irreco_data,
			    GtkWindow *parent);

GType irreco_device_dlg_get_type (void);
/*GtkWidget * irreco_device_dlg_new (GtkWindow *parent);*/

#endif /* __IRRECO_DEVICE_DLG_H__ */

/** @} */

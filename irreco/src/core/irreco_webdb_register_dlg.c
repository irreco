/*
 * irreco - Ir Remote Control
 * Copyright (C) 2008 Sami Mäki (kasmra@xob.kapsi.fi),
 * Harri Vattulainen (t5vaha01@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_webdb_register_dlg.h"

/**
 * @addtogroup IrrecoWebdbRegisterDlg
 * @ingroup Irreco
 *
 * Dialog based interface for user registration to Irreco webDB.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoWebdbRegisterDlg.
 */

G_DEFINE_TYPE (IrrecoWebdbRegisterDlg, irreco_webdb_register_dlg,
	       IRRECO_TYPE_DLG)


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
gboolean irreco_webdb_register_dlg_check_user_exists(
			       IrrecoWebdbRegisterDlg *self);
gboolean irreco_webdb_register_dlg_add_user(IrrecoWebdbRegisterDlg *self);
gboolean irreco_webdb_register_dlg_add_user_cache(
						IrrecoWebdbRegisterDlg *self);

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

static void irreco_webdb_register_dlg_dispose (GObject *object)
{
	if (G_OBJECT_CLASS (irreco_webdb_register_dlg_parent_class)->dispose)
		G_OBJECT_CLASS (
		irreco_webdb_register_dlg_parent_class)->dispose (object);
}

static void irreco_webdb_register_dlg_finalize (GObject *object)
{
	if (G_OBJECT_CLASS (irreco_webdb_register_dlg_parent_class)->finalize)
		G_OBJECT_CLASS (
		irreco_webdb_register_dlg_parent_class)->finalize (object);
}

static void
irreco_webdb_register_dlg_class_init (IrrecoWebdbRegisterDlgClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);


	object_class->dispose = irreco_webdb_register_dlg_dispose;
	object_class->finalize = irreco_webdb_register_dlg_finalize;
}

static void irreco_webdb_register_dlg_init (IrrecoWebdbRegisterDlg *self)
{
	GtkWidget *table;
	GtkWidget *label_name;
	GtkWidget *label_email;
	GtkWidget *label_passwd;
	GtkWidget *label_repasswd;
	GtkWidget *entry_name;
	GtkWidget *entry_email;
	GtkWidget *entry_passwd;
	GtkWidget *entry_repasswd;
	GtkWidget *align;
	GtkWidget *spacemaker;
	IRRECO_ENTER

	/* add buttons and stuff to register dlg */
	gtk_window_set_title(GTK_WINDOW(self),_("Register to Irreco Database"));
	gtk_window_set_modal(GTK_WINDOW(self), TRUE);
	gtk_window_set_destroy_with_parent(GTK_WINDOW(self), TRUE);
	gtk_dialog_set_has_separator(GTK_DIALOG(self), FALSE);
	gtk_dialog_add_buttons(GTK_DIALOG(self),
			       GTK_STOCK_OK, GTK_RESPONSE_OK,
			       NULL);

	/* rest of stuff in a table */
	table = gtk_table_new(5, 2, FALSE);

	/* labels */
	label_name = gtk_label_new("Nickname:");
	label_email = gtk_label_new("E-mail:");
	label_passwd = gtk_label_new("Password:");
	label_repasswd = gtk_label_new("Retype password:");
	gtk_table_attach_defaults(GTK_TABLE(table), label_name, 0, 1, 0, 1);
	gtk_table_attach_defaults(GTK_TABLE(table), label_email, 0, 1, 1, 2);
	gtk_table_attach_defaults(GTK_TABLE(table), label_passwd, 0, 1, 2, 3);
	gtk_table_attach_defaults(GTK_TABLE(table), label_repasswd, 0, 1, 3, 4);
	gtk_misc_set_alignment(GTK_MISC(label_name), 0, 0.5);
	gtk_misc_set_alignment(GTK_MISC(label_email), 0, 0.5);
	gtk_misc_set_alignment(GTK_MISC(label_passwd), 0, 0.5);
	gtk_misc_set_alignment(GTK_MISC(label_repasswd), 0, 0.5);

	/* text boxes */
	entry_name = gtk_entry_new();
	entry_email = gtk_entry_new();
	entry_passwd = gtk_entry_new();
	entry_repasswd = gtk_entry_new();
	gtk_entry_set_visibility(GTK_ENTRY(entry_passwd), FALSE);
	gtk_entry_set_visibility(GTK_ENTRY(entry_repasswd), FALSE);

	/* Hack to make entries take more than 16 characters */
	gtk_entry_set_text(GTK_ENTRY(entry_name),
				"...and they all lived happily ever after");
	gtk_entry_set_text(GTK_ENTRY(entry_name), "");
	gtk_entry_set_text(GTK_ENTRY(entry_email),
				"...and they all lived happily ever after");
	gtk_entry_set_text(GTK_ENTRY(entry_email), "");
	gtk_entry_set_text(GTK_ENTRY(entry_passwd),
				"...and they all lived happily ever after");
	gtk_entry_set_text(GTK_ENTRY(entry_passwd), "");
	gtk_entry_set_text(GTK_ENTRY(entry_repasswd),
				"...and they all lived happily ever after");
	gtk_entry_set_text(GTK_ENTRY(entry_repasswd), "");

	self->name = gtk_entry_get_text(GTK_ENTRY(entry_name));
	self->email = gtk_entry_get_text(GTK_ENTRY(entry_email));
	self->passwd = gtk_entry_get_text(GTK_ENTRY(entry_passwd));
	self->repasswd = gtk_entry_get_text(GTK_ENTRY(entry_repasswd));
	gtk_entry_set_max_length(GTK_ENTRY(entry_name), 40);
	gtk_entry_set_max_length(GTK_ENTRY(entry_email), 40);
	gtk_entry_set_max_length(GTK_ENTRY(entry_passwd), 40);
	gtk_entry_set_max_length(GTK_ENTRY(entry_repasswd), 40);
	gtk_table_attach(GTK_TABLE(table), entry_name, 1, 2, 0, 1,
			 GTK_FILL, GTK_FILL, 0,0);
	gtk_table_attach(GTK_TABLE(table), entry_email , 1, 2, 1, 2,
			 GTK_FILL, GTK_FILL, 0,0);
	gtk_table_attach(GTK_TABLE(table), entry_passwd , 1, 2, 2, 3,
			 GTK_FILL, GTK_FILL, 0,0);
	gtk_table_attach(GTK_TABLE(table), entry_repasswd, 1, 2, 3, 4,
			 GTK_FILL, GTK_FILL, 0,0);
	/* hack for textboxes to fill available area */
	/* would require window resize, so doesn't work at the moment */
	spacemaker = gtk_hbox_new(TRUE, 0);
	gtk_table_attach(GTK_TABLE(table), spacemaker, 1, 2, 4, 5,
			 GTK_EXPAND, GTK_SHRINK, 0,0);

	/* 12px padding around table */
	align = gtk_alignment_new(0.5, 0.5, 1, 1);
	gtk_alignment_set_padding(GTK_ALIGNMENT(align), 12, 12, 12, 12);
	gtk_container_add(GTK_CONTAINER(align), GTK_WIDGET(table));
	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(self)->vbox),
			  GTK_WIDGET(align));

	gtk_widget_show_all(GTK_WIDGET(self));

	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Private Functions
 * @{
 */


IrrecoWebdbRegisterDlg
    *irreco_webdb_register_dlg_new (IrrecoData *irreco_data, GtkWindow *parent)
{
	IrrecoWebdbRegisterDlg *self;
	IRRECO_ENTER

	self = g_object_new(IRRECO_TYPE_WEBDB_REGISTER_DLG, NULL);
	irreco_dlg_set_parent(IRRECO_DLG(self), parent);
	self->irreco_data = irreco_data;
	IRRECO_RETURN_PTR(self);
}

gboolean irreco_webdb_register_dlg_add_user_cache(IrrecoWebdbRegisterDlg *self)
{
	IrrecoWebdbCache *webdb_cache = NULL;
	IRRECO_ENTER

	webdb_cache = irreco_data_get_webdb_cache(self->irreco_data, FALSE);

	if(irreco_webdb_cache_add_user(webdb_cache, self->name, self-> email,
					self->passwd)) {
		IRRECO_RETURN_BOOL(TRUE);
	} else {
		irreco_error_dlg(GTK_WINDOW(self),
				irreco_webdb_cache_get_error(webdb_cache));

		IRRECO_RETURN_BOOL(FALSE);
	}
}

gboolean irreco_webdb_register_dlg_add_user(IrrecoWebdbRegisterDlg *self)
{
	gchar *character;
        const gchar *passwd_unhashed;
	IRRECO_ENTER

	/* checks if username is already in webdb */
	if(irreco_webdb_register_dlg_check_user_exists(self)) {
		irreco_error_dlg(GTK_WINDOW(self), "Username already taken.");

		IRRECO_RETURN_BOOL(FALSE);
	}

	/* Check if password and retyped password doesn't match */
	if(strcmp(self->passwd, self->repasswd) != 0) {
		irreco_error_dlg(GTK_WINDOW(self),
		"Mismatching password and retyped password.");
		IRRECO_RETURN_BOOL(FALSE);
	}

	/* Check that name, email and passwd are long enough */
	if(strlen(self->name) < 6) {
		irreco_error_dlg(GTK_WINDOW(self),
		"Name too short.\n"
		"Minimum length is 6 characters.");
		IRRECO_RETURN_BOOL(FALSE);
	}
	if(strlen(self->email) < 6) {
		irreco_error_dlg(GTK_WINDOW(self),
		"E-mail too short.\n"
		"Minimum length is 6 characters.");
		IRRECO_RETURN_BOOL(FALSE);
	}
	if(strlen(self->passwd) < 6) {
		irreco_error_dlg(GTK_WINDOW(self),
		"Password too short.\n"
		"Minimum length is 6 characters.");
		IRRECO_RETURN_BOOL(FALSE);
	}

	/* Check email to include @ and . (dot) */
	character = "@";
	if(g_strrstr(self->email, character) == NULL) {
		irreco_error_dlg(GTK_WINDOW(self),
		"Not valid E-mail\n"
		"Missing @ (at).");
		IRRECO_RETURN_BOOL(FALSE);
	}
	character = ".";
	if(g_strrstr(self->email, character) == NULL) {
		irreco_error_dlg(GTK_WINDOW(self),
		"Not valid E-mail\n"
		"Missing . (dot).");
		IRRECO_RETURN_BOOL(FALSE);
	}

        /* Store passwd before hashing */
        passwd_unhashed = self->passwd;

	/* Convert passwd to SHA-1 */
	self->passwd = g_compute_checksum_for_string(G_CHECKSUM_SHA1,
						     self->passwd,
						     strlen(self->passwd));

	if(irreco_webdb_register_dlg_add_user_cache(self)) {
		IRRECO_RETURN_BOOL(TRUE);
	} else {
                self->passwd = passwd_unhashed;
		IRRECO_RETURN_BOOL(FALSE);
	}
}

gboolean irreco_webdb_register_dlg_check_user_exists(
		IrrecoWebdbRegisterDlg *self)
{
	IrrecoWebdbCache *webdb_cache = NULL;
	gboolean user_exists;
	IRRECO_ENTER

	webdb_cache = irreco_data_get_webdb_cache(self->irreco_data, FALSE);

	if(irreco_webdb_cache_get_user_exists(webdb_cache, self->name,
	   				  &user_exists)) {
		/* Successfully called webdb */
	} else {
		/* Failed to call db */
		irreco_error_dlg(GTK_WINDOW(self),
					 irreco_webdb_cache_get_error(
					 webdb_cache));
	}

	IRRECO_RETURN_BOOL(user_exists);
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Functions                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */

void irreco_show_webdb_register_dlg(IrrecoData *irreco_data, GtkWindow *parent)
{
	IrrecoWebdbRegisterDlg *self;
	gint response;
	gboolean loop = TRUE;

	IRRECO_ENTER

	/* create register dialog */
	self = IRRECO_WEBDB_REGISTER_DLG(
			irreco_webdb_register_dlg_new(irreco_data, parent));

	do
	{
		/* show register dlg */
		response = gtk_dialog_run(GTK_DIALOG(self));

		switch(response)
		{
			case GTK_RESPONSE_DELETE_EVENT:
				loop = FALSE;
				break;
			case GTK_RESPONSE_OK:
				/* Add user to webdb */
				if(irreco_webdb_register_dlg_add_user(self)) {
					/* User added succesfully */
					irreco_info_dlg(GTK_WINDOW(self),
					"Registration Successful");
					loop = FALSE;
				}
				break;
			default:
				break;

		}

	}while(loop);

	gtk_widget_destroy(GTK_WIDGET(self));
	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Events and Callbacks                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/** @} */


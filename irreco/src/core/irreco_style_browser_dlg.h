/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi),
 * Harri Vattulainen (t5vaha01@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoStyleBrowserDlg
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoStyleBrowserDlg.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */

#ifndef __IRRECO_STYLE_BROWSER_DLG_H_TYPEDEF__
#define __IRRECO_STYLE_BROWSER_DLG_H_TYPEDEF__

typedef struct _IrrecoStyleBrowserDlg IrrecoStyleBrowserDlg;
typedef struct _IrrecoStyleBrowserDlgLoading IrrecoStyleBrowserDlgLoading;
typedef struct _IrrecoStyleBrowserDlgClass IrrecoStyleBrowserDlgClass;

#define IRRECO_TYPE_STYLE_BROWSER_DLG irreco_style_browser_dlg_get_type()

#define IRRECO_STYLE_BROWSER_DLG(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  IRRECO_TYPE_STYLE_BROWSER_DLG, IrrecoStyleBrowserDlg))

#define IRRECO_STYLE_BROWSER_DLG_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  IRRECO_TYPE_STYLE_BROWSER_DLG, IrrecoStyleBrowserDlgClass))

#define IRRECO_IS_STYLE_BROWSER_DLG(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  IRRECO_TYPE_STYLE_BROWSER_DLG))

#define IRRECO_IS_STYLE_BROWSER_DLG_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  IRRECO_TYPE_STYLE_BROWSER_DLG))

#define IRRECO_STYLE_BROWSER_DLG_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  IRRECO_TYPE_STYLE_BROWSER_DLG, IrrecoStyleBrowserDlgClass))

#endif /* __IRRECO_STYLE_BROWSER_DLG_H_TYPEDEF__ */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_STYLE_BROWSER_DLG_H__
#define __IRRECO_STYLE_BROWSER_DLG_H__
#include "irreco.h"
#include "irreco_dlg.h"
#include "irreco_data.h"
#include "irreco_listbox.h"
#include "irreco_listbox_text.h"
#include "irreco_button_browser_widget.h"


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

struct _IrrecoStyleBrowserDlg{
	IrrecoDlg parent;
	IrrecoData			*irreco_data;
	IrrecoThemeButton		*style;
	IrrecoButtonBrowserWidget	*button_widget;
	IrrecoStyleBrowserDlgLoading	*loading;
	gboolean			once;
};

struct _IrrecoStyleBrowserDlgLoading {
	IrrecoStyleBrowserDlg	*self;
	GtkWidget		*banner;
	gint			pos;
	gint			len;
};

struct _IrrecoStyleBrowserDlgClass{
	IrrecoDlgClass parent_class;
};


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
GType irreco_style_browser_dlg_get_type (void);
GtkWidget* irreco_style_browser_dlg_new(IrrecoData *irreco_data,
					GtkWindow *parent);
void irreco_style_browser_dlg_set_irreco_data(IrrecoStyleBrowserDlg *self,
					      IrrecoData *irreco_data);
gboolean irreco_show_style_browser_dlg(IrrecoData *irreco_data,
				       GtkWindow *parent,
				       IrrecoThemeButton ** style);

#endif /* __IRRECO_STYLE_BROWSER_DLG_H__ */

/** @} */

/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoHardkeyMap
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoHardkeyMap.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_HARDKEY_H_TYPEDEF__
#define __IRRECO_HARDKEY_H_TYPEDEF__

#include <gdk/gdkkeysyms.h>

enum {
	IRRECO_HARDKEY_UP		= GDK_Up,
	IRRECO_HARDKEY_DOWN		= GDK_Down,
	IRRECO_HARDKEY_LEFT		= GDK_Left,
	IRRECO_HARDKEY_RIGHT		= GDK_Right,
	IRRECO_HARDKEY_SELECT		= GDK_Return,
	IRRECO_HARDKEY_BACK		= GDK_Escape,
	IRRECO_HARDKEY_MENU		= GDK_F4,
	IRRECO_HARDKEY_HOME		= GDK_F5,
	IRRECO_HARDKEY_FULLSCREEN	= GDK_F6,
	IRRECO_HARDKEY_PLUS		= GDK_F7,
	IRRECO_HARDKEY_MINUS		= GDK_F8
};

typedef struct _IrrecoHardkeyMap IrrecoHardkeyMap;


#endif /* __IRRECO_HARDKEY_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_HARDKEY_H__
#define __IRRECO_HARDKEY_H__
#include "irreco.h"
#include "irreco_cmd_chain_manager.h"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

struct _IrrecoHardkeyMap {
	IrrecoCmdChainManager *manager;
	GHashTable *table;
};

typedef void (*IrrecoHardkeyMapFunc) 	(IrrecoHardkeyMap *hardkey_map,
					 guint             keyval,
					 IrrecoCmdChainId  chain_id,
					 IrrecoCmdChain   *chain,
					 gpointer          user_data);



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
gchar*		  irreco_hardkey_to_str(	guint keyval);
IrrecoHardkeyMap* irreco_hardkey_map_new(	IrrecoCmdChainManager *manager);
void 		  irreco_hardkey_map_free(	IrrecoHardkeyMap *self);
void		  irreco_hardkey_map_assosiate_chain(
						IrrecoHardkeyMap *self,
						guint keyval);
gboolean 	  irreco_hardkey_map_assosiate_chain_with_id(
						IrrecoHardkeyMap *self,
						guint keyval,
						IrrecoCmdChainId id);
IrrecoCmdChain*	  irreco_hardkey_map_get_cmd_chain(
						IrrecoHardkeyMap *self,
						guint keyval);
gboolean	  irreco_hardkey_map_find_cmd_chain_id(
						IrrecoHardkeyMap *self,
						IrrecoCmdChainId id,
						guint *keyval);
gboolean	  irreco_hardkey_map_cmd_chain_exists(
						IrrecoHardkeyMap *self,
						guint keyval);
void		  irreco_hardkey_map_chain_foreach(
						IrrecoHardkeyMap *self,
						IrrecoHardkeyMapFunc func,
						gpointer user_data);




#endif /* __IRRECO_HARDKEY_H__ */

/** @} */

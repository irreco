/*
 * irreco - Ir Remote Control
 * Copyright 2008 Sami Mäki (kasmra@xob.kapsi.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoLoginDlg
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoLoginDlg.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */

#ifndef _IRRECO_LOGIN_DLG_H_TYPEDEF
#define _IRRECO_LOGIN_DLG_H_TYPEDEF

#define IRRECO_TYPE_LOGIN_DLG irreco_login_dlg_get_type()
#define IRRECO_LOGIN_DLG(obj) \
    (G_TYPE_CHECK_INSTANCE_CAST ((obj), IRRECO_TYPE_LOGIN_DLG, \
                                 IrrecoLoginDlg))
#define IRRECO_LOGIN_DLG_CLASS(klass) \
    (G_TYPE_CHECK_CLASS_CAST ((klass), IRRECO_TYPE_LOGIN_DLG, \
                              IrrecoLoginDlgClass))
#define IRRECO_IS_LOGIN_DLG(obj) \
    (G_TYPE_CHECK_INSTANCE_TYPE ((obj), IRRECO_TYPE_LOGIN_DLG))
#define IRRECO_IS_LOGIN_DLG_CLASS(klass) \
    (G_TYPE_CHECK_CLASS_TYPE ((klass), IRRECO_TYPE_LOGIN_DLG))
#define IRRECO_LOGIN_DLG_GET_CLASS(obj) \
    (G_TYPE_INSTANCE_GET_CLASS ((obj), IRRECO_TYPE_LOGIN_DLG, \
                                IrrecoLoginDlgClass))

typedef struct _IrrecoLoginDlg IrrecoLoginDlg;
typedef struct _IrrecoLoginDlgClass IrrecoLoginDlgClass;

#endif /* _IRRECO_LOGIN_DLG_H_TYPEDEF */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_LOGIN_DLG_H__
#define __IRRECO_LOGIN_DLG_H__
#include "irreco.h"
#include "irreco_dlg.h"
#include "irreco_data.h"


/* Include the prototypes for GConf client functions. */
#include <gconf/gconf-client.h>


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

struct _IrrecoLoginDlg {
        IrrecoDlg 	parent;
        IrrecoData 	*irreco_data;

        const gchar	*user;
        const gchar	*password;

        const gchar     *userptr;
        const gchar     *passwdptr;

        GtkWidget	*entry_user;
	GtkWidget	*entry_password;
	GtkWidget	*check_password;

	gboolean	is_pw_entry_messed_with;
};

struct _IrrecoLoginDlgClass {
        IrrecoDlgClass parent_class;
};

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

GType irreco_login_dlg_get_type (void);
gboolean irreco_show_login_dlg(IrrecoData *irreco_data, GtkWindow *parent,
				  gchar **userptr, gchar **passwdptr);
gboolean irreco_login_dlg_login_cache(IrrecoLoginDlg *self, const gchar *pwhash);
void irreco_login_dlg_remember_password(GtkCheckButton *toggle_button,
		IrrecoLoginDlg *self);
void irreco_login_dlg_passwd_entry_messed_with(GtkEntry *pw_entry,
		IrrecoLoginDlg *login_dlg);

#endif /* __IRRECO_LOGIN_DLG_H__ */

/** @} */


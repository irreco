/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_backend_manager.h"
#include "irreco_config.h"

/**
 * @addtogroup IrrecoBackendManager
 * @ingroup Irreco
 *
 * Maintains tables of backend libraries and backend instances.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoBackendManager.
 */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

void
irreco_backend_manager_open_lib_dir_foreach(IrrecoDirForeachData * dir_data);



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction.                                                */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */


IrrecoBackendManager* irreco_backend_manager_create(IrrecoData *irreco_data)
{
	IrrecoBackendManager* manager;
	IRRECO_ENTER

	manager = g_slice_new0(IrrecoBackendManager);
	manager->irreco_data = irreco_data;
	manager->lib_table = irreco_string_table_new(
		G_DESTROYNOTIFY(irreco_backend_lib_close), NULL);
	manager->instance_table = irreco_string_table_new(
		G_DESTROYNOTIFY(irreco_backend_instance_destroy),
		IRRECO_KEY_SET_NOTIFY(irreco_backend_instance_set_name));
	manager->config_map = irreco_string_table_new(NULL,
		IRRECO_KEY_SET_NOTIFY(irreco_backend_instance_set_config));
	IRRECO_RETURN_PTR(manager);
}

void irreco_backend_manager_destroy(IrrecoBackendManager * manager)
{
	IRRECO_ENTER
	if (manager == NULL) IRRECO_RETURN
	irreco_string_table_free(manager->config_map);
	irreco_string_table_free(manager->instance_table);
	irreco_string_table_free(manager->lib_table);
	g_slice_free(IrrecoBackendManager, manager);

	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Utility.                                                                   */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Utility
 * @{
 */

void irreco_backend_manager_get_devcmd_lists(IrrecoBackendManager * manager)
{
	IRRECO_ENTER
	g_assert(manager != NULL);

	IRRECO_PRINTF("Getting device / command lists.\n");
	IRRECO_BACKEND_MANAGER_FOREACH_INSTANCE(manager, instance)
		irreco_backend_instance_get_devcmd_list(instance);
	IRRECO_BACKEND_MANAGER_FOREACH_END

	IRRECO_RETURN
}

void irreco_backend_manager_read_from_confs(IrrecoBackendManager * manager)
{
	IRRECO_ENTER
	g_assert(manager != NULL);

	IRRECO_PRINTF("Reading instace configurations.\n");
	IRRECO_BACKEND_MANAGER_FOREACH_INSTANCE(manager, instance)
		irreco_backend_instance_read_from_conf(instance);
	IRRECO_BACKEND_MANAGER_FOREACH_END

	IRRECO_RETURN
}

void irreco_backend_manager_print(IrrecoBackendManager * manager)
{
	gint i;
	IRRECO_ENTER
	g_assert(manager != NULL);

	i = 0;
	IRRECO_PRINTF("Backend libraries:\n");
	IRRECO_BACKEND_MANAGER_FOREACH_LIB(manager, lib)
		IRRECO_PRINTF(" %i. \"%s\" \"%s\"\n", i++,
			      lib->filename, lib->name);
	IRRECO_BACKEND_MANAGER_FOREACH_END

	i = 0;
	IRRECO_PRINTF("Backend instances:\n");
	IRRECO_BACKEND_MANAGER_FOREACH_INSTANCE(manager, instance)
		const gchar *name = irreco_backend_instance_get_name(instance);
		IRRECO_PRINTF(" %i. \"%s\" \"%s\" \"%s\"\n", i++,
			      instance->lib->name,
			      name,
			      instance->config);
	IRRECO_BACKEND_MANAGER_FOREACH_END

	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Library management.                                                        */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Library management
 * @{
 */

/**
 * Load backend from file.
 *
 * @param filepath	Complete path to library.
 * @return 		Pointer to new IrrecoBackendLib, or NULL on failure.
 */
IrrecoBackendLib *
irreco_backend_manager_load_lib(IrrecoBackendManager * manager,
			        const gchar * filepath)
{
	gchar *filename;
	IrrecoBackendLib *backend_lib;
	IRRECO_ENTER
	g_assert(manager != NULL);
	g_assert(filepath != NULL);

	filename = g_path_get_basename(filepath);
	backend_lib = irreco_backend_manager_load_with_name(
		manager, filepath, filename);
	g_free(filename);
	IRRECO_RETURN_PTR(backend_lib);
}

/**
 * Load backend from file.
 *
 * @param filepath	Complete path to library.
 * @param filename	Basename of the library.
 * @return 		Pointer to new IrrecoBackendLib, or NULL on failure.
 */
IrrecoBackendLib *
irreco_backend_manager_load_with_name(IrrecoBackendManager * manager,
				      const gchar * filepath,
				      const gchar * filename)
{
	IrrecoBackendLib *backend_lib;
	IRRECO_ENTER
	g_assert(manager != NULL);
	g_assert(filepath != NULL);
	g_assert(filename != NULL);

	if (irreco_string_table_exists(manager->lib_table, filename)) {
		IRRECO_ERROR("Backend \"%s\" has already been loaded.",
			     filename);
		IRRECO_RETURN_PTR(NULL);
	}

	backend_lib = irreco_backend_lib_load_with_name(filepath, filename);
	if (backend_lib != NULL) {
		irreco_string_table_add(manager->lib_table,
					backend_lib->filename,
					backend_lib);
		irreco_string_table_sort_abc(manager->lib_table);
	}
	IRRECO_RETURN_PTR(backend_lib);
}

/**
 * Load all backends from directory.
 */
gboolean
irreco_backend_manager_load_lib_dir(IrrecoBackendManager * manager,
				    gchar * directory)
{
	IrrecoDirForeachData dir_data;
	IRRECO_ENTER
	g_assert(manager != NULL);
	g_assert(directory != NULL);

	dir_data.directory = directory;
	dir_data.filesuffix = ".la";
	dir_data.user_data_1 = manager;

	IRRECO_RETURN_BOOL(irreco_dir_foreach(
		&dir_data, irreco_backend_manager_open_lib_dir_foreach));
}
void
irreco_backend_manager_open_lib_dir_foreach(IrrecoDirForeachData * dir_data)
{
	IrrecoBackendManager *manager;
	IRRECO_ENTER

	manager = (IrrecoBackendManager *) dir_data->user_data_1;
	irreco_backend_manager_load_with_name(manager,
					      dir_data->filepath,
					      dir_data->filename);

	IRRECO_RETURN
}

/**
 * Find library by filename.
 *
 * Returns: TRUE on success, FALSE otherwise.
 */
gboolean irreco_backend_manager_find_lib(IrrecoBackendManager * manager,
					 const gchar * filename,
					 IrrecoBackendLib ** lib)
{
	IRRECO_ENTER
	g_assert(manager != NULL);
	g_assert(filename != NULL);
	g_assert(lib != NULL);

	if (irreco_string_table_get(manager->lib_table, filename,
				    (gpointer *) lib)) {
		IRRECO_RETURN_BOOL(TRUE);
	}
	IRRECO_RETURN_BOOL(FALSE);
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Instance management.                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Instance management
 * @{
 */


/**
 * Create new IrrecoBackendInstance and add it to the instance_table.
 *
 * @param name		Name of instace. Or NULL for autogeneration.
 * @param config	Config file of instace. Or NULL for autogeneration.
 * @return 		New IrrecoBackendInstance, or NULL on failure.
 */
IrrecoBackendInstance *
irreco_backend_manager_create_instance(IrrecoBackendManager * manager,
				       IrrecoBackendLib * lib,
				       const gchar * name,
				       const gchar * config)
{
	IrrecoBackendInstance *instance;
	IRRECO_ENTER
	g_assert(manager != NULL);
	g_assert(lib != NULL);

	/* Create instance. */
	instance = irreco_backend_instance_create(lib, manager->irreco_data);
	if (instance == NULL) IRRECO_RETURN_PTR(NULL);

	/* Set instance name. */
	if (name != NULL) {
		if (irreco_backend_manager_set_instance_name(
			manager, instance, name) == FALSE) {
			IRRECO_ERROR("Could not set instace name to \"%s\"",
				     name);
			irreco_backend_instance_destroy(instance);
			IRRECO_RETURN_PTR(NULL);
		}
	} else {
		irreco_backend_manager_assign_instance_name(manager, instance);
	}

	/* Set instance config. */
	if (config != NULL) {
		if (irreco_backend_manager_set_instance_config(
			manager, instance, config) == FALSE) {
			IRRECO_ERROR("Could not set instace config to \"%s\"",
				     config);
			irreco_backend_instance_destroy(instance);
			IRRECO_RETURN_PTR(NULL);
		}
	} else {
		irreco_backend_manager_assign_instance_config(manager, instance);
	}

	IRRECO_RETURN_PTR(instance);
}

/**
 * Destroy IrrecoBackendInstance and remove it from the instance_table.
 *
 * @return TRUE on success, FALSE otherwise.
 */
gboolean
irreco_backend_manager_destroy_instance(IrrecoBackendManager * manager,
				       IrrecoBackendInstance * instance)
{
	gchar *config_path;
	IRRECO_ENTER

	g_assert(manager != NULL);
	g_assert(instance != NULL);

	if (irreco_string_table_steal_by_data(
		manager->instance_table, instance) == FALSE) {
		IRRECO_RETURN_BOOL(FALSE);
	}

	config_path = irreco_get_config_file("irreco", instance->config);
	irreco_string_table_remove(manager->config_map, instance->config);
	irreco_backend_instance_destroy_full(instance, TRUE);

	IRRECO_PRINTF("Deleting \"%s\".\n", config_path);
	g_unlink(config_path);
	g_free(config_path);

	IRRECO_RETURN_BOOL(TRUE);
}

/**
 * Find instance by name.
 *
 * @return TRUE on success, FALSE otherwise.
 */
gboolean irreco_backend_manager_find_instance(
				      IrrecoBackendManager * manager,
				      const gchar * name,
				      IrrecoBackendInstance ** instance)
{
	IRRECO_ENTER
	g_assert(manager != NULL);
	g_assert(name != NULL);
	g_assert(instance != NULL);

	if (irreco_string_table_get(manager->instance_table, name,
				    (gpointer *) instance)) {
		IRRECO_RETURN_BOOL(TRUE);
	}
	IRRECO_RETURN_BOOL(FALSE);
}

/**
 * Set name of the instance.
 *
 * @return TRUE on success, FALSE otherwise.
 */
gboolean
irreco_backend_manager_set_instance_name(IrrecoBackendManager * manager,
					 IrrecoBackendInstance * instance,
					 const gchar * name)
{
	const gchar *old_name = NULL;
	IRRECO_ENTER

	g_assert(manager != NULL);
	g_assert(instance != NULL);
	g_assert(name != NULL);

	if (irreco_string_table_exists(manager->instance_table, name)) {
		IRRECO_RETURN_BOOL(FALSE);
	}

	/* Add instace to the table if it has not been added. */
	old_name = irreco_backend_instance_get_name(instance);
	if (old_name == NULL || irreco_string_table_exists(
	    manager->instance_table, old_name) == FALSE) {
		irreco_string_table_add(manager->instance_table,
					name,
					instance);

	/* Else we can just change the key. */
	} else {
		irreco_string_table_change_key(manager->instance_table,
					       old_name,
					       name);
	}
	irreco_string_table_sort_abc(manager->instance_table);
	IRRECO_RETURN_BOOL(TRUE);
}

/**
 * Assingn a unique name to the instance.
 */
void
irreco_backend_manager_assign_instance_name(IrrecoBackendManager * manager,
					    IrrecoBackendInstance * instance)
{
	gint i = 0;
	GString *string;
	IRRECO_ENTER

	g_assert(manager != NULL);
	g_assert(instance != NULL);

	/* Generate a unique name for the instance. */
	string = g_string_new(NULL);
	do {
		g_string_set_size(string, 0);
		g_string_append_printf(string, "%s %i",
				       instance->lib->name, ++i);
	} while (irreco_string_table_exists(manager->instance_table,
					    string->str));
	irreco_backend_manager_set_instance_name(manager,
						 instance,
						 string->str);
	g_string_free(string, TRUE);

	IRRECO_RETURN
}

/**
 * Set configuration filename to the instance, if it is not used by another
 * instance.
 */
gboolean
irreco_backend_manager_set_instance_config(IrrecoBackendManager * manager,
					   IrrecoBackendInstance * instance,
					   const gchar * filename)
{
	IRRECO_ENTER
	g_assert(manager != NULL);
	g_assert(instance != NULL);
	g_assert(filename != NULL);

	if (irreco_string_table_exists(manager->config_map, filename)) {
		IRRECO_RETURN_BOOL(FALSE);
	}

	if (instance->config == NULL || irreco_string_table_exists(
	    manager->config_map, instance->config) == FALSE) {
		irreco_string_table_add(manager->config_map,
					filename,
					instance);
	} else {
		irreco_string_table_change_key(manager->config_map,
					       instance->config,
					       filename);
	}
	IRRECO_RETURN_BOOL(TRUE);
}

/**
 * Assingn a unique configuration filename to the instance.
 */
void
irreco_backend_manager_assign_instance_config(IrrecoBackendManager * manager,
					      IrrecoBackendInstance * instance)
{
	gint i = 0;
	GString *string;
	IRRECO_ENTER

	g_assert(manager != NULL);
	g_assert(instance != NULL);

	string = g_string_new(NULL);
	do {
		g_string_set_size(string, 0);
		g_string_append(string, "backend_");
		g_string_append(string, instance->lib->filename);
		irreco_char_replace(string->str, '.', '_');
		g_string_append_printf(string, "_%i.config", ++i);
	} while (irreco_string_table_exists(manager->config_map,
					    string->str));
	irreco_backend_manager_set_instance_config(manager,
						   instance,
						   string->str);
	g_string_free(string, TRUE);

	IRRECO_RETURN
}

/** @} */
/** @} */





















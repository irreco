/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoWindowEdit
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoWindowEdit.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_WINDOW_EDIT_H_TYPEDEF__
#define __IRRECO_WINDOW_EDIT_H_TYPEDEF__
typedef struct _IrrecoWindowEdit IrrecoWindowEdit;
#endif /* __IRRECO_WINDOW_EDIT_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_WINDOW_EDIT_H__
#define __IRRECO_WINDOW_EDIT_H__
#include "irreco.h"
#include "irreco_data.h"
#include "irreco_button.h"
#include "irreco_window.h"
#include "irreco_window_manager.h"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
struct _IrrecoWindowEdit {

	IrrecoWindow *window;
	IrrecoWindowManager *manager;
	IrrecoData *irreco_data;

	/* Handler id:s */
	gulong expose_handler_id;

	/* Buttonlayout being displayed. */
	IrrecoButtonLayout *irreco_layout;

	/* Menu stuff. */
	GtkWidget *menu;
	GtkWidget *menu_save;
	GtkWidget *menu_controllers;
	GtkWidget *menu_devices;
	GtkWidget *menu_download_irtrans;
	GtkWidget *menu_download_lirc;
	GtkWidget *menu_background;
	GtkWidget *menu_hardkeys;
	GtkWidget *menu_newbutton;

	GtkWidget *button_menu;
	IrrecoButton *button_menu_target;

	GtkWidget *menu_theme_manager;

	/* Dragging */
	GTimeVal	drag_start_time;
	gint 		drag_start_x;
	gint		drag_start_y;
	gint 		drag_pointer_x;
	gint 		drag_pointer_y;
	GdkModifierType drag_pointer_mask;
	gint		drag_grid;
	gboolean	drag_button_moved;
	gboolean	drag_handler_set;
	IrrecoButton	*drag_target;
	gint		drag_pointer_offset_x;
	gint		drag_pointer_offset_y;

	/* Grid image buffers. */
	GdkPixbuf *image_background;
	GdkPixbuf *image_grid_1;
	GdkPixbuf *image_grid_2;
	GdkPixbuf *image_grid_4;
	GdkPixbuf *image_grid_8;
	GdkPixbuf *image_grid_16;
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
IrrecoWindowEdit *irreco_window_edit_create(IrrecoWindowManager * manager);
void irreco_window_edit_destroy(IrrecoWindowEdit * edit_ui);
void irreco_window_edit_set_layout(IrrecoWindowEdit * edit_ui,
				   IrrecoButtonLayout * irreco_layout);



#endif /* __IRRECO_WINDOW_EDIT_H__ */

/** @} */


/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi),
 * Harri Vattulainen (t5vaha01@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_style_browser_dlg.h"
#include "irreco_scrolled_window.h"
#include <hildon/hildon-banner.h>

/**
 * @addtogroup IrrecoStyleBrowserDlg
 * @ingroup Irreco
 *
 * Dialog which displays a list of button styles to the user, so the user
 * can select one.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoStyleBrowserDlg.
 */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
void irreco_style_browser_dlg_set_button_widget(IrrecoStyleBrowserDlg *self);
void irreco_style_browser_dlg_theme_image_selection_changed(
						GtkTreeSelection * selection,
					        IrrecoStyleBrowserDlg * self);

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */


G_DEFINE_TYPE(IrrecoStyleBrowserDlg, irreco_style_browser_dlg, IRRECO_TYPE_DLG)

static void irreco_style_browser_dlg_finalize(GObject *object)
{
	IRRECO_ENTER
	G_OBJECT_CLASS(irreco_style_browser_dlg_parent_class)->finalize(object);
	IRRECO_RETURN
}

static void irreco_style_browser_dlg_class_init(IrrecoStyleBrowserDlgClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS(klass);
	object_class->finalize = irreco_style_browser_dlg_finalize;
}

static void irreco_style_browser_dlg_init(IrrecoStyleBrowserDlg *self)
{
	IRRECO_ENTER

	self->style = NULL;

	/* Construct dialog. */
	gtk_window_set_title(GTK_WINDOW(self), _("Style Browser"));
	gtk_window_set_modal(GTK_WINDOW(self), TRUE);
	gtk_window_set_destroy_with_parent(GTK_WINDOW(self), TRUE);
	gtk_dialog_set_has_separator(GTK_DIALOG(self), FALSE);

	/* Show dialog, hopefully on the left-top corner of the screen. */
	gtk_widget_show_all(GTK_WIDGET(self));
	gtk_window_move(GTK_WINDOW(self), 80, 60);
	IRRECO_RETURN
}

GtkWidget* irreco_style_browser_dlg_new(IrrecoData *irreco_data,
					GtkWindow *parent)
{
	IrrecoStyleBrowserDlg *self;
	IRRECO_ENTER
	self = g_object_new(IRRECO_TYPE_STYLE_BROWSER_DLG, NULL);
	irreco_dlg_set_parent(IRRECO_DLG(self), parent);
	irreco_style_browser_dlg_set_irreco_data(self, irreco_data);
	irreco_style_browser_dlg_set_button_widget(self);
	IRRECO_RETURN_PTR(self);
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Private Functions
 * @{
 */

static void irreco_style_browser_dlg_loading_cancel(IrrecoStyleBrowserDlg *self)
{
	IRRECO_ENTER
	if (self->loading != NULL) self->loading->self = NULL;
	IRRECO_RETURN
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Functions                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */

void irreco_style_browser_dlg_set_irreco_data(IrrecoStyleBrowserDlg *self,
					      IrrecoData *irreco_data)
{
	IRRECO_ENTER
	self->irreco_data = irreco_data;
	IRRECO_RETURN
}

void irreco_style_browser_dlg_set_button_widget(IrrecoStyleBrowserDlg *self)
{
	IRRECO_ENTER

	self->button_widget = irreco_button_browser_widget_new(self->irreco_data);
	gtk_box_pack_start_defaults(GTK_BOX(GTK_DIALOG(self)->vbox),
					GTK_WIDGET(self->button_widget));

	/* Signal handler */
	g_signal_connect(G_OBJECT(IRRECO_LISTBOX(
		self->button_widget->images)->tree_selection),
		"changed",
		G_CALLBACK(irreco_style_browser_dlg_theme_image_selection_changed),
		self);

	gtk_widget_show_all(GTK_WIDGET(self));

	IRRECO_RETURN
}

gboolean irreco_show_style_browser_dlg(IrrecoData *irreco_data,
				       GtkWindow *parent,
				       IrrecoThemeButton **style)
{
	gint response;
	IrrecoStyleBrowserDlg *self;
	IRRECO_ENTER

	self = IRRECO_STYLE_BROWSER_DLG(irreco_style_browser_dlg_new(
					irreco_data, parent));
	response = gtk_dialog_run(GTK_DIALOG(self));
	irreco_style_browser_dlg_loading_cancel(self);

	if (response == 1) *style = self->style;
	gtk_widget_destroy(GTK_WIDGET(self));

	if (response == 1) IRRECO_RETURN_BOOL(TRUE);
	IRRECO_RETURN_BOOL(FALSE);
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Events and Callbacks                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Events and Callbacks
 * @{
 */

void irreco_style_browser_dlg_theme_image_selection_changed(
						GtkTreeSelection * selection,
					        IrrecoStyleBrowserDlg * self)
{
	IrrecoThemeButton *button = self->button_widget->current_image;
	IRRECO_ENTER

	if (button != NULL) {
		self->style = button;
		gtk_dialog_response(GTK_DIALOG(self), 1);
	}
	IRRECO_RETURN
}

/** @} */
/** @} */

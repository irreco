/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoBackendErrDlg
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoBackendErrDlg.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_BACKEND_ERR_DLG_TYPEDEF__
#define __IRRECO_BACKEND_ERR_DLG_TYPEDEF__
#define IRRECO_TYPE_BACKEND_ERR_DLG irreco_backend_err_dlg_get_type()

#define IRRECO_BACKEND_ERR_DLG(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  IRRECO_TYPE_BACKEND_ERR_DLG, IrrecoBackendErrDlg))

#define IRRECO_BACKEND_ERR_DLG_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  IRRECO_TYPE_BACKEND_ERR_DLG, IrrecoBackendErrDlgClass))

#define IRRECO_IS_BACKEND_ERR_DLG(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  IRRECO_TYPE_BACKEND_ERR_DLG))

#define IRRECO_IS_BACKEND_ERR_DLG_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  IRRECO_TYPE_BACKEND_ERR_DLG))

#define IRRECO_BACKEND_ERR_DLG_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  IRRECO_TYPE_BACKEND_ERR_DLG, IrrecoBackendErrDlgClass))

typedef struct _IrrecoBackendErrDlg IrrecoBackendErrDlg;
typedef struct _IrrecoBackendErrDlgClass IrrecoBackendErrDlgClass;

#endif /* __IRRECO_BACKEND_ERR_DLG_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_BACKEND_ERR_DLG__
#define __IRRECO_BACKEND_ERR_DLG__
#include "irreco.h"
#include "irreco_dlg.h"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
struct _IrrecoBackendErrDlg {
	IrrecoDlg parent;

	GtkWidget* backend;
	GtkWidget* instace;
	GtkWidget* code;
	GtkWidget* message;
};

struct _IrrecoBackendErrDlgClass{
	IrrecoDlgClass parent_class;
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
GType		irreco_backend_err_dlg_get_type (void);
GtkWidget*	irreco_backend_err_dlg_new	(GtkWindow *parent,
						 const gchar *backend,
						 const gchar *instace,
						 const gchar *code,
						 const gchar *message);
void	 	irreco_show_backend_err_dlg	(GtkWindow *parent,
						 const gchar *backend,
						 const gchar *instace,
						 const gchar *code,
						 const gchar *message);


#endif /* __IRRECO_BACKEND_ERR_DLG__ */

/** @} */

/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoBackendManager
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoBackendManager.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_BACKEND_MANAGER_H_TYPEDEF__
#define __IRRECO_BACKEND_MANAGER_H_TYPEDEF__
typedef struct _IrrecoBackendManager IrrecoBackendManager;
#endif /* __IRRECO_BACKEND_MANAGER_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_BACKEND_MANAGER_H__
#define __IRRECO_BACKEND_MANAGER_H__
#include "irreco.h"
#include <irreco_string_table.h>
#include "irreco_backend_instance.h"
#include "irreco_backend_lib.h"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
struct _IrrecoBackendManager {
	IrrecoData *irreco_data;

	/* Contains all layouts. */
	/* - Key is libarary filename. */
	IrrecoStringTable *lib_table;

	/* Contains all layouts. */
	/* - Key is instance name. */
	IrrecoStringTable *instance_table;

	/* Maps config files to instances. */
	/* - Key is configuration filename of the instance. */
	IrrecoStringTable *config_map;
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#define IRRECO_BACKEND_MANAGER_FOREACH_LIB(_manager, _lib_var_name) 	       \
	IRRECO_STRING_TABLE_FOREACH_DATA(				       \
		(_manager)->lib_table, IrrecoBackendLib *, (_lib_var_name))

#define IRRECO_BACKEND_MANAGER_FOREACH_INSTANCE(_manager, _inst_var_name)      \
	IRRECO_STRING_TABLE_FOREACH_DATA(				       \
		(_manager)->instance_table, IrrecoBackendInstance *,	       \
		(_inst_var_name))

#define IRRECO_BACKEND_MANAGER_FOREACH_END IRRECO_STRING_TABLE_FOREACH_END



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
IrrecoBackendManager* irreco_backend_manager_create();
void irreco_backend_manager_destroy(IrrecoBackendManager * manager);
void irreco_backend_manager_get_devcmd_lists(IrrecoBackendManager * manager);
void irreco_backend_manager_read_from_confs(IrrecoBackendManager * manager);
void irreco_backend_manager_print(IrrecoBackendManager * manager);

IrrecoBackendLib *
irreco_backend_manager_load_lib(IrrecoBackendManager * manager,
			        const gchar * filepath);
IrrecoBackendLib *
irreco_backend_manager_load_with_name(IrrecoBackendManager * manager,
				      const gchar * filepath,
				      const gchar * filename);
gboolean
irreco_backend_manager_load_lib_dir(IrrecoBackendManager * manager,
				    gchar * directory);
gboolean irreco_backend_manager_find_lib(IrrecoBackendManager * manager,
					 const gchar * filename,
					 IrrecoBackendLib ** lib);

IrrecoBackendInstance *
irreco_backend_manager_create_instance(IrrecoBackendManager * manager,
				       IrrecoBackendLib * lib,
				       const gchar * name,
				       const gchar * config);
gboolean
irreco_backend_manager_destroy_instance(IrrecoBackendManager * manager,
				       IrrecoBackendInstance * instance);
gboolean irreco_backend_manager_find_instance(
				      IrrecoBackendManager * manager,
				      const gchar * name,
				      IrrecoBackendInstance ** instance);
gboolean
irreco_backend_manager_set_instance_name(IrrecoBackendManager * manager,
					 IrrecoBackendInstance * instance,
					 const gchar * name);
void
irreco_backend_manager_assign_instance_name(IrrecoBackendManager * manager,
					    IrrecoBackendInstance * instance);
gboolean
irreco_backend_manager_set_instance_config(IrrecoBackendManager * manager,
					   IrrecoBackendInstance * instance,
					   const gchar * filename);
void
irreco_backend_manager_assign_instance_config(IrrecoBackendManager * manager,
					      IrrecoBackendInstance * instance);

#endif /* __IRRECO_BACKEND_MANAGER_H__ */

/** @} */

/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * irreco - Ir Remote Control
 * Copyright (C) 2008 Pekka Gehör (pegu6@msn.com)
 *
 * irreco_theme_creator_backgrounds.c is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * irreco_theme_creator_backgrounds.c is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "irreco_theme_creator_backgrounds.h"
#include "irreco_theme_creator_dlg.h"
#include "irreco_listbox.h"

/**
 * @addtogroup IrrecoThemeCreatorBackgrounds
 * @ingroup Irreco
 *
 * User interface for IrrecoThemeCreatorDlgBackgrounds.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoThemeCreatorBackgrounds.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

#define IRRECO_BACKGROUNDS_PREVIEW_WIDHT	(IRRECO_SCREEN_WIDTH / 4.5)
#define IRRECO_BACKGROUNDS_PREVIEW_HEIGHT	(IRRECO_SCREEN_HEIGHT / 4.5)

enum
{
	DATA_COL,
	TEXT_COL,
	PIXBUF_COL,
	N_COLUMNS
};
/** Loader states. */
enum
{
	LOADER_STATE_INIT,
	LOADER_STATE_LOOP,
	LOADER_STATE_END,
	LOADER_STATE_CLEANUP
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes.                                                                */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
static void _image_selection_changed(GtkTreeSelection * selection,
				     IrrecoThemeCreatorBackgrounds *self);

gboolean
_get_selection(IrrecoThemeCreatorBackgrounds *self, gint *index, gchar **label,
	       gpointer *user_data);
gboolean
irreco_theme_creator_backgrounds_get_iter(IrrecoThemeCreatorBackgrounds *self,
					  gint index, GtkTreeIter * iter);
gboolean
irreco_theme_creator_backgrounds_set_selection(IrrecoThemeCreatorBackgrounds
						*self, gint index);


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

G_DEFINE_TYPE (IrrecoThemeCreatorBackgrounds, irreco_theme_creator_backgrounds,
	       IRRECO_TYPE_INTERNAL_WIDGET)


static void
irreco_theme_creator_backgrounds_finalize (GObject *object)
{
	/* TODO: Add deinitalization code here */
	IRRECO_ENTER
	G_OBJECT_CLASS(
		       irreco_theme_creator_backgrounds_parent_class)->finalize
		       (object);
	IRRECO_RETURN

}

static void
irreco_theme_creator_backgrounds_class_init(
				      IrrecoThemeCreatorBackgroundsClass *klass)
{
	GObjectClass *object_class;
	IRRECO_ENTER

	object_class = G_OBJECT_CLASS (klass);
	object_class->finalize = irreco_theme_creator_backgrounds_finalize;
	IRRECO_RETURN

}

static void
irreco_theme_creator_backgrounds_init (IrrecoThemeCreatorBackgrounds *self)
{
	/* TODO: Add initialization code here */

	/*Backgrounds widgets*/

	IRRECO_ENTER

	gtk_box_set_spacing(GTK_BOX(self), 1);

	/*BACKGROUNDS*/

	self->scroll_backgrounds = gtk_scrolled_window_new(NULL, NULL);
	self->store_backgrounds = gtk_list_store_new(N_COLUMNS, G_TYPE_POINTER,
						     G_TYPE_STRING,
	   					     GDK_TYPE_PIXBUF);
	self->view_backgrounds = gtk_tree_view_new();

	gtk_box_pack_start_defaults(GTK_BOX(self), self->scroll_backgrounds);

	/*BACKGROUNDS COLUMNS*/
	gtk_tree_view_set_model(GTK_TREE_VIEW(self->view_backgrounds),
				GTK_TREE_MODEL(self->store_backgrounds));
	g_object_unref(self->store_backgrounds);

	/* Create pixbuf column. */
	self->renderer_backgrounds = gtk_cell_renderer_pixbuf_new();
	self->column_backgrounds = gtk_tree_view_column_new_with_attributes(
				   "Background				  ",
       				   self->renderer_backgrounds,
	      			   "pixbuf", PIXBUF_COL, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(self->view_backgrounds),
				    self->column_backgrounds);

	/* Create text column. */
	self->renderer_backgrounds = gtk_cell_renderer_text_new();
	self->column_backgrounds = gtk_tree_view_column_new_with_attributes(
						"Name			",
      						self->renderer_backgrounds,
	    					"text", TEXT_COL, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(self->view_backgrounds),
				    self->column_backgrounds);
  	gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(self->view_backgrounds),
					  TRUE);


	/* Scroll_backgrounds*/

	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(
				       self->scroll_backgrounds),
	   			       GTK_POLICY_AUTOMATIC,
	      			       GTK_POLICY_AUTOMATIC);
	gtk_container_add(GTK_CONTAINER(self->scroll_backgrounds),
			  self->view_backgrounds);

	self->tree_selection = gtk_tree_view_get_selection(
					GTK_TREE_VIEW(self->view_backgrounds));
	gtk_tree_selection_set_mode(self->tree_selection,
				    GTK_SELECTION_SINGLE);

	/* Signal handlers. */
	g_signal_connect(G_OBJECT(self->tree_selection),
		"changed",
		G_CALLBACK(_image_selection_changed),
		self);
	IRRECO_RETURN
}

GtkWidget*
irreco_theme_creator_backgrounds_new(GtkWindow *parent, IrrecoData *irreco_data,
				     IrrecoTheme * irreco_theme)
{
	IrrecoThemeCreatorBackgrounds *self;
	IRRECO_ENTER

	self = g_object_new(IRRECO_TYPE_THEME_CREATOR_BACKGROUNDS,
			    "irreco-data", irreco_data, NULL);

	irreco_theme_creator_backgrounds_set_parent_window(self, parent);
	self->parent_window = GTK_WINDOW(parent);
	self->irreco_data = irreco_data;
	self->theme = irreco_theme;

	irreco_theme_creator_backgrounds_refresh(self);

	IRRECO_RETURN_PTR(GTK_WIDGET(self));
}

/** @} */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Private Functions
 * @{
 */

/*
 *Get information on the background picture
*/

static void _image_selection_changed(GtkTreeSelection * selection,
				     IrrecoThemeCreatorBackgrounds *self)
{
	GtkTreeIter       iter;
        GtkTreeModel     *model         = NULL;
	GtkTreePath      *path          = NULL;
	gint             *path_indices  = NULL;
	IRRECO_ENTER

	/* Get currect selection, if set. */
	selection = gtk_tree_view_get_selection(
		GTK_TREE_VIEW(self->view_backgrounds));
	if (gtk_tree_selection_get_selected(self->tree_selection,
					    &model, &iter)) {

		path = gtk_tree_model_get_path(model, &iter);
		path_indices = gtk_tree_path_get_indices(path);
		self->sel_index = path_indices[0];
		gtk_tree_path_free(path);

		gtk_tree_model_get(GTK_TREE_MODEL(model), &iter,
                   		   DATA_COL, &self->current_bg,
				   -1);
	} else {
		self->current_bg = NULL;
	}
	IRRECO_RETURN

}

gboolean
irreco_theme_creator_backgrounds_get_iter(IrrecoThemeCreatorBackgrounds *self,
					  gint index, GtkTreeIter * iter)
{
	gboolean rvalue;
	GtkTreeModel *model;
	GtkTreePath *path;
	IRRECO_ENTER

	model = gtk_tree_view_get_model(GTK_TREE_VIEW(
					self->view_backgrounds));
	path = gtk_tree_path_new_from_indices(index, -1);
	rvalue = gtk_tree_model_get_iter(model, iter, path);
	gtk_tree_path_free(path);
	IRRECO_RETURN_INT(rvalue);
}

gboolean
irreco_theme_creator_backgrounds_set_selection(IrrecoThemeCreatorBackgrounds
						*self, gint index)
{
	GtkTreeIter iter;
	IRRECO_ENTER

	if (irreco_theme_creator_backgrounds_get_iter(self, index, &iter)) {
		gtk_tree_selection_select_iter(self->tree_selection, &iter);
		IRRECO_RETURN_BOOL(TRUE);
	}
	IRRECO_RETURN_BOOL(FALSE);
}

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Public Functions                                                           */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Public Functions
 * @{
 */

/**
 * Set parent window for popup dialogs.
 */
void
irreco_theme_creator_backgrounds_set_parent_window(
					    IrrecoThemeCreatorBackgrounds *self,
	 				    GtkWindow *parent)
{
	IRRECO_ENTER
	self->parent_window = parent;
	IRRECO_RETURN
}

/**
 * Append row to the listbox with resized image.
 */

/** @} */
void
irreco_theme_creator_backgrounds_refresh(IrrecoThemeCreatorBackgrounds *self)
{

	GdkPixbuf		*pixbuf = NULL;
	GError			*error = NULL;
	IrrecoData		*irreco_data   = NULL;
	IrrecoThemeManager	*manager = NULL;
	GtkTreeIter		iter;
	IrrecoTheme		*theme;
	IRRECO_ENTER

	gtk_list_store_clear(self->store_backgrounds);
	irreco_data = self->irreco_data;
	manager = irreco_data->theme_manager;
	theme = self->theme;

	IRRECO_STRING_TABLE_FOREACH_DATA(theme->backgrounds,
					 IrrecoThemeBg *, background)

		irreco_theme_bg_print(background);
		pixbuf = gdk_pixbuf_new_from_file_at_scale(
					      background->image_path->str,
	   				      IRRECO_BACKGROUNDS_PREVIEW_WIDHT,
	      				      IRRECO_BACKGROUNDS_PREVIEW_HEIGHT,
		 			      GDK_INTERP_NEAREST, &error);

		if (irreco_gerror_check_print(&error)) {
				IRRECO_RETURN
		}
		gtk_list_store_append(self->store_backgrounds,
					      &iter);
		gtk_list_store_set(self->store_backgrounds,
				   &iter, DATA_COL,
	 			   background, PIXBUF_COL,
	  			   pixbuf, TEXT_COL,
	  			   background->image_name->str,
	  			   -1);

		gtk_tree_view_columns_autosize(GTK_TREE_VIEW(
					       self->view_backgrounds));

	IRRECO_STRING_TABLE_FOREACH_END

	if (pixbuf != NULL) g_object_unref(G_OBJECT(pixbuf));

	IRRECO_RETURN
}

void _backgrounds_row_activated_event(GtkTreeView *tree_view,
				      GtkTreePath *path,
	  			      GtkTreeViewColumn *column,
	    			      IrrecoThemeCreatorBackgrounds *self)
{
	IRRECO_ENTER
	IRRECO_RETURN
}

IrrecoThemeBg *
irreco_theme_creator_backgrounds_get_selected_bg(
					IrrecoThemeCreatorBackgrounds *self)
{

	IRRECO_ENTER
	IRRECO_RETURN_PTR(self->current_bg);
}

gboolean
irreco_theme_creator_backgrounds_remove_selected(IrrecoThemeCreatorBackgrounds *self)
{
	gint index;
	GtkTreeIter iter;
	IRRECO_ENTER

	_image_selection_changed(self->tree_selection, self);
	if ((index = self->sel_index) == -1
	    || irreco_theme_creator_backgrounds_get_iter(self, index, &iter) == FALSE) {
		IRRECO_RETURN_BOOL(FALSE);
	}
	if(!irreco_theme_creator_backgrounds_set_selection(self, index - 1)) {
		irreco_theme_creator_backgrounds_set_selection(self, index + 1);
	}

	gtk_list_store_remove(self->store_backgrounds, &iter);
	IRRECO_RETURN_BOOL(TRUE);
}

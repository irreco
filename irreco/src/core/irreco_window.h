/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoWindow
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoWindow.
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_WINDOW_H_TYPEDEF__
#define __IRRECO_WINDOW_H_TYPEDEF__

#define IRRECO_TYPE_WINDOW irreco_window_get_type()

#define IRRECO_WINDOW(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  IRRECO_TYPE_WINDOW, IrrecoWindow))

#define IRRECO_WINDOW_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  IRRECO_TYPE_WINDOW, IrrecoWindowClass))

#define IRRECO_IS_WINDOW(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  IRRECO_TYPE_WINDOW))

#define IRRECO_IS_WINDOW_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  IRRECO_TYPE_WINDOW))

#define IRRECO_WINDOW_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  IRRECO_TYPE_WINDOW, IrrecoWindowClass))

typedef struct _IrrecoWindow IrrecoWindow;
typedef struct _IrrecoWindowClass IrrecoWindowClass;

#endif /* __IRRECO_WINDOW_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_WINDOW_H__
#define __IRRECO_WINDOW_H__
#include "irreco.h"
#include <hildon/hildon-pannable-area.h>


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

struct _IrrecoWindow {
	HildonWindow parent;

	GtkWidget *layout;
	GtkWidget *scrolled_window;
	GtkWidget *event_box;
};

struct _IrrecoWindowClass{
	HildonWindowClass parent_class;
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

#define irreco_window_get_gtk_window(irreco_window) \
	(GTK_WINDOW((irreco_window)))

#define irreco_window_get_gdk_window(irreco_window) \
	((irreco_window)->window)

#define irreco_window_create() \
	IRRECO_WINDOW(irreco_window_new())

#define irreco_window_destroy(_window) \
	gtk_widget_destroy(GTK_WIDGET(_window))



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
GType 		irreco_window_get_type	();
GtkWidget* 	irreco_window_new	();


gboolean irreco_window_set_background_image(IrrecoWindow *self,
					    const GdkColor * color,
					    const gchar * image1,
					    const gchar * image2);
void irreco_window_set_background_buf(IrrecoWindow *self,
				      const GdkColor * color,
				      GdkPixbuf * pixbuf1,
				      GdkPixbuf * pixbuf2);
void irreco_window_get_scroll_offset(IrrecoWindow *self,
				     gdouble *x, gdouble *y);
void irreco_window_scroll_visible(IrrecoWindow *self, gdouble x, gdouble y);
gboolean irreco_window_is_fullscreen(IrrecoWindow *self);
void irreco_window_toggle_fullscreen(IrrecoWindow *self);
void irreco_window_scrollbar_hide(IrrecoWindow *self);
void irreco_window_scrollbar_show(IrrecoWindow *self);


#endif /* __IRRECO_WINDOW_H__ */

/** @} */


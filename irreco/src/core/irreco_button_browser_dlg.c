/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *		 2008  Joni Kokko (t5kojo01@students.oamk.fi)
 *		 2008  Harri Vattulainen (t5vaha01@students.oamk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irreco_button_browser_dlg.h"
#include <hildon/hildon-banner.h>



/**
 * @addtogroup IrrecoButtonBrowserDlg
 * @ingroup Irreco
 *
 * Allow user to select one button from one theme.
 *
 * @{
 */

/**
 * @file
 * Source file of @ref IrrecoButtonBrowserDlg.
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

#define IRRECO_BUTTON_BROWSER_PREVIEW_WIDHT	(IRRECO_SCREEN_WIDTH / 6)
#define IRRECO_BUTTON_BROWSER_PREVIEW_HEIGHT	(IRRECO_SCREEN_HEIGHT / 6)



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

static void
_irreco_button_browser_dlg_set_banner(IrrecoButtonBrowserDlg *self,
						const gchar *text,
						gdouble fraction);
static void
_irreco_button_browser_dlg_hide_banner(IrrecoButtonBrowserDlg *self);
static gboolean _irreco_button_browser_dlg_loader_images(
						IrrecoButtonBrowserDlg *self);
static void irreco_button_browser_dlg_image_selection_changed(
						GtkTreeSelection * selection,
					        IrrecoButtonBrowserDlg *self);
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Construction & Destruction
 * @{
 */

G_DEFINE_TYPE(IrrecoButtonBrowserDlg, irreco_button_browser_dlg,
	      IRRECO_TYPE_INTERNAL_DLG)

static void irreco_button_browser_dlg_constructed(GObject *object)
{
	IrrecoButtonBrowserDlg *self;

	IRRECO_ENTER

	G_OBJECT_CLASS(irreco_button_browser_dlg_parent_class)->constructed(
									object);
	self = IRRECO_BUTTON_BROWSER_DLG(object);

	self->current_image = NULL;

	/* Construct dialog. */
	gtk_window_set_title(GTK_WINDOW(self), _("Select preview button"));
	gtk_window_set_modal(GTK_WINDOW(self), TRUE);
	gtk_window_set_destroy_with_parent(GTK_WINDOW(self), TRUE);
	gtk_dialog_set_has_separator(GTK_DIALOG(self), FALSE);

	/* Create list for images */
	self->images = IRRECO_LISTBOX_IMAGE(
		irreco_listbox_image_new_with_autosize(200, 450, 180, 180));
	irreco_listbox_set_select_new_rows(IRRECO_LISTBOX(self->images), FALSE);
	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(self)->vbox),
			  GTK_WIDGET(self->images));

	g_signal_connect(G_OBJECT(IRRECO_LISTBOX(self->images)->tree_selection),
		"changed",
		G_CALLBACK(irreco_button_browser_dlg_image_selection_changed),
		self);

	gtk_widget_show_all(GTK_WIDGET(self));

	IRRECO_RETURN
}

static void irreco_button_browser_dlg_init(IrrecoButtonBrowserDlg *self)
{
	IRRECO_ENTER
	IRRECO_RETURN
}

static void irreco_button_browser_dlg_finalize(GObject *object)
{
	IrrecoButtonBrowserDlg *self;

	IRRECO_ENTER

	self = IRRECO_BUTTON_BROWSER_DLG(object);
	G_OBJECT_CLASS(irreco_button_browser_dlg_parent_class)->finalize(object);

	IRRECO_RETURN
}

static void
irreco_button_browser_dlg_class_init(IrrecoButtonBrowserDlgClass *klass)
{
	GObjectClass* object_class = G_OBJECT_CLASS (klass);
	object_class->finalize = irreco_button_browser_dlg_finalize;
	object_class->constructed = irreco_button_browser_dlg_constructed;
}

GtkWidget *irreco_button_browser_dlg_new(IrrecoData *irreco_data,
				     GtkWindow *parent_window)
{
	IrrecoButtonBrowserDlg *self;

	IRRECO_ENTER

	self = g_object_new(IRRECO_TYPE_BUTTON_BROWSER_DLG,
			    "irreco-data", irreco_data,
			    NULL);
	irreco_dlg_set_parent(IRRECO_DLG(self), parent_window);

	IRRECO_RETURN_PTR(self);
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/**
 * @name Private Functions
 * @{
 */

/**
 * Show hildon progressbar banner.
 *
 * This function will create a new banner if one has not been created yet,
 * if banner already exists, it's properties will be changed.
 *
 * @param text		Text to show.
 * @param fraction	Value of progress.
 */
static void
_irreco_button_browser_dlg_set_banner(IrrecoButtonBrowserDlg *self,
						const gchar *text,
						gdouble fraction)
{
	IRRECO_ENTER

	if (self->banner == NULL) {
		self->banner = hildon_banner_show_progress(
			GTK_WIDGET(self), NULL, "");
	}

	hildon_banner_set_text(HILDON_BANNER(self->banner), text);
	hildon_banner_set_fraction(HILDON_BANNER(self->banner), fraction);

	IRRECO_RETURN
}

/**
 * Destroy banner, if it exists.
 */
static void
_irreco_button_browser_dlg_hide_banner(IrrecoButtonBrowserDlg *self)
{
	IRRECO_ENTER

	if (self->banner) {
		gtk_widget_destroy(self->banner);
		self->banner = NULL;
	}

	IRRECO_RETURN
}

/**
 * Background-image loader.
 *
 * This loader will create a list of background-images from the
 * given theme and update the TreeView accordingly.
 */
static gboolean _irreco_button_browser_dlg_loader_images(
						IrrecoButtonBrowserDlg *self)
{
	gint theme_button_count;
	gfloat banner;
	gboolean rvalue;

	IRRECO_ENTER

	theme_button_count = irreco_string_table_lenght(self->theme->buttons);

	if (theme_button_count > 0) {
		if(self->loader_index == 0) {
			_irreco_button_browser_dlg_set_banner(self,
							      _("Loading ..."),
							      0);
		}
		rvalue = TRUE;
	} else {
		rvalue = FALSE;
	}

	if(self->loader_index < theme_button_count){
		IrrecoThemeButton	*button_image;
		const gchar		*image_name;
		/* get button */
		irreco_string_table_index(self->theme->buttons,
					self->loader_index,
					&image_name,
					(gpointer *) &button_image);
		/* add button to list */
		irreco_listbox_image_append(self->images,
				button_image->name->str,
				button_image,
				button_image->image_up->str);
		/* get and set banner fraction */
		banner = (gfloat) self->loader_index /
						(gfloat) theme_button_count;
		_irreco_button_browser_dlg_set_banner(self,
							_("Loading ..."),
							banner);
		/* get next button, next time */
		self->loader_index++;
	}

	if(self->loader_index == theme_button_count) {
		_irreco_button_browser_dlg_hide_banner(self);
		rvalue = FALSE;
	}

	IRRECO_RETURN_BOOL(rvalue);
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Public Functions                                                           */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * Show dialog, and ask user for input.
 */

IrrecoThemeButton *irreco_button_browser_dlg_run(GtkWindow *parent_window,
				     IrrecoData *irreco_data,
				     IrrecoTheme * irreco_theme)
{
	IrrecoButtonBrowserDlg	*self;
	gint			response;
	gboolean		loop 	= TRUE;
	IrrecoThemeButton	*rvalue = NULL;

	IRRECO_ENTER

	self = (IrrecoButtonBrowserDlg *)irreco_button_browser_dlg_new(
								irreco_data,
								parent_window);

	self->theme = irreco_theme;

	/* init and start loading buttons */
	self->loader_index = 0;
	irreco_listbox_clear(IRRECO_LISTBOX(self->images));
	g_idle_add((GSourceFunc)_irreco_button_browser_dlg_loader_images, self);

	do {
		response = gtk_dialog_run(GTK_DIALOG(self));

		switch (response) {
		case GTK_RESPONSE_DELETE_EVENT:
			IRRECO_DEBUG("GTK_RESPONSE_DELETE_EVENT\n");
			rvalue = NULL;
			loop = FALSE;
			break;

		case GTK_RESPONSE_ACCEPT:
			IRRECO_DEBUG("GTK_RESPONSE_ACCEPT\n");
			/*rvalue = g_strdup(self->current_image->name->str);*/
			rvalue = self->current_image;
			loop = FALSE;
			break;

		default:
			IRRECO_DEBUG("Something went horribly wrong\n");
			break;
		}
	} while (loop);

	gtk_widget_destroy(GTK_WIDGET(self));

	IRRECO_RETURN_PTR(rvalue);
}

/** @} */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Events and Callbacks                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * @name Events and Callbacks
 * @{
 */

/**
 * Respond to image selection change.
 *
 * This function will store selected button to self->current_image
 */
static void irreco_button_browser_dlg_image_selection_changed(
						GtkTreeSelection *selection,
						IrrecoButtonBrowserDlg *self)
{
	gint sel_index = -1;
	gchar *sel_label = NULL;
	IrrecoThemeButton *button = NULL;

	IRRECO_ENTER

	if (irreco_listbox_get_selection(IRRECO_LISTBOX(self->images),
				&sel_index, &sel_label, (gpointer *)&button)) {
		self->current_image = button;
	} else {
		self->current_image = NULL;
	}

	g_free(sel_label);
	gtk_dialog_response(GTK_DIALOG(self), GTK_RESPONSE_ACCEPT);

	IRRECO_RETURN
}

/** @} */



/** @} */

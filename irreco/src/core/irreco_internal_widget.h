/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007,2008  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/**
 * @addtogroup IrrecoInternalWidget
 * @{
 */

/**
 * @file
 * Header file of @ref IrrecoInternalWidget.
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRRECO_INTERNAL_WIDGET_H_TYPEDEF__
#define __IRRECO_INTERNAL_WIDGET_H_TYPEDEF__

#define IRRECO_TYPE_INTERNAL_WIDGET             (irreco_internal_widget_get_type ())
#define IRRECO_INTERNAL_WIDGET(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), IRRECO_TYPE_INTERNAL_WIDGET, IrrecoInternalWidget))
#define IRRECO_INTERNAL_WIDGET_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), IRRECO_TYPE_INTERNAL_WIDGET, IrrecoInternalWidgetClass))
#define IRRECO_IS_INTERNAL_WIDGET(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), IRRECO_TYPE_INTERNAL_WIDGET))
#define IRRECO_IS_INTERNAL_WIDGET_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), IRRECO_TYPE_INTERNAL_WIDGET))
#define IRRECO_INTERNAL_WIDGET_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), IRRECO_TYPE_INTERNAL_WIDGET, IrrecoInternalWidgetClass))

typedef struct _IrrecoInternalWidgetClass IrrecoInternalWidgetClass;
typedef struct _IrrecoInternalWidget IrrecoInternalWidget;

#endif /* __IRRECO_INTERNAL_WIDGET_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __IRRECO_INTERNAL_WIDGET_H__
#define __IRRECO_INTERNAL_WIDGET_H__
#include "irreco.h"
#include "irreco_data.h"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

struct _IrrecoInternalWidgetClass
{
	GtkHBoxClass parent_class;
};

struct _IrrecoInternalWidget
{
	GtkHBox parent_instance;
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

GType irreco_internal_widget_get_type(void) G_GNUC_CONST;
IrrecoData *irreco_internal_widget_get_irreco_data(IrrecoInternalWidget *self);


#endif /* __IRRECO_INTERNAL_WIDGET_H__ */

/** @} */


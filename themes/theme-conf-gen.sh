#!/bin/bash

SELF="$0"
SELF_ABSOLUTE=`readlink -f "$0"`

#
# Script for generating irreco config files for buttons themes.
#

theme_gen_main()
{
	COMMAND="$1"
	ARGS=("$@")
	
	case "$COMMAND" in
		--generate|generate|gen)	theme_gen_erate "${ARGS[@]:1}";;
		*)				theme_gen_help;;
	esac
}

theme_gen_help()
{
	echo "Usage: $SELF COMMAND"
	echo ""
	echo "Commands:"
	echo "    --generate | generate | gen SOURCE DESTINATION"
	echo "        Generate configuration files and directory structures"
	echo "        for so that Irreco will correctly read the theme."
}

theme_gen_erate()
{
	SOURCE="$1"
	
	THEME_NAME="$( cat "$SOURCE"/name )"
	THEME_DIR_NAME="$( theme_gen_dir_name "$THEME_NAME" )"
	
	DESTINATION="$2"
	IRRECO_DIR="$DESTINATION/lib/irreco"
	THEME_DIR="$IRRECO_DIR/themes/$THEME_DIR_NAME"
	BUTTON_DIR="$THEME_DIR/buttons"
	BG_DIR="$THEME_DIR/bg"

	[[ "$SOURCE" == "" ]] && theme_gen_error \
		"Source directory not given."
		
	[[ "$DESTINATION" == "" ]] && theme_gen_error \
		"Destination directory not given."
	
	[ ! -d "$SOURCE" ] && theme_gen_error \
		"Source directory does not exist, is not a directory."
	
	#[ -e "$DESTINATION" ] && theme_gen_error \
	#	"Destination already exists. Will not overwrite."
		
	[ ! -f "$SOURCE"/name ] && theme_gen_error \
		"Theme name file does not exist."
		
	
	THEME_PREFIX=`basename "$( realpath -f $SOURCE )"`
	BUTTONDIR="$SOURCE/buttons"
	BGDIR="$SOURCE/bg"
	
	echo "Source:       \"$SOURCE\""
	echo "Buttondir:    \"$BUTTONDIR\""
	echo "Bgdir:        \"$BGDIR\""
	echo "Destination:  \"$DESTINATION\""
	echo "Theme name:   \"$THEME_NAME\""
	echo "Theme dir:    \"$THEME_DIR\""
	echo "Theme prefix: \"$THEME_PREFIX\""
			
	if [ ! -d "$DESTINATION" ]; then
		mkdir "$DESTINATION"
		check_exit_code "$?"
	fi
	
	mkdir -p "$BUTTON_DIR"
	check_exit_code "$?"
	mkdir -p "$BG_DIR"
	check_exit_code "$?"
	
	echo
	echo "Creating theme config."
	theme_gen_theme_config > "$THEME_DIR/theme.conf"
	
	echo
	echo "Installing backgrounds:"
	if [ -d "$BGDIR" ]; then
		theme_gen_bg_list | theme_gen_bg_parse
	fi
	
	echo
	echo "Installing buttonstyles:"
	theme_gen_button_list | theme_gen_button_parse
	
	#echo 
	#echo "Destination file list:"
	#cd "$DESTINATION" && find | sort
}

#
# Generates a lower case name, and replaces spaces with underscores.
#
theme_gen_dir_name()
{
	echo "$1" | tr [:upper:] [:lower:] | tr [:blank:] _ 
}

theme_gen_theme_config()
{
	echo "[theme]"
	echo "name=$THEME_NAME"
	echo "source=deb"
}

theme_gen_bg_list()
{
	cd "$BGDIR"
	ls -1 | grep '.png$' | sed -r 's/.png//'
}

theme_gen_bg_parse()
{
	while read LINE; do
		echo "Generating: \"$LINE\""

		DEST_BG_DIR="$( theme_gen_dir_name "$LINE" )"
		DEST_IMAGE="$DEST_BG_DIR"/image.png
		DEST_CONFIG="$DEST_BG_DIR"/bg.conf
		BG_NAME="$LINE"

		# Copy image.
		mkdir -p "$BG_DIR"/"$DEST_BG_DIR"
		cp "$BGDIR"/"$BG_NAME".png \
		   "$BG_DIR"/"$DEST_IMAGE"
		check_exit_code "$?"

		# Create configuration.
		theme_gen_bg_config > "$BG_DIR"/"$DEST_CONFIG"
		check_exit_code "$?"
	done
}

theme_gen_bg_config()
{
	echo "[theme-bg]"

	if [[ "$BG_NAME" != "" ]]; then
		echo "name=$BG_NAME"
	fi

	echo "image=image.png"
}

theme_gen_button_list()
{
	cd "$BUTTONDIR"
	ls -1 | grep '.png$' | sed -r 's/_(pressed|unpressed).png//' | uniq
}

theme_gen_button_parse()
{
	while read LINE; do
		echo -n "Generating: \"$LINE\""
		
		DEST_PRESSED="$LINE"/pressed.png
		DEST_UNPRESSED="$LINE"/unpressed.png
		DEST_CONFIG="$LINE"/button.conf
		CONFIG_APPEND="$BUTTONDIR"/"$LINE"_config_append
		
		case "$LINE" in 
			down)		BUTTON_NAME="Down"
					ALLOW_TEXT="false";;
			left)		BUTTON_NAME="Left"
					ALLOW_TEXT="false";;
			next)		BUTTON_NAME="Next"
					ALLOW_TEXT="false";;
			pause)		BUTTON_NAME="Pause"
					ALLOW_TEXT="false";;
			play)		BUTTON_NAME="Play"
					ALLOW_TEXT="false";;
			playpause)	BUTTON_NAME="Play / Pause"
					ALLOW_TEXT="false";;
			power)		BUTTON_NAME="Power"
					ALLOW_TEXT="false";;
			prev)		BUTTON_NAME="Prev"
					ALLOW_TEXT="false";;
			right)		BUTTON_NAME="Right"
					ALLOW_TEXT="false";;
			up)		BUTTON_NAME="Up"
					ALLOW_TEXT="false";;
			menu)		BUTTON_NAME="Menu"
					ALLOW_TEXT="false";;
			ok)		BUTTON_NAME="OK"
					ALLOW_TEXT="false";;
			blank)		BUTTON_NAME="Blank"
					ALLOW_TEXT="true";;
			num_0)		BUTTON_NAME="Number 0"
					ALLOW_TEXT="false";;
			num_1)		BUTTON_NAME="Number 1"
					ALLOW_TEXT="false";;
			num_2)		BUTTON_NAME="Number 2"
					ALLOW_TEXT="false";;
			num_3)		BUTTON_NAME="Number 3"
					ALLOW_TEXT="false";;
			num_4)		BUTTON_NAME="Number 4"
					ALLOW_TEXT="false";;
			num_5)		BUTTON_NAME="Number 5"
					ALLOW_TEXT="false";;
			num_6)		BUTTON_NAME="Number 6"
					ALLOW_TEXT="false";;
			num_7)		BUTTON_NAME="Number 7"
					ALLOW_TEXT="false";;
			num_8)		BUTTON_NAME="Number 8"
					ALLOW_TEXT="false";;
			num_9) 		BUTTON_NAME="Number 9"
					ALLOW_TEXT="false";;
			*)	grep '^name=' "$CONFIG_APPEND" > /dev/null
				if [[ "$?" != "0" ]]; then
					echo "Error: Custom button type without name."
					echo "Unknown file: \"$LINE\""; 
					exit 1
				fi
				;;
		esac;
	
		# Copy unpressed image.
		mkdir -p "$BUTTON_DIR"/"$LINE"
		cp "$BUTTONDIR"/"$LINE"_unpressed.png \
		   "$BUTTON_DIR"/"$DEST_UNPRESSED"
		check_exit_code "$?"
		
		# Copy pressed image.
		if [ -e "$BUTTONDIR"/"$LINE"_pressed.png ]; then
			cp "$BUTTONDIR"/"$LINE"_pressed.png \
			   "$BUTTON_DIR"/"$DEST_PRESSED"
			check_exit_code "$?"
			PRESSED_EXITST='yes'
		else
			PRESSED_EXITST='no'
		fi
		
		theme_gen_button_config > "$BUTTON_DIR"/"$DEST_CONFIG"
		check_exit_code "$?"
		
		#echo "$CONFIG_APPEND"
		if [ -f "$CONFIG_APPEND" ]; then
			echo -n " ... appeding config"
			cat "$CONFIG_APPEND" >> "$BUTTON_DIR"/"$DEST_CONFIG"
			check_exit_code "$?"
		fi
		
		echo
	done	
}

theme_gen_button_config()
{
	echo "[theme-button]"
	
	if [[ "$BUTTON_NAME" != "" ]]; then
		echo "name=$BUTTON_NAME"
	fi
	
	if [[ "$ALLOW_TEXT" != "" ]]; then
		echo "allow-text=$ALLOW_TEXT"
	fi
	
	echo "up=unpressed.png"
	
	if [[ "$PRESSED_EXITST" == "yes" ]]; then
		echo "down=pressed.png"
	fi
}

#
# Stop script if exit code is not 0
#
# Usage: check_exit_code "$?"
#
check_exit_code()
{
	if [[ "$1" != "0" ]]; then
		echo "Exit code: $1"
		exit "$1"
	fi
}

theme_gen_error()
{
	echo "Error:" "$@"
	exit 1
}

theme_gen_main "$@"



















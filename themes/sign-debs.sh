#!/bin/bash
find -maxdepth 1 -name '*.changes' -exec debsign '{}' \;
rm irreco-theme.sha1
cfv -C -t sha1 -f irreco-theme.sha1 -- irreco-theme-*

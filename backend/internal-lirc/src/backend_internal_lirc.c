/*
 internal_lirc_backend
 Copyright (C) 2006 Kimmo Leppälä (kimmo.leppala@gmail.com)
 Modified 2009 by Sami Mäki (kasmra@xob.kapsi.fi)

 This is based on the original ir-send program by Christoph Bartelmus
 (lirc@bartelmus.de)

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

#include "backend_internal_lirc.h"

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <time.h>

#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <getopt.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <errno.h>
#include <signal.h>
#include <limits.h>
#include <netinet/in.h>

#define PACKET_SIZE 256
/* three seconds */
#define TIMEOUT 3

int timeout = 0;
char *progname = "internal_lirc backend";

void sigalrm(int sig)
{
	timeout = 1;
}

#include <sys/time.h>
/*
 * From gcc manual
 */
int input_timeout(int filedes, unsigned int seconds)
{
	fd_set set;
	struct timeval timeout;

	/* Initialize the file descriptor set. */
	FD_ZERO (&set);
	FD_SET (filedes, &set);

	/* Initialize the timeout data structure. */
	timeout.tv_sec = seconds;
	timeout.tv_usec = 0;

	/* select returns 0 if timeout, 1 if input available, -1 if error. */
	return select(FD_SETSIZE, &set, NULL, NULL, &timeout);
}

IrrecoBackendStatus read_string(
			IrrecoInternalLircBackend *internal_lirc_backend,
			const char ** string)
{
	static char buffer[PACKET_SIZE + 1] = "";
	char *end;
	static int ptr = 0;
	ssize_t ret;
	IRRECO_ENTER

	if (ptr > 0) {
		IRRECO_LINE
		memmove(buffer, buffer + ptr, strlen(buffer + ptr) + 1);
		ptr = strlen(buffer);
		end = strchr(buffer, '\n');
	} else {
		IRRECO_LINE
		end = NULL;
	}

	while (end == NULL) {
		IRRECO_LINE
		if (PACKET_SIZE <= ptr) {
			ptr = 0;
			*string = NULL;
			IRRECO_LINE
			IRRECO_RETURN_INT(internal_lirc_backend_error(
				internal_lirc_backend,
				INTERNAL_LIRC_BACKEND_ERR_BAD_PACKET));
		}

		if (input_timeout(internal_lirc_backend->socket, 3) < 1) {
			ptr = 0;
			*string = NULL;
			IRRECO_LINE
			IRRECO_RETURN_INT(internal_lirc_backend_error(
				internal_lirc_backend,
				INTERNAL_LIRC_BACKEND_ERR_TIMEOUT));;
		} else {
			IRRECO_LINE
			ret = read(internal_lirc_backend->socket, buffer + ptr,
				   PACKET_SIZE - ptr);
		}
		IRRECO_LINE
		buffer[ptr + ret] = 0;
		ptr = strlen(buffer);
		end = strchr(buffer, '\n');
	}

	end[0] = 0;
	ptr = strlen(buffer) + 1;
	IRRECO_PRINTF("Input buffer \"%s\".\n", buffer);
	*string = buffer;
	IRRECO_RETURN_INT(IRRECO_BACKEND_OK);
}

enum packet_state {
	P_BEGIN,
	P_MESSAGE,
	P_STATUS,
	P_DATA,
	P_N,
	P_DATA_N,
	P_END
};

IrrecoBackendStatus send_packet(IrrecoInternalLircBackend * internal_lirc_backend,
				const char *packet,
				InternalLircBackendResponseHandler handler)
{
	int done, todo;
	const char *string, *data;
	char *endptr;
	enum packet_state state;
	int status, n;
	unsigned long data_n = 0;
	IrrecoBackendStatus read_string_status;
	IRRECO_ENTER

	IRRECO_PRINTF("Sending packet: \"%s\".\n", packet);

	todo = strlen(packet);
	data = packet;
	IRRECO_PRINTF("todo: %d   data: %s\n", todo, data);
	while (todo > 0) {
		done = write(internal_lirc_backend->socket, (void *) data, todo);
		if (done < 0) {
			IRRECO_LINE
			IRRECO_RETURN_INT(internal_lirc_backend_error(
						internal_lirc_backend,
				INTERNAL_LIRC_BACKEND_ERR_COULD_NOT_SEND_PACKET,
				packet));
		}
		data += done;
		todo -= done;
		IRRECO_PRINTF("inwhile todo: %d  data: %s\n", todo, data);
	}

	/* get response */
	status = 0;
	state = P_BEGIN;
	n = 0;
	while (1) {
		IRRECO_LINE
		read_string_status = read_string(internal_lirc_backend,
							&string);
		if (read_string_status != IRRECO_BACKEND_OK) {
			IRRECO_LINE
			IRRECO_RETURN_INT(read_string_status);
		}
		IRRECO_LINE
		switch (state) {
		case P_BEGIN:
			IRRECO_LINE
			if (strcasecmp(string, "BEGIN") != 0) {
				continue;
			}
			state = P_MESSAGE;
			break;

		case P_MESSAGE:
			IRRECO_LINE
			if (strncasecmp(string, packet, strlen(string)) != 0 ||
			    strlen(string) + 1 != strlen(packet)) {
				state = P_BEGIN;
				continue;
			}
			state = P_STATUS;
			break;

		case P_STATUS:
			IRRECO_LINE
			if (strcasecmp(string, "SUCCESS") == 0) {
				status = 0;
			} else if (strcasecmp(string, "END") == 0) {
				status = 0;
				goto check_status_code;
			} else if (strcasecmp(string, "ERROR") == 0) {
				status = INTERNAL_LIRC_BACKEND_ERR_COMMAND_FAILED;
			} else {
				status = INTERNAL_LIRC_BACKEND_ERR_BAD_PACKET;
				goto check_status_code;
			}
			state = P_DATA;
			break;

		case P_DATA:
			IRRECO_LINE
			if (strcasecmp(string, "END") == 0) {
				goto check_status_code;
			} else if (strcasecmp(string, "DATA") == 0) {
				state = P_N;
				break;
			}
			status = INTERNAL_LIRC_BACKEND_ERR_BAD_PACKET;
			goto check_status_code;

		case P_N:
			IRRECO_LINE
			errno = 0;
			data_n = strtoul(string, &endptr, 0);
			if (!*string || *endptr) {
				status = INTERNAL_LIRC_BACKEND_ERR_BAD_PACKET;
				goto check_status_code;
			}
			if (data_n == 0) {
				state = P_END;
			} else {
				state = P_DATA_N;
			}
			break;

		case P_DATA_N:
			IRRECO_LINE
			IRRECO_DEBUG("Response data: \"%s\".\n", string);
			if (handler != NULL) handler(internal_lirc_backend, string);
			n++;
			if (n == data_n) state = P_END;
			break;

		case P_END:
			IRRECO_LINE
			if (strcasecmp(string, "END") == 0) {
				goto check_status_code;
			}
			status = INTERNAL_LIRC_BACKEND_ERR_BAD_PACKET;
			goto check_status_code;
			break;
		}
	}

	check_status_code:
	switch (status) {
		IRRECO_LINE
	case INTERNAL_LIRC_BACKEND_ERR_BAD_PACKET:
		IRRECO_LINE
		IRRECO_RETURN_INT(internal_lirc_backend_error(
			internal_lirc_backend,
			INTERNAL_LIRC_BACKEND_ERR_BAD_PACKET));

	case INTERNAL_LIRC_BACKEND_ERR_COMMAND_FAILED:
		IRRECO_LINE
		IRRECO_RETURN_INT(internal_lirc_backend_error(
			internal_lirc_backend,
			INTERNAL_LIRC_BACKEND_ERR_COMMAND_FAILED, packet));

	case IRRECO_BACKEND_OK:
		IRRECO_LINE
		IRRECO_RETURN_INT(IRRECO_BACKEND_OK);

	default:
		IRRECO_LINE
		IRRECO_PRINTF("Weird return code \"%d\"\n", status);
		IRRECO_ERROR("Weird return code \"%i\"\n", status);
		IRRECO_RETURN_INT(status);
	}
}

static IrrecoBackendStatus internal_lirc_backend_create_device(
			gpointer instance_context,
			GtkWindow *parent)
{
	IRRECO_ENTER
/* 	irreco_info_dlg(GTK_WINDOW(parent),
			"Feature not implemented."); */
	IRRECO_RETURN_INT(IRRECO_BACKEND_OK);
}

static gboolean internal_lirc_backend_is_device_editable(
			gpointer instance_context,
			const gchar *device_name,
			gpointer device_contex)
{
	gboolean retval = TRUE;
	IRRECO_ENTER
	IRRECO_RETURN_BOOL(retval);
}

static IrrecoBackendStatus internal_lirc_backend_edit_device(
			gpointer instance_context,
			const gchar *device_name,
			gpointer device_contex,
			GtkWindow *parent)
{
	IRRECO_ENTER
	irreco_info_dlg(GTK_WINDOW(parent),
			"Feature not implemented.");
	IRRECO_RETURN_INT(IRRECO_BACKEND_OK);
}

static IrrecoBackendStatus internal_lirc_delete_device(
			gpointer instance_context,
			const char *device_name,
			gpointer device_contex,
			GtkWindow *parent)
{
	IrrecoBackendStatus status = IRRECO_BACKEND_OK;
	IrrecoInternalLircBackend *internal_lirc_backend = instance_context;
	gchar *delete_cmd = NULL;
	IRRECO_ENTER

	delete_cmd = g_strdup_printf(
		"%s%s\"$//g' %s > /tmp/lircd.conf.temporary\
		&& cp /tmp/lircd.conf.temporary %s;\
		rm /tmp/lircd.conf.temporary",
		"sed -e \'s/^include.*", device_name,
		INTERNAL_LIRC_LIRCD_CONF_PATH,
		INTERNAL_LIRC_LIRCD_CONF_PATH);
	IRRECO_LINE
	IRRECO_PRINTF("%s\n", delete_cmd);
	system(delete_cmd);

	switch(status) {
		case IRRECO_BACKEND_OK:
			IRRECO_LINE
			IRRECO_RETURN_INT(IRRECO_BACKEND_OK);

		case INTERNAL_LIRC_BACKEND_ERR_COULD_NOT_DELETE_DEVICE:
			IRRECO_LINE
			IRRECO_RETURN_INT(
			INTERNAL_LIRC_BACKEND_ERR_COULD_NOT_DELETE_DEVICE);

		default:
			IRRECO_LINE
			IRRECO_PRINTF("Weird return code \"%d\"\n", status);
			IRRECO_RETURN_INT(status);
	}
	g_free(delete_cmd);

	/* Just testing */
	if(!status) {
		IRRECO_LINE
		g_print("errmsg: %s\n", internal_lirc_backend->error_msg->str);
		IRRECO_RETURN_INT(
			INTERNAL_LIRC_BACKEND_ERR_COULD_NOT_DELETE_DEVICE);
	}
	else IRRECO_RETURN_INT(status);
}


IrrecoBackendStatus internal_lirc_export_conf(gpointer instance_context,
				const char *device_name,
				IrrecoBackendFileContainer **file_container)
{
	IrrecoBackendStatus status = IRRECO_BACKEND_OK;
	gchar		*file_data = NULL;
	gboolean 	success    = TRUE;
	GString 	*file_name = g_string_new("");
	GString 	*file_path = g_string_new(INTERNAL_LIRC_REMOTES_DIR);
	IRRECO_ENTER

	g_string_append_printf(file_name, "%s", device_name);
	g_string_append_printf(file_path, "/%s", file_name->str);

	IRRECO_LINE
	IRRECO_PRINTF("%s, %s, %s\n", device_name, file_name->str, file_path->str);

	success = g_file_get_contents(file_path->str, &file_data, NULL, NULL);
	if (success == FALSE) {
		IRRECO_LINE
		status = INTERNAL_LIRC_BACKEND_ERR_FILE_OPEN_FAILED;
		goto clean;
	}
	IRRECO_LINE
	*file_container = irreco_backend_file_container_new();
	irreco_backend_file_container_set(*file_container,
					  "Internal Lirc",	/*backend*/
					  NULL,			/*category*/
    					  NULL,			/*manufacturer*/
    					  device_name,		/*model*/
    					  file_name->str,	/*name*/
       					  file_data);		/*data*/

	clean:
	g_free(file_data);
	g_string_free(file_name, TRUE);
	g_string_free(file_path, TRUE);

	IRRECO_RETURN_ENUM(status);
}

IrrecoBackendStatus internal_lirc_import_conf(gpointer instance_context,
				IrrecoBackendFileContainer *file_container)
{
	gssize length;
	gboolean success;
	GString *file_path = g_string_new(INTERNAL_LIRC_REMOTES_DIR);
	gchar *sys_cmd = NULL;
	IRRECO_ENTER
	length = strlen(file_container->data->str);

	g_string_append_printf(file_path, "/%s", file_container->name->str);
	IRRECO_PRINTF("%s\n", file_path->str);
	/* Create directory/directories for Lirc devices, if needed. */
	if(irreco_is_dir(INTERNAL_LIRC_BASE_DIR) == FALSE) {
		IRRECO_LINE
		if (g_mkdir(INTERNAL_LIRC_BASE_DIR, 0700) != 0) {
			IRRECO_LINE
			IRRECO_RETURN_ENUM(
			INTERNAL_LIRC_BACKEND_ERR_COULD_NOT_CREATE_BASE_DIR);
		}
	}
	IRRECO_LINE
	if(irreco_is_dir(INTERNAL_LIRC_REMOTES_DIR) == FALSE) {
		IRRECO_LINE
		if (g_mkdir(INTERNAL_LIRC_REMOTES_DIR, 0700) != 0) {
			IRRECO_LINE
			IRRECO_RETURN_ENUM(
			INTERNAL_LIRC_BACKEND_ERR_COULD_NOT_CREATE_DEVICE_DIR);
		}
	}
	IRRECO_LINE
	/* irreco_write_file from irreco_util.h */
	success = irreco_write_file(file_path->str,
				    file_container->data->str, length);
	IRRECO_LINE
	/* Echo include cmd to lircd.conf */
	sys_cmd = g_strdup_printf("%s '%s \"%s\"' >> %s", "echo", "include",
				file_path->str, INTERNAL_LIRC_LIRCD_CONF_PATH);
	IRRECO_PRINTF("%s\n", sys_cmd);
	system(sys_cmd);

	g_string_free(file_path, TRUE);
	g_free(sys_cmd);

	if(!success){
		IRRECO_LINE
		IRRECO_RETURN_ENUM(INTERNAL_LIRC_BACKEND_ERR_CONFIG_WRITE_FAILED);
	}
	IRRECO_LINE
	IRRECO_RETURN_ENUM(IRRECO_BACKEND_OK);
}

 IrrecoBackendStatus internal_lirc_check_conf(
 				gpointer instance_context,
 				IrrecoBackendFileContainer *file_container,
				gboolean *configuration)
{
	GString *file_path = g_string_new(INTERNAL_LIRC_REMOTES_DIR);
	IRRECO_ENTER
	g_string_append_printf(file_path, "/%s", file_container->name->str);

	/* irreco_file_exists from irreco_util.h */
	*configuration = irreco_file_exists(file_path->str);

	g_string_free(file_path, TRUE);

	IRRECO_RETURN_ENUM(IRRECO_BACKEND_OK);
}

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* IRRECO PLUGIN API.                                                         */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction.                                                */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

IrrecoBackendFunctionTable internal_lirc_backend_function_table = {
	IRRECO_BACKEND_API_VERSION,	/* backend_api_version */
	IRRECO_BACKEND_EDITABLE_DEVICES |
	IRRECO_BACKEND_CONFIGURATION_EXPORT |
	IRRECO_BACKEND_MULTI_DEVICE_SUPPORT
	/* 0 */,	 				/* flags	    */
	"Internal Lirc",			/* name		    */

	internal_lirc_backend_get_error_msg,	/* get_error_msg    */
	internal_lirc_backend_create,		/* create	    */
	internal_lirc_backend_destroy,		/* destroy	    */
	internal_lirc_backend_read_from_conf,	/* from_conf	    */
	internal_lirc_backend_save_to_conf,	/* to_conf	    */
	internal_lirc_backend_get_devices,	/* get_devices	    */
	internal_lirc_backend_get_commands,	/* get_commands	    */
	internal_lirc_backend_send_command,	/* send_command	    */
	internal_lirc_backend_configure,	/* configure	    */
	internal_lirc_backend_get_description,	/* get_description  */

	internal_lirc_backend_create_device,	/* create_device, optional */
	internal_lirc_backend_is_device_editable,/* is_device_editable, optional */
	internal_lirc_backend_edit_device, 	/* edit_device, optional */
	internal_lirc_delete_device,	    /* delete_device, optional */

	internal_lirc_export_conf,	    /* export_conf, optional */
	internal_lirc_import_conf,	    /* import_conf, optional */
	internal_lirc_check_conf,	    /* check_conf,  optional */

	NULL
};

IrrecoBackendFunctionTable *get_irreco_backend_function_table()
{
	IRRECO_ENTER
	IRRECO_RETURN_PTR(&internal_lirc_backend_function_table);
}

void* internal_lirc_backend_create()
{
	IrrecoInternalLircBackend * internal_lirc_backend;
	IRRECO_ENTER

	internal_lirc_backend = g_slice_new0(IrrecoInternalLircBackend);
	internal_lirc_backend->host = g_string_new("localhost");
	internal_lirc_backend->port = 8765;
	internal_lirc_backend->socket = -1;
	internal_lirc_backend->error_msg = g_string_new(NULL);
	IRRECO_RETURN_PTR(internal_lirc_backend);
}

void internal_lirc_backend_destroy(gpointer instance_context, gboolean permanently)
{
	IrrecoInternalLircBackend * internal_lirc_backend = instance_context;
	IRRECO_ENTER

	g_string_free(internal_lirc_backend->host, TRUE);
	g_string_free(internal_lirc_backend->error_msg, TRUE);
	g_slice_free(IrrecoInternalLircBackend, internal_lirc_backend);

	IRRECO_RETURN
}



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Error handling.                                                            */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

const gchar *internal_lirc_backend_get_error_msg(gpointer instance_context,
				       IrrecoBackendStatus code)
{
	IrrecoInternalLircBackend * internal_lirc_backend = instance_context;
	IRRECO_ENTER
	IRRECO_RETURN_PTR(internal_lirc_backend->error_msg->str);
}

gint internal_lirc_backend_error(IrrecoInternalLircBackend
			*internal_lirc_backend,
		        IrrecoInternalLircBackendError code, ...)
{
	va_list args;
	gchar *format;
	gchar *message;
	IRRECO_ENTER

	internal_lirc_backend_disconnect(internal_lirc_backend);

	/* Select message format. */
	switch (code) {
	case INTERNAL_LIRC_BACKEND_ERR_CANT_OPEN_SOCKET:
		format = _("Can not open socket.");
		break;

	case INTERNAL_LIRC_BACKEND_ERR_CANT_CONNECT:
		format = _("Can not connect to internal LIRC server.");
		break;

	case INTERNAL_LIRC_BACKEND_ERR_CANT_RESOLVE_HOST:
		format = _("Can not resolve hostname \"%s\".");
		break;

	case INTERNAL_LIRC_BACKEND_ERR_CONFIG_WRITE_FAILED:
		format = _("Failed to save configuration to file.");
		break;

	case INTERNAL_LIRC_BACKEND_ERR_CONFIG_READ_FAILED:
		format = _("Failed to read configuration from file.");
		break;

	case INTERNAL_LIRC_BACKEND_ERR_BAD_PACKET:
		format = _("Bad return packet");
		break;

	case INTERNAL_LIRC_BACKEND_ERR_COULD_NOT_SEND_PACKET:
		format = _("Could not send packet \"%s\".");
		break;

	case INTERNAL_LIRC_BACKEND_ERR_COMMAND_FAILED:
		format = _("Command \"%s\" failed.");
		break;

	case INTERNAL_LIRC_BACKEND_ERR_TIMEOUT:
		format = _("Timeout while waiting for the server to respond.");
		break;

	case INTERNAL_LIRC_BACKEND_ERR_FILE_OPEN_FAILED:
		format = _("Failed to open file.");
		break;

	case INTERNAL_LIRC_BACKEND_ERR_COULD_NOT_CREATE_DEVICE_DIR:
		format = _("Failed to create device directory.");
		break;

	case INTERNAL_LIRC_BACKEND_ERR_COULD_NOT_CREATE_BASE_DIR:
		format = _("Failed to create base directory.");
		break;

	case INTERNAL_LIRC_BACKEND_ERR_COULD_NOT_DELETE_DEVICE:
		format = _("Failed to delete device.");
		break;

	default:
		IRRECO_LINE
		g_string_set_size(internal_lirc_backend->error_msg, 0);
		g_string_append_printf(internal_lirc_backend->error_msg,
				       _("Unknown error code \"%i\"."), code);
		IRRECO_PRINTF("Error: %s\n",
				internal_lirc_backend->error_msg->str);
		IRRECO_LINE
		IRRECO_RETURN_INT(code);
		break;
	}

	va_start(args, code);
	g_vasprintf(&message, format, args);
	va_end(args);

	g_string_set_size(internal_lirc_backend->error_msg, 0);
	g_string_append(internal_lirc_backend->error_msg, message);
	g_free(message);
	IRRECO_LINE
	/* Print error and return code. */
	IRRECO_PRINTF("Error: %s\n", internal_lirc_backend->error_msg->str);
	IRRECO_RETURN_INT(code);
}



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Configuration writing and reading.                                         */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

IrrecoBackendStatus internal_lirc_backend_read_from_conf(
			gpointer instance_context, const gchar * config_file)
{
	gint port;
	gint rvalue;
	gchar *dir = NULL;
	gchar *host = NULL;
	IrrecoKeyFile *keyfile = NULL;
	IrrecoInternalLircBackend * internal_lirc_backend = instance_context;
	IRRECO_ENTER

	IRRECO_PRINTF("Reading config file \"%s\".\n", config_file);
	dir = g_path_get_dirname(config_file);
	IRRECO_PRINTF("dir: %s\n", dir);
	keyfile = irreco_keyfile_create(dir, config_file, "internal_lirc");
	IRRECO_PRINTF("%s, %s, %s\n", keyfile->file, keyfile->dir, keyfile->group);

	/* Attempt to read values from keyfile. */
	if (keyfile == NULL
	    || irreco_keyfile_get_int(keyfile, "port", &port) == FALSE
	    || irreco_keyfile_get_str(keyfile, "host", &host) == FALSE) {
		IRRECO_PRINTF("Config read failed\n");
		IRRECO_LINE
		rvalue = internal_lirc_backend_error(
				internal_lirc_backend,
				INTERNAL_LIRC_BACKEND_ERR_CONFIG_READ_FAILED);

	/* Save values to instace if read succeeded. */
	} else {
		IRRECO_LINE
		IRRECO_PRINTF("Host \"%s\"\n", host);
		IRRECO_PRINTF("Port \"%i\"\n", port);

		internal_lirc_backend->port = port;
		g_string_set_size(internal_lirc_backend->host, 0);
		g_string_append(internal_lirc_backend->host, host);
		rvalue = IRRECO_BACKEND_OK;
	}

	/* Creanup, and return status code. */
	irreco_keyfile_destroy(keyfile);
	g_free(host);
	g_free(dir);
	IRRECO_RETURN_INT(rvalue);
}



IrrecoBackendStatus internal_lirc_backend_save_to_conf(
			gpointer instance_context, const gchar * config_file)
{
	gboolean success;
	GKeyFile *keyfile;
	gchar group[] = "internal_lirc";
	IrrecoInternalLircBackend * internal_lirc_backend = instance_context;
	IRRECO_ENTER

	IRRECO_PRINTF("Saving config file \"%s\".\n", config_file);

	keyfile = g_key_file_new();
	g_key_file_set_string(keyfile, group, "host",
			internal_lirc_backend->host->str);
	g_key_file_set_integer(keyfile, group, "port",
			internal_lirc_backend->port);

	success = irreco_write_keyfile(keyfile, config_file);
	g_key_file_free(keyfile);

	if (success == TRUE) {
		IRRECO_LINE
		IRRECO_RETURN_INT(IRRECO_BACKEND_OK);
	}
	IRRECO_LINE
	IRRECO_RETURN_INT(internal_lirc_backend_error(
		internal_lirc_backend,
		INTERNAL_LIRC_BACKEND_ERR_CONFIG_WRITE_FAILED));
}



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Connection handling.                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

int internal_lirc_backend_connect(IrrecoInternalLircBackend
			*internal_lirc_backend)
{
	struct hostent *host;
	struct sockaddr_in addr;
	IRRECO_ENTER

	IRRECO_PRINTF("Connecting to internal LIRC server.\n");
	IRRECO_PRINTF("Hostname: \"%s\".\n", internal_lirc_backend->host->str);
	IRRECO_PRINTF("Port: \"%i\".\n", internal_lirc_backend->port);

	/* Get host IP. */
	memset( &addr, '\0', sizeof(addr));
	if ((host = gethostbyname(internal_lirc_backend->host->str)) == NULL) {
		IRRECO_RETURN_INT(internal_lirc_backend_error(
			internal_lirc_backend,
			INTERNAL_LIRC_BACKEND_ERR_CANT_RESOLVE_HOST,
			internal_lirc_backend->host->str));
	}

	/* Create socket. */
	addr.sin_family = AF_INET;
	addr.sin_port = htons(internal_lirc_backend->port);
	memcpy((gpointer) &addr.sin_addr, (gpointer) host->h_addr_list[0],
		(size_t) host->h_length);
	if ((internal_lirc_backend->socket = socket(
					PF_INET, SOCK_STREAM, 0)) == -1) {
		IRRECO_RETURN_INT(internal_lirc_backend_error(
			internal_lirc_backend,
			INTERNAL_LIRC_BACKEND_ERR_CANT_OPEN_SOCKET));
	}

	/* Connect socket. */
	if (connect(internal_lirc_backend->socket, (struct sockaddr *) &addr,
		    sizeof(addr)) == -1) {
		IRRECO_RETURN_INT(internal_lirc_backend_error(
			internal_lirc_backend,
			INTERNAL_LIRC_BACKEND_ERR_CANT_OPEN_SOCKET));
	}

	IRRECO_PRINTF("Connected to internal LIRC server.\n");
	IRRECO_RETURN_INT(IRRECO_BACKEND_OK);
}

void internal_lirc_backend_disconnect(IrrecoInternalLircBackend
			*internal_lirc_backend)
{
	IRRECO_ENTER
	if (internal_lirc_backend->socket != -1) {
		close(internal_lirc_backend->socket);
		internal_lirc_backend->socket = -1;
	}
	IRRECO_RETURN
}



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Device list .                                                              */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

IrrecoBackendStatus internal_lirc_backend_get_devices(gpointer instance_context,
					     IrrecoGetDeviceCallback callback)
{
	gint rvalue;
	IrrecoInternalLircBackend * internal_lirc_backend = instance_context;
	gchar *sys_cmd = NULL;
	gchar *clean_cmd = NULL;
	GDir *lirc_device_dir = NULL;
	const gchar *device_file = "";
	IRRECO_ENTER

	system("cat /dev/null > /home/user/.irreco/lircd.conf");

	/* Scan INTERNAL_LIRC_REMOTES_DIR for available devices and
	 * echo them into lircd.conf */
	lirc_device_dir = g_dir_open(
			INTERNAL_LIRC_REMOTES_DIR,
			0, NULL);
	while(device_file != NULL) {
		device_file = g_dir_read_name(lirc_device_dir);
		sys_cmd = g_strdup_printf(
			"echo 'include \"%s/%s\"' >> %s",
			INTERNAL_LIRC_REMOTES_DIR,
			device_file,
			INTERNAL_LIRC_LIRCD_CONF_PATH);
		system(sys_cmd);
	}
	g_dir_close(lirc_device_dir);

	/* Remove (null) device from lircd.conf */
	clean_cmd = g_strdup_printf(
		"%s//g' %s > /tmp/lircd.conf.temporary\
		&& cp /tmp/lircd.conf.temporary %s;\
		rm /tmp/lircd.conf.temporary",
		"sed -e \'s/.*(null)\"$",
		INTERNAL_LIRC_LIRCD_CONF_PATH,
		INTERNAL_LIRC_LIRCD_CONF_PATH);

	system(clean_cmd);

	g_free(sys_cmd);
	g_free(clean_cmd);

	system("sudo /etc/init.d/lirc reload");

	rvalue = internal_lirc_backend_connect(internal_lirc_backend);
	if (rvalue) IRRECO_RETURN_INT(rvalue);

	internal_lirc_backend->dev_callback = callback;
	rvalue = send_packet(internal_lirc_backend, "LIST\n",
			     internal_lirc_backend_get_device_responce);
	internal_lirc_backend->dev_callback = NULL;
	if (rvalue) IRRECO_RETURN_INT(rvalue);

	internal_lirc_backend_disconnect(internal_lirc_backend);
	IRRECO_RETURN_INT(IRRECO_BACKEND_OK);
}
void internal_lirc_backend_get_device_responce(IrrecoInternalLircBackend
					*internal_lirc_backend,
					const gchar * response)
{
	IRRECO_ENTER
	IRRECO_PRINTF("Device: \"%s\"\n", response);
	internal_lirc_backend->dev_callback(response, NULL);
	IRRECO_RETURN
}

IrrecoBackendStatus internal_lirc_backend_get_commands(gpointer instance_context,
					      const gchar * device_name,
					      gpointer device_contex,
					      IrrecoGetCommandCallback callback)
{
	int rvalue;
	GString *packet;
	IrrecoInternalLircBackend * internal_lirc_backend = instance_context;
	IRRECO_ENTER

	rvalue = internal_lirc_backend_connect(internal_lirc_backend);
	if (rvalue) IRRECO_RETURN_INT(rvalue);

	internal_lirc_backend->cmd_callback = callback;
	packet = g_string_new(NULL);
	g_string_append_printf(packet, "LIST %s\n", device_name);
	rvalue = send_packet(internal_lirc_backend, packet->str,
			     internal_lirc_backend_get_command_responce);
	g_string_free(packet, TRUE);
	internal_lirc_backend->cmd_callback = NULL;
	if (rvalue) IRRECO_RETURN_INT(rvalue);

	internal_lirc_backend_disconnect(internal_lirc_backend);
	IRRECO_RETURN_INT(IRRECO_BACKEND_OK);
}
void internal_lirc_backend_get_command_responce(IrrecoInternalLircBackend
					*internal_lirc_backend,
					const gchar * response)
{
	gint pos;
	IRRECO_ENTER

	IRRECO_PRINTF("Command: \"%s\"\n", response);
	if ((pos = irreco_char_pos(response, ' ')) == -1) {
		internal_lirc_backend->cmd_callback(response, NULL);
	} else {
		internal_lirc_backend->cmd_callback(response + pos + 1, NULL);
	}
	IRRECO_RETURN
}

gchar *internal_lirc_backend_get_description(gpointer instance_context)
{
	IrrecoInternalLircBackend * internal_lirc_backend = instance_context;
	IRRECO_ENTER
	IRRECO_RETURN_PTR(g_strdup(internal_lirc_backend->host->str));
}


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Command sending.                                                           */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

IrrecoBackendStatus internal_lirc_backend_send_command(gpointer instance_context,
					      const gchar * device_name,
					      gpointer device_contex,
					      const gchar * command_name,
					      gpointer command_contex)
{
	int rvalue;
	GString *packet;
	IrrecoInternalLircBackend * internal_lirc_backend = instance_context;
	IRRECO_ENTER

	rvalue = internal_lirc_backend_connect(internal_lirc_backend);
	if (rvalue) IRRECO_RETURN_INT(rvalue);
	IRRECO_LINE
	packet = g_string_new(NULL);
	g_string_append_printf(packet, "SEND_ONCE %s %s\n",
			       device_name, command_name);
	IRRECO_PRINTF("%s\n", packet->str);
	IRRECO_LINE
	rvalue = send_packet(internal_lirc_backend, packet->str, NULL);
	g_string_free(packet, TRUE);
	if (rvalue) IRRECO_RETURN_INT(rvalue);
	IRRECO_LINE
	internal_lirc_backend_disconnect(internal_lirc_backend);
	IRRECO_RETURN_INT(IRRECO_BACKEND_OK);
}



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Configuration dialog.                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/*
 * Create label, align it, and insert it into a table.
 */
void internal_lirc_backend_dlg_insert_label(GtkWidget *table,
					const gchar *str,
					guint l,
					guint r,
					guint t,
					guint b)
{
	GtkWidget *label;
	IRRECO_ENTER
	label = gtk_label_new(str);
	gtk_misc_set_alignment(GTK_MISC(label), 0, 0.5);
	gtk_table_attach(GTK_TABLE(table), label, l, r, t, b,
			 GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);
	IRRECO_RETURN
}

IrrecoBackendStatus internal_lirc_backend_configure(gpointer instance_context,
					   GtkWindow * parent)
{
	IrrecoInternalLircBackend * internal_lirc_backend = instance_context;
	gint loop = TRUE;
	GString *port_string;
	GtkWidget *dialog;
	GtkWidget *table;
	GtkWidget *host_entry;
	GtkWidget *port_entry;
	IRRECO_ENTER

	/* Create objects. */
	dialog = gtk_dialog_new_with_buttons(
		_("Internal LIRC server configuration"), parent,
		GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT |
		GTK_DIALOG_NO_SEPARATOR,
		GTK_STOCK_CANCEL, GTK_RESPONSE_REJECT,
		/*_("Test"), 100, */
		GTK_STOCK_OK, GTK_RESPONSE_ACCEPT,
		NULL);
	table = gtk_table_new(2, 2, FALSE);
	host_entry = gtk_entry_new();
	port_entry = gtk_entry_new();

	/* Set values. */
	gtk_entry_set_text(GTK_ENTRY(host_entry),
			internal_lirc_backend->host->str);
	port_string = g_string_new(NULL);
	g_string_append_printf(port_string, "%i", internal_lirc_backend->port);
	gtk_entry_set_text(GTK_ENTRY(port_entry), port_string->str);
	g_string_free(port_string, TRUE);

	/* Build dialog. */
	internal_lirc_backend_dlg_insert_label(table, _("Hostname"), 0, 1, 0, 1);
	gtk_table_attach_defaults(GTK_TABLE(table), host_entry, 1, 2, 0, 1);
	internal_lirc_backend_dlg_insert_label(table, _("Port"), 0, 1, 1, 2);
	gtk_table_attach_defaults(GTK_TABLE(table), port_entry, 1, 2, 1, 2);
	gtk_table_set_row_spacings(GTK_TABLE(table), 5);
        gtk_table_set_col_spacings(GTK_TABLE(table), 5);
	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->vbox), table);
	gtk_widget_show_all(dialog);

	while (loop == TRUE) {
		switch (gtk_dialog_run(GTK_DIALOG(dialog))) {
		case GTK_RESPONSE_REJECT:
			loop = FALSE;
			break;

		case GTK_RESPONSE_ACCEPT:
			{
			long int port;
			port = strtol(gtk_entry_get_text(GTK_ENTRY(
				      port_entry)), NULL, 10);
			if (port < 1 || port > 65535) {
				irreco_error_dlg(GTK_WINDOW(dialog),
						 _("Port number must be "
						 "in range 1 - 65535"));
				break;
			}

			internal_lirc_backend->port = port;
			g_string_set_size(internal_lirc_backend->host, 0);
			g_string_append(internal_lirc_backend->host,
					gtk_entry_get_text(
					GTK_ENTRY(host_entry)));
			loop = FALSE;
			break;
			}
		}
	}

	gtk_widget_destroy(dialog);
	IRRECO_RETURN_INT(IRRECO_BACKEND_OK);
}





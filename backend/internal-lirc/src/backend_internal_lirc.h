/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

#ifndef __BACKEND_INTERNAL_LIRC_H__
#define __BACKEND_INTERNAL_LIRC_H__

#define IRRECO_DEBUG_PREFIX "INTERNAL_LIRC"
#include "irreco_util.h"

#include <glib.h>
#include <glib/gprintf.h>
#include <glib/gstdio.h>
#include <gtk/gtk.h>

#include "irreco_backend_api.h"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Types                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
typedef enum {
	INTERNAL_LIRC_BACKEND_ERR_CANT_OPEN_SOCKET = 1,
	INTERNAL_LIRC_BACKEND_ERR_CANT_CONNECT,
	INTERNAL_LIRC_BACKEND_ERR_CANT_RESOLVE_HOST,
	INTERNAL_LIRC_BACKEND_ERR_CONFIG_WRITE_FAILED,
	INTERNAL_LIRC_BACKEND_ERR_CONFIG_READ_FAILED,
	INTERNAL_LIRC_BACKEND_ERR_BAD_PACKET,
	INTERNAL_LIRC_BACKEND_ERR_COULD_NOT_SEND_PACKET,
	INTERNAL_LIRC_BACKEND_ERR_COMMAND_FAILED,
	INTERNAL_LIRC_BACKEND_ERR_TIMEOUT,
	INTERNAL_LIRC_BACKEND_ERR_FILE_OPEN_FAILED,
	INTERNAL_LIRC_BACKEND_ERR_COULD_NOT_CREATE_DEVICE_DIR,
	INTERNAL_LIRC_BACKEND_ERR_COULD_NOT_CREATE_BASE_DIR,
	INTERNAL_LIRC_BACKEND_ERR_COULD_NOT_DELETE_DEVICE
} IrrecoInternalLircBackendError;

typedef struct _IrrecoInternalLircBackend IrrecoInternalLircBackend;
struct _IrrecoInternalLircBackend {
	GString	*host;
	int	port;
	int	socket;
	GString	*error_msg;

	IrrecoGetDeviceCallback		dev_callback;
	IrrecoGetCommandCallback	cmd_callback;
};

typedef void (*InternalLircBackendResponseHandler) (IrrecoInternalLircBackend
		* internal_lirc_backend, const char * response);



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#define _(String) (String)

#define INTERNAL_LIRC_BASE_DIR "/home/user/MyDocs/irreco"
#define INTERNAL_LIRC_REMOTES_DIR "/home/user/MyDocs/irreco/InternalLircDevices"
#define INTERNAL_LIRC_LIRCD_CONF_PATH "/etc/lircd.conf"
/* For sbox testing only */
/* #define INTERNAL_LIRC_LIRCD_CONF_PATH "/home/user/.irreco/lircd.conf" */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
void* internal_lirc_backend_create();
void internal_lirc_backend_destroy(void * instance_context, gboolean permanently);
IrrecoBackendStatus internal_lirc_backend_read_from_conf(void * instance_context,
					       const char * config_file);

IrrecoBackendStatus internal_lirc_backend_save_to_conf(void * instance_context,
					     const char * config_file);
const char *internal_lirc_backend_get_error_msg(void * instance_context,
				       IrrecoBackendStatus code);
int internal_lirc_backend_error(IrrecoInternalLircBackend * internal_lirc_backend,
		       IrrecoInternalLircBackendError code, ...);
int internal_lirc_backend_connect(IrrecoInternalLircBackend * internal_lirc_backend);
void internal_lirc_backend_disconnect(IrrecoInternalLircBackend * internal_lirc_backend);
IrrecoBackendStatus internal_lirc_backend_get_devices(void * instance_context,
					    IrrecoGetDeviceCallback callback);
void internal_lirc_backend_get_device_responce(IrrecoInternalLircBackend * internal_lirc_backend,
				      const char * response);
IrrecoBackendStatus internal_lirc_backend_get_commands(void * instance_context,
					     const char *device_name,
					     void *device_contex,
					     IrrecoGetCommandCallback callback);
void internal_lirc_backend_get_command_responce(IrrecoInternalLircBackend * internal_lirc_backend,
				       const char * response);
gchar *internal_lirc_backend_get_description(gpointer instance_context);
IrrecoBackendStatus internal_lirc_backend_send_command(void *instance_context,
					     const char *device_name,
					     void *device_contex,
					     const char *command_name,
					     void *command_contex);
IrrecoBackendStatus internal_lirc_backend_configure(void * instance_context,
					   GtkWindow * parent);



#endif /* __BACKEND_INTERNAL_LIRC_H__ */


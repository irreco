#!/bin/sh
set -x
cd `dirname "$0"`
mkdir -p po
autoreconf --install --force
intltoolize --automake --copy --force
find src -maxdepth 1 -type f -name '*.c' > po/POTFILES.in


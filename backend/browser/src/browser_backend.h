/*
 * irreco-backend-browser
 * Copyright (C) 2008 Arto Karppinen <arto.karppinen@iki.fi>
 * 
 * irreco-backend-browser is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * irreco-backend-browser is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _BROWSER_BACKEND_H_
#define _BROWSER_BACKEND_H_

#define IRRECO_DEBUG_PREFIX "BROW"

#define _(str) (str)

enum {
	BROWSER_ERROR_INVALID_APP = 123,
	BROWSER_ERROR_SPAWN_FAILED,
	BROWSER_ERROR_WGET,
	BROWSER_ERROR_MAEMO_BROWSER
};

enum {
	COL_NAME,
	COL_URL,
	COL_COUNT
};

typedef enum 
{
	BROWSER_APPLICATION_MAEMO,
	BROWSER_APPLICATION_WGET
} BrowserApplicationType;

#include <glib.h>
#include <gtk/gtk.h>
#include <irreco_util.h>
#include <irreco_backend_api.h>

const gchar *
browser_get_error_msg( IrrecoBackendStatus code );

#endif /* _BROWSER_BACKEND_H_ */

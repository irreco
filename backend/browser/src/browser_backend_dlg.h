/*
 * irreco-backend-browser
 * Copyright (C) 2008 Arto Karppinen <arto.karppinen@iki.fi>
 * 
 * irreco-backend-browser is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * irreco-backend-browser is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _BROWSER_BACKEND_DLG_H_
#define _BROWSER_BACKEND_DLG_H_

#include <glib-object.h>

G_BEGIN_DECLS

#define BROWSER_TYPE_BACKEND_DLG             (browser_backend_dlg_get_type ())
#define BROWSER_BACKEND_DLG(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), BROWSER_TYPE_BACKEND_DLG, BrowserBackendDlg))
#define BROWSER_BACKEND_DLG_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), BROWSER_TYPE_BACKEND_DLG, BrowserBackendDlgClass))
#define BROWSER_IS_BACKEND_DLG(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), BROWSER_TYPE_BACKEND_DLG))
#define BROWSER_IS_BACKEND_DLG_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), BROWSER_TYPE_BACKEND_DLG))
#define BROWSER_BACKEND_DLG_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), BROWSER_TYPE_BACKEND_DLG, BrowserBackendDlgClass))

#define BROWSER_BACKEND_DLG_PROP_STORE		"store"
#define BROWSER_BACKEND_DLG_PROP_BROWSER_TYPE	"browser-type"
#define BROWSER_BACKEND_DLG_PROP_DESCRIPTION	"description"

typedef struct _BrowserBackendDlgClass BrowserBackendDlgClass;
typedef struct _BrowserBackendDlg BrowserBackendDlg;

struct _BrowserBackendDlgClass
{
	GtkDialogClass parent_class;
};

struct _BrowserBackendDlg
{
	GtkDialog parent_instance;
};

GType browser_backend_dlg_get_type (void) G_GNUC_CONST;

GtkWidget *
browser_backend_dlg_new( GtkListStore *store );

void 
browser_backend_dlg_run( BrowserBackendDlg *self );

G_END_DECLS

#endif /* _BROWSER_BACKEND_DLG_H_ */


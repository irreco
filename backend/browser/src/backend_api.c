/*
 * irreco-backend-browser
 * Copyright (C) 2008 Arto Karppinen <arto.karppinen@iki.fi>
 * 
 * irreco-backend-browser is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * irreco-backend-browser is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "browser_backend.h"
#include "browser_backend_dlg.h"
#include "browser_run.h"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

#define _KEYFILE_GROUP_SETTINGS		"settings"
#define _KEYFILE_GROUP_URL		"url"
#define _KEYFILE_KEY_DESCRIPTION	"description"
#define _KEYFILE_KEY_BROWSER_TYPE	"browser-type"
#define _KEYFILE_KEY_NAME		"name"
#define _KEYFILE_KEY_URL		"url"

typedef struct _BrowserBackend BrowserBackend;
struct _BrowserBackend {
	BrowserApplicationType  browser_type;
	GString                *description;
	GtkListStore           *store;
	/** @todo Implement. */
};

typedef struct _BrowserSaveToConfData BrowserSaveToConfData;
struct _BrowserSaveToConfData{
	GKeyFile *keyfile;
	gint      i;
};

typedef struct _BrowserSendCommandData BrowserSendCommandData;
struct _BrowserSendCommandData {
	const gchar *name;
	GtkTreeIter  iter;
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

static gboolean
backend_get_commands_foreach( GtkTreeModel *model,
			      GtkTreePath  *path,
			      GtkTreeIter  *iter,
			      IrrecoGetCommandCallback *get_command_ptr );

static gboolean
backend_save_to_conf_foreach( GtkTreeModel          *model,
			      GtkTreePath           *path,
			      GtkTreeIter           *iter,
			      BrowserSaveToConfData *data );

static gboolean
backend_send_command_foreach( GtkTreeModel           *model,
			      GtkTreePath            *path,
			      GtkTreeIter            *iter,
			      BrowserSendCommandData *data );



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Error Messages                                                             */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

#define _ERROR_CASE(__err) case __err: IRRECO_RETURN_STR(#__err);

const gchar *
browser_get_error_msg( IrrecoBackendStatus code )
{
	IRRECO_ENTER
	
	switch ( code ) {
	_ERROR_CASE( BROWSER_ERROR_INVALID_APP )
	_ERROR_CASE( BROWSER_ERROR_SPAWN_FAILED )
	_ERROR_CASE( BROWSER_ERROR_WGET )
	_ERROR_CASE( BROWSER_ERROR_MAEMO_BROWSER )
	
	default: IRRECO_RETURN_STR( "Unknown error" )
	} 
}



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Backend API                                                                */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/**
 * Create new instance of the backend.
 *
 * The backend should be able to create multiple instances of itself.
 * Returns a new instance_context of the backend.
 *
 * @return New backend instance.
 */
static gpointer
backend_create()
{
	BrowserBackend *self = NULL;
	IRRECO_ENTER
	
	self = g_slice_new0( BrowserBackend );
	self->description = g_string_new( NULL );
	self->store = gtk_list_store_new( COL_COUNT, 
					  G_TYPE_STRING, 
					  G_TYPE_STRING);
	IRRECO_RETURN_PTR( self );
}

/**
 * Destroy instance of the backend.
 *
 * If 'permanently' argument is FALSE, the backend instance is to be reloaded
 * from the configuration file the next time Irreco is started. If 'permanently'
 * is TRUE, the configuration file will be deleted by Irreco, and this instace
 * will be destroyd permanently.
 *
 * If Irreco is closed, and backend instace is destroyed, then permanently is
 * FALSE. If user chooses to delete controller, then permanently is TRUE.
 *
 * @param permanently Will this backend instance be permanently destroyed.
 */
static void
backend_destroy(gpointer instance_context,
		gboolean permanently)
{
	BrowserBackend *self = (BrowserBackend *) instance_context;
	IRRECO_ENTER
	
	g_object_unref( G_OBJECT( self->store ));
	g_string_free( self->description, TRUE );
	g_slice_free( BrowserBackend, self );
}

/**
 * Return error message which corresponds to the backend status code.
 *
 * @return Error message.
 */
static const gchar *
backend_get_error_msg( gpointer            instance_context,
		       IrrecoBackendStatus code )
{
	/* BrowserBackend *self = (BrowserBackend *) instance_context; */
	IRRECO_ENTER
	
	IRRECO_RETURN_CONST_STR( browser_get_error_msg( code ))
}

/**
 * Create new instance of the backend.
 *
 * The backend should be able to read the configuration file and configure
 * itself accordingly.
 *
 * @param config_file Filename of configuration file.
 * @return #IRRECO_BACKEND_OK on success, error code on failure
 */
static IrrecoBackendStatus
backend_read_from_conf( gpointer     instance_context,
                        const gchar *config_file)
{
	BrowserBackend *self         = (BrowserBackend *) instance_context;
	GKeyFile       *keyfile      = NULL;
	gchar          *description  = NULL;
	gchar         **groups       = NULL;
	GError         *error        = NULL;
	gchar          *name         = NULL;
	gchar          *url          = NULL;
	gchar          *group        = NULL;
	gsize           n_groups     = 0;
	gint		i	     = 0;
	gint            browser_type = 0;
	GtkTreeIter     iter;
	IRRECO_ENTER

	/* Read keyfile. */
	keyfile = g_key_file_new();
	g_key_file_load_from_file( keyfile,
				   config_file,
				   G_KEY_FILE_NONE,
				   &error );
	if ( irreco_gerror_check_print( &error )) goto end;
	
	/* Parse settings. */
	description = g_key_file_get_string( keyfile, 
					     _KEYFILE_GROUP_SETTINGS, 
					     _KEYFILE_KEY_DESCRIPTION, 
					     &error );
	if ( ! irreco_gerror_check_print( &error )) {
		g_string_assign( self->description, description );
	}
	
	browser_type = g_key_file_get_integer( keyfile, 
					       _KEYFILE_GROUP_SETTINGS, 
					       _KEYFILE_KEY_BROWSER_TYPE, 
					       &error );
	if ( ! irreco_gerror_check_print( &error )) {
		switch ( browser_type ) {
		case BROWSER_APPLICATION_MAEMO:
		case BROWSER_APPLICATION_WGET:
			self->browser_type = browser_type;
		}
	}
	
	/* Read URL groups from keyfile. */
	groups = g_key_file_get_groups( keyfile, &n_groups );
	for ( i = 0; i < n_groups; i++ ) {
		
		g_free( name );
		g_free( url );
		name  = NULL;
		url   = NULL;
		group = groups[ i ];
		
		if ( ! g_str_has_prefix( group, _KEYFILE_GROUP_URL )) continue;
		
		name = g_key_file_get_string( keyfile, 
					      group, 
					      _KEYFILE_KEY_NAME, 
					      &error );
		if ( irreco_gerror_check_print( &error )) continue;
		
		url  = g_key_file_get_string( keyfile, 
					      group, 
					      _KEYFILE_KEY_URL, 
					      &error );
		if ( irreco_gerror_check_print( &error )) continue;
		
		gtk_list_store_append( self->store, &iter );
		gtk_list_store_set( self->store, &iter,
				    COL_NAME, name,
				    COL_URL, url,
				    -1 );
	}

end:
	if ( keyfile )     g_key_file_free( keyfile );
	if ( groups )      g_strfreev( groups );
	if ( description ) g_free( description );
	if ( name )        g_free( name );
	if ( url )         g_free( url );
	
	IRRECO_RETURN_ENUM( IRRECO_BACKEND_OK )
}

/**
 * Save backend configuration into the file.
 *
 * Irreco will assign the filename to use.
 *
 * @param config_file Filename of configuration file.
 * @return #IRRECO_BACKEND_OK on success, error code on failure
 */
static IrrecoBackendStatus
backend_save_to_conf( gpointer     instance_context,
                      const gchar *config_file )
{
	BrowserBackend       *self = (BrowserBackend *) instance_context;
	BrowserSaveToConfData data = { NULL, 0 };
	IRRECO_ENTER
	
	data.keyfile = g_key_file_new();
	
	g_key_file_set_integer( data.keyfile, 
			        _KEYFILE_GROUP_SETTINGS, 
			        _KEYFILE_KEY_BROWSER_TYPE, 
			        self->browser_type );
	g_key_file_set_string( data.keyfile, 
			       _KEYFILE_GROUP_SETTINGS, 
			       _KEYFILE_KEY_DESCRIPTION, 
			       self->description->str );
	
	gtk_tree_model_foreach( 
		GTK_TREE_MODEL( self->store ),
		(GtkTreeModelForeachFunc) backend_save_to_conf_foreach,
		(gpointer) &data );
	
	irreco_gkeyfile_write_to_file( data.keyfile, config_file );
	g_key_file_free(data.keyfile);
	
	IRRECO_RETURN_ENUM( IRRECO_BACKEND_OK )
}

static gboolean
backend_save_to_conf_foreach( GtkTreeModel          *model,
			      GtkTreePath           *path,
			      GtkTreeIter           *iter,
			      BrowserSaveToConfData *data )
{
	gchar *group = NULL;
	gchar *name  = NULL;
	gchar *url   = NULL;
	IRRECO_ENTER
	
	gtk_tree_model_get( model, iter, 
			    COL_NAME, &name,
			    COL_URL, &url, 
			    -1 );
	
	data->i = data->i + 1;
	group = g_strdup_printf( "%s-%i", _KEYFILE_GROUP_URL, data->i );
	g_key_file_set_string( data->keyfile, group, _KEYFILE_KEY_NAME, name );
	g_key_file_set_string( data->keyfile, group, _KEYFILE_KEY_URL,  url  );
	
	g_free( group );
	g_free( name );
	g_free( url );
	IRRECO_RETURN_BOOL( FALSE )
}

/**
 * Get list of devices the current backend instance knows.
 *
 * Backend must call IrrecoGetDeviceCallback once per each Device.
 *
 * @param callback Used to report Devices to Irreco.
 * @return #IRRECO_BACKEND_OK on success, error code on failure
 */
static IrrecoBackendStatus
backend_get_devices( gpointer 		     instance_context,
                     IrrecoGetDeviceCallback get_device )
{
	/* BrowserBackend *self = (BrowserBackend *) instance_context; */
	IRRECO_ENTER
	
	get_device( _("Browser"), NULL );
	IRRECO_RETURN_ENUM( IRRECO_BACKEND_OK )
}

/**
 * Get list of commands the device supports.
 *
 * @param callback Used to report Commands of Device to Irreco.
 * @return #IRRECO_BACKEND_OK on success, error code on failure
 */
static IrrecoBackendStatus
backend_get_commands( gpointer                 instance_context,
                      const gchar             *device_name,
                      gpointer                 device_contex,
                      IrrecoGetCommandCallback get_command )
{
	BrowserBackend *self = (BrowserBackend *) instance_context;
	IRRECO_ENTER
	
	gtk_tree_model_foreach( 
		GTK_TREE_MODEL( self->store ),
		(GtkTreeModelForeachFunc) backend_get_commands_foreach,
		(gpointer) &get_command );
	IRRECO_RETURN_ENUM( IRRECO_BACKEND_OK )
}

static gboolean
backend_get_commands_foreach( GtkTreeModel             *model,
			      GtkTreePath              *path,
			      GtkTreeIter              *iter,
			      IrrecoGetCommandCallback *get_command_ptr )
{
	IrrecoGetCommandCallback get_command = *get_command_ptr;
	gchar *name = NULL;
	IRRECO_ENTER
	
	gtk_tree_model_get( model, iter, COL_NAME, &name, -1 );
	get_command( name, NULL );
	g_free( name );
	IRRECO_RETURN_BOOL( FALSE )
}

/**
 * Send command to device.
 *
 * Irreco calls this function when user presses a button and triggers execution
 * of command chain. Then Irreco will call IrrecoBackendSendCommand once per
 * every command to be sent.
 *
 * @param device_name Name of Device, as reported to IrrecoGetDeviceCallback.
 * @param device_name Contex of Device, as reported to IrrecoGetDeviceCallback.
 * @param device_name Name of Command, as reported to IrrecoGetCommandCallback.
 * @param device_name Contex of Command, as reported to
 *                    IrrecoGetCommandCallback.
 * @return #IRRECO_BACKEND_OK on success, error code on failure
 */
static IrrecoBackendStatus
backend_send_command( gpointer     instance_context,
                      const gchar *device_name,
                      gpointer     device_contex,
                      const gchar *command_name,
                      gpointer     command_contex )
{
	BrowserBackend        *self   = (BrowserBackend *) instance_context;
	gchar                 *url    = NULL;
	BrowserSendCommandData data   = { NULL };
	IrrecoBackendStatus    status = 0;
	IRRECO_ENTER
	
	/* Find iter for correct row. */
	data.name = command_name;
	gtk_tree_model_foreach( 
		GTK_TREE_MODEL( self->store ),
		(GtkTreeModelForeachFunc) backend_send_command_foreach,
		(gpointer) &data );
	
	/* Run browser. */
	gtk_tree_model_get( GTK_TREE_MODEL( self->store ), &data.iter, 
			    COL_URL, &url, 
			    -1 );
	status = browser_run( self->browser_type, url );
	g_free( url );
	
	IRRECO_RETURN_INT( status )
}

static gboolean
backend_send_command_foreach( GtkTreeModel           *model,
			      GtkTreePath            *path,
			      GtkTreeIter            *iter,
			      BrowserSendCommandData *data )
{
	gboolean rvalue = FALSE;
	gchar   *name   = NULL;
	IRRECO_ENTER
	
	gtk_tree_model_get( model, iter, COL_NAME, &name, -1 );
	if ( g_utf8_collate( data->name, name ) == 0 ) {
		data->iter = *iter;
		rvalue = TRUE;
	}
	
	g_free( name );
	IRRECO_RETURN_BOOL( rvalue )
}

/**
 * Configure the backend.
 *
 * The backend should pop up some kind of dialog where the user can input the
 * configuration if needed.
 *
 * @param parent Set as the parent window of configuration dialog.
 * @return #IRRECO_BACKEND_OK on success, error code on failure
 */
static IrrecoBackendStatus
backend_configure( gpointer   instance_context,
                   GtkWindow *parent )
{
	BrowserBackend *self        = (BrowserBackend *) instance_context;
	GtkWidget      *dialog      = NULL;
	gchar          *description = NULL;
	IRRECO_ENTER

	dialog = browser_backend_dlg_new( self->store );
	gtk_window_set_transient_for( GTK_WINDOW( dialog ), parent );
	g_object_set( G_OBJECT( dialog ),
		      BROWSER_BACKEND_DLG_PROP_BROWSER_TYPE,
		      self->browser_type,
		      BROWSER_BACKEND_DLG_PROP_DESCRIPTION,
		      self->description->str,
		      NULL );
	browser_backend_dlg_run( BROWSER_BACKEND_DLG( dialog ));
	
	g_object_get( G_OBJECT( dialog ),
		      BROWSER_BACKEND_DLG_PROP_BROWSER_TYPE,
		      &self->browser_type,
		      BROWSER_BACKEND_DLG_PROP_DESCRIPTION,
		      &description,
		      NULL );
	g_string_assign( self->description, description );
	g_free( description );
	gtk_widget_destroy( dialog );
	
	IRRECO_RETURN_ENUM( IRRECO_BACKEND_OK )
}

/**
 * Get a description of the instace.
 *
 * The description should be about 10 - 30 characters long string that
 * differentiates one instance from all the other instances. The description
 * is to be displayed to the user.
 *
 * @return newly-allocated string.
 */
static gchar *
backend_get_description( gpointer instance_context )
{
	BrowserBackend *self = (BrowserBackend *) instance_context;
	IRRECO_ENTER
	
	IRRECO_RETURN_PTR( g_strdup( self->description->str ));
}

/**
 * Create a new device which can be controlled.
 *
 * The backend is expected to show appropriate dialog if needed.
 *
 * @return #IRRECO_BACKEND_OK on success, error code on failure
 */
static IrrecoBackendStatus
backend_create_device(gpointer instance_context,
                      GtkWindow * parent)
{
	/* BrowserBackend *self = (BrowserBackend *) instance_context; */
	IRRECO_ENTER
	IRRECO_RETURN_ENUM( IRRECO_BACKEND_OK )

}

/**
 * Can the device be edited?
 *
 * Irreco uses this to determinate if the device can be edited. This is usefull
 * mainly if the backend supports both editable and uneditable devices.
 *
 * @return TRUE or FALSE
 */
static gboolean
backend_is_device_editable( gpointer     instance_context,
                            const gchar *device_name,
                            gpointer     device_contex )
{
	/* BrowserBackend *self = (BrowserBackend *) instance_context; */
	IRRECO_ENTER
	IRRECO_RETURN_BOOL( TRUE )

}

/**
 * Edit currently existing device.
 *
 * Basically this means, that the user should be able to add, remove and edit
 * commands the device has. The backend is expected to show appropriate dialog
 * if needed.
 *
 * @return #IRRECO_BACKEND_OK on success, error code on failure
 */
static IrrecoBackendStatus
backend_edit_device( gpointer     instance_context,
                     const gchar *device_name,
                     gpointer     device_contex,
                     GtkWindow   *parent )
{
	/* BrowserBackend *self = (BrowserBackend *) instance_context; */
	IRRECO_ENTER
	IRRECO_RETURN_ENUM( backend_configure( instance_context, parent ));
}

/**
 * Destroy a device.
 *
 * The backend is expected to show appropriate dialog if needed.
 *
 * @return #IRRECO_BACKEND_OK on success, error code on failure
 */
static IrrecoBackendStatus
backend_delete_device( gpointer     instance_context,
                       const gchar *device_name,
                       gpointer     device_contex,
                       GtkWindow   *parent )
{
	/* BrowserBackend *self = (BrowserBackend *) instance_context; */
	IRRECO_ENTER
	IRRECO_RETURN_ENUM( IRRECO_BACKEND_OK )

}

/**
 * Function pointer table, aka vtable, of Irreco Backend.
 *
 * Please do not typecast functions. If you do, then GCC can not check
 * that function pointer and the function implementation match each other.
 *
 * If you need to typecast something, please typecast pointers inside
 * the function.
 *
 * @sa _IrrecoBackendFunctionTable
 */
IrrecoBackendFunctionTable backend_function_table = {
	IRRECO_BACKEND_API_VERSION,	/* backend_api_version		*/
	IRRECO_BACKEND_EDITABLE_DEVICES,/* flags 			*/
	"Web Browser",			/* name				*/

	backend_get_error_msg,		/* get_error_msg 		*/
	backend_create,			/* create			*/
	backend_destroy,		/* destroy			*/
	backend_read_from_conf,		/* from_conf			*/
	backend_save_to_conf,		/* to_conf			*/
	backend_get_devices,		/* get_devices			*/
	backend_get_commands,		/* get_commands			*/
	backend_send_command,		/* send_command			*/
	backend_configure,		/* configure			*/
	backend_get_description,	/* get_description    		*/

	backend_create_device,		/* create_device,      optional	*/
	backend_is_device_editable,	/* is_device_editable, optional	*/
	backend_edit_device,		/* edit_device,        optional	*/
	backend_delete_device,		/* delete_device,      optional	*/

	NULL,				/* export_conf,        optional	*/
	NULL,				/* import_conf,        optional	*/
	NULL,				/* check_conf,         optional	*/

	NULL				/* reserved */
};

/**
 * Get function table of the backend.
 *
 * @return Pointer to IrrecoBackendFunctionTable.
 */
IrrecoBackendFunctionTable *get_irreco_backend_function_table()
{
	return &backend_function_table;
}


/*
 * irreco-backend-browser
 * Copyright (C) 2008 Arto Karppinen <arto.karppinen@iki.fi>
 * 
 * irreco-backend-browser is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * irreco-backend-browser is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "browser_backend.h"
#include "browser_backend_dlg.h"
#include "browser_enum_types.h"
#include "browser_run.h"


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

enum
{
	PROP_0,
	PROP_STORE,
	PROP_BROWSER_TYPE,
	PROP_DESCRIPTION
};

enum
{
	BROWSER_RESPONSE_ADD = 10,
	BROWSER_RESPONSE_DELETE,
	BROWSER_RESPONSE_TEST
};

typedef struct _BrowserBackendDlgPrivate BrowserBackendDlgPrivate;
struct _BrowserBackendDlgPrivate {
	GtkListStore     *store;
	GtkTreeSelection *selection;
	
	GtkWidget        *scrolled_set;
	GtkWidget        *browser_radio;
	GtkWidget        *wget_radio;
	GtkWidget        *description_entry;
	
	gint              url_tab_id;
	gint              settings_tab_id;
};

#define _GET_PRIVATE(_self) \
	G_TYPE_INSTANCE_GET_PRIVATE( \
		BROWSER_BACKEND_DLG(_self), \
		BROWSER_TYPE_BACKEND_DLG, \
		BrowserBackendDlgPrivate);



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
static void
browser_backend_dlg_add_column( BrowserBackendDlg *self,
				GtkWidget         *treeview, 
				const gchar       *name, 
				gint               store_column );
static gboolean 
browser_backend_dlg_signal_switch_page( GtkNotebook       *notebook,
                                        GtkNotebookPage   *page,
                                        guint              page_num,
                                        BrowserBackendDlg *self );
static void 
browser_backend_dlg_signal_selection_changed(GtkTreeSelection  *treeselection,
                                             BrowserBackendDlg *self );
static void 
browser_backend_dlg_signal_text_edited( GtkCellRendererText *renderer,
					gchar               *path_str,
					gchar               *new_text,
					BrowserBackendDlg   *self );
static void
browser_backend_dlg_signal_setting_vadj_changed( GtkAdjustment     *adjustment, 
						 BrowserBackendDlg *self );



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

G_DEFINE_TYPE( BrowserBackendDlg, browser_backend_dlg, GTK_TYPE_DIALOG )

static void
browser_backend_dlg_init (BrowserBackendDlg *object)
{
	/* TODO: Add initialization code here */
}

static void
browser_backend_dlg_finalize (GObject *object)
{
	/* TODO: Add deinitalization code here */

	G_OBJECT_CLASS (browser_backend_dlg_parent_class)->finalize (object);
}

static void
browser_backend_dlg_set_property (GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec)
{
	BrowserBackendDlg        *self = NULL;
	BrowserBackendDlgPrivate *priv = NULL;
	
	g_return_if_fail (BROWSER_IS_BACKEND_DLG (object));
	
	self = BROWSER_BACKEND_DLG( object );
	priv = _GET_PRIVATE( self );

	switch (prop_id)
	{
	case PROP_STORE:
		priv->store = GTK_LIST_STORE( g_value_get_object( value ));
		break;
		
	case PROP_BROWSER_TYPE:
		switch ( g_value_get_enum( value )) {
		case BROWSER_APPLICATION_MAEMO:
			gtk_toggle_button_set_active( 
				GTK_TOGGLE_BUTTON( priv->browser_radio ), 
						   TRUE );
			break;
			
		case BROWSER_APPLICATION_WGET:
			gtk_toggle_button_set_active( 
				GTK_TOGGLE_BUTTON( priv->wget_radio ), 
						   TRUE );
			break;
		}
		break;
		
	case PROP_DESCRIPTION:
		gtk_entry_set_text( GTK_ENTRY( priv->description_entry ),
				    g_value_get_string( value ));
		break; 
		
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static void
browser_backend_dlg_get_property (GObject *object, guint prop_id, GValue *value, GParamSpec *pspec)
{
	BrowserBackendDlg        *self = NULL;
	BrowserBackendDlgPrivate *priv = NULL;
	
	g_return_if_fail (BROWSER_IS_BACKEND_DLG (object));

	self = BROWSER_BACKEND_DLG( object );
	priv = _GET_PRIVATE( self );
	
	switch (prop_id)
	{
	case PROP_STORE:
		g_value_set_object( value, priv->store );
		break;
		
	case PROP_BROWSER_TYPE:
		if ( gtk_toggle_button_get_active( 
			GTK_TOGGLE_BUTTON( priv->browser_radio ))) {
			g_value_set_enum( value, BROWSER_APPLICATION_MAEMO );
			
		} else if ( gtk_toggle_button_get_active( 
			GTK_TOGGLE_BUTTON( priv->wget_radio ))) {
			g_value_set_enum( value, BROWSER_APPLICATION_WGET );
		}
		break;
		
	case PROP_DESCRIPTION:
		g_value_set_string( value, 
			gtk_entry_get_text( 
				GTK_ENTRY( priv->description_entry )));
		break; 
		
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static void
browser_backend_dlg_constructed(GObject	*object)
{
	BrowserBackendDlg        *self              = NULL; 
	BrowserBackendDlgPrivate *priv              = NULL;
	GtkWidget                *notebook          = NULL;
	GtkWidget                *treeview          = NULL;
	GtkWidget                *scrolled_url      = NULL;
	GtkWidget                *settings          = NULL;
	GtkWidget                *scrolled_pad      = NULL;
	GtkWidget                *settings_pad      = NULL;
	GtkWidget                *open_label        = NULL;
	GtkWidget                *description_label = NULL;
	GtkAdjustment		 *adjustment        = NULL;
	IRRECO_ENTER
	
	self = BROWSER_BACKEND_DLG( object );
	priv = _GET_PRIVATE( self );
	
	/* Setup dialog. */
	gtk_window_set_title( GTK_WINDOW( self ),
			      _( "Web Browser Configuration" ));
	gtk_window_set_modal( GTK_WINDOW( self), TRUE );
	gtk_window_set_destroy_with_parent( GTK_WINDOW( self), TRUE );
	gtk_dialog_set_has_separator( GTK_DIALOG(self), FALSE );
	gtk_dialog_add_buttons( GTK_DIALOG( self ),
			        GTK_STOCK_ADD,    BROWSER_RESPONSE_ADD,
				GTK_STOCK_DELETE, BROWSER_RESPONSE_DELETE,
				_( "Test" ),      BROWSER_RESPONSE_TEST,
				GTK_STOCK_OK,     GTK_RESPONSE_OK,
				NULL );
	
	/* Build URL tab. */
	treeview = gtk_tree_view_new_with_model( GTK_TREE_MODEL( priv->store ));
	gtk_tree_view_set_headers_visible( GTK_TREE_VIEW( treeview ), TRUE );
	browser_backend_dlg_add_column( self, treeview, "Name", COL_NAME );
	browser_backend_dlg_add_column( self, treeview, "URL", COL_URL );
	
	priv->selection = gtk_tree_view_get_selection( 
		GTK_TREE_VIEW( treeview ));
	gtk_tree_selection_set_mode( priv->selection, GTK_SELECTION_SINGLE );
	browser_backend_dlg_signal_selection_changed( priv->selection, self );
	g_signal_connect( G_OBJECT( priv->selection ), "changed", 
		G_CALLBACK( browser_backend_dlg_signal_selection_changed ), 
		self );
	
	scrolled_url = gtk_scrolled_window_new( NULL, NULL );
	gtk_scrolled_window_set_policy( GTK_SCROLLED_WINDOW( scrolled_url ),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC );
	gtk_container_add( GTK_CONTAINER( scrolled_url ), GTK_WIDGET( treeview ));
	
	/* Build Settings tab. */
	settings = gtk_vbox_new( FALSE, 4 );
	open_label = irreco_gtk_label_bold( _( "How should URL's be opened?" ),
					    0, 0.5, 0, 0, 0, 0 );
	priv->browser_radio = gtk_radio_button_new( NULL );
	priv->wget_radio = gtk_radio_button_new_from_widget( 
		GTK_RADIO_BUTTON( priv->browser_radio ));
	gtk_button_set_label( GTK_BUTTON( priv->browser_radio ),
			      _( "Inside the Web Browser." ));
	gtk_button_set_label( GTK_BUTTON( priv->wget_radio ), 
		              _( "Hidden in the background using wget." ));
	
	description_label = irreco_gtk_label_bold( _( "Description" ),
						   0, 0.5, 8, 0, 0, 0 );
	priv->description_entry = gtk_entry_new();
	
	priv->scrolled_set = gtk_scrolled_window_new( NULL, NULL );
	gtk_scrolled_window_set_policy( GTK_SCROLLED_WINDOW( priv->scrolled_set ),
					GTK_POLICY_NEVER,
					GTK_POLICY_AUTOMATIC );
	adjustment = gtk_scrolled_window_get_vadjustment( 
			GTK_SCROLLED_WINDOW( priv->scrolled_set ));
	g_signal_connect( G_OBJECT( adjustment ), "changed", 
		G_CALLBACK( browser_backend_dlg_signal_setting_vadj_changed ), 
		self );
	
	gtk_container_add( GTK_CONTAINER( settings ), 
			   GTK_WIDGET( open_label ));
	gtk_container_add( GTK_CONTAINER( settings ), 
			   irreco_gtk_pad( GTK_WIDGET( priv->browser_radio ), 
				   	   0, 0, 8, 0 ));
	gtk_container_add( GTK_CONTAINER( settings ), 
			   irreco_gtk_pad( GTK_WIDGET( priv->wget_radio ), 
				   	   0, 0, 8, 0 ));
	gtk_container_add( GTK_CONTAINER( settings ), 
			   GTK_WIDGET( description_label ));
	gtk_container_add( GTK_CONTAINER( settings ), 
			  irreco_gtk_pad( GTK_WIDGET( priv->description_entry ), 
				   	  0, 0, 8, 0 ));
	gtk_scrolled_window_add_with_viewport(
		GTK_SCROLLED_WINDOW( priv->scrolled_set ), 
		irreco_gtk_align( GTK_WIDGET( settings ), 
			          0, 0, 1, 0, 0, 0, 0, 0 ));
	
	/* Build notebook. */
	notebook = gtk_notebook_new();
	scrolled_pad = irreco_gtk_pad( GTK_WIDGET( scrolled_url ), 
				       8, 8, 8, 8 );
	priv->url_tab_id = 
		gtk_notebook_append_page( GTK_NOTEBOOK( notebook ),
					  scrolled_pad,
					  gtk_label_new( _( "Addresses" )));
	settings_pad = irreco_gtk_align( GTK_WIDGET( priv->scrolled_set ), 
					 0, 0, 1, 1, 8, 8, 8, 8 );
	priv->settings_tab_id =
		gtk_notebook_append_page( GTK_NOTEBOOK( notebook ),
					  settings_pad,
					  gtk_label_new( _( "Settings" )));
	g_signal_connect( G_OBJECT( notebook ), "switch-page", 
			  G_CALLBACK( browser_backend_dlg_signal_switch_page ), 
			  self );
	
	/* Build dialog. */
	gtk_container_add( GTK_CONTAINER( GTK_DIALOG( self )->vbox ), 
			   irreco_gtk_pad( GTK_WIDGET( notebook ), 
				   	   8, 8, 8, 8 ));
	gtk_window_resize( GTK_WINDOW( self ), 1, 400 );
	gtk_widget_show_all( GTK_WIDGET( self ));
	IRRECO_RETURN
}

static void
browser_backend_dlg_class_init (BrowserBackendDlgClass *klass)
{
	GObjectClass* object_class = G_OBJECT_CLASS (klass);
	/* GtkDialogClass* parent_class = GTK_DIALOG_CLASS (klass); */

	object_class->finalize = browser_backend_dlg_finalize;
	object_class->set_property = browser_backend_dlg_set_property;
	object_class->get_property = browser_backend_dlg_get_property;
	object_class->constructed = browser_backend_dlg_constructed;
	
	g_type_class_add_private( klass, sizeof( BrowserBackendDlgPrivate ));  

	g_object_class_install_property( object_class,
	                                 PROP_STORE,
	                                 g_param_spec_object( BROWSER_BACKEND_DLG_PROP_STORE,
	                                                      "store",
	                                                      "store",
	                                                      GTK_TYPE_LIST_STORE,
	                                                      G_PARAM_READWRITE
							      | G_PARAM_CONSTRUCT_ONLY ));

	g_object_class_install_property( object_class,
	                                 PROP_BROWSER_TYPE,
	                                 g_param_spec_enum( BROWSER_BACKEND_DLG_PROP_BROWSER_TYPE,
	                                                    "browser-type",
	                                                    "browser-type",
	                                                    BROWSER_TYPE_APPLICATION_TYPE,
	                                                    BROWSER_APPLICATION_MAEMO,
							    G_PARAM_READWRITE ));
	
	g_object_class_install_property( object_class,
	                                 PROP_DESCRIPTION,
	                                 g_param_spec_string( BROWSER_BACKEND_DLG_PROP_DESCRIPTION,
	                                                      "description",
	                                                      "description",
	                                                      "",
							      G_PARAM_READWRITE ));
	
}

GtkWidget *
browser_backend_dlg_new( GtkListStore *store )
{
	BrowserBackendDlg *self = NULL;
	IRRECO_ENTER
	
	self = g_object_new( BROWSER_TYPE_BACKEND_DLG, "store", store, NULL );
	IRRECO_RETURN_PTR( self );
}



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

static void
browser_backend_dlg_add_column( BrowserBackendDlg *self,
				GtkWidget         *treeview, 
				const gchar       *name, 
				gint               store_column )
{
	GtkTreeViewColumn *column   = NULL;
	GtkCellRenderer   *renderer = NULL;
	IRRECO_ENTER
	
	/* Create renderer. */
	renderer = gtk_cell_renderer_text_new();
	g_object_set( G_OBJECT( renderer ), 
		      "editable", TRUE,
		      NULL);
	g_object_set_data( G_OBJECT( renderer ), "column",
			   GINT_TO_POINTER( store_column ));
	g_signal_connect( G_OBJECT( renderer ), "edited", 
			  G_CALLBACK( browser_backend_dlg_signal_text_edited ), 
			  self );
	
	/* Add column to treeview. */
	gtk_tree_view_set_headers_visible( GTK_TREE_VIEW( treeview ), TRUE );
	column = gtk_tree_view_column_new_with_attributes( name,
							   renderer,
                                                           "text", store_column,
							   NULL );
	gtk_tree_view_append_column( GTK_TREE_VIEW( treeview ), column );
	IRRECO_RETURN
}

static void
browser_backend_dlg_responce_add( BrowserBackendDlg *self )
{
	BrowserBackendDlgPrivate *priv   = NULL;
	GtkWidget                *dialog = NULL;
	GtkWidget                *table  = NULL;
	GtkWidget                *name   = NULL;
	GtkWidget                *url    = NULL;
	GtkAttachOptions          opts   = 0;
	IRRECO_ENTER
	
	priv = _GET_PRIVATE( self );

	/* Create objects. */
	dialog = gtk_dialog_new_with_buttons(
		_( "Add URL" ), GTK_WINDOW( self ),
		GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT | 
		GTK_DIALOG_NO_SEPARATOR, 
		GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
		GTK_STOCK_OK, GTK_RESPONSE_OK,
		NULL);
	table = gtk_table_new( 2, 2, FALSE );
	name  = gtk_entry_new();
	url   = gtk_entry_new();
	
	/* Build table. */
	opts = GTK_SHRINK | GTK_FILL;
	gtk_entry_set_text( GTK_ENTRY( url ), "http://" );
	gtk_table_attach( GTK_TABLE( table ),
			  irreco_gtk_label( _( "Name:" ), 0, 0.5, 0, 0, 0, 0 ),
			  0, 1, 0, 1, opts, opts, 0, 0 );
	gtk_table_attach_defaults( GTK_TABLE( table ), name, 1, 2, 0, 1 );
	gtk_table_attach( GTK_TABLE( table ),
			  irreco_gtk_label( _( "URL:" ), 0, 0.5, 0, 0, 0, 0 ),
			  0, 1, 1, 2, opts, opts, 0, 0 );
	gtk_table_attach_defaults( GTK_TABLE( table ), url, 1, 2, 1, 2 );
	
	/* Build dialog. */
	gtk_table_set_row_spacings( GTK_TABLE( table ), 4 );
        gtk_table_set_col_spacings( GTK_TABLE( table ), 4 );
	gtk_container_add( GTK_CONTAINER( GTK_DIALOG( dialog )->vbox ), 
			   irreco_gtk_pad( GTK_WIDGET( table ), 8, 8, 8, 8 ));
	gtk_window_resize( GTK_WINDOW( dialog ), 550, 1 );
	gtk_widget_show_all( dialog );
	
	/* Run dialog. */
	while ( gtk_dialog_run( GTK_DIALOG( dialog )) == GTK_RESPONSE_OK ) {
		
		const gchar *name_str = NULL;
		const gchar *url_str  = NULL;
		
		name_str = gtk_entry_get_text( GTK_ENTRY( name ));
		url_str  = gtk_entry_get_text( GTK_ENTRY( url ));
			    
		if ( irreco_str_isempty( name_str )) {
			irreco_error_dlg( GTK_WINDOW( dialog ), 
					  _( "Name is empty!" ));
		} else if ( irreco_str_isempty( url_str )) {
			irreco_error_dlg( GTK_WINDOW( dialog ), 
					  _( "URL is empty!" ));
		} else {
			GtkTreeIter iter;
			gtk_list_store_append( priv->store, &iter);
			gtk_list_store_set( priv->store, &iter,
					    COL_NAME, name_str,
					    COL_URL, url_str,
					    -1 );
			break;
		}
	}

	gtk_widget_destroy(dialog);
	IRRECO_RETURN
}

static void
browser_backend_dlg_responce_delete( BrowserBackendDlg *self )
{
	BrowserBackendDlgPrivate *priv = NULL;
	GtkTreeIter iter;
	IRRECO_ENTER
	
	priv = _GET_PRIVATE( self );
	
	if ( gtk_tree_selection_get_selected( priv->selection, NULL, &iter )) {
		
	}
	
	IRRECO_RETURN
}

static void
browser_backend_dlg_responce_test( BrowserBackendDlg *self )
{
	BrowserBackendDlgPrivate *priv   = NULL;
	gchar                    *url    = NULL;
	BrowserApplicationType    type   = 0;
	IrrecoBackendStatus       status = 0;
	GtkTreeIter               iter;
	IRRECO_ENTER
	
	priv = _GET_PRIVATE( self );
	
	if ( gtk_tree_selection_get_selected( priv->selection, NULL, &iter )) {
		
		gtk_tree_model_get( GTK_TREE_MODEL( priv->store ), &iter,
				    COL_URL, &url, 
				    -1 );
		g_object_get( G_OBJECT( self ),
			      BROWSER_BACKEND_DLG_PROP_BROWSER_TYPE,
			      &type,
			      NULL );
		
		status = browser_run( type, url );
		g_free( url );
		
		if ( status == IRRECO_BACKEND_OK ) {
			irreco_info_dlg( GTK_WINDOW( self ), 
					 _( "Success!" ));
		} else {
			irreco_error_dlg( GTK_WINDOW( self ), 
					  _( browser_get_error_msg( status )));
		}
	}
	
	IRRECO_RETURN
}



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Public Functions                                                           */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

void 
browser_backend_dlg_run( BrowserBackendDlg *self )
{
	BrowserBackendDlgPrivate *priv = NULL;
	gint response = 0; 
	gboolean loop = TRUE;
	IRRECO_ENTER
	
	priv = _GET_PRIVATE( self );
	while ( loop ) {
		
		response = gtk_dialog_run( GTK_DIALOG( self ));
		
		switch ( response ) {
		case GTK_RESPONSE_OK: 
			loop = FALSE;
			break;
		
		case BROWSER_RESPONSE_ADD:
			browser_backend_dlg_responce_add( self );
			break;
		
		case BROWSER_RESPONSE_DELETE:
			browser_backend_dlg_responce_delete( self );
			break;
		
		case BROWSER_RESPONSE_TEST:
			browser_backend_dlg_responce_test( self );
			break;
		}
	}
	
	IRRECO_RETURN
}



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Events and Callbacks                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

static gboolean 
browser_backend_dlg_signal_switch_page( GtkNotebook       *notebook,
                                        GtkNotebookPage   *page,
                                        guint              page_num,
                                        BrowserBackendDlg *self )
{
	BrowserBackendDlgPrivate *priv = NULL;
	GtkWidget *add  = NULL;
	GtkWidget *del  = NULL;
	GtkWidget *test = NULL;
	IRRECO_ENTER
	
	priv = _GET_PRIVATE( self );
	add  = irreco_gtk_dialog_get_button( GTK_WIDGET( self ), 0 );
	del  = irreco_gtk_dialog_get_button( GTK_WIDGET( self ), 1 );
	test = irreco_gtk_dialog_get_button( GTK_WIDGET( self ), 2 );
	
	if ( page_num == priv->url_tab_id ) {
		gtk_widget_show( add );
		gtk_widget_show( del );
		gtk_widget_show( test );
	} else {
		gtk_widget_hide( add );
		gtk_widget_hide( del );
		gtk_widget_hide( test );
	}
	
	IRRECO_RETURN_BOOL( FALSE )
}

static void 
browser_backend_dlg_signal_selection_changed(GtkTreeSelection  *treeselection,
                                             BrowserBackendDlg *self )
{
	BrowserBackendDlgPrivate *priv = NULL;
	GtkWidget *del  = NULL;
	GtkWidget *test = NULL;
	IRRECO_ENTER
	
	priv = _GET_PRIVATE( self );
	del  = irreco_gtk_dialog_get_button( GTK_WIDGET( self ), 1 );
	test = irreco_gtk_dialog_get_button( GTK_WIDGET( self ), 2 );
	
	if ( gtk_tree_selection_get_selected( priv->selection, NULL, NULL )) {
		gtk_widget_set_sensitive( del, TRUE );
		gtk_widget_set_sensitive( test, TRUE );
	} else {
		gtk_widget_set_sensitive( del, FALSE );
		gtk_widget_set_sensitive( test, FALSE );
	}
	IRRECO_RETURN
}

static void 
browser_backend_dlg_signal_text_edited( GtkCellRendererText *renderer,
					gchar               *path_str,
					gchar               *new_text,
					BrowserBackendDlg   *self )
{
	BrowserBackendDlgPrivate *priv = NULL;
	GtkTreePath* path;
	GtkTreeIter iter;
	gint column;
	IRRECO_ENTER
	
	priv = _GET_PRIVATE( self );
	column = GPOINTER_TO_INT( g_object_get_data( G_OBJECT( renderer ), 
						     "column" ));
	path = gtk_tree_path_new_from_string(path_str);
	gtk_tree_model_get_iter( GTK_TREE_MODEL( priv->store ), &iter, path );
	gtk_list_store_set( priv->store, &iter, column, new_text, -1);
	gtk_tree_path_free( path );
	
	IRRECO_RETURN
}

/**
 * This function is, in effect, called when the vertical size of setting 
 * GtkScrolledWindow is changed.
 */
static void
browser_backend_dlg_signal_setting_vadj_changed( GtkAdjustment     *adjustment, 
						 BrowserBackendDlg *self )
{
	BrowserBackendDlgPrivate *priv    = NULL;
	GtkWidget                *vscroll = NULL;
	gdouble                   value   = 0;
	gdouble                   upper   = 0;
	IRRECO_ENTER
	
	priv = _GET_PRIVATE( self );
	
	/* If description entry widget has focus, attempt to adjust 
	 * scrollbars so that the widget is visible. By default clicking on
	 * the widget causes maemo keyboard to be shown, which is turn causes
	 * the window to be resized, and that causes the vidget to be hidden. 
	 */
	if ( GTK_WIDGET_HAS_FOCUS( GTK_WIDGET( priv->description_entry ) )) {
		IRRECO_PRINTF( "focus\n" );
		
		g_object_get( G_OBJECT( adjustment ),
			      "value", &value,
			      "upper", &upper,
			      NULL );
		IRRECO_DEBUG( "value %f, upper %f\n", value, upper );
		
		vscroll = gtk_scrolled_window_get_vscrollbar( 
			GTK_SCROLLED_WINDOW( priv->scrolled_set ));
		gtk_range_set_value( GTK_RANGE( vscroll ), upper );
	}
	
	IRRECO_RETURN
}









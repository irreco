/*
 * irreco-backend-browser
 * Copyright (C) 2008 Arto Karppinen <arto.karppinen@iki.fi>
 * 
 * irreco-backend-browser is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * irreco-backend-browser is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "browser_run.h"


static IrrecoBackendStatus
browser_spawn( gchar **argv, gint *status )
{
	gchar   *out   = NULL;
	gchar   *err   = NULL;
	GError  *error = NULL;
	IRRECO_ENTER
	
	g_spawn_sync( getenv( "HOME" ),		/* working_directory	*/
		      argv,			/* argv			*/
		      NULL,			/* envp			*/
		      G_SPAWN_SEARCH_PATH,	/* flags		*/
		      NULL, 			/* child_setup		*/
		      NULL,			/* user_data		*/
		      &out,			/* standard_output	*/
		      &err, 			/* standard_error	*/
		      status,			/* exit_status		*/
		      &error );
	
	if ( irreco_gerror_check_print( &error )) {
		IRRECO_RETURN_ENUM( BROWSER_ERROR_SPAWN_FAILED )
	}
	
	IRRECO_PRINTF( "%s status: %i\n", argv[ 0 ], *status );
	if ( ! irreco_str_isempty( out )) {
		IRRECO_PRINTF( "%s stdout:\n%s", argv[ 0 ], out );
	}
	if ( ! irreco_str_isempty( err )) {
		IRRECO_PRINTF( "%s stderr:\n%s", argv[ 0 ], err );
	}
	
	g_free( out );
	g_free( err );
	
	IRRECO_RETURN_ENUM( IRRECO_BACKEND_OK )
}

IrrecoBackendStatus 
browser_run_maemo( const gchar *url )
{
	IrrecoBackendStatus status       = 0;
	gint                child_status = 0;
	GString            *string       = NULL; 
	gchar 	           *argv[]       = { "dbus-send", 
					     "--print-reply",
					     "--dest=com.nokia.osso_browser",
					     "/com/nokia/osso_browser",
					     "com.nokia.osso_browser.load_url",
					     NULL, NULL, NULL };
	IRRECO_ENTER
	
	string = g_string_new( "string:" );
	g_string_append( string, url );
	argv[ 5 ] = string->str;
	
	status = browser_spawn( argv, &child_status );
	
	if ( status == IRRECO_BACKEND_OK && child_status != 0 ) {
		status = BROWSER_ERROR_MAEMO_BROWSER;
	}
	
	g_string_free( string, TRUE );
	IRRECO_RETURN_INT( status );
}

IrrecoBackendStatus 
browser_run_wget( const gchar *url )
{
	IrrecoBackendStatus status       = 0;
	gint                child_status = 0;
	gchar               *argv[]      = { "wget", 
					     "--non-verbose",
					     "--output-document",
					     "/dev/null",
					     "--",
					     NULL, NULL, NULL };
	IRRECO_ENTER
	
	argv[ 5 ] = (gchar*) url;
	
	status = browser_spawn( argv, &child_status );
	if ( status == IRRECO_BACKEND_OK && child_status != 0 ) {
		status = BROWSER_ERROR_WGET;
	}

	IRRECO_RETURN_ENUM( status )
}

IrrecoBackendStatus 
browser_run( BrowserApplicationType type, const gchar *url )
{
	IRRECO_ENTER
	
	switch ( type ) {
	case BROWSER_APPLICATION_MAEMO:
		IRRECO_RETURN_INT( browser_run_maemo( url ));
		
	case BROWSER_APPLICATION_WGET:
		IRRECO_RETURN_INT( browser_run_wget( url ));
	}
	
	IRRECO_RETURN_ENUM( BROWSER_ERROR_INVALID_APP )
}











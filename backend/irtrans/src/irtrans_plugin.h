/*
 Irtrans_plugin - part of Ir Remote Control for N800
 Copyright (C) 2007 Jussi Pyykkö (jupyykko@netti.fi)

 This is based on the irreco_button_dlg program by Arto Karppinen
 							(arto.karppinen@iki.fi)

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRTRANS_PLUGIN_H_TYPEDEF__
#define __IRTRANS_PLUGIN_H_TYPEDEF__

typedef struct _IrTransPlugin IrTransPlugin;

#endif /* __IRTRANS_PLUGIN_H_TYPEDEF__ */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

#ifndef __IRTRANS_PLUGIN_H__
#define __IRTRANS_PLUGIN_H__

#ifndef G_SOURCEFUNC
#define	G_SOURCEFUNC(f) ((GSourceFunc) (f))
#endif

#define IRRECO_DEBUG_PREFIX "IRTR"

/*#define _(String) gettext (String)*/
#define _(String) String

#define _GNU_SOURCE

#include <stdlib.h>
#include <sys/stat.h>
#include <stdio.h>
#include <sys/types.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <libintl.h>
#include <locale.h>

#include <glib.h>
#include <glib/gprintf.h>
#include <glib/gstdio.h>

#include <hildon/hildon-program.h>
#include <hildon/hildon-banner.h>
#include <gtk/gtk.h>
#include <gtk/gtkmain.h>
#include <gtk/gtkbutton.h>
#include <IRTrans.h>
#include "../config.h"

#include "irreco_backend_api.h"
#include "irreco_util.h"
#include "irtrans_wrap.h"


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

struct _IrTransPlugin{

	IRTransWrap *irtrans_wrap;

	/*SOCKET irt_server;*/

	/*HildonBanner *hildon_banner;  */

	/* data structures for devices (remb) and commands (comb) */
	/*REMOTEBUFFER remb;*/
	/*COMMANDBUFFER comb;*/

	/* includes the name of the remote */
	gchar *remote;

	/* includes the command for the button */
	gchar *command;

	/* includes the IP-address of the IRTrans-module */
	/*gchar *host_addr;*/

	/* includes the host name of the IRTrans-module */
	gchar *description;

	/* indicates server type, TRUE for remote server */
	/*gboolean set_remote_irtrans_server;*/
};

enum
{
	IRTRANS_SERVER_FAILURE = 10000,
	IRTRANS_SEND_FAILURE,
	IRTRANS_BACKEND_SAVE_ERROR,
	IRTRANS_BACKEND_READ_ERROR,
	IRTRANS_START_FAILURE,
	IRTRANS_STOP_FAILURE,
	IRTRANS_HOST_FAILURE,
	IRTRANS_DEVICE_FAILURE,
	IRTRANS_COMMAND_FAILURE,
	IRTRANS_CONNECT_LOCAL_FAILURE,
	IRTRANS_CONNECT_REMOTE_FAILURE,
	IRTRANS_IRSERVER_MISSING,
	IRTRANS_REMOTES_DIR_MISSING,
	IRTRANS_CHDIR_HOME_FAILED,
	IRTRANS_SYMLINK_REMOTES_FAILED,
	IRTRANS_HOME_DIR_MISSING,
	IRTRANS_LEARN_FAILURE,
	IRTRANS_LEARN_THREAD_FAILURE,
 	IRTRANS_FILE_OPEN_ERROR
};

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/*
IrrecoBackendStatus start_irtrans_server(const gchar* ip);
void stop_irtrans_server(void);
*/
/*
IrrecoBackendStatus connect_to_local_irtrans_server(IrTransPlugin *plugin);
IrrecoBackendStatus connect_to_remote_irtrans_server(IrTransPlugin * plugin);
IrrecoBackendStatus connect_to_irtrans_server(IrTransPlugin * plugin);
*/

#endif /* __IRTRANS_PLUGIN_H__ */



/*
 Irtrans_config_dlg - part of Ir Remote Control for N800
 Copyright (C) 2007 Jussi Pyykkö (jupyykko@netti.fi)

 This is based on the irreco_button_dlg by Arto Karppinen
 							(arto.karppinen@iki.fi)

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRTRANS_REMOTE_DLG_H_TYPEDEF__
#define __IRTRANS_REMOTE_DLG_H_TYPEDEF__

typedef struct _IrTransRemoteDlg IrTransRemoteDlg;

#define IRTRANS_NEW_DEV 		1
#define IRTRANS_EDIT_DEV 		2
#define IRTRANS_DEL_DEV 		3

#endif /* __IRTRANS_REMOTE_H_TYPEDEF__ */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

#ifndef __IRTRANS_REMOTE_DLG_H__
#define __IRTRANS_REMOTE_DLG_H__

#include "irtrans_plugin.h"

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

struct _IrTransRemoteDlg{

	IrTransPlugin *plugin;

	/* widgets for loading the devices list */
	GtkTreeStore *remote_treestore;
	GtkWidget *remote_treeview;
	gint remote_selection_index;
};

enum
{
	REMOTE_COLUMN,
  	NR_COLUMNS
};

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

gboolean irtrans_remote_learn_dlg(IrTransPlugin * plugin, GtkWindow * parent);
gboolean irtrans_remote_dlg(IrTransPlugin * plugin, GtkWindow * parent);

#endif /* __IRTRANS_REMOTE_DLG_H__ */



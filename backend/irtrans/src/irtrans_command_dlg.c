/*
 Irtrans_config_dlg - part of Ir Remote Control for N800
 Copyright (C) 2007 Jussi Pyykkö (jupyykko@netti.fi)

 This is based on the irreco_button_dlg by Arto Karppinen
 							(arto.karppinen@iki.fi)

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "irtrans_command_dlg.h"

static IrTransCommandDlg *irtrans_command_dlg_create(void)
{
	IRRECO_ENTER
	IRRECO_RETURN_PTR(g_slice_new0(IrTransCommandDlg));
}

static void irtrans_command_dlg_destroy(IrTransCommandDlg * command_dialog)
{
	IRRECO_ENTER

	g_slice_free(IrTransCommandDlg, command_dialog);

	IRRECO_RETURN
}

static gboolean
irtrans_learn_command_callback(IrTransCommandDlg *command_dialog)
{
	IrrecoBackendStatus status;
	GtkWindow *parent;
	IRRECO_ENTER

	status = irtrans_wrap_learn_command(
		command_dialog->plugin->irtrans_wrap,
		command_dialog->plugin->remote,
		command_dialog->plugin->command,
		IRTRANS_LEARN_TIMEOUT);

	if (command_dialog->learn_dialog != NULL) {
  		parent = GTK_WINDOW(command_dialog->learn_dialog);
  	} else {
		parent = GTK_WINDOW(command_dialog->relearn_dialog);
  	}

	gtk_widget_set_sensitive(GTK_WIDGET(parent), TRUE);

	if (command_dialog->hildon_banner != NULL) {
		gtk_widget_destroy(GTK_WIDGET(command_dialog->hildon_banner));
		command_dialog->hildon_banner = NULL;
	}

	/* TODO: For some reason the UI will freeze if we dont close the UI.*/
	gtk_dialog_response(GTK_DIALOG(parent), GTK_RESPONSE_REJECT);

	/*
	if (status == IRRECO_BACKEND_OK) {
		gtk_dialog_response(GTK_DIALOG(parent), GTK_RESPONSE_REJECT);
	} else {
		irreco_info_dlg(parent, _("IR code was not recieved."));
	}
	*/

	IRRECO_RETURN_BOOL(FALSE)
}

static void irtrans_learn_command(IrTransCommandDlg *command_dialog)
{
	GString *message;
	GtkWindow *parent;
	IRRECO_ENTER

	if (command_dialog->learn_dialog != NULL) {
  		parent = GTK_WINDOW(command_dialog->learn_dialog);
  	} else {
		parent = GTK_WINDOW(command_dialog->relearn_dialog);
  	}

	message = g_string_new(NULL);
	g_string_printf(message,
			_("Press a button on your remote once!\n"
			"Waiting button press for %.0f seconds."),
			IRRECO_MSEC_TO_SECONDS(IRTRANS_LEARN_TIMEOUT));
	if (command_dialog->hildon_banner == NULL) {
		command_dialog->hildon_banner =
			HILDON_BANNER(hildon_banner_show_information(
				GTK_WIDGET(parent), NULL, message->str));
	}
	g_string_free(message, TRUE);

	gtk_widget_set_sensitive(GTK_WIDGET(parent), FALSE);
	g_idle_add((GSourceFunc) irtrans_learn_command_callback,
		   command_dialog);

	IRRECO_RETURN
}

#if 0
/*
 * Sends command name to the irserver which creates config-file (name.rem)
 * in to the remotes-folder.
 */
static void *irtrans_set_command(void * pointer)
{
	IrTransCommandDlg *command_dialog = pointer;
	IrrecoBackendStatus status;
	IRRECO_ENTER

	status = irtrans_wrap_connect(command_dialog->plugin->irtrans_wrap);
	if (status != IRRECO_BACKEND_OK) IRRECO_RETURN_PTR(0);

	status = irtrans_wrap_learn_command(
		command_dialog->plugin->irtrans_wrap,
		command_dialog->plugin->remote,
		command_dialog->plugin->command,
		TIMEOUT);

   	/* send "exit-signal" for gtk_dialog */
  	if(command_dialog->learn_dialog != NULL){
  		gtk_dialog_response(GTK_DIALOG(command_dialog->learn_dialog),
  							GTK_RESPONSE_REJECT);
  	} else {
  		gtk_dialog_response(GTK_DIALOG(command_dialog->relearn_dialog),
  							GTK_RESPONSE_REJECT);
  	}

  	IRRECO_RETURN_PTR(0);
}
#endif


/*
 * Get chosen command from the list pointed by selection_index.
 */
static gchar *get_command_iter(GtkWidget *treeview, gint selection_index)
{
	GtkTreeIter iter;
	GtkTreeModel *model = NULL;
	GtkTreePath *path = NULL;
	gchar *str_data = NULL;

	IRRECO_ENTER

	model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));
	path = gtk_tree_path_new_from_indices(selection_index, -1);
	gtk_tree_model_get_iter(model, &iter, path);
	gtk_tree_model_get(model, &iter, COMMAND_COLUMN, &str_data, -1);
        gtk_tree_path_free(path);

	IRRECO_RETURN_PTR(str_data);
}

static void command_list_selection_changed(GtkTreeSelection * selection,
				      	IrTransCommandDlg * command_dialog)
{
	GtkTreeIter iter;
        GtkTreeModel *model = NULL;
	GtkTreePath *path = NULL;
	gint *path_indices = NULL;

	IRRECO_ENTER

	if(gtk_tree_selection_get_selected(selection, &model, &iter)){

		path = gtk_tree_model_get_path(model, &iter);
		path_indices = gtk_tree_path_get_indices(path);
		command_dialog->command_selection_index = path_indices[0];
		gtk_tree_path_free(path);
	} else {
		command_dialog->command_selection_index = -1;
	}

	IRRECO_RETURN
}

static gboolean irtrans_load_command_list(IrTransCommandDlg * command_dialog)
{
  	GtkTreeIter iter;
	IrrecoBackendStatus status;
	const gchar *command;
  	IRRECO_ENTER

	status = irtrans_wrap_get_command_list(
		command_dialog->plugin->irtrans_wrap,
		command_dialog->plugin->remote,
		NULL);
	if (status != IRRECO_BACKEND_OK) IRRECO_RETURN_BOOL(FALSE);

	while (irtrans_wrap_get_from_list(
			command_dialog->plugin->irtrans_wrap, &command)) {
		gtk_tree_store_append(command_dialog->command_treestore,
				      &iter, NULL);
		gtk_tree_store_set(command_dialog->command_treestore,
				   &iter, COMMAND_COLUMN, command, -1);
	}

	IRRECO_RETURN_BOOL(TRUE);
}

static gboolean irtrans_create_command_list(IrTransCommandDlg * command_dialog)
{
	GtkTreeSelection *select = NULL;
	GtkTreeViewColumn *column = NULL;
	GtkCellRenderer *renderer = NULL;
	gboolean success;

  	IRRECO_ENTER

  	/* if former widget exists destroy it */
  	if(command_dialog->command_treeview != NULL){

		gtk_widget_destroy(command_dialog->command_treeview);
	}

  	/* create GtkTreeStore and GtkTreeView */
	command_dialog->command_treestore = gtk_tree_store_new(
							NRO_COLUMNS,
							G_TYPE_STRING);
	/* fill the treestore with data */
 	success = irtrans_load_command_list(command_dialog);

 	if (success){

 		command_dialog->command_treeview = gtk_tree_view_new_with_model(
							GTK_TREE_MODEL(
							command_dialog->
							command_treestore));
		g_object_unref(G_OBJECT(command_dialog->command_treestore));

		/* setup column */
		renderer = gtk_cell_renderer_text_new();
		column = gtk_tree_view_column_new_with_attributes(
								NULL, renderer,
								"text", 0, NULL);
		gtk_tree_view_append_column(GTK_TREE_VIEW(command_dialog->
							command_treeview),
							column);
		/* set selection callback */
		select = gtk_tree_view_get_selection(GTK_TREE_VIEW(
							command_dialog->
							command_treeview));
		gtk_tree_selection_set_mode(select, GTK_SELECTION_SINGLE);
		g_signal_connect(G_OBJECT(select), "changed", G_CALLBACK(
						command_list_selection_changed),
			 			command_dialog);
		IRRECO_RETURN_BOOL(TRUE);
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
}

/*
static void show_command_learn_banner(IrTransCommandDlg * command_dialog,
							GtkWindow * parent)
{
	IRRECO_ENTER

	command_dialog->hildon_banner = HILDON_BANNER(
						hildon_banner_show_animation(
						GTK_WIDGET(parent), NULL,
				_("Press the button of your remote once!")));
	IRRECO_RETURN
}
*/

	/****************************************************************/
	/*								*/
	/*								*/
	/*			   DIALOGS				*/
	/*								*/
	/*								*/
	/****************************************************************/

static gboolean irtrans_command_learn_dlg(IrTransCommandDlg *command_dialog,
							GtkWindow * parent)
{
	GtkWidget *command_table = NULL;
	GtkWidget *command_entry = NULL;
	gchar *command_buffer = NULL;
	/*GThread *thread = NULL;*/
	gint rvalue = -1;

	IRRECO_ENTER

	/* create objects */
	command_dialog->learn_dialog = gtk_dialog_new_with_buttons(
		_("Command learn dialog"), parent,
		GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT |
		GTK_DIALOG_NO_SEPARATOR,
		_("Learn Command"), IRTRANS_LEARN_CMD,
		GTK_STOCK_CANCEL, GTK_RESPONSE_REJECT, NULL);
	command_table = gtk_table_new(2, 2, FALSE);
	command_entry = gtk_entry_new();
	gtk_table_attach_defaults(GTK_TABLE(command_table),
				  gtk_label_new(_("Insert command name: ")),
				  0, 1, 0, 1);
	gtk_table_attach_defaults(GTK_TABLE(command_table),
				  command_entry,
				  1, 2, 0, 1);
	gtk_table_set_row_spacings(GTK_TABLE(command_table), 5);
        gtk_table_set_col_spacings(GTK_TABLE(command_table), 5);
        gtk_container_add(GTK_CONTAINER(GTK_DIALOG(
		command_dialog->learn_dialog)->vbox), command_table);
        gtk_widget_show_all(command_dialog->learn_dialog);

        while(rvalue == -1){
		switch(gtk_dialog_run(GTK_DIALOG(command_dialog->learn_dialog))){
		case GTK_RESPONSE_REJECT:

			rvalue = FALSE;
			break;

		case IRTRANS_LEARN_CMD:

			/* get command name */
			command_buffer = g_strdup(gtk_entry_get_text
							(GTK_ENTRY(
							command_entry)));

			if(g_utf8_strlen(command_buffer, -1) < 1){
				irreco_info_dlg(parent,
					_("Insert valid command name!"));
				g_free(command_buffer);
				break;
			} else {
				/* convert all characters to lowercase */
				command_dialog->plugin->command =
						g_utf8_strdown(
						command_buffer,
						strlen(command_buffer));

				g_free(command_buffer);

				/*IRRECO_PRINTF("Remote: %s\n",
				command_dialog->plugin->remote);
				IRRECO_PRINTF("Command: %s\n",
				command_dialog->plugin->command);*/

				if(command_dialog->plugin->remote!=NULL){

					irtrans_learn_command(command_dialog);

					/*
					#if 0
					show_command_learn_banner(
						command_dialog, parent);
					*/
					/* create thread for */
					/* set_command function */
					/*
					if((thread = g_thread_create(
						(GThreadFunc)
						irtrans_set_command,
						(void *)command_dialog,
						FALSE, NULL)) == NULL){

						}
					**/
				} else {
					irreco_info_dlg(parent,
							_("Choose device!"));
					rvalue = TRUE;
				}
				break;
			}
		}
	}

	/* destroy hildon_banner */
	/*
	if(command_dialog->hildon_banner != NULL){

		gtk_widget_destroy(GTK_WIDGET(command_dialog->hildon_banner));
	}
	*/

	gtk_widget_destroy(command_dialog->learn_dialog);
	IRRECO_RETURN_BOOL(rvalue);
}

static gboolean irtrans_command_relearn_dlg(IrTransCommandDlg *command_dialog,
							GtkWindow * parent)
{
	/*GThread *thread = NULL;*/
	gchar *header = NULL;
	gint rvalue = -1;

	command_dialog->learn_dialog = NULL;

	IRRECO_ENTER

	header = g_strjoin(" ", "Confirm relearn: ", command_dialog->
							plugin->command,
							"?", NULL);

	/* create objects */
	command_dialog->relearn_dialog = gtk_dialog_new_with_buttons(
		header, parent,
		GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT |
		GTK_DIALOG_NO_SEPARATOR,
		GTK_STOCK_OK, GTK_RESPONSE_ACCEPT,
		GTK_STOCK_CANCEL, GTK_RESPONSE_REJECT, NULL);

	gtk_window_resize(GTK_WINDOW(command_dialog->relearn_dialog), 400, 1);

        gtk_widget_show_all(command_dialog->relearn_dialog);

        while(rvalue == -1){
		switch(gtk_dialog_run(GTK_DIALOG(
					command_dialog->relearn_dialog))){

		case GTK_RESPONSE_REJECT:
			rvalue = FALSE;
			break;

		case GTK_RESPONSE_ACCEPT:

			irtrans_learn_command(command_dialog);
			/*
			#if 0
			show_command_learn_banner(command_dialog, parent);
			*/
			/* create thread for */
			/* set_command function */
			/*
			if((thread = g_thread_create(
				(GThreadFunc)irtrans_set_command,
				(void *)command_dialog,
				FALSE, NULL)) == NULL){
			}
			*/
			break;
		}
	}

	/* destroy hildon_banner */
	/*
	if(command_dialog->hildon_banner != NULL){

		gtk_widget_destroy(GTK_WIDGET(command_dialog->hildon_banner));
	}
	*/

	g_free(header);
	gtk_widget_destroy(command_dialog->relearn_dialog);
	IRRECO_RETURN_BOOL(rvalue);
}

gboolean irtrans_command_dlg(IrTransPlugin * plugin, GtkWindow * parent)
{
	IrTransCommandDlg *command_dialog;

	GtkWidget *dialog = NULL;
	GtkWidget *sw = NULL;
	gint rvalue = -1;
	gboolean success;
	/*gboolean start;*/

	IRRECO_ENTER

	command_dialog = irtrans_command_dlg_create();
	command_dialog->plugin = plugin;

	/* create objects. */
	dialog = gtk_dialog_new_with_buttons(
		plugin->remote, parent,
		GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT |
		GTK_DIALOG_NO_SEPARATOR,
		_("New Command"), IRTRANS_NEW_CMD,
		_("Command Relearn"), IRTRANS_RELEARN_CMD,
		_("Delete"), IRTRANS_DEL_CMD,
		_("Done"), GTK_RESPONSE_ACCEPT, NULL);
	sw = gtk_scrolled_window_new (NULL, NULL);
      	gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(sw),
      							GTK_SHADOW_ETCHED_IN);
      	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(sw),
      							GTK_POLICY_AUTOMATIC,
      							GTK_POLICY_AUTOMATIC);

      	/* create tree model */
 	success = irtrans_create_command_list(command_dialog);

 	if (success){

      		gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(sw),
      							command_dialog->
      							command_treeview);
      	} else {
		irreco_error_dlg(parent, _("Unable to load commands list!"));
	}

	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->vbox), sw);
	gtk_widget_show_all(dialog);

	while(rvalue == -1){
		switch(gtk_dialog_run(GTK_DIALOG(dialog))){

			case GTK_RESPONSE_ACCEPT:

				rvalue = TRUE;
				break;

			case IRTRANS_NEW_CMD:

				/* learn dialog for new button */
				irtrans_command_learn_dlg(
							command_dialog,
							parent);

				/* refresh command list */
				success = irtrans_create_command_list(
							command_dialog);
      				if (success){

      					gtk_scrolled_window_add_with_viewport(
      							GTK_SCROLLED_WINDOW(sw),
      							command_dialog->
      							command_treeview);
      					gtk_widget_show_all(dialog);
      				} else {
					irreco_error_dlg(parent,
						_("Unable to load command list!"));
				}
				break;

			case IRTRANS_RELEARN_CMD:

                                if(command_dialog->command_selection_index != 0) {
				command_dialog->plugin->command = g_strdup(
							get_command_iter(
							command_dialog->
							command_treeview,
							command_dialog->
							command_selection_index));
                                }
				/*IRRECO_PRINTF("Cmd_dlg command: %s\n",
					command_dialog->plugin->command);*/
				if(command_dialog->plugin->command != NULL){

					irtrans_command_relearn_dlg(
								command_dialog,
								parent);
					break;
				} else {
					irreco_info_dlg(parent,
						_("No reteachable commands!"));
					break;
				}

			case IRTRANS_DEL_CMD:

				irreco_info_dlg(parent, _("IRTrans does not "
						"support deleting commands."));
				break;
		}
	}

	gtk_widget_destroy(dialog);
	irtrans_command_dlg_destroy(command_dialog);
	IRRECO_RETURN_BOOL(rvalue);
}



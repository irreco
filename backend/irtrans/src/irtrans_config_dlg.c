/*
 Irtrans_config_dlg - part of Ir Remote Control for N800
 Copyright (C) 2007 Jussi Pyykkö (jupyykko@netti.fi)

 This is based on the irreco_button_dlg by Arto Karppinen
 							(arto.karppinen@iki.fi)

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "irtrans_config_dlg.h"
#include "irtrans_remote_dlg.h"

IrTransConfigDlg *irtrans_config_dlg_create(void)
{
	IRRECO_ENTER

	IRRECO_RETURN_PTR(g_slice_new0(IrTransConfigDlg));
}

void irtrans_config_dlg_destroy(IrTransConfigDlg * config_dialog)
{
	IRRECO_ENTER

	g_slice_free(IrTransConfigDlg, config_dialog);

	IRRECO_RETURN
}

/*
 * Checks the format of given IP-address.
 */
static gint check_ip_addr(const gchar *ip_address)
{
	gchar temp[SIZE];
	gchar *buffer = NULL;
	gint i = 0;
	gint addr = 0;
	gint ok = 1;

	IRRECO_ENTER

	if (ip_address == NULL) IRRECO_RETURN_INT(0);

	strcpy(temp, ip_address);

	/* split string into tokens */
	buffer = strtok(temp, "." );

	/* check splitted string */
	while(buffer != NULL){

		i++;
		sscanf(buffer, "%d", &addr);

		if(addr < 0 || addr > 255)
			ok = 0;

		buffer = strtok(NULL, ".");
	}

	if(i != 4 || !ok){

		IRRECO_RETURN_INT(0);
	} else {
		IRRECO_RETURN_INT(1);
	}
}

static gboolean irtrans_load_ip_list(IrTransConfigDlg * config_dialog)
{
	const gchar *address;
	IrrecoBackendStatus res_status;
	gint i = 0;
	GtkTreeIter iter;
	IRRECO_ENTER

	/* Attempt to autodetect IRTrans devices. */
	res_status = irtrans_wrap_get_autodetect_list(
		config_dialog->plugin->irtrans_wrap,
		&config_dialog->ip_count);
	if (res_status != IRRECO_BACKEND_OK) IRRECO_RETURN_BOOL(FALSE);

	while (irtrans_wrap_get_from_list(
		config_dialog->plugin->irtrans_wrap, &address)) {

		gtk_tree_store_append(config_dialog->ip_treestore,
				      &iter, NULL);
		gtk_tree_store_set(config_dialog->ip_treestore,
				   &iter, IP_COLUMN, address, -1);

		/* find index of the address */
		/* restored from the config file */
		if(irtrans_wrap_get_hostname(
		  config_dialog->plugin->irtrans_wrap) != NULL){

			if(strcmp(address, irtrans_wrap_get_hostname(
			    config_dialog->plugin->irtrans_wrap)) == 0){
				config_dialog->ip_selection_index = i;
			}
		}

		i++;
	}

	/* restored address didn't found from the autodetected list */
	if(config_dialog->ip_selection_index == -1 &&
		irtrans_wrap_get_hostname(
			config_dialog->plugin->irtrans_wrap) != NULL){

		gtk_tree_store_append(config_dialog->ip_treestore,
						&iter, NULL);
		gtk_tree_store_set(config_dialog->ip_treestore,
				   &iter, IP_COLUMN,
				   irtrans_wrap_get_hostname(
				   config_dialog->plugin->irtrans_wrap),
				   -1);
		config_dialog->ip_selection_index = i;
	}

	IRRECO_RETURN_BOOL(TRUE);
}

static GtkTreeModel *irtrans_create_ip_list(IrTransConfigDlg * config_dialog)
{
	GtkTreeViewColumn *column = NULL;
	GtkCellRenderer *renderer = NULL;
	GtkTreeModel *model = NULL;
	GtkTreeIter iter;
	gboolean success;

  	IRRECO_ENTER

  	/* create GtkTreeStore and GtkTreeView */
	config_dialog->ip_treestore = gtk_tree_store_new(N_COLUMNS,
							G_TYPE_STRING);
	if(!irtrans_wrap_get_remote_server(config_dialog->plugin->irtrans_wrap)){

		/* load ip-list if local server is set */
		/* fill the list store with data */
 		success = irtrans_load_ip_list(config_dialog);
 	} else {
 		success = TRUE;

 		/* append IP-address of the remote server */
 		if(irtrans_wrap_get_hostname(
			config_dialog->plugin->irtrans_wrap) != NULL){

			gtk_tree_store_append(config_dialog->ip_treestore,
							&iter, NULL);
			gtk_tree_store_set(config_dialog->ip_treestore,
					   &iter, IP_COLUMN,
					   irtrans_wrap_get_hostname(
					   config_dialog->plugin->irtrans_wrap),
					   -1);
			config_dialog->ip_selection_index = 0;
		}
 	}

 	if (success){

 		/* if former widget exists destroy it */
  		if(config_dialog->ip_treeview != NULL){

			gtk_widget_destroy(config_dialog->ip_treeview);
		}

 		config_dialog->ip_treeview = gtk_tree_view_new_with_model(
 								GTK_TREE_MODEL(
								config_dialog->
								ip_treestore));
		g_object_unref(G_OBJECT(config_dialog->ip_treestore));

		/* setup column */
		renderer = gtk_cell_renderer_text_new();
		column = gtk_tree_view_column_new_with_attributes(NULL,
								renderer,
								"text",
								0, NULL);
		gtk_tree_view_append_column(GTK_TREE_VIEW(
							config_dialog->
							ip_treeview),
							column);
		model = gtk_tree_view_get_model(GTK_TREE_VIEW(
							config_dialog->
							ip_treeview));

		IRRECO_RETURN_PTR(model);
	} else {
		IRRECO_RETURN_PTR(NULL);
	}
}

static void if_remote_pressed(GtkWidget* widget, gpointer data)
{
	IrTransConfigDlg *config_dialog = data;

	IRRECO_ENTER

	irtrans_wrap_set_remote_server(
		config_dialog->plugin->irtrans_wrap, TRUE);

	IRRECO_RETURN
}

static void if_local_pressed(GtkWidget* widget, gpointer data)
{
	IrTransConfigDlg *config_dialog = data;

	IRRECO_ENTER

	irtrans_wrap_set_remote_server(
		config_dialog->plugin->irtrans_wrap, FALSE);

	IRRECO_RETURN
}

	/****************************************************************/
	/*								*/
	/*								*/
	/*			   DIALOGS				*/
	/*								*/
	/*								*/
	/****************************************************************/

/*
 * Dialog to configure IrTrans plugin.
 */
gboolean irtrans_config_dlg(IrTransPlugin * plugin, GtkWindow * parent)
{
	IrTransConfigDlg *config_dialog;

	GtkTreeModel *model = NULL;
	GtkTable *table = NULL;
	GSList* check_group = NULL;

	GtkWidget *dialog = NULL;
	GtkWidget *combo = NULL;
	GtkWidget *description_entry = NULL;
	GtkWidget* remote_check = NULL;
	GtkWidget* local_check = NULL;
	GtkWidget *label_up = NULL;
	GtkWidget *label_down = NULL;
	GtkWidget *scrolled = NULL;

	GtkWidget *align_1 = NULL;
	GtkWidget *align_2 = NULL;
	GtkWidget *align_3 = NULL;
	GtkWidget *align_4 = NULL;

	GtkWidget *description_label = NULL;
	GtkWidget *ip_label = NULL;

	gint rvalue = -1;
	gint ok;
	gint retry = 0;
	/*gboolean start;*/

	IRRECO_ENTER

	config_dialog = irtrans_config_dlg_create();
	config_dialog->plugin = plugin;
	config_dialog->ip_selection_index = -1;
	config_dialog->ip_count = 0;

	/* create objects */
	dialog = gtk_dialog_new_with_buttons(
		_("IRTrans configure"), parent,
		GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT |
		GTK_DIALOG_NO_SEPARATOR,
		_("Find Modules"), IRTRANS_CONNECT_RETRY,
		_("Learn Device"), IRTRANS_LEARN_DEVICE,
		GTK_STOCK_CANCEL, GTK_RESPONSE_REJECT,
		GTK_STOCK_OK, GTK_RESPONSE_ACCEPT, NULL);

	/* create widgets */
	label_up = irreco_gtk_label_bold(_("General"), 0, 0, 6, 0, 0, 0);
	label_down = irreco_gtk_label_bold(_("IrTrans Server"), 0, 0, 6, 0, 0, 0);

	description_label = gtk_label_new(_("Module description: "));
	ip_label = gtk_label_new(_("Module IP-address: "));

	remote_check = gtk_radio_button_new_with_label(NULL,
						       _("Use remote server"));
	check_group = gtk_radio_button_get_group(GTK_RADIO_BUTTON(remote_check));
	local_check = gtk_radio_button_new_with_label(check_group,
						      _("Start local server"));
	check_group = gtk_radio_button_get_group(GTK_RADIO_BUTTON(local_check));

	/* build dialog */
	table = GTK_TABLE(gtk_table_new(6, 2, FALSE));
	combo = gtk_combo_box_entry_new_text();
	description_entry = gtk_entry_new();

	/*gtk_table_set_row_spacings(table, 6);
	gtk_table_set_col_spacings(table, 110);*/

	align_1 = irreco_gtk_align(description_label, 0 /*xalign*/,
						 0.5 /*yalign*/,
						 0 /*xscale*/,
						 0 /*yscale*/,
						 0 /*padding_top*/,
						 0 /*padding_bottom*/,
						 12 /*padding_left*/,
						 0 /*padding_right*/);

	align_2 = irreco_gtk_align(ip_label, 0, 0.5, 0, 0, 0, 0, 12, 0);

	align_3 = irreco_gtk_align(local_check, 0, 0.5, 0, 0, 0, 0, 12, 0);

	align_4 = irreco_gtk_align(remote_check, 0, 0.5, 0, 0, 0, 0, 12, 0);

	gtk_table_attach_defaults(GTK_TABLE(table), label_up, 0, 1, 0, 1);
	gtk_table_attach_defaults(GTK_TABLE(table), align_1, 0, 1, 1, 2);
	gtk_table_attach_defaults(GTK_TABLE(table), description_entry, 1, 2, 1, 2);
	gtk_table_attach_defaults(GTK_TABLE(table), align_2, 0, 1, 2, 3);
	gtk_table_attach_defaults(GTK_TABLE(table),GTK_WIDGET(combo),
							1, 2, 2, 3);
	gtk_table_attach_defaults(GTK_TABLE(table), label_down, 0, 1, 3, 4);
	gtk_table_attach_defaults(GTK_TABLE(table), align_3, 0, 1, 4, 5);
	gtk_table_attach_defaults(GTK_TABLE(table), align_4, 0, 1, 5, 6);

	scrolled = gtk_scrolled_window_new(NULL, NULL);
	/*gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(scrolled),
      							GTK_SHADOW_ETCHED_IN);*/
      	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolled),
      							GTK_POLICY_AUTOMATIC,
      							GTK_POLICY_AUTOMATIC);
      	gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(scrolled),
      							GTK_WIDGET(table));

      	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->vbox),
      							GTK_WIDGET(scrolled));

        /* radiobutton signals */
	g_signal_connect(G_OBJECT(remote_check), "clicked", G_CALLBACK(
							if_remote_pressed),
							(gpointer)config_dialog);
	g_signal_connect(G_OBJECT(local_check), "clicked", G_CALLBACK(
							if_local_pressed),
							(gpointer)config_dialog);

	/* if backend config-file doesn't exists set local server */
	if(irtrans_wrap_get_hostname(
		config_dialog->plugin->irtrans_wrap) == NULL){

		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(local_check),
									TRUE);
	}

	/* create model and fill with data if it exists */
	model = irtrans_create_ip_list(config_dialog);

	if(model == NULL){

		/* irreco_info_dialog from irreco_util.h */
		irreco_info_dlg(parent, _("Couldn't create IP-list!"));

	} else {
		gtk_combo_box_set_model(GTK_COMBO_BOX(combo), model);

		/* ip-list creation succeed but no LAN modules found*/
		if(config_dialog->ip_count == 0 &&
			!irtrans_wrap_get_remote_server(config_dialog->plugin->irtrans_wrap)){

			irreco_info_dlg(parent,
					_("Cannot autodetect IRTrans modules!\n"
					"Check your IRTrans modules and retry."));
		}
	}

	/* set values if backend config-file exists*/
	if(irtrans_wrap_get_hostname(config_dialog->plugin->irtrans_wrap)
		!= NULL && config_dialog->plugin->description != NULL) {

		gtk_entry_set_text(GTK_ENTRY(description_entry),
					config_dialog->plugin->description);
		gtk_combo_box_set_active(GTK_COMBO_BOX(combo),
					config_dialog->ip_selection_index);

		if(irtrans_wrap_get_remote_server(config_dialog->plugin->irtrans_wrap)){

			/* set remote button */
			gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(
								remote_check),
									TRUE);
		} else {
			gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(
								local_check),
									TRUE);
		}
	}

	gtk_window_resize(GTK_WINDOW(dialog), 1, 300);
        gtk_widget_show_all(dialog);

	while(rvalue == -1){
		switch(gtk_dialog_run(GTK_DIALOG(dialog))){
		case GTK_RESPONSE_REJECT:
			rvalue = FALSE;
			break;

		case GTK_RESPONSE_ACCEPT:

			/* get instance hostname and hostaddress */
			plugin->description = g_strdup(
					gtk_entry_get_text(GTK_ENTRY(
					description_entry)));

			irtrans_wrap_set_hostname(
				plugin->irtrans_wrap,
				gtk_combo_box_get_active_text(
				GTK_COMBO_BOX(combo)));

			/* check hostname and hostaddress */
			if(g_utf8_strlen(plugin->description, -1) < 1){

				irreco_error_dlg(parent,
				_("Empty module description is not valid."
				"Type something, or press cancel."));
				break;
			}

			ok = check_ip_addr(irtrans_wrap_get_hostname(
				plugin->irtrans_wrap));

			if(!ok){
				irreco_info_dlg(parent,
				_("Please check modules IP-address!"));
				break;
			}

			rvalue = TRUE;
			break;

		case IRTRANS_CONNECT_RETRY:
			if(retry < 2){

				retry++;

				model = irtrans_create_ip_list(
							config_dialog);
				if(model == NULL){

					/* irreco_info_dialog */
					/* from irreco_util.h */
					irreco_info_dlg(parent,
					_("Couldn't create IP-list!"));
				} else {
					gtk_combo_box_set_model(
					GTK_COMBO_BOX(combo), model);

					/* ip-list creation succeed */
					/* but no modules found */
					if(config_dialog->ip_count == 0
					   && !irtrans_wrap_get_remote_server(
						config_dialog->plugin->irtrans_wrap)) {

					irreco_info_dlg(parent,
				_("Cannot autodetect IRTrans modules!\n"
				"Check your IRTrans modules and retry."));
					}
				}

				gtk_widget_show_all(dialog);
				break;
			} else {

				irreco_info_dlg(parent,
				_("Cannot autodetect IRTrans modules!\n"
			"Check your WLAN connection and retry."));

				rvalue = FALSE;
				break;
			}

		case IRTRANS_LEARN_DEVICE:

			/* get instance hostname and hostaddress */
			plugin->description = g_strdup(
					gtk_entry_get_text(GTK_ENTRY(
					description_entry)));

			irtrans_wrap_set_hostname(
				plugin->irtrans_wrap,
				gtk_combo_box_get_active_text(
				GTK_COMBO_BOX(combo)));

			/*check_host_name and host_address*/
			if(g_utf8_strlen(plugin->description, -1) < 1){
				irreco_error_dlg(parent,
				_("Empty module description is not valid."
				"Type something, or press cancel."));
				break;
			}

			ok = check_ip_addr(irtrans_wrap_get_hostname(
				plugin->irtrans_wrap));
			if (ok) {

				IrrecoBackendStatus status;
				status = irtrans_wrap_connect(
					plugin->irtrans_wrap);
				if (status != IRRECO_BACKEND_OK) {
					break;
				}

				/* enter to device learn dialog */
				irtrans_remote_dlg(plugin, parent);

				irtrans_wrap_disconnect(
					plugin->irtrans_wrap);
				break;
			} else {
				irreco_info_dlg(parent,
				_("Please check modules IP-address!"));
				break;
			}
		}
	}

	irtrans_config_dlg_destroy(config_dialog);
	gtk_widget_destroy(dialog);
	IRRECO_RETURN_BOOL(rvalue);
}


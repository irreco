/*
 Irtrans_plugin - part of Ir Remote Control for N800
 Copyright (C) 2007 Jussi Pyykkö (jupyykko@netti.fi)

 This is based on the test_plugin program by Arto Karppinen
 							(arto.karppinen@iki.fi)

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include "irtrans_plugin.h"
#include "irtrans_config_dlg.h"
#include "irtrans_remote_dlg.h"
#include "irtrans_command_dlg.h"


const char *irtrans_plugin_get_error_msg(gpointer instance_context,
					 IrrecoBackendStatus code)
{
	IRRECO_ENTER

	switch(code){
	default:
		IRRECO_RETURN_STR("Unknown error");
	case IRTRANS_SERVER_FAILURE:
		IRRECO_RETURN_STR("IRTrans server is not responding!");
	case IRTRANS_SEND_FAILURE:
		IRRECO_RETURN_STR("IRTrans send failed!");
	case IRTRANS_BACKEND_SAVE_ERROR:
		IRRECO_RETURN_STR("IRTrans config-file save error!");
	case IRTRANS_BACKEND_READ_ERROR:
		IRRECO_RETURN_STR("IRTrans config-file read error!");
	case IRTRANS_START_FAILURE:
		IRRECO_RETURN_STR("IRTrans system start failed!");
	case IRTRANS_STOP_FAILURE:
		IRRECO_RETURN_STR("IRTrans system stop failed!");
	case IRTRANS_HOST_FAILURE:
		IRRECO_RETURN_STR("IP address has not been set");
	case IRTRANS_DEVICE_FAILURE:
		IRRECO_RETURN_STR("Could not get device list.");
	case IRTRANS_COMMAND_FAILURE:
		IRRECO_RETURN_STR("Could not get command list.");
	case IRTRANS_CONNECT_LOCAL_FAILURE:
		IRRECO_RETURN_STR("Could not connect to local irserver.");
	case IRTRANS_CONNECT_REMOTE_FAILURE:
		IRRECO_RETURN_STR("Could not connect to remote irserver.");
	case IRTRANS_IRSERVER_MISSING:
		IRRECO_RETURN_STR("Could not find irserver executable.");
	case IRTRANS_REMOTES_DIR_MISSING:
		IRRECO_RETURN_STR("Could not create remotes directory for irserver.");
	case IRTRANS_CHDIR_HOME_FAILED:
		IRRECO_RETURN_STR("Could not chdir into \"" IRTRANS_HOME_DIR "\".");
	case IRTRANS_SYMLINK_REMOTES_FAILED:;
		IRRECO_RETURN_STR("Could not create remoted directory symlink.");
	case IRTRANS_HOME_DIR_MISSING:
		IRRECO_RETURN_STR("Could not create \"" IRTRANS_HOME_DIR "\".");
	case IRTRANS_LEARN_FAILURE:
		IRRECO_RETURN_STR("Could not learn command.");
	case IRTRANS_LEARN_THREAD_FAILURE:
		IRRECO_RETURN_STR("Could not create learn command thread.");
	case IRTRANS_FILE_OPEN_ERROR:
		IRRECO_RETURN_STR("Could not open configuration file.");

	#define IRTRANS_ERROR_CASE(__err) case __err: IRRECO_RETURN_STR(#__err);
	IRTRANS_ERROR_CASE(ERR_OPEN)
	IRTRANS_ERROR_CASE(ERR_RESET)
	IRTRANS_ERROR_CASE(ERR_VERSION)
	IRTRANS_ERROR_CASE(ERR_TIMEOUT)
	IRTRANS_ERROR_CASE(ERR_READVERSION)
	IRTRANS_ERROR_CASE(ERR_DBOPENINPUT)
	IRTRANS_ERROR_CASE(ERR_REMOTENOTFOUND)
	IRTRANS_ERROR_CASE(ERR_COMMANDNOTFOUND)
	IRTRANS_ERROR_CASE(ERR_TIMINGNOTFOUND)
	IRTRANS_ERROR_CASE(ERR_OPENASCII)
	IRTRANS_ERROR_CASE(ERR_NODATABASE)
	IRTRANS_ERROR_CASE(ERR_OPENUSB)
	IRTRANS_ERROR_CASE(ERR_RESEND)
	IRTRANS_ERROR_CASE(ERR_TOGGLE_DUP)
	IRTRANS_ERROR_CASE(ERR_DBOPENINCLUDE)
	IRTRANS_ERROR_CASE(ERR_NOFILEOPEN)
	IRTRANS_ERROR_CASE(ERR_FLOCK)
	IRTRANS_ERROR_CASE(ERR_STTY)
	IRTRANS_ERROR_CASE(ERR_HOTCODE)
	IRTRANS_ERROR_CASE(ERR_NOTIMING)
	IRTRANS_ERROR_CASE(ERR_TEMPCOMMAND)
	IRTRANS_ERROR_CASE(ERR_OPENTRANS)
	IRTRANS_ERROR_CASE(ERR_TESTCOM)
	IRTRANS_ERROR_CASE(ERR_SHUTDOWN)
	IRTRANS_ERROR_CASE(ERR_ISMACRO)
	IRTRANS_ERROR_CASE(ERR_LONGRAW)
	IRTRANS_ERROR_CASE(ERR_LONGDATA)
	IRTRANS_ERROR_CASE(ERR_WRONGBUS)
	IRTRANS_ERROR_CASE(ERR_COMMANDSTRING)
	IRTRANS_ERROR_CASE(ERR_OVERWRITE)
	IRTRANS_ERROR_CASE(ERR_CCF)
	IRTRANS_ERROR_CASE(ERR_UDPFORMAT)
	IRTRANS_ERROR_CASE(ERR_TESTCOMOK)
	IRTRANS_ERROR_CASE(ERR_NOIRDB)
	IRTRANS_ERROR_CASE(ERR_NOTSUPPORTED)
	IRTRANS_ERROR_CASE(ERR_NO_RS232)
	IRTRANS_ERROR_CASE(ERR_SENDOK)
	IRTRANS_ERROR_CASE(ERR_CCFLEN)
	IRTRANS_ERROR_CASE(ERR_OPENSOCKET)
	IRTRANS_ERROR_CASE(ERR_BINDSOCKET)
	IRTRANS_ERROR_CASE(ERR_FINDHOST)
	IRTRANS_ERROR_CASE(ERR_CONNECT)
	IRTRANS_ERROR_CASE(ERR_SEND)
	IRTRANS_ERROR_CASE(ERR_RECV)
	IRTRANS_ERROR_CASE(ERR_BINDWEB)
	IRTRANS_ERROR_CASE(ERR_DEVICEUNKNOWN)
	#undef IRTRANS_ERROR_CASE
	}
}

void *irtrans_plugin_create()
{
	IrTransPlugin *plugin;
	IRRECO_ENTER

	plugin = g_slice_new0(IrTransPlugin);
	plugin->irtrans_wrap = irtrans_wrap_new();
	IRRECO_RETURN_PTR(plugin);
}

void irtrans_plugin_destroy(gpointer instance_context, gboolean permanently)
{
	IrTransPlugin *plugin = instance_context;
	IRRECO_ENTER

	irtrans_wrap_disconnect(plugin->irtrans_wrap);

	g_free(plugin->remote);
	g_free(plugin->command);

	irtrans_wrap_delete(plugin->irtrans_wrap);
	g_slice_free(IrTransPlugin, plugin);

	IRRECO_RETURN
}

IrrecoBackendStatus irtrans_plugin_read_from_conf(gpointer instance_context,
						  const char * config_file)
{
	IrTransPlugin *plugin = instance_context;
	GError *error = NULL;
	gchar *address = NULL;
	gchar *name = NULL;
	GKeyFile *keyfile = NULL;
	gchar *group = NULL;
	gchar** groups = NULL;
	gsize group_count = 0;
	gboolean irtrans_server_flag;
	gint i;
	IRRECO_ENTER

	keyfile = g_key_file_new();
	g_key_file_load_from_file(keyfile, config_file, 0, &error);

	if(irreco_gerror_check_print(&error)){

		g_key_file_free(keyfile);
		IRRECO_RETURN_ENUM(IRTRANS_BACKEND_READ_ERROR);
	}

	groups = g_key_file_get_groups(keyfile, &group_count);

	if(groups == NULL){
		g_key_file_free(keyfile);
		IRRECO_RETURN_ENUM(IRTRANS_BACKEND_READ_ERROR);
	}

	/*read IP-addresses, host names and server flags from the .config file.*/
	for (i = 0; i < group_count; i++) {

		group = groups[i];

		if(g_str_has_prefix(group, "irtrans")){

			address = g_key_file_get_string(keyfile,
						group, "address", &error);
			name = g_key_file_get_string(keyfile,
						group, "name", &error);
			irtrans_server_flag = g_key_file_get_boolean(keyfile,
						group, "flag", &error);

			if(irreco_gerror_check_print(&error)){

				g_strfreev(groups);
				g_key_file_free(keyfile);
				IRRECO_RETURN_ENUM(IRTRANS_BACKEND_READ_ERROR);
			} else {
				irtrans_wrap_set_hostname(plugin->irtrans_wrap,
							  address);
				plugin->description = name;
				irtrans_wrap_set_remote_server(
					plugin->irtrans_wrap,
					irtrans_server_flag);
			}
		}
	}
	g_strfreev(groups);
	g_key_file_free(keyfile);

	IRRECO_RETURN_ENUM(IRRECO_BACKEND_OK);
}

IrrecoBackendStatus irtrans_plugin_save_to_conf(gpointer instance_context,
						const char * config_file)
{
	IrTransPlugin *plugin = instance_context;
	GKeyFile *keyfile = NULL;
	GError *error = NULL;
	gchar group[] = "irtrans";
	gchar *data = NULL;
	gsize data_size;
	gboolean success;
	IRRECO_ENTER

	if(config_file == NULL) IRRECO_RETURN_ENUM(IRTRANS_BACKEND_SAVE_ERROR);

	keyfile = g_key_file_new();
	g_key_file_set_string(keyfile, group, "address", irtrans_wrap_get_hostname(plugin->irtrans_wrap));
	g_key_file_set_string(keyfile, group, "name", plugin->description);
	g_key_file_set_boolean(keyfile, group, "flag",
			       irtrans_wrap_get_remote_server(
			       plugin->irtrans_wrap));

	data = g_key_file_to_data(keyfile, &data_size, &error);

	if(irreco_gerror_check_print(&error)){

		g_key_file_free(keyfile);
		IRRECO_RETURN_ENUM(IRTRANS_BACKEND_SAVE_ERROR);
	}

	/* irreco_write_file from irreco_util.h */
	success = irreco_write_file(config_file, data, data_size);

	if(!success){

		g_key_file_free(keyfile);
		g_free(data);
		IRRECO_RETURN_ENUM(IRTRANS_BACKEND_SAVE_ERROR);
	}
	g_key_file_free(keyfile);
	g_free(data);

	IRRECO_RETURN_ENUM(IRRECO_BACKEND_OK);
}

IrrecoBackendStatus irtrans_plugin_get_devices(gpointer instance_context,
					       IrrecoGetDeviceCallback callback)
{
	IrrecoBackendStatus status;
	IrTransPlugin *plugin = instance_context;
	const gchar *device;
  	IRRECO_ENTER

  	status = irtrans_wrap_get_remote_list(plugin->irtrans_wrap, NULL);
	if (status != IRRECO_BACKEND_OK) IRRECO_RETURN_INT(status);

	while (irtrans_wrap_get_from_list(plugin->irtrans_wrap, &device)) {
		callback(device, NULL);
	}

	irtrans_wrap_disconnect(plugin->irtrans_wrap);
	IRRECO_RETURN_ENUM(IRRECO_BACKEND_OK);
}

IrrecoBackendStatus irtrans_plugin_get_commands(gpointer instance_context,
			      		const char * device_name,
			     		void * device_contex,
			      		IrrecoGetCommandCallback callback)
{
	IrrecoBackendStatus status;
	IrTransPlugin *plugin = instance_context;
	const gchar *command;
  	IRRECO_ENTER

  	status = irtrans_wrap_get_command_list(
		plugin->irtrans_wrap, device_name, NULL);
	if (status != IRRECO_BACKEND_OK) IRRECO_RETURN_INT(status);

	while (irtrans_wrap_get_from_list(plugin->irtrans_wrap, &command)) {
		callback(command, NULL);
	}

	irtrans_wrap_disconnect(plugin->irtrans_wrap);
	IRRECO_RETURN_ENUM(IRRECO_BACKEND_OK);
}

IrrecoBackendStatus irtrans_plugin_send_command(gpointer instance_context,
			      const char * device_name,
			      void * device_contex,
			      const char * command_name,
			      void * command_contex)
{
	IrrecoBackendStatus status;
	IrTransPlugin *plugin = instance_context;
  	IRRECO_ENTER

	status = irtrans_wrap_send_command(plugin->irtrans_wrap,
					   device_name, command_name);
	irtrans_wrap_disconnect(plugin->irtrans_wrap);
	IRRECO_RETURN_INT(status);
}


IrrecoBackendStatus irtrans_plugin_configure(gpointer instance_context,
					     GtkWindow * parent)
{
	IrTransPlugin *plugin = instance_context;
	IRRECO_ENTER

	/* enter to irtrans configure dialog */
	irtrans_config_dlg(plugin, parent);
	irtrans_wrap_disconnect(plugin->irtrans_wrap);
	IRRECO_RETURN_ENUM(IRRECO_BACKEND_OK);
}

/*
 * Get a description of the instace.
 *
 * The description should be about 10 - 30 characters long string that
 * differentiated one  instance from all the other instances. The description
 * is to be displayed to the user.
 */
gchar *irtrans_get_description(gpointer instance_context)
{
	IrTransPlugin *plugin = instance_context;
	IRRECO_ENTER

	IRRECO_RETURN_PTR(g_strdup(plugin->description));
}

/*
 * Create a new device which can be controlled.
 *
 * Returns: IRRECO_BACKEND_OK or error code.
 */
IrrecoBackendStatus irtrans_create_device(gpointer instance_context,
							 GtkWindow * parent)
{
	IrrecoBackendStatus status;
	IrTransPlugin *plugin = instance_context;
	IRRECO_ENTER

	status = irtrans_wrap_connect(plugin->irtrans_wrap);
	if (status != IRRECO_BACKEND_OK) IRRECO_RETURN_INT(status);

	/* enter learn dialog to create new device */
	irtrans_remote_learn_dlg(plugin, parent);

	irtrans_wrap_disconnect(plugin->irtrans_wrap);
	IRRECO_RETURN_ENUM(IRRECO_BACKEND_OK);
}

/*
 * Can the device be edited? Irreco uses this to determinate if the device
 * can be edited. This is usefull mainly if the backend supports both editable
 * and uneditable devices.
 */
gboolean irtrans_is_device_editable(gpointer instance_context,
					  const gchar * device_name,
					  gpointer device_contex)
{
	IRRECO_ENTER
	IRRECO_RETURN_BOOL(TRUE);
}

/*
 * Edit currently existing device. Basically this means that the user should
 * be able to add, remove and edit commands the device has.
 */
IrrecoBackendStatus irtrans_edit_device(gpointer instance_context,
					      const gchar * device_name,
					      gpointer device_contex,
					      GtkWindow * parent)
{
	IrrecoBackendStatus status;
	IrTransPlugin *plugin = instance_context;
	IRRECO_ENTER

	status = irtrans_wrap_connect(plugin->irtrans_wrap);
	if (status != IRRECO_BACKEND_OK) IRRECO_RETURN_INT(status);

	/* enter command dialog to learn commands */
	g_free(plugin->remote);
	plugin->remote = g_strdup(device_name);
	irtrans_command_dlg(plugin, parent);

	irtrans_wrap_disconnect(plugin->irtrans_wrap);
	IRRECO_RETURN_ENUM(IRRECO_BACKEND_OK);
}

IrrecoBackendStatus irtrans_delete_device(gpointer instance_context,
					        const gchar * device_name,
						gpointer device_contex,
						GtkWindow * parent)
{
	IrTransPlugin *plugin = instance_context;
	GtkWidget* dialog = NULL;
	IRRECO_ENTER

	dialog = gtk_message_dialog_new(parent,
		GTK_DIALOG_DESTROY_WITH_PARENT,
		GTK_MESSAGE_INFO, GTK_BUTTONS_OK,
		_("IRTrans does not support deleting devices."));
	gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);

	irtrans_wrap_disconnect(plugin->irtrans_wrap);
	IRRECO_RETURN_ENUM(IRRECO_BACKEND_OK);
}

IrrecoBackendStatus irtrans_export_conf(gpointer instance_context,
				const char *device_name,
				IrrecoBackendFileContainer **file_container)
{
	IrrecoBackendStatus status = IRRECO_BACKEND_OK;
	gchar		*file_data = NULL;
	gboolean 	success    = TRUE;
	GString 	*file_name = g_string_new("");
	GString 	*file_path = g_string_new(IRTRANS_REMOTES_DIR);
	IRRECO_ENTER

	g_string_append_printf(file_name, "%s.rem", device_name);
	g_string_append_printf(file_path, "/%s", file_name->str);
	success = g_file_get_contents(file_path->str, &file_data, NULL, NULL);
	if (success == FALSE) {
		status = IRTRANS_FILE_OPEN_ERROR;
		goto clean;
	}

	*file_container = irreco_backend_file_container_new();
	irreco_backend_file_container_set(*file_container,
					  "IRTrans Transceiver",/*backend*/
					  NULL,			/*category*/
    					  NULL,			/*manufacturer*/
    					  device_name,		/*model*/
    					  file_name->str,	/*name*/
       					  file_data);		/*data*/
	clean:
	g_free(file_data);
	g_string_free(file_name, TRUE);
	g_string_free(file_path, TRUE);

	IRRECO_RETURN_ENUM(status);
}

IrrecoBackendStatus irtrans_import_conf(gpointer instance_context,
				IrrecoBackendFileContainer *file_container)
{
	gssize length;
	gboolean success;
	GString *file_path = g_string_new(IRTRANS_REMOTES_DIR);
	IRRECO_ENTER
	length = strlen(file_container->data->str);
	g_string_append_printf(file_path, "/%s", file_container->name->str);


	/* irreco_write_file from irreco_util.h */
	success = irreco_write_file(file_path->str,
				    file_container->data->str, length);

	g_string_free(file_path, TRUE);

	if(!success){

		IRRECO_RETURN_ENUM(IRTRANS_BACKEND_SAVE_ERROR);
	}

	IRRECO_RETURN_ENUM(IRRECO_BACKEND_OK);
}

 IrrecoBackendStatus irtrans_check_conf(
 				gpointer instance_context,
 				IrrecoBackendFileContainer *file_container,
				gboolean *configuration)
{
	GString *file_path = g_string_new(IRTRANS_REMOTES_DIR);
	IRRECO_ENTER
	g_string_append_printf(file_path, "/%s", file_container->name->str);

	/* irreco_file_exists from irreco_util.h */
	*configuration = irreco_file_exists(file_path->str);

	g_string_free(file_path, TRUE);

	IRRECO_RETURN_ENUM(IRRECO_BACKEND_OK);
}
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Function table structure.                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

IrrecoBackendFunctionTable irtrans_plugin_function_table = {
	IRRECO_BACKEND_API_VERSION,	/* backend_api_version		*/
	IRRECO_BACKEND_EDITABLE_DEVICES |
	IRRECO_BACKEND_CONFIGURATION_EXPORT |
	IRRECO_BACKEND_MULTI_DEVICE_SUPPORT,/* flags 			*/
	"IRTrans Transceiver",		/* name				*/

	irtrans_plugin_get_error_msg,	/* get_error_msg 		*/
	irtrans_plugin_create,		/* create			*/
	irtrans_plugin_destroy,		/* destroy			*/
	irtrans_plugin_read_from_conf,	/* from_conf			*/
	irtrans_plugin_save_to_conf,	/* to_conf			*/
	irtrans_plugin_get_devices,	/* get_devices			*/
	irtrans_plugin_get_commands,	/* get_commands			*/
	irtrans_plugin_send_command,	/* send_command			*/
	irtrans_plugin_configure,	/* configure			*/

	irtrans_get_description,	/* get_description,    optional	*/
	irtrans_create_device,		/* create_device,      optional	*/
	irtrans_is_device_editable,	/* is_device_editable, optional	*/
	irtrans_edit_device,		/* edit_device,        optional	*/
	irtrans_delete_device,		/* delete_device,      optional	*/

  	irtrans_export_conf,		/*export_conf,	       optional */
	irtrans_import_conf,		/*import_conf,	       optional */
 	irtrans_check_conf,		/*check_conf,	       optional */

	NULL				/* reserved */
};

IrrecoBackendFunctionTable *get_irreco_backend_function_table()
{
	IRRECO_ENTER
	IRRECO_RETURN_PTR(&irtrans_plugin_function_table);
}



/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "irtrans_wrap.h"


/*
 * IRTransWrap
 *
 * - Object Oriented wrapper for IRTrans shlib API.
 * - Can automatically start and stop local irserver when needed.
 * - Will retry all commands util timeout.
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

IRTransWrap *irtrans_wrap_new()
{
	IRTransWrap *self;
	IRRECO_ENTER

	self = g_slice_new0(IRTransWrap);
	self->server_socket = -1;
	self->hostname = g_string_new(NULL);
	self->loop = irreco_retry_loop_new(IRTRANS_WAIT_SLEEP,
					   IRTRANS_WAIT_MAX);
	IRRECO_RETURN_PTR(self);
}

void irtrans_wrap_delete(IRTransWrap *self)
{
	IRRECO_ENTER
	irreco_retry_loop_free(self->loop);
	if (self->list_array) g_ptr_array_free(self->list_array, TRUE);
	if (self->list_chunk) g_string_chunk_free(self->list_chunk);
	g_string_free(self->hostname, TRUE);
	g_slice_free(IRTransWrap, self);
	IRRECO_RETURN
}



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Private Functions                                                          */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/*
 * Check if irserver is using the given process identifier.
 */
static gboolean irtrans_wrap_is_irserver_running_with_pid(pid_t pid)
{
	gboolean is_irserver;
	GError *error = NULL;
	GString *link_path = NULL;
	gchar *real_path = NULL;
	IRRECO_ENTER

	/* Location of the link which should point to the executable
	   using the pid. */
	link_path = g_string_new(NULL);
	g_string_printf(link_path, "/proc/%i/exe", pid);
	IRRECO_DEBUG("Link location \"%s\".\n", link_path->str);

	/* Read link. */
	real_path = g_file_read_link(link_path->str, &error);
	if (irreco_gerror_check_free(&error)) {
		g_string_free(link_path, TRUE);
		g_free(real_path);
		IRRECO_RETURN_BOOL(FALSE);
	}
	IRRECO_DEBUG("Executable location \"%s\".\n", real_path);

	/* Does it point to irserver. */
	is_irserver = g_str_has_suffix(real_path, "/irserver");
	g_string_free(link_path, TRUE);
	g_free(real_path);
	IRRECO_RETURN_BOOL(is_irserver);
}

/*
 * Check if pidfile exists, and that the pid is used by irserver.
 */
static gboolean irtrans_wrap_is_irserver_running(IRTransWrap *self,
						 pid_t *pid)
{
	gint pid_int;
	gchar pid_str[80];
	IRRECO_ENTER

	/* Attempt to read the pidfile. */
	if (!irreco_read_line(IRTRANS_PIDFILE, pid_str, sizeof(pid_str))) {
		IRRECO_DEBUG("Could not read pidfile.\n");
		IRRECO_RETURN_BOOL(FALSE);
	}

	/* Attempt to convert the pid to int. */
	if (!sscanf(pid_str, "%i", &pid_int)) {
		IRRECO_DEBUG("Pidfile did not contain a valid pid.\n");
		g_unlink(IRTRANS_PIDFILE);
		IRRECO_RETURN_BOOL(FALSE);
	}

	/* Is the pid inside the pidfile a valid one? */
	if (!irtrans_wrap_is_irserver_running_with_pid(pid_int)) {
		IRRECO_DEBUG("irserver is no longer using pid \"%i\".\n",
			     pid_int);
		g_unlink(IRTRANS_PIDFILE);
		IRRECO_RETURN_BOOL(FALSE);
	}

	IRRECO_DEBUG("irserver is using pid \"%i\".\n", pid_int);
	if (pid) *pid = pid_int;
	IRRECO_RETURN_BOOL(TRUE);
}

/*
 * Start local irserver.
 */
static IrrecoBackendStatus
irtrans_wrap_start_server(IRTransWrap *self, const gchar *hostname)
{
	gint rvalue;
	GString *cmdline;
	IRRECO_ENTER

	/* Local server is already running. */
	if (irtrans_wrap_is_irserver_running(self, NULL)) {
		IRRECO_RETURN_ENUM(IRRECO_BACKEND_OK);
	}

	/* Migrate to new directory structure from old versions. */
	if(irreco_is_dir(IRTRANS_OLD_REMOTES_DIR) == TRUE &&
	   irreco_is_dir(IRTRANS_REMOTES_DIR) == FALSE) {
		IRRECO_PRINTF("Renaming remotes directory from \"%s\" "
			      "to \"%s\"\n", IRTRANS_OLD_REMOTES_DIR,
			      IRTRANS_REMOTES_DIR);
		g_rename(IRTRANS_OLD_REMOTES_DIR, IRTRANS_REMOTES_DIR);
	}

	/* Create directory structure needed for IRTrans. */
	if(irreco_is_dir(IRTRANS_HOME_DIR) == FALSE) {
		if (g_mkdir(IRTRANS_HOME_DIR, 0700) != 0) {
			IRRECO_RETURN_ENUM(IRTRANS_HOME_DIR_MISSING);
		}
	}
	if(irreco_is_dir(IRTRANS_REMOTES_DIR) == FALSE) {
		if (g_mkdir(IRTRANS_REMOTES_DIR, 0700) != 0) {
			IRRECO_RETURN_ENUM(IRTRANS_REMOTES_DIR_MISSING);
		}
	}
	if(irreco_is_dir(IRTRANS_REMOTES_SYMLINK) == FALSE) {
		if (symlink(IRTRANS_REMOTES_DIR,
			    IRTRANS_REMOTES_SYMLINK) != 0) {
			IRRECO_RETURN_ENUM(IRTRANS_SYMLINK_REMOTES_FAILED);
		}
	}

	/* change working directory */
	if (chdir(IRTRANS_HOME_DIR) != 0) {
		IRRECO_RETURN_ENUM(IRTRANS_CHDIR_HOME_FAILED);
	}

	/* Check if irserver exists. */
	if (system("which irserver > /dev/null") != 0) {
		IRRECO_RETURN_ENUM(IRTRANS_IRSERVER_MISSING);
	}

	/* Start irserver. */
	cmdline = g_string_new(NULL);
	g_string_printf(cmdline, "irserver -pidfile %s -no_lirc %s &",
			IRTRANS_PIDFILE, hostname);
	IRRECO_PRINTF("Executing: %s\n", cmdline->str);
	rvalue = system(cmdline->str);
	g_string_free(cmdline, TRUE);

	/* To old working dir. */
	chdir(getenv("OLDPWD"));

	if(rvalue != 0) {
		IRRECO_PRINTF("Failed to start irserver.\n");
		IRRECO_RETURN_ENUM(IRTRANS_START_FAILURE);
	}

	IRRECO_RETURN_ENUM(IRRECO_BACKEND_OK);
}

/*
 * Terminates local irserver process.
 */
static void irtrans_wrap_stop_server(IRTransWrap *self)
{
	pid_t pid;
	IRRECO_ENTER

	if (irtrans_wrap_is_irserver_running(self, &pid)) {

		/* It is, so kill irserver. */
		IRRECO_PRINTF("Killing irserver with pid \"%i\"\n", pid);
		if (kill(pid, 2) != 0) {
			IRRECO_ERROR("Failed to kill irserver.\n");
			IRRECO_RETURN
		}

		/* Loop until irserver dies. */
		while (irtrans_wrap_is_irserver_running_with_pid(pid)) {
			g_usleep(IRTRANS_WAIT_SLEEP);
		}
		g_usleep(IRTRANS_WAIT_SLEEP);
	}
	IRRECO_RETURN
}

static void irtrans_wrap_append_to_list(IRTransWrap *self, const gchar *string)
{
	gchar* chuck_str;
	IRRECO_ENTER

	IRRECO_PRINTF("Appending to list \"%s\".\n", string);
	chuck_str = g_string_chunk_insert(self->list_chunk, string);
	g_ptr_array_add(self->list_array, chuck_str);
	IRRECO_RETURN
}



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Functions                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/*
 * Depending on remote_server setting, opens connection to either local or
 * remote server.
 */
IrrecoBackendStatus irtrans_wrap_connect(IRTransWrap *self)
{
	IRRECO_ENTER

	if (irreco_str_isempty(self->hostname->str)) {
		IRRECO_RETURN_ENUM(IRTRANS_HOST_FAILURE);
	}

	if(self->remote_server) {
		IRRECO_RETURN_INT(irtrans_wrap_connect_to_remote(self));
	} else {
		IRRECO_RETURN_INT(irtrans_wrap_connect_to_local(self));
	}
}

/*
 * Start local irserver and connect to it.
 */
IrrecoBackendStatus irtrans_wrap_connect_to_local(IRTransWrap *self)
{
	IrrecoBackendStatus status;
	IRRECO_ENTER

	/* Are we already connected to something? */
	if (self->server_socket != -1) {
		if (irtrans_wrap_is_irserver_running(self, NULL)) {
			IRRECO_PRINTF("Connection is already open to local "
				      "server.\n");
			IRRECO_RETURN_ENUM(IRRECO_BACKEND_OK);
		} else {
			irtrans_wrap_disconnect(self);
		}
	}

	IRRECO_PRINTF("Connecting to local server.\n");
	IRRECO_RETRY_LOOP_START(self->loop)
		status = irtrans_wrap_start_server(self, self->hostname->str);
		if (status != IRRECO_BACKEND_OK) break;

		status = ConnectIRTransServer("127.0.0.1",
					      &self->server_socket);
		if (status == 0) break;
	IRRECO_RETRY_LOOP_END(self->loop)

	if (status != IRRECO_BACKEND_OK) irtrans_wrap_stop_server(self);
	IRRECO_RETURN_INT(status);
}

/*
 * Open connection to remote irserver.
 */
IrrecoBackendStatus irtrans_wrap_connect_to_remote(IRTransWrap *self)
{
	IrrecoBackendStatus status;
	IRRECO_ENTER

	/* Are we already connected to something? */
	if (self->server_socket != -1) {
		if (irtrans_wrap_is_irserver_running(self, NULL)) {
			irtrans_wrap_disconnect(self);
		} else {
			IRRECO_PRINTF("Connection is already open to remote "
				      "server.\n");
			IRRECO_RETURN_ENUM(IRRECO_BACKEND_OK);
		}
	}

	IRRECO_PRINTF("Connecting to remote server.\n");
	IRRECO_RETRY_LOOP_START(self->loop)
		status = ConnectIRTransServer(self->hostname->str,
					      &self->server_socket);
		if (status == 0) break;
	IRRECO_RETRY_LOOP_END(self->loop)
	IRRECO_RETURN_INT(status);
}

void irtrans_wrap_disconnect(IRTransWrap *self)
{
	IRRECO_ENTER

	if (self->server_socket != -1) {
		DisconnectIRTransServer(self->server_socket);
		self->server_socket = -1;
	} else {
		IRRECO_PRINTF("Connection is not open, doing nothing.\n");
	}
	irtrans_wrap_stop_server(self);
	IRRECO_RETURN
}

void irtrans_wrap_set_remote_server(IRTransWrap *self, gboolean remote_server)
{
	IRRECO_ENTER
	irtrans_wrap_disconnect(self);
	self->remote_server = remote_server;
	IRRECO_RETURN
}

gboolean irtrans_wrap_get_remote_server(IRTransWrap *self)
{
	IRRECO_ENTER
	IRRECO_RETURN_BOOL(self->remote_server);
}

void irtrans_wrap_set_hostname(IRTransWrap *self, const gchar *string)
{
	IRRECO_ENTER
	irtrans_wrap_disconnect(self);
	if (string == NULL) {
		g_string_assign(self->hostname, "");
	} else {
		g_string_assign(self->hostname, string);
	}
	IRRECO_RETURN
}

const gchar *irtrans_wrap_get_hostname(IRTransWrap *self)
{
	IRRECO_ENTER
	if (irreco_str_isempty(self->hostname->str)) {
		IRRECO_RETURN_STR(NULL);
	} else {
		IRRECO_RETURN_STR(self->hostname->str);
	}
}

void irtrans_wrap_clear_list(IRTransWrap *self)
{
	IRRECO_ENTER

	if (self->list_array) g_ptr_array_free(self->list_array, TRUE);
	if (self->list_chunk) g_string_chunk_free(self->list_chunk);

	self->list_chunk = g_string_chunk_new(0);
	self->list_array = g_ptr_array_new();
	self->list_get_pos = 0;
	IRRECO_RETURN
}

gboolean irtrans_wrap_get_from_list(IRTransWrap *self, const gchar **string)
{
	IRRECO_ENTER

	if (self->list_array != NULL &&
	    self->list_get_pos < self->list_array->len) {
		*string = (const gchar *) g_ptr_array_index(
			self->list_array, self->list_get_pos);
		IRRECO_PRINTF("Getting \"%s\" from list index \"%i\".\n",
			      *string, self->list_get_pos);
		self->list_get_pos = self->list_get_pos + 1;
		IRRECO_RETURN_BOOL(TRUE);
	} else {
		*string = NULL;
		IRRECO_RETURN_BOOL(FALSE);
	}
}

/*
 * Autodetect IRTrans modules, and add ip addresses to IRTransWrap string list.
 *
 * Args: count	Will receive number of modules detected, if set.
 */
IrrecoBackendStatus irtrans_wrap_get_autodetect_list(IRTransWrap *self,
						     gint *count)
{
	IrrecoBackendStatus status;
	NETWORKMODEEXN data;
	gint i, j;
	IRRECO_ENTER

	/* Prepare eviroment. */
	if (count) *count = 0;
	irtrans_wrap_disconnect(self);
	irtrans_wrap_clear_list(self);

	IRRECO_PRINTF("Starting local server for autodetection.\n");
	IRRECO_RETRY_LOOP_START(self->loop)
		status = irtrans_wrap_start_server(self, "lan");
		if (status != IRRECO_BACKEND_OK) break;

		status = ConnectIRTransServer("127.0.0.1",
					      &self->server_socket);
		if (status == 0) break;
	IRRECO_RETRY_LOOP_END(self->loop)

	if (status != IRRECO_BACKEND_OK) {
		irtrans_wrap_stop_server(self);
		IRRECO_RETURN_INT(status);
	}

	/* Nullify structure, just in case. */
	memset(&data, '\0', sizeof(data));

	/* Fetch list of IRTrans-modules. */
	status = GetDeviceStatusExN(self->server_socket, 0, &data);
	if (status != 0) {
		IRRECO_PRINTF("Failed to get list of irtrans modules.\n");
		irtrans_wrap_stop_server(self);
		IRRECO_RETURN_INT(status);
	}

	/* Remote2 field should be in format:
	       '@@@~~~lan~~~@@@ __MAC__ __IP__ __PADDING__'
	   Which should look like this:
	       '@@@~~~lan~~~@@@ 00-50-c2-52-78-f2 172.23.118.19          '
	*/

	for (i = 0; i < data.count; i++) {

		if(memcmp(data.stat[i][0].remote2, "@@@~~~lan~~~@@@", 15) == 0||
		   memcmp(data.stat[i][0].remote2, "@@@~~~LAN~~~@@@", 15) == 0){

			/* Get ip. */
			gchar buffer[81];
			memset(buffer, '\0', sizeof(buffer));
			memcpy(buffer, data.stat[i][0].remote2 + 34, 46);

			/* Replace padding spaces at the end with nulls. */
			for (j = sizeof(buffer) - 1; j-- > 0;) {
				if (buffer[j] == ' ') buffer[j] = '\0';
			}

			/* Append to list. */
			if (count) *count = *count + 1;
			irtrans_wrap_append_to_list(self, buffer);
		}
	}

	irtrans_wrap_stop_server(self);
	IRRECO_RETURN_ENUM(IRRECO_BACKEND_OK);
}

/*
 * Get list of devices.
 *
 * Args: count	Will receive number of modules detected, if set.
 */
IrrecoBackendStatus irtrans_wrap_get_remote_list(IRTransWrap *self,
						 gint *count)
{
	IrrecoBackendStatus status;
	REMOTEBUFFER data;
	gint i, j;
	IRRECO_ENTER

	/* Prepare eviroment. */
	irtrans_wrap_clear_list(self);
	if (count) *count = 0;

	/* Nullify structure, just in case. */
	memset(&data, '\0', sizeof(data));

	/* Fetch list of devices. */
	IRRECO_RETRY_LOOP_START(self->loop)
		status = irtrans_wrap_connect(self);
		if (status != IRRECO_BACKEND_OK) break;
		status = GetRemotes(self->server_socket, 0, &data);
		if (status == 0) break;
		irtrans_wrap_disconnect(self);
	IRRECO_RETRY_LOOP_END(self->loop)
	if (status != 0) IRRECO_RETURN_INT(status);

	for (i = 0; i < data.count_buffer; i++) {

		/* Get device name. */
		gchar buffer[81];
		memcpy(buffer, data.remotes[i].name, sizeof(buffer) - 1);
		buffer[sizeof(buffer) - 1] = '\0';

		/* Replace padding spaces at the end with nulls. */
		for (j = sizeof(buffer) - 1; j-- > 0;) {
			if (buffer[j] == ' ') {
				buffer[j] = '\0';
			} else {
				break;
			}
		}

		/* Append to list. */
		if (count) *count = *count + 1;
		irtrans_wrap_append_to_list(self, buffer);
	}

	IRRECO_RETURN_ENUM(IRRECO_BACKEND_OK);
}

/*
 * Get list of commands a given device has.
 *
 * Args: device Get command from this device.
 *       count	Will receive number of modules detected, if set.
 */
IrrecoBackendStatus irtrans_wrap_get_command_list(IRTransWrap *self,
						  const gchar *device,
						  gint *count)
{
	IrrecoBackendStatus status;
	COMMANDBUFFER data;
	gint i, j;
	IRRECO_ENTER

	/* Prepare eviroment. */
	irtrans_wrap_clear_list(self);
	if (count) *count = 0;

	/* Nullify structure, just in case. */
	memset(&data, '\0', sizeof(data));

	/* Fetch list of devices. */
	IRRECO_RETRY_LOOP_START(self->loop)
		status = irtrans_wrap_connect(self);
		if (status != IRRECO_BACKEND_OK) break;
		status = GetCommands(self->server_socket, (gchar *) device,
				     0, &data);
		if (status == 0) break;
		irtrans_wrap_disconnect(self);
	IRRECO_RETRY_LOOP_END(self->loop)
	if (status != 0) IRRECO_RETURN_INT(status);

	for (i = 0; i < data.count_buffer; i++) {

		/* Get device name. */
		gchar buffer[21];
		memcpy(buffer, data.commands[i], sizeof(buffer) - 1);
		buffer[sizeof(buffer) - 1] = '\0';

		/* Replace padding spaces at the end with nulls. */
		for (j = sizeof(buffer) - 1; j-- > 0;) {
			if (buffer[j] == ' ') {
				buffer[j] = '\0';
			} else {
				break;
			}
		}

		/* Append to list. */
		if (count) *count = *count + 1;
		irtrans_wrap_append_to_list(self, buffer);
	}

	IRRECO_RETURN_ENUM(IRRECO_BACKEND_OK);
}

/*
 * Send command.
 */
IrrecoBackendStatus irtrans_wrap_send_command(IRTransWrap *self,
					      const gchar *device,
					      const gchar *command)
{
	IrrecoBackendStatus status;
	NETWORKSTATUS *net_status = NULL;
	IRRECO_ENTER

	/* Send command. */
	IRRECO_RETRY_LOOP_START(self->loop)
		status = irtrans_wrap_connect(self);
		if (status != IRRECO_BACKEND_OK) break;
		net_status = SendRemoteCommand(self->server_socket,
					       (gchar *) device,
					       (gchar *) command,
					       0, 0, 0);
		if (net_status == NULL) break;
		irtrans_wrap_disconnect(self);
	IRRECO_RETRY_LOOP_END(self->loop)
	if (status != 0) IRRECO_RETURN_INT(status);

	if (net_status == NULL) {
		IRRECO_RETURN_ENUM(IRRECO_BACKEND_OK);
	} else {
		IRRECO_RETURN_ENUM(IRTRANS_SEND_FAILURE);
	}
}

/*
 * Learn command.
 */
IrrecoBackendStatus irtrans_wrap_learn_command(IRTransWrap *self,
					       const gchar *device,
					       const gchar *command,
					       gushort timeout)
{
	IrrecoBackendStatus status;
	NETWORKSTATUS *net_status;
	IRRECO_ENTER

	status = irtrans_wrap_connect(self);
	if (status != IRRECO_BACKEND_OK) IRRECO_RETURN_INT(status);

	/* Learn command. */
	net_status = LearnIRCode(self->server_socket,
				 (gchar *) device,
				 (gchar *) command,
				 timeout);

	if (net_status == NULL) {
		IRRECO_PRINTF("Learned IR code for command \"%s\" of "
			      "device \"%s\".\n", command, device);
		IRRECO_RETURN_ENUM(IRRECO_BACKEND_OK);
	} else {
		IRRECO_PRINTF("Failed to learn IR code for command \"%s\" of  "
			      "device \"%s\".\n", command, device);
		IRRECO_RETURN_ENUM(IRTRANS_LEARN_FAILURE);
	}
}



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Events and Callbacks                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/




































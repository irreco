/*
 Irtrans_config_dlg - part of Ir Remote Control for N800
 Copyright (C) 2007 Jussi Pyykkö (jupyykko@netti.fi)

 This is based on the irreco_button_dlg by Arto Karppinen
 							(arto.karppinen@iki.fi)

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "irtrans_remote_dlg.h"
#include "irtrans_command_dlg.h"

static IrTransRemoteDlg *irtrans_remote_dlg_create(void)
{
	IRRECO_ENTER
	IRRECO_RETURN_PTR(g_slice_new0(IrTransRemoteDlg));
}

static void irtrans_remote_dlg_destroy(IrTransRemoteDlg * remote_dialog)
{
	IRRECO_ENTER

	g_slice_free(IrTransRemoteDlg, remote_dialog);

	IRRECO_RETURN
}

/*
 * Sends remote name to the irserver which creates config-file (name.rem)
 * into the remotes folder.
 */
static gboolean irtrans_set_device_name(IrTransPlugin * plugin)
{
	IRRECO_ENTER

	/* This command will return error code, but it will create a new
	   remote regardless of that. */
	irtrans_wrap_learn_command(plugin->irtrans_wrap, plugin->remote, "", 0);
  	IRRECO_RETURN_BOOL(TRUE);

	/*
	IrrecoBackendStatus status;
	IRRECO_ENTER

	status = irtrans_wrap_learn_command(plugin->irtrans_wrap,
					    plugin->remote, "", 0);
	if (status != IRRECO_BACKEND_OK) IRRECO_RETURN_BOOL(FALSE);
  	IRRECO_RETURN_BOOL(TRUE);
	*/
}

static gboolean irtrans_load_device_list(IrTransRemoteDlg * remote_dialog)
{
	GtkTreeIter iter;
	const gchar *remote;
	IrrecoBackendStatus status;
  	IRRECO_ENTER

	status = irtrans_wrap_get_remote_list(
		remote_dialog->plugin->irtrans_wrap, NULL);
	if (status != IRRECO_BACKEND_OK) IRRECO_RETURN_BOOL(FALSE);

	while (irtrans_wrap_get_from_list(
			remote_dialog->plugin->irtrans_wrap, &remote)) {
		gtk_tree_store_append(remote_dialog->remote_treestore,
				      &iter, NULL);
		gtk_tree_store_set(remote_dialog->remote_treestore,
				   &iter, REMOTE_COLUMN, remote, -1);
	}

	IRRECO_RETURN_BOOL(TRUE);
}

/*
 * Get chosen device name from the list pointed by selection_index.
 */
static gchar *get_remote_iter(GtkWidget *treeview, gint selection_index)
{
	GtkTreeIter iter;
	GtkTreeModel *model = NULL;
	GtkTreePath *path = NULL;
	gchar *str_data = NULL;

	IRRECO_ENTER

	model = gtk_tree_view_get_model(GTK_TREE_VIEW(treeview));
	path = gtk_tree_path_new_from_indices(selection_index, -1);
	gtk_tree_model_get_iter(model, &iter, path);
	gtk_tree_model_get(model, &iter, REMOTE_COLUMN, &str_data, -1);
        gtk_tree_path_free(path);

	IRRECO_RETURN_STR(str_data);
}

static void irtrans_remote_list_selection_changed(GtkTreeSelection * selection,
				      		IrTransRemoteDlg * remote_dialog)
{
	GtkTreeIter iter;
        GtkTreeModel *model = NULL;
	GtkTreePath *path = NULL;
	gint *path_indices = NULL;

	IRRECO_ENTER

	if(gtk_tree_selection_get_selected(selection, &model, &iter)){

		path = gtk_tree_model_get_path(model, &iter);
		path_indices = gtk_tree_path_get_indices(path);
		remote_dialog->remote_selection_index = path_indices[0];
		gtk_tree_path_free(path);
	} else {
		remote_dialog->remote_selection_index = -1;
	}

	IRRECO_RETURN
}

static gboolean irtrans_create_remote_list(IrTransRemoteDlg * remote_dialog)
{
	GtkTreeSelection *select = NULL;
	GtkTreeViewColumn *column = NULL;
	GtkCellRenderer *renderer = NULL;
	gboolean success;

  	IRRECO_ENTER

  	/* if former widget exists destroy it */
  	if(remote_dialog->remote_treeview != NULL){

		gtk_widget_destroy(remote_dialog->remote_treeview);
	}

  	/* create GtkTreeStore and GtkTreeView */
	remote_dialog->remote_treestore = gtk_tree_store_new(NR_COLUMNS,
								G_TYPE_STRING);
	/* fill the treestore with data */
 	success = irtrans_load_device_list(remote_dialog);

 	if (success){

 		remote_dialog->remote_treeview = gtk_tree_view_new_with_model(
								GTK_TREE_MODEL(
								remote_dialog->
								remote_treestore));
		g_object_unref(G_OBJECT(remote_dialog->remote_treestore));

		/* setup column */
		renderer = gtk_cell_renderer_text_new();
		column = gtk_tree_view_column_new_with_attributes(NULL,
								renderer,
								"text",
								0, NULL);
		gtk_tree_view_append_column(GTK_TREE_VIEW(remote_dialog->
								remote_treeview),
				    				column);
		/* set selection callback */
		select = gtk_tree_view_get_selection(GTK_TREE_VIEW(
								remote_dialog->
								remote_treeview));
		gtk_tree_selection_set_mode(select, GTK_SELECTION_SINGLE);
		g_signal_connect(G_OBJECT(select), "changed", G_CALLBACK(
					irtrans_remote_list_selection_changed),
			 					remote_dialog);
		IRRECO_RETURN_BOOL(TRUE);
	} else {
		IRRECO_RETURN_BOOL(FALSE);
	}
}

	/****************************************************************/
	/*								*/
	/*								*/
	/*			   DIALOGS				*/
	/*								*/
	/*								*/
	/****************************************************************/

gboolean irtrans_remote_learn_dlg(IrTransPlugin * plugin, GtkWindow * parent)
{
	GtkWidget *dialog = NULL;
	GtkWidget *table = NULL;
	GtkWidget *remote_entry = NULL;
	gchar *device_buffer = NULL;
	gint rvalue = -1;
	gboolean success;

	IRRECO_ENTER

	/* create objects. */
	dialog = gtk_dialog_new_with_buttons(
		_("IRTrans device learn dialog"), parent,
		GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT |
		GTK_DIALOG_NO_SEPARATOR,
		GTK_STOCK_OK, GTK_RESPONSE_ACCEPT,
		GTK_STOCK_CANCEL, GTK_RESPONSE_REJECT,
		NULL);
	table = gtk_table_new(2, 2, FALSE);
	remote_entry = gtk_entry_new();

	/* build dialog. */
	gtk_table_attach_defaults(GTK_TABLE(table),
			gtk_label_new(_("Insert device name: ")), 0, 1, 0, 1);
	gtk_table_attach_defaults(GTK_TABLE(table), remote_entry, 1, 2, 0, 1);
	gtk_table_set_row_spacings(GTK_TABLE(table), 5);
        gtk_table_set_col_spacings(GTK_TABLE(table), 5);
        gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->vbox), table);
	gtk_widget_show_all(dialog);

	while(rvalue == -1){
		switch(gtk_dialog_run(GTK_DIALOG(dialog))){

			case GTK_RESPONSE_REJECT:

				rvalue = FALSE;
				break;

			case GTK_RESPONSE_ACCEPT:

				/* get device name from the entry */
				device_buffer = g_strdup(gtk_entry_get_text(
								GTK_ENTRY(
								remote_entry)));

				if(g_utf8_strlen(device_buffer, -1) < 1){

					irreco_info_dlg(parent,
						_("Insert valid devicename!"));
					g_free(device_buffer);
					break;
				} else {
					/* convert all characters to lowercase */
					plugin->remote = g_utf8_strdown(
								device_buffer,
								strlen(
								device_buffer));
					g_free(device_buffer);

					/* learn remotename */
					success = irtrans_set_device_name(
									plugin);

					if(!success){

						irreco_error_dlg(parent,
						_("Remote learn failed!"));
						break;
					}
					rvalue = TRUE;
					break;
				}
		}
	}

	gtk_widget_destroy(dialog);
	IRRECO_RETURN_BOOL(rvalue);
}

gboolean irtrans_remote_dlg(IrTransPlugin * plugin, GtkWindow * parent)
{
	IrTransRemoteDlg *remote_dialog;

	GtkWidget *dialog = NULL;
	GtkWidget *sw = NULL;
	gint rvalue = -1;
	gboolean success;

	IRRECO_ENTER

	remote_dialog = irtrans_remote_dlg_create();
	remote_dialog->plugin = plugin;
	remote_dialog->remote_treeview = NULL;

	/* create objects. */
	dialog = gtk_dialog_new_with_buttons(_("Devices"), parent,
		GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT |
		GTK_DIALOG_NO_SEPARATOR,
		_("New"), IRTRANS_NEW_DEV,
		_("Edit"), IRTRANS_EDIT_DEV,
		_("Delete"), IRTRANS_DEL_DEV,
		_("Done"), GTK_RESPONSE_ACCEPT, NULL);
	sw = gtk_scrolled_window_new (NULL, NULL);
      	gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(sw),
      							GTK_SHADOW_ETCHED_IN);
      	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(sw),
      							GTK_POLICY_NEVER,
      							GTK_POLICY_AUTOMATIC);

      	success = irtrans_create_remote_list(remote_dialog);

      	/* build dialog */
      	if (success){

      		gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(sw),
      							remote_dialog->
      							remote_treeview);
      	} else {
		irreco_info_dlg(parent, _("Unable to load devices list!\n"));

		/* exit from the dialog */
		rvalue = TRUE;
	}

      	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->vbox), sw);
      	gtk_widget_show_all(dialog);

	while(rvalue == -1){
		switch(gtk_dialog_run(GTK_DIALOG(dialog))){

			case GTK_RESPONSE_ACCEPT:

				rvalue = TRUE;
				break;

			case IRTRANS_NEW_DEV:

				/* learn dialog for new device */
				irtrans_remote_learn_dlg(plugin, parent);

				/* refresh devicelist */
				success = irtrans_create_remote_list(
								remote_dialog);

      				if (success){

      					gtk_scrolled_window_add_with_viewport(
      							GTK_SCROLLED_WINDOW(sw),
      								remote_dialog->
      								remote_treeview);

      					gtk_widget_show_all(dialog);
      				} else {
      					irreco_info_dlg(parent,
      					_("Unable to load devices list!\n"));
      				}
				break;

			case IRTRANS_EDIT_DEV:

				/* get chosen device */
				plugin->remote = g_strdup(
							get_remote_iter(
							remote_dialog->
							remote_treeview,
							remote_dialog->
							remote_selection_index));

				if(plugin->remote != NULL){

					/* enter editing remote file */
					irtrans_command_dlg(plugin, parent);

				} else {
					irreco_info_dlg(parent,
							_("No editable devices!!"));
					break;
				}
				break;

			case IRTRANS_DEL_DEV:

				irreco_info_dlg(parent, _("IRTrans does not "
						"support deleting devices."));
				break;
		}
	}

	irtrans_remote_dlg_destroy(remote_dialog);
	gtk_widget_destroy(dialog);
	IRRECO_RETURN_BOOL(rvalue);
}



/*
 Irtrans_config_dlg - part of Ir Remote Control for N800
 Copyright (C) 2007 Jussi Pyykkö (jupyykko@netti.fi)

 This is based on the irreco_button_dlg by Arto Karppinen
 							(arto.karppinen@iki.fi)

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __IRTRANS_COMMAND_DLG_H_TYPEDEF__
#define __IRTRANS_COMMAND_DLG_H_TYPEDEF__

typedef struct _IrTransCommandDlg IrTransCommandDlg;

#define IRTRANS_NEW_CMD			1
#define IRTRANS_RELEARN_CMD		2
#define IRTRANS_DEL_CMD			3
#define IRTRANS_LEARN_CMD		4
#define IRTRANS_LEARN_TIMEOUT		IRRECO_SECONDS_TO_MSEC(10)
/*#define TIMEOUT				10000*/

#endif /* __IRTRANS_COMMAND_H_TYPEDEF__ */

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

#ifndef __IRTRANS_COMMAND_DLG_H__
#define __IRTRANS_COMMAND_DLG_H__

#include "irtrans_plugin.h"

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

struct _IrTransCommandDlg{

	IrTransPlugin *plugin;

	HildonBanner * hildon_banner;

      	/* widgets for loading the commands list */
	GtkTreeStore *command_treestore;
	GtkWidget *command_treeview;
	gint command_selection_index;

      	/* widgets for command dialogs */
      	GtkWidget *relearn_dialog;
      	GtkWidget *learn_dialog;
};

enum
{
	COMMAND_COLUMN,
  	NRO_COLUMNS
};

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

gboolean irtrans_command_dlg(IrTransPlugin * plugin, GtkWindow * parent);

#endif /* __IRTRANS_COMMAND_DLG_H__ */



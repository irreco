/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Typedef                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*
 * Make sure that typedefs are available before we include anything elese.
 *
 * This makes sure that whatever other structures that depend on structures
 * defined in this file will compile OK recardles of header inclusion order.
 */
#ifndef __HEADER_NAME_H_TYPEDEF__
#define __HEADER_NAME_H_TYPEDEF__
typedef struct _IRTransWrap IRTransWrap;
#endif /* __HEADER_NAME_H_TYPEDEF__ */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#ifndef __HEADER_NAME_H__
#define __HEADER_NAME_H__
#include "irtrans_plugin.h"
#include <irreco_retry_loop.h>



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Datatypes                                                                  */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
struct _IRTransWrap {
	SOCKET		 server_socket;
	gboolean	 remote_server;
	GString		*hostname;

	/* Because IRTrans does not return the result of queries in any
	   kind of format GLib / Gtk can use directly, we create our own list
	   and copy the data there. */
	GStringChunk	*list_chunk;
	GPtrArray	*list_array;
	gint		list_get_pos;

	IrrecoRetryLoop *loop;
};



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#define IRTRANS_HOME_DIR	"/home/user/.irserver"
#define IRTRANS_REMOTES_DIR	"/home/user/MyDocs/IRTrans Devices"
#define IRTRANS_OLD_REMOTES_DIR	"/home/user/MyDocs/remotes"
#define IRTRANS_REMOTES_SYMLINK IRTRANS_HOME_DIR "/remotes"
#define IRTRANS_PIDFILE		IRTRANS_HOME_DIR "/pid"
#define IRTRANS_WAIT_MAX	IRRECO_SECONDS_TO_USEC(3)
#define IRTRANS_WAIT_SLEEP	IRRECO_SECONDS_TO_USEC(0.1)



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
IRTransWrap *irtrans_wrap_new();
void irtrans_wrap_delete(IRTransWrap *self);
IrrecoBackendStatus irtrans_wrap_connect(IRTransWrap *self);
IrrecoBackendStatus irtrans_wrap_connect_to_local(IRTransWrap *self);
IrrecoBackendStatus irtrans_wrap_connect_to_remote(IRTransWrap *self);
void irtrans_wrap_disconnect(IRTransWrap *self);
void irtrans_wrap_set_remote_server(IRTransWrap *self, gboolean remote_server);
gboolean irtrans_wrap_get_remote_server(IRTransWrap *self);
void irtrans_wrap_set_hostname(IRTransWrap *self, const gchar *string);
const gchar *irtrans_wrap_get_hostname(IRTransWrap *self);
IrrecoBackendStatus irtrans_wrap_get_autodetect_list(IRTransWrap *self, gint *count);
void irtrans_wrap_clear_list(IRTransWrap *self);
gboolean irtrans_wrap_get_from_list(IRTransWrap *self, const gchar **string);
IrrecoBackendStatus irtrans_wrap_get_remote_list(IRTransWrap *self,
						 gint *count);
IrrecoBackendStatus irtrans_wrap_get_command_list(IRTransWrap *self,
						  const gchar *device,
						  gint *count);
IrrecoBackendStatus irtrans_wrap_send_command(IRTransWrap *self,
					      const gchar *device,
					      const gchar *command);
IrrecoBackendStatus irtrans_wrap_learn_command(IRTransWrap *self,
					       const gchar *device,
					       const gchar *command,
					       gushort timeout);

#endif /* __HEADER_NAME_H__ */



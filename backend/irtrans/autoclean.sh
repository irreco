#!/bin/sh
cd `dirname "$0"`

# Remove stuff created by autogen

rm -fv aclocal.m4
rm -fv autom4te.cache
rm -fv compile
rm -fv config.guess
rm -fv config.h
rm -fv config.h.in
rm -fv config.log
rm -fv config.status
rm -fv config.sub
rm -fv configure
rm -fv depcomp
rm -fv .exit_code
rm -fv install-sh
rm -fv intltool-extract
rm -fv intltool-extract.in
rm -fv intltool-merge
rm -fv intltool-merge.in
rm -fv intltool-update
rm -fv intltool-update.in
rm -fv libtool
rm -fv ltmain.sh
rm -fv Makefile
rm -fv Makefile.in
rm -fv make_log
rm -fv missing
rm -fv po/Makefile
rm -fv po/Makefile.in
rm -fv po/Makefile.in.in
rm -fv po/POTFILES
rm -fv po/POTFILES.in
rm -fv po/stamp-it
rm -fv src/.deps
rm -fv src/.libs
rm -fv src/Makefile
rm -fv src/Makefile.in
rm -fv stamp-h1
rm -fv tmp
rm -rfv autom4te.cache
echo done


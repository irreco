/*
 * Irreco Telnet backend
 * Copyright (C) 2008 Sampo Savola (samposav@paju.oulu.fi)
 * This backend has some parts from Jami Pekkanen's original mythtv backend
 * (jami.pekkanen at tkk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#define IRRECO_DEBUG_PREFIX "TELNET"
#include <irreco_util.h>
#include <irreco_backend_api.h>
#include <unistd.h>
#include <malloc.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <gtk/gtk.h>
#include <glib.h>
#include "config.h"
#include <fcntl.h>
#include <hildon/hildon-number-editor.h>

/* This is used elsewhere too */
#define _(String) (String)

typedef struct
{
	GString *host;
	gint port;
	GIOChannel *con;
	IrrecoKeyFile *keyfile;
	gchar** keys;
	gsize length_keys;
	gsize length_groups;
	gchar** groups;
	gint type;
	char* device;
	GtkTreeModel *model;
	GtkTreeIter iter;
	GtkListStore	 *store;
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;
	GtkWidget 	*treeview;

} TelnetBackend;

typedef enum
{
	TELNET_BACKEND_ERROR_CONFIG_READ = 1,
	TELNET_BACKEND_ERROR_CONFIG_WRITE,
	TELNET_BACKEND_ERROR_CONNECT,
	TELNET_BACKEND_ERROR_CON_WRITE,
	TELNET_BACKEND_ERROR_COMMANDS_READ,
 	EDIT_COMMANDS = 0,
  	REMOVE_COMMAND,
   	ADD_COMMAND,
	ADD_NEW = 0
} TelnetBackendError;
enum
{
 COL_NAME,
 COL_CMD,
 NUM_COLS = 2

} ;

void telnet_backend_edit_commands(TelnetBackend *self, GtkWindow *parent);
void telnet_command_edited_callback(GtkCellRendererText *renderer, gchar
		 		    *path_string,gchar *new_text,
	 			    TelnetBackend *self);
void telnet_backend_add_device(TelnetBackend *self);
void remove_command(TelnetBackend *self);
void add_command(TelnetBackend *self);
void telnet_backend_store_commands(TelnetBackend *self);
const char *telnet_backend_get_error_msg(TelnetBackend *self,
				IrrecoBackendStatus code)
{
	switch(code)
	{
		case TELNET_BACKEND_ERROR_CONFIG_READ:
			return _("Couldn't read configuration");
		case TELNET_BACKEND_ERROR_CONFIG_WRITE:
			return _("Couldn't write configuration");
		case TELNET_BACKEND_ERROR_CONNECT:
			return _("Couldn't connect to remote system");
		case TELNET_BACKEND_ERROR_CON_WRITE:
			return _("Error while sending data to remote system");
		case TELNET_BACKEND_ERROR_COMMANDS_READ:
			return _("Error while reading commands file");
		default: break;
	}

	return _("Unknown error");
}

void *telnet_backend_create()
{
	TelnetBackend *self;

	self = g_slice_new0(TelnetBackend);
	self->host = g_string_new("localhost");
	self->port = 3333;
	self->type = 0;
	self->con = NULL;
	self->groups = NULL;
	self->device = NULL;
	self->keyfile = irreco_keyfile_create
			("", TELNET_DATA_DIR"/telnet.conf",
			 NULL);
	self->groups = g_key_file_get_groups(self->keyfile->keyfile,
					     &self->length_groups);

	/**create list store **/

	self->store = gtk_list_store_new (NUM_COLS,
					  G_TYPE_STRING,G_TYPE_STRING);


	/** tree **/
	self->model= GTK_TREE_MODEL (self->store);



	return self;
}

void telnet_backend_disconnect(TelnetBackend *self)
{
	if(self->con)
	{
		g_io_channel_shutdown(self->con, TRUE, NULL);
		g_io_channel_unref(self->con);
		self->con = NULL;
	}
}

void telnet_backend_destroy(TelnetBackend *self)
{
	telnet_backend_disconnect(self);
	g_string_free(self->host, TRUE);
	g_slice_free(TelnetBackend, self);
}

#define TELNET_BACKEND_CONFIG_GROUP "telnet"

IrrecoBackendStatus
telnet_backend_read_from_conf(TelnetBackend *self,
			const char *config_file)
{

	GKeyFile *avain = NULL;
	gint port;
	gint retval;
	gchar *host = NULL;
	gchar *device = NULL;

	IrrecoKeyFile *keyfile = NULL;

	gchar **commands;
	gsize commands_length;

	gint j;
	GtkTreeIter iter;


	keyfile = irreco_keyfile_create(
			g_path_get_dirname(config_file),
			config_file,
			TELNET_BACKEND_CONFIG_GROUP);

	retval = TELNET_BACKEND_ERROR_CONFIG_READ;

	if(keyfile == NULL) goto cleanup;
	if(!irreco_keyfile_get_int(keyfile, "port", &port)) goto cleanup;
	if(!irreco_keyfile_get_str(keyfile, "host", &host)) goto cleanup;
	if(!irreco_keyfile_get_str(keyfile, "type", &device)) goto cleanup;



	if(irreco_keyfile_get_gkeyfile(keyfile, &avain)){}


	commands = g_key_file_get_keys(avain,
					"commands",&commands_length,NULL);



	/** read commands from instance conf to model **/
	if(commands != NULL){
	for(j=0;j<commands_length;j++){


		gtk_list_store_append (GTK_LIST_STORE(self->store), &iter);

		gtk_list_store_set (GTK_LIST_STORE(self->store), &iter,
				    COL_NAME, commands[j],COL_CMD,
				    g_key_file_get_string(avain,"commands",
				    commands[j],NULL),-1);
		}

	}
	self->port = port;
	g_string_set_size(self->host, 0);
	g_string_append(self->host, host);
	self->device = device;
	retval = IRRECO_BACKEND_OK;


cleanup:
	irreco_keyfile_destroy(keyfile);
	g_free(host);

	return retval;
}

IrrecoBackendStatus
telnet_backend_save_to_conf(TelnetBackend *self,
			const char *config_file)
{

	gboolean valid;
	GKeyFile *keyfile = NULL;
	gchar *grp = TELNET_BACKEND_CONFIG_GROUP;

	keyfile = g_key_file_new();
	g_key_file_set_string(keyfile, grp, "host", self->host->str);
	g_key_file_set_integer(keyfile, grp, "port", self->port);
	g_key_file_set_string(keyfile, grp, "type", self->device);
	valid = gtk_tree_model_get_iter_first (self->model, &self->iter);

	while (valid)
	{
		gchar *name;
		gchar *cmd;
		gtk_tree_model_get (self->model, &self->iter, COL_NAME, &name,
		COL_CMD, &cmd, -1);
		g_key_file_set_string(keyfile, "commands",name,cmd);
		valid = gtk_tree_model_iter_next (self->model, &self->iter);

	}



	if(!irreco_write_keyfile(keyfile, config_file))
		return TELNET_BACKEND_ERROR_CONFIG_WRITE;

	return IRRECO_BACKEND_OK;
}

IrrecoBackendStatus
telnet_backend_get_devices(TelnetBackend *self,
		IrrecoGetDeviceCallback callback)
{

	if(self->groups != NULL)
	{
		callback(self->device, NULL);
	}

	return IRRECO_BACKEND_OK;
}

IrrecoBackendStatus
telnet_backend_get_commands(TelnetBackend *self,
			const char *device_name,
   			gpointer device_context,
			IrrecoGetCommandCallback callback)
{
	gboolean valid;
	valid = gtk_tree_model_get_iter_first (self->model, &self->iter);

	while (valid)
	{

		gchar *name;
		gchar *cmd;

		gtk_tree_model_get (self->model, &self->iter, COL_NAME, &name,
				    COL_CMD, &cmd, -1);

		callback(name,cmd);
		valid = gtk_tree_model_iter_next (self->model, &self->iter);

	}
	return IRRECO_BACKEND_OK;
}

void
telnet_backend_connection_error(TelnetBackend *self, GError *error)
{
	/* TODO: See if connection has died? */
	IRRECO_PRINTF("Connection error occured, Killing connection");
	telnet_backend_disconnect(self);
}

void telnet_backend_connection_error_callback(GIOChannel *source,
		GIOCondition cond, TelnetBackend *self)
{
	IRRECO_PRINTF("Connection error by callback");
	/* TODO: Can GError be digged from the object? */
	telnet_backend_connection_error(self, NULL);
}


IrrecoBackendStatus
telnet_backend_connect(TelnetBackend *self)
{
	int sock;
	long arg;
	int res;
	int valopt;
	fd_set myset;
	socklen_t lon;
	struct sockaddr_in addr;
	struct hostent *host;
	struct timeval tv;
	IRRECO_PRINTF("Connecting to %s:%d \n", self->host->str, self->port);

	memset(&addr, '\0', sizeof(addr));

	sock = socket(AF_INET, SOCK_STREAM, 0);
	arg = fcntl(sock, F_GETFL, NULL);
	arg |= O_NONBLOCK;
	fcntl(sock, F_SETFL, arg);

	addr.sin_family = AF_INET;
	addr.sin_port = htons(self->port);

	if(inet_aton(self->host->str, &addr.sin_addr))
	{
		IRRECO_PRINTF("Address is IP\n");
	}
	else if((host = gethostbyname(self->host->str)))
	{
		IRRECO_PRINTF("Address is valid hostname");
		memcpy((void *) &addr.sin_addr, (void *) host->h_addr_list[0],
			(size_t) host->h_length);
	}
	res = connect(sock, (struct sockaddr *)&addr, sizeof(addr));

	if (res < 0) {
		if (errno == EINPROGRESS) {
			tv.tv_sec = 3;
			tv.tv_usec = 0;
			FD_ZERO(&myset);
			FD_SET(sock, &myset);
			if (select(sock+1, NULL, &myset, NULL, &tv) > 0)
			{
				lon = sizeof(int);
				getsockopt(sock, SOL_SOCKET, SO_ERROR, (void*)
					   (&valopt), &lon);
				if(valopt)
				{
					IRRECO_PRINTF("Error in connection: %s\n"
						      ,strerror(errno));
					return TELNET_BACKEND_ERROR_CONNECT;
				}
			}
			else {

				IRRECO_PRINTF("Connection timed out: %s\n",
					      strerror(errno));
				return TELNET_BACKEND_ERROR_CONNECT;

			}
		}
		else {

			IRRECO_PRINTF("Couldn't resolve address: %s",
				      strerror(errno));
			return TELNET_BACKEND_ERROR_CONNECT;

		}
	}

	arg = fcntl(sock, F_GETFL, NULL);
	arg &= (~O_NONBLOCK);
	fcntl(sock, F_SETFL, arg);

	self->con = g_io_channel_unix_new(sock);

	g_io_add_watch(self->con, G_IO_ERR,
			(GIOFunc) telnet_backend_connection_error_callback,
			self);

	/* TODO: Should wait until the server responds */

	return 0; /* IRRECO_BACKEND_OK */

}

IrrecoBackendStatus
telnet_backend_ensure_connection(TelnetBackend *self)
{
	if(self->con)
	{
		/* TODO: Better checking? */
		return 0; /* IRRECO_BACKEND_OK */
	}
	else
	{
		return telnet_backend_connect(self);
	}
}

IrrecoBackendStatus
telnet_backend_send_command(TelnetBackend *self,
			const char *device_name,
			void *device_context,
			const char *command_name,
			void *command_context)
{
	gsize total_written;
	gsize written = 0;
	GString *command;
	GIOStatus status;
	GError *error = NULL;
	int constatus;

	command = g_string_new((gchar *)command_context);
	g_string_append(command, "\r\n");

	IRRECO_PRINTF("In telnet_backend_send_command\n");

	if((constatus = telnet_backend_ensure_connection(self)))
	{
		return constatus;
	}

	IRRECO_PRINTF("Connection ensured, starting write\n");

	/* TODO: Should read incoming stuff */

	for(total_written = 0; total_written < command->len;
	    total_written += written)
	{
		/* TODO: Better error reporting */
		status = g_io_channel_write_chars(self->con,
			 &(command->str[total_written]), -1, &written, &error);
		if(status == G_IO_STATUS_ERROR)
		{
			IRRECO_PRINTF("Failed writing to socket: %s \n",
				      error->message);
			telnet_backend_connection_error(self, error);
			return TELNET_BACKEND_ERROR_CON_WRITE;
		}
	}

	IRRECO_PRINTF("Command written. Flushing\n");

	status = g_io_channel_flush(self->con, NULL);

	/* TODO: Better handling for G_IO_STATUS_AGAIN ? */
	switch(status)
	{
		case G_IO_STATUS_ERROR:
		case G_IO_STATUS_AGAIN:
			telnet_backend_connection_error(self, error);
			return TELNET_BACKEND_ERROR_CON_WRITE;
		default: break;
	}

	IRRECO_PRINTF("Command sent successfully\n");

	g_string_free(command, TRUE);

	return IRRECO_BACKEND_OK;
}

IrrecoBackendStatus
telnet_backend_configure(TelnetBackend *self,
		GtkWindow *parent)
{
	GtkDialog *dialog;
	GtkTable *table;
	GtkEntry *host_widget;
	GtkWidget* port_editor;
	const gchar *new_host;
	gint new_port;
	GtkComboBox *device;
	gint response;
	gboolean loop;
	int i;
	gint min_port;
	gint max_port;

	min_port = 1;
	max_port = 65535;
	device = GTK_COMBO_BOX(gtk_combo_box_new_text());

	/** get devices to be controlled from telnet.conf **/
	if(self->groups != NULL ){
		for(i=0;i<self->length_groups;i++)
		{
		gtk_combo_box_append_text(GTK_COMBO_BOX(device),self->groups[i]);

		if(self->device != NULL)
		{
			if(g_utf8_collate(self->groups[i],self->device) == 0)
			{
			gtk_combo_box_set_active(GTK_COMBO_BOX(device), i);
			}
		}
		else
		{
		gtk_combo_box_set_active(GTK_COMBO_BOX(device), 0);
		}

		}


	}
	/**    **/
	dialog = GTK_DIALOG(gtk_dialog_new_with_buttons(
			"Telnet configuration",
			parent,
			GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_STOCK_CANCEL, GTK_RESPONSE_REJECT,
			GTK_STOCK_OK, GTK_RESPONSE_ACCEPT, NULL));

	table = GTK_TABLE(gtk_table_new(3, 2, FALSE));

	host_widget = GTK_ENTRY(gtk_entry_new());
	gtk_entry_set_text(host_widget, self->host->str);

	port_editor = hildon_number_editor_new(min_port, max_port);


	hildon_number_editor_set_value(HILDON_NUMBER_EDITOR(port_editor), self->port);

	gtk_table_attach_defaults(table,
			gtk_label_new(_("Host")), 0, 1, 0, 1);
	gtk_table_attach_defaults(table,
			GTK_WIDGET(host_widget), 1, 2, 0, 1);
	gtk_table_attach_defaults(table,
			gtk_label_new(_("Port")), 0, 1, 1, 2);
	gtk_table_attach_defaults(table,
			GTK_WIDGET(port_editor), 1, 2, 1, 2);
	gtk_table_attach_defaults(table,
			gtk_label_new(_("Type")), 0, 1, 2, 3);
	gtk_table_attach_defaults(table,
			GTK_WIDGET(device), 1, 2, 2, 3);

	gtk_container_add(GTK_CONTAINER(dialog->vbox), GTK_WIDGET(table));

	gtk_widget_show_all(GTK_WIDGET(dialog));


	do{
		response = gtk_dialog_run(GTK_DIALOG(dialog));
		switch(response) {

			case GTK_RESPONSE_REJECT:

			loop = FALSE;
			break;

			case GTK_RESPONSE_ACCEPT:

				new_host = gtk_entry_get_text(host_widget);
				new_port =
				hildon_number_editor_get_value(
				HILDON_NUMBER_EDITOR(port_editor));

				if(gtk_combo_box_get_active_text(
				   		GTK_COMBO_BOX(device))!= NULL)
				{
					self->device =
					gtk_combo_box_get_active_text(
					GTK_COMBO_BOX(device));
					self->port = new_port;
					g_string_assign(self->host, new_host);
					telnet_backend_store_commands(self);

				}
				telnet_backend_disconnect(self);
				telnet_backend_connect(self);


			loop = FALSE;
			break;

		}
	} while (loop);
	gtk_widget_destroy(GTK_WIDGET(dialog));

	return IRRECO_BACKEND_OK;
}
gboolean telnet_backend_is_device_editable(TelnetBackend *self,
					  const gchar * device_name,
       gpointer device_contex)
{

	return TRUE;
}
IrrecoBackendStatus telnet_backend_edit_device(TelnetBackend *self,
					       const char *device_name,
	    				       gpointer device_contex,
	  				       GtkWindow *parent)
{
	telnet_backend_edit_commands(self, parent);
	return IRRECO_BACKEND_OK;
}

void telnet_backend_store_commands(TelnetBackend *self){
	gint j;
	GtkTreeIter iter;

	/*gboolean valid;*/


	/** get keys **/

	self->keys = g_key_file_get_keys(self->keyfile->keyfile,
					 self->device,&self->length_keys,NULL);

	/** first clear old entries **/
	gtk_list_store_clear(GTK_LIST_STORE(self->store));


	/** add data to the list store **/
	for(j=0;j<self->length_keys;j++){

		gtk_list_store_append (GTK_LIST_STORE(self->store), &iter);
		gtk_list_store_set (GTK_LIST_STORE(self->store), &iter,
				    COL_NAME, self->keys[j],COL_CMD,
				    g_key_file_get_string(
				    self->keyfile->keyfile,
				    self->device,self->keys[j],NULL),-1);
		/*g_print(self->keys[j]);

		g_print(g_key_file_get_string(self->keyfile->keyfile,
			self->device,self->keys[j],NULL));*/
	}


	/**testitulostus**/
	/*
	valid = gtk_tree_model_get_iter_first (self->model, &self->iter);

	while (valid)
	{

		gchar *name;
		gchar *cmd;

		gtk_tree_model_get (self->model, &self->iter, COL_NAME, &name,
				    COL_CMD, &cmd, -1);

		g_print("temp\n");

		g_print("%s  %s \n", name,cmd);
		valid = gtk_tree_model_iter_next (self->model, &self->iter);

	}*/
}

void telnet_backend_edit_commands(TelnetBackend *self, GtkWindow *parent)
{
	GtkDialog	*dialog;
	/*GtkTable	*table;*/

	GtkWidget	*scrollbar;
	gboolean 	loop;
	gint 		response;
	GtkWidget *hbox;
	loop = TRUE;


/** treeview **/
	self->treeview = gtk_tree_view_new_with_model(self->model);

	gtk_tree_view_set_rules_hint (GTK_TREE_VIEW (self->treeview), TRUE);



	self->model = gtk_tree_view_get_model(GTK_TREE_VIEW(self->treeview));
	gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(self->treeview),TRUE);

/** column for Name **/
	self->renderer = gtk_cell_renderer_text_new ();
	g_object_set (self->renderer,
		      "editable", TRUE,	NULL);

	g_signal_connect (self->renderer, "edited",
			  G_CALLBACK (telnet_command_edited_callback), self);

	g_object_set_data (G_OBJECT (self->renderer), "column",
			   GINT_TO_POINTER (COL_NAME));

	self->column = gtk_tree_view_column_new_with_attributes ("Name  ",
			self->renderer, "text",COL_NAME,NULL);

	gtk_tree_view_append_column(GTK_TREE_VIEW(self->treeview), self->column);
/** column for Command **/
	self->renderer = gtk_cell_renderer_text_new ();
	self->column = gtk_tree_view_column_new_with_attributes ("Command      \
			                 ",
		       self->renderer,"text",COL_CMD,NULL);

	g_object_set (self->renderer, "editable", TRUE,	NULL);

	g_signal_connect (self->renderer, "edited",
			  G_CALLBACK (telnet_command_edited_callback), self);
	g_object_set_data (G_OBJECT (self->renderer), "column",
			   GINT_TO_POINTER(COL_CMD));

	gtk_tree_view_append_column(GTK_TREE_VIEW(self->treeview), self->column);

/** Create dialog **/

	dialog = GTK_DIALOG(gtk_dialog_new_with_buttons("Edit Commands",parent,
		GTK_DIALOG_MODAL, GTK_STOCK_ADD,
		ADD_COMMAND,GTK_STOCK_REMOVE,
		REMOVE_COMMAND ,GTK_STOCK_CANCEL,
		GTK_RESPONSE_REJECT,
		GTK_STOCK_OK,
		GTK_RESPONSE_ACCEPT,
		NULL));

/**scrollbar**/

	scrollbar = gtk_vscrollbar_new(gtk_tree_view_get_vadjustment(
			   GTK_TREE_VIEW(self->treeview)));

/**packing**/

	hbox = gtk_hbox_new (FALSE, 8);
	gtk_container_add (GTK_CONTAINER (dialog->vbox), hbox);

	gtk_box_pack_start (GTK_BOX (hbox), self->treeview, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (hbox), scrollbar, FALSE, FALSE, 0);

	gtk_widget_show_all(GTK_WIDGET(dialog));




	do{
		response = gtk_dialog_run(GTK_DIALOG(dialog));
		switch(response) {

			case GTK_RESPONSE_REJECT:
			loop = FALSE;
			break;
			case GTK_RESPONSE_ACCEPT:
			loop = FALSE;
			break;
			case REMOVE_COMMAND:
			remove_command(self);
			break;
			case ADD_COMMAND:
			add_command(self);
			break;

		}


	}while(loop);


	gtk_widget_destroy(GTK_WIDGET(dialog));

}
void telnet_command_edited_callback(GtkCellRendererText *renderer,
				    gchar *path_string,gchar *new_text,
				    TelnetBackend *self)
{
	gint column;
	GtkTreePath *path;

	gboolean valid;
	path = gtk_tree_path_new_from_string (path_string);

	column = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(renderer),"column"));
	path = gtk_tree_path_new_from_string (path_string);
	gtk_tree_model_get_iter (self->model, &self->iter, path);

	switch(column)
	{
		case COL_CMD:
		{
			gint i;
			gchar *old_text;
			gtk_tree_model_get (self->model, &self->iter,
					    column, &old_text, -1);
			g_free (old_text);
			i = gtk_tree_path_get_indices (path)[0];
			gtk_list_store_set (GTK_LIST_STORE (self->model),
					    &self->iter, column,new_text, -1);

		}
		break;
		case COL_NAME:
		{

			gint i;
			gchar *old_text;
			gtk_tree_model_get (self->model, &self->iter, column,
					    &old_text, -1);
			g_free (old_text);
			i = gtk_tree_path_get_indices (path)[0];
			gtk_list_store_set (GTK_LIST_STORE (self->model),
					    &self->iter, column,new_text, -1);

		}

		break;
	}

	valid = gtk_tree_model_get_iter_first (self->model, &self->iter);

}
void remove_command(TelnetBackend *self)
{

	GtkTreeIter iter;
	GtkTreeSelection *selection;


	selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(self->treeview));
	if (gtk_tree_selection_get_selected (selection, NULL, &iter))
	{
		gtk_list_store_remove (GTK_LIST_STORE (self->model), &iter);

	}


}
void add_command(TelnetBackend *self)
{
	gtk_list_store_append(GTK_LIST_STORE(self->store), &self->iter);
	gtk_list_store_set (GTK_LIST_STORE(self->store), &self->iter,
			    COL_NAME, "name",COL_CMD,"command",-1);

}


gchar *telnet_backend_get_description(gpointer instance_context)
{
	return NULL;
}

IrrecoBackendStatus telnet_backend_create_device(gpointer instance_context,
		GtkWindow * parent)
{


	return IRRECO_BACKEND_OK;
}
IrrecoBackendStatus telnet_backend_delete_device(gpointer instance_context,
		const gchar * device_name,
  gpointer device_contex,
  GtkWindow * parent)
{

	return IRRECO_BACKEND_OK;
}

IrrecoBackendFunctionTable telnet_backend_function_table =
{
	IRRECO_BACKEND_API_VERSION,
	 IRRECO_BACKEND_EDITABLE_DEVICES,
	"Telnet",
	(IrrecoBackendGetErrorMsg) telnet_backend_get_error_msg,
	(IrrecoBackendCreate) telnet_backend_create,
	(IrrecoBackendDestroy) telnet_backend_destroy,
	(IrrecoBackendReadFromConf) telnet_backend_read_from_conf,
	(IrrecoBackendSaveToConf) telnet_backend_save_to_conf,
	(IrrecoBackendGetDevices) telnet_backend_get_devices,
	(IrrecoBackendGetCommands) telnet_backend_get_commands,
	(IrrecoBackendSendCommand) telnet_backend_send_command,
	(IrrecoBackendConfigure) telnet_backend_configure,
	(IrrecoBackendGetDescription) telnet_backend_get_description,
	(IrrecoBackendCreateDevice) telnet_backend_create_device,
 	(IrrecoBackendIsDeviceEditable)telnet_backend_is_device_editable,
	(IrrecoBackendEditDevice) telnet_backend_edit_device,
	(IrrecoBackendDeleteDevice) telnet_backend_delete_device,NULL,NULL,NULL
};

IrrecoBackendFunctionTable *get_irreco_backend_function_table()
{
	return &telnet_backend_function_table;
}

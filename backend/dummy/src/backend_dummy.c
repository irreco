
#define IRRECO_DEBUG_PREFIX "DUMMY"
#include <glib.h>
#include <glib/gprintf.h>
#include <gtk/gtk.h>
#include <irreco_backend_api.h>

/*
 * Arbitary values for testing pointer travelsal.
 */
#define TEST_INSTANCE_CONTEX	((void *) 12345)
#define TEST_DEVICE_CONTEX2	((void *) 12346)
#define TEST_DEVICE_CONTEX3	((void *) 12347)
#define TEST_DEVICE_CONTEX	((void *) 54321)
#define TEST_COMMAND_CONTEX	((void *) 54345)

const char *dummy_backend_get_error_msg(void * instance_context,
				       IrrecoBackendStatus code)
{
	switch (code) {
		case 1: return "Mad user error";
		default: return "Unknown error";
	}
}

void *dummy_backend_create()
{
	g_printf("DUMMY: %s\n", __func__);
	return TEST_INSTANCE_CONTEX;
}

void dummy_backend_destroy(void * instance_context, gboolean permanently)
{
	g_printf("DUMMY: %s\n", __func__);
	g_assert(instance_context == TEST_INSTANCE_CONTEX);
}

IrrecoBackendStatus dummy_backend_read_from_conf(void * instance_context,
						 const char * config_file)
{
	g_printf("DUMMY: %s\n", __func__);
	g_assert(instance_context == TEST_INSTANCE_CONTEX);
	return IRRECO_BACKEND_OK;
}

IrrecoBackendStatus dummy_backend_save_to_conf(void * instance_context,
					       const char * config_file)
{
	g_printf("DUMMY: %s\n", __func__);
	g_assert(instance_context == TEST_INSTANCE_CONTEX);
	return IRRECO_BACKEND_OK;
}

gint i = 0;
IrrecoBackendStatus dummy_backend_get_devices(void * instance_context,
					      IrrecoGetDeviceCallback callback)
{
	g_printf("DUMMY: %s\n", __func__);
	g_assert(instance_context == TEST_INSTANCE_CONTEX);
	callback("Device 1", TEST_DEVICE_CONTEX);
	callback("Device 2", TEST_DEVICE_CONTEX2);
	callback("Device 3", TEST_DEVICE_CONTEX3);
	callback("Device 4", TEST_DEVICE_CONTEX);
	i = 0;
	return IRRECO_BACKEND_OK;
}

IrrecoBackendStatus dummy_backend_get_commands(void * instance_context,
					       const char * device_name,
					       void * device_contex,
					      IrrecoGetCommandCallback callback)
{
	i++;
	g_printf("DUMMY: %s\n", __func__);
	g_assert(instance_context == TEST_INSTANCE_CONTEX);
	g_assert(device_name != NULL);
	g_assert(device_contex == TEST_DEVICE_CONTEX
		 || device_contex == TEST_DEVICE_CONTEX2
		 || device_contex == TEST_DEVICE_CONTEX3);

	if (i == 1) {
		callback("00", TEST_COMMAND_CONTEX);
		callback("01", TEST_COMMAND_CONTEX);
		callback("02", TEST_COMMAND_CONTEX);
		callback("03", TEST_COMMAND_CONTEX);
		callback("04", TEST_COMMAND_CONTEX);
		callback("05", TEST_COMMAND_CONTEX);
		callback("06", TEST_COMMAND_CONTEX);
		callback("07", TEST_COMMAND_CONTEX);
		callback("08", TEST_COMMAND_CONTEX);
		callback("09", TEST_COMMAND_CONTEX);
		callback("10", TEST_COMMAND_CONTEX);
		callback("12", TEST_COMMAND_CONTEX);
		callback("13", TEST_COMMAND_CONTEX);
		callback("14", TEST_COMMAND_CONTEX);
		callback("15", TEST_COMMAND_CONTEX);
		callback("16", TEST_COMMAND_CONTEX);
		callback("17", TEST_COMMAND_CONTEX);
		callback("18", TEST_COMMAND_CONTEX);
		callback("19", TEST_COMMAND_CONTEX);
		callback("20", TEST_COMMAND_CONTEX);
	} else if (i == 2) {
		/* None. */
	} else {
		callback("Command 1", TEST_COMMAND_CONTEX);
		callback("Command 2", TEST_COMMAND_CONTEX);
		callback("Command 3", TEST_COMMAND_CONTEX);
		callback("Command 4", TEST_COMMAND_CONTEX);
	}
	return IRRECO_BACKEND_OK;
}

IrrecoBackendStatus dummy_backend_send_command(void * instance_context,
					       const char * device_name,
					       void * device_contex,
					       const char * command_name,
					       void * command_contex)
{
	g_printf("DUMMY: %s\n", __func__);
	g_assert(instance_context == TEST_INSTANCE_CONTEX);
	g_assert(device_name != NULL);
	g_assert(device_contex == TEST_DEVICE_CONTEX
		 || device_contex == TEST_DEVICE_CONTEX2
		 || device_contex == TEST_DEVICE_CONTEX3);
	g_assert(command_name != NULL);
	g_assert(command_contex == TEST_COMMAND_CONTEX);
	g_printf("Send: %s, %s\n", device_name, command_name);
	return -1;
}



IrrecoBackendStatus dummy_backend_configure(void * instance_context,
					    GtkWindow * parent)
{
	GtkWidget* dialog;
	g_printf("DUMMY: %s\n", __func__);
	g_assert(instance_context == TEST_INSTANCE_CONTEX);

	dialog = gtk_message_dialog_new(parent,
		GTK_DIALOG_DESTROY_WITH_PARENT,
		GTK_MESSAGE_INFO, GTK_BUTTONS_OK, "dummy_backend_configure()");
	gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);

	return IRRECO_BACKEND_OK;
}

gchar *dummy_backend_get_description(gpointer instance_context)
{
	return g_strdup("Dummy description");
}

IrrecoBackendStatus dummy_backend_create_device(gpointer instance_context,
					        GtkWindow * parent)
{
	GtkWidget* dialog;
	g_printf("DUMMY: %s\n", __func__);
	g_assert(instance_context == TEST_INSTANCE_CONTEX);

	dialog = gtk_message_dialog_new(parent,
		GTK_DIALOG_DESTROY_WITH_PARENT,
		GTK_MESSAGE_INFO, GTK_BUTTONS_OK,
		"dummy_backend_create_device()");
	gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);

	return IRRECO_BACKEND_OK;
}

gboolean dummy_backend_is_device_editable(gpointer instance_context,
					  const gchar * device_name,
					  gpointer device_contex)
{
	g_printf("DUMMY: %s\n", __func__);
	g_assert(device_contex == TEST_DEVICE_CONTEX
		 || device_contex == TEST_DEVICE_CONTEX2
		 || device_contex == TEST_DEVICE_CONTEX3);

	if (device_contex == TEST_DEVICE_CONTEX2
	    || device_contex == TEST_DEVICE_CONTEX3) {
		return FALSE;
	}
	return TRUE;
}

IrrecoBackendStatus dummy_backend_edit_device(gpointer instance_context,
					      const gchar * device_name,
					      gpointer device_contex,
					      GtkWindow * parent)
{
	GtkWidget* dialog;
	g_printf("DUMMY: %s\n", __func__);
	g_assert(instance_context == TEST_INSTANCE_CONTEX);

	dialog = gtk_message_dialog_new(parent,
		GTK_DIALOG_DESTROY_WITH_PARENT,
		GTK_MESSAGE_INFO, GTK_BUTTONS_OK,
		"dummy_backend_edit_device()");
	gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);

	return IRRECO_BACKEND_OK;
}

IrrecoBackendStatus dummy_backend_delete_device(gpointer instance_context,
					        const gchar * device_name,
						gpointer device_contex,
						GtkWindow * parent)
{
	GtkWidget* dialog;
	g_printf("DUMMY: %s\n", __func__);
	g_assert(instance_context == TEST_INSTANCE_CONTEX);

	dialog = gtk_message_dialog_new(parent,
		GTK_DIALOG_DESTROY_WITH_PARENT,
		GTK_MESSAGE_INFO, GTK_BUTTONS_OK,
		"dummy_backend_delete_device()");
	gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);

	return IRRECO_BACKEND_OK;
}

IrrecoBackendStatus dummy_backend_export_conf(gpointer instance_context,
				const char * device_name,
				IrrecoBackendFileContainer **file_container)
{
	g_printf("DUMMY: %s\n", __func__);

	*file_container = irreco_backend_file_container_new();

	irreco_backend_file_container_set(*file_container,
					  "Dummy backend",
					  "category",
    					  "manufacturer",
    					  "model",
    					  "file_name",
       					  "file_data");

	return IRRECO_BACKEND_OK;
}

IrrecoBackendStatus dummy_backend_import_conf(gpointer instance_context,
				IrrecoBackendFileContainer *file_container)
{
	g_printf("DUMMY: %s\n", __func__);

	g_printf("\nCategory: %s\n",file_container->category->str);
	g_printf("Manufacturer: %s\n",file_container->manufacturer->str);
	g_printf("Model: %s\n",file_container->model->str);
	g_printf("Hash: %s\n",file_container->hash->str);
	g_printf("Name: %s\n",file_container->name->str);
	g_printf("Data: %s\n",file_container->data->str);

	return IRRECO_BACKEND_OK;
}

 IrrecoBackendStatus dummy_backend_check_conf(
 				gpointer instance_context,
 				IrrecoBackendFileContainer *file_container,
				gboolean *configuration)
{
	*configuration = TRUE;
	return IRRECO_BACKEND_OK;
}

IrrecoBackendFunctionTable dummy_backend_function_table = {
	IRRECO_BACKEND_API_VERSION,	/* backend_api_version		*/
	IRRECO_BACKEND_EDITABLE_DEVICES |
	IRRECO_BACKEND_CONFIGURATION_EXPORT |
	IRRECO_BACKEND_MULTI_DEVICE_SUPPORT/* |
	IRRECO_BACKEND_NEW_INST_ON_CONF_IMPORT*/,/* flags 		*/
	NULL,				/* name				*/

	dummy_backend_get_error_msg,	/* get_error_msg 		*/
	dummy_backend_create,		/* create			*/
	dummy_backend_destroy,		/* destroy			*/
	dummy_backend_read_from_conf,	/* from_conf			*/
	dummy_backend_save_to_conf,	/* to_conf			*/
	dummy_backend_get_devices,	/* get_devices			*/
	dummy_backend_get_commands,	/* get_commands			*/
	dummy_backend_send_command,	/* send_command			*/
	dummy_backend_configure,	/* configure			*/

	dummy_backend_get_description,	/* get_description,    optional	*/
	dummy_backend_create_device,	/* create_device,      optional	*/
	dummy_backend_is_device_editable,/*is_device_editable, optional */
	dummy_backend_edit_device,	/* edit_device,        optional	*/
	dummy_backend_delete_device,	/* delete_device,      optional	*/

 	dummy_backend_export_conf,	/*export_conf,	       optional */
	dummy_backend_import_conf,	/*import_conf,	       optional */
 	dummy_backend_check_conf,	/*check_conf,	       optional */
	NULL				/* reserved */
};
IrrecoBackendFunctionTable *get_irreco_backend_function_table()
{
	g_printf("DUMMY: %s\n", __func__);
	dummy_backend_function_table.name = "Dummy backend";
	return &dummy_backend_function_table;
}


/*
 * Irreco CommandLine backend
 * Copyright (C) 2008 Sampo Savola (samposav@paju.oulu.fi)
 *
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#define IRRECO_DEBUG_PREFIX "CommandLine"
#include <irreco_util.h>
#include <irreco_backend_api.h>
#include <unistd.h>
#include <malloc.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <gtk/gtk.h>
#include <glib.h>
#include "config.h"
#include <fcntl.h>
#include <hildon/hildon-number-editor.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/resource.h>
/* This is used elsewhere too */
#define _(String) (String)

typedef struct
{
	IrrecoKeyFile *keyfile;
	gchar** keys;
	gsize length_keys;
	gsize length_groups;
	gchar** groups;
	char* device;
	GtkTreeModel *model;
	GtkTreeIter iter;
	GtkListStore	 *store;
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;
	GtkWidget 	*treeview;
	GtkTextBuffer *buffer;
	GtkTextTag *tag;
	GtkTextIter start;
	GtkTextIter end;
	char* temp;
	gboolean child_exists;
	GPid Child_Process;
	GtkWidget	*add_btn;
	GtkWidget	*remove_btn;
	GtkWidget	*test_btn;
	gchar *Child_Argv[2];
	gint Child_Out;
	gint Child_Err;
	GError *Child_Error;
	GIOChannel *gio;
	GIOChannel *gioerror;
	GError *err;
	gchar *cmd;
	GKeyFile *avain;
} CommandLineBackend;


typedef enum
{
	CommandLine_BACKEND_ERROR_CONFIG_READ = 1,
	CommandLine_BACKEND_ERROR_CONFIG_WRITE,
	CommandLine_BACKEND_ERROR_CONNECT,
	CommandLine_BACKEND_ERROR_CON_WRITE,
	CommandLine_BACKEND_ERROR_COMMANDS_READ,
 	EDIT_COMMANDS = 0,
  	REMOVE_COMMAND,
   	TEST_COMMAND,
	RUN_COMMAND,
   	ADD_COMMAND,
	ADD_NEW = 0,
	KILL
} CommandLineBackendError;
enum
{
	COL_NAME,
	COL_CMD,
	NUM_COLS = 2
} ;
CommandLineBackend *current;
void CommandLine_backend_edit_commands(CommandLineBackend *self,
				       GtkWindow *parent);
void CommandLine_command_edited_callback(GtkCellRendererText *renderer, gchar
		 			 *path_string, gchar *new_text,
	 				 CommandLineBackend *self);
void CommandLine_backend_add_device(CommandLineBackend *self);
static void remove_command(GtkWidget *widget, gpointer data);
void test_command(GtkWidget *widget, gpointer data);
void run_command(CommandLineBackend *self);
void add_command(GtkWidget *widget, gpointer data);
void kill_child(CommandLineBackend *self);
void add_url(GtkWidget *widget, gpointer data);
gboolean io_callback(GIOChannel *ioch,GIOCondition cond, gpointer data);
const char *CommandLine_backend_get_error_msg(CommandLineBackend *self,
					      IrrecoBackendStatus code)
{
	switch(code)
	{
		case CommandLine_BACKEND_ERROR_CONFIG_READ:
			return _("Couldn't read configuration");
		case CommandLine_BACKEND_ERROR_CONFIG_WRITE:
			return _("Couldn't write configuration");
		case CommandLine_BACKEND_ERROR_CONNECT:
			return _("Couldn't connect to remote system");
		case CommandLine_BACKEND_ERROR_CON_WRITE:
			return _("Error while sending data to remote system");
		case CommandLine_BACKEND_ERROR_COMMANDS_READ:
			return _("Error while reading commands file");
		default: break;
	}
	return _("Unknown error");
}

void *CommandLine_backend_create()
{
	CommandLineBackend *self;
	self = g_slice_new0(CommandLineBackend);
	self->device = "local";
	/**create list store **/
	self->store = gtk_list_store_new(NUM_COLS,
					 G_TYPE_STRING, G_TYPE_STRING);
	/** tree **/
	self->model = GTK_TREE_MODEL(self->store);
	return self;
}

void CommandLine_backend_destroy(CommandLineBackend *self)
{
	IRRECO_ENTER
	irreco_keyfile_destroy(self->keyfile);
	g_free(self->groups);
	g_object_unref(self->store);
	/*g_object_unref (self->model);*/
	g_slice_free(CommandLineBackend, self);
	/*g_key_file_free(self->avain);*/
	IRRECO_RETURN
}

#define CommandLine_BACKEND_CONFIG_GROUP "CommandLine"

IrrecoBackendStatus
CommandLine_backend_read_from_conf(CommandLineBackend *self,
				   const char *config_file)
{
	gint retval;
	/*gchar *device = NULL;*/
	IrrecoKeyFile *keyfile = NULL;
	gchar **commands;
	gsize commands_length;
	gint j;
	GtkTreeIter iter;
	gchar *path;
	gchar *command;
	path = g_path_get_dirname(config_file);
	keyfile = irreco_keyfile_create(path, config_file,
					CommandLine_BACKEND_CONFIG_GROUP);
	retval = CommandLine_BACKEND_ERROR_CONFIG_READ;

	if (keyfile == NULL) goto cleanup;

	/*if(!irreco_keyfile_get_str(keyfile, "type", &device)) goto cleanup;*/


	irreco_keyfile_get_gkeyfile(keyfile, &self->avain);
	commands = g_key_file_get_keys(self->avain, "commands",
				       &commands_length, NULL);

	/** read commands from instance conf to model **/
	if (commands != NULL) {
		for (j=0;j<commands_length;j++) {
			command = g_key_file_get_string(self->avain,"commands",
							commands[j], NULL);
			gtk_list_store_append(GTK_LIST_STORE(self->store),
					      &iter);
			gtk_list_store_set(GTK_LIST_STORE(self->store), &iter,
					   COL_NAME, commands[j], COL_CMD,
					   command, -1);
			g_free(command);
		}
	}
	retval = IRRECO_BACKEND_OK;
cleanup:
	irreco_keyfile_destroy(keyfile);
	g_free(path);
	g_strfreev(commands);
	return retval;
}

IrrecoBackendStatus
CommandLine_backend_save_to_conf(CommandLineBackend *self,
				 const char *config_file)
{
	gboolean valid;
	GKeyFile *keyfile = NULL;
	gchar *grp = CommandLine_BACKEND_CONFIG_GROUP;
	keyfile = g_key_file_new();
	g_key_file_set_string(keyfile, grp, "type", "local");
	valid = gtk_tree_model_get_iter_first(self->model, &self->iter);
	while (valid) {
		gchar *name;
		gchar *cmd;
		gtk_tree_model_get(self->model, &self->iter, COL_NAME, &name,
				   COL_CMD, &cmd, -1);
		g_key_file_set_string(keyfile, "commands", name, cmd);
		valid = gtk_tree_model_iter_next(self->model, &self->iter);
	}
	if (!irreco_write_keyfile(keyfile, config_file)) {
		return CommandLine_BACKEND_ERROR_CONFIG_WRITE;
	}
	g_key_file_free(keyfile);
	return IRRECO_BACKEND_OK;
}

IrrecoBackendStatus
CommandLine_backend_get_devices(CommandLineBackend *self,
				IrrecoGetDeviceCallback callback)
{
	callback("local", NULL);
	return IRRECO_BACKEND_OK;
}

IrrecoBackendStatus
CommandLine_backend_get_commands(CommandLineBackend *self,
				 const char *device_name,
     				 gpointer device_context,
	  			 IrrecoGetCommandCallback callback)
{
	gboolean valid;
	valid = gtk_tree_model_get_iter_first(self->model, &self->iter);
	while (valid) {
		gchar *name;
		gchar *cmd;
		gtk_tree_model_get(self->model, &self->iter, COL_NAME, &name,
				    COL_CMD, &cmd, -1);
		callback(name,cmd);
		valid = gtk_tree_model_iter_next(self->model, &self->iter);
	}
	return IRRECO_BACKEND_OK;
}

/** *******************************
* Waitpid to clean child process  *
***********************************/
void child_watch()
{
	pid_t pid;
	int   stat;
	IRRECO_ENTER
	IRRECO_PRINTF("SIGCHLD \n");
	while ((pid = waitpid(current->Child_Process, &stat, WNOHANG)) > 0) {
		IRRECO_PRINTF("WAITPID \n");
	}
	current->child_exists = FALSE;
	IRRECO_PRINTF("CHILD CLEANED \n");
	IRRECO_RETURN
}

IrrecoBackendStatus
CommandLine_backend_send_command(CommandLineBackend *self,
				 const char *device_name,void *device_context,
     				 const char *command_name,void *command_context)
{
	GString *command;
	GError *error = NULL;
	gchar *argv[] = { "sh", "-c", NULL, NULL };
	command = g_string_new((gchar *)command_context);
	IRRECO_PRINTF("In commandline_backend_send_command \n");
	IRRECO_PRINTF("Command is %s \n",command->str);
	argv[2] = command->str;
	if (g_spawn_async(NULL, argv, NULL, G_SPAWN_SEARCH_PATH, NULL, NULL,
	   NULL, &error)) {
		IRRECO_PRINTF("Command sent ok \n");
	}
	else {
		IRRECO_PRINTF("Command send failed \n");
	}
	g_string_free(command,TRUE);
	return IRRECO_BACKEND_OK;
}

IrrecoBackendStatus
CommandLine_backend_configure(CommandLineBackend *self,
			      GtkWindow *parent)
{
	CommandLine_backend_edit_commands(self, parent);
	return IRRECO_BACKEND_OK;
}

gboolean CommandLine_backend_is_device_editable(CommandLineBackend *self,
						const gchar * device_name,
      						gpointer device_contex)
{
	return TRUE;
}

IrrecoBackendStatus CommandLine_backend_edit_device(CommandLineBackend *self,
						    const char *device_name,
	  					    gpointer device_contex,
	    					    GtkWindow *parent)
{
	CommandLine_backend_edit_commands(self, parent);
	return IRRECO_BACKEND_OK;
}

/** ************************************************
* Kill child process when kill process is pressed  *
****************************************************/
void kill_child(CommandLineBackend *self)
{
	IRRECO_ENTER
	if (self->child_exists) {
		if (kill(self->Child_Process,9)) {
			IRRECO_PRINTF("Killig child failed \n");
		}
		else {
			IRRECO_PRINTF("Process %d killed\n",
				      self->Child_Process);
			child_watch();
		}
	}
	else {
		IRRECO_PRINTF("No child to kill \n");
	}
	IRRECO_RETURN
}

/** ************************************************
* Run command from test dialog                     *
****************************************************/
void run_command(CommandLineBackend *self)
{
	IRRECO_ENTER
	if (self->child_exists) {
		kill_child(self);
	}
	self->Child_Argv[0] = NULL;
	self->Child_Argv[1] = NULL;
	self->Child_Argv[2] = NULL;
	self->Child_Argv[0] = "sh";
	self->Child_Argv[1] = "-c";
	self->Child_Argv[2] = self->cmd;
	current->child_exists = FALSE;
	self->Child_Err = 0;
	self->err = NULL;
	if (self->Child_Error != NULL) {
		g_error_free (self->Child_Error);
	}
	self->Child_Error = NULL;
	self->tag = gtk_text_buffer_create_tag(self->buffer,NULL,"family",
					       "monospace", NULL);
	gtk_text_buffer_get_bounds(self->buffer, &self->start, &self->end);
	gtk_text_buffer_delete(self->buffer, &self->start, &self->end);
	gtk_text_buffer_get_end_iter(self->buffer, &self->start);

	if (g_spawn_async_with_pipes(NULL, self->Child_Argv, NULL,
				     G_SPAWN_DO_NOT_REAP_CHILD |
				     G_SPAWN_SEARCH_PATH, NULL, NULL,
				     &self->Child_Process, NULL,
				     &self->Child_Out,
				     &self->Child_Err, &self->Child_Error)) {
		IRRECO_PRINTF("Process %d spawned\n", self->Child_Process);
		current->child_exists=TRUE;
	}
	else {
		IRRECO_PRINTF("Spawn error %s  %d \n",
			      self->Child_Error->message,
			      self->Child_Error->code);
		kill_child(self);
	}
	self->gioerror =  g_io_channel_unix_new(self->Child_Err);
	self->gio =  g_io_channel_unix_new(self->Child_Out);
	g_io_add_watch_full(self->gio, 300, G_IO_IN | G_IO_HUP | G_IO_NVAL,
			    io_callback, self, NULL);
	/*g_io_add_watch(self->gio, G_IO_IN | G_IO_HUP | G_IO_NVAL,
			 io_callback,self);*/
	g_io_add_watch(self->gioerror, G_IO_IN, io_callback, self);
	g_io_channel_set_encoding(self->gio, NULL, NULL);
	g_io_channel_set_flags(self->gio, G_IO_FLAG_SET_MASK, &self->err);
	g_io_channel_set_flags(self->gioerror, G_IO_FLAG_SET_MASK, &self->err);
	IRRECO_RETURN
}
/** ************************************************
 * Test dialog                                     *
 ****************************************************/
void  test_command(GtkWidget *widget, gpointer data)
{
	GtkWidget *scrollwin;
	GtkDialog *dialog;
	GtkWidget *command_label;
	GtkWidget *output_label;
	GtkEntry *command_widget;
	gint response = 0;
	gboolean loop;
	GtkWidget *textview;
	GtkTreeSelection *selection;
	GtkTreeModel     *model;
	GtkTreeIter       iter;
	GtkWidget *hbox;
	GtkWidget *vbox1;
	GtkWidget *hbox1;
	GtkWidget *vbox2;
	GtkWidget *hbox2;
	GtkWidget *vbox3;
	GtkWidget *hbox3;
	gchar *new_cmd;
	CommandLineBackend *self;
	IRRECO_ENTER
	self = (CommandLineBackend*)data;
	current = self;
	hbox = gtk_hbox_new (FALSE,2);
	vbox1 = gtk_vbox_new (FALSE,2);
	hbox1 = gtk_hbox_new (FALSE,2);
	vbox2 = gtk_vbox_new (FALSE,2);
	hbox2 = gtk_hbox_new (FALSE,2);
	vbox3 = gtk_vbox_new (FALSE,2);
	hbox3 = gtk_hbox_new (FALSE,2);
	self->buffer = NULL;
	textview = gtk_text_view_new();
	gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(textview), GTK_WRAP_CHAR);
	gtk_text_view_set_editable(GTK_TEXT_VIEW(textview), FALSE);
	gtk_text_view_set_cursor_visible(GTK_TEXT_VIEW(textview), FALSE);
	self->Child_Argv[0] = "sh";
	self->Child_Argv[1] = "-c";
	selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(self->treeview));
	if (gtk_tree_selection_get_selected(selection, &model, &iter)) {
		gchar *cmd;
		gtk_tree_model_get(model, &iter, COL_CMD, &cmd, -1);
		self->Child_Argv[2] = cmd;
	}
	else {
		return;
	}
	self->cmd = g_strconcat(self->Child_Argv[0], " ", self->Child_Argv[1],
				" \"", self->Child_Argv[2], "\"", NULL);
	loop = TRUE;
	command_label = gtk_label_new(NULL);
	gtk_label_set_markup(GTK_LABEL(command_label), "<b>Command:</b>");
	gtk_misc_set_alignment(GTK_MISC(command_label), 0, 0);
	gtk_misc_set_padding(GTK_MISC(command_label), 12, 0);
	output_label = gtk_label_new(NULL);
	gtk_label_set_markup(GTK_LABEL(output_label), "<b>Output:</b>");
	gtk_misc_set_alignment(GTK_MISC(output_label), 0, 0);
	gtk_misc_set_padding(GTK_MISC(output_label), 12, 0);
	command_widget = GTK_ENTRY(gtk_entry_new());
	gtk_entry_set_text(command_widget, self->Child_Argv[2]);
	scrollwin = gtk_scrolled_window_new(NULL, NULL);
	self->buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(textview));
	gtk_text_buffer_get_end_iter(self->buffer, &self->start);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrollwin),
					GTK_POLICY_AUTOMATIC,
     					GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(scrollwin),
					      textview);
	gtk_widget_set_size_request(GTK_WIDGET(scrollwin), 600, -1);
	gtk_widget_set_size_request(GTK_WIDGET(command_widget), 60, -1);
	dialog = GTK_DIALOG(gtk_dialog_new_with_buttons(
			    "Test command",NULL,GTK_DIALOG_MODAL |
			    GTK_DIALOG_DESTROY_WITH_PARENT, "Kill", KILL,
       			    "Run", RUN_COMMAND, GTK_STOCK_CANCEL,
	      		    GTK_RESPONSE_REJECT, GTK_STOCK_OK,
	     		    GTK_RESPONSE_ACCEPT, NULL));

	gtk_box_pack_start(GTK_BOX(dialog->vbox), command_label, TRUE, TRUE, 0);
	gtk_box_pack_start(GTK_BOX(hbox1), GTK_WIDGET(command_widget), TRUE,
			   TRUE, 30);
	gtk_container_add(GTK_CONTAINER(vbox1), hbox1);
	gtk_box_pack_start(GTK_BOX (vbox2), output_label, TRUE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX (hbox3), GTK_WIDGET(scrollwin), TRUE, TRUE,
			   30);
	gtk_container_add(GTK_CONTAINER(vbox3), hbox3);
	gtk_container_set_border_width(GTK_CONTAINER(dialog->vbox), 0);
	gtk_container_set_border_width(GTK_CONTAINER(vbox1), 6);
	gtk_container_set_border_width(GTK_CONTAINER(vbox2), 0);
	gtk_container_set_border_width(GTK_CONTAINER(vbox3), 0);
	gtk_container_add(GTK_CONTAINER(dialog->vbox), vbox1);
	gtk_container_add(GTK_CONTAINER(vbox2), hbox2);
	gtk_container_add(GTK_CONTAINER(dialog->vbox), vbox2);
	gtk_container_add(GTK_CONTAINER(dialog->vbox), vbox3);
	gtk_widget_show_all(GTK_WIDGET(dialog));
	run_command(self);
	do {
		response = gtk_dialog_run(GTK_DIALOG(dialog));
		switch(response) {
			case KILL:
				kill_child(self);
				break;
			case RUN_COMMAND:
				self->cmd= gtk_editable_get_chars(GTK_EDITABLE(
								  command_widget),
								  0, 500);
				run_command(self);
				break;
			case GTK_RESPONSE_ACCEPT:
				if (self->child_exists) {
					kill_child(self);
				}
				new_cmd =
				gtk_editable_get_chars(GTK_EDITABLE
						      (command_widget), 0, 500);
				selection =
				gtk_tree_view_get_selection(GTK_TREE_VIEW
							   (self->treeview));

				if (gtk_tree_selection_get_selected(selection,
				    &model, &iter)) {
					gtk_list_store_set(GTK_LIST_STORE
							   (self->store), &iter,
							   COL_CMD, new_cmd,-1);
				}
			loop = FALSE;
			break;
			case GTK_RESPONSE_REJECT:
				kill_child(self);
				loop = FALSE;
				break;
		}
	} while (loop);
	g_free(self->cmd);
	gtk_text_buffer_get_bounds(self->buffer, &self->start, &self->end);
	gtk_text_buffer_delete(self->buffer, &self->start, &self->end);
	self->buffer = NULL;
	current = NULL;
	gtk_widget_destroy(GTK_WIDGET(dialog));
	IRRECO_RETURN
}

/** *************************************************
 * Edit commands dialog                             *
 ****************************************************/
void CommandLine_backend_edit_commands(CommandLineBackend *self,
				       GtkWindow *parent)
{
	GtkDialog	*dialog;
	GtkWidget	*cancel_btn;
	GtkWidget	*ok_btn;
	GtkWidget	*add_url_btn;
	gboolean 	loop;
	gint		response;
	GtkWidget	*hbox;
	GtkWidget	*hbox1;
	GtkWidget	*vbox1;
	GtkWidget	*sw;
	loop = TRUE;

/** treeview **/

	self->treeview = gtk_tree_view_new_with_model(self->model);
	gtk_tree_view_set_rules_hint(GTK_TREE_VIEW(self->treeview), TRUE);
	self->model = gtk_tree_view_get_model(GTK_TREE_VIEW(self->treeview));
	gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(self->treeview), TRUE);

/** column for Name **/

	self->renderer = gtk_cell_renderer_text_new();
	g_object_set(self->renderer, "editable", TRUE, NULL);
	g_signal_connect(self->renderer, "edited",
			 G_CALLBACK(CommandLine_command_edited_callback),
			 self);
	g_object_set_data(G_OBJECT(self->renderer), "column",
			  GINT_TO_POINTER(COL_NAME));

	self->column =
	gtk_tree_view_column_new_with_attributes("Name  ",
						 self->renderer, "text",
						 COL_NAME,NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(self->treeview),self->column);

/** column for Command **/
	self->renderer = gtk_cell_renderer_text_new ();
	self->column =
	gtk_tree_view_column_new_with_attributes("Command",self->renderer,
						 "text",COL_CMD,NULL);

	g_object_set(self->renderer, "editable", TRUE, NULL);

	g_signal_connect(self->renderer, "edited",
			  G_CALLBACK(CommandLine_command_edited_callback),
			 self);
	g_object_set_data(G_OBJECT(self->renderer), "column",
			  GINT_TO_POINTER(COL_CMD));
	gtk_tree_view_append_column(GTK_TREE_VIEW(self->treeview),
				    self->column);

/** Create dialog **/

	dialog = GTK_DIALOG(gtk_dialog_new());
	gtk_window_set_title(GTK_WINDOW(dialog), "Configure");
	gtk_window_set_transient_for(GTK_WINDOW(dialog), parent);

	self->add_btn = gtk_button_new_with_label("Add");
	add_url_btn = gtk_button_new_with_label("Add URL");
	self->remove_btn = gtk_button_new_with_label("Remove");
	self->test_btn = gtk_button_new_with_label("Test");

	g_signal_connect (G_OBJECT (self->add_btn), "clicked",
			  G_CALLBACK (add_command),self);

	g_signal_connect (G_OBJECT (self->remove_btn), "clicked",
			  G_CALLBACK (remove_command),(gpointer)self);

	g_signal_connect (G_OBJECT (self->test_btn), "clicked",
			  G_CALLBACK (test_command),self);

	g_signal_connect (G_OBJECT (add_url_btn), "clicked",
			  G_CALLBACK (add_url),self);

	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->action_area),
			   self->add_btn, TRUE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->action_area),
			    add_url_btn, TRUE, TRUE, 0);

	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->action_area),
			    self->remove_btn, TRUE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->action_area),
			    self->test_btn, TRUE, TRUE, 0);

	cancel_btn = gtk_dialog_add_button (GTK_DIALOG (dialog),
					GTK_STOCK_CANCEL,
     GTK_RESPONSE_REJECT);
	ok_btn = gtk_dialog_add_button (GTK_DIALOG (dialog),
			       GTK_STOCK_OK,
	  GTK_RESPONSE_ACCEPT);


/**packing**/
	vbox1 = gtk_vbox_new(FALSE,8);
	hbox = gtk_hbox_new (FALSE, 8);
	hbox1 = gtk_hbox_new (FALSE, 8);

	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW (sw),
				       GTK_POLICY_AUTOMATIC,
				       GTK_POLICY_AUTOMATIC);

	gtk_widget_set_size_request (GTK_WIDGET(sw),300,180);
	gtk_container_add (GTK_CONTAINER (dialog->vbox), vbox1);
	gtk_box_pack_start (GTK_BOX (vbox1), sw, TRUE, FALSE, 0);
	gtk_container_add (GTK_CONTAINER (sw), self->treeview);
	gtk_widget_show_all(GTK_WIDGET(dialog));

	do{
		response = gtk_dialog_run(GTK_DIALOG(dialog));
		switch(response)
		{
			case GTK_RESPONSE_REJECT:
			loop = FALSE;
			break;
			case GTK_RESPONSE_ACCEPT:
			loop = FALSE;
			break;
		}


	}while(loop);
	gtk_widget_destroy(GTK_WIDGET(dialog));
}

/** *************************************************
 * Callback for "edited" signal in liststore        *
 ****************************************************/
void CommandLine_command_edited_callback(GtkCellRendererText *renderer,
					 gchar *path_string, gchar *new_text,
					 CommandLineBackend *self)
{
	gint		column;
	GtkTreePath	*path;
	gboolean 	valid;
	IRRECO_ENTER
	column = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(renderer),
				 "column"));
	path = gtk_tree_path_new_from_string (path_string);
	gtk_tree_model_get_iter (self->model, &self->iter, path);

	switch(column)
	{
		case COL_CMD:
		{
			gint i;
			gchar *old_text;
			gtk_tree_model_get (self->model, &self->iter,
					    column, &old_text, -1);
			g_free (old_text);
			i = gtk_tree_path_get_indices (path)[0];
			gtk_list_store_set (GTK_LIST_STORE (self->model),
					    &self->iter, column,new_text, -1);
		}
		break;
		case COL_NAME:
		{

			gint i;
			gchar *old_text;
			gtk_tree_model_get (self->model, &self->iter, column,
					    &old_text, -1);
			g_free (old_text);
			i = gtk_tree_path_get_indices (path)[0];
			gtk_list_store_set (GTK_LIST_STORE (self->model),
					    &self->iter, column,new_text, -1);
		}

		break;
	}
	valid = gtk_tree_model_get_iter_first (self->model, &self->iter);
	gtk_tree_path_free(path);
	IRRECO_RETURN
}
/** *************************************************
 * Callback for reading data from the giochannel    *
 ****************************************************/
gboolean io_callback(GIOChannel *ioch,GIOCondition cond,gpointer data)
{
	CommandLineBackend *self;
	gchar       buffer[4000] = {0} ;
	gsize       bytes = 0;
	self = (CommandLineBackend*)data;

	if (cond & G_IO_IN)
	{
		if (g_io_channel_read_chars(ioch, buffer, sizeof (buffer),
		    &bytes, NULL) != G_IO_STATUS_ERROR && bytes > 0)
		{
			if(buffer != NULL)
			{
				gtk_text_buffer_insert_with_tags(self->buffer,
								 &self->start,
	 							 buffer, bytes,
	  							self->tag,NULL);
			}
		}
		else
		{
			g_io_channel_unref (ioch);
			return FALSE;
		}
		if (cond & G_IO_HUP)
		{
			while (g_io_channel_read_chars (ioch, buffer,
			       sizeof(buffer), &bytes,
				      NULL) != G_IO_STATUS_ERROR && bytes > 0)
			{

			}
		}
	}

	if (cond & (G_IO_ERR | G_IO_HUP | G_IO_NVAL))
	{
		if (g_io_channel_read_chars(ioch, buffer, sizeof (buffer),
		    &bytes, NULL) != G_IO_STATUS_ERROR && bytes > 0)
		{
			if(buffer != NULL)
			{
				gtk_text_buffer_insert(self->buffer,
						       &self->start,
	     					       buffer, bytes);
			}
		}
		else
		{
			g_io_channel_unref (ioch);
			return FALSE;
		}
		return FALSE;
	}
	return TRUE;
}

static void remove_command(GtkWidget *widget, gpointer data)
{
	GtkTreeIter iter;
	GtkTreeSelection *selection;
	CommandLineBackend *self =data;
	IRRECO_ENTER
	selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(self->treeview));
	if (gtk_tree_selection_get_selected (selection, NULL, &iter))
	{
		gtk_list_store_remove (GTK_LIST_STORE (self->model), &iter);
	}
	IRRECO_RETURN
}
void add_command(GtkWidget *widget, gpointer data)
{
	CommandLineBackend *self =data;
	IRRECO_ENTER
	gtk_list_store_append(GTK_LIST_STORE(self->store), &self->iter);
	gtk_list_store_set (GTK_LIST_STORE(self->store), &self->iter,
			    COL_NAME, "name",COL_CMD,"command",-1);
	IRRECO_RETURN
}
/** *************************************************
 * Dialog for parsing url to be added to liststore  *
 ****************************************************/
void add_url(GtkWidget *widget, gpointer data)
{
	GtkDialog *dialog;
	GtkWidget *box;
	GtkEntry *url_entry;
	GtkWidget *url_label;
	gint	response = 0;
	gchar *url;
	gchar *url_command;
	gchar *url_dbus;
	gboolean loop;
	CommandLineBackend *self = data;
	IRRECO_ENTER
	dialog = GTK_DIALOG(gtk_dialog_new_with_buttons("Add URL",NULL,
			    GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
			    GTK_STOCK_OK, GTK_RESPONSE_ACCEPT, GTK_STOCK_CANCEL,
			    GTK_RESPONSE_REJECT,NULL));

	url_entry = GTK_ENTRY(gtk_entry_new());
	gtk_entry_set_text(GTK_ENTRY(url_entry),"http://");
	url_label = gtk_label_new(NULL);
	url_dbus = "dbus-send --print-reply --dest=com.nokia.osso_browser "
	           "/com/nokia/osso_browser com.nokia.osso_browser.load_url "
		   "string:";

	gtk_widget_set_size_request (GTK_WIDGET(url_entry),300,-1);
	gtk_label_set_markup (GTK_LABEL (url_label), "<b>URL:</b>");


	box = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (box),url_label, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (box),GTK_WIDGET(url_entry), TRUE, TRUE, 0);

	gtk_box_pack_start (GTK_BOX(dialog->vbox),box,TRUE,TRUE,0);
	gtk_widget_show_all(GTK_WIDGET(dialog));

	do{
		response = gtk_dialog_run(GTK_DIALOG(dialog));
		switch(response) {

			case GTK_RESPONSE_REJECT:

				loop = FALSE;

				break;

			case GTK_RESPONSE_ACCEPT:

				url = gtk_editable_get_chars(GTK_EDITABLE
							    (url_entry),0,500);
				url_command = g_strconcat(url_dbus, url,NULL);
				gtk_list_store_append(GTK_LIST_STORE
						     (self->store),&self->iter);
				gtk_list_store_set(GTK_LIST_STORE(self->store),
						   &self->iter,COL_NAME, "name",
	 					   COL_CMD,url_command,-1);
				loop = FALSE;
				break;
		}
	} while (loop);
	gtk_widget_destroy(GTK_WIDGET(dialog));
	IRRECO_RETURN
}

gchar *CommandLine_backend_get_description(gpointer instance_context)
{
	return NULL;
}

IrrecoBackendStatus CommandLine_backend_create_device(gpointer instance_context,
						      GtkWindow * parent)
{
	GtkWidget* dialog;
	g_printf("Commandline: %s\n", __func__);
	dialog = gtk_message_dialog_new(parent,GTK_DIALOG_DESTROY_WITH_PARENT,
					GTK_MESSAGE_INFO, GTK_BUTTONS_OK,
     					"Commandline backend does not support\n"
					" creating devices");
	gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);
	return IRRECO_BACKEND_OK;
}
IrrecoBackendStatus CommandLine_backend_delete_device(gpointer instance_context,
						      const gchar * device_name,
	    					      gpointer device_contex,
						      GtkWindow * parent)
{
	GtkWidget* dialog;
	g_printf("Commandline: %s\n", __func__);
	dialog = gtk_message_dialog_new(parent, GTK_DIALOG_DESTROY_WITH_PARENT,
					GTK_MESSAGE_INFO, GTK_BUTTONS_OK,
					"Commandline backend does not support\n"
					"deleting devices");
	gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);
	return IRRECO_BACKEND_OK;
}

IrrecoBackendFunctionTable CommandLine_backend_function_table =
{
	IRRECO_BACKEND_API_VERSION,
	 IRRECO_BACKEND_EDITABLE_DEVICES,
	"Commandline",
	(IrrecoBackendGetErrorMsg)CommandLine_backend_get_error_msg,
	(IrrecoBackendCreate)CommandLine_backend_create,
	(IrrecoBackendDestroy)CommandLine_backend_destroy,
	(IrrecoBackendReadFromConf)CommandLine_backend_read_from_conf,
	(IrrecoBackendSaveToConf)CommandLine_backend_save_to_conf,
	(IrrecoBackendGetDevices)CommandLine_backend_get_devices,
	(IrrecoBackendGetCommands)CommandLine_backend_get_commands,
	(IrrecoBackendSendCommand)CommandLine_backend_send_command,
	(IrrecoBackendConfigure)CommandLine_backend_configure,
	(IrrecoBackendGetDescription)CommandLine_backend_get_description,
	(IrrecoBackendCreateDevice)CommandLine_backend_create_device,
 	(IrrecoBackendIsDeviceEditable)CommandLine_backend_is_device_editable,
	(IrrecoBackendEditDevice)CommandLine_backend_edit_device,
	(IrrecoBackendDeleteDevice)CommandLine_backend_delete_device,NULL,
	 			   NULL,NULL
};

IrrecoBackendFunctionTable *get_irreco_backend_function_table()
{
	return &CommandLine_backend_function_table;
}

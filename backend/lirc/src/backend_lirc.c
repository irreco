/*
 lirc_backend - part of URemote770
 Copyright (C) 2006 Kimmo Leppälä (kimmo.leppala@gmail.com)

 This is based on the original ir-send program by Christoph Bartelmus (lirc@bartelmus.de)

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

#include "backend_lirc.h"

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <time.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <getopt.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <errno.h>
#include <signal.h>
#include <limits.h>
#include <netinet/in.h>

#define PACKET_SIZE 256
/* three seconds */
#define TIMEOUT 3

int timeout = 0;
char *progname = "lirc backend";

void sigalrm(int sig)
{
    timeout = 1;
}

#include <sys/time.h>
/*
 * From gcc manual
 */
int input_timeout(int filedes, unsigned int seconds)
{
	fd_set set;
	struct timeval timeout;

	/* Initialize the file descriptor set. */
	FD_ZERO (&set);
	FD_SET (filedes, &set);

	/* Initialize the timeout data structure. */
	timeout.tv_sec = seconds;
	timeout.tv_usec = 0;

	/* select returns 0 if timeout, 1 if input available, -1 if error. */
	return select(FD_SETSIZE, &set, NULL, NULL, &timeout);
}

IrrecoBackendStatus read_string(IrrecoLircBackend * lirc_backend,
				const char ** string)
{
	static char buffer[PACKET_SIZE + 1] = "";
	char *end;
	static int ptr = 0;
	ssize_t ret;
	IRRECO_ENTER

	if (ptr > 0) {
		memmove(buffer, buffer + ptr, strlen(buffer + ptr) + 1);
		ptr = strlen(buffer);
		end = strchr(buffer, '\n');
	} else {
		end = NULL;
	}

	while (end == NULL) {
		if (PACKET_SIZE <= ptr) {
			ptr = 0;
			*string = NULL;
			IRRECO_RETURN_INT(lirc_backend_error(
				lirc_backend, LIRC_BACKEND_ERR_BAD_PACKET));
		}

		if (input_timeout(lirc_backend->socket, 3) < 1) {
			ptr = 0;
			*string = NULL;
			IRRECO_RETURN_INT(lirc_backend_error(
				lirc_backend, LIRC_BACKEND_ERR_TIMEOUT));;
		} else {
			ret = read(lirc_backend->socket, buffer + ptr,
				   PACKET_SIZE - ptr);
		}

		buffer[ptr + ret] = 0;
		ptr = strlen(buffer);
		end = strchr(buffer, '\n');
	}

	end[0] = 0;
	ptr = strlen(buffer) + 1;
	IRRECO_DEBUG("Input buffer \"%s\".\n", buffer);
	*string = buffer;
	IRRECO_RETURN_INT(IRRECO_BACKEND_OK);
}

enum packet_state {
    P_BEGIN,
    P_MESSAGE,
    P_STATUS,
    P_DATA,
    P_N,
    P_DATA_N,
    P_END
};

IrrecoBackendStatus send_packet(IrrecoLircBackend * lirc_backend,
				const char *packet,
				LircBackendResponseHandler handler)
{
	int done, todo;
	const char *string, *data;
	char *endptr;
	enum packet_state state;
	int status, n;
	unsigned long data_n = 0;
	IrrecoBackendStatus read_string_status;
	IRRECO_ENTER

	IRRECO_PRINTF("Sending packet: \"%s\".\n", packet);

	todo = strlen(packet);
	data = packet;
	while (todo > 0) {
		done = write(lirc_backend->socket, (void *) data, todo);
		if (done < 0) {
			IRRECO_RETURN_INT(lirc_backend_error(lirc_backend,
				LIRC_BACKEND_ERR_COULD_NOT_SEND_PACKET,
				packet));
		}
		data += done;
		todo -= done;
	}

	/* get response */
	status = 0;
	state = P_BEGIN;
	n = 0;
	while (1) {

		read_string_status = read_string(lirc_backend, &string);
		if (read_string_status != IRRECO_BACKEND_OK) {
			IRRECO_RETURN_INT(read_string_status);
		}

		switch (state) {
		case P_BEGIN:
			if (strcasecmp(string, "BEGIN") != 0) {
				continue;
			}
			state = P_MESSAGE;
			break;

		case P_MESSAGE:
			if (strncasecmp(string, packet, strlen(string)) != 0 ||
			    strlen(string) + 1 != strlen(packet)) {
				state = P_BEGIN;
				continue;
			}
			state = P_STATUS;
			break;

		case P_STATUS:
			if (strcasecmp(string, "SUCCESS") == 0) {
				status = 0;
			} else if (strcasecmp(string, "END") == 0) {
				status = 0;
				goto check_status_code;
			} else if (strcasecmp(string, "ERROR") == 0) {
				status = LIRC_BACKEND_ERR_COMMAND_FAILED;
			} else {
				status = LIRC_BACKEND_ERR_BAD_PACKET;
				goto check_status_code;
			}
			state = P_DATA;
			break;

		case P_DATA:
			if (strcasecmp(string, "END") == 0) {
				goto check_status_code;
			} else if (strcasecmp(string, "DATA") == 0) {
				state = P_N;
				break;
			}
			status = LIRC_BACKEND_ERR_BAD_PACKET;
			goto check_status_code;

		case P_N:
			errno = 0;
			data_n = strtoul(string, &endptr, 0);
			if (!*string || *endptr) {
				status = LIRC_BACKEND_ERR_BAD_PACKET;
				goto check_status_code;
			}
			if (data_n == 0) {
				state = P_END;
			} else {
				state = P_DATA_N;
			}
			break;

		case P_DATA_N:
			IRRECO_DEBUG("Responce data: \"%s\".\n", string);
			if (handler != NULL) handler(lirc_backend, string);
			n++;
			if (n == data_n) state = P_END;
			break;

		case P_END:
			if (strcasecmp(string, "END") == 0) {
				goto check_status_code;
			}
			status = LIRC_BACKEND_ERR_BAD_PACKET;
			goto check_status_code;
			break;
		}
	}

	check_status_code:
	switch (status) {
	case LIRC_BACKEND_ERR_BAD_PACKET:
		IRRECO_RETURN_INT(lirc_backend_error(
			lirc_backend, LIRC_BACKEND_ERR_BAD_PACKET));

	case LIRC_BACKEND_ERR_COMMAND_FAILED:
		IRRECO_RETURN_INT(lirc_backend_error(
			lirc_backend, LIRC_BACKEND_ERR_COMMAND_FAILED, packet));

	case IRRECO_BACKEND_OK:
		IRRECO_RETURN_INT(IRRECO_BACKEND_OK);

	default:
		IRRECO_ERROR("Weird return code \"%i\"\n", status);
		IRRECO_RETURN_INT(status);
	}
}



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* IRRECO PLUGIN API.                                                         */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Construction & Destruction.                                                */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

IrrecoBackendFunctionTable lirc_backend_function_table = {
	IRRECO_BACKEND_API_VERSION,	/* backend_api_version */
	0,
	NULL,				/* name	*/
	lirc_backend_get_error_msg,	/* */
	lirc_backend_create,		/* create	*/
	lirc_backend_destroy,		/* destroy	*/
	lirc_backend_read_from_conf,	/* from_conf	*/
	lirc_backend_save_to_conf,	/* to_conf	*/
	lirc_backend_get_devices,	/* get_devices	*/
	lirc_backend_get_commands,	/* get_commands	*/
	lirc_backend_send_command,	/* send_command	*/
	lirc_backend_configure,		/* configure	*/
	lirc_get_description,
	NULL,
	NULL
};

IrrecoBackendFunctionTable *get_irreco_backend_function_table()
{
	IRRECO_ENTER
	lirc_backend_function_table.name = _("LIRC Server");
	IRRECO_RETURN_PTR(&lirc_backend_function_table);
}

void* lirc_backend_create()
{
	IrrecoLircBackend * lirc_backend;
	IRRECO_ENTER

	lirc_backend = g_slice_new0(IrrecoLircBackend);
	lirc_backend->host = g_string_new("localhost");
	lirc_backend->port = 8765;
	lirc_backend->socket = -1;
	lirc_backend->error_msg = g_string_new(NULL);
	IRRECO_RETURN_PTR(lirc_backend);
}

void lirc_backend_destroy(gpointer instance_context, gboolean permanently)
{
	IrrecoLircBackend * lirc_backend = instance_context;
	IRRECO_ENTER

	g_string_free(lirc_backend->host, TRUE);
	g_string_free(lirc_backend->error_msg, TRUE);
	g_slice_free(IrrecoLircBackend, lirc_backend);

	IRRECO_RETURN
}



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Error handling.                                                            */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

const gchar *lirc_backend_get_error_msg(gpointer instance_context,
				       IrrecoBackendStatus code)
{
	IrrecoLircBackend * lirc_backend = instance_context;
	IRRECO_ENTER
	IRRECO_RETURN_PTR(lirc_backend->error_msg->str);
}

gint lirc_backend_error(IrrecoLircBackend * lirc_backend,
		        IrrecoLircBackendError code, ...)
{
	va_list args;
	gchar *format;
	gchar *message;
	IRRECO_ENTER

	lirc_backend_disconnect(lirc_backend);

	/* Select message format. */
	switch (code) {
	case LIRC_BACKEND_ERR_CANT_OPEN_SOCKET:
		format = _("Can not open socket.");
		break;

	case LIRC_BACKEND_ERR_CANT_CONNECT:
		format = _("Can not connect to LIRC server.");
		break;

	case LIRC_BACKEND_ERR_CANT_RESOLVE_HOST:
		format = _("Can not resolve hostname \"%s\".");
		break;

	case LIRC_BACKEND_ERR_CONFIG_WRITE_FAILED:
		format = _("Failed to save configuration to file.");
		break;

	case LIRC_BACKEND_ERR_CONFIG_READ_FAILED:
		format = _("Failed to read configuration from file.");
		break;

	case LIRC_BACKEND_ERR_BAD_PACKET:
		format = _("Bad return packet");
		break;

	case LIRC_BACKEND_ERR_COULD_NOT_SEND_PACKET:
		format = _("Could not send packet \"%s\".");
		break;

	case LIRC_BACKEND_ERR_COMMAND_FAILED:
		format = _("Command \"%s\" failed.");
		break;

	case LIRC_BACKEND_ERR_TIMEOUT:
		format = _("Timeout while waiting for the server to respond.");
		break;

	default:
		g_string_set_size(lirc_backend->error_msg, 0);
		g_string_append_printf(lirc_backend->error_msg,
				       _("Unknown error code \"%i\"."), code);
		IRRECO_PRINTF("Error: %s\n", lirc_backend->error_msg->str);
		IRRECO_RETURN_INT(code);
		break;
	}

	va_start(args, code);
	g_vasprintf(&message, format, args);
	va_end(args);

	g_string_set_size(lirc_backend->error_msg, 0);
	g_string_append(lirc_backend->error_msg, message);
	g_free(message);

	/* Print error and return code. */
	IRRECO_PRINTF("Error: %s\n", lirc_backend->error_msg->str);
	IRRECO_RETURN_INT(code);
}



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Configuration writing and reading.                                         */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

IrrecoBackendStatus lirc_backend_read_from_conf(gpointer instance_context,
						const gchar * config_file)
{
	gint port;
	gint rvalue;
	gchar *dir = NULL;
	gchar *host = NULL;
	IrrecoKeyFile *keyfile = NULL;
	IrrecoLircBackend * lirc_backend = instance_context;
	IRRECO_ENTER

	IRRECO_DEBUG("Reading config file \"%s\".\n", config_file);
	dir = g_path_get_dirname(config_file);
	keyfile = irreco_keyfile_create(dir, config_file, "lirc");

	/* Attempt to read values from keyfile. */
	if (keyfile == NULL
	    || irreco_keyfile_get_int(keyfile, "port", &port) == FALSE
	    || irreco_keyfile_get_str(keyfile, "host", &host) == FALSE) {
		IRRECO_PRINTF("Config read failed\n");
		rvalue = lirc_backend_error(
			lirc_backend, LIRC_BACKEND_ERR_CONFIG_READ_FAILED);

	/* Save values to instace if read succeeded. */
	} else {
		IRRECO_PRINTF("Host \"%s\"\n", host);
		IRRECO_PRINTF("Port \"%i\"\n", port);

		lirc_backend->port = port;
		g_string_set_size(lirc_backend->host, 0);
		g_string_append(lirc_backend->host, host);
		rvalue = IRRECO_BACKEND_OK;
	}

	/* Creanup, and return status code. */
	irreco_keyfile_destroy(keyfile);
	g_free(host);
	g_free(dir);
	IRRECO_RETURN_INT(rvalue);
}



IrrecoBackendStatus lirc_backend_save_to_conf(gpointer instance_context,
					      const gchar * config_file)
{
	gboolean success;
	GKeyFile *keyfile;
	gchar group[] = "lirc";
	IrrecoLircBackend * lirc_backend = instance_context;
	IRRECO_ENTER

	IRRECO_DEBUG("Saving config file \"%s\".\n", config_file);

	keyfile = g_key_file_new();
	g_key_file_set_string(keyfile, group, "host", lirc_backend->host->str);
	g_key_file_set_integer(keyfile, group, "port", lirc_backend->port);

	success = irreco_write_keyfile(keyfile, config_file);
	g_key_file_free(keyfile);

	if (success == TRUE) {
		IRRECO_RETURN_INT(IRRECO_BACKEND_OK);
	}
	IRRECO_RETURN_INT(lirc_backend_error(
		lirc_backend, LIRC_BACKEND_ERR_CONFIG_WRITE_FAILED));
}



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Connection handling.                                                       */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

int lirc_backend_connect(IrrecoLircBackend * lirc_backend)
{
	struct hostent *host;
	struct sockaddr_in addr;
	IRRECO_ENTER

	IRRECO_PRINTF("Connecting to LIRC server.\n");
	IRRECO_PRINTF("Hostname: \"%s\".\n", lirc_backend->host->str);
	IRRECO_PRINTF("Port: \"%i\".\n", lirc_backend->port);

	/* Get host IP. */
	memset( &addr, '\0', sizeof(addr));
	if ((host = gethostbyname(lirc_backend->host->str)) == NULL) {
		IRRECO_RETURN_INT(lirc_backend_error(
			lirc_backend, LIRC_BACKEND_ERR_CANT_RESOLVE_HOST,
			lirc_backend->host->str));
	}

	/* Create socket. */
	addr.sin_family = AF_INET;
	addr.sin_port = htons(lirc_backend->port);
	memcpy((gpointer) &addr.sin_addr, (gpointer) host->h_addr_list[0],
		(size_t) host->h_length);
	if ((lirc_backend->socket = socket(PF_INET, SOCK_STREAM, 0)) == -1) {
		IRRECO_RETURN_INT(lirc_backend_error(
			lirc_backend, LIRC_BACKEND_ERR_CANT_OPEN_SOCKET));
	}

	/* Connect socket. */
	if (connect(lirc_backend->socket, (struct sockaddr *) &addr,
		    sizeof(addr)) == -1) {
		IRRECO_RETURN_INT(lirc_backend_error(
			lirc_backend, LIRC_BACKEND_ERR_CANT_OPEN_SOCKET));
	}

	IRRECO_PRINTF("Connected to LIRC server.\n");
	IRRECO_RETURN_INT(IRRECO_BACKEND_OK);
}

void lirc_backend_disconnect(IrrecoLircBackend * lirc_backend)
{
	IRRECO_ENTER
	if (lirc_backend->socket != -1) {
		close(lirc_backend->socket);
		lirc_backend->socket = -1;
	}
	IRRECO_RETURN
}



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Device list .                                                              */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

IrrecoBackendStatus lirc_backend_get_devices(gpointer instance_context,
					     IrrecoGetDeviceCallback callback)
{
	gint rvalue;
	IrrecoLircBackend * lirc_backend = instance_context;
	IRRECO_ENTER

	rvalue = lirc_backend_connect(lirc_backend);
	if (rvalue) IRRECO_RETURN_INT(rvalue);

	lirc_backend->dev_callback = callback;
	rvalue = send_packet(lirc_backend, "LIST\n",
			     lirc_backend_get_device_responce);
	lirc_backend->dev_callback = NULL;
	if (rvalue) IRRECO_RETURN_INT(rvalue);

	lirc_backend_disconnect(lirc_backend);
	IRRECO_RETURN_INT(IRRECO_BACKEND_OK);
}
void lirc_backend_get_device_responce(IrrecoLircBackend * lirc_backend,
				      const gchar * response)
{
	IRRECO_ENTER
	IRRECO_PRINTF("Device: \"%s\"\n", response);
	lirc_backend->dev_callback(response, NULL);
	IRRECO_RETURN
}

IrrecoBackendStatus lirc_backend_get_commands(gpointer instance_context,
					      const gchar * device_name,
					      gpointer device_contex,
					      IrrecoGetCommandCallback callback)
{
	int rvalue;
	GString *packet;
	IrrecoLircBackend * lirc_backend = instance_context;
	IRRECO_ENTER

	rvalue = lirc_backend_connect(lirc_backend);
	if (rvalue) IRRECO_RETURN_INT(rvalue);

	lirc_backend->cmd_callback = callback;
	packet = g_string_new(NULL);
	g_string_append_printf(packet, "LIST %s\n", device_name);
	rvalue = send_packet(lirc_backend, packet->str,
			     lirc_backend_get_command_responce);
	g_string_free(packet, TRUE);
	lirc_backend->cmd_callback = NULL;
	if (rvalue) IRRECO_RETURN_INT(rvalue);

	lirc_backend_disconnect(lirc_backend);
	IRRECO_RETURN_INT(IRRECO_BACKEND_OK);
}
void lirc_backend_get_command_responce(IrrecoLircBackend * lirc_backend,
				       const gchar * response)
{
	gint pos;
	IRRECO_ENTER

	IRRECO_PRINTF("Command: \"%s\"\n", response);
	if ((pos = irreco_char_pos(response, ' ')) == -1) {
		lirc_backend->cmd_callback(response, NULL);
	} else {
		lirc_backend->cmd_callback(response + pos + 1, NULL);
	}
	IRRECO_RETURN
}

gchar *lirc_get_description(gpointer instance_context)
{
	IrrecoLircBackend * lirc_backend = instance_context;
	IRRECO_ENTER
	IRRECO_RETURN_PTR(g_strdup(lirc_backend->host->str));
}



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Command sending.                                                           */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

IrrecoBackendStatus lirc_backend_send_command(gpointer instance_context,
					      const gchar * device_name,
					      gpointer device_contex,
					      const gchar * command_name,
					      gpointer command_contex)
{
	int rvalue;
	GString *packet;
	IrrecoLircBackend * lirc_backend = instance_context;
	IRRECO_ENTER

	rvalue = lirc_backend_connect(lirc_backend);
	if (rvalue) IRRECO_RETURN_INT(rvalue);

	packet = g_string_new(NULL);
	g_string_append_printf(packet, "SEND_ONCE %s %s\n",
			       device_name, command_name);
	rvalue = send_packet(lirc_backend, packet->str, NULL);
	g_string_free(packet, TRUE);
	if (rvalue) IRRECO_RETURN_INT(rvalue);

	lirc_backend_disconnect(lirc_backend);
	IRRECO_RETURN_INT(IRRECO_BACKEND_OK);
}



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Configuration dialog.                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/*
 * Create label, align it, and insert it into a table.
 */
void lirc_backend_dlg_insert_label(GtkWidget *table, const gchar *str, guint l,
				   guint r, guint t, guint b)
{
	GtkWidget *label;
	IRRECO_ENTER
	label = gtk_label_new(str);
	gtk_misc_set_alignment(GTK_MISC(label), 0, 0.5);
	gtk_table_attach(GTK_TABLE(table), label, l, r, t, b,
			 GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);
	IRRECO_RETURN
}

IrrecoBackendStatus lirc_backend_configure(gpointer instance_context,
					   GtkWindow * parent)
{
	IrrecoLircBackend * lirc_backend = instance_context;
	gint loop = TRUE;
	GString *port_string;
	GtkWidget *dialog;
	GtkWidget *table;
	GtkWidget *host_entry;
	GtkWidget *port_entry;
	IRRECO_ENTER

	/* Create objects. */
	dialog = gtk_dialog_new_with_buttons(
		_("LIRC server configuration"), parent,
		GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT |
		GTK_DIALOG_NO_SEPARATOR,
		GTK_STOCK_CANCEL, GTK_RESPONSE_REJECT,
		/*_("Test"), 100, */
		GTK_STOCK_OK, GTK_RESPONSE_ACCEPT,
		NULL);
	table = gtk_table_new(2, 2, FALSE);
	host_entry = gtk_entry_new();
	port_entry = gtk_entry_new();

	/* Set values. */
	gtk_entry_set_text(GTK_ENTRY(host_entry), lirc_backend->host->str);
	port_string = g_string_new(NULL);
	g_string_append_printf(port_string, "%i", lirc_backend->port);
	gtk_entry_set_text(GTK_ENTRY(port_entry), port_string->str);
	g_string_free(port_string, TRUE);

	/* Build dialog. */
	lirc_backend_dlg_insert_label(table, _("Hostname"), 0, 1, 0, 1);
	gtk_table_attach_defaults(GTK_TABLE(table), host_entry, 1, 2, 0, 1);
	lirc_backend_dlg_insert_label(table, _("Port"), 0, 1, 1, 2);
	gtk_table_attach_defaults(GTK_TABLE(table), port_entry, 1, 2, 1, 2);
	gtk_table_set_row_spacings(GTK_TABLE(table), 5);
        gtk_table_set_col_spacings(GTK_TABLE(table), 5);
	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->vbox), table);
	gtk_widget_show_all(dialog);

	while (loop == TRUE) {
		switch (gtk_dialog_run(GTK_DIALOG(dialog))) {
		case GTK_RESPONSE_REJECT:
			loop = FALSE;
			break;

		case GTK_RESPONSE_ACCEPT:
			{
			long int port;
			port = strtol(gtk_entry_get_text(GTK_ENTRY(
				      port_entry)), NULL, 10);
			if (port < 1 || port > 65535) {
				irreco_error_dlg(GTK_WINDOW(dialog),
						 _("Port number must be "
						 "in range 1 - 65535"));
				break;
			}

			lirc_backend->port = port;
			g_string_set_size(lirc_backend->host, 0);
			g_string_append(lirc_backend->host, gtk_entry_get_text(
					GTK_ENTRY(host_entry)));
			loop = FALSE;
			break;
			}
		}
	}

	gtk_widget_destroy(dialog);
	IRRECO_RETURN_INT(IRRECO_BACKEND_OK);
}





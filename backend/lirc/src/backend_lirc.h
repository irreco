/*
 * irreco - Ir Remote Control
 * Copyright (C) 2007  Arto Karppinen (arto.karppinen@iki.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Include                                                                    */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

#ifndef __BACKEND_LIRC_H__
#define __BACKEND_LIRC_H__

#define IRRECO_DEBUG_PREFIX "LIRC"
#include "irreco_util.h"

#include <glib.h>
#include <glib/gprintf.h>
#include <glib/gstdio.h>
#include <gtk/gtk.h>

#include "irreco_backend_api.h"



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Types                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
typedef enum {
	LIRC_BACKEND_ERR_CANT_OPEN_SOCKET = 1,
	LIRC_BACKEND_ERR_CANT_CONNECT,
	LIRC_BACKEND_ERR_CANT_RESOLVE_HOST,
	LIRC_BACKEND_ERR_CONFIG_WRITE_FAILED,
	LIRC_BACKEND_ERR_CONFIG_READ_FAILED,
	LIRC_BACKEND_ERR_BAD_PACKET,
	LIRC_BACKEND_ERR_COULD_NOT_SEND_PACKET,
	LIRC_BACKEND_ERR_COMMAND_FAILED,
	LIRC_BACKEND_ERR_TIMEOUT
} IrrecoLircBackendError;

typedef struct _IrrecoLircBackend IrrecoLircBackend;
struct _IrrecoLircBackend {
	GString	*host;
	int	port;
	int	socket;
	GString	*error_msg;

	IrrecoGetDeviceCallback		dev_callback;
	IrrecoGetCommandCallback	cmd_callback;
};

typedef void (*LircBackendResponseHandler) (IrrecoLircBackend * lirc_backend,
					    const char * response);



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Macro                                                                      */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
#define _(String) (String)



/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Prototypes                                                                 */
/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
void* lirc_backend_create();
void lirc_backend_destroy(void * instance_context, gboolean permanently);
IrrecoBackendStatus lirc_backend_read_from_conf(void * instance_context,
					       const char * config_file);

IrrecoBackendStatus lirc_backend_save_to_conf(void * instance_context,
					     const char * config_file);
const char *lirc_backend_get_error_msg(void * instance_context,
				       IrrecoBackendStatus code);
int lirc_backend_error(IrrecoLircBackend * lirc_backend,
		       IrrecoLircBackendError code, ...);
int lirc_backend_connect(IrrecoLircBackend * lirc_backend);
void lirc_backend_disconnect(IrrecoLircBackend * lirc_backend);
IrrecoBackendStatus lirc_backend_get_devices(void * instance_context,
					    IrrecoGetDeviceCallback callback);
void lirc_backend_get_device_responce(IrrecoLircBackend * lirc_backend,
				      const char * response);
IrrecoBackendStatus lirc_backend_get_commands(void * instance_context,
					     const char *device_name,
					     void *device_contex,
					     IrrecoGetCommandCallback callback);
void lirc_backend_get_command_responce(IrrecoLircBackend * lirc_backend,
				       const char * response);
gchar *lirc_get_description(gpointer instance_context);
IrrecoBackendStatus lirc_backend_send_command(void *instance_context,
					     const char *device_name,
					     void *device_contex,
					     const char *command_name,
					     void *command_contex);
IrrecoBackendStatus lirc_backend_configure(void * instance_context,
					   GtkWindow * parent);



#endif /* __BACKEND_LIRC_H__ */





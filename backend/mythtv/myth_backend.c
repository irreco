/*
 * Irreco MythTV backend
 * Copyright (C) 2007  Jami Pekkanen (jami.pekkanen at tkk.fi)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#define IRRECO_DEBUG_PREFIX "MYTH"
#include <irreco_util.h>
#include <irreco_backend_api.h>

#include <unistd.h>
#include <malloc.h>
#include <errno.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>

#include <gtk/gtk.h>

/* This is used elsewhere too */
#define _(String) (String)

typedef struct
{
	GString *host;
	gint port;
	GIOChannel *con;
} MythBackend;

typedef enum
{
	MYTH_BACKEND_ERROR_CONFIG_READ = 1,
	MYTH_BACKEND_ERROR_CONFIG_WRITE,
	MYTH_BACKEND_ERROR_CONNECT,
	MYTH_BACKEND_ERROR_CON_WRITE
} MythBackendError;

typedef enum
{
	MYTH_BACKEND_DEVICE_GLOBAL,
	MYTH_BACKEND_DEVICE_VIEWING
} MythBackendDevice;

const char *myth_backend_get_error_msg(MythBackend *self,
				IrrecoBackendStatus code)
{
	switch(code)
	{
		case MYTH_BACKEND_ERROR_CONFIG_READ:
			return _("Couldn't read configuration");
		case MYTH_BACKEND_ERROR_CONFIG_WRITE:
			return _("Couldn't write configuration");
		case MYTH_BACKEND_ERROR_CONNECT:
			return _("Couldn't connect to remote system");
		case MYTH_BACKEND_ERROR_CON_WRITE:
			return _("Error while sending data to remote system");
		default: break;
	}

	return _("Unknown error");
}

void *myth_backend_create()
{
	MythBackend *self;

	self = g_slice_new0(MythBackend);
	self->host = g_string_new("localhost");
	self->port = 6546;
	self->con = NULL;

	return self;
}

void myth_backend_disconnect(MythBackend *self)
{
	if(self->con)
	{
		g_io_channel_shutdown(self->con, TRUE, NULL);
		g_io_channel_unref(self->con);
		self->con = NULL;
	}
}

void myth_backend_destroy(MythBackend *self)
{
	myth_backend_disconnect(self);
	g_string_free(self->host, TRUE);
	g_slice_free(MythBackend, self);
}

#define MYTH_BACKEND_CONFIG_GROUP "myth"

IrrecoBackendStatus
myth_backend_read_from_conf(MythBackend *self,
			const char *config_file)
{
	gint port;
	gint retval;
	gchar *host = NULL;
	IrrecoKeyFile *keyfile = NULL;

	keyfile = irreco_keyfile_create(
			g_path_get_dirname(config_file),
			config_file,
			MYTH_BACKEND_CONFIG_GROUP);

	retval = MYTH_BACKEND_ERROR_CONFIG_READ;

	if(keyfile == NULL) goto cleanup;
	if(!irreco_keyfile_get_int(keyfile, "port", &port)) goto cleanup;
	if(!irreco_keyfile_get_str(keyfile, "host", &host)) goto cleanup;


	self->port = port;
	g_string_set_size(self->host, 0);
	g_string_append(self->host, host);
	retval = IRRECO_BACKEND_OK;

cleanup:
	irreco_keyfile_destroy(keyfile);
	g_free(host);

	return retval;
}

IrrecoBackendStatus
myth_backend_save_to_conf(MythBackend *self,
			const char *config_file)
{
	GKeyFile *keyfile = NULL;
	gchar *grp = MYTH_BACKEND_CONFIG_GROUP;

	keyfile = g_key_file_new();
	g_key_file_set_string(keyfile, grp, "host", self->host->str);
	g_key_file_set_integer(keyfile, grp, "port", self->port);

	if(!irreco_write_keyfile(keyfile, config_file))
		return MYTH_BACKEND_ERROR_CONFIG_WRITE;

	return IRRECO_BACKEND_OK;
}

IrrecoBackendStatus
myth_backend_get_devices(MythBackend *self,
		IrrecoGetDeviceCallback callback)
{
	callback("Global", (void *)MYTH_BACKEND_DEVICE_GLOBAL);
	callback("Viewing", (void *)MYTH_BACKEND_DEVICE_VIEWING);
	return IRRECO_BACKEND_OK;
}


IrrecoBackendStatus
myth_backend_get_commands(MythBackend *self,
			const char *device_name,
			MythBackendDevice device_context,
			IrrecoGetCommandCallback callback)
{
	switch(device_context)
	{
		case MYTH_BACKEND_DEVICE_GLOBAL:
			callback("Enter", "key enter");
			callback("Escape", "key escape");
			callback("Up", "key up");
			callback("Right", "key right");
			callback("Down", "key down");
			callback("Left", "key left");
			callback("TV", "jump livetv");
			callback("Main Menu","jump mainmenu");
			callback("Video", "jump mythvideo");
			callback("DVD", "jump playdvd");
			callback("Channel Up", "play channel up");
			callback("Channel Down", "play channel down");
			break;
		case MYTH_BACKEND_DEVICE_VIEWING:
			callback("Aspect ratio", "key w");
			callback("OSD Menu", "key m");
			callback("Volume down", "key f10");
			callback("Volume up", "key f11");
			callback("Change channel Up", "play channel up");
			callback("Change channel Down", "play channel down");
			callback("Seek to beginning", "play seek beginning");
			callback("Seek forward", "play seek forward");
			callback("Seek backwards", "play seek backward");
			callback("Pause", "play speed pause");
			callback("Play", "play speed normal");
			callback("Playback at normal speed", "play speed 1x");
			callback("Playback at reverse", "play speed -1x");
			callback("Playback at 1/16x speed", "play speed 1/16x");
			callback("Playback at 1/8x speed", "play speed 1/8x");
			callback("Playback at 1/4x speed", "play speed 1/4x");
			callback("Playback at 1/2x speed", "play speed 1/2x");
			callback("Playback at 2x speed", "play speed 2x");
			callback("Playback at 4x speed", "play speed 4x");
			callback("Playback at 8x speed", "play speed 8x");
			callback("Playback at 16x speed", "play speed 16x");
			callback("Stop", "play stop");
			break;
	}

	return IRRECO_BACKEND_OK;
}

void
myth_backend_connection_error(MythBackend *self, GError *error)
{
	/* TODO: See if connection has died? */
	IRRECO_PRINTF("Connection error occured, Killing connection");
	myth_backend_disconnect(self);
}

void myth_backend_connection_error_callback(GIOChannel *source,
		GIOCondition cond, MythBackend *self)
{
	IRRECO_PRINTF("Connection error by callback");
	/* TODO: Can GError be digged from the object? */
	myth_backend_connection_error(self, NULL);
}


IrrecoBackendStatus
myth_backend_connect(MythBackend *self)
{
	int sock;
	struct sockaddr_in addr;
	struct hostent *host;

	IRRECO_PRINTF("Connecting to %s:%d", self->host->str, self->port);

	memset(&addr, '\0', sizeof(addr));

	addr.sin_family = AF_INET;
	addr.sin_port = htons(self->port);

	if(inet_aton(self->host->str, &addr.sin_addr))
	{
		IRRECO_PRINTF("Address is IP");
	}
	else if((host = gethostbyname(self->host->str)))
	{
		IRRECO_PRINTF("Address is valid hostname");
		memcpy((void *) &addr.sin_addr, (void *) host->h_addr_list[0],
			(size_t) host->h_length);

	}
	else
	{
		IRRECO_PRINTF("Couldn't resolve address: %s", strerror(errno));
		return MYTH_BACKEND_ERROR_CONNECT;

	}

	sock = socket(PF_INET, SOCK_STREAM, 0);

	if(sock < 0)
	{
		IRRECO_PRINTF("Creating socket failed: %s", strerror(errno));
		return MYTH_BACKEND_ERROR_CONNECT;
	}

	if(connect(sock, (struct sockaddr *) &addr, sizeof(addr)) < 0)
	{
		IRRECO_PRINTF("Connecting socket failed: %s", strerror(errno));
		return MYTH_BACKEND_ERROR_CONNECT;
	}

	self->con = g_io_channel_unix_new(sock);

	g_io_add_watch(self->con, G_IO_ERR,
			(GIOFunc) myth_backend_connection_error_callback,
			self);

	/* TODO: Should wait until the server responds */

	return 0; /* IRRECO_BACKEND_OK */

}

IrrecoBackendStatus
myth_backend_ensure_connection(MythBackend *self)
{
	if(self->con)
	{
		/* TODO: Better checking? */
		return 0; /* IRRECO_BACKEND_OK */
	}
	else
	{
		return myth_backend_connect(self);
	}
}

IrrecoBackendStatus
myth_backend_send_command(MythBackend *self,
			const char *device_name,
			void *device_context,
			const char *command_name,
			void *command_context)
{
	gsize total_written;
	gsize written = 0;
	GString *command;
	GIOStatus status;
	GError *error = NULL;
	int constatus;

	command = g_string_new((gchar *)command_context);
	g_string_append(command, "\r\n");

	IRRECO_PRINTF("In myth_backend_send_command");

	if((constatus = myth_backend_ensure_connection(self)))
	{
		return constatus;
	}

	IRRECO_PRINTF("Connection ensured, starting write");

	/* TODO: Should read incoming stuff */

	for(total_written = 0; total_written < command->len; total_written += written)
	{
		/* TODO: Better error reporting */
		status = g_io_channel_write_chars(self->con, &(command->str[total_written]), -1, &written, &error);
		if(status == G_IO_STATUS_ERROR)
		{
			IRRECO_PRINTF("Failed writing to socket: %s", error->message);
			myth_backend_connection_error(self, error);
			return MYTH_BACKEND_ERROR_CON_WRITE;
		}
	}

	IRRECO_PRINTF("Command written. Flushing");

	status = g_io_channel_flush(self->con, NULL);

	/* TODO: Better handling for G_IO_STATUS_AGAIN ? */
	switch(status)
	{
		case G_IO_STATUS_ERROR:
		case G_IO_STATUS_AGAIN:
			myth_backend_connection_error(self, error);
			return MYTH_BACKEND_ERROR_CON_WRITE;
		default: break;
	}

	IRRECO_PRINTF("Command sent successfully");

	g_string_free(command, TRUE);

	return IRRECO_BACKEND_OK;
}

IrrecoBackendStatus
myth_backend_configure(MythBackend *self,
		GtkWindow *parent)
{
	GtkDialog *dialog;
	GtkTable *table;
	GtkEntry *host_widget;
	GtkSpinButton *port_widget;
	const gchar *new_host;
	gint new_port;


	dialog = GTK_DIALOG(gtk_dialog_new_with_buttons(
			"MythTV configuration",
			parent,
			GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_STOCK_CANCEL, GTK_RESPONSE_REJECT,
			GTK_STOCK_OK, GTK_RESPONSE_ACCEPT, NULL));

	table = GTK_TABLE(gtk_table_new(2, 2, FALSE));

	host_widget = GTK_ENTRY(gtk_entry_new());
	gtk_entry_set_text(host_widget, self->host->str);

	port_widget = GTK_SPIN_BUTTON(
			gtk_spin_button_new_with_range(0, 2<<15, 1));
	gtk_spin_button_set_digits(port_widget, 0);
	gtk_spin_button_set_value(port_widget, self->port);

	gtk_table_attach_defaults(table,
			gtk_label_new(_("Host")), 0, 1, 0, 1);
	gtk_table_attach_defaults(table,
			GTK_WIDGET(host_widget), 1, 2, 0, 1);

	gtk_table_attach_defaults(table,
			gtk_label_new(_("Port")), 0, 1, 1, 2);
	gtk_table_attach_defaults(table,
			GTK_WIDGET(port_widget), 1, 2, 1, 2);

	gtk_container_add(GTK_CONTAINER(dialog->vbox), GTK_WIDGET(table));

	gtk_widget_show_all(GTK_WIDGET(dialog));

	if(gtk_dialog_run(dialog) == GTK_RESPONSE_ACCEPT)
	{
		new_host = gtk_entry_get_text(host_widget);
		new_port = gtk_spin_button_get_value_as_int(port_widget);

		if(strcmp(new_host, self->host->str) || new_port != self->port)
		{
			g_string_assign(self->host, new_host);
			self->port = new_port;
			IRRECO_PRINTF("New configuration, resetting connection");
			myth_backend_disconnect(self);
		}
	}

	gtk_widget_destroy(GTK_WIDGET(dialog));

	return IRRECO_BACKEND_OK;
}

IrrecoBackendFunctionTable myth_backend_function_table =
{
	IRRECO_BACKEND_API_VERSION,
	0,
	"MythTV Frontend",
	(IrrecoBackendGetErrorMsg) myth_backend_get_error_msg,
	(IrrecoBackendCreate) myth_backend_create,
	(IrrecoBackendDestroy) myth_backend_destroy,
	(IrrecoBackendReadFromConf) myth_backend_read_from_conf,
	(IrrecoBackendSaveToConf) myth_backend_save_to_conf,
	(IrrecoBackendGetDevices) myth_backend_get_devices,
	(IrrecoBackendGetCommands) myth_backend_get_commands,
	(IrrecoBackendSendCommand) myth_backend_send_command,
	(IrrecoBackendConfigure) myth_backend_configure,
	NULL, NULL, NULL
};

IrrecoBackendFunctionTable *get_irreco_backend_function_table()
{
	return &myth_backend_function_table;
}

#!/bin/bash
cd `dirname "$0"`
source variables.sh
scratchbox_need

#
# Script for automating backend testing.
#

backend_main()
{
	BACKEND="$1"
	COMMAND="$2"
	ARGS=("$@")
	
	if [[ "$COMMAND" == "" ]]; then
		echo "Error: You did not give me a command to run!"
		backend_usage
		exit 1
	fi
	
	if [[ "$BACKEND" == "" ]]; then
		echo "Error: You did not give me backend name!"
		backend_usage
		exit 1
	fi
	
	if [[ "$BACKEND" == "all" ]]; then
		cd "$BACKEND_DIR"
		{ while read BACKEND; do
			if [ -d "$BACKEND" ]; then
				"$SCRIPT_DIR/$SCRIPT_NAME"  \
					"$BACKEND" "$COMMAND" "${ARGS[@]:2}"
				check_exit_code "$?"
			fi
		done } < <( ls -1 | egrep -v "$ALL_BACKENDS_FILTER" )
		exit 0
	fi
	
	if [ ! -d "$BACKEND_DIR/$BACKEND" ]; then
		echo "Error: Backend \"$BACKEND\" does not exist!"
		exit 1
	fi
	
	cd "$BACKEND_DIR/$BACKEND"
	check_exit_code "$?"
	
	declare -x PKG_CONFIG_PATH="$SCRIPT_TMP_DIR:$PKG_CONFIG_PATH"
	#echo "PKG_CONFIG_PATH: $PKG_CONFIG_PATH"
	
	case "$COMMAND" in
		--config|config|conf)	backend_conf "${ARGS[@]:2}";;
		--make|make)		backend_make "${ARGS[@]:2}";;
		--install|install|inst)	backend_install "${ARGS[@]:2}";;
		--clean|clean)		backend_clean "${ARGS[@]:2}";;
		--src|src)		backend_src "${ARGS[@]:1}";;
		*)			echo "Error: Unknown command \"$COMMAND\"";
					backend_usage;
					exit 1;;
	esac
}

backend_usage()
{
	echo ""
	echo "Usage: $SCRIPT_NAME BACKEND COMMAND [ options ]"
	echo "       $SCRIPT_NAME all COMMAND [ options ]"
	echo ""
	echo "If BACKEND is all, then COMMAND is executed for all backends"
	echo "excepth those which match this regex: \"$ALL_BACKENDS_FILTER\"."
	echo ""
	echo "Commands:"
	echo "    --config | config | conf [ options ]"
	echo "        Congigure backend. Options are passed to ./configure script."
	echo "    --make | make"
	echo "        Compile backend."
	echo "    --install | install | inst"
	echo "        Install backend to \"$INSTALL_DIR\"."
	echo "    --clean | clean"
	echo "        Clean backend."
	echo "    --src | src"
	echo "        Show a list of backend source code files."
	echo ""
}

backend_conf()
{
	create_install_dir
	
	if [ -e './autogen.sh' ]; then
		backend_print_title "AUTOGEN"
		./autogen.sh
		check_exit_code "$?"
	fi
	
	backend_print_title "CONFIGURE"
	./configure --prefix="$INSTALL_DIR"  --enable-debug=yes "$@"
	check_exit_code "$?"
}

backend_make()
{
	if [ ! -e "Makefile" ]; then
		backend_conf
	fi
	generic_make backend_print_title "$@"
}

backend_install()
{
	backend_make
	backend_print_title "INSTALL"
	make install
	check_exit_code "$?"
}

backend_clean()
{
	generic_clean backend_print_title
}

#
# Prints a list of backend sources and headers.
#
backend_src()
{
	backend_print_title "SOURCES"
	echo -n "$BACKEND""_SOURCES =                       "
	cd "$BACKEND_DIR/$BACKEND"
	find  | egrep '\.[ch]$' | sort | scripts_src_pad
	cd "$OLDPWD"
}

backend_print_title()
{	
	print_title `echo "$BACKEND" | tr a-z A-Z `" $1"
}

backend_main "$@"



















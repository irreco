#!/bin/bash

echo ""
echo "Deb related option disabled."
echo "Use deb-creator.sh to make .deb packages."
echo "Some scripts still use this script to do cleaning, so don't delete yet."
echo ""

cd `dirname "$0"`
source variables.sh
scratchbox_need

make_deb_main()
{
	COMMAND="$1"
	ARGS=("$@")
	
	if [[ "$COMMAND" == "" ]]; then
		make_deb_usage
		echo "Error: You did not give me a command to run!"
		exit 1
	fi
	
	cd ..
		
	case "$COMMAND" in
#		--release|release|rel)		make_deb_release "${ARGS[@]:2}";;
#		--debug|debug|bug)		make_deb_debug "${ARGS[@]:2}";;
#		--developer|developer|dev)	make_deb_dev "${ARGS[@]:2}";;
		--clean|clean)			make_deb_clean "${ARGS[@]:2}";;
		*)				echo "Error: Unknown command \"$COMMAND\"";
						make_deb_usage;
						exit 1;;
	esac
}

make_deb_usage()
{
	echo "Usage: $SCRIPT_NAME COMMAND"
	echo ""
	echo "Commands:"
#	echo "    --release | release | rel"
#	echo "        Create a release package of irreco."
#	echo "    --debug | debug | bug"
#	echo "        Create a debug package of irreco."
#	echo "    --developer | developer | dev"
#	echo "        Create a package for backend developers. This will"
#	echo "        install irreco and irreco-util pkg-config files, and"
#	echo "        irreco-api and irreco-util headers."
	echo "    --clean | clean"
	echo "        Delete few files."
	echo ""
#	echo "IRRECO_BACKENDS variable:"
#	echo "    IRRECO_BACKENDS variable can be used to select which backends"
#	echo "    will be compilend into the deb. For example: "
#	echo ""
#	echo "        IRRECO_BACKENDS=\"dummy lirc\" ./make-deb.sh --debug"
#	echo ""
#	echo "    would make a debug package with only dummy and lirc backends."
#	echo ""
}

make_deb_release()
{
	make_deb_clean
	make_deb_print_title "RELEASE"
	
	REGEX='AC_INIT\(\[irreco\], \[(.*)\]\)'
	VERSION_CONFIGURE=$(egrep -o '^'"$REGEX"'$' ./irreco/configure.ac | sed -r "s|$REGEX|\1|")
	check_exit_code "$?"
	if [[ "$VERSION_CONFIGURE" == "" ]]; then
		echo "Could not get version number."
		exit 1
	fi
	
	head ./ChangeLog -n 1 | grep "$VERSION_CONFIGURE" > /dev/null
	check_exit_code "$?" "Version numbers dont match in ./ChangeLog and ./irreco/configure.ac"
	
	head ./debian/changelog -n 1 | grep "$VERSION_CONFIGURE" > /dev/null
	check_exit_code "$?" "Version numbers dont match in ./debian/changelog and ./irreco/configure.ac"
	
	head ./debian/changelog -n 1 | egrep '\([0-9.]+-[0-9]+\)' > /dev/null
	check_exit_code "$?" "Version number in ./debian/changelog must be in"\
		"the form (__SRC_VERSION__-__DEB_VERSION__). For example (0.5.6-1)."
	
	make_deb "irtrans lirc mythtv"
}

make_deb_debug()
{
	make_deb_clean
	make_deb_print_title "DEBUG"
	export IRRECO_DEBUG=yes
	make_deb "dummy irtrans lirc mythtv"
}

make_deb_dev()
{
	make_deb_clean
	make_deb_print_title "DEV"
	export IRRECO_DEBUG=yes
	export IRRECO_DEVELOPER=yes
	make_deb "dummy"
}

#
# Make debian package.
#
make_deb()
{
	"$SCRIPT_DIR"/irtrans.sh make 
	
	# Check backend list
	if [[ "$IRRECO_BACKENDS" == "" ]]; then
		export IRRECO_BACKENDS="$1"
	fi
	echo "IRRECO_BACKENDS=$IRRECO_BACKENDS";
	
	# Build package.
	export IRTRANS_LIB_DIR="$IRTRANS_SHLIB_DIR"
	save_exit_code dpkg-buildpackage -rfakeroot -B 2>&1 \
		| tee "$DEB_LOG_FILE"
		
	# Check output
	get_exit_code 
	if [[ "$EXIT_CODE" != "0" ]]; then
		make_deb_print_title "WARNINGS & ERRORS"
		check_log "$DEB_LOG_FILE" "0" 
	fi
	exit "$EXIT_CODE"
}

make_deb_clean()
{
	rm -f "$SCRIPT_TMP_DIR/irreco_sb_target"	
	make_deb_print_title "CLEAN"
	"$SCRIPT_DIR"/irreco.sh clean
	check_exit_code "$?"
	"$SCRIPT_DIR"/backend.sh all clean
	check_exit_code "$?"
	rm -fv ./build-stamp
	rm -fv ./configure-stamp
	rm -fv ./irreco.pc
	rm -rfv ./debian/irreco
}

make_deb_print_title()
{	
	print_title "MAKE DEB $1"
}

make_deb_main "$@"


















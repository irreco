#!/bin/bash

# Go to folder where this file lies.
cd `dirname "$0"`	

main()
{
	case "$1" in
		--deb|-deb|deb|.deb|debaa|debbaa|debioi)
			make_deb;;			
		*)
			info_print;
			exit 1;;
	esac
}

info_print()
{
	echo ""
	echo " Usage: telnet-deb-creator.sh COMMAND"
	echo ""
	echo " Commands:"
	echo "    --deb | -deb | deb"
	echo "        This creates telnet backend .deb into ../debs/"
	echo "        Packaging requires that you have packaged and"
	echo "        installed Irreco dev package into Scratchbox."
	echo ""
}

make_deb()
{

	# Check we are in scratchbox
	if [[ "$_SBOX_DIR" == "" ||
		"$_SBOX_RESTART_FILE" == "" ||
		"$_SBOX_SHELL_PID" == "" ||
		"$_SBOX_USER_GROUPNAME" == "" ]]; then
			echo "Error: Need scratchbox."
			exit 1
	fi
	
	# Go to telnet backend directory, clean stuff and build package
	cd ../backend/telnet/trunk/
	echo " Run autoclean"
	./autoclean.sh
	echo " Run dpkg-buildpackage"
	dpkg-buildpackage -rfakeroot -i
	
	# Check there is place to ditch deb
	if [ ! -e "../../../debs/" ]; then
		mkdir -v ../../../debs/
	fi
	
	# Ditch .deb to correct directory
	#mv -fv ../irreco-backend-telnet* ../../../debs/
	
	# Do cleaning
	echo " Do clean"
	#./autoclean.sh
	echo " Done clean"

}

main "$@"

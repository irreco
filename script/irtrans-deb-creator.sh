#!/bin/bash

# Go to folder where this file lies.
cd `dirname "$0"`	

main()
{
	case "$1" in
		--deb|-deb|deb|.deb|debaa|debbaa|debioi)
			make_deb;;			
		*)
			info_print;
			exit 1;;
	esac
}

info_print()
{
	echo ""
	echo " Usage: irtrans-deb-creator COMMAND"
	echo ""
	echo " Commands:"
	echo "    --deb | -deb | deb"
	echo "        This creates irtrans .deb into ../debs/"
	echo "        Packaging requires that you have installet Irreco dev"
	echo "        package into Scratchbox."
	echo ""
}

make_deb()
{

	# Check we are in scratchbox
	if [[ "$_SBOX_DIR" == "" ||
		"$_SBOX_RESTART_FILE" == "" ||
		"$_SBOX_SHELL_PID" == "" ||
		"$_SBOX_USER_GROUPNAME" == "" ]]; then
			echo "Error: Need scratchbox."
			exit 1
	fi

	# Unpack irtrans lib and make libIRTrans.so
	cd ../irtrans/trunk
	
	echo ""
	echo "Untar and make libirtrans"
	echo ""
	
	./untar-irtrans.sh 
	
	make clean
	
	make irtrans_noccf
	
	# Go to irtrans backend directory, clean stuff and build package
	cd ../../backend/irtrans/trunk/
	./autoclean.sh
	dpkg-buildpackage -rfakeroot -i
	
	# Ditch .deb to correct directory
	mkdir -v ../../../debs/
	#mv -fv ../irreco-backend-irtrans* ../../../debs/
	#mv -fv ../irtrans*.changes ../../../debs/
	
	# Do cleaning
	#cd ../../../script/
	#./irtrans.sh clean

}

main "$@"

#!/bin/bash
cd `dirname "$0"`
source variables.sh
scratchbox_need

#
# Script for automating theme testing.
#

theme_main()
{
	THEME="$1"
	COMMAND="$2"
	ARGS=("$@")
	
	find_theme_dir
		
	if [[ "$THEME" == "all" ]]; then
		cd "$THEME_DIR"
		{ while read THEME; do
			if [ -d "$THEME" ]; then
				"$SCRIPT_DIR/$SCRIPT_NAME"  \
					"$THEME" "$COMMAND" "${ARGS[@]:2}"
				check_exit_code "$?"
			fi
		done } < <( ls -1 )
		exit 0
	fi
	
	case "$COMMAND" in
		--install|install|inst)	theme_install "${ARGS[@]:2}";;
		*)			echo "Error: Unknown command \"$COMMAND\"";
					theme_usage;
					exit 1;;
	esac
}

theme_usage()
{
	echo ""
	echo "Usage: $SCRIPT_NAME THEME COMMAND [ options ]"
	echo "       $SCRIPT_NAME all COMMAND [ options ]"
	echo ""
	echo "Commands:"
	echo "    --install | install | inst"
	echo "        Install themes to \"$INSTALL_DIR\"."
	echo ""
}

theme_install()
{
	theme_print_title "INSTALL"
	"$THEME_DIR"/theme-conf-gen.sh --generate "$THEME_DIR"/"$THEME" "$INSTALL_DIR"
	check_exit_code "$?"
}

theme_print_title()
{	
	print_title `echo "$THEME" | tr a-z A-Z `" $1"
}

theme_main "$@"


















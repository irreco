#!/bin/bash
cd `dirname "$0"`
source variables.sh
scratchbox_need

irreco_main()
{
	COMMAND="$1"
	
	if [[ "$COMMAND" == "" ]]; then
		echo "Error: You did not give me a command to run!"
		irreco_usage
		exit 1
	fi
	
	cd "$IRRECO_DIR"
	check_exit_code "$?"
	
	ARGS=("$@")
	case "$COMMAND" in
		--config|config|conf)	irreco_conf "${ARGS[@]:1}";;
		--make|make)		irreco_make "${ARGS[@]:1}";;
		--install|install|inst)	irreco_install "${ARGS[@]:1}";;
		--clean|clean)		irreco_clean "${ARGS[@]:1}";;
		--src|src)		irreco_src "${ARGS[@]:1}";;
		*)			echo "Error: Unknown command \"$COMMAND\"";
					irreco_usage;
					exit 1;;
	esac
}

irreco_usage()
{
	echo "Usage: $SCRIPT_NAME COMMAND [ options ]"
}

irreco_conf()
{
	create_install_dir
	
	irreco_print_title "AUTOGEN"
	./autogen.sh
	check_exit_code "$?"
	
	irreco_print_title "CONFIGURE"
	./configure --prefix="$INSTALL_DIR" --enable-debug=yes "$@" \
	--enable-dev=yes --with-database="http://localhost/irreco/webdb/"
	check_exit_code "$?"
	
	"$SCRIPT_DIR"/fix-pkg-config.sh --path "$INSTALL_DIR"/lib
}

irreco_make()
{
	if [ ! -e "Makefile" ]; then
		irreco_conf
	fi
	generic_make irreco_print_title "$@"
}

irreco_install()
{
	irreco_make
	irreco_print_title "INSTALL"
	make install
	check_exit_code "$?"
}

irreco_clean()
{
	generic_clean irreco_print_title
}

#
# Prints a list of irreco sources and headers.
#
irreco_src()
{
	irreco_print_title "CORE SOURCES"
	echo -n "irreco_SOURCES =                        "
	ls "$IRRECO_DIR"/src/core \
		| egrep '\.[ch]$' | grep 'irreco.*' | scripts_src_pad
	
	irreco_print_title "UTIL SOURCES"
	echo -n "lib_irreco_util_la_SOURCES =            "
	ls "$IRRECO_DIR"/src/util \
		| egrep '\.[ch]$' | grep 'irreco.*' | scripts_src_pad
	echo -n "irreco_util_header_DATA =               "
	ls "$IRRECO_DIR"/src/util \
		| egrep '\.[ch]$' | grep 'irreco.*\.h' | scripts_src_pad
		
	irreco_print_title "WEBDB SOURCES"
	echo -n "lib_irreco_webdb_la_SOURCES =           "
	ls "$IRRECO_DIR"/src/webdb \
		| egrep '\.[ch]$' | grep 'irreco.*' | scripts_src_pad
}

irreco_print_title()
{	
	print_title "IRRECO $1"
}

irreco_main "$@"

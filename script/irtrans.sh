#!/bin/bash
cd `dirname "$0"`
source variables.sh
scratchbox_need
find_irtrans_dir

irtrans_main()
{
	COMMAND="$1"

	if [[ "$COMMAND" == "" ]]; then
		echo "Error: You did not give me a command to run!"
		irtrans_usage
		exit 1
	fi

	cd "$IRTRANS_DIR"
	check_exit_code "$?"

	ARGS=("$@")
	case "$COMMAND" in
		--install|makeinst|inst)	irtrans_install "${ARGS[@]:1}";;
#		--makeinst|makeinst)	irtrans_makeinst "${ARGS[@]:1}";;
#		--make|make)		irtrans_make "${ARGS[@]:1}";;
#		--install|install|inst)	irtrans_install "${ARGS[@]:1}";;
		--clean|clean)		irtrans_clean "${ARGS[@]:1}";;
#		--test|test)		irtrans_test "${ARGS[@]:1}";;
		*)			echo "Error: Unknown command \"$COMMAND\"";
					irtrans_usage;
					exit 1;;
	esac
}

irtrans_usage()
{
	echo "Usage: $SCRIPT_NAME COMMAND"
	echo ""
	echo "Commands:"
	echo "    --makeinst | makeinst"
	echo "        Installs irtrans to irreco tmp install directory"
	echo "        You still need to run \"backend.sh irtrans inst\" to install irtrans backend"
#	echo ""
#	echo "    --install | install | inst"
#	echo "        Run make for irtrans if needed"
#	echo "        copy libIRTrans.so to $INSTALL_DIR/lib"
#	echo "        copy irserver to $INSTALL_DIR/bin"
#	echo "        Run autogen.sh for irreco-backend-irtrans if needed"
#	echo "        Run configure.sh for irreco-backend-irtrans if needed"
#	echo "        Run make for irreco-backend-irtrans"
#	echo "        Run make install for irreco-backend-irtrans"
#	echo ""
	echo "    --clean|clean"
	echo "        Run \"make clean\" for irserver and shlib"
	echo "        And removes .debs and .changes from irtrans dir"
	echo ""
}

irtrans_install()
{
	irtrans_print_title "Add to PKG_CONFIG_PATH ../../install/lib/pkgconfig/"
	export PKG_CONFIG_PATH=../../install/lib/pkgconfig/:$PKG_CONFIG_PATH

	irtrans_print_title "Make install irserver"
	cd "$IRTRANS_IRSERVER_DIR/src"
	check_exit_code "$?"

	make irservern800_noccf
	make install2

	irtrans_print_title "Create modified irtrans_shlib.pc"
	cd "$IRTRANS_SHLIB_DIR/pkg"
	check_exit_code "$?"

	mkdir ../../../install
	mkdir ../../../install/lib
	mkdir ../../../install/lib/pkgconfig/
	mkdir ../../../install/include
	mkdir ../../../install/include/irtrans
	mkdir ../../../install/include/irtrans/shlib

	echo "prefix=$INSTALL_DIR" > ../../../install/lib/pkgconfig/irtrans_shlib.pc
	cat template.pc >> ../../../install/lib/pkgconfig/irtrans_shlib.pc

	irtrans_print_title "make libIRTrans.so and copy headers"
	cd "$IRTRANS_SHLIB_DIR/src"
	check_exit_code "$?"

	make libIRTrans.so
	cp -v libIRTrans.so ../../../install/lib/libIRTrans.so
	cp -v *.h ../../../install/include/irtrans/shlib/


}

#irtrans_make()
#{
#	match_sb_target "$SCRIPT_TMP_DIR/irtrans_sb_target"
#	if [[ "$?" != "0" ]]; then
#		irtrans_print_title "MAKE"
#
#		cd "$IRTRANS_DIR"
#		check_exit_code "$?"
#
#		make clean
#		check_exit_code "$?"
#
#		#make $@
#		make irtrans_noccf
#		check_exit_code "$?"
#
#		make install_sdk
#		check_exit_code "$?"
#
#		cd "$OLDPWD"
#		check_exit_code "$?"
#	fi
#}

#irtrans_makeinst()
#{
#	irtrans_print_title "irserver debbing"
#	cd "$IRTRANS_SHLIB_DIR"
#	check_exit_code "$?"
#	dpkg-buildpackage -rfakeroot -b
#	check_exit_code "$?"

#	irtrans_print_title "shlib debbing"
#	cd "$IRTRANS_IRSERVER_DIR"
#	check_exit_code "$?"
#	dpkg-buildpackage -rfakeroot -b
#	check_exit_code "$?"

#	irtrans_print_title "dpkg'ing debs"
#	cd "$IRTRANS_DIR"
#	dpkg -i irtrans-irserver_*.deb
#	check_exit_code "$?"
#	dpkg -i irtrans-shlib_*.deb
#	check_exit_code "$?"
#	dpkg -i irtrans-shlib-dev_*.deb
#	check_exit_code "$?"

#	echo ""
#	echo "Irserver and libIRTrans (aka. shlib) on place"
#	echo "Now just remember to install backend"
#	echo ""

#	exit 1
#}

#irtrans_backend_conf()
# {
# 	create_install_dir
#
# 	if [ -e './autogen.sh' ]; then
# 		irtrans_print_title "BACKEND AUTOGEN"
# 		./autogen.sh
# 		check_exit_code "$?"
# 	fi
#
# 	irtrans_print_title "BACKEND CONFIGURE"
#
# 	PKG_CONFIG_PATH=$PKG_CONFIG_PATH:$INSTALL_DIR/lib/pkgconfig ./configure \
# 	--with-irtrans=$IRTRANS_DIR/irtransdll --prefix=$INSTALL_DIR --enable-debug=yes "$@"
# 	check_exit_code "$?"
# }

# irtrans_install()
# {
# 	if [ ! -e "$IRTRANS_SHLIB_DIR/libIRTrans.so" ] || \
# 	   [ ! -e "$IRTRANS_IRSERVER_DIR/irserver" ]; then
# 		irtrans_make
# 	fi
# 	cp -v "$IRTRANS_SHLIB_DIR/libIRTrans.so" "$INSTALL_DIR/lib"
# 	cp -v "$IRTRANS_IRSERVER_DIR/irserver" "$INSTALL_DIR/bin"
#
# 	cd "$BACKEND_DIR/irtrans"
# 	check_exit_code "$?"
#
# 	if [ ! -e "Makefile" ]; then
# 		irtrans_backend_conf
# 	fi
#
# 	irtrans_print_title "BACKEND MAKE"
# 	generic_make
# 	irtrans_print_title "BACKEND MAKE INSTALL"
# 	make install
# 	cd "$OLDPWD"
# }

irtrans_clean()
{
	irtrans_print_title "MAKE CLEAN"
	cd "$IRTRANS_SHLIB_DIR/src"
	make clean
	cd "$IRTRANS_IRSERVER_DIR/src"
	make clean

#	irtrans_print_title "rm debs"
#	cd "$IRTRANS_DIR"
#	rm -rf irtrans-irserver_*.*
#	rm -rf irtrans-shlib*.*

#	echo ""
#	echo "Irserver and shlib removed"
#	echo "You may uninstall irtrans things with this:"
#	echo "dpkg --remove irtrans-shlib-dev irtrans-shlib irtrans-irserver"

# 	rm "$SCRIPT_TMP_DIR/irtrans_sb_target"
# 	cd "$IRTRANS_DIR"
# 	make clean
# 	cd "$OLDPWD"
# 	irtrans_print_title "BACKEND CLEAN"
# 	cd "$BACKEND_DIR/irtrans"
# 	make clean
# 	./autoclean.sh
# 	cd "$OLDPWD"
}

irtrans_print_title()
{
	print_title "IRTRANS $1"
}

irtrans_main "$@"

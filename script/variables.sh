
#
# Variables
#

# Get the absolute path of a file.
# Args 1: Relative path to file
get_absolute_path() {
	readlink -f "$1" 
	if [[ "$?" != "0" ]]; then
		echo "Error: Could not get absolute path to \"$1\"." 1>&2
		exit 1
	fi
}

SCRIPT_NAME=`basename "$0"`
SCRIPT_DIR=`get_absolute_path "$PWD"`
SCRIPT_TMP_DIR="$SCRIPT_DIR/tmp"
SCRIPT_PARENT_DIR=`get_absolute_path "$SCRIPT_DIR/.."`
SCRIPT_PATH="$SCRIPT_DIR/$SCRIPT_NAME"
BACKEND_DIR=`get_absolute_path ../backend`
IRRECO_DIR=`get_absolute_path ../irreco`
IRRECO_DATA="$IRRECO_DIR/data"
IRRECO_SRC="$IRRECO_DIR/src"
INSTALL_DIR=`get_absolute_path ".."`"/install"
MAKE_LOG_FILE="$SCRIPT_TMP_DIR/make_log"
RUN_LOG_FILE="$SCRIPT_TMP_DIR/run_log"
DEB_LOG_FILE="$SCRIPT_TMP_DIR/deb_log"
ERROR_CONTEXT=0
EXIT_CODE_FILE="$SCRIPT_TMP_DIR/exit_code"
ALL_BACKENDS_FILTER="(mythtv|irtrans)"

mkdir -p "$SCRIPT_TMP_DIR"

find_theme_dir()
{
	#THEME_DIR='../../../../themes'
	#if [ ! -d "$THEME_DIR" ]; then
	#	THEME_DIR='../../../themes'
	#	if [ ! -d "$THEME_DIR" ]; then
	#		THEME_DIR='../../themes'
	#		if [ ! -d "$THEME_DIR" ]; then
	#			echo "Cant find theme dir"
	#			exit 1
	#		fi
	#	fi
	#fi
	
	THEME_DIR='../themes'
	if [ ! -d "$THEME_DIR" ]; then
		echo "Cant find theme dir"
		exit 1
	fi
	
	THEME_DIR=`get_absolute_path "$THEME_DIR"`
}

find_irtrans_dir()
{
	#IRTRANS_DIR='../../../../irtrans/'
	#if [ ! -d "$IRTRANS_DIR" ]; then
	#	IRTRANS_DIR='../../../irtrans/'
	#	if [ ! -d "$IRTRANS_DIR" ]; then
	#		IRTRANS_DIR='../../irtrans/'
	#		if [ ! -d "$IRTRANS_DIR" ]; then
	#			echo "Cant find theme dir"
	#			exit 1
	#		fi
	#	fi
	#fi
	
	IRTRANS_DIR='../irtrans/'
	if [ ! -d "$IRTRANS_DIR" ]; then
		echo "Cant find theme dir"
		exit 1
	fi
	
	IRTRANS_DIR=`get_absolute_path "$IRTRANS_DIR"`
	IRTRANS_SHLIB_DIR="$IRTRANS_DIR/shlib"
	IRTRANS_IRSERVER_DIR="$IRTRANS_DIR/irserver"
}


#
# Utility functions
#

create_install_dir()
{
	if [ ! -e "$INSTALL_DIR" ]; then
		mkdir "$INSTALL_DIR"
		check_exit_code "$?"
	fi
}

scratchbox_need()
{
	if [[ "$_SBOX_DIR" == "" ||
	      "$_SBOX_RESTART_FILE" == "" ||
	      "$_SBOX_SHELL_PID" == "" ||
	      "$_SBOX_USER_GROUPNAME" == "" ]]; then
		echo "Error: Need scratchbox."
		exit 1
	fi
	
	#which sb-conf &> /dev/null
	#if [[ "$?" != "0" ]]; then
	#	echo "Error: Need scratchbox."
	#	exit 1
	#fi
}

scratchbox_avoid()
{
	if [[ "$_SBOX_DIR" != "" ||
	      "$_SBOX_RESTART_FILE" != "" ||
	      "$_SBOX_SHELL_PID" != "" ||
	      "$_SBOX_USER_GROUPNAME" != "" ]]; then
		echo "Error: Cant run this command inside scratchbox."
		exit 1
	fi	
	
	#which sb-conf &> /dev/null
	#if [[ "$?" == "0" ]]; then
	#	echo "Error: Cant run this command inside scratchbox."
	#	exit 1
	#fi
}

print_title()
{
	TITLE="$1"
	echo -en "\n___ "
	echo -n "$TITLE "
	
	LEN=`echo ${#TITLE}`
	for ((LEN += 6; LEN <= 80; LEN++)); do
		echo -n _
	done
	echo
}

#
# Check log for error & warnings
# Args 1: Log file
#      2: Exit on match
check_log()
{
	REGEX_IGNORE='(^if gcc |^[[:space:]]*gcc |^/bin/sh |^if /bin/sh|^/usr/share/aclocal/audiofile\.m4)'
	
	REGEX="(warning|error|cannot find"
	REGEX="$REGEX|^[0-9a-zA-Z\/._-]+\.[cho]:"
	REGEX="$REGEX|^[0-9a-zA-Z\/._-]+:[0-9]+"
	REGEX="$REGEX|^[0-9a-zA-Z\/._-]+:[0-9]+:[0-9]+"
	REGEX="$REGEX|^:[0-9a-zA-Z\/._-]+"
	REGEX="${REGEX})"
	
	cat "$1" | egrep -v "$REGEX_IGNORE" \
	| egrep -i -A $ERROR_CONTEXT -B $ERROR_CONTEXT --color=yes "$REGEX"
	EXIT_CODE="$?"
	if [[ "$2" == "1" ]]; then	
		if [[ "$EXIT_CODE" != "0" ]]; then
			echo "... none"
		else
			exit 1
		fi
	fi
}
	
save_exit_code()
{
	"$@"
	echo "$?" > "$EXIT_CODE_FILE"
}

get_exit_code()
{
	EXIT_CODE=`cat "$EXIT_CODE_FILE"`
	rm "$EXIT_CODE_FILE"
	return $EXIT_CODE
}

#
# Stop script if exit code is not 0
#
# Usage: check_exit_code "$?"
check_exit_code()
{
	if [[ "$1" != "0" ]]; then
		if [[ "$2" != "" ]]; then
			echo "Error:" "$2" "$3" "$4" "$5"
		fi
		echo "Exit code: $1"
		exit "$1"
	fi
}

scripts_src_pad()
{
	while read LINE; do
		echo "\\"
		echo -n "        $LINE"
		LEN=32-${#LINE}
		for ((; LEN > 0; LEN--)); do
			echo -n " "
		done
	done
	echo 
}



#
# Generic build functions
#

generic_make()
{
	MAKE_ARGS=("$@")
	TITLE_FUNC="$1"
	
	$TITLE_FUNC "MAKE"
	save_exit_code make "${MAKE_ARGS[@]:1}" 2>&1 |  tee "$MAKE_LOG_FILE"
	
	$TITLE_FUNC "ERRORS"
	check_log "$MAKE_LOG_FILE" "1"
	get_exit_code
	check_exit_code "$?"
}

generic_clean()
{
	TITLE_FUNC="$1"
	
	$TITLE_FUNC "CLEAN"
	
	if [ -e "Makefile" ]; then
		make uninstall
		make clean
		make distclean
	fi
	
	if [ -e "autoclean.sh" ]; then
		./autoclean.sh
	fi
}


#
# Save current sb_target to file, 
# and return 0 if it is the same as current target.
#
match_sb_target()
{
	SB_TARGET_FILE="$1"
	CURRENT_TARGET=`sb-conf current`
	
	if [ -f "$SB_TARGET_FILE" ]; then
		PREVIOUS_TARGET=`cat "$SB_TARGET_FILE"`
	else
		PREVIOUS_TARGET=""
	fi
	
	#echo "SB_TARGET_FILE = $SB_TARGET_FILE"
	#echo "CURRENT_TARGET = $CURRENT_TARGET"
	#echo "PREVIOUS_TARGET = $PREVIOUS_TARGET"
	
	if [[ "$CURRENT_TARGET" != "$PREVIOUS_TARGET" ]]; then
		echo "Saving \"$CURRENT_TARGET\" to \"$SB_TARGET_FILE\""
		echo "$CURRENT_TARGET" > "$SB_TARGET_FILE"
		check_exit_code "$?"
		#echo return 1
		return 1
	fi
	
	#echo return 0
	return 0
}







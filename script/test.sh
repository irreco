#!/bin/bash
cd `dirname "$0"`
source variables.sh
scratchbox_need

test_main()
{	
	COMMAND="$1"
	
	if [[ "$COMMAND" == "" ]]; then
		echo "Error: You did not give me a command to run!"
		test_usage
		exit 1
	fi
	
	#if [[ ! -e "$IRTRANS_SHLIB_DIR" ]]; then
	#	echo "Error: IRTrans dir does not exist!"
	#	exit 1
	#fi
		
	ARGS=("$@")
	case "$COMMAND" in
		--conf|conf)		test_conf "${ARGS[@]:1}";;
		--make|make)		test_make "${ARGS[@]:1}";;
		--test|test)		test_test "${ARGS[@]:1}";;
		--clean|clean)		test_clean "${ARGS[@]:1}";;
		--install|install|inst)	test_install "${ARGS[@]:1}";;
		--run|run)		test_run "${ARGS[@]:1}";;
		--runcheck|runcheck)	test_runcheck "${ARGS[@]:1}";;
		--run-in-stand-alone)	test_run_in_stand_alone "${ARGS[@]:1}";;
		--checklog|checklog)	test_checklog "${ARGS[@]:1}";;
		*)			echo "Error: Unknown command \"$COMMAND\"";
					test_usage;
					exit 1;;
	esac
}



test_usage()
{
	echo "Usage: $SCRIPT_NAME COMMAND [ options ]"
}

test_test()
{
	test_install
	test_run
}

test_clean()
{
	"$SCRIPT_DIR"/irreco.sh clean
	check_exit_code "$?"
	
	"$SCRIPT_DIR"/backend.sh all clean
	check_exit_code "$?"
	
	print_title "CLEAN INSTALL DIR"
	rm -rfv "$INSTALL_DIR"
	
	#print_title "CLEAN SCRIPT TMP"
	#rm -rfv "$SCRIPT_TMP_DIR/"*
}

test_conf()
{
	#"$SCRIPT_DIR"/irtrans.sh untar "irtrans_noccf"
	#check_exit_code "$?"
	#"$SCRIPT_DIR"/irtrans.sh make "irtrans_noccf"
	#check_exit_code "$?"
	
	match_sb_target "$SCRIPT_TMP_DIR/irreco_sb_target"
	if [[ "$?" != "0" ]]; then test_clean; fi
	
	mkdir -p "$INSTALL_DIR"
	
	if [ ! -f "$SCRIPT_PARENT_DIR"/irreco/Makefile ]; then
		"$SCRIPT_DIR"/irreco.sh conf
		check_exit_code "$?"
	fi
	
	"$SCRIPT_DIR"/irreco.sh install
	check_exit_code "$?"
	
	#if [ ! -f "$SCRIPT_PARENT_DIR"/backend/irtrans/Makefile ]; then
	#	echo $IRTRANS_SHLIB_DIR
	#	echo $IRTRANS_IRSERVER_DIR
	#
	#	"$SCRIPT_DIR"/backend.sh irtrans conf --prefix=/usr \
	#		"--with-irtrans=$IRTRANS_SHLIB_DIR" 
	#	check_exit_code "$?"
	#fi
	
	#if [ ! -f "$SCRIPT_PARENT_DIR"/backend/mythtv/installroot.tmp ]; then
	#	"$SCRIPT_DIR"/backend.sh mythtv conf
	#	check_exit_code "$?"
	#fi
	
	#"$SCRIPT_DIR"/backend.sh dummy conf
	#check_exit_code "$?"
	#"$SCRIPT_DIR"/backend.sh lirc conf
	#check_exit_code "$?"
	#"$SCRIPT_DIR"/backend.sh mythtv conf
	#check_exit_code "$?"
}

test_make()
{
	test_conf
	
	"$SCRIPT_DIR"/backend.sh all make
	check_exit_code "$?"
	
	#"$SCRIPT_DIR"/irreco.sh make
	#check_exit_code "$?"	
	#"$SCRIPT_DIR"/backend.sh dummy make
	#check_exit_code "$?"
	#"$SCRIPT_DIR"/backend.sh lirc make
	#check_exit_code "$?"
	#"$SCRIPT_DIR"/backend.sh irtrans make
	#check_exit_code "$?"
	#"$SCRIPT_DIR"/backend.sh mythtv make
	#check_exit_code "$?"
}

test_install()
{
	test_make
	
	"$SCRIPT_DIR"/backend.sh all install
	check_exit_code "$?"
	
	#cp "$IRTRANS_SHLIB_DIR/libIRTrans.so" "$INSTALL_DIR/lib"
	#check_exit_code "$?"
	
	#"$SCRIPT_DIR"/backend.sh dummy install
	#check_exit_code "$?"
	#"$SCRIPT_DIR"/backend.sh lirc install
	#check_exit_code "$?"
	#"$SCRIPT_DIR"/backend.sh irtrans install
	#check_exit_code "$?"
	#"$SCRIPT_DIR"/backend.sh mythtv install
	#check_exit_code "$?"
	#"$SCRIPT_DIR"/irtrans.sh install
	#check_exit_code "$?"
}

test_run()
{
	print_title "RUN"
	test_run_check_dir
	run-standalone.sh "$SCRIPT_PATH" --run-in-stand-alone "$@" \
		2>&1 | tee "$RUN_LOG_FILE"
	test_checklog
}

test_runcheck()
{
	print_title "RUN AND CHECK"
	test_run_check_dir
	run-standalone.sh "$SCRIPT_PATH" --run-in-stand-alone "$@" \
		2>&1 | tee "$RUN_LOG_FILE" \
		| sed 's|^|_|' | test_run_check_enter_exit
}

test_run_check_dir()
{
	# Prepare directories
	mkdir -p "$INSTALL_DIR/lib"
	check_exit_code "$?"
	mkdir -p "$INSTALL_DIR/home/user/MyDocs"
	check_exit_code "$?"
	
	if [ ! -e "$INSTALL_DIR/home/$USER" ]; then
		cd "$INSTALL_DIR/home"
		ln -s user "$USER" 
		check_exit_code "$?"
		cd "$OLDPWD"
	fi
}

test_run_in_stand_alone()
{
	test_run_check_dir
	
	# Change home dir
	#declare -x HOME="$INSTALL_DIR/home/user"
	
	# Fix paths
	declare -x PATH="$PATH:$INSTALL_DIR/bin"
	declare -x LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$INSTALL_DIR/lib"
	declare -x G_SLICE="always-malloc"
	declare -x G_DEBUG="gc-friendly"
	
	# Goto homedir
	cd "$INSTALL_DIR/home/user"
	check_exit_code "$?"
	
	# Start app
	if [[ "$1" == "" ]]; then
		echo "Running with default args."
		"$INSTALL_DIR"/bin/irreco --debug=99
	elif [[ "$1" == "strace" ]]; then
		echo "Running with strace:" "$@"
		strace "$INSTALL_DIR"/bin/irreco --debug=99
	elif [[ "$1" == "gdb" ]]; then
		echo "Running with gdb:" "$@"
		run-standalone.sh gdb "$INSTALL_DIR"/bin/irreco
	else
		echo "Args:" "$@"
		"$INSTALL_DIR"/bin/irreco "$@"
	fi
}

test_checklog()
{
	test_is_log_check_needed "$@" && \
		cat "$RUN_LOG_FILE" | sed 's|^|_|' | test_run_check_enter_exit
}

#
# Run some simple checks on the run_log to determinate if we need to do a 
# thorough check of the log.
#
test_is_log_check_needed()
{
	if [[ "$1" == "force" || "$1" == "--force" ]]; then
		echo -e "\n___ LOG CHECK FORCED ___________________________________________________________"
		return 0
	fi
	
	#
	# Dont check the log if core dump happend. In that case the log will
	# be incorrect anyways..
	#
	tail "$RUN_LOG_FILE" -n 1 | grep 'core dumped' > /dev/null
	if [[ "$?" == "0" ]]; then
		return 1
	fi
	
	grep '<-' "$RUN_LOG_FILE" | tail -n 1 | grep '^[^ ]' > /dev/null
	if [[ "$?" != "0" ]]; then
		echo -e "\n___ INDENT MISSMATCH, CHEKING LOG ______________________________________________"
		return 0
	fi
	
	grep 'INDENT COUNTER BELOW ZERO' "$RUN_LOG_FILE" > /dev/null
	if [[ "$?" == "0" ]]; then
		echo -e "\n___ INDENT MISSMATCH, CHEKING LOG ______________________________________________"
		return 0
	fi
	
	echo "... log indent appears to be fine."
	return 1
}

#
# This amazing piece of script checks that irreco ENTER and RETURN messages
# match each other.
#
test_run_check_enter_exit()
{
	PATTERN_ENTER='^[a-zA-Z]+[ ]*-> ([a-z_0-9]*)'
	PATTERN_RETURN='^[a-zA-Z]+[ ]*<- ([a-z_0-9]*)'
	DEBUG=0
	
	i=0
	while read -rs LINE; do
	
		# Bash read command strips leading spaces, so we use sed to 
		# insert _ the the beginning of every line. This strips
		# _ away, so we get the original string.
		LINE="${LINE:1}"
		echo "$LINE"
		
		echo "$LINE" | egrep "$PATTERN_ENTER" > /dev/null
		if [[ "$?" == "0" ]]; then
			((i++))
			FUNC=`echo "$LINE" | egrep -o "$PATTERN_ENTER" \
				| sed -r "s|$PATTERN_ENTER|\1|"`
			if [ "$DEBUG" == "1" ]; then echo "Enter $i: $FUNC"; fi
			INDENT[$i]="$FUNC"
		else
		
			echo "$LINE" | egrep "$PATTERN_RETURN" > /dev/null
			if [[ "$?" == "0" ]]; then
				
				FUNC=`echo "$LINE" | egrep -o "$PATTERN_RETURN" \
					| sed -r "s|$PATTERN_RETURN|\1|"`
				if [ "$DEBUG" == "1" ]; then echo "Return $i: $FUNC"; fi
				if [[ "${INDENT[$i]}" != "$FUNC" ]]; then
					echo -e "\n___ ERROR ______________________________________________________________________"
					echo "Returned from \"$FUNC\""
					echo "Instead of \"${INDENT[$i]}\""
					exit 1
				fi
				
				((i--))
				
			fi
		
		fi
	done
	
	if [[ "${INDENT[$i]}" != "" ]]; then
		echo -e "\n___ ERROR ______________________________________________________________________"
		echo "Did not return from \"${INDENT[$i]}\""
		echo "Program probably crashed inside of \"${INDENT[$i]}\""
		exit 1
	fi
}

test_main "$@"



#!/bin/sh
cd `dirname "$0"`
cd ..

#
# Pkg-config files are built so that they give correct paths
# if they are used to create a DEV package, however, they do
# not give correct paths if we want to get stuff directly from
# irreco sources.
# 
# This script creates modified pkg-config files which point
# directly to irreco sources. This ensures that linking works.
# 

IRRECO_DIR='./irreco'

fix_pkg_config_path_main()
{
	ARGS=("$@")
	case "$1" in
		--path|path)		fix_pkg_config_path "${ARGS[@]:1}";;
		--clean|clean)		fix_pkg_config_clean "${ARGS[@]:1}";;
		*)			echo "Error: Unknown command \"$1\"";
					exit 1;;
	esac
}

fix_pkg_config_path()
{
	grep -v '^Cflags:' $IRRECO_DIR/data/irreco.pc 	 > ./script/tmp/irreco.pc
	echo -n "Cflags: -I" 				 >> ./script/tmp/irreco.pc
	echo -n `readlink -f $IRRECO_DIR/src/api` 	 >> ./script/tmp/irreco.pc
	echo -n " -I" 					 >> ./script/tmp/irreco.pc
	echo -n `readlink -f $IRRECO_DIR/src/util` 	 >> ./script/tmp/irreco.pc
	echo 						 >> ./script/tmp/irreco.pc
	
	grep -v '^Libs:' $IRRECO_DIR/data/irreco-util.pc > ./script/tmp/irreco-util.pc
	echo -n "Libs: -L" 				 >> ./script/tmp/irreco-util.pc
	echo -n "$1" 					 >> ./script/tmp/irreco-util.pc
	echo -n " -l_irreco_util" 			 >> ./script/tmp/irreco-util.pc
	echo 						 >> ./script/tmp/irreco.pc
}

fix_pkg_config_clean()
{
	rm -fv ./script/tmp/irreco.pc
	rm -fv ./script/tmp/irreco-util.pc
}

fix_pkg_config_path_main "$@"







#!/bin/bash

# TODO version number checking
# TODO opt to make debug package of irreco
# TODO opt to make just cleaning

# Go to folder where this file lies.
cd `dirname "$0"`

deb_creator_main()
{
	echo ""
	echo "This script is deprecated."
	echo "You should send sources to compiling queue at maemo.org"
	echo ""
	exit 1;
	
	COMMAND="$1"
	if [[ "$COMMAND" == "" ]]; then
		info_print
		exit 1
	fi
	
	case "$COMMAND" in
		--release|release|rel)		enable_rel "${ARGS[@]:2}";;
		--developer|developer|dev)	enable_dev "${ARGS[@]:2}";;
		*)				echo "Error: Unknown command \"$COMMAND\"";
						info_print;
						exit 1;;
	esac

}

info_print()
{
	echo ""
	echo "Usage: deb-creator COMMAND"
	echo ""
	echo "Commands:"
	echo "    --release | release | rel"
	echo "        Create Irreco-core release package"
	echo "    --developer | developer | dev"
	echo "        Create a package for backend developers. This will"
	echo "        include Irreco, irreco-util pkg-config files,"
	echo "        irreco-api and irreco-util headers."
	echo "        Only create this package if you need to package backend"
	echo "        and only install this package to Scratchbox."
	echo ""
	echo " Deb package will be created to ../debs/"
}

enable_dev()
{
	echo ""
	echo "Creating developer package"
	echo ""
	export IRRECO_DEVELOPER=yes
	cp -fv  ../irreco/debian/control-dev ../irreco/debian/control
	make_deb
}

enable_rel()
{
	echo ""
	echo "Creating release package"
	echo ""
	cp -fv  ../irreco/debian/control-rel ../irreco/debian/control
	make_deb
}

make_deb()
{
	# Check we are in scratchbox
	if [[ "$_SBOX_DIR" == "" ||
		"$_SBOX_RESTART_FILE" == "" ||
		"$_SBOX_SHELL_PID" == "" ||
		"$_SBOX_USER_GROUPNAME" == "" ]]; then
			echo "Error: Need scratchbox."
			exit 1
	fi
		
	# Go to irreco-folder
	cd ../irreco
	
	# Clean stuff
	if [ -e "Makefile" ]; then
		make uninstall
		make clean
		make distclean
	fi
	if [ -e "autoclean.sh" ]; then
		./autoclean.sh
	fi
	
	rm -rfv ./debian/irreco
	
	# Create .deb (binary only)
	dpkg-buildpackage -rfakeroot -B
	
	# Ditch .deb to correct directory
	mkdir -v ../debs/
	mv -fv ../irreco-core* ../debs/
	
	# Clean
	cd ../script/
	./irreco.sh clean
}

deb_creator_main "$@"

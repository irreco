#!/bin/bash
cd `dirname "$0"`
source variables.sh
scratchbox_need

./irreco.sh clean
./backend.sh all clean
./make-deb.sh clean
./fix-pkg-config.sh clean

print_title "INSTALL CLEAN"
if [ -d "$INSTALL_DIR" ]; then
	rm -rvf "$INSTALL_DIR"
fi

print_title "SCRIPT TMP CLEAN"
rm -vf "$SCRIPT_TMP_DIR"/*

# rm -fv ../debs/*.deb ../debs/*.changes



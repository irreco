#!/bin/bash

# Go to folder where this file lies.
cd `dirname "$0"`	

main()
{
	echo ""
	echo "This script is deprecated, do not use."
	echo "Instead run \"/themes/make source\", sign and send to maemo.org"
	echo ""
	exit 1;
	case "$1" in
		--deb|-deb|deb|.deb|debaa|debbaa|debioi)
			make_deb;;			
		*)
			info_print;
			exit 1;;
	esac
}

info_print()
{
	echo ""
	echo " Usage: theme-deb-creator.sh COMMAND"
	echo ""
	echo " Commands:"
	echo "    --deb | -deb | deb"
	echo "        This creates theme .deb's"
	echo "        of evil, scifi, sunflower, default, modern and white."
	echo ""
}

make_deb()
{

	# Check we are in scratchbox
	if [[ "$_SBOX_DIR" == "" ||
		"$_SBOX_RESTART_FILE" == "" ||
		"$_SBOX_SHELL_PID" == "" ||
		"$_SBOX_USER_GROUPNAME" == "" ]]; then
			echo "Error: Need scratchbox."
			exit 1
	fi
	
	
	# Go to theme directory and build package
	cd ../themes/default/
	echo " Run dpkg-buildpackage for default theme"
	dpkg-buildpackage -rfakeroot -i

	if [[ "$?" == "1" ]]; then
		echo ""
		echo " Failed to build default theme package"
		echo " Aborting"
		echo ""
		exit 1
	fi

	
	# Go to theme directory and build package
	cd ../evil/
	echo " Run dpkg-buildpackage for evil theme"
	dpkg-buildpackage -rfakeroot -i

	if [[ "$?" == "1" ]]; then
		echo ""
		echo " Failed to build evil theme package"
		echo " Aborting"
		echo ""
		exit 1
	fi

	# Go to theme directory and build package
	cd ../scifi/
	echo " Run dpkg-buildpackage for scifi theme"
	dpkg-buildpackage -rfakeroot -i

	if [[ "$?" == "1" ]]; then
		echo ""
		echo " Failed to build scifi theme package"
		echo " Aborting"
		echo ""
		exit 1
	fi

	# Go to theme directory and build package
	cd ../sunflower/
	echo " Run dpkg-buildpackage for sunflower theme"
	dpkg-buildpackage -rfakeroot -i

	if [[ "$?" == "1" ]]; then
		echo ""
		echo " Failed to build sunflower theme package"
		echo " Aborting"
		echo ""
		exit 1
	fi

	# Go to theme directory and build package
	cd ../modern/
	echo " Run dpkg-buildpackage for modern theme"
	dpkg-buildpackage -rfakeroot -i

	if [[ "$?" == "1" ]]; then
		echo ""
		echo " Failed to build modern theme package"
		echo " Aborting"
		echo ""
		exit 1
	fi

	# Go to theme directory and build package
	cd ../white/
	echo " Run dpkg-buildpackage for white theme"
	dpkg-buildpackage -rfakeroot -i

	if [[ "$?" == "1" ]]; then
		echo ""
		echo " Failed to build white theme package"
		echo " Aborting"
		echo ""
		exit 1
	fi


	# Check there is place to ditch deb's
	#echo " Check debs directory exists"
	#if [ ! -e "../../debs/" ]; then
	#	mkdir -v ../../debs/
	#fi
	
	# Ditch .deb to correct directory
	# echo " Move deb's and changes's"
	#mv -fv ../irreco-theme-*.deb ../../debs/
	#mv -fv ../irreco-theme-*.changes ../../debs/
	

}

main "$@"

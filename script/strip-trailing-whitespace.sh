#!/bin/bash
cd `dirname "$0"`

strip_whitespace()
{
	COUNT=0
	while read FILE; do
		echo "Stripping whitespaces from: $FILE"
		sed -i -r -e 's|[ \t]+$||' $FILE
		((COUNT++))
	done
	echo "Stripped whitespaces from $COUNT files."
}

if [ "$1" = '--all' ]; then
	find ../ -type f -regex '.*\.[ch]' > tmp/tmp_file
	find ../ -type f -regex '.*\.php' >> tmp/tmp_file
	cat tmp/tmp_file \
	| xargs -d '\n' egrep -l '[[:space:]]+$' \
	| strip_whitespace
	rm tmp/tmp_file
elif [ -f "$1" ]; then
	echo "$1" | strip_whitespace
else
	echo "Usage: $0 FILENAME | --all"
fi

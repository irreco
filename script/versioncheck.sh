#!/bin/bash

cd `dirname "$0"`
source variables.sh

echo ""
echo " Check versions match in configure.ac, ChangeLog and /debian/changelog"
echo ""

# Check all version numbers match
REGEX='AC_INIT\(\[irreco\], \[(.*)\]\)'
VERSION_CONFIGURE=$(egrep -o '^'"$REGEX"'$' ../irreco/configure.ac | sed -r "s|$REGEX|\1|")
check_exit_code "$?"
if [[ "$VERSION_CONFIGURE" == "" ]]; then
	echo "Could not get version number."
	exit 1
fi

head ../irreco/ChangeLog -n 1 | grep "$VERSION_CONFIGURE" > /dev/null
check_exit_code "$?" "Version numbers dont match in ./ChangeLog and ./irreco/configure.ac"

head ../irreco/debian/changelog -n 1 | grep "$VERSION_CONFIGURE" > /dev/null
check_exit_code "$?" "Version numbers dont match in ./debian/changelog and ./irreco/configure.ac"

#head ../irreco/debian/changelog -n 1 | egrep '\([0-9.]+-(pre)*[0-9]+\)' > /dev/null
check_exit_code "$?" "Version number in ./debian/changelog must be in"\
	"the form (__SRC_VERSION__-__DEB_VERSION__). For example (0.5.6-1)."

echo " Version check OK"
echo ""

